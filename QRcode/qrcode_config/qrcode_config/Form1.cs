﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace qrcode_config
{
    public partial class Form1 : Form
    {
        private IniHandler iniHandler;

        private bool isSave = true;

        SqlConnection sqlConnection;

        public Form1()
        {
            InitializeComponent();

            iniHandler = new IniHandler(Application.StartupPath + "/config.ini");

            string connectionString = GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);

            if (iniHandler.isExistIni() == true)
            {
                string user = iniHandler.ReadIniFile("Setting", "User", "default");

                if (user == "MASTER")
                {
                    recordBtn.Enabled = true;
                    recordBtn.Visible = true;
                }
                else
                {
                    recordBtn.Enabled = false;
                    recordBtn.Visible = false;
                }
                
                NameTxt.Text = iniHandler.ReadIniFile("Setting", "Name", "");
                TypeTxt.Text = iniHandler.ReadIniFile("Setting", "Type_", "");
                CaseTxt.Text = iniHandler.ReadIniFile("Setting", "Case No", "");
                ItemTxt.Text = iniHandler.ReadIniFile("Setting", "Item No", "");
                FormatTxt.Text = iniHandler.ReadIniFile("Setting", "Format", "");
                WeightTxt.Text = iniHandler.ReadIniFile("Setting", "Weight", "");
                Motor_1Txt.Text = iniHandler.ReadIniFile("Setting", "MOTOR_1", "");
                FanTxt.Text = iniHandler.ReadIniFile("Setting", "FAN", "");
                Motor_2Txt.Text = iniHandler.ReadIniFile("Setting", "MOTOR_2", "");

                //UL
                powerTxt.Text = iniHandler.ReadIniFile("Setting", "PowerSource", "");
                inputTxt.Text = iniHandler.ReadIniFile("Setting", "Intput(Nominal)", "");
                currentTxt.Text = iniHandler.ReadIniFile("Setting", "OperatingCurrent", "");
                methodTxt.Text = iniHandler.ReadIniFile("Setting", "ProtectionMethod", "");

                string isLogoStr = iniHandler.ReadIniFile("Setting", "Logo", "");

                if (isLogoStr == "False")
                {
                    NoLOGOradioBtn.Checked = true;
                    groupBox4.Enabled = false;
                }
                else
                {
                    LOGOradioBtn.Checked = true;
                    groupBox4.Enabled = true;
                }

                string type = iniHandler.ReadIniFile("Setting", "Type", "");

                switch (type)
                {
                    case "DC":
                        DCRadioBtn.Checked = true;
                        break;
                    case "AC":
                        ACRadioBtn.Checked = true;
                        break;
                    case "EBM":
                        EBMRadioBtn.Checked = true;
                        break;
                    case "UL":
                        ULRadioBtn.Checked = true;
                        break;
                    default:
                        DCRadioBtn.Checked = true;
                        break;
                }

                string logo_Type = iniHandler.ReadIniFile("Setting", "Logo_Type", "");

                switch (logo_Type)
                {
                    case "1":
                        topwellRadioBtn.Checked = true;
                        break;
                    case "2":
                        puriumfilRadioBtn.Checked = true;
                        break;
                }

            }
            else
            {
                DCRadioBtn.Checked = true;
            }

            NameTxt.TextChanged += TextChanged;
            TypeTxt.TextChanged += TextChanged;
            CaseTxt.TextChanged += TextChanged;
            ItemTxt.TextChanged += TextChanged;
            FormatTxt.TextChanged += TextChanged;
            WeightTxt.TextChanged += TextChanged;
            FanTxt.TextChanged += TextChanged;
            Motor_1Txt.TextChanged += TextChanged;
            Motor_2Txt.TextChanged += TextChanged;
            powerTxt.TextChanged += TextChanged;
            inputTxt.TextChanged += TextChanged;
            currentTxt.TextChanged += TextChanged;
            methodTxt.TextChanged += TextChanged;

            NoLOGOradioBtn.CheckedChanged += TextChanged;
            LOGOradioBtn.CheckedChanged += TextChanged;

            DCRadioBtn.CheckedChanged += TextChanged;
            ACRadioBtn.CheckedChanged += TextChanged;
            EBMRadioBtn.CheckedChanged += TextChanged;
            ULRadioBtn.CheckedChanged += TextChanged;

            topwellRadioBtn.CheckedChanged += TextChanged;
            puriumfilRadioBtn.CheckedChanged += TextChanged;
        }

        private string GetConnectionString()
        {
            //return "Data Source=(local);"
            //      + "Integrated Security=SSPI;";
            //return "Data Source = 192.168.0.181;"
            //      + "User = sa; Password = 12;";
            string ServerName = iniHandler.ReadIniFile("Setting", "ServerName", "");
            if (string.IsNullOrWhiteSpace(ServerName)) {
                ServerName = "DESKTOP-8OH9HM4\\SQLEXPRESS";  //產線電腦資料庫 192.168.3.99
            }
            return "Data Source=" + ServerName + ";"
                + "User = sa; Password = 12;";
        }

        private void UseBtn_Click(object sender, EventArgs e)
        {
            if (iniHandler.isExistIni() == true)
            {
                this.Text = "QRCODE設定";
                isSave = true;

                iniHandler.WriteIniFile("Setting", "Name", NameTxt.Text);
                iniHandler.WriteIniFile("Setting", "Type_", TypeTxt.Text);
                iniHandler.WriteIniFile("Setting", "Case No", CaseTxt.Text);
                iniHandler.WriteIniFile("Setting", "Item No", ItemTxt.Text);
                iniHandler.WriteIniFile("Setting", "Format", FormatTxt.Text);
                iniHandler.WriteIniFile("Setting", "Weight", WeightTxt.Text);
                iniHandler.WriteIniFile("Setting", "FAN", FanTxt.Text);
                iniHandler.WriteIniFile("Setting", "MOTOR_1", Motor_1Txt.Text);
                iniHandler.WriteIniFile("Setting", "MOTOR_2", Motor_2Txt.Text);


                if (LOGOradioBtn.Checked == true)
                    iniHandler.WriteIniFile("Setting", "Logo", "True");
                else
                    iniHandler.WriteIniFile("Setting", "Logo", "False");

                if (DCRadioBtn.Checked)
                {
                    iniHandler.WriteIniFile("Setting", "Type", "DC");
                    iniHandler.WriteIniFile("Setting", "PowerSource", "");
                    iniHandler.WriteIniFile("Setting", "Intput(Nominal)", "");
                    iniHandler.WriteIniFile("Setting", "OperatingCurrent", "");
                    iniHandler.WriteIniFile("Setting", "ProtectionMethod", "");
                }
                else if (ACRadioBtn.Checked)
                {
                    iniHandler.WriteIniFile("Setting", "Type", "AC");
                    iniHandler.WriteIniFile("Setting", "PowerSource", "");
                    iniHandler.WriteIniFile("Setting", "Intput(Nominal)", "");
                    iniHandler.WriteIniFile("Setting", "OperatingCurrent", "");
                    iniHandler.WriteIniFile("Setting", "ProtectionMethod", "");
                }
                else if (EBMRadioBtn.Checked)
                {
                    iniHandler.WriteIniFile("Setting", "Type", "EBM");
                    iniHandler.WriteIniFile("Setting", "PowerSource", "");
                    iniHandler.WriteIniFile("Setting", "Intput(Nominal)", "");
                    iniHandler.WriteIniFile("Setting", "OperatingCurrent", "");
                    iniHandler.WriteIniFile("Setting", "ProtectionMethod", "");
                }
                else if (ULRadioBtn.Checked)
                {
                    iniHandler.WriteIniFile("Setting", "Type", "UL");
                    iniHandler.WriteIniFile("Setting", "PowerSource", powerTxt.Text);
                    iniHandler.WriteIniFile("Setting", "Intput(Nominal)", inputTxt.Text);
                    iniHandler.WriteIniFile("Setting", "OperatingCurrent", currentTxt.Text);
                    iniHandler.WriteIniFile("Setting", "ProtectionMethod", methodTxt.Text);
                }


                if (topwellRadioBtn.Checked)
                {
                    iniHandler.WriteIniFile("Setting", "Logo_Type", "1");
                }
                else if (puriumfilRadioBtn.Checked)
                {
                    iniHandler.WriteIniFile("Setting", "Logo_Type", "2");
                }

                MessageBox.Show("QRcode設定套用成功", "訊息");
            }
        }

        private void BringDataBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("連線異常,請確認ServerName設定是否正確!" + ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            String sql = "SELECT * FROM [QRCODE].[dbo].[QR_INFO] WHERE REPLACE(item_no,' ','') = '" + ItemNoTxt.Text.Replace(" ", "") + "'";


            SqlCommand command = new SqlCommand(sql, sqlConnection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        NameTxt.Text = reader.GetString(0);
                        TypeTxt.Text = reader.GetString(1);
                        CaseTxt.Text = reader.GetString(2);
                        ItemTxt.Text = reader.GetString(3);
                        FormatTxt.Text = reader.GetString(4);
                        WeightTxt.Text = reader.GetString(5);
                        FanTxt.Text = reader.GetString(6);
                        Motor_1Txt.Text = reader.GetString(7);
                        Motor_2Txt.Text = reader.GetString(8);

                        switch (reader.GetString(9))
                        {
                            case "DC":
                                DCRadioBtn.Checked = true;
                                powerTxt.Text = "";
                                inputTxt.Text = "";
                                currentTxt.Text = "";
                                methodTxt.Text = "";
                                ul_panel.Visible = false;
                                clearBtn.Location = new System.Drawing.Point(280, 578);
                                SaveBtn.Location = new System.Drawing.Point(160, 578);
                                recordBtn.Location = new System.Drawing.Point(40, 578);
                                this.Size = new System.Drawing.Size(422, 683);

                                label5.Text = "Type";
                                label8.Text = "Format";
                                break;
                            case "AC":
                                ACRadioBtn.Checked = true;
                                powerTxt.Text = "";
                                inputTxt.Text = "";
                                currentTxt.Text = "";
                                methodTxt.Text = "";
                                ul_panel.Visible = false;
                                clearBtn.Location = new System.Drawing.Point(280, 578);
                                SaveBtn.Location = new System.Drawing.Point(160, 578);
                                recordBtn.Location = new System.Drawing.Point(40, 578);
                                this.Size = new System.Drawing.Size(422, 683);
                                label5.Text = "Type";
                                label8.Text = "Format";
                                break;
                            case "EBM":
                                EBMRadioBtn.Checked = true;
                                powerTxt.Text = "";
                                inputTxt.Text = "";
                                currentTxt.Text = "";
                                methodTxt.Text = "";
                                ul_panel.Visible = false;
                                clearBtn.Location = new System.Drawing.Point(280, 578);
                                SaveBtn.Location = new System.Drawing.Point(160, 578);
                                recordBtn.Location = new System.Drawing.Point(40, 578);
                                this.Size = new System.Drawing.Size(422, 683);
                                label5.Text = "Type";
                                label8.Text = "Format";
                                break;
                            case "UL":
                                ULRadioBtn.Checked = true;
                                powerTxt.Text = reader.GetString(11);
                                inputTxt.Text = reader.GetString(12);
                                currentTxt.Text = reader.GetString(13);
                                methodTxt.Text = reader.GetString(14);
                                ul_panel.Visible = true;
                                clearBtn.Location = new System.Drawing.Point(280, 728);
                                SaveBtn.Location = new System.Drawing.Point(160, 728);
                                recordBtn.Location = new System.Drawing.Point(40, 728);
                                this.Size = new System.Drawing.Size(422, 833);
                                label5.Text = "Model";
                                label8.Text = "Size";
                                break;
                            default:
                                DCRadioBtn.Checked = true;
                                ul_panel.Visible = false;
                                clearBtn.Location = new System.Drawing.Point(280, 578);
                                SaveBtn.Location = new System.Drawing.Point(160, 578);
                                recordBtn.Location = new System.Drawing.Point(40, 578);
                                this.Size = new System.Drawing.Size(422, 683);
                                label5.Text = "Type";
                                label8.Text = "Format";
                                break;
                        }

                        if (reader.GetString(10) == "False")
                        {
                            NoLOGOradioBtn.Checked = true;
                            groupBox4.Enabled = false;
                        }
                        else
                        {
                            LOGOradioBtn.Checked = true;
                            groupBox4.Enabled = true;
                        }

                        if (reader.GetString(15) == "1")
                        {
                            topwellRadioBtn.Checked = true;
                        }
                        else if (reader.GetString(15) == "2")
                        {
                            puriumfilRadioBtn.Checked = true;
                        }

                    }
                    MessageBox.Show("帶入成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else{
                    MessageBox.Show("無此型號資料!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SetTextEmpty();
                }
            }

            sqlConnection.Close();

        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            SetTextEmpty();
        }

        private void SetTextEmpty()
        {
            DCRadioBtn.Checked = true;
            LOGOradioBtn.Checked = true;
            groupBox4.Enabled = true;
            topwellRadioBtn.Checked = true;
            NameTxt.Text = "";
            TypeTxt.Text = "";
            CaseTxt.Text = "";
            ItemTxt.Text = "";
            FormatTxt.Text = "";
            WeightTxt.Text = "";
            FanTxt.Text = "";
            Motor_1Txt.Text = "";
            Motor_2Txt.Text = "";
            powerTxt.Text = "";
            inputTxt.Text = "";
            currentTxt.Text = "";
            methodTxt.Text = "";
        }

        private void TextChanged(object sender, EventArgs e)
        {
            isSave = false;
            this.Text = "QRCODE設定  *未存檔";
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isSave == false)
            {
                if (MessageBox.Show("尚未存檔，確定要離開?", "警告", MessageBoxButtons.YesNo) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void RecordBtn_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(ItemTxt.Text.Replace(" ", ""))) {
                MessageBox.Show("Item No 不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
             }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("連線異常,請確認ServerName設定是否正確!" + ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            string logo = "True";
            if (NoLOGOradioBtn.Checked == true)
                logo = "False";
            else if (LOGOradioBtn.Checked == true)
                logo = "True";

            string type = "DC";
            if (DCRadioBtn.Checked == true)
                type = "DC";
            else if (ACRadioBtn.Checked == true)
                type = "AC";
            else if (EBMRadioBtn.Checked == true)
                type = "EBM";
            else if (ULRadioBtn.Checked == true)
                type = "UL";

            string logo_Type = "1";
            if (topwellRadioBtn.Checked == true)
            {
                logo_Type = "1";
            }
            else if (puriumfilRadioBtn.Checked == true)
            {
                logo_Type = "2";
            }


                String sql = "SELECT count(1) FROM [QRCODE].[dbo].[QR_INFO] WHERE REPLACE(item_no,' ','') = '" + ItemTxt.Text.Replace(" ","") + "'";

            SqlCommand command = new SqlCommand(sql, sqlConnection);
            int itemNoCount = 0;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        itemNoCount = reader.GetInt32(0);
                    }
                }
            }

            if (itemNoCount == 0) {  //新增

                String insertUserSql = "INSERT INTO [QRCODE].[dbo].[QR_INFO]" +
                                       " (name,type_,case_no,item_no,format,weight,fan,motor_1,motor_2,type,logo,power_source,input,operating_current,protection_method,logo_type) " +
                                       " VALUES ('" + NameTxt.Text.Replace(" ", "") + "' , '" + TypeTxt.Text.Replace(" ", "") + "' , '" + CaseTxt.Text.Replace(" ", "") + "'," +
                                       "          '" + ItemTxt.Text.Replace(" ", "") + "' , '" + FormatTxt.Text.Replace(" ", "") + "' , '" + WeightTxt.Text.Replace(" ", "") + "'," +
                                       "          '" + FanTxt.Text.Replace(" ", "") + "' , '" + Motor_1Txt.Text.Replace(" ", "") + "' , '" + Motor_2Txt.Text.Replace(" ", "") + "'," +
                                       "          '" + type + "' , '" + logo + "' , '" + powerTxt.Text.Replace(" ", "") + "' , " +
                                       "          '" + inputTxt.Text.Replace(" ", "") + "' , '" + currentTxt.Text.Replace(" ", "") + "' , '" + methodTxt.Text.Replace(" ", "") + "'," +
                                       "          '" + logo_Type + "')"; 

                try
                {
                    command = new SqlCommand(insertUserSql, sqlConnection);
                    if (command.ExecuteNonQuery().ToString() != "0")
                    {
                        MessageBox.Show("新增成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("新增失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            else //修改
            {
                String updateUserSql = "UPDATE [QRCODE].[dbo].[QR_INFO] " +
                                       "SET name = '" + NameTxt.Text.Replace(" ", "") + "', type_ = '" + TypeTxt.Text.Replace(" ", "") + "'," +
                                       "    case_no = '" + CaseTxt.Text.Replace(" ", "") + "', format = '" + FormatTxt.Text.Replace(" ", "") + "'," +
                                       "    weight = '" + WeightTxt.Text.Replace(" ", "") + "', fan = '" + FanTxt.Text.Replace(" ", "") + "'," +
                                       "    motor_1 = '" + Motor_1Txt.Text.Replace(" ", "") + "', motor_2 = '" + Motor_2Txt.Text.Replace(" ", "") + "'," +
                                       "    type = '" + type + "', logo = '" + logo + "' ," +
                                       "    power_source = '" + powerTxt.Text.Replace(" ", "") + "', input = '" + inputTxt.Text.Replace(" ", "") + "'," +
                                       "    operating_current = '" + currentTxt.Text.Replace(" ", "") + "', protection_method = '" + methodTxt.Text.Replace(" ", "") + "', " +
                                       "    logo_type = '" + logo_Type + "' " +
                                       "WHERE REPLACE(item_no,' ','') = '" + ItemTxt.Text.Replace(" ", "") + "'";

                try
                {
                    command = new SqlCommand(updateUserSql, sqlConnection);
                    if (command.ExecuteNonQuery().ToString() != "0")
                    {
                        MessageBox.Show("儲存成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("儲存失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }

            sqlConnection.Close();
        }

        private void ItemNoTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                ItemNoTxt.Text = ItemNoTxt.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ItemTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                ItemTxt.Text = ItemTxt.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Type_RadioBtn_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Name == "ULRadioBtn")
            {
                ul_panel.Visible = true;
                label5.Text = "Model";
                label8.Text = "Size";
                foreach (Control control in ul_panel.Controls)
                {
                    if(control is TextBox)
                    {
                        control.Text = "";
                    }
                }
                clearBtn.Location = new System.Drawing.Point(280, 728);
                SaveBtn.Location = new System.Drawing.Point(160, 728);
                recordBtn.Location = new System.Drawing.Point(40, 728);
                this.Size = new System.Drawing.Size(422, 833);
            }
            else
            {
                ul_panel.Visible = false;
                label5.Text = "Type";
                label8.Text = "Format";
                foreach (Control control in ul_panel.Controls)
                {
                    if (control is TextBox)
                    {
                        control.Text = "";
                    }
                }
                clearBtn.Location = new System.Drawing.Point(280, 578);
                SaveBtn.Location = new System.Drawing.Point(160, 578);
                recordBtn.Location = new System.Drawing.Point(40, 578);
                this.Size = new System.Drawing.Size(422, 683);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (ULRadioBtn.Checked)
            {
                ul_panel.Visible = true;
                clearBtn.Location = new System.Drawing.Point(280, 728);
                SaveBtn.Location = new System.Drawing.Point(160, 728);
                recordBtn.Location = new System.Drawing.Point(40, 728);
                this.Size = new System.Drawing.Size(422, 833);
                label5.Text = "Model";
                label8.Text = "Size";
            }
            else
            {
                ul_panel.Visible = false;
                clearBtn.Location = new System.Drawing.Point(280, 578);
                SaveBtn.Location = new System.Drawing.Point(160, 578);
                recordBtn.Location = new System.Drawing.Point(40, 578);
                this.Size = new System.Drawing.Size(422, 683);
                label5.Text = "Type";
                label8.Text = "Format";
            }
        }

        private void LOGOradioBtn_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = true;
        }

        private void NoLOGOradioBtn_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = false;
        }
    }
}
