﻿namespace qrcode_config
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Motor_2Txt = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.Motor_1Txt = new System.Windows.Forms.TextBox();
            this.FanTxt = new System.Windows.Forms.TextBox();
            this.WeightTxt = new System.Windows.Forms.TextBox();
            this.FormatTxt = new System.Windows.Forms.TextBox();
            this.ItemTxt = new System.Windows.Forms.TextBox();
            this.CaseTxt = new System.Windows.Forms.TextBox();
            this.TypeTxt = new System.Windows.Forms.TextBox();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ULRadioBtn = new System.Windows.Forms.RadioButton();
            this.DCRadioBtn = new System.Windows.Forms.RadioButton();
            this.EBMRadioBtn = new System.Windows.Forms.RadioButton();
            this.ACRadioBtn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LOGOradioBtn = new System.Windows.Forms.RadioButton();
            this.NoLOGOradioBtn = new System.Windows.Forms.RadioButton();
            this.ItemNoTxt = new System.Windows.Forms.TextBox();
            this.BringDataBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.recordBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.ul_panel = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.methodTxt = new System.Windows.Forms.TextBox();
            this.currentTxt = new System.Windows.Forms.TextBox();
            this.inputTxt = new System.Windows.Forms.TextBox();
            this.method_label = new System.Windows.Forms.Label();
            this.current_label = new System.Windows.Forms.Label();
            this.input_label = new System.Windows.Forms.Label();
            this.powerTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.power_label = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.topwellRadioBtn = new System.Windows.Forms.RadioButton();
            this.puriumfilRadioBtn = new System.Windows.Forms.RadioButton();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.ul_panel.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Motor_2Txt
            // 
            this.Motor_2Txt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Motor_2Txt.Location = new System.Drawing.Point(217, 640);
            this.Motor_2Txt.Name = "Motor_2Txt";
            this.Motor_2Txt.Size = new System.Drawing.Size(281, 35);
            this.Motor_2Txt.TabIndex = 37;
            this.Motor_2Txt.Text = " ";
            // 
            // SaveBtn
            // 
            this.SaveBtn.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SaveBtn.Location = new System.Drawing.Point(210, 895);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(116, 60);
            this.SaveBtn.TabIndex = 36;
            this.SaveBtn.Text = "套用";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.UseBtn_Click);
            // 
            // Motor_1Txt
            // 
            this.Motor_1Txt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Motor_1Txt.Location = new System.Drawing.Point(217, 599);
            this.Motor_1Txt.Name = "Motor_1Txt";
            this.Motor_1Txt.Size = new System.Drawing.Size(281, 35);
            this.Motor_1Txt.TabIndex = 35;
            // 
            // FanTxt
            // 
            this.FanTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FanTxt.Location = new System.Drawing.Point(217, 549);
            this.FanTxt.Name = "FanTxt";
            this.FanTxt.Size = new System.Drawing.Size(281, 35);
            this.FanTxt.TabIndex = 34;
            // 
            // WeightTxt
            // 
            this.WeightTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.WeightTxt.Location = new System.Drawing.Point(217, 505);
            this.WeightTxt.Name = "WeightTxt";
            this.WeightTxt.Size = new System.Drawing.Size(281, 35);
            this.WeightTxt.TabIndex = 33;
            // 
            // FormatTxt
            // 
            this.FormatTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormatTxt.Location = new System.Drawing.Point(217, 455);
            this.FormatTxt.Name = "FormatTxt";
            this.FormatTxt.Size = new System.Drawing.Size(281, 35);
            this.FormatTxt.TabIndex = 32;
            // 
            // ItemTxt
            // 
            this.ItemTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ItemTxt.Location = new System.Drawing.Point(217, 410);
            this.ItemTxt.Name = "ItemTxt";
            this.ItemTxt.Size = new System.Drawing.Size(281, 35);
            this.ItemTxt.TabIndex = 31;
            this.ItemTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ItemTxt_KeyUp);
            // 
            // CaseTxt
            // 
            this.CaseTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.CaseTxt.Location = new System.Drawing.Point(217, 363);
            this.CaseTxt.Name = "CaseTxt";
            this.CaseTxt.Size = new System.Drawing.Size(281, 35);
            this.CaseTxt.TabIndex = 30;
            // 
            // TypeTxt
            // 
            this.TypeTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TypeTxt.Location = new System.Drawing.Point(217, 314);
            this.TypeTxt.Name = "TypeTxt";
            this.TypeTxt.Size = new System.Drawing.Size(281, 35);
            this.TypeTxt.TabIndex = 29;
            // 
            // NameTxt
            // 
            this.NameTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.NameTxt.Location = new System.Drawing.Point(217, 262);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(281, 35);
            this.NameTxt.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(20, 602);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 24);
            this.label11.TabIndex = 27;
            this.label11.Text = "Motor";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(20, 552);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 24);
            this.label10.TabIndex = 26;
            this.label10.Text = "Fan";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(20, 508);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 24);
            this.label9.TabIndex = 25;
            this.label9.Text = "Weight";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(20, 458);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 24);
            this.label8.TabIndex = 24;
            this.label8.Text = "Format";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(20, 413);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 24);
            this.label7.TabIndex = 23;
            this.label7.Text = "Item No";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(20, 366);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 24);
            this.label6.TabIndex = 22;
            this.label6.Text = "Case No";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(20, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 24);
            this.label5.TabIndex = 21;
            this.label5.Text = "Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(20, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 24);
            this.label4.TabIndex = 20;
            this.label4.Text = "Name";
            // 
            // clearBtn
            // 
            this.clearBtn.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.clearBtn.Location = new System.Drawing.Point(367, 896);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(116, 60);
            this.clearBtn.TabIndex = 46;
            this.clearBtn.Text = "清空";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ULRadioBtn);
            this.groupBox2.Controls.Add(this.DCRadioBtn);
            this.groupBox2.Controls.Add(this.EBMRadioBtn);
            this.groupBox2.Controls.Add(this.ACRadioBtn);
            this.groupBox2.Location = new System.Drawing.Point(36, 186);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(447, 50);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " ";
            // 
            // ULRadioBtn
            // 
            this.ULRadioBtn.AutoSize = true;
            this.ULRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ULRadioBtn.Location = new System.Drawing.Point(363, 20);
            this.ULRadioBtn.Name = "ULRadioBtn";
            this.ULRadioBtn.Size = new System.Drawing.Size(55, 24);
            this.ULRadioBtn.TabIndex = 58;
            this.ULRadioBtn.Text = "UL";
            this.ULRadioBtn.UseVisualStyleBackColor = true;
            this.ULRadioBtn.Click += new System.EventHandler(this.Type_RadioBtn_Click);
            // 
            // DCRadioBtn
            // 
            this.DCRadioBtn.AutoSize = true;
            this.DCRadioBtn.Checked = true;
            this.DCRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.DCRadioBtn.Location = new System.Drawing.Point(33, 20);
            this.DCRadioBtn.Name = "DCRadioBtn";
            this.DCRadioBtn.Size = new System.Drawing.Size(57, 24);
            this.DCRadioBtn.TabIndex = 41;
            this.DCRadioBtn.TabStop = true;
            this.DCRadioBtn.Text = "DC";
            this.DCRadioBtn.UseVisualStyleBackColor = true;
            this.DCRadioBtn.Click += new System.EventHandler(this.Type_RadioBtn_Click);
            // 
            // EBMRadioBtn
            // 
            this.EBMRadioBtn.AutoSize = true;
            this.EBMRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.EBMRadioBtn.Location = new System.Drawing.Point(143, 20);
            this.EBMRadioBtn.Name = "EBMRadioBtn";
            this.EBMRadioBtn.Size = new System.Drawing.Size(71, 24);
            this.EBMRadioBtn.TabIndex = 43;
            this.EBMRadioBtn.Text = "EBM";
            this.EBMRadioBtn.UseVisualStyleBackColor = true;
            this.EBMRadioBtn.Click += new System.EventHandler(this.Type_RadioBtn_Click);
            // 
            // ACRadioBtn
            // 
            this.ACRadioBtn.AutoSize = true;
            this.ACRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ACRadioBtn.Location = new System.Drawing.Point(253, 20);
            this.ACRadioBtn.Name = "ACRadioBtn";
            this.ACRadioBtn.Size = new System.Drawing.Size(57, 24);
            this.ACRadioBtn.TabIndex = 42;
            this.ACRadioBtn.Text = "AC";
            this.ACRadioBtn.UseVisualStyleBackColor = true;
            this.ACRadioBtn.Click += new System.EventHandler(this.Type_RadioBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LOGOradioBtn);
            this.groupBox1.Controls.Add(this.NoLOGOradioBtn);
            this.groupBox1.Location = new System.Drawing.Point(36, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(447, 54);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " ";
            // 
            // LOGOradioBtn
            // 
            this.LOGOradioBtn.AutoSize = true;
            this.LOGOradioBtn.Checked = true;
            this.LOGOradioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LOGOradioBtn.Location = new System.Drawing.Point(33, 16);
            this.LOGOradioBtn.Name = "LOGOradioBtn";
            this.LOGOradioBtn.Size = new System.Drawing.Size(103, 24);
            this.LOGOradioBtn.TabIndex = 38;
            this.LOGOradioBtn.TabStop = true;
            this.LOGOradioBtn.Text = "有LOGO";
            this.LOGOradioBtn.UseVisualStyleBackColor = true;
            this.LOGOradioBtn.Click += new System.EventHandler(this.LOGOradioBtn_Click);
            // 
            // NoLOGOradioBtn
            // 
            this.NoLOGOradioBtn.AutoSize = true;
            this.NoLOGOradioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.NoLOGOradioBtn.Location = new System.Drawing.Point(186, 16);
            this.NoLOGOradioBtn.Name = "NoLOGOradioBtn";
            this.NoLOGOradioBtn.Size = new System.Drawing.Size(103, 24);
            this.NoLOGOradioBtn.TabIndex = 39;
            this.NoLOGOradioBtn.Text = "無LOGO";
            this.NoLOGOradioBtn.UseVisualStyleBackColor = true;
            this.NoLOGOradioBtn.Click += new System.EventHandler(this.NoLOGOradioBtn_Click);
            // 
            // ItemNoTxt
            // 
            this.ItemNoTxt.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ItemNoTxt.Location = new System.Drawing.Point(109, 21);
            this.ItemNoTxt.Name = "ItemNoTxt";
            this.ItemNoTxt.Size = new System.Drawing.Size(181, 31);
            this.ItemNoTxt.TabIndex = 45;
            this.ItemNoTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ItemNoTxt_KeyUp);
            // 
            // BringDataBtn
            // 
            this.BringDataBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.BringDataBtn.Location = new System.Drawing.Point(305, 15);
            this.BringDataBtn.Name = "BringDataBtn";
            this.BringDataBtn.Size = new System.Drawing.Size(118, 39);
            this.BringDataBtn.TabIndex = 46;
            this.BringDataBtn.Text = "帶入設定";
            this.BringDataBtn.UseVisualStyleBackColor = true;
            this.BringDataBtn.Click += new System.EventHandler(this.BringDataBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 24);
            this.label1.TabIndex = 47;
            this.label1.Text = "型號 :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.BringDataBtn);
            this.groupBox3.Controls.Add(this.ItemNoTxt);
            this.groupBox3.Location = new System.Drawing.Point(36, -2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(447, 62);
            this.groupBox3.TabIndex = 48;
            this.groupBox3.TabStop = false;
            // 
            // recordBtn
            // 
            this.recordBtn.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.recordBtn.Location = new System.Drawing.Point(56, 896);
            this.recordBtn.Name = "recordBtn";
            this.recordBtn.Size = new System.Drawing.Size(116, 60);
            this.recordBtn.TabIndex = 49;
            this.recordBtn.Text = "儲存";
            this.recordBtn.UseVisualStyleBackColor = true;
            this.recordBtn.Click += new System.EventHandler(this.RecordBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 14F);
            this.label2.Location = new System.Drawing.Point(190, 265);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 24);
            this.label2.TabIndex = 50;
            this.label2.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 14F);
            this.label3.Location = new System.Drawing.Point(190, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 24);
            this.label3.TabIndex = 51;
            this.label3.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("新細明體", 14F);
            this.label12.Location = new System.Drawing.Point(190, 366);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 24);
            this.label12.TabIndex = 52;
            this.label12.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("新細明體", 14F);
            this.label13.Location = new System.Drawing.Point(190, 413);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 24);
            this.label13.TabIndex = 53;
            this.label13.Text = ":";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("新細明體", 14F);
            this.label14.Location = new System.Drawing.Point(190, 458);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 24);
            this.label14.TabIndex = 54;
            this.label14.Text = ":";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("新細明體", 14F);
            this.label15.Location = new System.Drawing.Point(190, 508);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 24);
            this.label15.TabIndex = 55;
            this.label15.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("新細明體", 14F);
            this.label16.Location = new System.Drawing.Point(190, 552);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 24);
            this.label16.TabIndex = 56;
            this.label16.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("新細明體", 14F);
            this.label17.Location = new System.Drawing.Point(190, 602);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 24);
            this.label17.TabIndex = 57;
            this.label17.Text = ":";
            // 
            // ul_panel
            // 
            this.ul_panel.Controls.Add(this.label20);
            this.ul_panel.Controls.Add(this.label21);
            this.ul_panel.Controls.Add(this.label22);
            this.ul_panel.Controls.Add(this.methodTxt);
            this.ul_panel.Controls.Add(this.currentTxt);
            this.ul_panel.Controls.Add(this.inputTxt);
            this.ul_panel.Controls.Add(this.method_label);
            this.ul_panel.Controls.Add(this.current_label);
            this.ul_panel.Controls.Add(this.input_label);
            this.ul_panel.Controls.Add(this.powerTxt);
            this.ul_panel.Controls.Add(this.label19);
            this.ul_panel.Controls.Add(this.power_label);
            this.ul_panel.Location = new System.Drawing.Point(12, 681);
            this.ul_panel.Name = "ul_panel";
            this.ul_panel.Size = new System.Drawing.Size(519, 209);
            this.ul_panel.TabIndex = 458;
            this.ul_panel.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("新細明體", 14F);
            this.label20.Location = new System.Drawing.Point(178, 159);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 24);
            this.label20.TabIndex = 81;
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("新細明體", 14F);
            this.label21.Location = new System.Drawing.Point(178, 109);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 24);
            this.label21.TabIndex = 80;
            this.label21.Text = ":";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("新細明體", 14F);
            this.label22.Location = new System.Drawing.Point(178, 65);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 24);
            this.label22.TabIndex = 79;
            this.label22.Text = ":";
            // 
            // methodTxt
            // 
            this.methodTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.methodTxt.Location = new System.Drawing.Point(205, 156);
            this.methodTxt.Name = "methodTxt";
            this.methodTxt.Size = new System.Drawing.Size(281, 35);
            this.methodTxt.TabIndex = 78;
            // 
            // currentTxt
            // 
            this.currentTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.currentTxt.Location = new System.Drawing.Point(205, 106);
            this.currentTxt.Name = "currentTxt";
            this.currentTxt.Size = new System.Drawing.Size(281, 35);
            this.currentTxt.TabIndex = 77;
            // 
            // inputTxt
            // 
            this.inputTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.inputTxt.Location = new System.Drawing.Point(205, 62);
            this.inputTxt.Name = "inputTxt";
            this.inputTxt.Size = new System.Drawing.Size(281, 35);
            this.inputTxt.TabIndex = 76;
            // 
            // method_label
            // 
            this.method_label.AutoSize = true;
            this.method_label.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.method_label.Location = new System.Drawing.Point(8, 164);
            this.method_label.Name = "method_label";
            this.method_label.Size = new System.Drawing.Size(161, 19);
            this.method_label.TabIndex = 75;
            this.method_label.Text = "Protection Method";
            // 
            // current_label
            // 
            this.current_label.AutoSize = true;
            this.current_label.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.current_label.Location = new System.Drawing.Point(8, 113);
            this.current_label.Name = "current_label";
            this.current_label.Size = new System.Drawing.Size(158, 19);
            this.current_label.TabIndex = 74;
            this.current_label.Text = "Operating Current";
            // 
            // input_label
            // 
            this.input_label.AutoSize = true;
            this.input_label.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.input_label.Location = new System.Drawing.Point(8, 69);
            this.input_label.Name = "input_label";
            this.input_label.Size = new System.Drawing.Size(136, 19);
            this.input_label.TabIndex = 73;
            this.input_label.Text = "Input(Nominal)";
            // 
            // powerTxt
            // 
            this.powerTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.powerTxt.Location = new System.Drawing.Point(205, 10);
            this.powerTxt.Name = "powerTxt";
            this.powerTxt.Size = new System.Drawing.Size(281, 35);
            this.powerTxt.TabIndex = 72;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("新細明體", 14F);
            this.label19.Location = new System.Drawing.Point(178, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 24);
            this.label19.TabIndex = 71;
            this.label19.Text = ":";
            // 
            // power_label
            // 
            this.power_label.AutoSize = true;
            this.power_label.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.power_label.Location = new System.Drawing.Point(8, 17);
            this.power_label.Name = "power_label";
            this.power_label.Size = new System.Drawing.Size(122, 19);
            this.power_label.TabIndex = 70;
            this.power_label.Text = "Power Source";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.topwellRadioBtn);
            this.groupBox4.Controls.Add(this.puriumfilRadioBtn);
            this.groupBox4.Location = new System.Drawing.Point(36, 126);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(447, 54);
            this.groupBox4.TabIndex = 459;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " ";
            // 
            // topwellRadioBtn
            // 
            this.topwellRadioBtn.AutoSize = true;
            this.topwellRadioBtn.Checked = true;
            this.topwellRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.topwellRadioBtn.Location = new System.Drawing.Point(33, 16);
            this.topwellRadioBtn.Name = "topwellRadioBtn";
            this.topwellRadioBtn.Size = new System.Drawing.Size(70, 24);
            this.topwellRadioBtn.TabIndex = 38;
            this.topwellRadioBtn.TabStop = true;
            this.topwellRadioBtn.Text = "奇立";
            this.topwellRadioBtn.UseVisualStyleBackColor = true;
            // 
            // puriumfilRadioBtn
            // 
            this.puriumfilRadioBtn.AutoSize = true;
            this.puriumfilRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.puriumfilRadioBtn.Location = new System.Drawing.Point(186, 16);
            this.puriumfilRadioBtn.Name = "puriumfilRadioBtn";
            this.puriumfilRadioBtn.Size = new System.Drawing.Size(90, 24);
            this.puriumfilRadioBtn.TabIndex = 39;
            this.puriumfilRadioBtn.Text = "普利恩";
            this.puriumfilRadioBtn.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(543, 970);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.ul_panel);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.recordBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.Motor_2Txt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Motor_1Txt);
            this.Controls.Add(this.FanTxt);
            this.Controls.Add(this.WeightTxt);
            this.Controls.Add(this.FormatTxt);
            this.Controls.Add(this.ItemTxt);
            this.Controls.Add(this.CaseTxt);
            this.Controls.Add(this.TypeTxt);
            this.Controls.Add(this.NameTxt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QRCODE設定";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ul_panel.ResumeLayout(false);
            this.ul_panel.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox Motor_2Txt;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.TextBox Motor_1Txt;
        private System.Windows.Forms.TextBox FanTxt;
        private System.Windows.Forms.TextBox WeightTxt;
        private System.Windows.Forms.TextBox FormatTxt;
        private System.Windows.Forms.TextBox ItemTxt;
        private System.Windows.Forms.TextBox CaseTxt;
        private System.Windows.Forms.TextBox TypeTxt;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton DCRadioBtn;
        private System.Windows.Forms.RadioButton EBMRadioBtn;
        private System.Windows.Forms.RadioButton ACRadioBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton LOGOradioBtn;
        private System.Windows.Forms.RadioButton NoLOGOradioBtn;
        private System.Windows.Forms.TextBox ItemNoTxt;
        private System.Windows.Forms.Button BringDataBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button recordBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton ULRadioBtn;
        private System.Windows.Forms.Panel ul_panel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox methodTxt;
        private System.Windows.Forms.TextBox currentTxt;
        private System.Windows.Forms.TextBox inputTxt;
        private System.Windows.Forms.Label method_label;
        private System.Windows.Forms.Label current_label;
        private System.Windows.Forms.Label input_label;
        private System.Windows.Forms.TextBox powerTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label power_label;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton topwellRadioBtn;
        private System.Windows.Forms.RadioButton puriumfilRadioBtn;
    }
}

