﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace qrcode_config
{
    class IniHandler
    {
        private string filePath;
        private StringBuilder lpReturnedString;
        private int bufferSize;
        private uint MAX_BUFFER;
        private IntPtr pReturnedString;
        private uint bytesReturned;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string lpString, string lpFileName);
 
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);


        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern uint GetPrivateProfileSectionNames(IntPtr lpszReturnBuffer, uint nSize, string lpFileName);

        public IniHandler(string iniPath)
        {
            filePath = iniPath;
            bufferSize = 512;
            MAX_BUFFER = 32767;
            lpReturnedString = new StringBuilder(bufferSize);
        }

        public bool isExistIni()
        {
            return File.Exists(filePath);
        }

        // read ini date depend on section and key
        public string ReadIniFile(string section, string key, string defaultValue)
        {
            lpReturnedString.Clear();
            GetPrivateProfileString(section, key, defaultValue, lpReturnedString, bufferSize, filePath);
            return lpReturnedString.ToString();
        }

        public string[] ReadSectionIniFile()
        {
            pReturnedString = Marshal.AllocCoTaskMem((int)MAX_BUFFER);
            bytesReturned = GetPrivateProfileSectionNames(pReturnedString, MAX_BUFFER, filePath);
            return IntPtrToStringArray(pReturnedString, bytesReturned);
        }

        //指標資料轉字串陣列
        private string[] IntPtrToStringArray(IntPtr pReturnedString, uint bytesReturned)
        {
            //use of Substring below removes terminating null for split
            if (bytesReturned == 0)
            {
                Marshal.FreeCoTaskMem(pReturnedString);
                return null;
            }
            string local = Marshal.PtrToStringAnsi(pReturnedString, (int)bytesReturned).ToString();
            Marshal.FreeCoTaskMem(pReturnedString);
            return local.Substring(0, local.Length - 1).Replace("\0\0",",").Replace("\0","").Split(',');
        }

        // write ini data depend on section and key
        public void WriteIniFile(string section, string key, Object value)
        {
            WritePrivateProfileString(section, key, value.ToString(), filePath);
        }
    }
}
