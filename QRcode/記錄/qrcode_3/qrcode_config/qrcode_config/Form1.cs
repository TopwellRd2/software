﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace qrcode_config
{
    public partial class Form1 : Form
    {
        private IniHandler iniHandler;

        private IniHandler historyIniHandler;

        private bool isSave = true;

        public Form1()
        {
            InitializeComponent();

            iniHandler = new IniHandler(Application.StartupPath + "/config.ini");
            historyIniHandler = new IniHandler(Application.StartupPath + "/history.ini");

            if (iniHandler.isExistIni() == true)
            {
                string user = iniHandler.ReadIniFile("Setting", "User", "default");

                if (user == "MASTER")
                {
                    recordBtn.Enabled = true;
                    recordBtn.Visible = true;
                }
                else
                {
                    recordBtn.Enabled = false;
                    recordBtn.Visible = false;
                }
                
                NameTxt.Text = iniHandler.ReadIniFile("Setting", "Name", "default");
                TypeTxt.Text = iniHandler.ReadIniFile("Setting", "Type_", "default");
                CaseTxt.Text = iniHandler.ReadIniFile("Setting", "Case No", "default");
                ItemTxt.Text = iniHandler.ReadIniFile("Setting", "Item No", "default");
                FormatTxt.Text = iniHandler.ReadIniFile("Setting", "Format", "default");
                WeightTxt.Text = iniHandler.ReadIniFile("Setting", "Weight", "default");
                Motor_1Txt.Text = iniHandler.ReadIniFile("Setting", "MOTOR_1", "default");
                FanTxt.Text = iniHandler.ReadIniFile("Setting", "FAN", "default");
                Motor_2Txt.Text = iniHandler.ReadIniFile("Setting", "MOTOR_2", "default");

                string isLogoStr = iniHandler.ReadIniFile("Setting", "Logo", "default");

                if (isLogoStr == "False")
                    NoLOGOradioBtn.Checked = true;
                else
                    LOGOradioBtn.Checked = true;

                string type = iniHandler.ReadIniFile("Setting", "Type", "default");

                switch (type)
                {
                    case "DC":
                        DCRadioBtn.Checked = true;
                        break;
                    case "AC":
                        ACRadioBtn.Checked = true;
                        break;
                    case "EBM":
                        EBMRadioBtn.Checked = true;
                        break;
                    default:
                        DCRadioBtn.Checked = true;
                        break;
                }
            }
            else
            {
                DCRadioBtn.Checked = true;
            }

            NameTxt.TextChanged += TextChanged;
            TypeTxt.TextChanged += TextChanged;
            CaseTxt.TextChanged += TextChanged;
            ItemTxt.TextChanged += TextChanged;
            FormatTxt.TextChanged += TextChanged;
            WeightTxt.TextChanged += TextChanged;
            FanTxt.TextChanged += TextChanged;
            Motor_1Txt.TextChanged += TextChanged;
            Motor_2Txt.TextChanged += TextChanged;

            NoLOGOradioBtn.CheckedChanged += TextChanged;
            LOGOradioBtn.CheckedChanged += TextChanged;

            DCRadioBtn.CheckedChanged += TextChanged;
            ACRadioBtn.CheckedChanged += TextChanged;
            EBMRadioBtn.CheckedChanged += TextChanged;
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (iniHandler.isExistIni() == true)
            {
                this.Text = "QRCODE設定";
                isSave = true;

                iniHandler.WriteIniFile("Setting", "Name", NameTxt.Text);
                iniHandler.WriteIniFile("Setting", "Type_", TypeTxt.Text);
                iniHandler.WriteIniFile("Setting", "Case No", CaseTxt.Text);
                iniHandler.WriteIniFile("Setting", "Item No", ItemTxt.Text);
                iniHandler.WriteIniFile("Setting", "Format", FormatTxt.Text);
                iniHandler.WriteIniFile("Setting", "Weight", WeightTxt.Text);
                iniHandler.WriteIniFile("Setting", "FAN", FanTxt.Text);
                iniHandler.WriteIniFile("Setting", "MOTOR_1", Motor_1Txt.Text);
                iniHandler.WriteIniFile("Setting", "MOTOR_2", Motor_2Txt.Text);

                if (LOGOradioBtn.Checked == true)
                    iniHandler.WriteIniFile("Setting", "Logo", "True");
                else
                    iniHandler.WriteIniFile("Setting", "Logo", "False");

                if (DCRadioBtn.Checked)
                    iniHandler.WriteIniFile("Setting", "Type", "DC");
                if (ACRadioBtn.Checked)
                    iniHandler.WriteIniFile("Setting", "Type", "AC");
                if (EBMRadioBtn.Checked)
                    iniHandler.WriteIniFile("Setting", "Type", "EBM");

                MessageBox.Show("存檔成功", "訊息");
            }
        }

        private void BringDataBtn_Click(object sender, EventArgs e)
        {
            string itemNo = ItemNoTxt.Text;

            if (historyIniHandler.isExistIni() == true)
            {
                if (historyIniHandler.ReadIniFile(itemNo, "Item No", "default") == "default")
                {
                    MessageBox.Show("無此型號資料");
                }
                else
                {
                    string isLogoStr = historyIniHandler.ReadIniFile(itemNo, "Logo", "default");

                    if (isLogoStr == "False")
                        NoLOGOradioBtn.Checked = true;
                    else
                        LOGOradioBtn.Checked = true;

                    string type = historyIniHandler.ReadIniFile(itemNo, "Type", "default");

                    switch (type)
                    {
                        case "DC":
                            DCRadioBtn.Checked = true;
                            break;
                        case "AC":
                            ACRadioBtn.Checked = true;
                            break;
                        case "EBM":
                            EBMRadioBtn.Checked = true;
                            break;
                        default:
                            DCRadioBtn.Checked = true;
                            break;
                    }


                    NameTxt.Text = historyIniHandler.ReadIniFile(itemNo, "Name", "default");
                    TypeTxt.Text = historyIniHandler.ReadIniFile(itemNo, "Type_", "default");
                    CaseTxt.Text = historyIniHandler.ReadIniFile(itemNo, "Case No", "default");
                    ItemTxt.Text = historyIniHandler.ReadIniFile(itemNo, "Item No", "default");
                    FormatTxt.Text = historyIniHandler.ReadIniFile(itemNo, "Format", "default");
                    WeightTxt.Text = historyIniHandler.ReadIniFile(itemNo, "Weight", "default");
                    FanTxt.Text = historyIniHandler.ReadIniFile(itemNo, "FAN", "default");
                    Motor_1Txt.Text = historyIniHandler.ReadIniFile(itemNo, "MOTOR_1", "default");
                    Motor_2Txt.Text = historyIniHandler.ReadIniFile(itemNo, "MOTOR_2", "default");
                }
            }
            else
            {
                MessageBox.Show("找不到history.ini", "錯誤");
            }

        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            NameTxt.Text = "";
            TypeTxt.Text = "";
            CaseTxt.Text = "";
            ItemTxt.Text = "";
            FormatTxt.Text = "";
            WeightTxt.Text = "";
            FanTxt.Text = "";
            Motor_1Txt.Text = "";
            Motor_2Txt.Text = "";
        }

        private void TextChanged(object sender, EventArgs e)
        {
            isSave = false;
            this.Text = "QRCODE設定  *未存檔";
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isSave == false)
            {
                if (MessageBox.Show("尚未存檔，確定要離開?", "警告", MessageBoxButtons.YesNo) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void recordBtn_Click(object sender, EventArgs e)
        {
            if (historyIniHandler.isExistIni() == true)
            {
                string logo = "True";
                if (NoLOGOradioBtn.Checked == true)
                    logo = "False";
                else if (LOGOradioBtn.Checked == true)
                    logo = "True";

                string type = "DC";
                if (DCRadioBtn.Checked == true)
                    type = "DC";
                else if (ACRadioBtn.Checked == true)
                    type = "AC";
                else if (EBMRadioBtn.Checked == true)
                    type = "EBM";


                historyIniHandler.WriteIniFile(ItemTxt.Text, "Type", type);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "Logo", logo);

                historyIniHandler.WriteIniFile(ItemTxt.Text, "Name", NameTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "Type_", TypeTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "Case No", CaseTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "Item No", ItemTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "Format", FormatTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "Weight", WeightTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "FAN", FanTxt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "MOTOR_1", Motor_1Txt.Text);
                historyIniHandler.WriteIniFile(ItemTxt.Text, "MOTOR_2", Motor_2Txt.Text);

                MessageBox.Show("紀錄資料成功", "訊息");
            }
            else
            {
                MessageBox.Show("找不到history.ini","錯誤");
            }
        }
    }
}
