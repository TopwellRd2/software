﻿namespace qrcode_config
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Motor_2Txt = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.Motor_1Txt = new System.Windows.Forms.TextBox();
            this.FanTxt = new System.Windows.Forms.TextBox();
            this.WeightTxt = new System.Windows.Forms.TextBox();
            this.FormatTxt = new System.Windows.Forms.TextBox();
            this.ItemTxt = new System.Windows.Forms.TextBox();
            this.CaseTxt = new System.Windows.Forms.TextBox();
            this.TypeTxt = new System.Windows.Forms.TextBox();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ACRadioBtn = new System.Windows.Forms.RadioButton();
            this.EBMRadioBtn = new System.Windows.Forms.RadioButton();
            this.DCRadioBtn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ItemNoTxt = new System.Windows.Forms.TextBox();
            this.NoLOGOradioBtn = new System.Windows.Forms.RadioButton();
            this.LOGOradioBtn = new System.Windows.Forms.RadioButton();
            this.BringDataBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.recordBtn = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Motor_2Txt
            // 
            this.Motor_2Txt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Motor_2Txt.Location = new System.Drawing.Point(142, 580);
            this.Motor_2Txt.Name = "Motor_2Txt";
            this.Motor_2Txt.Size = new System.Drawing.Size(281, 35);
            this.Motor_2Txt.TabIndex = 37;
            this.Motor_2Txt.Text = " ";
            // 
            // SaveBtn
            // 
            this.SaveBtn.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SaveBtn.Location = new System.Drawing.Point(364, 253);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(116, 108);
            this.SaveBtn.TabIndex = 36;
            this.SaveBtn.Text = "存檔";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // Motor_1Txt
            // 
            this.Motor_1Txt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Motor_1Txt.Location = new System.Drawing.Point(143, 531);
            this.Motor_1Txt.Name = "Motor_1Txt";
            this.Motor_1Txt.Size = new System.Drawing.Size(280, 35);
            this.Motor_1Txt.TabIndex = 35;
            // 
            // FanTxt
            // 
            this.FanTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FanTxt.Location = new System.Drawing.Point(144, 481);
            this.FanTxt.Name = "FanTxt";
            this.FanTxt.Size = new System.Drawing.Size(212, 35);
            this.FanTxt.TabIndex = 34;
            // 
            // WeightTxt
            // 
            this.WeightTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.WeightTxt.Location = new System.Drawing.Point(144, 436);
            this.WeightTxt.Name = "WeightTxt";
            this.WeightTxt.Size = new System.Drawing.Size(212, 35);
            this.WeightTxt.TabIndex = 33;
            // 
            // FormatTxt
            // 
            this.FormatTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormatTxt.Location = new System.Drawing.Point(144, 387);
            this.FormatTxt.Name = "FormatTxt";
            this.FormatTxt.Size = new System.Drawing.Size(212, 35);
            this.FormatTxt.TabIndex = 32;
            // 
            // ItemTxt
            // 
            this.ItemTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ItemTxt.Location = new System.Drawing.Point(145, 342);
            this.ItemTxt.Name = "ItemTxt";
            this.ItemTxt.Size = new System.Drawing.Size(211, 35);
            this.ItemTxt.TabIndex = 31;
            // 
            // CaseTxt
            // 
            this.CaseTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.CaseTxt.Location = new System.Drawing.Point(146, 295);
            this.CaseTxt.Name = "CaseTxt";
            this.CaseTxt.Size = new System.Drawing.Size(210, 35);
            this.CaseTxt.TabIndex = 30;
            // 
            // TypeTxt
            // 
            this.TypeTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TypeTxt.Location = new System.Drawing.Point(146, 246);
            this.TypeTxt.Name = "TypeTxt";
            this.TypeTxt.Size = new System.Drawing.Size(210, 35);
            this.TypeTxt.TabIndex = 29;
            // 
            // NameTxt
            // 
            this.NameTxt.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.NameTxt.Location = new System.Drawing.Point(146, 194);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(210, 35);
            this.NameTxt.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(13, 532);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 24);
            this.label11.TabIndex = 27;
            this.label11.Text = "MOTOR :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(16, 483);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 24);
            this.label10.TabIndex = 26;
            this.label10.Text = "FAN      :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(14, 437);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 24);
            this.label9.TabIndex = 25;
            this.label9.Text = "Weight   :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(15, 386);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 24);
            this.label8.TabIndex = 24;
            this.label8.Text = "Format   :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(12, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 24);
            this.label7.TabIndex = 23;
            this.label7.Text = "Item No  :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(10, 293);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 24);
            this.label6.TabIndex = 22;
            this.label6.Text = "Case No  :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(15, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 24);
            this.label5.TabIndex = 21;
            this.label5.Text = "Type      :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(15, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 24);
            this.label4.TabIndex = 20;
            this.label4.Text = "Name     :";
            // 
            // clearBtn
            // 
            this.clearBtn.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.clearBtn.Location = new System.Drawing.Point(363, 192);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(116, 47);
            this.clearBtn.TabIndex = 46;
            this.clearBtn.Text = "清空";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DCRadioBtn);
            this.groupBox2.Controls.Add(this.EBMRadioBtn);
            this.groupBox2.Controls.Add(this.ACRadioBtn);
            this.groupBox2.Location = new System.Drawing.Point(17, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 50);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " ";
            // 
            // ACRadioBtn
            // 
            this.ACRadioBtn.AutoSize = true;
            this.ACRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ACRadioBtn.Location = new System.Drawing.Point(187, 15);
            this.ACRadioBtn.Name = "ACRadioBtn";
            this.ACRadioBtn.Size = new System.Drawing.Size(57, 24);
            this.ACRadioBtn.TabIndex = 42;
            this.ACRadioBtn.Text = "AC";
            this.ACRadioBtn.UseVisualStyleBackColor = true;
            // 
            // EBMRadioBtn
            // 
            this.EBMRadioBtn.AutoSize = true;
            this.EBMRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.EBMRadioBtn.Location = new System.Drawing.Point(92, 14);
            this.EBMRadioBtn.Name = "EBMRadioBtn";
            this.EBMRadioBtn.Size = new System.Drawing.Size(71, 24);
            this.EBMRadioBtn.TabIndex = 43;
            this.EBMRadioBtn.Text = "EBM";
            this.EBMRadioBtn.UseVisualStyleBackColor = true;
            // 
            // DCRadioBtn
            // 
            this.DCRadioBtn.AutoSize = true;
            this.DCRadioBtn.Checked = true;
            this.DCRadioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.DCRadioBtn.Location = new System.Drawing.Point(11, 14);
            this.DCRadioBtn.Name = "DCRadioBtn";
            this.DCRadioBtn.Size = new System.Drawing.Size(57, 24);
            this.DCRadioBtn.TabIndex = 41;
            this.DCRadioBtn.TabStop = true;
            this.DCRadioBtn.Text = "DC";
            this.DCRadioBtn.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LOGOradioBtn);
            this.groupBox1.Controls.Add(this.NoLOGOradioBtn);
            this.groupBox1.Location = new System.Drawing.Point(17, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 54);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " ";
            // 
            // ItemNoTxt
            // 
            this.ItemNoTxt.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ItemNoTxt.Location = new System.Drawing.Point(109, 21);
            this.ItemNoTxt.Name = "ItemNoTxt";
            this.ItemNoTxt.Size = new System.Drawing.Size(181, 31);
            this.ItemNoTxt.TabIndex = 45;
            // 
            // NoLOGOradioBtn
            // 
            this.NoLOGOradioBtn.AutoSize = true;
            this.NoLOGOradioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.NoLOGOradioBtn.Location = new System.Drawing.Point(140, 17);
            this.NoLOGOradioBtn.Name = "NoLOGOradioBtn";
            this.NoLOGOradioBtn.Size = new System.Drawing.Size(103, 24);
            this.NoLOGOradioBtn.TabIndex = 39;
            this.NoLOGOradioBtn.TabStop = true;
            this.NoLOGOradioBtn.Text = "無LOGO";
            this.NoLOGOradioBtn.UseVisualStyleBackColor = true;
            // 
            // LOGOradioBtn
            // 
            this.LOGOradioBtn.AutoSize = true;
            this.LOGOradioBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LOGOradioBtn.Location = new System.Drawing.Point(11, 16);
            this.LOGOradioBtn.Name = "LOGOradioBtn";
            this.LOGOradioBtn.Size = new System.Drawing.Size(103, 24);
            this.LOGOradioBtn.TabIndex = 38;
            this.LOGOradioBtn.TabStop = true;
            this.LOGOradioBtn.Text = "有LOGO";
            this.LOGOradioBtn.UseVisualStyleBackColor = true;
            // 
            // BringDataBtn
            // 
            this.BringDataBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.BringDataBtn.Location = new System.Drawing.Point(305, 15);
            this.BringDataBtn.Name = "BringDataBtn";
            this.BringDataBtn.Size = new System.Drawing.Size(118, 39);
            this.BringDataBtn.TabIndex = 46;
            this.BringDataBtn.Text = "帶入設定";
            this.BringDataBtn.UseVisualStyleBackColor = true;
            this.BringDataBtn.Click += new System.EventHandler(this.BringDataBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 24);
            this.label1.TabIndex = 47;
            this.label1.Text = "型號 :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.BringDataBtn);
            this.groupBox3.Controls.Add(this.ItemNoTxt);
            this.groupBox3.Location = new System.Drawing.Point(16, -2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(448, 62);
            this.groupBox3.TabIndex = 48;
            this.groupBox3.TabStop = false;
            // 
            // recordBtn
            // 
            this.recordBtn.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.recordBtn.Location = new System.Drawing.Point(370, 377);
            this.recordBtn.Name = "recordBtn";
            this.recordBtn.Size = new System.Drawing.Size(102, 83);
            this.recordBtn.TabIndex = 49;
            this.recordBtn.Text = "資料紀錄";
            this.recordBtn.UseVisualStyleBackColor = true;
            this.recordBtn.Click += new System.EventHandler(this.recordBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 636);
            this.Controls.Add(this.recordBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.Motor_2Txt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Motor_1Txt);
            this.Controls.Add(this.FanTxt);
            this.Controls.Add(this.WeightTxt);
            this.Controls.Add(this.FormatTxt);
            this.Controls.Add(this.ItemTxt);
            this.Controls.Add(this.CaseTxt);
            this.Controls.Add(this.TypeTxt);
            this.Controls.Add(this.NameTxt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QRCODE設定";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox Motor_2Txt;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.TextBox Motor_1Txt;
        private System.Windows.Forms.TextBox FanTxt;
        private System.Windows.Forms.TextBox WeightTxt;
        private System.Windows.Forms.TextBox FormatTxt;
        private System.Windows.Forms.TextBox ItemTxt;
        private System.Windows.Forms.TextBox CaseTxt;
        private System.Windows.Forms.TextBox TypeTxt;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton DCRadioBtn;
        private System.Windows.Forms.RadioButton EBMRadioBtn;
        private System.Windows.Forms.RadioButton ACRadioBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton LOGOradioBtn;
        private System.Windows.Forms.RadioButton NoLOGOradioBtn;
        private System.Windows.Forms.TextBox ItemNoTxt;
        private System.Windows.Forms.Button BringDataBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button recordBtn;
    }
}

