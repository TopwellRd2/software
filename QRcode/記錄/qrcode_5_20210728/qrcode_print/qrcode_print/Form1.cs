﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using EzioDll;


namespace qrcode_print
{
    public partial class Form1 : Form
    {
        private System.Timers.Timer timer;

        GodexPrinter Printer = new GodexPrinter();

        SqlConnection sqlConnection;

        const string FILE_PATH = "C:/QRcodeImage";

        string type = "";

        string result = "";

        string boxName = "";

        int boxNameCount = 0;

        string motorName = "";

        int motorNameCount = 0;

        string controllerName = "";

        String itemNo = "";

        string appearanceStatus = "";

        string voiceStatus = "";

        int controllerNameCount = 0;

        private IniHandler iniHandler;

        private delegate void UpdateUI(String str); //宣告委派
        private delegate void CloseWin(); //宣告委派

        public Form1()
        {
            InitializeComponent();

            iniHandler = new IniHandler(Application.StartupPath + "/config.ini");
            
            string connectionString = GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("連線異常,請確認ServerName設定是否正確!" + ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            #region Get sql data to Result and BoxName and the same name(controller and motor) count

            type = iniHandler.ReadIniFile("Setting", "Type", "default");
            string UserName = iniHandler.ReadIniFile("Setting", "UserName", "default");

            string strSQL = "SELECT * from [" + type + "].[dbo].[" + type + "] " +
                            "WHERE CONVERT(VARCHAR,時間) = (select MAX([時間]) from [" + type + "].[dbo].[" + type + "] " +
                            "                                Where CONVERT(VARCHAR,日期) = (select MAX([日期]) from [" + type + "].[dbo].[" + type + "] WHERE [備註] ='" + UserName + "')" +
                            "                                  AND [備註] ='" + UserName + "' )" +
                            "AND CONVERT(VARCHAR,日期) = (select MAX([日期]) from [" + type + "].[dbo].[" + type + "] WHERE [備註] ='" + UserName + "')" +
                            "AND [備註] ='"+ UserName +"'";

            SqlCommand command = new SqlCommand(strSQL, sqlConnection);

            
            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        boxName = reader.GetString(3);
                        boxName = boxName.Trim();

                        motorName = reader.GetString(4);
                        motorName = motorName.Trim();

                        controllerName = reader.GetString(5);
                        controllerName = controllerName.Trim();

                        itemNo = reader.GetString(2);
                        itemNo = itemNo.Trim();

                        if (type == "AC")
                        {
                            appearanceStatus = reader.GetString(26);
                            voiceStatus = reader.GetString(27);

                            result = $"日期 : {reader.GetString(0)}\r\n時間 : {reader.GetString(1)}\r\n" +
                                     $"產品代碼 : {reader.GetString(2)}\r\n箱體編號 : {reader.GetString(3)}\r\n" +
                                     $"馬達編號 : {reader.GetString(4)}\r\n控制器編號 : {reader.GetString(5)}\r\n" +
                                     $"一速電壓 : {reader.GetSqlSingle(6).ToString()}\r\n一速電流 : {reader.GetSqlSingle(7).ToString()}\r\n" +
                                     $"一速功率 : {reader.GetSqlSingle(8).ToString()}\r\n一速保護電流 : {reader.GetSqlSingle(9).ToString()}\r\n" +
                                     $"一速震動 : {reader.GetSqlSingle(10).ToString()}\r\n二速電壓 : {reader.GetSqlSingle(11).ToString()}\r\n" +
                                     $"二速電流 : {reader.GetSqlSingle(12).ToString()}\r\n二速功率 : {reader.GetSqlSingle(13).ToString()}\r\n" +
                                     $"二速保護電流 : {reader.GetSqlSingle(14).ToString()}\r\n二速震動 : {reader.GetSqlSingle(15).ToString()}\r\n" +
                                     $"三速電壓 : {reader.GetSqlSingle(16).ToString()}\r\n三速電流 : {reader.GetSqlSingle(17).ToString()}\r\n" +
                                     $"三速功率 : {reader.GetSqlSingle(18).ToString()}\r\n三速保護電流 : {reader.GetSqlSingle(19).ToString()}\r\n" +
                                     $"三速震動 : {reader.GetSqlSingle(20).ToString()}\r\n四速電壓 : {reader.GetSqlSingle(21).ToString()}\r\n" +
                                     $"四速電流 : {reader.GetSqlSingle(22).ToString()}\r\n四速功率 : {reader.GetSqlSingle(23).ToString()}\r\n" +
                                     $"四速保護電流 : {reader.GetSqlSingle(24).ToString()}\r\n四速震動 : {reader.GetSqlSingle(25).ToString()}\r\n" +
                                     $"外觀檢測 : {reader.GetString(26)}\r\n運轉異音 : {reader.GetString(27)}";
                        }
                        else
                        {
                            appearanceStatus = reader.GetString(17);
                            voiceStatus = reader.GetString(18);

                            result = $"日期 : {reader.GetString(0)}\r\n時間: {reader.GetString(1)}\r\n" +
                                     $"產品代碼 : {reader.GetString(2)}\r\n箱體編號 : {reader.GetString(3)}\r\n" +
                                     $"馬達編號 : {reader.GetString(4)}\r\n控制器編號 : {reader.GetString(5)}\r\n" +
                                     $"出廠電壓 : {reader.GetSqlSingle(6).ToString()}\r\n出廠電流 : {reader.GetSqlSingle(7).ToString()}\r\n" +
                                     $"出廠功率 : {reader.GetSqlSingle(8).ToString()}\r\n出廠震動 : {reader.GetSqlSingle(9).ToString()}\r\n" +
                                     $"出廠轉速 : {reader.GetSqlSingle(10).ToString()}\r\n出廠頻率 : {reader.GetSqlSingle(11).ToString()}\r\n" +
                                     $"外觀檢測 : {reader.GetString(17)}\r\n" +
                                     $"運轉異音 : {reader.GetString(18)}\r\n使用者 : {reader.GetString(19)}\r\n" +
                                     $"功率因素 : {reader.GetSqlSingle(20).ToString()}";

                        }
                        //MessageBox.Show(result);
                    }
                } else {
                    MessageBox.Show("找不到資料,請確認UserName是否與圖控設定一致!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Load += (sender, e) => { this.Close(); };
                    return;
                }
            }

            strSQL = "select COUNT([箱體編號]) from [" + type + "].[dbo].[" + type + "] " +
                     "WHERE [箱體編號] = '" + boxName + "'" +
                     "  AND [產品代碼] = '" + itemNo + "'";   //箱體號碼因為由案號編碼所以有可能重複,所以多加品號區分重複

            command = new SqlCommand(strSQL, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        boxNameCount = reader.GetInt32(0);
                    }
                }
            }

            strSQL = " select COUNT([馬達編號]) from [" +
                           type +
                           "].[dbo].[" +
                           type +
                           "] " +
                           "WHERE [馬達編號] = '" +
                           motorName +"'";

            command = new SqlCommand(strSQL, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        motorNameCount = reader.GetInt32(0);
                    }
                }
            }
            
            strSQL = " select COUNT([控制器編號]) from [" +
               type +
               "].[dbo].[" +
               type +
               "] " +
               "WHERE [控制器編號] = '" +
               controllerName + "'";

            command = new SqlCommand(strSQL, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        controllerNameCount = reader.GetInt32(0);
                    }
                }
            }
            #endregion

            timer = new System.Timers.Timer();
            timer.Interval = 500;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Tick);

            if (!appearanceStatus.Contains("OK"))
            {
                MessageBox.Show("外觀檢測不是OK !!!", "錯誤");
                this.Load += (sender, e) => { this.Close(); };
                return;
            }

            if (!voiceStatus.Contains("OK"))
            {
                MessageBox.Show("運轉異音不是OK !!!", "錯誤");
                this.Load += (sender, e) => { this.Close(); };
                return;
            }

            if (controllerNameCount > 1 || motorNameCount > 1 || boxNameCount > 1)
            {
                if (MessageBox.Show("箱體編號、控制器編號、馬達編號有重複，確定要列印QRCODE ? \r\n ( 相同箱體編號總數量: " + boxNameCount + "   相同控制器編號總數量 : " + controllerNameCount + "   相同馬達編號總數量 : " + motorNameCount  + " )",
                    "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,MessageBoxOptions.DefaultDesktopOnly) == DialogResult.Yes)
                {
                    timer.Start();
                }
                else
                {
                    this.Load += (sender, e) => { this.Close(); };
                }
            }
            else
            {
                timer.Start();
            }
        }
        
        private string GetConnectionString()
        {
            //return "Data Source=(local);"
            //      + "Integrated Security=SSPI;";
            //return "Data Source = 192.168.0.181;"
            //      + "User = sa; Password = 12;";
            string ServerName = iniHandler.ReadIniFile("Setting", "ServerName", "");
            if (string.IsNullOrEmpty(ServerName))
            {
                ServerName = "DESKTOP-8OH9HM4\\SQLEXPRESS";  //產線電腦資料庫 192.168.3.99
            }
            return "Data Source="+ ServerName + ";"
                   + "User = sa; Password = 12;";
        }

        //------------------------------------------------------------------------
        // Label Setup
        //------------------------------------------------------------------------
        private void LabelSetup()
        {
            Printer.Config.LabelMode((PaperMode)0, 40, 12);
            Printer.Config.LabelWidth(60);
            Printer.Config.Dark(5);
            Printer.Config.Speed(2);
            Printer.Config.PageNo(1);
            Printer.Config.CopyNo(1);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;

            if (!File.Exists(FILE_PATH))
            {
                Directory.CreateDirectory(FILE_PATH);
            }

            if (File.Exists(FILE_PATH + "/" + boxName + ".bmp"))
            {
                //MessageBox.Show("檔案已存在");
            }

            bool isLogo = false;

            string Logo = iniHandler.ReadIniFile("Setting", "Logo", "default");

            if (Logo == "True")
                isLogo = true;
            else
                isLogo = false;

            if (QrcodeGenerator.createQRCode2IMG(result, FILE_PATH, boxName, isLogo, type))
            {
                Printer.Open(PortType.USB);
                LabelSetup();

                Printer.Command.Start();

                string NameTxt    = iniHandler.ReadIniFile("Setting", "Name",    "default");
                string TypeTxt    = iniHandler.ReadIniFile("Setting", "Type_",   "default");
                string CaseTxt    = iniHandler.ReadIniFile("Setting", "Case No", "default");
                string ItemTxt    = iniHandler.ReadIniFile("Setting", "Item No", "default");
                string FormatTxt  = iniHandler.ReadIniFile("Setting", "Format",  "default");
                string WeightTxt  = iniHandler.ReadIniFile("Setting", "Weight",  "default");
                string FanTxt     = iniHandler.ReadIniFile("Setting", "FAN",     "default");
                string Motor_1Txt = iniHandler.ReadIniFile("Setting", "MOTOR_1", "default");
                string Motor_2Txt = iniHandler.ReadIniFile("Setting", "MOTOR_2", "default");

                int pos_x = 7;
                int pos_y = 65;
                //int pos_y = isLogo ? 65 : 33;
                int font_height = 22;
                int font_width = 10;
                int space = font_height + 3;
                //int space = isLogo ? font_height + 3 : font_height + 8;
                
                Printer.Command.PrintText(pos_x, pos_y, font_height, "", $"Name      : {NameTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space + 1, font_height, "", $"Type        : {TypeTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Case NO : {CaseTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Item NO  : {ItemTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Format    : {FormatTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Weight    : {WeightTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"FAN        : {FanTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"MOTOR  : {Motor_1Txt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);
                Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"                {Motor_2Txt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                //箱體標號  改由QrcodeGenerator製作 
                //Printer.Command.PrintText(pos_x, pos_y += (space + 14), 22, "", $"{boxName}", 10, FontWeight.FW_700_BOLD, RotateMode.Angle_0); 

                int printResult = Printer.Command.PrintHalftoneImage(0, 0, FILE_PATH + "/" + boxName + ".bmp", 0, HalftoneMode.None);

                Printer.Command.End();
                Printer.Close();

                if (printResult == 0)
                {
                    updateText("列印失敗");
                }
                else if (printResult == 1)
                {
                    updateText("列印成功");
                }
                else
                    MessageBox.Show("錯誤代碼 :" + printResult.ToString());

                System.IO.FileInfo fi = new System.IO.FileInfo(FILE_PATH + "/" + boxName + ".bmp");
                try
                {
                    fi.Delete();
                }
                catch (System.IO.IOException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("QR CODE 圖檔產生錯誤");
            }

            Thread.Sleep(1000);
            closeForm();
        }

        private void closeForm()
        {
            if (this.InvokeRequired)
            {
                CloseWin uu = new CloseWin(closeForm);
                this.Invoke(uu);
            }
            else
            {
                this.Close();
            }
        }

        private void updateText(String str)
        {      
            if (this.InvokeRequired)
            {
                UpdateUI uu = new UpdateUI(updateText);
                this.Invoke(uu, str);
            }
            else
            {
                InfoLabel.Text = str;
            }
        }
    }
}
