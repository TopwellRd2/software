USE [DC]
GO

/****** Object:  Trigger [dbo].[TRG_BEFORE_INS]    Script Date: 2021/10/07 17:20:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRG_BEFORE_INS] ON [dbo].[DC]
AFTER INSERT
AS 
   DECLARE @日期 nchar(10)
   DECLARE @時間 nchar(10)
   DECLARE @箱體編號 nchar(30)
   DECLARE @馬達編號 nchar(30)
   DECLARE @控制器編號 nchar(30)
   DECLARE @出廠震動 real
   DECLARE @最高震動 real

BEGIN
   SET NOCOUNT ON; 
    
	SELECT @日期=日期,@時間=時間,@箱體編號=箱體編號,@馬達編號=馬達編號,@控制器編號=控制器編號,@出廠震動=出廠震動,@最高震動=最高震動 FROM inserted;

	IF(@出廠震動 ='0') 
	   update dbo.DC set 出廠震動=1 where 日期=@日期 and 時間=@時間 and 箱體編號=@箱體編號 and 馬達編號=@馬達編號 and 控制器編號 =@控制器編號

	IF(@最高震動 ='0') 
	   update dbo.DC set 最高震動=1 where 日期=@日期 and 時間=@時間 and 箱體編號=@箱體編號 and 馬達編號=@馬達編號 and 控制器編號 =@控制器編號

END
GO

ALTER TABLE [dbo].[DC] ENABLE TRIGGER [TRG_BEFORE_INS]
GO

