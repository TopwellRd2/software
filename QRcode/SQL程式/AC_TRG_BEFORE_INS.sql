USE [AC]
GO

/****** Object:  Trigger [dbo].[TRG_BEFORE_INS]    Script Date: 2021/10/07 17:20:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRG_BEFORE_INS] ON [dbo].[AC]
AFTER INSERT
AS 
DECLARE @日期 nchar(10)
   DECLARE @時間 nchar(10)
   DECLARE @箱體編號 nchar(30)
   DECLARE @馬達編號 nchar(30)
   DECLARE @控制器編號 nchar(30)
   DECLARE @一速震動 real
   DECLARE @二速震動 real
   DECLARE @三速震動 real
   DECLARE @四速震動 real

BEGIN
    SET NOCOUNT ON;
    
	SELECT @日期=日期,@時間=時間,@箱體編號=箱體編號,@馬達編號=馬達編號,@控制器編號=控制器編號,@一速震動=一速震動,@二速震動=二速震動,@三速震動=三速震動,@四速震動=四速震動 FROM inserted;

	IF(@一速震動 ='0') 
	   update dbo.AC set 一速震動=1 where 日期=@日期 and 時間=@時間 and 箱體編號=@箱體編號 and 馬達編號=@馬達編號 and 控制器編號 =@控制器編號

	IF(@二速震動 ='0') 
	   update dbo.AC set 二速震動=1 where 日期=@日期 and 時間=@時間 and 箱體編號=@箱體編號 and 馬達編號=@馬達編號 and 控制器編號 =@控制器編號

	IF(@三速震動 ='0') 
	   update dbo.AC set 三速震動=1 where 日期=@日期 and 時間=@時間 and 箱體編號=@箱體編號 and 馬達編號=@馬達編號 and 控制器編號 =@控制器編號

	IF(@四速震動 ='0') 
	   update dbo.AC set 四速震動=1 where 日期=@日期 and 時間=@時間 and 箱體編號=@箱體編號 and 馬達編號=@馬達編號 and 控制器編號 =@控制器編號
END
GO

ALTER TABLE [dbo].[AC] ENABLE TRIGGER [TRG_BEFORE_INS]
GO

