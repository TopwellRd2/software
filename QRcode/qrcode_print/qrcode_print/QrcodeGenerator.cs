﻿using System;
using System.Windows.Forms;
using ZXing;
using System.Drawing;
using System.Drawing.Imaging;

namespace qrcode_print
{
    /// <summary>
    /// 
    /// </summary>
    class QrcodeGenerator
    {
        public static bool createQRCode2IMG(string msg, string savePath, string fileName, bool isLogo, string type, int logo_type)
        {
            try {
                int QR_Height = 0;
                int QR_Width = 0;
                if (type == "UL" || type == "AC")
                {
                    QR_Height = 270;
                    QR_Width = 270;
                }
                else
                {
                    QR_Height = 240;
                    QR_Width = 240;
                }
                BarcodeWriter bw = new BarcodeWriter()
                 {
                    Format = BarcodeFormat.QR_CODE,
                    Options = new ZXing.QrCode.QrCodeEncodingOptions
                    {
                        ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.L,
                        Height = QR_Height,
                        Width = QR_Width,
                        //Margin = 0,
                        //QrVersion = 40,
                        CharacterSet = "UTF-8"  // 少了這一行中文就亂碼了
                    }
                };
                
                Bitmap bitmap = bw.Write(msg);

                //===================計算邊長=========================
                int qrcode_length = 0;
                Point first_point = new Point();
                #region 計算邊長
               
                for (int h = 0; h < bitmap.Height; ++h) {
                    bool isFind = false;
                    for (int w = 0; w < bitmap.Width; ++w) {
                        Color a = bitmap.GetPixel(w, h);
                        if (bitmap.GetPixel(w, h).ToArgb().Equals(Color.Black.ToArgb()) )
                        {
                            first_point.X = w;
                            first_point.Y = h;
                            isFind = true;
                            break;
                        }
                    }
                    if (isFind)
                        break;
                }

                for (int w = bitmap.Width - 1; w > 0; w--) {
                    if (bitmap.GetPixel(w, first_point.Y).ToArgb().Equals(Color.Black.ToArgb())) {
                        qrcode_length = w - first_point.X;
                        break;
                    }
                }
                #endregion   
                //=====================================================

                Bitmap ret;
                Graphics graphics;

                if (type == "UL")
                {
                    ret = new Bitmap(Properties.Resources.ULtemplate);
                    graphics = Graphics.FromImage(ret);
                    int empty_length = (bitmap.Width - qrcode_length) / 2;
                    graphics.DrawImage(bitmap, new Point(ret.Width - bitmap.Width + empty_length - 30, (ret.Height - bitmap.Height ) / 2 + 30));
                } else 
                {
                    ret = new Bitmap(Properties.Resources.template);
                    graphics = Graphics.FromImage(ret);
                    int empty_length = (bitmap.Width - qrcode_length) / 2;
                    graphics.DrawImage(bitmap, new Point(ret.Width - bitmap.Width + empty_length - 10, (ret.Height - bitmap.Height) / 2 + 15));
                }

                //bitmap.Save(savePath + "/" + fileName + "qr.png");


                //graphics.DrawImage(Properties.Resources.frame, new Point(ret.Width - 200 - 15, (ret.Height-200)/2));


                //first_point.X += 5;
                //first_point.Y += 5;
                //Point strintPoint = new Point(first_point.X, first_point.Y);
                //strintPoint.Y += qrcode_length + 5;
                //strintPoint.X += (int)( (qrcode_length - (fileName.Length * 13) ) / 2);

                int x = 0;
                for (int i = 0; i < ret.Height; i++)
                {
                    bool isFound = false;
                    for (int j = 0; j < ret.Width; j++)
                    {
                        if (ret.GetPixel(j, i).ToArgb().Equals(Color.Black.ToArgb() ) )
                        {
                            x = j;
                            isFound = true;
                            break;
                        }
                    }
                    if (isFound)
                        break;
                }
                
                if (type == "AC")
                {
                    graphics.DrawString(fileName, new Font("Arial", 15, FontStyle.Bold), Brushes.Black, new Point(x + 8, (ret.Height - 200) / 2 + 233));
                }
                else if (type == "UL")
                {
                    graphics.DrawString("Mfg No." + fileName, new Font("Arial", 16, FontStyle.Bold), Brushes.Black, new Point(x + 5, (ret.Height - 200) / 2 + 262));
                    graphics.DrawString("AIR FILTER", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(x + 120, 30));
                    graphics.DrawString("E524409", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(x + 120, 60));
                    graphics.DrawImage(Properties.Resources.UL_logo, new Point(x + 25, 15));
                }
                else
                {
                    graphics.DrawString(fileName, new Font("Arial", 15, FontStyle.Bold), Brushes.Black, new Point(x + 8, (ret.Height - 200) / 2 + 230));
                }


                if (isLogo)
                {
                    if (type == "UL")
                    {
                        graphics.DrawImage(Properties.Resources.topwell_full_logo, new Point(14, 35));
                    }
                    else
                    {
                        if (logo_type == 1) //topwell 原logo
                        {
                            graphics.DrawImage(Properties.Resources.topwell_logo, new Point(68, 3));
                        }
                        else if (logo_type == 2) //含普利恩logo
                        {
                            graphics.DrawImage(Properties.Resources.puriumfil_logo, new Point(85, -3));
                        }
                    }
                }

                //graphics.DrawString("Name        112121 121212212345678987654321\r\n2222222\r\n2222\r\n2222\r\n2222\r\n7222\r\n6222\r\n5222\r\n4222\r\n3222\r\n7222\r\n8222\r\n9222", new Font("Arial", 16, FontStyle.Bold), Brushes.Black, new Point(0,0));
                //graphics.DrawString("Name    ", new Font("", 18), Brushes.Black, new Point(0, 10));


                //Bitmap qcok = new Bitmap("D:/workspace/qrcode_generator/qrcode_generator/QCOK.png");
                //Point qcokPoint = new Point(first_point.X, first_point.Y);
                //qcokPoint.X += (qrcode_length - qcok.Width) / 2 + 5;
                //qcokPoint.Y += (qrcode_length - qcok.Height) / 2 + 5;

                //graphics.DrawImage(qcok,  qcokPoint);

                //for (int i = 0; i < ret.Height; i++)
                //{
                //    for (int j = 0; j < ret.Width; j++)
                //    {
                //        //ret.GetPixel(j, i).ToArgb().Equals(Color.White.ToArgb())
                //        if (ret.GetPixel(j, i).R*3 + ret.GetPixel(j, i).G*0.59 +ret.GetPixel(j, i).B * 0.11 > 150) 
                //        {
                //            ret.SetPixel(j, i, Color.White);
                //        }
                //        else
                //        {
                //            ret.SetPixel(j, i, Color.Black);
                //        }
                //    }

                //}

                ret.Save(savePath + "/" + fileName + ".bmp", ImageFormat.Bmp);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.StackTrace);
                return false;
            }
            return true;
        }
    }
}
