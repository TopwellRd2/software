﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using EzioDll;


namespace qrcode_print
{
    public partial class Form1 : Form
    {
        private System.Timers.Timer timer;

        GodexPrinter Printer = new GodexPrinter();

        DateTime dt = DateTime.Now;

        SqlConnection sqlConnection;

        const string FILE_PATH = "C:/QRcodeImage";

        String type = "";  //資料庫名稱,UL存在DC資料庫裡

        String type1 = ""; //實際種類,包含UL

        int logo_type = 0; // 1:topwell 原logo  2:含普利恩logo

        String result = "";

        String boxName = "";

        int boxNameCount = 0;

        String motorName = "";

        int motorNameCount = 0;

        String controllerName = "";

        String itemNo = "";

        String appearanceStatus = "";

        String voiceStatus = "";

        int controllerNameCount = 0;

        string UserName = "";

        //UL
        string size = "";

        private IniHandler iniHandler;

        private delegate void UpdateUI(String str); //宣告委派
        private delegate void CloseWin(); //宣告委派

        public Form1()
        {
            InitializeComponent();

            iniHandler = new IniHandler(Application.StartupPath + "/config.ini");
            
            string connectionString = GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("連線異常,請確認ServerName設定是否正確!" + ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            #region Get sql data to Result and BoxName and the same name(controller and motor) count

            type = iniHandler.ReadIniFile("Setting", "Type", "");
            if (type == "UL")  //UL跟DC存在同一個資料庫
            {
                type = "DC";
                type1 = "UL";
            }
            else
            {
                type1 = type;
            }
            size = iniHandler.ReadIniFile("Setting", "Format", "");
            UserName = iniHandler.ReadIniFile("Setting", "UserName", "");

            string strSQL = "SELECT * FROM [" + type + "].[dbo].[" + type + "] " +
                            "WHERE CONVERT(datetime,[時間],8) = (SELECT MAX(CONVERT(datetime,[時間],8)) FROM [" + type + "].[dbo].[" + type + "] " +
                            "                                    WHERE CONVERT(datetime,[日期],111) = (SELECT MAX(CONVERT(datetime,[日期],111)) FROM [" + type + "].[dbo].[" + type + "] WHERE [備註] ='" + UserName + "')" +
                            "                                  AND [備註] ='" + UserName + "' )" +
                            "AND CONVERT(datetime,[日期],111) = (SELECT MAX(CONVERT(datetime,[日期],111)) FROM [" + type + "].[dbo].[" + type + "] WHERE [備註] ='" + UserName + "')" +
                            "AND [備註] ='" + UserName + "'";

            SqlCommand command = new SqlCommand(strSQL, sqlConnection);

            
            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        boxName = reader.GetString(3);
                        boxName = boxName.Trim();

                        motorName = reader.GetString(4);
                        motorName = motorName.Trim();

                        controllerName = reader.GetString(5);
                        controllerName = controllerName.Trim();

                        itemNo = reader.GetString(2);
                        itemNo = itemNo.Trim();

                        if (type1 == "AC")
                        {
                            appearanceStatus = reader["外觀檢測"].ToString();
                            voiceStatus = reader["運轉異音"].ToString();

                            result = $"日期 Date : {reader["日期"].ToString()}\r\n" +
                                     $"時間 Time : {reader["時間"].ToString()}\r\n" +
                                     $"產品代碼 Product No. : {reader["產品代碼"].ToString()}\r\n" +
                                     $"箱體編號 Cabinet No. : {reader["箱體編號"].ToString()}\r\n" +
                                     $"馬達編號 Motor No. : {reader["馬達編號"].ToString()}\r\n" +
                                     $"控制器編號 Controller No. :  {reader["控制器編號"].ToString()}\r\n" +
                                     $"一速電壓 First speed voltage (V) : {reader["一速電壓"].ToString()}\r\n" +
                                     $"一速電流 First speed current (A) : {reader["一速電流"].ToString()}\r\n" +
                                     $"一速功率 First speed power (W) : {reader["一速功率"].ToString()}\r\n" +
                                     $"一速保護電流 First speed current protection (A) : {reader["一速保護電流"].ToString()}\r\n" +
                                     $"一速震動 First speed vibration (μm) : {reader["一速震動"].ToString()}\r\n" +
                                     $"二速電壓 Second speed voltage (V) : {reader["二速電壓"].ToString()}\r\n" +
                                     $"二速電流 Second speed current (A) : {reader["二速電流"].ToString()}\r\n" +
                                     $"二速功率 Second speed power (W) : {reader["二速功率"].ToString()}\r\n" +
                                     $"二速保護電流 Second speed current protection (A) : {reader["二速保護電流"].ToString()}\r\n" +
                                     $"二速震動 Second speed vibration (μm) : {reader["二速震動"].ToString()}\r\n" +
                                     $"三速電壓 Third speed voltage (V) : {reader["三速電壓"].ToString()}\r\n" +
                                     $"三速電流 Third speed current (A) : {reader["三速電流"].ToString()}\r\n" +
                                     $"三速功率 Third speed power (W) : {reader["三速功率"].ToString()}\r\n" +
                                     $"三速保護電流 Third speed current protection (A) : {reader["三速保護電流"].ToString()}\r\n" +
                                     $"三速震動 Third speed vibration (μm) : {reader["三速震動"].ToString()}\r\n" +
                                     $"四速電壓 Fourth speed voltage (V) : {reader["四速電壓"].ToString()}\r\n" +
                                     $"四速電流 Fourth speed current (A) : {reader["四速電流"].ToString()}\r\n" +
                                     $"四速功率 Fourth speed power (W) : {reader["四速功率"].ToString()}\r\n" +
                                     $"四速保護電流 Fourth speed current protection (A) : {reader["四速保護電流"].ToString()}\r\n" +
                                     $"四速震動 Fourth speed vibration (μm) : {reader["四速震動"].ToString()}\r\n" +
                                     $"外觀檢測 Visual inspection : {reader["外觀檢測"].ToString()}\r\n" +
                                     $"運轉異音 Running noise :{reader["運轉異音"].ToString()}";

                                    //$"日期 Date : {reader.GetString(0)}\r\n" +
                                    //$"時間 Time : {reader.GetString(1)}\r\n" +
                                    //$"產品代碼 Product No. : {reader.GetString(2)}\r\n" +
                                    //$"箱體編號 Cabinet No. : {reader.GetString(3)}\r\n" +
                                    //$"馬達編號 Motor No. : {reader.GetString(4)}\r\n" +
                                    //$"控制器編號 Controller No. :  {reader.GetString(5)}\r\n" +
                                    //$"一速電壓 First speed voltage (V) : {reader.GetSqlSingle(6).ToString()}\r\n" +
                                    //$"一速電流 First speed current (A) : {reader.GetSqlSingle(7).ToString()}\r\n" +
                                    //$"一速功率 First speed power (W) : {reader.GetSqlSingle(8).ToString()}\r\n" +
                                    //$"一速保護電流 First speed current protection (A) : {reader.GetSqlSingle(9).ToString()}\r\n" +
                                    //$"一速震動 First speed vibration (μm) : {reader.GetSqlSingle(10).ToString()}\r\n" +
                                    //$"二速電壓 Second speed voltage (V) : {reader.GetSqlSingle(11).ToString()}\r\n" +
                                    //$"二速電流 Second speed current (A) : {reader.GetSqlSingle(12).ToString()}\r\n" +
                                    //$"二速功率 Second speed power (W) : {reader.GetSqlSingle(13).ToString()}\r\n" +
                                    //$"二速保護電流 Second speed current protection (A) : {reader.GetSqlSingle(14).ToString()}\r\n" +
                                    //$"二速震動 Second speed vibration (μm) : {reader.GetSqlSingle(15).ToString()}\r\n" +
                                    //$"三速電壓 Third speed voltage (V) : {reader.GetSqlSingle(16).ToString()}\r\n" +
                                    //$"三速電流 Third speed current (A) : {reader.GetSqlSingle(17).ToString()}\r\n" +
                                    //$"三速功率 Third speed power (W) : {reader.GetSqlSingle(18).ToString()}\r\n" +
                                    //$"三速保護電流 Third speed current protection (A) : {reader.GetSqlSingle(19).ToString()}\r\n" +
                                    //$"三速震動 Third speed vibration (μm) : {reader.GetSqlSingle(20).ToString()}\r\n" +
                                    //$"四速電壓 Fourth speed voltage (V) : {reader.GetSqlSingle(21).ToString()}\r\n" +
                                    //$"四速電流 Fourth speed current (A) : {reader.GetSqlSingle(22).ToString()}\r\n" +
                                    //$"四速功率 Fourth speed power (W) : {reader.GetSqlSingle(23).ToString()}\r\n" +
                                    //$"四速保護電流 Fourth speed current protection (A) : {reader.GetSqlSingle(24).ToString()}\r\n" +
                                    //$"四速震動 Fourth speed vibration (μm) : {reader.GetSqlSingle(25).ToString()}\r\n" +
                                    //$"外觀檢測 Visual inspection : {reader.GetString(26)}\r\n" +
                                    //$"運轉異音 Running noise :{reader.GetString(27)}";
                        }
                        else if (type1 == "UL")
                        {
                            appearanceStatus = reader["外觀檢測"].ToString();
                            voiceStatus = reader["運轉異音"].ToString();

                            result = $"日期 Date : {reader["日期"].ToString()}\r\n" +
                                     $"時間 Time : {reader["時間"].ToString()}\r\n" +
                                     $"產品型號 Product model : {reader["產品代碼"].ToString()}\r\n" +
                                     $"產品尺寸 Product size (mm) :{size}\r\n" +
                                     $"生產編號 Product No. : {reader["箱體編號"].ToString()}\r\n" +
                                     $"馬達編號 Motor No. : {reader["馬達編號"].ToString()}\r\n" +
                                     $"控制器編號 Controller No. : {reader["控制器編號"].ToString()}\r\n" +
                                     $"測試轉速 1 Test speed 1 (rpm) : {reader["最低轉速"].ToString()}\r\n" +
                                     $"測試電壓 1 Test voltage 1 (V) : {reader["最低電壓"].ToString()}\r\n" +
                                     $"測試頻率 1 Test frequency 1 (Hz) : {reader["最低頻率"].ToString()}\r\n" +
                                     $"輸入功率 1 Input power 1 (W)  : {reader["最低功率"].ToString()}\r\n" +
                                     $"輸入電流 1 Input current 1 (A) : {reader["最低電流"].ToString()}\r\n" +
                                     $"功率因數 1 Power factor 1 (PF) : {reader["最低功率因素"].ToString()}\r\n" +
                                     $"震動測試 1 Vibration test 1 (μm) : {reader["最低震動"].ToString()}\r\n" +
                                     $"測試轉速 2 Test speed 2 (rpm) : {reader["最高轉速"].ToString()}\r\n" +
                                     $"測試電壓 2 Test voltage 2 (V) : {reader["最高電壓"].ToString()}\r\n" +
                                     $"測試頻率 2 Test frequency 2 (Hz) : {reader["最高頻率"].ToString()}\r\n" +
                                     $"輸入功率 2 Input power 2 (W) : {reader["最高功率"].ToString()}\r\n" +
                                     $"輸入電流 2 Input current 2 (A) : {reader["最高電流"].ToString()}\r\n" +
                                     $"功率因數 2 Power factor 2 (PF) : {reader["最高功率因素"].ToString()}\r\n" +
                                     $"震動測試 2 Vibration test 2 (μm) : {reader["最高震動"].ToString()}\r\n" +
                                     $"耐壓測試 Pressure test (2700V) : {reader["耐壓測試"].ToString()}\r\n" +
                                     $"外觀測試 Visual inspection : {reader["外觀檢測"].ToString()}\r\n" +
                                     $"運轉異音 Running noise : {reader["運轉異音"].ToString()}\r\n" +
                                     $"測試人員 Tester : {reader["使用者"].ToString()}";

                                    // $"日期 Date : {reader.GetString(0)}\r\n" +
                                    // $"時間 Time : {reader.GetString(1)}\r\n" +
                                    // $"產品型號 Product model : {reader.GetString(2)}\r\n" +
                                    // $"產品尺寸 Product size (mm) :{size}\r\n" +
                                    // $"生產編號 Product No. : {reader.GetString(3)}\r\n" +
                                    // $"馬達編號 Motor No. : {reader.GetString(4)}\r\n" +
                                    // $"控制器編號 Controller No. : {reader.GetString(5)}\r\n" +
                                    // $"測試轉速 1 Test speed 1 (rpm) : {reader.GetSqlSingle(30).ToString()}\r\n" +
                                    // $"測試電壓 1 Test voltage 1 (V) : {reader.GetSqlSingle(26).ToString()}\r\n" +
                                    // $"測試頻率 1 Test frequency 1 (Hz) : {reader.GetSqlSingle(50).ToString()}\r\n" +
                                    // $"輸入功率 1 Input power 1 (W)  : {reader.GetSqlSingle(28).ToString()}\r\n" +
                                    // $"輸入電流 1 Input current 1 (A) : {reader.GetSqlSingle(27).ToString()}\r\n" +
                                    // $"功率因數 1 Power factor 1 (PF) : {reader.GetSqlSingle(49).ToString()}\r\n" +
                                    // $"震動測試 1 Vibration test 1 (μm) : {reader.GetSqlSingle(29).ToString()}\r\n" +
                                    // $"測試轉速 2 Test speed 2 (rpm) : {reader.GetSqlSingle(16).ToString()}\r\n" +
                                    // $"測試電壓 2 Test voltage 2 (V) : {reader.GetSqlSingle(12).ToString()}\r\n" +
                                    // $"測試頻率 2 Test frequency 2 (Hz) : {reader.GetSqlSingle(51).ToString()}\r\n" +
                                    // $"輸入功率 2 Input power 2 (W) : {reader.GetSqlSingle(14).ToString()}\r\n" +
                                    // $"輸入電流 2 Input current 2 (A) : {reader.GetSqlSingle(13).ToString()}\r\n" +
                                    // $"功率因數 2 Power factor 2 (PF) : {reader.GetSqlSingle(52).ToString()}\r\n" +
                                    // $"震動測試 2 Vibration test 2 (μm) : {reader.GetSqlSingle(15).ToString()}\r\n" +
                                    // $"耐壓測試 Pressure test (2700V) : {reader.GetString(24)}\r\n" +
                                    // $"外觀測試 Visual inspection : {reader.GetString(17)}\r\n" +
                                    // $"運轉異音 Running noise : {reader.GetString(18)}\r\n" +
                                    // $"測試人員 Tester : {reader.GetString(19)}";
                                    ////因字串量過大刪除,否則QRCODE需到一定大小,會無法放入貼紙中
                                    ////$"測試轉速 3 Test speed 3 (rpm) : {reader.GetSqlSingle(10).ToString()}\r\n" + 
                                    ////$"測試電壓 3 Test voltage 3 (V) : {reader.GetSqlSingle(6).ToString()}\r\n" +
                                    ////$"測試頻率 3 Test frequency 3 (Hz) : {reader.GetSqlSingle(11).ToString()}\r\n" +
                                    ////$"輸入功率 3 Input power 3 (W) : {reader.GetSqlSingle(8).ToString()}\r\n" +
                                    ////$"輸入電流 3 Input current 3 (A) : {reader.GetSqlSingle(7).ToString()}\r\n" +
                                    ////$"功率因數 3 Power factor 3 (PF) : {reader.GetSqlSingle(20).ToString()}\r\n" +
                                    ////$"震動測試 3 Vibration test 3 (μm) : {reader.GetSqlSingle(9).ToString()}\r\n" +
                        }
                        else
                        {
                            appearanceStatus = reader["外觀檢測"].ToString();
                            voiceStatus = reader["運轉異音"].ToString();

                            result =  $"日期 Date : {reader["日期"].ToString()}\r\n" +
                                      $"時間 Time : {reader["時間"].ToString()}\r\n" +
                                      $"產品代碼 Product No. : {reader["產品代碼"].ToString()}\r\n" +
                                      $"箱體編號 Cabinet No. : {reader["箱體編號"].ToString()}\r\n" +
                                      $"馬達編號 Motor No. : {reader["馬達編號"].ToString()}\r\n" +
                                      $"控制器編號 Controller No. : {reader["控制器編號"].ToString()}\r\n" +
                                      $"測試轉速 1 Test speed 1 (rpm) : {reader["最低轉速"].ToString()}\r\n" +
                                      $"測試電壓 1 Test voltage 1 (V) : {reader["最低電壓"].ToString()}\r\n" +
                                      $"測試頻率 1 Test frequency 1 (Hz) : {reader["最低頻率"].ToString()}\r\n" +
                                      $"輸入功率 1 Input power 1 (W)  : {reader["最低功率"].ToString()}\r\n" +
                                      $"輸入電流 1 Input current 1 (A) : {reader["最低電流"].ToString()}\r\n" +
                                      $"功率因數 1 Power factor 1 (PF) : {reader["最低功率因素"].ToString()}\r\n" +
                                      $"震動測試 1 Vibration test 1 (μm) : {reader["最低震動"].ToString()}\r\n" +
                                      $"測試轉速 2 Test speed 2 (rpm) : {reader["最高轉速"].ToString()}\r\n" +
                                      $"測試電壓 2 Test voltage 2 (V) : {reader["最高電壓"].ToString()}\r\n" +
                                      $"測試頻率 2 Test frequency 2 (Hz) : {reader["最高頻率"].ToString()}\r\n" +
                                      $"輸入功率 2 Input power 2 (W) : {reader["最高功率"].ToString()}\r\n" +
                                      $"輸入電流 2 Input current 2 (A) : {reader["最高電流"].ToString()}\r\n" +
                                      $"功率因數 2 Power factor 2 (PF) : {reader["最高功率因素"].ToString()}\r\n" +
                                      $"震動測試 2 Vibration test 2 (μm) : {reader["最高震動"].ToString()}\r\n" +
                                      $"外觀檢測 Visual inspection : {reader["外觀檢測"]}\r\n" +
                                      $"運轉異音 Running noise : {reader["運轉異音"]}\r\n" +
                                      $"測試人員 Tester : {reader["使用者"]}\r\n";

                                    // $"日期 Date : {reader.GetString(0)}\r\n" +
                                    // $"時間 Time : {reader.GetString(1)}\r\n" +
                                    // $"產品代碼 Product No. : {reader.GetString(2)}\r\n" +
                                    // $"箱體編號 Cabinet No. : {reader.GetString(3)}\r\n" +
                                    // $"馬達編號 Motor No. : {reader.GetString(4)}\r\n" +
                                    // $"控制器編號 Controller No. : {reader.GetString(5)}\r\n" +
                                    // $"測試轉速 1 Test speed 1 (rpm) : {reader.GetSqlSingle(30).ToString()}\r\n" +
                                    // $"測試電壓 1 Test voltage 1 (V) : {reader.GetSqlSingle(26).ToString()}\r\n" +
                                    // $"測試頻率 1 Test frequency 1 (Hz) : {reader.GetSqlSingle(50).ToString()}\r\n" +
                                    // $"輸入功率 1 Input power 1 (W)  : {reader.GetSqlSingle(28).ToString()}\r\n" +
                                    // $"輸入電流 1 Input current 1 (A) : {reader.GetSqlSingle(27).ToString()}\r\n" +
                                    // $"功率因數 1 Power factor 1 (PF) : {reader.GetSqlSingle(49).ToString()}\r\n" +
                                    // $"震動測試 1 Vibration test 1 (μm) : {reader.GetSqlSingle(29).ToString()}\r\n" +
                                    // $"測試轉速 2 Test speed 2 (rpm) : {reader.GetSqlSingle(16).ToString()}\r\n" +
                                    // $"測試電壓 2 Test voltage 2 (V) : {reader.GetSqlSingle(12).ToString()}\r\n" +
                                    // $"測試頻率 2 Test frequency 2 (Hz) : {reader.GetSqlSingle(52).ToString()}\r\n" +
                                    // $"輸入功率 2 Input power 2 (W) : {reader.GetSqlSingle(14).ToString()}\r\n" +
                                    // $"輸入電流 2 Input current 2 (A) : {reader.GetSqlSingle(13).ToString()}\r\n" +
                                    // $"功率因數 2 Power factor 2 (PF) : {reader.GetSqlSingle(51).ToString()}\r\n" +
                                    // $"震動測試 2 Vibration test 2 (μm) : {reader.GetSqlSingle(15).ToString()}\r\n" +
                                    // $"外觀檢測 Visual inspection : {reader.GetString(17)}\r\n" +
                                    // $"運轉異音 Running noise : {reader.GetString(18)}\r\n" +
                                    // $"測試人員 Tester : {reader.GetString(19)}\r\n";
                                    ////因字串量過大刪除,否則QRCODE需到一定大小,會無法放入貼紙中
                                    ////$"測試轉速 3 Test speed 3 (rpm) : {reader.GetSqlSingle(10).ToString()}\r\n" +
                                    ////$"測試電壓 3 Test voltage 3 (V) : {reader.GetSqlSingle(6).ToString()}\r\n" +
                                    ////$"測試頻率 3 Test frequency 3 (Hz) : {reader.GetSqlSingle(11).ToString()}\r\n" +
                                    ////$"輸入功率 3 Input power 3 (W) : {reader.GetSqlSingle(8).ToString()}\r\n" +
                                    ////$"輸入電流 3 Input current 3 (A) : {reader.GetSqlSingle(7).ToString()}\r\n" +
                                    ////$"功率因數 3 Power factor 3 (PF) : {reader.GetSqlSingle(20).ToString()}\r\n" +
                                    ////$"震動測試 3 Vibration test 3 (μm) : {reader.GetSqlSingle(9).ToString()}\r\n" +

                        }
                        //MessageBox.Show(result);
                    }
                } else {
                    MessageBox.Show("找不到資料,請確認UserName是否與圖控設定一致!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Load += (sender, e) => { this.Close(); };
                    return;
                }
            }

            strSQL = "select COUNT([箱體編號]) from [" + type + "].[dbo].[" + type + "] " +
                     "WHERE [箱體編號] = '" + boxName + "'" +
                     "  AND [產品代碼] = '" + itemNo + "'";   //箱體號碼因為由案號編碼所以有可能重複,所以多加品號區分重複

            command = new SqlCommand(strSQL, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        boxNameCount = reader.GetInt32(0);
                    }
                }
            }

            strSQL = " select COUNT([馬達編號]) from [" +
                           type +
                           "].[dbo].[" +
                           type +
                           "] " +
                           "WHERE [馬達編號] = '" +
                           motorName +"'";

            command = new SqlCommand(strSQL, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        motorNameCount = reader.GetInt32(0);
                    }
                }
            }
            
            strSQL = " select COUNT([控制器編號]) from [" +
               type +
               "].[dbo].[" +
               type +
               "] " +
               "WHERE [控制器編號] = '" +
               controllerName + "'";

            command = new SqlCommand(strSQL, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        controllerNameCount = reader.GetInt32(0);
                    }
                }
            }
            #endregion

            timer = new System.Timers.Timer();
            timer.Interval = 500;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Tick);

            if (!appearanceStatus.Contains("OK"))
            {
                MessageBox.Show("外觀檢測不是OK !!!", "錯誤");
                this.Load += (sender, e) => { this.Close(); };
                return;
            }

            if (!voiceStatus.Contains("OK"))
            {
                MessageBox.Show("運轉異音不是OK !!!", "錯誤");
                this.Load += (sender, e) => { this.Close(); };
                return;
            }

            if (controllerNameCount > 1 || motorNameCount > 1 || boxNameCount > 1)
            {
                if (MessageBox.Show("箱體編號、控制器編號、馬達編號有重複，確定要列印QRCODE ? \r\n ( 相同箱體編號總數量: " + boxNameCount + "   相同控制器編號總數量 : " + controllerNameCount + "   相同馬達編號總數量 : " + motorNameCount  + " )",
                    "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,MessageBoxOptions.DefaultDesktopOnly) == DialogResult.Yes)
                {
                    timer.Start();
                }
                else
                {
                    this.Load += (sender, e) => { this.Close(); };
                }
            }
            else
            {
                timer.Start();
            }
        }
        
        private string GetConnectionString()
        {
            //return "Data Source=(local);"
            //      + "Integrated Security=SSPI;";
            //return "Data Source = 192.168.0.181;"
            //      + "User = sa; Password = 12;";
            string ServerName = iniHandler.ReadIniFile("Setting", "ServerName", "");
            if (string.IsNullOrEmpty(ServerName))
            {
                ServerName = "DESKTOP-8OH9HM4\\SQLEXPRESS";  //產線電腦資料庫 192.168.3.99
            }
            return "Data Source="+ ServerName + ";"
                   + "User = sa; Password = 12;";
        }

        //------------------------------------------------------------------------
        // Label Setup
        //------------------------------------------------------------------------
        private void LabelSetup()
        {
            if (type1 == "UL")    //貼紙大小
            {
                Printer.Config.LabelMode((PaperMode)0, 70, 12);
                Printer.Config.LabelWidth(70);
                Printer.Config.Dark(8);
            }
            else
            {
                Printer.Config.LabelMode((PaperMode)0, 40, 12);
                Printer.Config.LabelWidth(60);
                //Printer.Config.LabelMode((PaperMode)0, 70, 12);
                //Printer.Config.LabelWidth(70);
                Printer.Config.Dark(8);
            }
            Printer.Config.Speed(2);
            Printer.Config.PageNo(1);
            Printer.Config.CopyNo(1);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;

            if (!File.Exists(FILE_PATH))
            {
                Directory.CreateDirectory(FILE_PATH);
            }

            if (File.Exists(FILE_PATH + "/" + boxName + ".bmp"))
            {
                //MessageBox.Show("檔案已存在");
            }

            bool isLogo = false;

            string Logo = iniHandler.ReadIniFile("Setting", "Logo", "");

            if (Logo == "True")
                isLogo = true;
            else
                isLogo = false;

            if (string.IsNullOrEmpty(iniHandler.ReadIniFile("Setting", "Logo_Type", "")))
            {
                string itemNO = iniHandler.ReadIniFile("Setting", "Item No", "");
                string strSQL = "SELECT logo_type FROM [QRCODE].[dbo].[QR_INFO] " +
                                "WHERE item_no = '"+ itemNO +"'";
                            
                SqlCommand command = new SqlCommand(strSQL, sqlConnection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            logo_type = Int32.Parse(reader.GetString(0)); ;
                        }
                    }
                }
                iniHandler.WriteIniFile("Setting", "Logo_Type", logo_type);
            }
            else
            {
                logo_type = Int32.Parse(iniHandler.ReadIniFile("Setting", "Logo_Type", ""));
            }

            if (QrcodeGenerator.createQRCode2IMG(result, FILE_PATH, boxName, isLogo, type1, logo_type))
            {
                Printer.Open(PortType.USB);
                LabelSetup();

                Printer.Command.Start();

                string NameTxt    = iniHandler.ReadIniFile("Setting", "Name",    "");
                string TypeTxt    = iniHandler.ReadIniFile("Setting", "Type_",   "");
                string CaseTxt    = iniHandler.ReadIniFile("Setting", "Case No", "");
                string ItemTxt    = iniHandler.ReadIniFile("Setting", "Item No", "");
                string FormatTxt  = iniHandler.ReadIniFile("Setting", "Format",  "");
                string WeightTxt  = iniHandler.ReadIniFile("Setting", "Weight",  "");
                string FanTxt     = iniHandler.ReadIniFile("Setting", "FAN",     "");
                string Motor_1Txt = iniHandler.ReadIniFile("Setting", "MOTOR_1", "");
                string Motor_2Txt = iniHandler.ReadIniFile("Setting", "MOTOR_2", "");
                //UL
                string powerTxt = iniHandler.ReadIniFile("Setting", "PowerSource", "");
                string inputTxt = iniHandler.ReadIniFile("Setting", "Intput(Nominal)", "");
                string currentTxt = iniHandler.ReadIniFile("Setting", "OperatingCurrent", "");
                string methodTxt = iniHandler.ReadIniFile("Setting", "ProtectionMethod", "");

                int pos_x;
                int pos_y;
                int font_width;
                int font_width_UL = 11;
                if (type1 =="UL")
                {
                    pos_x = 15;
                    pos_y = 85;
                    font_width = 10;
                }
                else 
                {
                    //ori
                    pos_x = 9;
                    pos_y = 65;
                    //font_width = 10;
                    //因增加內容物字串而調整
                    font_width = 8;
                }
                //int pos_y = isLogo ? 65 : 33;
                int font_height = 22;
                int space = font_height + 3;
                //int space = isLogo ? font_height + 3 : font_height + 8;
                
                if (type1 == "UL")
                {
                    Printer.Command.PrintText(pos_x, pos_y, font_height, "", $"Name      : {NameTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space + 1, font_height, "", $"Model     : {TypeTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Item NO  : {ItemTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Size         : {FormatTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Weight    : {WeightTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"FAN         : {FanTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"MOTOR   : {Motor_1Txt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    if(powerTxt.Length > 11)
                    {
                        Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Power Source : {powerTxt.Substring(0,10)}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);
                        Printer.Command.PrintText(pos_x, pos_y += 20, font_height, "", $"                           {powerTxt.Substring(10)}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);
                    }
                    else
                    {
                        Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Power Source : {powerTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);
                    }

                    Printer.Command.PrintText(pos_x, pos_y += 20, font_height, "", $"Input(Nominal): {inputTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Operating Current  : {currentTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Mfg.Date  : {dt.Year.ToString() + "/" + dt.Month.ToString()}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"Protection Method  : {methodTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);
                    if (powerTxt.Length < 12)
                    {
                        Printer.Command.PrintText(pos_x, pos_y += 15, font_height, "", $"", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);
                    }
                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"CAUTION : MOUNT WITH THE LOWEST MOVING", font_width_UL, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"PARTS AT LEAST 2.5 METERS ABOVE FLOOR OR", font_width_UL, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"GRADE LEVEL.", font_width_UL, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"ATTENTION: INSTALLER DE SORTE QUE LES", font_width_UL, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"PIÈCES INFÈRIEURES SOIENT  À AU MOINS 2,5", font_width_UL, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space, font_height, "", $"MÈTRES AU-DESSUS  DU PLANCHER OU DU SOL.", font_width_UL, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                }
                else
                {
                    Printer.Command.PrintText(pos_x, pos_y, font_height, "", $"Name      : {NameTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"Type        : {TypeTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"Case NO: {CaseTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"Item NO  : {ItemTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"Format    : {FormatTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"Weight    : {WeightTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"FAN         : {FanTxt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"MOTOR  : {Motor_1Txt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                    Printer.Command.PrintText(pos_x, pos_y += space , font_height, "", $"             {Motor_2Txt}", font_width, FontWeight.FW_700_BOLD, RotateMode.Angle_0);

                }

                //箱體標號  改由QrcodeGenerator製作 
                //Printer.Command.PrintText(pos_x, pos_y += (space + 14), 22, "", $"{boxName}", 10, FontWeight.FW_700_BOLD, RotateMode.Angle_0); 

                int printResult = Printer.Command.PrintHalftoneImage(0, 0, FILE_PATH + "/" + boxName + ".bmp", 0, HalftoneMode.None);

                Printer.Command.End();
                Printer.Close();

                if (printResult == 0)
                {
                    updateText("列印失敗");
                }
                else if (printResult == 1)
                {
                    updateText("列印成功");
                }
                else
                    MessageBox.Show("錯誤代碼 :" + printResult.ToString());

                //System.IO.FileInfo fi = new System.IO.FileInfo(FILE_PATH + "/" + boxName + ".bmp");
                //try
                //{
                //    fi.Delete();
                //}
                //catch (System.IO.IOException ex)
                //{
                //    Console.WriteLine(ex.Message);
                //}
            }
            else
            {
                MessageBox.Show("QR CODE 圖檔產生錯誤");
            }

            sqlConnection.Close();
            Thread.Sleep(1000);
            closeForm();
        }

        private void closeForm()
        {
            if (this.InvokeRequired)
            {
                CloseWin uu = new CloseWin(closeForm);
                this.Invoke(uu);
            }
            else
            {
                this.Close();
            }
        }

        private void updateText(String str)
        {      
            if (this.InvokeRequired)
            {
                UpdateUI uu = new UpdateUI(updateText);
                this.Invoke(uu, str);
            }
            else
            {
                InfoLabel.Text = str;
            }
        }
    }
}
