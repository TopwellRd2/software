﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;

namespace sqldtataToExcel
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        private IniHandler iniHandler = new IniHandler(Application.StartupPath + "/settings.ini");

        public Form1()
        {
            InitializeComponent();
            start_dpt.Value = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd") + " 00:00:00"); ;
            end_dpt.Value = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd") + " 23:59:59");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (iniHandler.isExistIni() == true)
            {
                ip_TextBox.Text = iniHandler.ReadIniFile("settings", "IP", "");
                db_TextBox.Text = iniHandler.ReadIniFile("settings", "DB", "");
                table_TextBox.Text = iniHandler.ReadIniFile("settings", "Table", "");
                user_TextBox.Text = iniHandler.ReadIniFile("settings", "User", "");
                password_TextBox.Text = iniHandler.ReadIniFile("settings", "Password", "");
                columnName_TextBox.Text = iniHandler.ReadIniFile("settings", "ColumnName", "");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            iniHandler.WriteIniFile("settings", "IP", ip_TextBox.Text);
            iniHandler.WriteIniFile("settings", "DB", db_TextBox.Text);
            iniHandler.WriteIniFile("settings", "Table", table_TextBox.Text);
            iniHandler.WriteIniFile("settings", "User", user_TextBox.Text);
            iniHandler.WriteIniFile("settings", "Password", password_TextBox.Text);
            iniHandler.WriteIniFile("settings", "ColumnName", columnName_TextBox.Text);
        }
        private void use_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (use_CheckBox.Checked)
            {
                columnName_TextBox.Enabled = true;
                start_dpt.Enabled = true;
                end_dpt.Enabled = true;
            }
            else
            {
                columnName_TextBox.Enabled = false;
                start_dpt.Enabled = false;
                end_dpt.Enabled = false;
            }
        }

        private void export_Button_Click(object sender, EventArgs e)
        {
            DataTable dataTable = new DataTable();
            foreach (Control control in this.Controls)
            {
                if (control is TextBox)
                {
                    if (string.IsNullOrWhiteSpace(((TextBox)control).Text))
                    {
                        MessageBox.Show(control.Name.Replace("_TextBox", "") + "不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    } 
                }
            }

            String ip = ip_TextBox.Text;
            String db = db_TextBox.Text;
            String table = table_TextBox.Text;
            String user = user_TextBox.Text;
            String password = password_TextBox.Text;
            String column_Name = columnName_TextBox.Text;
            String fileName = fileName_TextBox.Text + ".xlsx";

            String sqlConnectStr = "Data Source=" + ip + ";Initial Catalog=" + db + "; Persist Security Info=True;User ID=" + user + ";Password=" + password;
            sqlConnection = new SqlConnection(sqlConnectStr);

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("1.請確認網路連線是否正常 \n2.請確認所填入的連線資訊是否正確", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String selectSql = "SELECT * FROM [" + db + "].[dbo].[" + table + "] WHERE 1 = 1 ";
            if (use_CheckBox.Checked)
            {
                selectSql = selectSql + " AND [" + column_Name + "] BETWEEN '" + start_dpt.Value.ToString("yyyy/MM/dd HH:mm:ss") + "' AND '" + end_dpt.Value.ToString("yyyy/MM/dd HH:mm:ss") + "'";
            }

            try
            {
                SqlCommand command = new SqlCommand(selectSql, sqlConnection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dataTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show("請檢查 Column Name 是否設定錯誤 ! \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (dataTable.Rows.Count == 0)
            {
                MessageBox.Show("查無資料!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }
            String filePath = folderBrowserDialog.SelectedPath;

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            // load excel, and create a new workbook
            Excel.Application app = new Excel.Application();
            Excel.Workbook wb = app.Workbooks.Add();
            Excel.Worksheet workSheet = wb.ActiveSheet;
            try
            {
                // column headings
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    workSheet.Cells[1, i + 1] = dataTable.Columns[i].ColumnName;
                }

                // rows
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < dataTable.Columns.Count; j++)
                    {
                        workSheet.Cells[i + 2, j + 1] = dataTable.Rows[i][j];
                    }
                }

                //using (SqlDataReader reader = command.ExecuteReader())
                //{
                //    if (reader.HasRows)
                //    {
                //        for (int i = 0; i < reader.FieldCount; i++)
                //        {
                //            workSheet.Cells[1, i + 1] = reader.GetName(i);
                //        }
                //        int z = 0;
                //        while (reader.Read())
                //        {
                //            for (int j = 0; j < reader.FieldCount; j++)
                //            {
                //                //if (reader.GetValue(j).GetType() == typeof(DateTime))
                //                //{
                //                //    workSheet.Cells[z + 2, j + 1] = reader.GetDateTime(j);
                //                //}
                //                //else
                //                //{
                //                //    workSheet.Cells[z + 2, j + 1] = reader.GetValue(j);
                //                //}
                //                workSheet.Cells[z + 2, j + 1] = reader.GetValue(j);
                //            }
                //            z++;
                //        }
                //    }
                //}

                try
                {
                    //存檔,如路徑已經有此檔案則直接刪除 再重新存一個新的
                    if (File.Exists(filePath + "\\" + fileName))
                    {
                        File.Delete(filePath + "\\" + fileName);
                    }
                    wb.SaveAs(filePath + "\\" + fileName);
                    MessageBox.Show("已匯出至" + filePath + "\\" + fileName + "!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    wb.Close();
                    app.Quit();
                    sqlConnection.Close();
                    MessageBox.Show("ExportToExcel: Excel file could not be saved! Check filepath.\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                wb.Close();
                app.Quit();
                sqlConnection.Close();
                MessageBox.Show("ExportToExcel: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            wb.Close();
            app.Quit();
            sqlConnection.Close();
        }
    }
}
