﻿
namespace sqldtataToExcel
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ip_TextBox = new System.Windows.Forms.TextBox();
            this.db_TextBox = new System.Windows.Forms.TextBox();
            this.table_TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.end_dpt = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.start_dpt = new System.Windows.Forms.DateTimePicker();
            this.date_GroupBox = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.columnName_TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.password_TextBox = new System.Windows.Forms.TextBox();
            this.user_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.fileName_TextBox = new System.Windows.Forms.TextBox();
            this.export_Button = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.label12 = new System.Windows.Forms.Label();
            this.use_CheckBox = new System.Windows.Forms.CheckBox();
            this.date_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(105, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(93, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "DB :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(64, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 31);
            this.label4.TabIndex = 2;
            this.label4.Text = "Table :";
            // 
            // ip_TextBox
            // 
            this.ip_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ip_TextBox.Location = new System.Drawing.Point(159, 15);
            this.ip_TextBox.Name = "ip_TextBox";
            this.ip_TextBox.Size = new System.Drawing.Size(365, 34);
            this.ip_TextBox.TabIndex = 4;
            this.ip_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // db_TextBox
            // 
            this.db_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.db_TextBox.Location = new System.Drawing.Point(159, 78);
            this.db_TextBox.Name = "db_TextBox";
            this.db_TextBox.Size = new System.Drawing.Size(365, 34);
            this.db_TextBox.TabIndex = 5;
            this.db_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // table_TextBox
            // 
            this.table_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.table_TextBox.Location = new System.Drawing.Point(159, 128);
            this.table_TextBox.Name = "table_TextBox";
            this.table_TextBox.Size = new System.Drawing.Size(365, 34);
            this.table_TextBox.TabIndex = 6;
            this.table_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(107, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(216, 22);
            this.label5.TabIndex = 7;
            this.label5.Text = "( or server instance name )";
            // 
            // end_dpt
            // 
            this.end_dpt.CalendarFont = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.end_dpt.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.end_dpt.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.end_dpt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_dpt.Location = new System.Drawing.Point(214, 223);
            this.end_dpt.Name = "end_dpt";
            this.end_dpt.Size = new System.Drawing.Size(326, 38);
            this.end_dpt.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(128, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 31);
            this.label6.TabIndex = 10;
            this.label6.Text = "from :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(161, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 31);
            this.label7.TabIndex = 9;
            this.label7.Text = "to :";
            // 
            // start_dpt
            // 
            this.start_dpt.CalendarFont = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.start_dpt.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.start_dpt.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.start_dpt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_dpt.Location = new System.Drawing.Point(214, 165);
            this.start_dpt.Name = "start_dpt";
            this.start_dpt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.start_dpt.Size = new System.Drawing.Size(326, 38);
            this.start_dpt.TabIndex = 8;
            // 
            // date_GroupBox
            // 
            this.date_GroupBox.Controls.Add(this.use_CheckBox);
            this.date_GroupBox.Controls.Add(this.label12);
            this.date_GroupBox.Controls.Add(this.label10);
            this.date_GroupBox.Controls.Add(this.columnName_TextBox);
            this.date_GroupBox.Controls.Add(this.label9);
            this.date_GroupBox.Controls.Add(this.label6);
            this.date_GroupBox.Controls.Add(this.start_dpt);
            this.date_GroupBox.Controls.Add(this.end_dpt);
            this.date_GroupBox.Controls.Add(this.label7);
            this.date_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.date_GroupBox.Location = new System.Drawing.Point(12, 286);
            this.date_GroupBox.Name = "date_GroupBox";
            this.date_GroupBox.Size = new System.Drawing.Size(571, 290);
            this.date_GroupBox.TabIndex = 9;
            this.date_GroupBox.TabStop = false;
            this.date_GroupBox.Text = "Date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(21, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(394, 22);
            this.label10.TabIndex = 17;
            this.label10.Text = "( Column name of the Date in the SQL database )";
            // 
            // columnName_TextBox
            // 
            this.columnName_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.columnName_TextBox.Location = new System.Drawing.Point(214, 96);
            this.columnName_TextBox.Name = "columnName_TextBox";
            this.columnName_TextBox.Size = new System.Drawing.Size(326, 34);
            this.columnName_TextBox.TabIndex = 7;
            this.columnName_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(19, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(193, 31);
            this.label9.TabIndex = 16;
            this.label9.Text = "Column Name :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // password_TextBox
            // 
            this.password_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.password_TextBox.Location = new System.Drawing.Point(159, 228);
            this.password_TextBox.Name = "password_TextBox";
            this.password_TextBox.PasswordChar = '*';
            this.password_TextBox.Size = new System.Drawing.Size(365, 34);
            this.password_TextBox.TabIndex = 8;
            this.password_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // user_TextBox
            // 
            this.user_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.user_TextBox.Location = new System.Drawing.Point(159, 178);
            this.user_TextBox.Name = "user_TextBox";
            this.user_TextBox.Size = new System.Drawing.Size(365, 34);
            this.user_TextBox.TabIndex = 7;
            this.user_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(18, 231);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 31);
            this.label3.TabIndex = 15;
            this.label3.Text = "Password :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(74, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 31);
            this.label8.TabIndex = 14;
            this.label8.Text = "User :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(12, 612);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 31);
            this.label11.TabIndex = 20;
            this.label11.Text = "File Name :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fileName_TextBox
            // 
            this.fileName_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.fileName_TextBox.Location = new System.Drawing.Point(161, 613);
            this.fileName_TextBox.Name = "fileName_TextBox";
            this.fileName_TextBox.Size = new System.Drawing.Size(224, 34);
            this.fileName_TextBox.TabIndex = 18;
            this.fileName_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // export_Button
            // 
            this.export_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.export_Button.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.export_Button.Location = new System.Drawing.Point(400, 594);
            this.export_Button.Name = "export_Button";
            this.export_Button.Size = new System.Drawing.Size(183, 66);
            this.export_Button.TabIndex = 19;
            this.export_Button.Text = "Export";
            this.export_Button.UseVisualStyleBackColor = true;
            this.export_Button.Click += new System.EventHandler(this.export_Button_Click);
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.SelectedPath = "C:\\Users\\TOPWELL\\Desktop";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(66, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 31);
            this.label12.TabIndex = 19;
            this.label12.Text = "Use or not :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // use_CheckBox
            // 
            this.use_CheckBox.AutoSize = true;
            this.use_CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.use_CheckBox.Checked = true;
            this.use_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.use_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.use_CheckBox.Location = new System.Drawing.Point(245, 57);
            this.use_CheckBox.Name = "use_CheckBox";
            this.use_CheckBox.Size = new System.Drawing.Size(18, 17);
            this.use_CheckBox.TabIndex = 6;
            this.use_CheckBox.UseVisualStyleBackColor = true;
            this.use_CheckBox.CheckedChanged += new System.EventHandler(this.use_CheckBox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(602, 678);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.fileName_TextBox);
            this.Controls.Add(this.export_Button);
            this.Controls.Add(this.password_TextBox);
            this.Controls.Add(this.user_TextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.date_GroupBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.table_TextBox);
            this.Controls.Add(this.db_TextBox);
            this.Controls.Add(this.ip_TextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "sqldataToExcel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.date_GroupBox.ResumeLayout(false);
            this.date_GroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ip_TextBox;
        private System.Windows.Forms.TextBox db_TextBox;
        private System.Windows.Forms.TextBox table_TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker end_dpt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker start_dpt;
        private System.Windows.Forms.GroupBox date_GroupBox;
        private System.Windows.Forms.TextBox password_TextBox;
        private System.Windows.Forms.TextBox user_TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox columnName_TextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox fileName_TextBox;
        private System.Windows.Forms.Button export_Button;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.CheckBox use_CheckBox;
        private System.Windows.Forms.Label label12;
    }
}

