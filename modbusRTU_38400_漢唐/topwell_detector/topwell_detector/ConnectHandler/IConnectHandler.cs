﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_detector
{
    interface IConnectHandler
    {
        bool IsConnected { get; }
        void connect(string ip);

        bool writeStatus(int id, uint val);
        bool writeSettingSpeed(int id, uint val);

        void OtkWriteStatus(uint val);
        void OtkWriteSettingSpeed(uint val);

        void close();
    }
}
