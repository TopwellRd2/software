﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace topwell_detector
{
    class SerialHandler : IConnectHandler
    {
        private SerialPort _serialPort;
        private Thread _thread;
        public bool IsConnected { get; private set; }

        private const byte READ_FUNCTION_CODE = 3;

        private const byte WRITE_FUNCTION_CODE = 6;

        private const uint READ_COUNT = 2;

        private ushort _startAddress = 0;

        private FFUModel[] m_FFUModelAry; 

        public SerialHandler(FFUModel[] FFUModelAry)
        {
            m_FFUModelAry = FFUModelAry;
            IsConnected = false;
        }

        public void close()
        {
            if (_serialPort != null)
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }
            IsConnected = false;
        }

        public void connect(string port)
        {
            try
            {
                _serialPort = new SerialPort(port, 38400, Parity.None, 8, StopBits.One);

                if (!_serialPort.IsOpen)
                {
                    _serialPort.Open();
                    _serialPort.RtsEnable = true;
                    _serialPort.DtrEnable = true;

                    IsConnected = true;

                    startThread();
                }
            }
            catch(Exception ex)
            {
                IsConnected = false;
                MessageBox.Show(ex.Message);
            }       
        }

        private void startThread()
        {
            _thread = new Thread(DoReceive);
            _thread.IsBackground = true;
            _thread.Start();
        }

        private void DoReceive()
        {
            try {
                while (_serialPort.IsOpen)
                {
                    byte[] inputByte;
                    for (int ffu = 0; ffu < 1; ++ffu)
                    {
                        inputByte = ReadHoldingRegistersMsg(Convert.ToByte(Form1.id), 0, 3, 2);
                        _serialPort.Write(inputByte, 0, inputByte.Length);

                        int count = 0;
                        bool readSuccess = true;

                        while (_serialPort.BytesToRead < 8)
                        {
                            Thread.Sleep(16); 
                            count++;

                            if (count > 20)
                            {
                                readSuccess = false;
                                break;
                            }
                        }

                        if (readSuccess)
                        {
                            byte[] bufferReceiver = new byte[_serialPort.BytesToRead];
                            _serialPort.Read(bufferReceiver, 0, _serialPort.BytesToRead);

                            int[] data = new int[READ_COUNT];

                            for (int i = 0; i < data.Length; i++)
                            {
                                data[i] = (data[i] << 8) + bufferReceiver[3 + (2 * i)];
                                data[i] = (data[i] << 8) + bufferReceiver[4 + (2 * i)];
                            }

                            if (bufferReceiver[1] == 3)
                            {
                                m_FFUModelAry[ffu].ErrorReadCount = 0;

                                m_FFUModelAry[ffu].Status = data[0];
                                m_FFUModelAry[ffu].SettingSpeed = data[1];
                                //m_FFUModelAry[ffu].NowSpeed = data[1];
                                //m_FFUModelAry[ffu].Alarm = data[3];
                            }
                            else
                            {
                                readError(ffu);
                            }
                        }
                        else
                        {
                            readError(ffu);
                        }


                        inputByte = ReadHoldingRegistersMsg(Convert.ToByte(Form1.id), 0, 4, 2);
                        _serialPort.Write(inputByte, 0, inputByte.Length);

                        count = 0;
                        readSuccess = true;

                        while (_serialPort.BytesToRead < 8)
                        {
                            Thread.Sleep(16);
                            count++;

                            if (count > 8)
                            {
                                readSuccess = false;
                                break;
                            }
                        }

                        if (readSuccess)
                        {
                            byte[] bufferReceiver = new byte[_serialPort.BytesToRead];
                            _serialPort.Read(bufferReceiver, 0, _serialPort.BytesToRead);

                            int[] data = new int[READ_COUNT];

                            for (int i = 0; i < data.Length; i++)
                            {
                                data[i] = (data[i] << 8) + bufferReceiver[3 + (2 * i)];
                                data[i] = (data[i] << 8) + bufferReceiver[4 + (2 * i)];
                            }

                            if (bufferReceiver[1] == 4)
                            {
                                //m_FFUModelAry[ffu].ErrorReadCount = 0;

                                //m_FFUModelAry[ffu].Status = data[0];
                                //m_FFUModelAry[ffu].SettingSpeed = data[1];
                                m_FFUModelAry[ffu].NowSpeed = data[1];
                                m_FFUModelAry[ffu].Alarm = data[0];
                            }
                            else
                            {
                                readError(ffu);
                            }
                        }
                        else
                        {
                            readError(ffu);
                        }


                        //Thread.Sleep(65);  //todo
                        
                        //if (_serialPort.BytesToRead >= 20)
                        //{
                        //    byte[] bufferReceiver = new byte[_serialPort.BytesToRead];
                        //    _serialPort.Read(bufferReceiver, 0, _serialPort.BytesToRead);

                        //    int[] data = new int[READ_COUNT];

                        //    for (int i = 0; i < data.Length; i++)
                        //    {
                        //        data[i] = (data[i] << 8) + bufferReceiver[3 + (2 * i)];
                        //        data[i] = (data[i] << 8) + bufferReceiver[4 + (2 * i)];
                        //    }

                        //    if (bufferReceiver[0] == ffu + 1 && bufferReceiver[1] == 3)
                        //    {
                        //        m_FFUModelAry[ffu].ErrorReadCount = 0;

                        //        m_FFUModelAry[ffu].Status = data[0];
                        //        m_FFUModelAry[ffu].SettingSpeed = data[1];
                        //        m_FFUModelAry[ffu].NowSpeed = data[4];
                        //        m_FFUModelAry[ffu].Alarm = data[3];
                        //    }
                           
                        //}
                        //else
                        //{
                        //    m_FFUModelAry[ffu].ErrorReadCount++;

                        //    if (m_FFUModelAry[ffu].ErrorReadCount > 2)
                        //    {
                        //        m_FFUModelAry[ffu].Status = -1;
                        //        m_FFUModelAry[ffu].SettingSpeed = -1;
                        //        m_FFUModelAry[ffu].NowSpeed = -1;
                        //        m_FFUModelAry[ffu].Alarm = 255;
                        //    }    
                        //}
                        //Thread.Sleep(20);
                    }
                    //Thread.Sleep(500);
                }
            }
            catch (ThreadAbortException e)
            {

            }
            catch (Exception ex) {
                 IsConnected = false;
                MessageBox.Show(ex.Message);
            }
        }

        private void readError(int ffu)
        {
            m_FFUModelAry[ffu].ErrorReadCount++;

            if (m_FFUModelAry[ffu].ErrorReadCount > 2)
            {
                m_FFUModelAry[ffu].ErrorReadCount = 0;

                m_FFUModelAry[ffu].Status = -1;
                m_FFUModelAry[ffu].SettingSpeed = -1;
                m_FFUModelAry[ffu].NowSpeed = -1;
                m_FFUModelAry[ffu].Alarm = 255;
            }   
        }

        public bool writeStatus(int id, uint val)
        {
            bool ret = writeData(Form1.id, 0, val);
            startThread();

            return false;
        }

        public bool writeSettingSpeed(int id, uint val)
        {
            bool ret =  writeData(Form1.id, 1, val);
            startThread();

            return ret;
        }

        public void OtkWriteStatus(uint val)
        {

        }

        public void OtkWriteSettingSpeed(uint val)
        {

        }

        private bool writeData(int id, ushort pos, uint val)
        {
            _thread.Abort();
            _serialPort.DiscardInBuffer();

            Thread.Sleep(100);

            byte[] inputByte;
            inputByte = ReadHoldingRegistersMsg(Convert.ToByte(id), pos, WRITE_FUNCTION_CODE, val);

            if (IsConnected){
                _serialPort.Write(inputByte, 0, inputByte.Length);

                //Thread.Sleep(500); 

                //if (_serialPort.BytesToRead >= inputByte.Length)
                if (true)
                {
                    //byte[] bufferReceiver = new byte[_serialPort.BytesToRead];
                    //_serialPort.Read(bufferReceiver, 0, _serialPort.BytesToRead);

                    //for (int i = 0; i < inputByte.Length; i++)
                    //{
                    //    //if (inputByte[i] != bufferReceiver[i])
                    //    //    return false;
                    //}
                    return true;
                }
                else
                    return false;
            }        
            else
                return false;              
        }

        private byte[] ReadHoldingRegistersMsg(byte slaveAddress, ushort startAddress, byte function, uint numberOfPoints)
        {
            byte[] frame = new byte[8];
            frame[0] = slaveAddress;			    // Slave Address
            frame[1] = function;				    // Function             
            frame[2] = (byte)(startAddress >> 8);	// Starting Address High
            frame[3] = (byte)startAddress;		    // Starting Address Low            
            frame[4] = (byte)(numberOfPoints >> 8);	// Quantity of Registers High
            frame[5] = (byte)numberOfPoints;		// Quantity of Registers Low
            byte[] crc = this.CalculateCRC(frame);  // Calculate CRC.
            frame[frame.Length - 2] = crc[0];       // Error Check Low
            frame[frame.Length - 1] = crc[1];       // Error Check High
            return frame;
        }

        private byte[] CalculateCRC(byte[] data)
        {
            ushort CRCFull = 0xFFFF; // Set the 16-bit register (CRC register) = FFFFH.
            char CRCLSB;
            byte[] CRC = new byte[2];
            for (int i = 0; i < (data.Length) - 2; i++)
            {
                CRCFull = (ushort)(CRCFull ^ data[i]); // 

                for (int j = 0; j < 8; j++)
                {
                    CRCLSB = (char)(CRCFull & 0x0001);
                    CRCFull = (ushort)((CRCFull >> 1) & 0x7FFF);

                    if (CRCLSB == 1)
                        CRCFull = (ushort)(CRCFull ^ 0xA001);
                }
            }
            CRC[1] = (byte)((CRCFull >> 8) & 0xFF);
            CRC[0] = (byte)(CRCFull & 0xFF);
            return CRC;
        }

    }
}
