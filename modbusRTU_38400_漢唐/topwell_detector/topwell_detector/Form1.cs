﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace topwell_detector
{
    enum ConnectType { None, TcpIp, SerialPort };

    public partial class Form1 : Form
    {
        public const int FFU_COUNT = 1;

        private const string SerialPortStr = "Serial Port";

        private const string TcpIpStr = "TCP/IP";

        #region UI Controls Array

        private TextBox[] StatusTxtAry = new TextBox[FFU_COUNT];
        private TextBox[] SettingSpeedTxtAry = new TextBox[FFU_COUNT];
        private TextBox[] NowSpeedTxtAry = new TextBox[FFU_COUNT];
        private TextBox[] AlarmTxtAry = new TextBox[FFU_COUNT];
        private Label[] IdLabelAry = new Label[FFU_COUNT];

        #endregion

        private FFUModel[] FFUModelAry = new FFUModel[FFU_COUNT];

        private Form_SettingSpeed m_Form_SettingSpeed = new Form_SettingSpeed();
        private Form_SettingStatus m_Form_SettingStatus = new Form_SettingStatus();
        private Form_OtkFun m_Form_OtkFun = new Form_OtkFun();
        private Form_Simple m_Form_Simple = new Form_Simple();

        private IConnectHandler m_ConnectHandler;

        private ConnectType m_connectType;

        public Form1()
        {
            InitializeComponent();

            initDisplay();

            m_connectType = ConnectType.None;

            for (int i = 0; i < FFU_COUNT; i++)
            {
                FFUModelAry[i] = new FFUModel();
            }

            setFFUModel2DisconnectState();
            updateModel2UI();  

            SerialPortToolStrip.Visible = false;
            TCPIPToolStrip.Visible = false;

            m_Form_SettingSpeed.EnterBtn.Click += new EventHandler(settingSpeedEnterBtnClickEvent);
            m_Form_SettingStatus.EnterBtn.Click += new EventHandler(settingStatusEnterBtnClickEvent);

            m_Form_OtkFun.StatusBtn.Click += new EventHandler(OtkSettingStatusEnterBtnClickEvent);
            m_Form_OtkFun.SpeedBtn.Click += new EventHandler(OtkSettingSpeedEnterBtnClickEvent);
        }

        #region 初始化UI
        /// <summary>
        /// 初始化UI
        /// </summary>
        private void initDisplay()
        {
            //新增Type 選項
            ConnectTypeBox.Items.Add(SerialPortStr);
            ConnectTypeBox.Items.Add(TcpIpStr);
            //新增迴路選項 1~6
            for (int i = 1; i <= 6; i++)
                CircuitBox.Items.Add(i);

            for (int i = 0; i < 1; i++)
            {
                const int INTERVAL = 305;

                Label statusLabel = new Label();
                statusLabel.Font = new System.Drawing.Font("",9);
                statusLabel.Size = new System.Drawing.Size(50, 20);
                statusLabel.Text = "Status";
                statusLabel.Location = new Point(50 + (i * INTERVAL), 0);

                Label setingSpeedLabel = new Label();
                setingSpeedLabel.Font = new System.Drawing.Font("", 9);
                setingSpeedLabel.Size = new System.Drawing.Size(70, 20);
                setingSpeedLabel.Text = "RPM Set";
                setingSpeedLabel.Location = new Point(110 + (i * INTERVAL), 0);

                Label nowSpeedLabel = new Label();
                nowSpeedLabel.Font = new System.Drawing.Font("", 9);
                nowSpeedLabel.Size = new System.Drawing.Size(50, 20);
                nowSpeedLabel.Text = "  RPM";
                nowSpeedLabel.Location = new Point(180 + (i * INTERVAL), 0);

                Label alarmLabel = new Label();
                alarmLabel.Font = new System.Drawing.Font("", 9);
                alarmLabel.Size = new System.Drawing.Size(50, 20);
                alarmLabel.Text = "Alarm Code";
                alarmLabel.Location = new Point(245 + (i * INTERVAL), 0);
                
                panel1.Controls.Add(statusLabel);
                panel1.Controls.Add(setingSpeedLabel);
                panel1.Controls.Add(nowSpeedLabel);
                panel1.Controls.Add(alarmLabel);

                const int ROW_COUNT = 1;

                for (int j = 0; j < ROW_COUNT; j++)
                {
                    if (i == 1 && j == 31) break;

                    Label idLabel = new Label();
                    idLabel.Size = new System.Drawing.Size(50, 20);
                    //idLabel.Text = "ID : " + ((i * ROW_COUNT) + j + 1).ToString();
                    idLabel.Location = new Point(0 + (i * INTERVAL), 25 + j * 30);
                    IdLabelAry[(i * ROW_COUNT) + j] = idLabel;

                    TextBox statusText = new TextBox();
                    statusText.Name = "Status_" + ((i * ROW_COUNT) + j + 1).ToString();
                    statusText.Click += new EventHandler(this.settingStatusBtnClickEvent);
                    statusText.Size = new System.Drawing.Size(50, 20);
                    statusText.Location = new Point(50 + (i * INTERVAL), 25 + j * 30);
                    statusText.ReadOnly = true;
                    StatusTxtAry[(i * ROW_COUNT) + j] = statusText;

                    TextBox settingSpeedText = new TextBox();
                    settingSpeedText.Name = "settingSpeed_" + ((i * ROW_COUNT) + j + 1).ToString();
                    settingSpeedText.Click += new EventHandler(this.settingSpeedBtnClickEvent);
                    settingSpeedText.Size = new System.Drawing.Size(50, 20);
                    settingSpeedText.Location = new Point(115 + (i * INTERVAL), 25 + j * 30);
                    settingSpeedText.ReadOnly = true;
                    SettingSpeedTxtAry[(i * ROW_COUNT) + j] = settingSpeedText;

                    TextBox nowSpeedText = new TextBox();
                    nowSpeedText.Size = new System.Drawing.Size(50, 20);
                    nowSpeedText.Location = new Point(180 + (i * INTERVAL), 25 + j * 30);
                    nowSpeedText.ReadOnly = true;
                    NowSpeedTxtAry[(i * ROW_COUNT) + j] = nowSpeedText;

                    TextBox alarmText = new TextBox();
                    alarmText.Size = new System.Drawing.Size(50, 20);
                    alarmText.Location = new Point(245 + (i * INTERVAL), 25 + j * 30);
                    alarmText.ReadOnly = true;
                    AlarmTxtAry[(i * ROW_COUNT) + j] = alarmText;

                    panel1.Controls.Add(statusText);
                    panel1.Controls.Add(settingSpeedText);
                    panel1.Controls.Add(nowSpeedText);
                    panel1.Controls.Add(alarmText);

                    panel1.Controls.Add(idLabel);
                }          
            }
        }
        #endregion

        #region Label點擊事件
        private void settingSpeedBtnClickEvent(object sender, EventArgs e)
        {
            if (m_ConnectHandler != null && m_ConnectHandler.IsConnected)
            {
                int id;
                bool result = int.TryParse(((TextBox)sender).Name.Split('_')[1], out id);

                m_Form_SettingSpeed.Name = "settingSpeed_" + ((TextBox)sender).Name.Split('_')[1];
                m_Form_SettingSpeed.IdLabel.Text = "ID : " + ((TextBox)sender).Name.Split('_')[1];
                m_Form_SettingSpeed.textBox1.Text = FFUModelAry[id - 1].SettingSpeed.ToString();
                m_Form_SettingSpeed.ShowDialog();
            }
        }

        private void settingStatusBtnClickEvent(object sender, EventArgs e)
        {
            if (m_ConnectHandler != null && m_ConnectHandler.IsConnected)
            {
                int id;
                bool result = int.TryParse(((TextBox)sender).Name.Split('_')[1], out id);

                m_Form_SettingStatus.Name = "settingStatus_" + ((TextBox)sender).Name.Split('_')[1];
                m_Form_SettingStatus.IdLabel.Text = "ID : " + ((TextBox)sender).Name.Split('_')[1];
                m_Form_SettingStatus.textBox1.Text = FFUModelAry[id - 1].Status.ToString();
                m_Form_SettingStatus.ShowDialog();
            }
        }
        #endregion

        #region 寫入資料事件
        private void settingSpeedEnterBtnClickEvent(object sender, EventArgs e)
        {
            int val = 0;
            int id = 0;

            bool result = int.TryParse(m_Form_SettingSpeed.textBox1.Text, out val);
            int.TryParse(m_Form_SettingSpeed.Name.Split('_')[1], out id);

            if (result)
            {
                if (val > 1800 || val < 300)
                {
                    MessageBox.Show("不能大於 1800 或 小於 300");
                }
                else
                {
                    m_ConnectHandler.writeSettingSpeed(id, (uint)val);
                    //if (m_ConnectHandler.writeSettingSpeed(id, (uint)val))
                    //    //MessageBox.Show("寫入成功");
                    //else
                    //    MessageBox.Show("寫入失敗");
                }
                
            }
            else
            {
                MessageBox.Show("非數字");
            }

            m_Form_SettingSpeed.Close();
        }

        private void settingStatusEnterBtnClickEvent(object sender, EventArgs e)
        {
            int val = 0;
            int id = 0;

            bool result = int.TryParse(m_Form_SettingStatus.textBox1.Text, out val);
            int.TryParse(m_Form_SettingStatus.Name.Split('_')[1], out id);

            if (val != 0 && val != 1)
            {
                MessageBox.Show("只能輸入 1 或 0");
            }
            else
            {
                if (result)
                {
                    if (m_ConnectHandler.writeStatus(id, (uint)val))
                        MessageBox.Show("寫入成功");
                    else
                        MessageBox.Show("寫入失敗");
                }
                else
                {
                    MessageBox.Show("非數字");
                }
            }
       
            m_Form_SettingStatus.Close();
        }

        private void OtkSettingStatusEnterBtnClickEvent(object sender, EventArgs e)
        {
            int val = 0;

            bool result = int.TryParse(m_Form_OtkFun.StatusTxt.Text, out val);

            if (val != 0 && val != 1)
            {
                MessageBox.Show("只能輸入 1 或 0");
            }
            else
            {
                if (result)
                {
                    m_ConnectHandler.OtkWriteStatus((uint)val);
                }
                else
                {
                    MessageBox.Show("非數字");
                }
            }

            m_Form_OtkFun.Close();
        }

        private void OtkSettingSpeedEnterBtnClickEvent(object sender, EventArgs e)
        {
            int val = 0;

            bool result = int.TryParse(m_Form_OtkFun.SpeedTxt.Text, out val);

            if (result)
            {
                if (val > 1800 || val < 300)
                {
                    MessageBox.Show("不能大於 1800 或 小於 300");
                }
                else
                {
                    m_ConnectHandler.OtkWriteSettingSpeed((uint)val);
                }

            }
            else
            {
                MessageBox.Show("非數字");
            }

            m_Form_OtkFun.Close();

        }
        #endregion

        #region 工作列事件
        private void ConnectTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ConnectTypeBox.Text == SerialPortStr)
            {
                m_connectType = ConnectType.SerialPort;
       
                SerialPortToolStrip.Visible = true;
                TCPIPToolStrip.Visible = false;
            }
            else if (ConnectTypeBox.Text == TcpIpStr)
            {
                m_connectType = ConnectType.TcpIp;

                CircuitBox.SelectedIndex = -1;
                SerialPortToolStrip.Visible = false;
                TCPIPToolStrip.Visible = true;
            }
            ConnectTypeBox.Enabled = false;
        }

      
        private void LockBtn_Click(object sender, EventArgs e)
        {
            ConnectTypeBox.SelectedIndex = -1;
            ConnectTypeBox.Enabled = true;

            SerialPortCombox.Items.Clear();
            SerialPortCombox.SelectedIndex = -1;
            CircuitBox.SelectedIndex = -1;

            SerialPortToolStrip.Visible = false;
            TCPIPToolStrip.Visible = false;

            if (m_ConnectHandler != null)
                m_ConnectHandler.close();

            setFFUModel2DisconnectState();
            updateModel2UI();     
        }

        private void ResearchSerialPortBtn_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            SerialPortCombox.Items.Clear();
            foreach (string port in ports)
                SerialPortCombox.Items.Add(port);
        }

        public static int id = 0;

        private void SerialPortConnectBtn_Click(object sender, EventArgs e)
        {
            id = Convert.ToInt32(idTxt.Text);

            if (m_connectType == ConnectType.SerialPort)
            {
                if (m_ConnectHandler != null && m_ConnectHandler.IsConnected)
                {
                    m_ConnectHandler.close();
                }
                else
                {
                    if (SerialPortCombox.SelectedItem != null)
                    {
                        if (m_ConnectHandler != null)
                            m_ConnectHandler.close();

                        string port = SerialPortCombox.SelectedItem.ToString();

                        m_ConnectHandler = new SerialHandler(FFUModelAry);
                        m_ConnectHandler.connect(port);

                        updateTimer.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("沒選擇 COM");
                    }
                } 
            }
            else
                MessageBox.Show("Error 1");
        }

        private void TcpIpConnectBtn_Click(object sender, EventArgs e)
        {
            if ( m_connectType == ConnectType.TcpIp)
            {
                if (m_ConnectHandler != null && m_ConnectHandler.IsConnected)
                {
                    m_ConnectHandler.close();
                }
                else
                {
                    if (CircuitBox.SelectedItem != null)
                    {
                        try
                        {
                            Ping ping = new Ping();
                            PingReply reply = ping.Send(IpTxt.Text, 2000);

                            if (reply.Status == IPStatus.Success)
                            {
                                int register;
                                int.TryParse(CircuitBox.SelectedItem.ToString(), out register);

                                if (m_ConnectHandler != null)
                                    m_ConnectHandler.close();

                                m_ConnectHandler = new TCPIPHandler(FFUModelAry, register);
                                m_ConnectHandler.connect(IpTxt.Text);

                                updateTimer.Enabled = true;
                            }
                            else
                            {
                                MessageBox.Show("Ping Error");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.StackTrace.ToString());
                        }      
                    }
                    else
                    {
                        MessageBox.Show("沒選擇迴路");
                    }
                }
            }
            else
                MessageBox.Show("Error 1");
        }
        #endregion

        #region TimerEvent
        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (m_ConnectHandler.IsConnected)
            {
                m_Form_Simple.label1.Text = "已連線";
                m_Form_Simple.label1.ForeColor = Color.Green;

                if (m_connectType == ConnectType.SerialPort)
                { 
                    SerialConnectStateLable.Text = "已連線";
                    SerialConnectStateLable.ForeColor = Color.Green;
                }
                else if(m_connectType == ConnectType.TcpIp)
                {
                    TCPConnectStateLable.Text = "已連線";
                    TCPConnectStateLable.ForeColor = Color.Green;  
                }  
            }
            else
            {
                m_Form_Simple.label1.Text = "未連線";
                m_Form_Simple.label1.ForeColor = Color.Red;

                if (m_connectType == ConnectType.SerialPort)
                {
                    SerialPortCombox.SelectedIndex = -1;
                    SerialPortCombox.Items.Clear();
                   
                    SerialConnectStateLable.Text = "未連線";
                    SerialConnectStateLable.ForeColor = Color.Red;
                }
                else if (m_connectType == ConnectType.TcpIp)
                {
                    TCPConnectStateLable.Text = "未連線";
                    TCPConnectStateLable.ForeColor = Color.Red;
                }
                m_ConnectHandler.close();

                updateTimer.Enabled = false;
                setFFUModel2DisconnectState();
            }

            updateModel2UI();      
        }

        private void updateModel2UI()
        {
            for (int i = 0; i < FFU_COUNT; i++)
            {
                StatusTxtAry[i].Text = FFUModelAry[i].Status.ToString();
                SettingSpeedTxtAry[i].Text = FFUModelAry[i].SettingSpeed.ToString();
                NowSpeedTxtAry[i].Text = FFUModelAry[i].NowSpeed.ToString();
                AlarmTxtAry[i].Text = FFUModelAry[i].Alarm.ToString();

                if (FFUModelAry[i].Alarm > 0)
                {
                    IdLabelAry[i].ForeColor = Color.Red;
                    m_Form_Simple.IDLabelAry[i].ForeColor = Color.Red;
                }
                else
                {
                    IdLabelAry[i].ForeColor = Color.Green;
                    m_Form_Simple.IDLabelAry[i].ForeColor = Color.Green;
                }
            }
        }

        private void setFFUModel2DisconnectState()
        {
            for (int i = 0; i < FFU_COUNT; i++)
            {
                FFUModelAry[i].Status = -1;
                FFUModelAry[i].SettingSpeed = -1;
                FFUModelAry[i].NowSpeed = -1;
                FFUModelAry[i].Alarm = 255;
            }
        }
        #endregion

        #region 功能按鈕點擊事件
        private void SimpleFormBtn_Click(object sender, EventArgs e)
        {
            m_Form_Simple.BringToFront();
            m_Form_Simple.Show();
        }

        private void OtkBtn_Click(object sender, EventArgs e)
        {
            m_Form_OtkFun.StatusTxt.Text = "";
            m_Form_OtkFun.SpeedTxt.Text = "";

            if (m_connectType == ConnectType.TcpIp)
            {
                if (m_ConnectHandler != null && m_ConnectHandler.IsConnected)
                    m_Form_OtkFun.ShowDialog();
                else
                    MessageBox.Show("未連線", "錯誤");
            }
            else 
            {
                MessageBox.Show("目前只有接Gateway有此功能","錯誤");
            }
        }
        #endregion

    }                            
}
