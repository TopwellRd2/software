﻿
namespace export_balancing_report
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.start_dpt = new System.Windows.Forms.DateTimePicker();
            this.allGroupBox = new System.Windows.Forms.GroupBox();
            this.exportAllLabel = new System.Windows.Forms.Label();
            this.exportAll_Button = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.export_Button = new System.Windows.Forms.Button();
            this.selectFileLabel = new System.Windows.Forms.Label();
            this.exportFolderLabel = new System.Windows.Forms.Label();
            this.selectFile_Button = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.case_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.end_dpt = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.file = new System.Windows.Forms.OpenFileDialog();
            this.allGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // start_dpt
            // 
            this.start_dpt.CalendarFont = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.start_dpt.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.start_dpt.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.start_dpt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_dpt.Location = new System.Drawing.Point(322, 65);
            this.start_dpt.Name = "start_dpt";
            this.start_dpt.Size = new System.Drawing.Size(332, 38);
            this.start_dpt.TabIndex = 2;
            // 
            // allGroupBox
            // 
            this.allGroupBox.Controls.Add(this.exportAllLabel);
            this.allGroupBox.Controls.Add(this.exportAll_Button);
            this.allGroupBox.Controls.Add(this.label8);
            this.allGroupBox.Controls.Add(this.label5);
            this.allGroupBox.Controls.Add(this.label7);
            this.allGroupBox.Controls.Add(this.label6);
            this.allGroupBox.Controls.Add(this.export_Button);
            this.allGroupBox.Controls.Add(this.selectFileLabel);
            this.allGroupBox.Controls.Add(this.exportFolderLabel);
            this.allGroupBox.Controls.Add(this.selectFile_Button);
            this.allGroupBox.Controls.Add(this.label4);
            this.allGroupBox.Controls.Add(this.case_TextBox);
            this.allGroupBox.Controls.Add(this.label1);
            this.allGroupBox.Controls.Add(this.end_dpt);
            this.allGroupBox.Controls.Add(this.label3);
            this.allGroupBox.Controls.Add(this.label2);
            this.allGroupBox.Controls.Add(this.start_dpt);
            this.allGroupBox.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.allGroupBox.Location = new System.Drawing.Point(12, 12);
            this.allGroupBox.Name = "allGroupBox";
            this.allGroupBox.Size = new System.Drawing.Size(721, 716);
            this.allGroupBox.TabIndex = 3;
            this.allGroupBox.TabStop = false;
            this.allGroupBox.Text = "請填以下匯出條件";
            // 
            // exportAllLabel
            // 
            this.exportAllLabel.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.exportAllLabel.Location = new System.Drawing.Point(25, 648);
            this.exportAllLabel.Name = "exportAllLabel";
            this.exportAllLabel.Size = new System.Drawing.Size(381, 48);
            this.exportAllLabel.TabIndex = 19;
            this.exportAllLabel.Text = "匯出路徑 :   D:\\動平衡資料\\20211004\\";
            this.exportAllLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // exportAll_Button
            // 
            this.exportAll_Button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.exportAll_Button.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.exportAll_Button.Location = new System.Drawing.Point(412, 643);
            this.exportAll_Button.Name = "exportAll_Button";
            this.exportAll_Button.Size = new System.Drawing.Size(266, 55);
            this.exportAll_Button.TabIndex = 18;
            this.exportAll_Button.Text = "匯出檔案";
            this.exportAll_Button.UseVisualStyleBackColor = false;
            this.exportAll_Button.Click += new System.EventHandler(this.exportAll_Button_Click);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(34, 597);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(331, 32);
            this.label8.TabIndex = 17;
            this.label8.Text = "(依據上方條件匯出)";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(6, 552);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(426, 45);
            this.label5.TabIndex = 16;
            this.label5.Text = "2. 匯出完整資料 :";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(175, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 32);
            this.label7.TabIndex = 15;
            this.label7.Text = "(案號前六碼)";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(334, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(331, 32);
            this.label6.TabIndex = 14;
            this.label6.Text = "(若要匯出所有案號,此條件請空白)";
            // 
            // export_Button
            // 
            this.export_Button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.export_Button.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.export_Button.Location = new System.Drawing.Point(412, 467);
            this.export_Button.Name = "export_Button";
            this.export_Button.Size = new System.Drawing.Size(266, 55);
            this.export_Button.TabIndex = 13;
            this.export_Button.Text = "匯出檔案";
            this.export_Button.UseVisualStyleBackColor = false;
            this.export_Button.Click += new System.EventHandler(this.export_Button_Click);
            // 
            // selectFileLabel
            // 
            this.selectFileLabel.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.selectFileLabel.Location = new System.Drawing.Point(242, 352);
            this.selectFileLabel.Name = "selectFileLabel";
            this.selectFileLabel.Size = new System.Drawing.Size(473, 93);
            this.selectFileLabel.TabIndex = 12;
            this.selectFileLabel.Text = "檔案路徑";
            this.selectFileLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.selectFileLabel.Click += new System.EventHandler(this.selectFileLabel_Click);
            // 
            // exportFolderLabel
            // 
            this.exportFolderLabel.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.exportFolderLabel.Location = new System.Drawing.Point(25, 472);
            this.exportFolderLabel.Name = "exportFolderLabel";
            this.exportFolderLabel.Size = new System.Drawing.Size(381, 48);
            this.exportFolderLabel.TabIndex = 11;
            this.exportFolderLabel.Text = "匯出路徑 :   D:\\動平衡資料\\20211004\\";
            this.exportFolderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectFile_Button
            // 
            this.selectFile_Button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.selectFile_Button.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.selectFile_Button.Location = new System.Drawing.Point(30, 366);
            this.selectFile_Button.Name = "selectFile_Button";
            this.selectFile_Button.Size = new System.Drawing.Size(206, 66);
            this.selectFile_Button.TabIndex = 10;
            this.selectFile_Button.Text = "選擇檔案";
            this.selectFile_Button.UseVisualStyleBackColor = false;
            this.selectFile_Button.Click += new System.EventHandler(this.selectFile_Button_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(6, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(426, 45);
            this.label4.TabIndex = 8;
            this.label4.Text = "1. 請選擇欲匯出格式之檔案 :";
            // 
            // case_TextBox
            // 
            this.case_TextBox.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.case_TextBox.Location = new System.Drawing.Point(322, 185);
            this.case_TextBox.Name = "case_TextBox";
            this.case_TextBox.Size = new System.Drawing.Size(332, 43);
            this.case_TextBox.TabIndex = 7;
            this.case_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(218, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "案號 :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // end_dpt
            // 
            this.end_dpt.CalendarFont = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.end_dpt.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.end_dpt.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.end_dpt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_dpt.Location = new System.Drawing.Point(322, 123);
            this.end_dpt.Name = "end_dpt";
            this.end_dpt.Size = new System.Drawing.Size(332, 38);
            this.end_dpt.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(143, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 36);
            this.label3.TabIndex = 4;
            this.label3.Text = "日期  -  起 :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(247, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 36);
            this.label2.TabIndex = 3;
            this.label2.Text = "迄 :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // file
            // 
            this.file.Filter = "Excel files (*.xls or *.xlsx)|*.xls;*.xlsx";
            this.file.Title = "選擇匯出檔案格式";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(745, 740);
            this.Controls.Add(this.allGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "動平衡資料匯出";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.allGroupBox.ResumeLayout(false);
            this.allGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DateTimePicker start_dpt;
        private System.Windows.Forms.GroupBox allGroupBox;
        private System.Windows.Forms.DateTimePicker end_dpt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox case_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button selectFile_Button;
        private System.Windows.Forms.Label selectFileLabel;
        private System.Windows.Forms.Label exportFolderLabel;
        private System.Windows.Forms.Button export_Button;
        private System.Windows.Forms.OpenFileDialog file;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button exportAll_Button;
        private System.Windows.Forms.Label exportAllLabel;
    }
}

