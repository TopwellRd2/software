﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace export_balancing_report
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        String export_path = "D:\\動平衡資料\\"+ DateTime.Now.ToString("yyyyMMdd");
        String exportAll_path = "D:\\動平衡完整資料";
        AutoSizeFormClass asc = new AutoSizeFormClass();

        public Form1()
        {
            InitializeComponent();
            start_dpt.Value = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd") + " 00:00:00"); ;
            end_dpt.Value = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd") + " 23:59:59");
            sqlConnection = new SqlConnection(Properties.Settings.Default.DBConnectionString);
            exportFolderLabel.Text = "匯出路徑:" + export_path + "\\" ;
            exportAllLabel.Text = "匯出路徑:" + exportAll_path + "\\";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            asc.controllInitializeSize(this, allGroupBox);
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (asc.oldCtrl.Count != 0)
            {
                asc.controlAutoSize(this, allGroupBox);
            }
        }

        private void selectFile_Button_Click(object sender, EventArgs e)
        {
            file.ShowDialog();
            if (!string.IsNullOrWhiteSpace(file.FileName))
            {
                selectFileLabel.Text = file.FileName;
            }
        }

        private void export_Button_Click(object sender, EventArgs e)
        {
            export_path = "D:\\動平衡資料\\" + DateTime.Now.ToString("yyyyMMdd");
            exportFolderLabel.Text = "匯出路徑:" + export_path + "\\";
            if (string.IsNullOrWhiteSpace(file.FileName))
            {
                MessageBox.Show("請先選擇匯出檔案的格式!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (file.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                else if (!string.IsNullOrWhiteSpace(file.FileName))
                {
                    selectFileLabel.Text = file.FileName;
                }
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //有輸入案號時
            if (!string.IsNullOrWhiteSpace(case_TextBox.Text)) 
            {

                String userSql = "SELECT DISTINCT [使用者] " +
                                 "  FROM [動平衡資料].[dbo].[RawData] " +
                                 " WHERE [時間] between '" + start_dpt.Value + "' and '" + end_dpt.Value + "'" +
                                 "   AND SUBSTRING([案號],1,6) = '" + case_TextBox.Text + "'";

                SqlCommand command = new SqlCommand(userSql, sqlConnection);
                List<String> userList = new List<String>();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            userList.Add(reader.GetString(0));
                        }
                    }
                    else
                    {
                        MessageBox.Show("查無資料!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                foreach (String user in userList)
                {
                    String chooseSql = "SELECT * " +
                                       "  FROM [動平衡資料].[dbo].[RawData] " +
                                       " WHERE [時間] between '" + start_dpt.Value + "' and '" + end_dpt.Value + "'" +
                                       "   AND SUBSTRING([案號],1,6) = '" + case_TextBox.Text + "'" +
                                       "   AND [使用者] = '" + user + "'" +
                                       " ORDER BY RIGHT([案號],5)";

                    command = new SqlCommand(chooseSql, sqlConnection);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            //先建當日+案號資料夾
                            if (!Directory.Exists(export_path + "\\" + case_TextBox.Text))
                            {
                                Directory.CreateDirectory(export_path + "\\" + case_TextBox.Text);
                            }
                            String export_fileName_xlsx = export_path + "\\" + case_TextBox.Text +"\\" + start_dpt.Value.ToString("yyyyMMdd") + "-" + case_TextBox.Text + "-" + user + ".xlsx";
                            //Excel
                            Excel.Application excelApp = new Excel.Application();  //應用程式
                            Excel.Workbook excelFormat = excelApp.Workbooks.Open(file.FileName);   //開啟此路徑檔案
                            Excel.Worksheet WorksheetFormat = (Excel.Worksheet)excelFormat.Worksheets[1];
                            WorksheetFormat.Copy(Type.Missing, WorksheetFormat);          //在第一個工作表後複製一樣格式的工作表出來
                            Excel.Worksheet newWorksheet = (Excel.Worksheet)excelFormat.Worksheets[2];
                            newWorksheet.Name = "data";
                            int i = 0;
                            int y = 0;
                            String vPageStr = "36";
                            String hPageStr = "70";
                            Excel.Range rangeFormat = WorksheetFormat.Range["A1", "R35"];      //原格式的範圍
                            Excel.Range newRange = newWorksheet.Range["A" + vPageStr, "R" + hPageStr];  //從A36:R70開始貼上
                            while (reader.Read())
                            {
                                try
                                {
                                    if (i == 24)    //格式一頁只能存24筆資料,換頁
                                    {
                                        i = 0;
                                        y++;
                                        vPageStr = (35 * y + 1).ToString();
                                        hPageStr = (35 * y + 35).ToString();
                                        newRange = newWorksheet.Range["A" + vPageStr, "R" + hPageStr];
                                        rangeFormat.Copy(newRange); //複製原格式到此工作表
                                        newWorksheet.PageSetup.PrintArea = "$A$1:$R$" + hPageStr;  //增加列印範圍
                                        newWorksheet.HPageBreaks.Add(newWorksheet.Range["A" + vPageStr]);  //插入分頁符號
                                        for (int z = 1; z < 36; z++)   //複製列高
                                        {
                                            newWorksheet.Range["A" + (35 * y + z).ToString(), "R" + (35 * y + z).ToString()].RowHeight = WorksheetFormat.Range["A" + z.ToString(), "R" + z.ToString()].RowHeight;
                                        }
                                    }
                                    //寫入Excel,依據格式位置
                                    newWorksheet.Cells[6 + 35 * y + i, 1] = reader.GetString(0);
                                    newWorksheet.Cells[6 + 35 * y + i, 2] = reader.GetString(1);
                                    newWorksheet.Cells[6 + 35 * y + i, 7] = reader.GetSqlSingle(4).ToString();
                                    newWorksheet.Cells[6 + 35 * y + i, 11] = Math.Round((Convert.ToDecimal(reader.GetDouble(5))), 2);
                                    newWorksheet.Cells[6 + 35 * y + i, 15] = Math.Round((Convert.ToDecimal(reader.GetDouble(7))), 2);
                                    i++;
                                }
                                catch (Exception ex)
                                {
                                    //關閉並釋放
                                    excelFormat.Close();
                                    excelApp.Quit();
                                    sqlConnection.Close();
                                    MessageBox.Show("Error:" + ex, "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                            }

                            //存檔,如路徑已經有此檔案則直接刪除 再重新存一個新的
                            if (File.Exists(export_fileName_xlsx))
                            {
                                File.Delete(export_fileName_xlsx);
                            }
                            excelFormat.SaveAs(export_fileName_xlsx);

                            //PDF
                            //String export_fileName_pdf = export_path + "\\" + case_TextBox.Text + ".pdf";
                            //if (File.Exists(export_fileName_pdf))
                            //{
                            //    File.Delete(export_fileName_pdf);
                            //}
                            //newWorksheet.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, export_fileName_pdf);

                            //關閉並釋放
                            excelFormat.Close();
                            excelApp.Quit();
                        }
                    }
                }
                MessageBox.Show("匯出完成!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //無輸入案號
            else
            {
                String caseSql = "SELECT DISTINCT SUBSTRING([案號],1,6) " +
                                 "  FROM [動平衡資料].[dbo].[RawData] " +
                                 " WHERE [時間] between '" + start_dpt.Value + "' and '" + end_dpt.Value + "'";

                SqlCommand command = new SqlCommand(caseSql, sqlConnection);
                List<String> caseList = new List<String>();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            caseList.Add(reader.GetString(0));
                        }
                    }
                    else
                    {
                        MessageBox.Show("查無資料!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                foreach (String caseNo in caseList)
                {

                    String userSql = "SELECT DISTINCT [使用者] " +
                                     "  FROM [動平衡資料].[dbo].[RawData] " +
                                     " WHERE [時間] between '" + start_dpt.Value + "' and '" + end_dpt.Value + "'" +
                                     "   AND SUBSTRING([案號],1,6) = '" + caseNo + "'";

                    command = new SqlCommand(userSql, sqlConnection);
                    List<String> userList = new List<String>();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                userList.Add(reader.GetString(0));
                            }
                        }
                        else
                        {
                            MessageBox.Show("查無資料!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    foreach (String user in userList)
                    {
                        String allSql = "SELECT * " +
                                        "  FROM [動平衡資料].[dbo].[RawData] " +
                                        " WHERE [時間] between '" + start_dpt.Value + "' and '" + end_dpt.Value + "'" +
                                        "   AND SUBSTRING([案號],1,6) = '" + caseNo + "'" +
                                        "   AND [使用者] = '" + user + "'" +
                                        " ORDER BY RIGHT([案號],5)";

                        command = new SqlCommand(allSql, sqlConnection);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                //先建當天資料夾
                                if (!Directory.Exists(export_path + "\\" + caseNo))
                                {
                                    Directory.CreateDirectory(export_path + "\\" + caseNo);
                                }
                                String export_fileName_xlsx = export_path + "\\" + caseNo + "\\" + start_dpt.Value.ToString("yyyyMMdd") + "-" + caseNo + "-" + user + ".xlsx";
                                //Excel
                                Excel.Application excelApp = new Excel.Application();  //應用程式
                                Excel.Workbook excelFormat = excelApp.Workbooks.Open(file.FileName);   //開啟此路徑檔案
                                Excel.Worksheet WorksheetFormat = (Excel.Worksheet)excelFormat.Worksheets[1];
                                WorksheetFormat.Copy(Type.Missing, WorksheetFormat);          //在第一個工作表後複製一樣格式的工作表出來
                                Excel.Worksheet newWorksheet = (Excel.Worksheet)excelFormat.Worksheets[2];
                                newWorksheet.Name = "data";
                                int i = 0;
                                int y = 0;
                                String vPageStr = "36";
                                String hPageStr = "70";
                                Excel.Range rangeFormat = WorksheetFormat.Range["A1", "R35"];      //原格式的範圍
                                Excel.Range newRange = newWorksheet.Range["A" + vPageStr, "R" + hPageStr];  //從A36:R70開始貼上
                                while (reader.Read())
                                {
                                    try
                                    {
                                        if (i == 24)    //格式一頁只能存24筆資料,換頁
                                        {
                                            i = 0;
                                            y++;
                                            vPageStr = (35 * y + 1).ToString();
                                            hPageStr = (35 * y + 35).ToString();
                                            newRange = newWorksheet.Range["A" + vPageStr, "R" + hPageStr];
                                            rangeFormat.Copy(newRange); //複製原格式到此工作表
                                            newWorksheet.PageSetup.PrintArea = "$A$1:$R$" + hPageStr;  //增加列印範圍
                                            newWorksheet.HPageBreaks.Add(newWorksheet.Range["A" + vPageStr]);  //插入分頁符號
                                            for (int z = 1; z < 36; z++)   //複製列高
                                            {
                                                newWorksheet.Range["A" + (35 * y + z).ToString(), "R" + (35 * y + z).ToString()].RowHeight = WorksheetFormat.Range["A" + z.ToString(), "R" + z.ToString()].RowHeight;
                                            }
                                        }
                                        //寫入Excel,依據格式位置
                                        newWorksheet.Cells[6 + 35 * y + i, 1] = reader.GetString(0);
                                        newWorksheet.Cells[6 + 35 * y + i, 2] = reader.GetString(1);
                                        newWorksheet.Cells[6 + 35 * y + i, 7] = reader.GetSqlSingle(4).ToString();
                                        newWorksheet.Cells[6 + 35 * y + i, 11] = Math.Round((Convert.ToDecimal(reader.GetDouble(5))), 2);
                                        newWorksheet.Cells[6 + 35 * y + i, 15] = Math.Round((Convert.ToDecimal(reader.GetDouble(7))), 2);
                                        i++;
                                    }
                                    catch (Exception ex)
                                    {
                                        //關閉並釋放
                                        excelFormat.Close();
                                        excelApp.Quit();
                                        sqlConnection.Close();
                                        MessageBox.Show("Error:" + ex, "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                }

                                //存檔,如路徑已經有此檔案則直接刪除 再重新存一個新的
                                if (File.Exists(export_fileName_xlsx))
                                {
                                    File.Delete(export_fileName_xlsx);
                                }
                                excelFormat.SaveAs(export_fileName_xlsx);

                                ////PDF
                                //String export_fileName_pdf = export_path + "\\" + case_TextBox.Text + ".pdf";
                                //if (File.Exists(export_fileName_pdf))
                                //{
                                //    File.Delete(export_fileName_pdf);
                                //}
                                //newWorksheet.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, export_fileName_pdf);

                                //關閉並釋放
                                excelFormat.Close();
                                excelApp.Quit();
                            }
                        }
                    }
                }
                MessageBox.Show("匯出完成!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            sqlConnection.Close();
        }

        private void exportAll_Button_Click(object sender, EventArgs e)
        {
            DataTable dataTable = new DataTable();
            exportAll_path = "D:\\動平衡完整資料";
            exportAllLabel.Text = "匯出路徑:" + exportAll_path + "\\";

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String allSql = "SELECT * " +
                            "  FROM [動平衡資料].[dbo].[RawData] " +
                            " WHERE [時間] between '" + start_dpt.Value + "' and '" + end_dpt.Value + "'";

            //有輸入案號時
            if (!string.IsNullOrWhiteSpace(case_TextBox.Text))
            {
                allSql = allSql + "   AND SUBSTRING([案號],1,6) = '" + case_TextBox.Text + "'";
            }

            allSql = allSql + " ORDER BY [時間]";

            SqlCommand command = new SqlCommand(allSql, sqlConnection);
            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(dataTable);

            if (dataTable.Rows.Count == 0)
            {
                MessageBox.Show("請確認查詢條件,查無資料!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!Directory.Exists(exportAll_path))
            {
                Directory.CreateDirectory(exportAll_path);
            }

            // load excel, and create a new workbook
            Excel.Application app = new Excel.Application();
            Excel.Workbook wb = app.Workbooks.Add();
            Excel.Worksheet workSheet = wb.ActiveSheet;
            try
            {
                // column headings
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {
                    workSheet.Cells[1, i + 1] = dataTable.Columns[i].ColumnName;
                }

                // rows
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < dataTable.Columns.Count; j++)
                    {
                        workSheet.Cells[i + 2, j + 1] = dataTable.Rows[i][j];
                    }
                }

                //using (SqlDataReader reader = command.ExecuteReader())
                //{
                //    if (reader.HasRows)
                //    {
                //        for (int i = 0; i < reader.FieldCount; i++)
                //        {
                //            workSheet.Cells[1, i + 1] = reader.GetName(i);
                //        }
                //        int z = 0;
                //        while (reader.Read())
                //        {
                //            for (int j = 0; j < reader.FieldCount; j++)
                //            {
                //                //if (reader.GetValue(j).GetType() == typeof(DateTime))
                //                //{
                //                //    workSheet.Cells[z + 2, j + 1] = reader.GetDateTime(j);
                //                //}
                //                //else
                //                //{
                //                //    workSheet.Cells[z + 2, j + 1] = reader.GetValue(j);
                //                //}
                //                workSheet.Cells[z + 2, j + 1] = reader.GetValue(j);
                //            }
                //            z++;
                //        }
                //    }
                //}

                try
                {
                    String fileName;
                    String startDate = start_dpt.Value.ToString("yyyyMMdd");
                    String endDate = end_dpt.Value.ToString("yyyyMMdd");
                    //有輸入案號時
                    if (!string.IsNullOrWhiteSpace(case_TextBox.Text))
                    {
                        fileName = exportAll_path + "\\" + startDate + "to" + endDate + "_" + case_TextBox.Text;
                    }
                    else
                    {
                        fileName = exportAll_path + "\\" + startDate + "to" + endDate + "_All";
                    }
                    //存檔,如路徑已經有此檔案則直接刪除 再重新存一個新的
                    if (File.Exists(fileName + ".xlsx"))
                    {
                        File.Delete(fileName + ".xlsx");
                    }
                    wb.SaveAs(fileName + ".xlsx");
                    MessageBox.Show("已匯出至" + fileName + ".xlsx !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    wb.Close();
                    app.Quit();
                    sqlConnection.Close();
                    MessageBox.Show("ExportToExcel: Excel file could not be saved! Check filepath.\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                wb.Close();
                app.Quit();
                sqlConnection.Close();
                MessageBox.Show("ExportToExcel: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            wb.Close();
            app.Quit();
            sqlConnection.Close();

        }

        private void selectFileLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
