﻿using System;
using Microsoft.VisualBasic.CompilerServices;

namespace trFixHistAlarmFile
{
	// Token: 0x0200000A RID: 10
	[StandardModule]
	internal sealed class modGlobal
	{
		// Token: 0x04000038 RID: 56
		public static object g_Object;

		// Token: 0x04000039 RID: 57
		public static bool g_Log;

		// Token: 0x0400003A RID: 58
		public static bool g_Demo = false;

		// Token: 0x0400003B RID: 59
		public static string g_QryType;

		// Token: 0x0400003C RID: 60
		public static int g_TimeOut;

		// Token: 0x0400003D RID: 61
		public static int g_x = -1;

		// Token: 0x0400003E RID: 62
		public static int g_y = -1;

		// Token: 0x0400003F RID: 63
		public static int g_H = 0;

		// Token: 0x04000040 RID: 64
		public static int g_W = 0;

		// Token: 0x04000041 RID: 65
		public static bool g_bNoTaskBar;

		// Token: 0x04000042 RID: 66
		public static frmHistAlarmFile fHistAlarm = new frmHistAlarmFile();

		// Token: 0x04000043 RID: 67
		public static string sTrendtek = "\r\n\r\nPlease contact Trendtek Automation\r\nwww.TrendTek.com.tw";

		// Token: 0x04000044 RID: 68
		internal const string ApTitle = "歷史警報/事件 檔案查詢程式";

		// Token: 0x04000045 RID: 69
		internal const string encryptKey = "73627273";

		// Token: 0x04000046 RID: 70
		internal const string encryptIV = "TrendTek";
	}
}
