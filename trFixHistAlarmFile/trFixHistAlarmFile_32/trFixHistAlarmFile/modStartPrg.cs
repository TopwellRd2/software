﻿using System;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using kvNetClass;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace trFixHistAlarmFile
{
	// Token: 0x0200000C RID: 12
	[StandardModule]
	internal sealed class modStartPrg
	{
		// Token: 0x0600007B RID: 123 RVA: 0x000061F0 File Offset: 0x000043F0
		[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
		public static void StartPrg()
		{
			try
			{
				int num = modStartPrg.kvCommon.funSingleProcessChk(Application.ProductName);
				bool flag = num > 0;
				if (flag)
				{
					try
					{
						Interaction.AppActivate(num);
					}
					catch (Exception ex)
					{
					}
					finally
					{
						ProjectData.EndApp();
					}
				}
				modSub.subParameterSearch();
				Application.Run(modGlobal.fHistAlarm);
				bool g_Demo = modGlobal.g_Demo;
				if (g_Demo)
				{
					string text = "感謝試用 " + Application.ProductName + "\r\n" + modGlobal.sTrendtek;
					MessageBox.Show(text, "DEMO模式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
				modSub.subReleaseResource("", "");
			}
			catch (Exception ex2)
			{
				MessageBox.Show(ex2.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		// Token: 0x04000049 RID: 73
		private static clsFixHelper kvFixHelper = new clsFixHelper();

		// Token: 0x0400004A RID: 74
		private static clsCommon kvCommon = new clsCommon();
	}
}
