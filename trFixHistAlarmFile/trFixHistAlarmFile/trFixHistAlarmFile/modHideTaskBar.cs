﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.CompilerServices;

namespace trFixHistAlarmFile
{
	// Token: 0x0200000B RID: 11
	[StandardModule]
	internal sealed class modHideTaskBar
	{
		// Token: 0x06000074 RID: 116
		[DllImport("user32", CharSet = CharSet.Ansi, EntryPoint = "FindWindowA", ExactSpelling = true, SetLastError = true)]
		private static extern int FindWindow([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpClassName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpWindowName);

		// Token: 0x06000075 RID: 117
		[DllImport("user32.dll", CharSet = CharSet.Ansi, EntryPoint = "FindWindowExA", ExactSpelling = true, SetLastError = true)]
		private static extern int FindWindowEx(int hWnd1, int hWnd2, int lpsz1, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpsz2);

		// Token: 0x06000076 RID: 118
		[DllImport("user32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		private static extern int SetForegroundWindow(int hwnd);

		// Token: 0x06000077 RID: 119
		[DllImport("user32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		private static extern int ShowWindow(int hwnd, int nCmdShow);

		// Token: 0x06000078 RID: 120
		[DllImport("user32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		private static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		// Token: 0x06000079 RID: 121 RVA: 0x00006084 File Offset: 0x00004284
		public static void subHideTaskBar(bool bHide)
		{
			try
			{
				string text = "Shell_traywnd";
				string text2 = "";
				int hwnd = modHideTaskBar.FindWindow(ref text, ref text2);
				int hWnd = 0;
				int hWnd2 = 0;
				int lpsz = 49175;
				text2 = null;
				int hwnd2 = modHideTaskBar.FindWindowEx(hWnd, hWnd2, lpsz, ref text2);
				if (bHide)
				{
					modHideTaskBar.SetWindowPos(hwnd, 0, 0, 0, 0, 0, 128);
					modHideTaskBar.ShowWindow(hwnd2, 0);
				}
				else
				{
					modHideTaskBar.SetWindowPos(hwnd, 0, 0, 0, 0, 0, 64);
					modHideTaskBar.ShowWindow(hwnd2, 1);
					text2 = null;
					text = "Proficy iFix WorkSpace (Run)";
					int num = modHideTaskBar.FindWindow(ref text2, ref text);
					bool flag = num > 0;
					if (flag)
					{
						modHideTaskBar.SetForegroundWindow(num);
					}
					else
					{
						text = null;
						text2 = "Proficy iFix WorkSpace (Configure)";
						num = modHideTaskBar.FindWindow(ref text, ref text2);
						bool flag2 = num > 0;
						if (flag2)
						{
							modHideTaskBar.SetForegroundWindow(num);
						}
						else
						{
							text2 = null;
							text = "Intellution iFix WorkSpace (Run)";
							num = modHideTaskBar.FindWindow(ref text2, ref text);
							bool flag3 = num > 0;
							if (flag3)
							{
								modHideTaskBar.SetForegroundWindow(num);
							}
							else
							{
								text = null;
								text2 = "Intellution iFix WorkSpace (Configure)";
								num = modHideTaskBar.FindWindow(ref text, ref text2);
								bool flag4 = num > 0;
								if (flag4)
								{
									modHideTaskBar.SetForegroundWindow(num);
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("HideTaskBar> " + ex.Message);
			}
		}

		// Token: 0x04000047 RID: 71
		private const int SWP_HIDEWINDOW = 128;

		// Token: 0x04000048 RID: 72
		private const int SWP_SHOWWINDOW = 64;
	}
}
