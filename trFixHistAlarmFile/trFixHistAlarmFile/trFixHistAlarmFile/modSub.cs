﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using kvNetClass;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Trendtek.Library;

namespace trFixHistAlarmFile
{
	// Token: 0x0200000D RID: 13
	[StandardModule]
	internal sealed class modSub
	{
		// Token: 0x0600007D RID: 125 RVA: 0x00006300 File Offset: 0x00004500
		public static void subChkLicense()
		{
			string text = "";
			try
			{
				clsFixLicChk clsFixLicChk = new clsFixLicChk();
				text = clsFixLicChk.funFixLicenseCheck(Application.StartupPath + "\\TrendTek.lic", Application.ProductName + ",FREE", "73627273", "TrendTek");
				bool flag = Operators.CompareString(text, "DEMO", false) == 0;
				if (flag)
				{
					modGlobal.g_Demo = true;
					bool flag2 = Operators.CompareString(text, "OK", false) != 0;
					if (flag2)
					{
						clsCommon clsCommon = new clsCommon();
						clsCommon.PlaySound("tada.wav");
						MessageBox.Show(text + modGlobal.sTrendtek, "授權碼不正確", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						modSub.subError(text, true, true, "", "");
					}
				}
			}
			catch (Exception ex)
			{
				modSub.subError(text + ex.Message, true, true, "", "");
			}
			finally
			{
			}
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00006410 File Offset: 0x00004610
		public static void subParameterSearch()
		{
			try
			{
				string commandLine = Environment.CommandLine;
				bool flag = Strings.InStr(commandLine, "/LOG", CompareMethod.Text) > 0;
				if (flag)
				{
					modGlobal.g_Log = true;
					string text = string.Concat(new string[]
					{
						"Command: ",
						commandLine,
						"  (Version: ",
						Application.ProductVersion,
						")"
					});
					modSub.subError(text, false, modGlobal.g_Log, "", "");
				}
				else
				{
					modGlobal.g_Log = false;
				}
				bool flag2 = Strings.InStr(commandLine, "/NTB", CompareMethod.Text) > 0;
				if (flag2)
				{
					modGlobal.g_bNoTaskBar = true;
				}
				string expression = modSub.funGetsubParameterX(commandLine, "/S:", "", false, false);
				bool flag3 = Strings.Len(expression) > 0;
				if (flag3)
				{
					string[] array = Strings.Split(expression, ",", -1, CompareMethod.Binary);
					try
					{
						switch (array.Length)
						{
						case 1:
							modGlobal.g_x = Conversions.ToInteger(array[0]);
							break;
						case 2:
							modGlobal.g_x = Conversions.ToInteger(array[0]);
							modGlobal.g_y = Conversions.ToInteger(array[1]);
							break;
						case 3:
							modGlobal.g_x = Conversions.ToInteger(array[0]);
							modGlobal.g_y = Conversions.ToInteger(array[1]);
							modGlobal.g_W = Conversions.ToInteger(array[2]);
							break;
						case 4:
							modGlobal.g_x = Conversions.ToInteger(array[0]);
							modGlobal.g_y = Conversions.ToInteger(array[1]);
							modGlobal.g_W = Conversions.ToInteger(array[2]);
							modGlobal.g_H = Conversions.ToInteger(array[3]);
							break;
						}
					}
					catch (Exception ex)
					{
					}
				}
			}
			catch (Exception ex2)
			{
				string text = "讀取[命令列引數]> : ";
				MessageBox.Show(text + ex2.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				modSub.subError(text + ex2.Message, true, false, "", "");
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00006640 File Offset: 0x00004840
		public static void subSingleInstanceByProcessName()
		{
			Common common = new Common();
			int hwdOfEarlyAPByProcessName = common.GetHwdOfEarlyAPByProcessName(Application.ProductName);
			bool flag = hwdOfEarlyAPByProcessName > 0;
			if (flag)
			{
				try
				{
					Interaction.AppActivate(hwdOfEarlyAPByProcessName);
				}
				catch (Exception ex)
				{
				}
				finally
				{
					Environment.Exit(0);
				}
			}
		}

		// Token: 0x06000080 RID: 128 RVA: 0x000066B0 File Offset: 0x000048B0
		public static void subSingleInstanceByApTitle(object sTitle)
		{
			Common common = new Common();
			int hwdByWindowsTitle = common.GetHwdByWindowsTitle(Conversions.ToString(sTitle));
			bool flag = hwdByWindowsTitle > 0;
			if (flag)
			{
				try
				{
					object[] array;
					bool[] array2;
					NewLateBinding.LateCall(null, typeof(Interaction), "AppActivate", array = new object[]
					{
						sTitle
					}, null, null, array2 = new bool[]
					{
						true
					}, true);
					if (array2[0])
					{
						sTitle = RuntimeHelpers.GetObjectValue(array[0]);
					}
				}
				catch (Exception ex)
				{
				}
				finally
				{
					Environment.Exit(0);
				}
			}
		}

		// Token: 0x06000081 RID: 129 RVA: 0x0000675C File Offset: 0x0000495C
		public static void subReleaseResource(string sAppName = "", string sTitleName = "")
		{
			string sSource = Environment.OSVersion.ToString();
			string text = modSub.funGetValueInString(sSource, "Microsoft Windows NT ", ".");
			string str = modSub.funGetValueInString(sSource, "Microsoft Windows NT " + text + ".", ".");
			bool flag = text.Length > 0 & text.Length > 0;
			float num;
			if (flag)
			{
				num = Conversions.ToSingle(text + "." + str);
			}
			else
			{
				num = 0f;
			}
			modGlobal.g_Object = null;
			bool flag2 = Strings.Len(sAppName) > 0;
			if (flag2)
			{
				modSub.kvComm = new clsCommon();
				bool flag3 = Strings.Len(sTitleName) > 0;
				if (flag3)
				{
					bool flag4 = (double)num < 5.009;
					if (flag4)
					{
						modSub.kvComm.subKillProcessGarbage(sAppName);
					}
					else
					{
						modSub.kvComm.subKillProcessGarbage(sAppName, sTitleName);
					}
				}
				else
				{
					modSub.kvComm.subKillProcessGarbage(sAppName);
				}
			}
			modSub.kvComm = null;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00006850 File Offset: 0x00004A50
		public static void subError(string sMsg, bool bEnd = false, bool bLog = true, string sKillAppName = "", string sKillTitleName = "")
		{
			string text = Strings.Format(DateAndTime.Today, "yyyyMMdd");
			try
			{
				if (bLog)
				{
					StreamWriter streamWriter = File.AppendText(string.Concat(new string[]
					{
						Application.StartupPath,
						"\\",
						Application.ProductName,
						text,
						".log"
					}));
					sMsg = Strings.Format(DateAndTime.Now, "MM/dd/yyyy HH:mm:ss") + "\t歷史警報/事件 檔案查詢程式 > " + sMsg;
					streamWriter.WriteLine(sMsg);
					streamWriter.Close();
				}
				sMsg = Strings.Format(DateAndTime.Now, "MM/dd/yyyy HH:mm:ss") + "\t" + sMsg;
				Debug.Write(sMsg);
				if (bEnd)
				{
					bool flag = Strings.Len(sKillAppName) > 0;
					if (flag)
					{
						bool flag2 = Strings.Len(sKillTitleName) > 0;
						if (flag2)
						{
							modSub.subReleaseResource(sKillAppName, sKillTitleName);
						}
						else
						{
							modSub.subReleaseResource(sKillAppName, "");
						}
					}
					else
					{
						modSub.subReleaseResource("", "");
					}
					Environment.Exit(0);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("subError> " + ex.Message);
			}
		}

		// Token: 0x06000083 RID: 131 RVA: 0x000069A0 File Offset: 0x00004BA0
		public static string funGetsubParameterX(string sCommand, string sSearch, string sError, bool bEnd = true, bool bLog = false)
		{
			checked
			{
				string result;
				try
				{
					int num = Strings.Len(sSearch);
					bool flag = Strings.InStr(1, sCommand, sSearch, CompareMethod.Text) != 0;
					if (flag)
					{
						int num2 = Strings.InStr(1, sCommand, sSearch, CompareMethod.Text);
						int num3 = Strings.InStr(num2 + 1, sCommand, " /", CompareMethod.Text);
						bool flag2 = num3 > 0;
						string text;
						if (flag2)
						{
							text = Strings.Mid(sCommand, num2 + num, num3 - num2 - num).Trim();
						}
						else
						{
							text = Strings.Right(sCommand, Strings.Len(sCommand) - num2 - num + 1).Trim();
						}
						bool flag3 = Strings.Len(text) > 0;
						if (flag3)
						{
							return text;
						}
					}
					bool flag4 = Strings.Len(sError) == 0;
					if (flag4)
					{
						result = "";
					}
					else
					{
						if (bEnd)
						{
							if (bLog)
							{
								modSub.subError(sError, true, bLog, "", "");
							}
							else
							{
								MessageBox.Show(sError, "歷史警報查詢", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								modSub.subError(sError, true, bLog, "", "");
							}
						}
						else if (bLog)
						{
							modSub.subError(sError, false, bLog, "", "");
						}
						else
						{
							MessageBox.Show(sError, "歷史警報查詢", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						result = "";
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}
				return result;
			}
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00006B14 File Offset: 0x00004D14
		public static string funGetValueInString(string sSource, string sTarget1, string sTarget2)
		{
			checked
			{
				string result;
				try
				{
					int num = Strings.Len(sTarget1);
					int num2 = Strings.InStr(1, sSource, sTarget1, CompareMethod.Text);
					bool flag = num2 < 1;
					if (flag)
					{
						result = "";
					}
					else
					{
						int num3 = Strings.InStr(num2 + num, sSource, sTarget2, CompareMethod.Text);
						bool flag2 = num3 < 1;
						if (flag2)
						{
							result = "";
						}
						else
						{
							result = Strings.Mid(sSource, num2 + num, num3 - num2 - num);
						}
					}
				}
				catch (Exception ex)
				{
					result = "";
				}
				return result;
			}
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00006B9C File Offset: 0x00004D9C
		public static Color funColorConvert(string colorString)
		{
			Color result;
			try
			{
				result = Color.FromArgb(Conversions.ToInteger(colorString));
			}
			catch (Exception ex)
			{
				bool isSystemColor = Color.FromName(colorString).IsSystemColor;
				if (isSystemColor)
				{
					result = Color.FromName(colorString);
				}
				else
				{
					bool isKnownColor = Color.FromName(colorString).IsKnownColor;
					if (isKnownColor)
					{
						result = Color.FromName(colorString);
					}
					else
					{
						int argb = checked((int)Math.Round(Conversion.Val("&H" + colorString + "&")));
						result = Color.FromArgb(argb);
					}
				}
			}
			return result;
		}

		// Token: 0x0400004B RID: 75
		private static clsCommon kvComm = new clsCommon();

		// Token: 0x0400004C RID: 76
		private static clsFixHelper kvFixHelper = new clsFixHelper();

		// Token: 0x0400004D RID: 77
		private static clsFixLicChk kvFixLicChk;
	}
}
