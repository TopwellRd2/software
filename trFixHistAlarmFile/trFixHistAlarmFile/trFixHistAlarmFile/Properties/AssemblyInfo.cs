﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyVersion("1.5.0.0")]
[assembly: AssemblyTitle("trFixHistAlarmFile")]
[assembly: AssemblyDescription("歷史警報檔案查詢")]
[assembly: AssemblyCompany("Trendtek System Automation")]
[assembly: AssemblyProduct("trFixHistAlarmFile")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2011")]
[assembly: AssemblyTrademark("Trendtek")]
[assembly: ComVisible(false)]
[assembly: Guid("6984a6c0-35df-47b4-8b2f-6f9feb9b96e2")]
[assembly: AssemblyFileVersion("1.5")]
