﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using GeFanuc.iFixToolkit.Adapter;
using kvNetClass;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace trFixHistAlarmFile
{
	// Token: 0x02000008 RID: 8
	public partial class frmHistAlarmFile : Form
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002300 File Offset: 0x00000500
		public frmHistAlarmFile()
		{
			base.Load += this.frmHistAlarm_Load;
			base.Shown += this.frmHistAlarmFile_Shown;
			base.KeyDown += this.frmMain_KeyDown;
			base.Closing += this.frmHistAlarm_Closing;
			base.FormClosed += this.frmHistAlarmFile_FormClosed;
			this.kvFixHelper = new clsFixHelper();
			this.kvComm = new clsCommon();
			this._iniChanged = false;
			this._intLine = 0;
			this.InitializeComponent();
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000023D8 File Offset: 0x000005D8
		// (set) Token: 0x06000016 RID: 22 RVA: 0x000023E4 File Offset: 0x000005E4
		internal virtual DateTimePicker dtStart
		{
			[CompilerGenerated]
			get
			{
				return this._dtStart;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.dtStart_Click);
				DateTimePicker dtStart = this._dtStart;
				if (dtStart != null)
				{
					dtStart.CloseUp -= value2;
				}
				this._dtStart = value;
				dtStart = this._dtStart;
				if (dtStart != null)
				{
					dtStart.CloseUp += value2;
				}
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002427 File Offset: 0x00000627
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002431 File Offset: 0x00000631
		internal virtual ComboBox cobNode { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000019 RID: 25 RVA: 0x0000243A File Offset: 0x0000063A
		// (set) Token: 0x0600001A RID: 26 RVA: 0x00002444 File Offset: 0x00000644
		internal virtual TextBox txtTag { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001B RID: 27 RVA: 0x0000244D File Offset: 0x0000064D
		// (set) Token: 0x0600001C RID: 28 RVA: 0x00002457 File Offset: 0x00000657
		internal virtual ComboBox cobType { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002460 File Offset: 0x00000660
		// (set) Token: 0x0600001E RID: 30 RVA: 0x0000246A File Offset: 0x0000066A
		internal virtual Label Label9 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600001F RID: 31 RVA: 0x00002473 File Offset: 0x00000673
		// (set) Token: 0x06000020 RID: 32 RVA: 0x00002480 File Offset: 0x00000680
		internal virtual Button btnOK
		{
			[CompilerGenerated]
			get
			{
				return this._btnOK;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnOK_Click);
				Button btnOK = this._btnOK;
				if (btnOK != null)
				{
					btnOK.Click -= value2;
				}
				this._btnOK = value;
				btnOK = this._btnOK;
				if (btnOK != null)
				{
					btnOK.Click += value2;
				}
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000021 RID: 33 RVA: 0x000024C3 File Offset: 0x000006C3
		// (set) Token: 0x06000022 RID: 34 RVA: 0x000024CD File Offset: 0x000006CD
		internal virtual ColorDialog ColorDialog1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000023 RID: 35 RVA: 0x000024D6 File Offset: 0x000006D6
		// (set) Token: 0x06000024 RID: 36 RVA: 0x000024E0 File Offset: 0x000006E0
		internal virtual GroupBox GroupBox3 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000025 RID: 37 RVA: 0x000024E9 File Offset: 0x000006E9
		// (set) Token: 0x06000026 RID: 38 RVA: 0x000024F3 File Offset: 0x000006F3
		internal virtual GroupBox GroupBox6 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000027 RID: 39 RVA: 0x000024FC File Offset: 0x000006FC
		// (set) Token: 0x06000028 RID: 40 RVA: 0x00002508 File Offset: 0x00000708
		internal virtual Button btnPrint
		{
			[CompilerGenerated]
			get
			{
				return this._btnPrint;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnPrint_Click);
				Button btnPrint = this._btnPrint;
				if (btnPrint != null)
				{
					btnPrint.Click -= value2;
				}
				this._btnPrint = value;
				btnPrint = this._btnPrint;
				if (btnPrint != null)
				{
					btnPrint.Click += value2;
				}
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000029 RID: 41 RVA: 0x0000254B File Offset: 0x0000074B
		// (set) Token: 0x0600002A RID: 42 RVA: 0x00002558 File Offset: 0x00000758
		internal virtual Button btnPrintPreview
		{
			[CompilerGenerated]
			get
			{
				return this._btnPrintPreview;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnPrint_Click);
				Button btnPrintPreview = this._btnPrintPreview;
				if (btnPrintPreview != null)
				{
					btnPrintPreview.Click -= value2;
				}
				this._btnPrintPreview = value;
				btnPrintPreview = this._btnPrintPreview;
				if (btnPrintPreview != null)
				{
					btnPrintPreview.Click += value2;
				}
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600002B RID: 43 RVA: 0x0000259B File Offset: 0x0000079B
		// (set) Token: 0x0600002C RID: 44 RVA: 0x000025A5 File Offset: 0x000007A5
		internal virtual PrintDialog Print1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600002D RID: 45 RVA: 0x000025AE File Offset: 0x000007AE
		// (set) Token: 0x0600002E RID: 46 RVA: 0x000025B8 File Offset: 0x000007B8
		internal virtual PrintPreviewDialog PreviewDialog1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600002F RID: 47 RVA: 0x000025C1 File Offset: 0x000007C1
		// (set) Token: 0x06000030 RID: 48 RVA: 0x000025CB File Offset: 0x000007CB
		internal virtual SaveFileDialog SaveFileDialog1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000031 RID: 49 RVA: 0x000025D4 File Offset: 0x000007D4
		// (set) Token: 0x06000032 RID: 50 RVA: 0x000025E0 File Offset: 0x000007E0
		internal virtual PrintDocument DocumentPrint
		{
			[CompilerGenerated]
			get
			{
				return this._DocumentPrint;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				PrintPageEventHandler value2 = new PrintPageEventHandler(this.DocumentPrint_PrintPage);
				PrintEventHandler value3 = new PrintEventHandler(this.DocumentPrint_BeginPrint);
				PrintDocument documentPrint = this._DocumentPrint;
				if (documentPrint != null)
				{
					documentPrint.PrintPage -= value2;
					documentPrint.BeginPrint -= value3;
				}
				this._DocumentPrint = value;
				documentPrint = this._DocumentPrint;
				if (documentPrint != null)
				{
					documentPrint.PrintPage += value2;
					documentPrint.BeginPrint += value3;
				}
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000033 RID: 51 RVA: 0x0000263E File Offset: 0x0000083E
		// (set) Token: 0x06000034 RID: 52 RVA: 0x00002648 File Offset: 0x00000848
		internal virtual PageSetupDialog PageSetupDialog1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000035 RID: 53 RVA: 0x00002651 File Offset: 0x00000851
		// (set) Token: 0x06000036 RID: 54 RVA: 0x0000265C File Offset: 0x0000085C
		internal virtual Button btnPrinterSet
		{
			[CompilerGenerated]
			get
			{
				return this._btnPrinterSet;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnPrinterSet_Click);
				Button btnPrinterSet = this._btnPrinterSet;
				if (btnPrinterSet != null)
				{
					btnPrinterSet.Click -= value2;
				}
				this._btnPrinterSet = value;
				btnPrinterSet = this._btnPrinterSet;
				if (btnPrinterSet != null)
				{
					btnPrinterSet.Click += value2;
				}
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000037 RID: 55 RVA: 0x0000269F File Offset: 0x0000089F
		// (set) Token: 0x06000038 RID: 56 RVA: 0x000026A9 File Offset: 0x000008A9
		internal virtual GroupBox gpQuery { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000039 RID: 57 RVA: 0x000026B2 File Offset: 0x000008B2
		// (set) Token: 0x0600003A RID: 58 RVA: 0x000026BC File Offset: 0x000008BC
		internal virtual Label lblDay
		{
			[CompilerGenerated]
			get
			{
				return this._lblDay;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.lblDay_Click);
				Label lblDay = this._lblDay;
				if (lblDay != null)
				{
					lblDay.Click -= value2;
				}
				this._lblDay = value;
				lblDay = this._lblDay;
				if (lblDay != null)
				{
					lblDay.Click += value2;
				}
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600003B RID: 59 RVA: 0x000026FF File Offset: 0x000008FF
		// (set) Token: 0x0600003C RID: 60 RVA: 0x0000270C File Offset: 0x0000090C
		internal virtual Label lblYesterday
		{
			[CompilerGenerated]
			get
			{
				return this._lblYesterday;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.lblDay_Click);
				Label lblYesterday = this._lblYesterday;
				if (lblYesterday != null)
				{
					lblYesterday.Click -= value2;
				}
				this._lblYesterday = value;
				lblYesterday = this._lblYesterday;
				if (lblYesterday != null)
				{
					lblYesterday.Click += value2;
				}
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600003D RID: 61 RVA: 0x0000274F File Offset: 0x0000094F
		// (set) Token: 0x0600003E RID: 62 RVA: 0x0000275C File Offset: 0x0000095C
		internal virtual Label lbl2DatsBefor
		{
			[CompilerGenerated]
			get
			{
				return this._lbl2DatsBefor;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.lblDay_Click);
				Label lbl2DatsBefor = this._lbl2DatsBefor;
				if (lbl2DatsBefor != null)
				{
					lbl2DatsBefor.Click -= value2;
				}
				this._lbl2DatsBefor = value;
				lbl2DatsBefor = this._lbl2DatsBefor;
				if (lbl2DatsBefor != null)
				{
					lbl2DatsBefor.Click += value2;
				}
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600003F RID: 63 RVA: 0x0000279F File Offset: 0x0000099F
		// (set) Token: 0x06000040 RID: 64 RVA: 0x000027A9 File Offset: 0x000009A9
		internal virtual GroupBox GroupBox4 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000041 RID: 65 RVA: 0x000027B2 File Offset: 0x000009B2
		// (set) Token: 0x06000042 RID: 66 RVA: 0x000027BC File Offset: 0x000009BC
		internal virtual Label Label6 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000043 RID: 67 RVA: 0x000027C5 File Offset: 0x000009C5
		// (set) Token: 0x06000044 RID: 68 RVA: 0x000027CF File Offset: 0x000009CF
		internal virtual GroupBox GroupBox5 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000045 RID: 69 RVA: 0x000027D8 File Offset: 0x000009D8
		// (set) Token: 0x06000046 RID: 70 RVA: 0x000027E2 File Offset: 0x000009E2
		internal virtual Label lblMessage { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000047 RID: 71 RVA: 0x000027EB File Offset: 0x000009EB
		// (set) Token: 0x06000048 RID: 72 RVA: 0x000027F8 File Offset: 0x000009F8
		internal virtual Label lblBackward
		{
			[CompilerGenerated]
			get
			{
				return this._lblBackward;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.lblForward_Click);
				Label lblBackward = this._lblBackward;
				if (lblBackward != null)
				{
					lblBackward.Click -= value2;
				}
				this._lblBackward = value;
				lblBackward = this._lblBackward;
				if (lblBackward != null)
				{
					lblBackward.Click += value2;
				}
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000049 RID: 73 RVA: 0x0000283B File Offset: 0x00000A3B
		// (set) Token: 0x0600004A RID: 74 RVA: 0x00002848 File Offset: 0x00000A48
		internal virtual Label lblForward
		{
			[CompilerGenerated]
			get
			{
				return this._lblForward;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.lblForward_Click);
				Label lblForward = this._lblForward;
				if (lblForward != null)
				{
					lblForward.Click -= value2;
				}
				this._lblForward = value;
				lblForward = this._lblForward;
				if (lblForward != null)
				{
					lblForward.Click += value2;
				}
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600004B RID: 75 RVA: 0x0000288B File Offset: 0x00000A8B
		// (set) Token: 0x0600004C RID: 76 RVA: 0x00002895 File Offset: 0x00000A95
		internal virtual RichTextBox RichTextBoxAlarm { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600004D RID: 77 RVA: 0x0000289E File Offset: 0x00000A9E
		// (set) Token: 0x0600004E RID: 78 RVA: 0x000028A8 File Offset: 0x00000AA8
		internal virtual RichTextBox RichTextBox1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600004F RID: 79 RVA: 0x000028B1 File Offset: 0x00000AB1
		// (set) Token: 0x06000050 RID: 80 RVA: 0x000028BB File Offset: 0x00000ABB
		internal virtual Label Label8 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000051 RID: 81 RVA: 0x000028C4 File Offset: 0x00000AC4
		// (set) Token: 0x06000052 RID: 82 RVA: 0x000028CE File Offset: 0x00000ACE
		internal virtual Label Label10 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000053 RID: 83 RVA: 0x000028D7 File Offset: 0x00000AD7
		// (set) Token: 0x06000054 RID: 84 RVA: 0x000028E4 File Offset: 0x00000AE4
		internal virtual Button btnForeground
		{
			[CompilerGenerated]
			get
			{
				return this._btnForeground;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnForeground_Click);
				Button btnForeground = this._btnForeground;
				if (btnForeground != null)
				{
					btnForeground.Click -= value2;
				}
				this._btnForeground = value;
				btnForeground = this._btnForeground;
				if (btnForeground != null)
				{
					btnForeground.Click += value2;
				}
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00002927 File Offset: 0x00000B27
		// (set) Token: 0x06000056 RID: 86 RVA: 0x00002934 File Offset: 0x00000B34
		internal virtual Button btnBackground
		{
			[CompilerGenerated]
			get
			{
				return this._btnBackground;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnForeground_Click);
				Button btnBackground = this._btnBackground;
				if (btnBackground != null)
				{
					btnBackground.Click -= value2;
				}
				this._btnBackground = value;
				btnBackground = this._btnBackground;
				if (btnBackground != null)
				{
					btnBackground.Click += value2;
				}
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002977 File Offset: 0x00000B77
		// (set) Token: 0x06000058 RID: 88 RVA: 0x00002981 File Offset: 0x00000B81
		internal virtual ToolTip ToolTip1 { get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000059 RID: 89 RVA: 0x0000298A File Offset: 0x00000B8A
		// (set) Token: 0x0600005A RID: 90 RVA: 0x00002994 File Offset: 0x00000B94
		internal virtual Button btnFind
		{
			[CompilerGenerated]
			get
			{
				return this._btnFind;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnFind_Click);
				Button btnFind = this._btnFind;
				if (btnFind != null)
				{
					btnFind.Click -= value2;
				}
				this._btnFind = value;
				btnFind = this._btnFind;
				if (btnFind != null)
				{
					btnFind.Click += value2;
				}
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600005B RID: 91 RVA: 0x000029D7 File Offset: 0x00000BD7
		// (set) Token: 0x0600005C RID: 92 RVA: 0x000029E4 File Offset: 0x00000BE4
		internal virtual Button btnFindNext
		{
			[CompilerGenerated]
			get
			{
				return this._btnFindNext;
			}
			[CompilerGenerated]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				EventHandler value2 = new EventHandler(this.btnFindNext_Click);
				Button btnFindNext = this._btnFindNext;
				if (btnFindNext != null)
				{
					btnFindNext.Click -= value2;
				}
				this._btnFindNext = value;
				btnFindNext = this._btnFindNext;
				if (btnFindNext != null)
				{
					btnFindNext.Click += value2;
				}
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00003EF4 File Offset: 0x000020F4
		internal void subDbIniFileRead()
		{
			string path = Application.StartupPath + "\\" + Application.ProductName + ".ini";
			try
			{
				bool flag = File.Exists(path);
				if (flag)
				{
					FileStream fileStream = File.Open(path, FileMode.Open, FileAccess.Read);
					StreamReader streamReader = new StreamReader(fileStream, Encoding.Default);
					string text = streamReader.ReadToEnd();
					text = text.ToUpper();
					string text2 = this.kvComm.funGetValueInString(text, "MarginsPrinterName=", ";");
					bool flag2 = Strings.Len(text2) > 0;
					if (flag2)
					{
						this.DocumentPrint.PrinterSettings.PrinterName = text2;
					}
					text2 = this.kvComm.funGetValueInString(text, "MarginsLandscape=", ";");
					bool flag3 = Strings.Len(text2) > 0 & Operators.CompareString(text2.ToUpper(), "TRUE", false) == 0;
					if (flag3)
					{
						this.DocumentPrint.DefaultPageSettings.Landscape = true;
					}
					else
					{
						this.DocumentPrint.DefaultPageSettings.Landscape = false;
					}
					text2 = this.kvComm.funGetValueInString(text, "MarginsTop=", ";");
					bool flag4 = Strings.Len(text2) > 0;
					if (flag4)
					{
						this.DocumentPrint.DefaultPageSettings.Margins.Top = Conversions.ToInteger(text2);
					}
					text2 = this.kvComm.funGetValueInString(text, "MarginsBottom=", ";");
					bool flag5 = Strings.Len(text2) > 0;
					if (flag5)
					{
						this.DocumentPrint.DefaultPageSettings.Margins.Bottom = Conversions.ToInteger(text2);
					}
					text2 = this.kvComm.funGetValueInString(text, "MarginsLeft=", ";");
					bool flag6 = Strings.Len(text2) > 0;
					if (flag6)
					{
						this.DocumentPrint.DefaultPageSettings.Margins.Left = Conversions.ToInteger(text2);
					}
					text2 = this.kvComm.funGetValueInString(text, "MarginsRight=", ";");
					bool flag7 = Strings.Len(text2) > 0;
					if (flag7)
					{
						this.DocumentPrint.DefaultPageSettings.Margins.Right = Conversions.ToInteger(text2);
					}
					text2 = this.kvComm.funGetValueInString(text, "ForegroundColor=", ";");
					bool flag8 = Strings.Len(text2) > 0;
					if (flag8)
					{
						this.btnForeground.BackColor = modSub.funColorConvert(text2);
					}
					this.RichTextBoxAlarm.ForeColor = this.btnForeground.BackColor;
					text2 = this.kvComm.funGetValueInString(text, "BackgroundColor=", ";");
					bool flag9 = Strings.Len(text2) > 0;
					if (flag9)
					{
						this.btnBackground.BackColor = modSub.funColorConvert(text2);
					}
					this.RichTextBoxAlarm.BackColor = this.btnBackground.BackColor;
				}
				else
				{
					this.DocumentPrint.DefaultPageSettings.Margins.Top = 30;
					this.DocumentPrint.DefaultPageSettings.Margins.Bottom = 70;
					this.DocumentPrint.DefaultPageSettings.Margins.Left = 50;
					this.DocumentPrint.DefaultPageSettings.Margins.Right = 50;
					this.DocumentPrint.DefaultPageSettings.Landscape = true;
					StreamReader streamReader;
					bool flag10 = !Information.IsNothing(streamReader);
					if (flag10)
					{
						streamReader.Close();
					}
					FileStream fileStream;
					bool flag11 = !Information.IsNothing(fileStream);
					if (flag11)
					{
						fileStream.Close();
					}
				}
			}
			catch (Exception ex)
			{
			}
			finally
			{
				StreamReader streamReader;
				bool flag12 = !Information.IsNothing(streamReader);
				if (flag12)
				{
					streamReader.Close();
				}
				FileStream fileStream;
				bool flag13 = !Information.IsNothing(fileStream);
				if (flag13)
				{
					fileStream.Close();
				}
			}
		}

		// Token: 0x0600005F RID: 95 RVA: 0x000042B4 File Offset: 0x000024B4
		public void subDateColor()
		{
			bool flag = DateTime.Compare(this.dtStart.Value.Date, DateAndTime.Now.Date) == 0;
			if (flag)
			{
				this.lblDay.BackColor = Color.ForestGreen;
				this.lblYesterday.BackColor = Color.MediumBlue;
				this.lbl2DatsBefor.BackColor = Color.DarkBlue;
				this.lblDay.ForeColor = Color.Yellow;
				this.lblYesterday.ForeColor = Color.White;
				this.lbl2DatsBefor.ForeColor = Color.White;
			}
			else
			{
				bool flag2 = DateTime.Compare(this.dtStart.Value.Date, DateAndTime.DateAdd(DateInterval.Day, -1.0, DateAndTime.Now).Date) == 0;
				if (flag2)
				{
					this.lblDay.BackColor = Color.Blue;
					this.lblYesterday.BackColor = Color.ForestGreen;
					this.lbl2DatsBefor.BackColor = Color.DarkBlue;
					this.lblDay.ForeColor = Color.White;
					this.lblYesterday.ForeColor = Color.Yellow;
					this.lbl2DatsBefor.ForeColor = Color.White;
				}
				else
				{
					bool flag3 = DateTime.Compare(this.dtStart.Value.Date, DateAndTime.DateAdd(DateInterval.Day, -2.0, DateAndTime.Now).Date) == 0;
					if (flag3)
					{
						this.lblDay.BackColor = Color.Blue;
						this.lblYesterday.BackColor = Color.MediumBlue;
						this.lbl2DatsBefor.BackColor = Color.ForestGreen;
						this.lblDay.ForeColor = Color.White;
						this.lblYesterday.ForeColor = Color.White;
						this.lbl2DatsBefor.ForeColor = Color.Yellow;
					}
					else
					{
						this.lblDay.BackColor = Color.Blue;
						this.lblYesterday.BackColor = Color.MediumBlue;
						this.lbl2DatsBefor.BackColor = Color.DarkBlue;
						this.lblDay.ForeColor = Color.White;
						this.lblYesterday.ForeColor = Color.White;
						this.lbl2DatsBefor.ForeColor = Color.White;
					}
				}
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00004510 File Offset: 0x00002710
		private void frmHistAlarm_Load(object sender, EventArgs e)
		{
			checked
			{
				try
				{
					modDllChk.subDLLCheck();
					modSub.subSingleInstanceByProcessName();
					modSub.subParameterSearch();
					this.Text = this.Text + "  (version " + Application.ProductVersion + ")";
					bool flag = modGlobal.g_x < 0;
					if (flag)
					{
						base.Width = Screen.PrimaryScreen.WorkingArea.Width;
						base.Height = Screen.PrimaryScreen.WorkingArea.Height;
						base.Left = 0;
						base.Top = 0;
					}
					else
					{
						base.Width = modGlobal.g_W;
						base.Height = modGlobal.g_H;
						modGlobal.g_x = Conversions.ToInteger(Interaction.IIf(Screen.PrimaryScreen.WorkingArea.Width - modGlobal.g_W - modGlobal.g_x > 0, modGlobal.g_x, Screen.PrimaryScreen.WorkingArea.Width - modGlobal.g_W));
						modGlobal.g_x = Conversions.ToInteger(Interaction.IIf(modGlobal.g_x < 0, 0, modGlobal.g_x));
						base.Left = modGlobal.g_x;
						modGlobal.g_y = Conversions.ToInteger(Interaction.IIf(Screen.PrimaryScreen.WorkingArea.Height - modGlobal.g_H - modGlobal.g_y > 0, modGlobal.g_y, Screen.PrimaryScreen.WorkingArea.Height - modGlobal.g_H));
						modGlobal.g_y = Conversions.ToInteger(Interaction.IIf(modGlobal.g_y < 0, 0, modGlobal.g_y));
						base.Top = modGlobal.g_y;
					}
					this.subDbIniFileRead();
					this.RichTextBox1.Visible = false;
					ComboBox cobType = this.cobType;
					cobType.Items.Add("警報 *.ALM");
					cobType.Items.Add("事件 *.EVT");
					cobType.Items.Add("安全 *.LOG");
					cobType.SelectedIndex = 0;
					bool flag2 = Strings.Len(modGlobal.g_QryType) > 0;
					if (flag2)
					{
						this.cobType.Text = modGlobal.g_QryType;
						this.cobType.Enabled = false;
					}
					this.ToolTip1.SetToolTip(this.btnFind, "Find");
					this.ToolTip1.SetToolTip(this.btnFindNext, "Find Next");
					this.subDateColor();
					int num = Helper.FixIsFixRunning();
					bool flag3 = num == 1;
					if (flag3)
					{
						short num2 = 0;
						short num3 = 99;
						int num5;
						short num4 = (short)num5;
						string[] array;
						short num6 = Eda.EnumScadaNodes(ref array, ref num2, num3, ref num4);
						num5 = (int)num4;
						short num7 = num6;
						string[] array2 = new string[num5 - 1 + 1];
						bool flag4 = num7 == 0 | num7 == 100;
						if (flag4)
						{
							int num8 = num5 - 1;
							for (int i = 0; i <= num8; i++)
							{
								bool flag5 = Operators.CompareString(this.kvFixHelper.funRemoveNull(array[i]).ToUpper(), "THISNODE", false) != 0;
								if (flag5)
								{
									array2[i] = this.kvFixHelper.funRemoveNull(array[i]);
								}
							}
							Array.Sort<string>(array2);
							int num9 = num5 - 1;
							for (int i = 0; i <= num9; i++)
							{
								bool flag6 = !Information.IsNothing(array2[i]);
								if (flag6)
								{
									this.cobNode.Items.Add(array2[i]);
								}
							}
							bool flag7 = this.cobNode.Items.Count > 0;
							if (flag7)
							{
								this.cobNode.SelectedIndex = 0;
							}
						}
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message + "\r" + ex.StackTrace, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					base.Close();
				}
			}
		}

		// Token: 0x06000061 RID: 97 RVA: 0x0000490C File Offset: 0x00002B0C
		private void frmHistAlarmFile_Shown(object sender, EventArgs e)
		{
			try
			{
				modSub.subChkLicense();
				bool g_bNoTaskBar = modGlobal.g_bNoTaskBar;
				if (g_bNoTaskBar)
				{
					modHideTaskBar.subHideTaskBar(true);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Form_Shown", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Environment.Exit(0);
			}
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00004970 File Offset: 0x00002B70
		private void frmMain_KeyDown(object sender, KeyEventArgs e)
		{
			Keys keyData = e.KeyData;
			if (keyData != Keys.Return)
			{
				if (keyData == Keys.F3)
				{
					this.btnFindNext_Click(RuntimeHelpers.GetObjectValue(sender), e);
				}
			}
			else
			{
				this.btnOK_Click(RuntimeHelpers.GetObjectValue(sender), e);
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000049BC File Offset: 0x00002BBC
		private void frmHistAlarm_Closing(object sender, CancelEventArgs e)
		{
			string path = Application.StartupPath + "\\" + Application.ProductName + ".ini";
			try
			{
				bool iniChanged = this._iniChanged;
				if (iniChanged)
				{
					FileStream fileStream = File.Open(path, FileMode.Create, FileAccess.ReadWrite);
					StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.Default);
					streamWriter.WriteLine("MarginsPrinterName=" + this.DocumentPrint.PrinterSettings.PrinterName.ToString() + ";");
					streamWriter.WriteLine("MarginsLandscape=" + Conversions.ToString(this.DocumentPrint.DefaultPageSettings.Landscape) + ";");
					streamWriter.WriteLine("MarginsTop=" + Conversions.ToString(this.DocumentPrint.DefaultPageSettings.Margins.Top) + ";");
					streamWriter.WriteLine("MarginsBottom=" + Conversions.ToString(this.DocumentPrint.DefaultPageSettings.Margins.Bottom) + ";");
					streamWriter.WriteLine("MarginsLeft=" + Conversions.ToString(this.DocumentPrint.DefaultPageSettings.Margins.Left) + ";");
					streamWriter.WriteLine("MarginsRight=" + Conversions.ToString(this.DocumentPrint.DefaultPageSettings.Margins.Right) + ";");
					streamWriter.WriteLine("ForegroundColor=" + this.btnForeground.BackColor.Name + ";");
					streamWriter.WriteLine("BackgroundColor=" + this.btnBackground.BackColor.Name + ";");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			finally
			{
				StreamWriter streamWriter;
				bool flag = !Information.IsNothing(streamWriter);
				if (flag)
				{
					streamWriter.Close();
				}
				FileStream fileStream;
				bool flag2 = !Information.IsNothing(fileStream);
				if (flag2)
				{
					streamWriter.Close();
				}
			}
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00004C04 File Offset: 0x00002E04
		private void frmHistAlarmFile_FormClosed(object sender, FormClosedEventArgs e)
		{
			try
			{
				bool g_Demo = modGlobal.g_Demo;
				if (g_Demo)
				{
					string text = "Thanks for trying " + Application.ProductName + "\r\n" + modGlobal.sTrendtek;
					MessageBox.Show(text, "DEMO mode", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
				bool g_bNoTaskBar = modGlobal.g_bNoTaskBar;
				if (g_bNoTaskBar)
				{
					modHideTaskBar.subHideTaskBar(false);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Form Closed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00004C90 File Offset: 0x00002E90
		private void btnOK_Click(object sender, EventArgs e)
		{
			try
			{
				base.Enabled = false;
				this.Cursor = Cursors.WaitCursor;
				StringBuilder stringBuilder = new StringBuilder(256);
				StringBuilder stringBuilder2 = new StringBuilder(64);
				int num = Helper.FixGetPath("ALMPATH", stringBuilder2, 64);
				bool flag = num != 11000;
				if (flag)
				{
					num = Helper.NlsGetText(num, stringBuilder, 256);
					throw new Exception(stringBuilder.ToString());
				}
				string text = this.cobType.Text;
				if (Operators.CompareString(text, "警報 *.ALM", false) != 0)
				{
					if (Operators.CompareString(text, "事件 *.EVT", false) != 0)
					{
						if (Operators.CompareString(text, "安全 *.LOG", false) == 0)
						{
							this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".LOG";
						}
					}
					else
					{
						this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".EVT";
					}
				}
				else
				{
					this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".ALM";
				}
				bool flag2 = File.Exists(this._sFileName);
				bool flag3 = flag2;
				if (flag3)
				{
					bool flag4 = DateTime.Compare(this.dtStart.Value.Date, DateAndTime.Today) == 0;
					if (flag4)
					{
						File.Copy(this._sFileName, stringBuilder2.ToString() + "\\ReadOnly.TXT", true);
						this._sFileName = stringBuilder2.ToString() + "\\ReadOnly.TXT";
					}
					this.RichTextBoxAlarm.LoadFile(this._sFileName, RichTextBoxStreamType.PlainText);
				}
				else
				{
					Interaction.MsgBox(this._sFileName + "檔案不存在", MsgBoxStyle.OkOnly, null);
					this.RichTextBoxAlarm.Clear();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			finally
			{
				base.Enabled = true;
				this.Cursor = Cursors.Default;
				base.BringToFront();
			}
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00004F18 File Offset: 0x00003118
		private void btnQuit_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00004F24 File Offset: 0x00003124
		private void btnPrint_Click(object sender, EventArgs e)
		{
			bool flag = Operators.CompareString(this.RichTextBoxAlarm.Text.ToString(), "", false) == 0;
			if (flag)
			{
				Interaction.MsgBox("No data.", MsgBoxStyle.OkOnly, null);
			}
			else
			{
				try
				{
					this.RichTextBox1.Text = "";
					this._pagePrintRow = 0;
					this._PageNumber = 1;
					this.DocumentPrint.DocumentName = Application.ProductName;
					string left = ((Button)sender).Name.ToUpper();
					bool flag2 = Operators.CompareString(left, "btnPrint".ToUpper(), false) == 0;
					if (flag2)
					{
						PrintDialog print = this.Print1;
						print.Document = this.DocumentPrint;
						print.ShowNetwork = true;
						print.AllowSomePages = true;
						print.AllowSelection = true;
						this.DocumentPrint.Print();
					}
					else
					{
						flag2 = (Operators.CompareString(left, "btnPrintPreview".ToUpper(), false) == 0);
						if (flag2)
						{
							PrintPreviewDialog previewDialog = this.PreviewDialog1;
							previewDialog.Document = this.DocumentPrint;
							previewDialog.PrintPreviewControl.Zoom = 1.2;
							previewDialog.TopMost = true;
							previewDialog.ShowDialog();
						}
					}
					this.btnOK.Focus();
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
		}

		// Token: 0x06000068 RID: 104 RVA: 0x000050AC File Offset: 0x000032AC
		private void DocumentPrint_PrintPage(object sender, PrintPageEventArgs e)
		{
			checked
			{
				try
				{
					float num = 0f;
					float x = (float)e.MarginBounds.Left;
					float num2 = (float)(e.MarginBounds.Top + 60);
					int num3 = 0;
					num = (float)e.MarginBounds.Height / unchecked(this.RichTextBoxAlarm.Font.GetHeight(e.Graphics) + 1f);
					string text = this.RichTextBoxAlarm.Lines[this._intLine];
					while ((float)num3 < num & text != null)
					{
						float y = unchecked(num2 + (float)num3 * this.RichTextBoxAlarm.Font.GetHeight(e.Graphics));
						e.Graphics.DrawString(text, this.RichTextBoxAlarm.Font, Brushes.Black, x, y, new StringFormat());
						this._intLine++;
						num3++;
						bool flag = (float)num3 < num;
						if (flag)
						{
							try
							{
								text = this.RichTextBoxAlarm.Lines[this._intLine];
							}
							catch (Exception ex)
							{
								text = null;
								break;
							}
						}
					}
					e.Graphics.DrawString("Page " + Conversions.ToString(this._PageNumber), this.RichTextBoxAlarm.Font, Brushes.Gray, (float)((double)e.MarginBounds.Width / 2.0), (float)(e.MarginBounds.Bottom + 5));
					ref int ptr = ref this._PageNumber;
					this._PageNumber = ptr + 1;
					bool flag2 = Operators.CompareString(text, null, false) != 0;
					if (flag2)
					{
						e.HasMorePages = true;
					}
					else
					{
						e.HasMorePages = false;
					}
				}
				catch (Exception ex2)
				{
					MessageBox.Show(ex2.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
		}

		// Token: 0x06000069 RID: 105 RVA: 0x000052E0 File Offset: 0x000034E0
		private void btnPrinterSet_Click(object sender, EventArgs e)
		{
			checked
			{
				try
				{
					this.Print1.PrinterSettings = this.DocumentPrint.DefaultPageSettings.PrinterSettings;
					this.Print1.AllowSelection = true;
					this.Print1.AllowSomePages = true;
					bool flag = this.Print1.ShowDialog() == DialogResult.OK;
					if (flag)
					{
						this.DocumentPrint.DefaultPageSettings.PrinterSettings = this.Print1.PrinterSettings;
						this.PageSetupDialog1.PageSettings = this.DocumentPrint.DefaultPageSettings;
						bool flag2 = this.PageSetupDialog1.ShowDialog() == DialogResult.OK;
						if (flag2)
						{
							this.DocumentPrint.DefaultPageSettings = this.PageSetupDialog1.PageSettings;
							this.DocumentPrint.DefaultPageSettings.Margins.Top = (int)Math.Round(unchecked((double)this.PageSetupDialog1.PageSettings.Margins.Top * 2.54));
							this.DocumentPrint.DefaultPageSettings.Margins.Bottom = (int)Math.Round(unchecked((double)this.PageSetupDialog1.PageSettings.Margins.Bottom * 2.54));
							this.DocumentPrint.DefaultPageSettings.Margins.Left = (int)Math.Round(unchecked((double)this.PageSetupDialog1.PageSettings.Margins.Left * 2.54));
							this.DocumentPrint.DefaultPageSettings.Margins.Right = (int)Math.Round(unchecked((double)this.PageSetupDialog1.PageSettings.Margins.Right * 2.54));
						}
					}
					this._iniChanged = true;
					this.btnOK.Focus();
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
		}

		// Token: 0x0600006A RID: 106 RVA: 0x000054E4 File Offset: 0x000036E4
		private void lblDay_Click(object sender, EventArgs e)
		{
			try
			{
				string left = ((Label)sender).Name.ToUpper();
				bool flag = Operators.CompareString(left, "lblDay".ToUpper(), false) == 0;
				if (flag)
				{
					this.dtStart.Value = DateAndTime.Now.AddDays(0.0);
				}
				else
				{
					flag = (Operators.CompareString(left, "lblYesterday".ToUpper(), false) == 0);
					if (flag)
					{
						this.dtStart.Value = DateAndTime.Now.AddDays(-1.0);
					}
					else
					{
						flag = (Operators.CompareString(left, "lbl2DatsBefor".ToUpper(), false) == 0);
						if (flag)
						{
							this.dtStart.Value = DateAndTime.Now.AddDays(-2.0);
						}
						else
						{
							flag = (Operators.CompareString(left, "lblForward".ToUpper(), false) == 0);
							if (flag)
							{
								this.dtStart.Value = this.dtStart.Value.AddDays(-1.0);
							}
							else
							{
								flag = (Operators.CompareString(left, "lblBackward".ToUpper(), false) == 0);
								if (flag)
								{
									this.dtStart.Value = this.dtStart.Value.AddDays(1.0);
								}
							}
						}
					}
				}
				this.subDateColor();
				StringBuilder stringBuilder = new StringBuilder(256);
				StringBuilder stringBuilder2 = new StringBuilder(64);
				int num = Helper.FixGetPath("ALMPATH", stringBuilder2, 64);
				bool flag2 = num != 11000;
				if (flag2)
				{
					num = Helper.NlsGetText(num, stringBuilder, 256);
					throw new Exception(stringBuilder.ToString());
				}
				string text = this.cobType.Text;
				if (Operators.CompareString(text, "警報 *.ALM", false) != 0)
				{
					if (Operators.CompareString(text, "事件 *.EVT", false) != 0)
					{
						if (Operators.CompareString(text, "安全 *.LOG", false) == 0)
						{
							this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".LOG";
						}
					}
					else
					{
						this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".EVT";
					}
				}
				else
				{
					this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".ALM";
				}
				bool flag3 = File.Exists(this._sFileName);
				bool flag4 = flag3;
				if (flag4)
				{
					bool flag5 = DateTime.Compare(this.dtStart.Value.Date, DateAndTime.Today) == 0;
					if (flag5)
					{
						File.Copy(this._sFileName, stringBuilder2.ToString() + "\\ReadOnly.TXT", true);
						this._sFileName = stringBuilder2.ToString() + "\\ReadOnly.TXT";
					}
					this.RichTextBoxAlarm.LoadFile(this._sFileName, RichTextBoxStreamType.PlainText);
				}
				else
				{
					Interaction.MsgBox(this._sFileName + " 檔案不存在", MsgBoxStyle.OkOnly, null);
					this.RichTextBoxAlarm.Clear();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00005888 File Offset: 0x00003A88
		private void lblForward_Click(object sender, EventArgs e)
		{
			try
			{
				string left = ((Label)sender).Name.ToUpper();
				bool flag = Operators.CompareString(left, "lblForward".ToUpper(), false) == 0;
				if (flag)
				{
					this.dtStart.Value = this.dtStart.Value.AddDays(-1.0);
				}
				else
				{
					flag = (Operators.CompareString(left, "lblBackward".ToUpper(), false) == 0);
					if (flag)
					{
						this.dtStart.Value = this.dtStart.Value.AddDays(1.0);
					}
				}
				this.subDateColor();
				StringBuilder stringBuilder = new StringBuilder(256);
				StringBuilder stringBuilder2 = new StringBuilder(64);
				int num = Helper.FixGetPath("ALMPATH", stringBuilder2, 64);
				bool flag2 = num != 11000;
				if (flag2)
				{
					num = Helper.NlsGetText(num, stringBuilder, 256);
					throw new Exception(stringBuilder.ToString());
				}
				string text = this.cobType.Text;
				if (Operators.CompareString(text, "警報 *.ALM", false) != 0)
				{
					if (Operators.CompareString(text, "事件 *.EVT", false) != 0)
					{
						if (Operators.CompareString(text, "安全 *.LOG", false) == 0)
						{
							this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".LOG";
						}
					}
					else
					{
						this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".EVT";
					}
				}
				else
				{
					this._sFileName = stringBuilder2.ToString() + "\\" + Strings.Format(this.dtStart.Value, "yyMMdd") + ".ALM";
				}
				bool flag3 = File.Exists(this._sFileName);
				bool flag4 = flag3;
				if (flag4)
				{
					bool flag5 = DateTime.Compare(this.dtStart.Value.Date, DateAndTime.Today) == 0;
					if (flag5)
					{
						File.Copy(this._sFileName, stringBuilder2.ToString() + "\\ReadOnly.TXT", true);
						this._sFileName = stringBuilder2.ToString() + "\\ReadOnly.TXT";
					}
					this.RichTextBoxAlarm.LoadFile(this._sFileName, RichTextBoxStreamType.PlainText);
				}
				else
				{
					Interaction.MsgBox(this._sFileName + " 檔案不存在", MsgBoxStyle.OkOnly, null);
					this.RichTextBoxAlarm.Clear();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00005B64 File Offset: 0x00003D64
		private void btnFind_Click(object sender, EventArgs e)
		{
			try
			{
				this.RichTextBoxAlarm.SelectionLength = 0;
				bool flag = Operators.CompareString(this.txtTag.Text.ToString(), "", false) != 0;
				if (flag)
				{
					this._indexToText = this.RichTextBoxAlarm.Find(this.txtTag.Text.ToString());
				}
				else
				{
					bool flag2 = Operators.CompareString(this.cobNode.Text, "", false) != 0;
					if (flag2)
					{
						this._indexToText = this.RichTextBoxAlarm.Find(this.cobNode.Text.ToString());
					}
				}
				bool flag3 = this._indexToText != -1;
				if (flag3)
				{
					this.btnFindNext.Enabled = true;
					this.RichTextBoxAlarm.Focus();
					this.RichTextBoxAlarm.SelectionStart = this._indexToText;
				}
				else
				{
					this.btnFindNext.Enabled = false;
					Interaction.MsgBox("Not found", MsgBoxStyle.OkOnly, null);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00005C98 File Offset: 0x00003E98
		private void btnForeground_Click(object sender, EventArgs e)
		{
			try
			{
				ColorDialog colorDialog = this.ColorDialog1;
				object obj = NewLateBinding.LateGet(sender, null, "backColor", new object[0], null, null, null);
				colorDialog.Color = ((obj != null) ? ((Color)obj) : default(Color));
				bool flag = this.ColorDialog1.ShowDialog() == DialogResult.OK;
				if (flag)
				{
					NewLateBinding.LateSet(sender, null, "backColor", new object[]
					{
						this.ColorDialog1.Color
					}, null, null);
					this._iniChanged = true;
					bool flag2 = Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "btnForeground", false);
					if (flag2)
					{
						this.RichTextBoxAlarm.ForeColor = this.ColorDialog1.Color;
					}
					else
					{
						this.RichTextBoxAlarm.BackColor = this.ColorDialog1.Color;
					}
				}
				this.btnOK.Focus();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00005DBC File Offset: 0x00003FBC
		private void DocumentPrint_BeginPrint(object sender, PrintEventArgs e)
		{
			this._intLine = 0;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00005DC8 File Offset: 0x00003FC8
		private void btnFindNext_Click(object sender, EventArgs e)
		{
			checked
			{
				try
				{
					this._indexToText++;
					this.RichTextBoxAlarm.SelectionLength = 0;
					bool flag = Operators.CompareString(this.txtTag.Text.ToString(), "", false) != 0;
					if (flag)
					{
						this._indexToText = this.RichTextBoxAlarm.Find(this.txtTag.Text.ToString(), this._indexToText, RichTextBoxFinds.None);
					}
					else
					{
						bool flag2 = Operators.CompareString(this.cobNode.Text, "", false) != 0;
						if (flag2)
						{
							this._indexToText = this.RichTextBoxAlarm.Find(this.cobNode.Text.ToString(), this._indexToText, RichTextBoxFinds.None);
						}
					}
					bool flag3 = this._indexToText != -1;
					if (flag3)
					{
						this.btnFindNext.Enabled = true;
						this.RichTextBoxAlarm.Focus();
						this.RichTextBoxAlarm.SelectionStart = this._indexToText;
					}
					else
					{
						this.btnFindNext.Enabled = false;
						Interaction.MsgBox("Not found", MsgBoxStyle.OkOnly, null);
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "歷史警報/事件 檔案查詢程式", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00005F24 File Offset: 0x00004124
		private void dtStart_Click(object sender, EventArgs e)
		{
			this.subDateColor();
		}

		// Token: 0x0400000B RID: 11
		private clsFixHelper kvFixHelper;

		// Token: 0x0400000C RID: 12
		private clsCommon kvComm;

		// Token: 0x0400000D RID: 13
		private int _pagePrintRow;

		// Token: 0x0400000E RID: 14
		private int _PageNumber;

		// Token: 0x0400000F RID: 15
		private bool _iniChanged;

		// Token: 0x04000010 RID: 16
		private string _sFileName;

		// Token: 0x04000011 RID: 17
		private int _intLine;

		// Token: 0x04000012 RID: 18
		private int _indexToText;
	}
}
