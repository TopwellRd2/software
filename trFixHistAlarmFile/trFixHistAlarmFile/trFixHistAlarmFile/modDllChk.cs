﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace trFixHistAlarmFile
{
	// Token: 0x02000009 RID: 9
	[StandardModule]
	internal sealed class modDllChk
	{
		// Token: 0x06000071 RID: 113 RVA: 0x00005F30 File Offset: 0x00004130
		public static void subDLLCheck()
		{
			string str = Application.StartupPath + "\\";
			modDllChk.subDLLCheck(str + "GeFanuc.iFixToolkit.Adapter", "GeFanuc.iFixToolkit.Adapter.Helper");
			modDllChk.subDLLCheck(str + "kvNetClass", "kvNetClass.clsCrypt");
			modDllChk.subDLLCheck(str + "Trendtek.Library", "Trendtek.Library.Common");
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00005F90 File Offset: 0x00004190
		public static void subDLLCheck(string DllName, string ClassName)
		{
			try
			{
				Assembly assembly = Assembly.LoadFrom(DllName + ".dll");
				Type type = assembly.GetType(ClassName);
				bool flag = Information.IsNothing(type);
				if (flag)
				{
					MessageBox.Show(string.Concat(new string[]
					{
						"In the <",
						DllName,
						".dll> doesn't have <",
						ClassName,
						"> CLASS"
					}), "Class Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Environment.Exit(0);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Missing  <'" + DllName + ".dll> file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Environment.Exit(0);
			}
		}
	}
}
