﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime     'SQL連線方式

Public Class UsersWorkTable

    Dim menu As New ContextMenu
    Dim miAdd As New MenuItem("新增")
    Dim textBeforeEdit As String

    Public Sub miAdd_Click(sender As Object, e As System.EventArgs)                     '新增資料的功能
        Dim frm As modify = New modify
        frm.ShowDialog()                                                                 '彈出對話視窗(新增資料)

        'btnGetData.PerformClick()                                                        '重新讀取資料(刷新)
    End Sub

    Private Sub UsersWorkTable_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DataGridView1.VirtualMode = False
        DataGridView1.RowHeadersVisible = False                 '隱藏RowHeader
        DataGridView1.ReadOnly = False
        menu.MenuItems.Add(miAdd)                               '新增到右鍵選單menu中

        menu.MenuItems.Add(New MenuItem("取消"))


        Dim conn = New SqlConnection '設"conn"為新的SQL連線
        Dim strinsert As String
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT *"                                                          '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.UserWorkTable"                         'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                        '定義cmnd為SqlCommand指令

        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)                                                          '讀取於記憶體之Table
        DataGridView1.DataSource = set1
        DataGridView1.AutoResizeColumns()                                               '自動調整欄位寬度
        conn.Close()                                                                    '結束連線
    End Sub

    Private Sub DataGridView1_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDown
        If e.Button = MouseButtons.Right Then                                            '按下右鍵
            menu.Show(DataGridView1, New Point(e.X, e.Y))                                '顯示右鍵選單
        End If
    End Sub


    Private Sub DataGridView1_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles DataGridView1.CellBeginEdit
        If e.ColumnIndex = 0 Then
            If DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).ToString = "Null" Then
                textBeforeEdit = ""
            Else
                textBeforeEdit = DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            End If
            Return
        Else
            MessageBox.Show("僅開放姓名欄")
            e.Cancel = True
        End If
    End Sub

    Private Sub DataGridView1_RowValidated(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.RowValidated
        '''一進來就執行--->需要再確認
        Dim i, j, k As Integer
        Dim si, sj As String
        si = DataGridView1.Rows(e.RowIndex).Cells(0).Value
    End Sub
End Class