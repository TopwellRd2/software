﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime     'SQL連線方式

Public Class UsersWorkTable

    Private Sub UsersWorkTable_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conn = New SqlConnection '設"conn"為新的SQL連線
        Dim strinsert As String
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT *"                                                          '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.UserWorkTable"                         'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                        '定義cmnd為SqlCommand指令

        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)                                                          '讀取於記憶體之Table
        DataGridView1.DataSource = set1

        conn.Close()                                                                    '結束連線
    End Sub
End Class