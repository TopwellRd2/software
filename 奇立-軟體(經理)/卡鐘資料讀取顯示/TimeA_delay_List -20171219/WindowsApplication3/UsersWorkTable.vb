﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime                                                  'SQL連線方式

Public Class UsersWorkTable

    Dim menu As New ContextMenu
    Dim miAdd As New MenuItem("新增")
    Dim textBeforeEdit As String
    Dim conn = New SqlConnection                                                         '設"conn"為新的SQL連線
    Dim strinsert As String
    Dim dataAdapter As New SqlDataAdapter
    Dim set1 As DataTable = New DataTable
    Public ps(7) As String                                                               '紀錄原本資料
    Dim cs(7) As String                                                                  '紀錄修改後資料
    Public mstats As Integer = 0                                                         '0:新增 / 1:修改
    WithEvents bsCustomers As New BindingSource

    Public Sub miAdd_Click(sender As Object, e As System.EventArgs)                      '新增資料的功能
        Dim frm As modify = New modify
        frm.ShowDialog()                                                                 '彈出對話視窗(新增資料)

        'btnGetData.PerformClick()                                                       '重新讀取資料(刷新)
    End Sub

    Private Sub UsersWorkTable_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DataGridView1.VirtualMode = False
        DataGridView1.RowHeadersVisible = False                                          '隱藏RowHeader
        DataGridView1.ReadOnly = False
        menu.MenuItems.Add(miAdd)                                                        '新增到右鍵選單menu中

        menu.MenuItems.Add(New MenuItem("取消"))

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
        updata()                                                                         '更新Table顯示資料
    End Sub

    Private Sub DataGridView1_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDown
        If e.Button = MouseButtons.Right Then                                            '按下右鍵
            menu.Show(DataGridView1, New Point(e.X, e.Y))                                '顯示右鍵選單
        End If
    End Sub


    Private Sub DataGridView1_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles DataGridView1.CellBeginEdit
        '擷取目前該列資料
        Dim i As Integer
        For i = 0 To 7
            ps(i) = DataGridView1.Rows(e.RowIndex).Cells(i).Value
        Next
    End Sub

    'Private Sub DataGridView1_RowValidated(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.RowValidated
    '    '''一進來就執行--->需要再確認
    '    Dim i As Integer
    '    For i = 0 To 7
    '        cs(i) = DataGridView1.Rows(e.RowIndex).Cells(i).Value
    '    Next
    'End Sub

    Private Sub DataGridView1_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        If e.RowIndex = DataGridView1.NewRowIndex Then
            '位於最後一欄(空白列)
        Else
            For i = 0 To 7
                ps(i) = DataGridView1.Rows(e.RowIndex).Cells(i).Value
            Next
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        '新增資料
        mstats = 0
        modify.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If MessageBox.Show("**確定刪除?**", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            System.Windows.Forms.Application.Run()
        Else
            '把SQL指令先轉成字串
            strinsert = "DELETE FROM TIMEA.dbo.UserWorkTable"                            '刪除資料之之Table
            strinsert = strinsert & " WHERE UserName = '" & ps(0) & "'"                  '找出預定刪除資料
            Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                     '定義cmnd為SqlCommand指令
            cmnd.ExecuteNonQuery()                                                       '資料更新
            updata()                                                                     '更新Table顯示資料
        End If
    End Sub

    Private Sub UsersWorkTable_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        conn.Close()                                                                     '結束連線
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click    '修改資料
        mstats = 1
        modify.Show()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click    '更新Table顯示資料
        updata()
    End Sub

    Public Sub updata()                                                                  '更新Table顯示資料
        DataGridView1.DataSource = Nothing                                               '清空顯示表格
        'DataGridView1.Rows.Clear()
        set1.Clear()                                                                     '清除記憶體暫存DataTable資料

        '把SQL指令先轉成字串
        strinsert = "SELECT *"                                                           '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.UserWorkTable"                          'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " Order by UserName"                                     '按姓名排序

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                         '定義cmnd為SqlCommand指令

        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)                                                           '讀取於記憶體之Table
        DataGridView1.DataSource = set1
        DataGridView1.AutoResizeColumns()                                                '自動調整欄位寬度
    End Sub

End Class