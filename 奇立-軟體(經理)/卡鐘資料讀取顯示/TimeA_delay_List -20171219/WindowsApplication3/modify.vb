﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime     'SQL連線方式

Public Class modify

    Dim conn = New SqlConnection                                                         '設"conn"為新的SQL連線
    Dim strinsert As String

    Private Sub modify_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UsersWorkTable.mstats = 0 Then
            TextBox1.Text = "輸入員工姓名"
            TextBox2.Text = "0"
            TextBox3.Text = "0"
            TextBox4.Text = "0"
            TextBox5.Text = "0"
            TextBox6.Text = "0"
            TextBox7.Text = "0"
            TextBox8.Text = "0"
        Else
            TextBox1.Text = UsersWorkTable.ps(0)                                         '帶入預修改的欄位資料
            TextBox2.Text = UsersWorkTable.ps(1)
            TextBox3.Text = UsersWorkTable.ps(2)
            TextBox4.Text = UsersWorkTable.ps(3)
            TextBox5.Text = UsersWorkTable.ps(4)
            TextBox6.Text = UsersWorkTable.ps(5)
            TextBox7.Text = UsersWorkTable.ps(6)
            TextBox8.Text = UsersWorkTable.ps(7)
        End If
        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        conn.Close()
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Name As String
        Dim MON, TUE, WED, THU, FRI, SAT, SUN As Integer
        Name = TextBox1.Text
        MON = TextBox2.Text
        TUE = TextBox3.Text
        WED = TextBox4.Text
        THU = TextBox5.Text
        FRI = TextBox6.Text
        SAT = TextBox7.Text
        SUN = TextBox8.Text
        If MessageBox.Show("**確定新增/修改?**", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            System.Windows.Forms.Application.Run()
        Else
            If UsersWorkTable.mstats = 1 Then
                '把SQL指令先轉成字串
                strinsert = "DELETE FROM TIMEA.dbo.UserWorkTable"                                                                                 '刪除資料之之Table
                strinsert = strinsert & " WHERE UserName = '" & UsersWorkTable.ps(0) & "'"                                                        '找出預定刪除資料
                Dim cmnd1 As SqlCommand = New SqlCommand(strinsert, conn)                                                                         '定義cmnd為SqlCommand指令
                cmnd1.ExecuteNonQuery()                                                                                                           '資料更新
            End If
            '把SQL指令先轉成字串
            strinsert = "INSERT INTO TIMEA.dbo.UserWorkTable (UserName,MON,TUE,WED,THU,FRI,SAT,SUN)"                                              '預寫入之Table
            strinsert = strinsert & " Values('" & Name & "'," & MON & "," & TUE & "," & WED & "," & THU & "," & FRI & "," & SAT & "," & SUN & ")" '預定填入之資料
            Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                                                                              '定義cmnd為SqlCommand指令
            cmnd.ExecuteNonQuery()                                                                                                                '資料更新
            conn.Close()
            Me.Close()
            UsersWorkTable.updata()
        End If
    End Sub

    Private Sub modify_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        conn.Close()
    End Sub
End Class