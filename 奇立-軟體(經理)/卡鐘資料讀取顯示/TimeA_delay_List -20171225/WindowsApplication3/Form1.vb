﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime     'SQL連線方式
Imports System.Data.OleDb                   'ODBC連線方式
Imports System.Linq                        '引用Linq語言 (DataBase 使用)
Imports System.Collections.Generic

Public Class Form1

    Dim start As Boolean = False                                                        '判別是否要執行程式使用

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim index As Integer = 2014
        Dim dl As String

        For portCount = 1 To 7                                                          '選取年份建置 (2015~2021)
            index = index + 1
            ComboBox1.Items.Add(index)
        Next

        For portCount = 1 To 12                                                         '選取月份建置
            If portCount < 10 Then
                ComboBox2.Items.Add("0" & portCount)
            Else
                ComboBox2.Items.Add(portCount)
            End If
        Next

        For portCount = 1 To 31                                                         '選取日期建置
            If portCount < 10 Then
                ComboBox3.Items.Add("0" & portCount)
            Else
                ComboBox3.Items.Add(portCount)
            End If
        Next
        ComboBox1.Text = DateTime.Today.Year()                                          '套入今天之年分

        dl = DateTime.Today.Month()
        If Len(dl) = 1 Then                                                             '套入今天之月份
            ComboBox2.Text = "0" & dl
        Else
            ComboBox2.Text = dl
        End If

        dl = DateTime.Today.Day()
        If Len(dl) = 1 Then                                                             '套入今天之日期
            ComboBox3.Text = "0" & dl
        Else
            ComboBox3.Text = dl
        End If

        Timer1.Enabled = True                                                           'Timer 開啟
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            System.Windows.Forms.Application.Run()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click    '手動篩選該日之上班未刷卡資料
        Dim das As String
        das = Mid(ComboBox1.Text, 3, 2) & ComboBox2.Text & ComboBox3.Text                '合併時間格式 (yymmdd)
        gweek(das, Label8.Text)                                                          '擷取該日期之星期幾
        TwoTableSelect(das, 1)                                                           '顯示上班8:30還未刷卡資料
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click    '手動篩選該日之下班未刷卡資料
        Dim das As String
        das = Mid(ComboBox1.Text, 3, 2) & ComboBox2.Text & ComboBox3.Text                '合併時間格式 (yymmdd)
        gweek(das, Label11.Text)                                                         '擷取該日期之星期幾
        TwoTableSelect(das, 2)                                                           '顯示下班未刷卡資料
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click    '自動篩選該日與前一個工作日(有刷卡紀錄)之刷卡狀況
        Dim das As String
        das = Mid(ComboBox1.Text, 3, 2) & ComboBox2.Text & ComboBox3.Text                '合併時間格式 (yymmdd)
        gweek(das, Label8.Text)                                                          '擷取該日期之星期幾
        TwoTableSelect(das, 1)                                                           '顯示上班8:30還未刷卡資料
        cpreday(das)                                                                     '擷取上一個刷卡日期
        gweek(das, Label11.Text)                                                         '擷取該日期之星期幾
        TwoTableSelect(das, 2)                                                           '顯示昨日17:30後未刷卡資料
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click    '開啟員工上班資料表
        UsersWorkTable.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click    '連線測試
        Dim _connection As OleDbConnection = New OleDbConnection()
        Dim ConnectionString As String
        ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;"                           'ODBC設定之 Driver
        ConnectionString = ConnectionString + "Extended Properties=Paradox 5.x;"         '資料庫之類別
        ConnectionString = ConnectionString + "Data Source=Z:\;"                         '路徑
        _connection.ConnectionString = ConnectionString

        Dim conn = New SqlConnection                                                     '設"conn"為新的SQL連線
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        Try
            conn.Open()                                                                  '開啟連線
            _connection.Open()
            MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
            'MessageBox.Show("Error openning database! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        _connection.Close()                                                              '結束連線
        conn.Close()                                                                     '結束連線
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick        '於早上8:30公告今日遲到與昨日下班未刷卡名單
        Dim times As String
        times = Format(Now(), "h:m")                                                     '紀錄目前時間
        If times = "08:30" Then                                                          '8:30執行程式,並只執行一次
            If start = False Then
                Button4.PerformClick()
                start = True
            End If
        Else
            start = False
        End If
    End Sub

    Public Sub FilterSet(dates As String, status As Integer)                            'SQL Filter command , 資料帶入DataGridView物件(資料表格顯示)
        Dim conn = New SqlConnection '設"conn"為新的SQL連線
        Dim strinsert As String
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT [username] as 員工姓名"                                                     '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.Users left outer join TIMEA.dbo.HistoryBak on "        'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " dbo.HistoryBak.LogDate = '" & dates & "' and dbo.HistoryBak.State = '" & status & "' and dbo.Users.CardNo = dbo.HistoryBak.CardNo " '指定日期與上班/下班
        strinsert = strinsert & " where LogDate Is null "                                               '只顯示未刷卡資訊

        If status = 1 Then
            strinsert = strinsert & " or LogTime > 083000"
        End If
        'strinsert = strinsert & " ORDER BY dbo.Users.CardNo ,dbo.HistoryBak.LogDate"   '暫定不需要使用排序

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                        '定義cmnd為SqlCommand指令

        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)                                                          '讀取於記憶體之Table
        Dim ii As Integer
        ii = set1.Rows.Count()
        If status = 1 Then                                                              '顯示下班未刷卡名單
            'DataGridView2.DataSource = set1
            Label9.Text = set1.Rows.Count
        Else                                                                            '顯示下班未刷卡名單
            'DataGridView3.DataSource = set1
            Label12.Text = set1.Rows.Count
        End If
        conn.Close()                                                                    '結束連線
    End Sub

    Public Sub FilterSetlist(dates As String, status As Integer)                        'SQL Filter command , 資料帶入Labelb物件
        Dim conn = New SqlConnection '設"conn"為新的SQL連線
        Dim strinsert As String
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;" & _
                                "Persist Security Info=True;User ID=sa;Password=12"     '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT [username] as 員工姓名"                                                     '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.Users left outer join TIMEA.dbo.HistoryBak on "        'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " dbo.HistoryBak.LogDate = '" & dates & "' and dbo.HistoryBak.State = '" & status & _
                    "' and dbo.Users.CardNo = dbo.HistoryBak.CardNo "                                   '指定日期與上班/下班
        strinsert = strinsert & " where LogDate Is null "                                               '只顯示未刷卡資訊

        If status = 1 Then
            strinsert = strinsert & " or LogTime > 083000"
        End If

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                        '定義cmnd為SqlCommand指令

        Dim reader As SqlDataReader
        reader = cmnd.ExecuteReader

        Dim tmp As Label
        Dim index As Integer = 0
        While reader.Read()
            If status = 1 Then                                                          '顯示上班未刷卡名單
                index = index + 1
                tmp = Me.Controls("Labela" & (index).ToString)
                If index < 61 Then
                    tmp.Text = reader.Item("員工姓名").ToString
                End If
            Else                                                                        '顯示下班未刷卡名單
                index = index + 1
                tmp = Me.Controls("Labelb" & (index).ToString)
                If index < 61 Then
                    tmp.Text = reader.Item("員工姓名").ToString
                End If
            End If
        End While

        If status = 1 Then                                                              '清空預設上班未刷卡名單
            While index < 60
                index = index + 1
                tmp = Me.Controls("Labela" & (index).ToString)
                tmp.Text = ""
            End While
        Else                                                                            '清空預設下班未刷卡名單
            While index < 60
                index = index + 1
                tmp = Me.Controls("Labelb" & (index).ToString)
                tmp.Text = ""
            End While
        End If
        conn.Close()                                                                    '結束連線
    End Sub

    Public Sub preday(ByRef dates As String)                                            '取得前一天日期
        Dim conn = New SqlConnection                                                    '設"conn"為新的SQL連線
        Dim strinsert As String
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;" & _
                                "Persist Security Info=True;User ID=sa;Password=12"     '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                 '開啟連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT * "                                                         '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.HistoryBak "                           '使用之Table
        strinsert = strinsert & " where LogDate < " & dates                             '篩選小於設定之日期
        strinsert = strinsert & " order by LogDate desc"                                '依日期排序

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                        '定義cmnd為SqlCommand指令

        Dim reader As SqlDataReader
        reader = cmnd.ExecuteReader
        reader.Read()
        dates = reader.Item("LogDate").ToString
        conn.Close()                                                                    '結束連線
    End Sub

    Public Sub SQL_CSV()                                                                 '資料匯出成CSV檔 (資料來源SQL)
        Dim conn = New SqlConnection '設"conn"為新的SQL連線
        Dim strinsert As String
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;" & _
                                "Persist Security Info=True;User ID=sa;Password=12"      '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                  '開啟連線
            'MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT [username] as 員工姓名 , [logdate] as 日期 , [logtime] as 刷卡時間"    '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.Users left outer join TIMEA.dbo.HistoryBak on "   'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " Users.CardNo = HistoryBak.CardNo and LogDate like '" & Mid(ComboBox1.Text, 3, 2) & ComboBox2.Text & "%'" '指定日期與上班/下班"
        'strinsert = strinsert & " order by 員工姓名"                                    '依時間排序

        'strinsert = strinsert & " ORDER BY dbo.Users.CardNo ,dbo.HistoryBak.LogDate"   '暫定不需要使用排序

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                        '定義cmnd為SqlCommand指令

        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)                                                          '讀取於記憶體之Table

        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        Dim FileName As String = "Test.csv"
        Dim FilePath As String = SavePath + FileName
        Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)

        '寫入欄位名稱
        If set1.Columns.Count > 0 Then
            sw.Write(set1.Columns.Item(0).ColumnName.ToString)
        End If
        For i As Integer = 1 To set1.Columns.Count - 1
            sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
        Next
        sw.Write(sw.NewLine)

        '寫入各欄位資料
        For i As Integer = 0 To set1.Rows.Count - 1
            For j As Integer = 0 To set1.Columns.Count - 1
                If j = 0 Then
                    sw.Write(set1.Rows(i)(j))
                Else
                    sw.Write("," + set1.Rows(i)(j))
                End If
            Next
            sw.Write(sw.NewLine)
        Next

        sw.Close()
        conn.Close()
    End Sub

    Public Sub ODBC_CSV()                                                                '資料匯出成CSV檔 (資料來源為Pasadox Database)
        Dim strinsert As String
        Dim _connection As OleDbConnection = New OleDbConnection()
        Dim ConnectionString As String
        ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;"                           'ODBC設定之 Driver
        ConnectionString = ConnectionString + "Extended Properties=Paradox 5.x;"         '資料庫之類別
        ConnectionString = ConnectionString + "Data Source=Z:\;"                         '路徑
        _connection.ConnectionString = ConnectionString
        Try                                                                              '開啟連線
            _connection.Open()
        Catch ex As Exception
            MessageBox.Show("Error openning database! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT [username] as 員工姓名 , [logdate] as 日期 , [logtime] as 刷卡時間"                                                     '只顯示姓名資訊
        strinsert = strinsert & " From Users left join HistoryBak on ("        'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " Users.CardNo = HistoryBak.CardNo and LogDate like '" & Mid(ComboBox1.Text, 3, 2) & ComboBox2.Text & "%')" '指定日期與上班/下班"
        'strinsert = strinsert & " order by 員工姓名"                                        '依時間排序
        strinsert = strinsert & " ORDER BY Users.CardNo,HistoryBak.LogDate"
        'strinsert = strinsert & " ORDER BY dbo.Users.CardNo ,dbo.HistoryBak.LogDate"   '暫定不需要使用排序

        Dim da As OleDbDataAdapter = New OleDbDataAdapter(strinsert, _connection)       '擷取出篩選後的資料
        Dim dsRetrievedData As DataSet = New DataSet()
        da.Fill(dsRetrievedData)                                                        '擷取出的資料放到暫存TABLE
        Dim set1 As DataTable
        set1 = dsRetrievedData.Tables(0)
        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        Dim FileName As String = DateTime.Today.Year() & DateTime.Today.Month() & ".csv"
        Dim FilePath As String = SavePath + FileName
        Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)

        '寫入欄位名稱
        If set1.Columns.Count > 0 Then
            sw.Write(set1.Columns.Item(0).ColumnName.ToString)
        End If
        For i As Integer = 1 To set1.Columns.Count - 1
            sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
        Next
        sw.Write(sw.NewLine)

        '寫入各欄位資料
        For i As Integer = 0 To set1.Rows.Count - 1
            For j As Integer = 0 To set1.Columns.Count - 1
                If j = 0 Then
                    sw.Write(set1.Rows(i)(j))
                Else
                    sw.Write("," + set1.Rows(i)(j))
                End If
            Next
            sw.Write(sw.NewLine)
        Next

        sw.Close()
        _connection.Close()
    End Sub

    Public Sub PFilterSetlist(dates As String, status As Integer)                        'ODBC Driver Pasadox Filter command
        Dim strinsert As String
        Dim _connection As OleDbConnection = New OleDbConnection()
        Dim ConnectionString As String
        ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;"                           'ODBC設定之 Driver
        ConnectionString = ConnectionString + "Extended Properties=Paradox 5.x;"         '資料庫之類別
        ConnectionString = ConnectionString + "Data Source=Z:\;"                         '路徑
        _connection.ConnectionString = ConnectionString
        Try
            _connection.Open()                                                           '開啟連線
        Catch ex As Exception
            MessageBox.Show("Error openning database! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT [Users.username] as 員工姓名"                                '只顯示姓名資訊
        strinsert = strinsert & " From Users left join HistoryBak on ("                  'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " HistoryBak.LogDate = '" & dates & "' and HistoryBak.State = " & status & _
                    " and Users.CardNo = HistoryBak.CardNo) "                            '指定日期與上班/下班
        strinsert = strinsert & " where (LogDate Is null "                               '只顯示未刷卡資訊

        If status = 1 Then
            strinsert = strinsert & " or LogTime > '083000')"
        Else
            strinsert = strinsert & ")"
        End If

        Dim da As OleDbDataAdapter = New OleDbDataAdapter(strinsert, _connection)       '擷取出篩選後的資料
        Dim dsRetrievedData As DataSet = New DataSet()
        da.Fill(dsRetrievedData)                                                        '擷取出的資料放到暫存TABLE

        If status = 1 Then                                                              '顯示下班未刷卡名單(Table)
            'DataGridView2.DataSource = dsRetrievedData
            'DataGridView2.DataMember = dsRetrievedData.Tables(0).TableName
            Label9.Text = dsRetrievedData.Tables(0).Rows.Count
        Else                                                                            '顯示下班未刷卡名單(Table)
            'DataGridView3.DataSource = dsRetrievedData
            'DataGridView3.DataMember = dsRetrievedData.Tables(0).TableName
            Label12.Text = dsRetrievedData.Tables(0).Rows.Count
        End If

        Dim command As New OleDbCommand(strinsert, _connection)
        Dim reader As OleDbDataReader
        reader = command.ExecuteReader()                                                '傳送 CommandText 至 Connection 和組建 OleDbDataReader
        Dim tmp As Label                                                                '定義 Lable 型態 -> 抓取 Lable並寫入字串
        Dim index As Integer = 0

        While reader.Read()
            If status = 1 Then                                                          '顯示上班未刷卡名單(最多顯示60員)
                index = index + 1
                tmp = Me.Controls("Labela" & (index).ToString)
                If index < 61 Then
                    tmp.Text = reader.Item("員工姓名").ToString
                End If
            Else                                                                        '顯示下班未刷卡名單(最多顯示60員)
                index = index + 1
                tmp = Me.Controls("Labelb" & (index).ToString)
                If index < 61 Then
                    tmp.Text = reader.Item("員工姓名").ToString
                End If
            End If
        End While

        If status = 1 Then                                                              '清空預設上班未刷卡名單(不到60員,剩餘的清掉)
            While index < 60
                index = index + 1
                tmp = Me.Controls("Labela" & (index).ToString)
                tmp.Text = ""
            End While
        Else                                                                            '清空預設下班未刷卡名單(不到60員,剩餘的清掉)
            While index < 60
                index = index + 1
                tmp = Me.Controls("Labelb" & (index).ToString)
                tmp.Text = ""
            End While
        End If
        _connection.Close()                                                              '結束連線
    End Sub

    Public Sub cpreday(ByRef dates As String)                                            '取得前一天日期(資料來源為Pasadox Database 'HistoryBak' Table)
        Dim strinsert As String
        Dim _connection As OleDbConnection = New OleDbConnection()
        Dim ConnectionString As String
        ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;"                           'ODBC設定之 Driver
        ConnectionString = ConnectionString + "Extended Properties=Paradox 5.x;"         '資料庫之類別
        ConnectionString = ConnectionString + "Data Source=Z:\;"                         '路徑
        _connection.ConnectionString = ConnectionString
        Try                                                                              '開啟連線
            _connection.Open()
        Catch ex As Exception
            MessageBox.Show("Error openning database! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        strinsert = "SELECT LogDate "                                                    '只顯示姓名資訊
        strinsert = strinsert & " From HistoryBak "                                      '使用之Table
        strinsert = strinsert & " where LogDate < '" & dates                             '篩選小於設定之日期
        strinsert = strinsert & "' order by LogDate desc"                                '依日期排序

        Dim command As New OleDbCommand(strinsert, _connection)                          '定義command為OleDbCommand指令
        Dim reader As OleDbDataReader
        reader = command.ExecuteReader()
        reader.Read()
        dates = reader.Item("LogDate").ToString                                         '擷取"logdate"第一筆資料
        _connection.Close()                                                              '結束連線
    End Sub

    Public Sub gweek(ByRef dates As String, ByRef week As String)                              '判斷選擇日期為星期幾
        Dim day As String = "20" & Mid(dates, 1, 2) & "/" & Mid(dates, 3, 2) & "/" & Mid(dates, 5, 2)
        week = day & " - " & System.DateTime.Parse(day).DayOfWeek.ToString()
    End Sub

    Public Sub TwoTableSelect(dates As String, status As Integer)                        'ODBC Driver Pasadox Filter command & SQL Filter Command / Two Table select
        Dim strinsert As String
        Dim _connection As OleDbConnection = New OleDbConnection()
        Dim ConnectionString As String
        ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;"                           'ODBC設定之 Driver
        ConnectionString = ConnectionString + "Extended Properties=Paradox 5.x;"         '資料庫之類別
        ConnectionString = ConnectionString + "Data Source=Z:\;"                         '路徑
        _connection.ConnectionString = ConnectionString

        Try
            _connection.Open()                                                           '開啟連線
        Catch ex As Exception
            MessageBox.Show("Error openning database! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT [Users.username] as 員工姓名"                                '只顯示姓名資訊
        strinsert = strinsert & " From Users left join HistoryBak on ("                  'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " HistoryBak.LogDate = '" & dates & "' and HistoryBak.State = " & status & _
                    " and Users.CardNo = HistoryBak.CardNo) "                            '指定日期與上班/下班
        strinsert = strinsert & " where (LogDate Is null "                               '只顯示未刷卡資訊

        If status = 1 Then
            strinsert = strinsert & " or LogTime > '083000')"
        Else
            strinsert = strinsert & ")"
        End If

        Dim da As OleDbDataAdapter = New OleDbDataAdapter(strinsert, _connection)       '擷取出篩選後的資料
        Dim dsRetrievedData As DataSet = New DataSet()
        da.Fill(dsRetrievedData)                                                        '擷取出的資料放到暫存TABLE

        Dim conn = New SqlConnection                                                    '設"conn"為新的SQL連線
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=TIMEA;Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        Try
            conn.Open()                                                                 '開啟連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try

        '把SQL指令先轉成字串
        strinsert = "SELECT *"                                                           '只顯示姓名資訊
        strinsert = strinsert & " From TIMEA.dbo.UserWorkTable"                          'User Table為基準 填入 刷卡紀錄(無資料會有Null資訊)
        strinsert = strinsert & " Order by UserName"                                     '按姓名排序

        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                         '定義cmnd為SqlCommand指令
        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)                                                           '讀取於記憶體之Table

        '日期轉換成yyyy/mm/dd->轉成星期->換成大寫截取前三碼
        Dim week As String = "20" & Mid(dates, 1, 2) & "/" & Mid(dates, 3, 2) & "/" & Mid(dates, 5, 2)
        week = System.DateTime.Parse(week).DayOfWeek.ToString().ToUpper
        week = Mid(week, 1, 3)

        '兩個虛擬資料庫做篩選動作,Linq語法
        Dim tdt As DataTable
        tdt = dsRetrievedData.Tables(0)
        Dim query = _
        From order In tdt.AsEnumerable() _
        Join detail In set1.AsEnumerable() _
        On order.Field(Of String)("員工姓名") Equals _
        detail.Field(Of String)("UserName") _
        Where detail.Field(Of Integer)(week) = 1

        Dim tmp As Label
        Dim index As Integer = 0
        Dim names(88) As String
        If status = 1 Then                                                              '顯示下班未刷卡名單(Table)
            Label9.Text = query.Count
        Else                                                                            '顯示下班未刷卡名單(Table)
            Label12.Text = query.Count
        End If
        For i = 0 To query.Count - 1
            names(index) = query.ToArray(i).detail.ItemArray(0).ToString
            index = index + 1
            If status = 1 Then                                                          '顯示上班未刷卡名單(最多顯示60員)
                tmp = Me.Controls("Labela" & (index).ToString)
                If index < 61 Then
                    tmp.Text = query.ToArray(i).detail.ItemArray(0).ToString
                End If
            Else                                                                        '顯示下班未刷卡名單(最多顯示60員)
                tmp = Me.Controls("Labelb" & (index).ToString)
                If index < 61 Then
                    tmp.Text = query.ToArray(i).detail.ItemArray(0).ToString
                End If
            End If
        Next

        If status = 1 Then                                                              '清空預設上班未刷卡名單(不到60員,剩餘的清掉)
            While index < 60
                index = index + 1
                tmp = Me.Controls("Labela" & (index).ToString)
                tmp.Text = ""
            End While
        Else                                                                            '清空預設下班未刷卡名單(不到60員,剩餘的清掉)
            While index < 60
                index = index + 1
                tmp = Me.Controls("Labelb" & (index).ToString)
                tmp.Text = ""
            End While
        End If
        _connection.Close()                                                              '結束連線
        conn.Close()                                                                     '結束連線
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click                    '把查詢資料匯出至檔案
        Dim SavePath As String = "C:\Temp\"
        Dim pday As String = Mid(Label11.Text, 1, 4) & Mid(Label11.Text, 6, 2) & Mid(Label11.Text, 9, 2) '篩選出日期yyyymmdd
        Dim day As String = Mid(Label8.Text, 1, 4) & Mid(Label8.Text, 6, 2) & Mid(Label8.Text, 9, 2)     '篩選出日期yyyymmdd
        Dim FileName As String = pday & "_" & day & ".csv"                                               '結合前後日期為當檔案名稱
        Dim FilePath As String = SavePath + FileName
        Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)              '建立CSV檔

        sw.Write(Label11.Text)                                                                           '寫入時間
        sw.Write(sw.NewLine)
        sw.Write(Label13.Text)                                                                           '寫入刷卡狀態
        sw.Write(sw.NewLine)
        Dim tmp As Label
        For i As Integer = 1 To Label9.Text - 1
            tmp = Me.Controls("Labela" & (i).ToString)
            If i = 1 Then                                                                                '資料以一列表示
                sw.Write(tmp.Text)
            Else
                sw.Write("," + tmp.Text)
            End If
        Next
        sw.Write(sw.NewLine)                                                                             '換行
        sw.Write(sw.NewLine)                                                                             '換行(為了兩個狀態有一個區隔)

        sw.Write(Label8.Text)                                                                            '寫入時間
        sw.Write(sw.NewLine)
        sw.Write(Label10.Text)                                                                           '寫入刷卡狀態
        sw.Write(sw.NewLine)
        For i As Integer = 1 To Label12.Text - 1
            tmp = Me.Controls("Labelb" & (i).ToString)
            If i = 1 Then                                                                                '資料以一列表示
                sw.Write(tmp.Text)
            Else
                sw.Write("," + tmp.Text)
            End If
        Next
        sw.Close()                                                                                       '解除檔案鎖定
    End Sub
End Class
