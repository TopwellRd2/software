using System;
using System.Data;
using TQuark.Lib.Data;
using TQuark.Lib.Attendance;
using TQuark.Lib.Attendance.Server;
using TQuark.Lib.Attendance.Server.RecordHandler;
using TQuark.Module.Guard.Door;

namespace TQuark.OG.PlugIn.Sample
{
	/// <summary>
    /// RecordHandler sample that writting Normal attendance records to table.
	/// </summary>
	public class SampleDbRecordHandler : IRecordArrivedHandler
	{
		#region IRecordArrivedHandler Member

        /// <summary>
        /// When the attendance Record into the system, this Function will be called
        /// </summary>
        /// <param name="context">Attendance Record processing data</param>
        /// <param name="device">Device of Attendance Record is Readed.
        /// <param name="record">The original Attendance Record</param>
        void IRecordArrivedHandler.RecordArrived(RecordHandlerContext context, IComboAttendanceDevice device, TQuark.Lib.Attendance.AttendanceRecord record)
		{
            // You can get All data from record or SessionParameter.
            DoorSessionParamsAgent SessionParameter = new DoorSessionParamsAgent(context.SessionParams);
            // If record is valid, this process will be stop.
            if (!SessionParameter.IsValidCard)
                return;

            // If you only need to record some device id, you can uncomment The following directives
            //if (record.DeviceID != "01")
            //	return;

            // Getting Connection, it will Enlist in Record-Arrived-Process Transaction Control
            IDbConnection Connection = RecordHandlerHelp.GetConnection(context);
            // Getting Command that enlist in TransactionControl
            IDbCommand Command = DBTransactionMonitor.Default.GetTransactionControl(Connection).CreateCommand(string.Empty);

            // Write date to database by SQL command.
            // You can use any method to set the Command
            Command.CommandText = string.Format(@"
                insert into SampleIOLog(t_WorkerNo, t_FunctionCode, t_DeviceID, t_Time, t_LogTime) 
                values('{0}', '{1}', '{2}', '{3}', '{4}')",
                SessionParameter.WorkerInfo.WorkerNo,
                record.Function,
                record.DeviceID,
                record.Time.ToString("yyyy/MM/dd HH:mm"),
                DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
            Command.CommandType = CommandType.Text;
            Command.ExecuteNonQuery();
            Command.Dispose();
        }

		#endregion

		private string m_Name = "SampleDbRecordHandler";

        #region _IRecordArrivedHandler Member

        /// <summary>
        /// This Name is Identifier of RecordHandler, which users can customize, but the value must be a unique System wise.
		/// </summary>
		string _IRecordArrivedHandler.Name
		{
			get	{ return m_Name;}
			set	{ m_Name = value;}
		}

		#endregion
	}
}
