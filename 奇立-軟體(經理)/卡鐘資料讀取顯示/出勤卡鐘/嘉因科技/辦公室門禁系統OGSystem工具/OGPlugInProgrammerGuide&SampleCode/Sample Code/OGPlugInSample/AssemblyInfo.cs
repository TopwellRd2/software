using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("TQuark.OG.PlugIn.Sample")]
[assembly: AssemblyDescription("Office Guard Lite System PlugIn Sample")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TQuark Information Co., LTD. ")]
[assembly: AssemblyProduct("Office Guard Lite System")]
[assembly: AssemblyCopyright("Copyright.2009 - TQuark Information")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("1.03.011.00")]

[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]

[assembly: TQuark.Lib.Core.Module.TQuarkAssemblyPlugIn()]