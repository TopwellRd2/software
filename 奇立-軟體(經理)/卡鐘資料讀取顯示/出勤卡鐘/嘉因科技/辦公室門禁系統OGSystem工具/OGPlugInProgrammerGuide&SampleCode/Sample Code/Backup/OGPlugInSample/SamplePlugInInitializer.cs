using System;
using System.Data;
using TQuark.Lib.Data;
using TQuark.Lib.Core.Module;
using TQuark.Lib.Attendance.Server;
using TQuark.Module.Guard.Door;
using TQuark.OGL.PlugInCommon;

namespace TQuark.OG.PlugIn.Sample
{
	/// <summary>
	/// PlugIn Initialization lass�C
	/// </summary>
	public class SamplePlugInInitializer : OGLPlugInInitializer, IPlugInInitializer
	{
		/// <summary>
        /// Initialize this PlugIn, and modify processing flow Server
		/// </summary>
		/// <param name="initSystem"></param>
		/// <param name="userParam"></param>
		protected override void InitPlugIn(AttendanceServerInitedByDoorGuardSystem initSystem, object userParam)
		{
            AttendanceServer Server = initSystem.AttendanceServer;
            // Transaction Control
            IDBTransactionControl TransactionControl = null;

            // Use Microsoft SQL Server Connection 
            // ConnectionString 
            // data source : SQL Server Name 
            // initial catalog : Database Name
            // User: Login Account 
            // Password : Login Password 
            string ConnectionString = "data source=(Local);initial catalog=SampleIOLog;User=sa;Password=TQUARK";
            IDbConnection Connection = new System.Data.SqlClient.SqlConnection(ConnectionString);

            // Use ODBC Connection
            // If you use ODBC Connection, you can uncomment The following directives
            //string ConnectionString = "DSN=PlugInSample;Uid=sa;Pwd=TQUARK;";
            //IDbConnection Connection = new System.Data.Odbc.OdbcConnection(ConnectionString);

            Connection.Open();
            DBTransactionMonitor.Default.HookConnection(Connection, DBTransactionMonitingType.NestedTransaction);
            TransactionControl = DBTransactionMonitor.Default.GetTransactionControl(Connection);

            RecordHandlerBindContext BindContext = new RecordHandlerBindContext();
            // Setting Transaction To Context
            BindContext.TransactionControl = TransactionControl;
            // Set Running Step at This PlugIn
            BindContext.HandlePhase = HandlerExecutionPhase.Process;
            // When the Exception occurred, the records remain in the exception handling
            BindContext.KeepRecordAtException = true;
            // When the Exception occurred, the future RecordHandler does not perform
            BindContext.ExceptionPathDecider = new _RecordHandlerBindContext.ExceptionAlwaysAbortDecider();
            // This RecordHandler Apply 
            // 1. RecordHandlerFlowTypes.All : All Record
            // 2. RecordHandlerFlowTypes.NormalRecord : Normal Record
            // 3. RecordHandlerFlowTypes.IllegalRecord : Illegal Record
            BindContext.RecordHandlerFlow = RecordHandlerFlowTypes.NormalRecord;
            // Join the RecordHandler to the system
            Server.AddRecordHandler(BindContext, new SampleDbRecordHandler(), null);
        }
	}
}
