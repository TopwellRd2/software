﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime     'SQL連線方式

Public Class Form1
    Dim conn = New SqlConnection                                                         '設"conn"為新的SQL連線
    Dim strinsert As String
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ''預設顯示 Parity "Even"
        ComboBox1.Items.Add("AC")
        ComboBox1.Items.Add("DC")
        ComboBox1.Items.Add("EBM")
        ComboBox1.SelectedIndex = 0

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=" & ComboBox1.Text & ";Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        Try
            conn.Open()                                                                                                                          '開啟SQL連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        Dim table As String = ComboBox1.Text
        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=" & table & ";Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        Try
            conn.Open()                                                                  '開啟連線
            MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
            'MessageBox.Show("Error openning database! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        conn.Close()                                                                     '結束連線
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        strinsert = "SELECT * "
        strinsert = strinsert & "FROM [" & ComboBox1.Text & "].[dbo].[" & ComboBox1.Text & "] "                                                      '資料來源
        strinsert = strinsert & "where 產品代碼 like '%" & TextBox0.Text & "%' "                                                                      '案號篩選
        'strinsert = strinsert & "Order by [日期]asc,[時間]asc"                                                                                       '時間排序遞增
        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)
        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)

        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        Dim FileName As String = DateTime.Today.Year()
        ''為了使輸出檔案名稱為yyyymmdd.csv
        Dim dl As String
        dl = DateTime.Today.Month()
        If Len(dl) = 1 Then                                                             '套入今天之月份
            FileName = FileName & "0" & dl
        Else
            FileName = FileName & dl
        End If
        dl = DateTime.Today.Day()
        If Len(dl) = 1 Then                                                             '套入今天之日期
            FileName = FileName & "0" & dl & ".csv"
        Else
            FileName = FileName & dl & ".csv"
        End If
        Dim FilePath As String = SavePath + FileName

        Try
            Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)                                                  '建立CSV檔
            '寫入欄位名稱
            If set1.Columns.Count > 0 Then
                sw.Write(set1.Columns.Item(0).ColumnName.ToString)
            End If
            For i As Integer = 1 To set1.Columns.Count - 1
                sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
            Next
            sw.Write(sw.NewLine)

            '寫入各欄位資料
            For i As Integer = 0 To set1.Rows.Count - 1
                For j As Integer = 0 To set1.Columns.Count - 1
                    If j = 0 Then
                        sw.Write(set1.Rows(i)(j))
                    Else
                        sw.Write("," + set1.Rows(i)(j).ToString)
                    End If
                Next
                sw.Write(sw.NewLine)
            Next
            sw.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "檔案已開啟")
        End Try
    End Sub
End Class
