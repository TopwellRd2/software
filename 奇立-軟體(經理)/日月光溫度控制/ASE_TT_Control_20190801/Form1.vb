﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.ComponentModel
Imports OPCAutomation
Imports System.Text
Imports System.Runtime.InteropServices

Public Class Form1

    Dim WithEvents AnOPCServer As OPCAutomation.OPCServer                                'OPC Server
    Dim WithEvents ObjOPCServer As OPCAutomation.OPCServer
    Dim WithEvents ObjOPCGroup As OPCAutomation.OPCGroup
    Public GlobalOPCItems(0 To 45) As OPCItem                                             '需要讀寫之TAG

    Dim sint As Integer = 0                                                              '是否自動記錄資料旗標,0/自動;1/不自動記錄
    Dim rpm, rpml, rpmh As Integer                                                       'rpm-設定轉速 / rpml-設定轉速之下限  / rpmh-設定轉速之上限

    Public FFUS(45) As String
    Public FFUSR(45) As Integer
    Public Sensor(3) As String

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        '視窗關閉再次去認功能,"在單一執行緒上啟動第二個訊息迴圈不是有效的作業。請使用 Form.ShowDialog 代替,待處理-20180208"''
        '程式搬到FromClosing 並把Application.Run() -> e.Cancel = True即可
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        '視窗關閉再次去認功能,"在單一執行緒上啟動第二個訊息迴圈不是有效的作業。請使用 Form.ShowDialog 代替,待處理-20180208"''
        '程式搬到FromClosing 並把Application.Run() -> e.Cancel = True即可
    End Sub

    '宣告方法:
    '* lpAppName：指向包含Section 名稱的字符串地址
    '* lpKeyName：指向包含Key 名稱的字符串地址
    '* lpDefault：如果Key 值沒有找到，缺省返回缺省的字符串
    '* lpReturnedString：用於保存返回字符串的緩衝區
    '* nSize： 緩衝區的長度
    '* lpFileName ：ini 文件的文件名

    Public Declare Function GetPrivateProfileString Lib "kernel32" _
     Alias "GetPrivateProfileStringA" ( _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpApplicationName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpDefault As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpReturnedString As StringBuilder, _
     ByVal nSize As UInt32, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As UInt32
    Public Declare Function WritePrivateProfileString Lib "kernel32" _
     Alias "WritePrivateProfileStringA" ( _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpApplicationName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpReturnedString As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As UInt32

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '顯示Local端的OPC SERVER
        Try
            Dim GlobalOPCServer = New OPCAutomation.OPCServer()                          '嘗試擷取本機所有的OPCServer

            Dim ServerList As Object = GlobalOPCServer.GetOPCServers
            For index As Short = LBound(ServerList) To UBound(ServerList)                '加入控件列表中，注意這裡使用 LBound 和 UBound
                ComboBox1.Items.Add(ServerList(index))
            Next
            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If
            GlobalOPCServer = Nothing
        Catch Ex As Exception
            MessageBox.Show("List OPC servers failed: " + Ex.Message, "OPCSample", MessageBoxButtons.OK)
        End Try

        RPMMS1.Text = 100                                                                '初始 轉速級距
        RPMAS1.Text = 10                                                                 '初始 追載之轉速
        RPMF1.Text = 30                                                                  '初始 調整頻率

        '設定需要調整之FFU TAG
        '讀出ini檔,區段裡的值:
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\ffu.ini"
        '參數一 Section Name
        '參數二 於.ini中的項目
        '參數三 項目的內容
        '參數四 .ini檔的名稱
        '參數五 大小
        '參數六 路徑


        '讀取OPCSERVER資料
        GetPrivateProfileString("OPCSERVER", "OS1", "", sKeyValue, nSize, sinifilename)
        ComboBox1.Text = sKeyValue.ToString

        '讀取FFU RPMS Tag 設定資料
        Dim fs As String
        For i = 1 To 45
            fs = "FFU" + i.ToString
            GetPrivateProfileString("FFUS", fs, "", sKeyValue, nSize, sinifilename)
            FFUS(i) = sKeyValue.ToString
        Next

        For i = 1 To 3
            fs = "Sensor" + i.ToString
            GetPrivateProfileString("Sensor", fs, "", sKeyValue, nSize, sinifilename)
            Sensor(i) = sKeyValue.ToString
        Next
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick        'timer自動執行程式
        Dim rpmt As Integer
        Dim srpm As Integer
        ReadData(srpm)                                                                   '先讀取目前之設定轉速
        If TTS1.Text < TT1.Text Then                                                     '設定溫度小於實際溫度,執行加轉,加轉判別有無超過上限
            If rpmh <= srpm + RPMAS1.Text Then
                If RPMAS1.Text <> rpmh Then
                    rpmt = rpmh
                End If
            Else
                rpmt = srpm + RPMAS1.Text
            End If
        Else
            If rpml >= srpm - RPMAS1.Text Then
                If RPMAS1.Text <> rpml Then
                    rpmt = rpml
                End If
            Else
                rpmt = srpm - RPMAS1.Text
            End If
        End If
        'WriteData(rpmt)                                                                  '條件判別後寫入需要的轉速
        For i = 1 To 45
            WriteDatat(i, FFUS(i), rpmt)
        Next
        ReadData(RPMS1.Text)                                                             '讀取以寫入之轉速確認是否有調整
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click    '搜尋LOCAL OPC-SERVER
        ComboBox1.Items.Clear()
        Try
            Dim GlobalOPCServer = New OPCAutomation.OPCServer()

            Dim ServerList As Object = GlobalOPCServer.GetOPCServers
            For index As Short = LBound(ServerList) To UBound(ServerList)                '加入控件列表中，注意這裡使用 LBound 和 UBound
                ComboBox1.Items.Add(ServerList(index))
            Next
            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If
            GlobalOPCServer = Nothing
        Catch Ex As Exception
            MessageBox.Show("List OPC servers failed: " + Ex.Message, "OPCSample", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(0) = ObjOPCGroup.OPCItems.AddItem(Sensor(1), 0)
                GlobalOPCItems(1) = ObjOPCGroup.OPCItems.AddItem(Sensor(2), 1)
                GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem(FFUS(1), 2)
                GlobalOPCItems(0).Read(OPCAutomation.OPCDataSource.OPCDevice)
                GlobalOPCItems(1).Read(OPCAutomation.OPCDataSource.OPCDevice)
                GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
                Button8.PerformClick()
                Button1.PerformClick()
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click    '顯示資料
        TT1.Text = GlobalOPCItems(0).Value
        TTS1.Text = GlobalOPCItems(1).Value
        RPMS1.Text = GlobalOPCItems(2).Value
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click    '設定轉速可以調整之範圍
        rpm = GlobalOPCItems(2).Value
        rpml = GlobalOPCItems(2).Value - RPMMS1.Text
        rpmh = GlobalOPCItems(2).Value + RPMMS1.Text
        RPMR1.Text = rpml & "~" & rpmh
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click    '設定調整之頻率
        Timer1.Interval = RPMF1.Text * 60000
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click    '切換是否要執行自動加減載
        If sint = 0 Then                                                                 '切換成Auto modle
            sint = 1
            Timer1.Enabled = True
            Label13.Text = "Auto modle"
        Else                                                                             '切換成Manually modle
            sint = 0
            Timer1.Enabled = False
            Label13.Text = "Manually modle"
        End If
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick       '單純顯示時間之計時器
        Label8.Text = DateTime.Now
    End Sub

    '################################################################
    '########## Sub procedure to read OPC Item values ##############

    Private Sub ReadData(ByRef srpm As Integer)
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_01_01_01_SETSPD.F_CV", 2)
                GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
                srpm = GlobalOPCItems(2).Value
                ObjOPCServer.Disconnect()                                                '連線資料讀取寫入後中斷連線,不中斷後續執行會產生錯誤
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

    Private Sub WriteData(ByVal rpm As Integer)
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_01_01_01_SETSPD.F_CV", 2)
                GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
                GlobalOPCItems(2).Write(rpm)
                ObjOPCServer.Disconnect()                                                '連線資料讀取寫入後中斷連線,不中斷後續執行會產生錯誤
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

    Private Sub ReadDatat(ByVal i As Integer, ByVal srpm As String, ByRef rpms As Integer)
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(i) = ObjOPCGroup.OPCItems.AddItem(srpm, i)
                GlobalOPCItems(i).Read(OPCAutomation.OPCDataSource.OPCDevice)
                rpms = GlobalOPCItems(i).Value
                ObjOPCServer.Disconnect()                                                '連線資料讀取寫入後中斷連線,不中斷後續執行會產生錯誤
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

    Private Sub WriteDatat(ByVal i As Integer, ByVal srpm As String, ByVal rpm As Integer)
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(i) = ObjOPCGroup.OPCItems.AddItem(srpm, i)
                GlobalOPCItems(i).Write(rpm)
                ObjOPCServer.Disconnect()                                                '連線資料讀取寫入後中斷連線,不中斷後續執行會產生錯誤
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

End Class
