﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.ComponentModel
Imports OPCAutomation

Public Class Form1

    Dim sint As Integer = 0                                                              '是否自動記錄資料旗標,0/自動;1/不自動記錄

    Dim WithEvents AnOPCServer As OPCAutomation.OPCServer
    Dim WithEvents ConnectedOPCServer As OPCAutomation.OPCServer
    Dim WithEvents ConnectedGroup As OPCAutomation.OPCGroup
    Dim WithEvents ObjOPCServer As OPCAutomation.OPCServer
    Dim WithEvents ObjOPCGroups As OPCAutomation.OPCGroups
    Dim WithEvents ObjOPCGroup As OPCAutomation.OPCGroup
    Dim ObjOPCItems As OPCAutomation.OPCItems
    Dim ObjOPCItem As OPCAutomation.OPCItem
    Const NoOfItems = 2
    Dim Array_Items(NoOfItems) As String
    Dim Array_ClientHanlers(NoOfItems) As Integer
    Dim Array_ServerHandlers(NoOfItems) As Integer
    Dim Array_Values(NoOfItems) As Object
    Public GlobalOPCItems(0 To 3) As OPCItem
    Dim rpm, rpml, rpmh As Integer

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ' ''視窗關閉再次去認功能,"在單一執行緒上啟動第二個訊息迴圈不是有效的作業。請使用 Form.ShowDialog 代替,待處理-20180208"''
        '程式搬到FromClosing 並把Application.Run() -> e.Cancel = True即可
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ' ''視窗關閉再次去認功能,"在單一執行緒上啟動第二個訊息迴圈不是有效的作業。請使用 Form.ShowDialog 代替,待處理-20180208"''
        '程式搬到FromClosing 並把Application.Run() -> e.Cancel = True即可

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '顯示Local端的OPC SERVER
        Try
            Dim GlobalOPCServer = New OPCAutomation.OPCServer()

            Dim ServerList As Object = GlobalOPCServer.GetOPCServers
            For index As Short = LBound(ServerList) To UBound(ServerList) '加入控件列表中，注意这里使用 LBound 和 UBound
                ComboBox1.Items.Add(ServerList(index))
            Next
            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If
            GlobalOPCServer = Nothing
        Catch Ex As Exception
            MessageBox.Show("List OPC servers failed: " + Ex.Message, "OPCSample", MessageBoxButtons.OK)
        End Try
        RPMMS1.Text = 100
        RPMAS1.Text = 10
        RPMF1.Text = 30

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick                                                                 'timer自動執行程式

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click    '搜尋LOCAL OPC SERVER
        Try
            Dim GlobalOPCServer = New OPCAutomation.OPCServer()

            Dim ServerList As Object = GlobalOPCServer.GetOPCServers
            For index As Short = LBound(ServerList) To UBound(ServerList) '加入控件列表中，注意这里使用 LBound 和 UBound
                ComboBox1.Items.Add(ServerList(index))
            Next
            If ComboBox1.Items.Count > 0 Then
                ComboBox1.SelectedIndex = 0
            End If
            GlobalOPCServer = Nothing
        Catch Ex As Exception
            MessageBox.Show("List OPC servers failed: " + Ex.Message, "OPCSample", MessageBoxButtons.OK)
        End Try
    End Sub

    'Private Sub StartOPCClient(ByVal OPCServerName As String)
    '    Dim liCounter As Integer
    '    Dim gpServerHandlers As Array
    '    Dim gsErrors As Array
    '    Try
    '        ObjOPCServer = New OPCAutomation.OPCServer
    '        '##### Initialize OPC Server ################################
    '        ObjOPCServer.Connect(OPCServerName, "")
    '        ObjOPCGroups = ObjOPCServer.OPCGroups
    '        ObjOPCGroup = ObjOPCGroups.Add("OPCGroup1")
    '        'Add OPCGroup
    '        ObjOPCGroup.UpdateRate = 1000
    '        ObjOPCGroup.IsActive = False
    '        ObjOPCGroup.IsSubscribed = ObjOPCGroup.IsActive
    '        ObjOPCItems = ObjOPCGroup.OPCItems
    '        'Build OPCItems Array
    '        For liCounter = 1 To NoOfItems
    '            Array_Items(liCounter) = "OPCClient.Device" & liCounter
    '            Array_ClientHanlers(liCounter) = liCounter
    '        Next
    '        'Add OPCItems
    '        ObjOPCItems.AddItems(NoOfItems, Array_Items, Array_ClientHanlers, gpServerHandlers, gsErrors)
    '        'Get the server handlers
    '        For liCounter = 1 To NoOfItems
    '            If gsErrors(liCounter) = 0 Then
    '                Array_ServerHandlers(liCounter) = gpServerHandlers(liCounter)
    '            Else
    '                MsgBox("Item" & liCounter & "has problem", _
    '                       MsgBoxStyle.Critical, "OPC Client")
    '            End If
    '        Next
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "OPC Client")
    '    End Try
    'End Sub

    '################################################################
    '########## Sub procedure to read OPC Item values ##############

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(0) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_S01_TT.F_CV", 0)
                GlobalOPCItems(1) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_S01_RH.F_CV", 1)
                GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_01_01_01_SETSPD.F_CV", 2)
                GlobalOPCItems(3) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.TEST_AA.F_CV", 3)
                GlobalOPCItems(0).Read(OPCAutomation.OPCDataSource.OPCDevice)
                GlobalOPCItems(1).Read(OPCAutomation.OPCDataSource.OPCDevice)
                GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
                'GlobalOPCItems(2).Write(10)
                'GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        'GlobalOPCItems(0) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_S01_TT.F_CV", 0)
        'GlobalOPCItems(1) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_S01_RH.F_CV", 1)
        'GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_01_01_01_SETSPD.F_CV", 2)
        'GlobalOPCItems(3) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.TEST_AA.F_CV", 3)
        'GlobalOPCItems(0).Read(OPCAutomation.OPCDataSource.OPCDevice)
        'GlobalOPCItems(1).Read(OPCAutomation.OPCDataSource.OPCDevice)
        'GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
        TT1.Text = GlobalOPCItems(0).Value
        TTS1.Text = GlobalOPCItems(1).Value
        RPMS1.Text = GlobalOPCItems(2).Value
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        rpm = GlobalOPCItems(2).Value
        rpml = GlobalOPCItems(2).Value - RPMMS1.Text
        rpmh = GlobalOPCItems(2).Value + RPMMS1.Text
        RPMR1.Text = rpml & "~" & rpmh
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Timer1.Interval = RPMF1.Text * 1000
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If sint = 0 Then                                                            '切換成Auto modle
            sint = 1
            Timer1.Enabled = True
            Label13.Text = "Auto modle"
        Else                                                                             '切換成Manually modle
            sint = 0
            Timer1.Enabled = False
            Label13.Text = "Manually modle"
        End If
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Label8.Text = DateTime.Now
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim rpmt As Integer
        Dim srpm As Integer
        ReadData(srpm)
        If TTS1.Text < TT1.Text Then  '%%%設定溫度小於實際溫度,執行加轉,加轉判別有無超過上限)
            If rpmh <= srpm + RPMAS1.Text Then
                If RPMAS1.Text <> rpmh Then
                    rpmt = rpmh
                End If
            Else
                rpmt = srpm + RPMAS1.Text
            End If
        Else
            If rpml >= srpm - RPMAS1.Text Then
                If RPMAS1.Text <> rpml Then
                    rpmt = rpml
                End If
            Else
                rpmt = srpm - RPMAS1.Text
            End If
        End If
        WriteData(rpmt)
        ReadData(RPMS1.Text)
        'RPMS1.Text = srpm
    End Sub

    Private Sub ReadData(ByRef srpm As Integer)
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_01_01_01_SETSPD.F_CV", 2)
                GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
                srpm = GlobalOPCItems(2).Value
                ObjOPCServer.Disconnect()
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

    Private Sub WriteData(ByVal rpm As Integer)
        If ComboBox1.Text <> "" Then
            ObjOPCServer = New OPCAutomation.OPCServer
            Try
                ObjOPCServer.Connect(ComboBox1.Text)
                'Set property for Group connection
                ObjOPCServer.OPCGroups.DefaultGroupIsActive = True
                ObjOPCServer.OPCGroups.DefaultGroupDeadband = 0
                'Add group
                ObjOPCGroup = ObjOPCServer.OPCGroups.Add
                ObjOPCGroup.UpdateRate = 3 * 1000
                ObjOPCGroup.IsSubscribed = True
                'Add items
                GlobalOPCItems(2) = ObjOPCGroup.OPCItems.AddItem("C1_FFUAB.C1_FFU_3F_L30_01_01_01_SETSPD.F_CV", 2)
                GlobalOPCItems(2).Read(OPCAutomation.OPCDataSource.OPCDevice)
                GlobalOPCItems(2).Write(rpm)
                ObjOPCServer.Disconnect()
            Catch ex As Exception
                ObjOPCServer = Nothing
                MessageBox.Show("OPC server connect failed : " + ex.Message, "OPCSample", MessageBoxButtons.OK)
            End Try
        End If
    End Sub

End Class
