﻿Imports System.Xml
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.Net
Imports System.Net.Sockets

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim xdoc As XmlDocument
        Dim xElement As XmlElement
        Dim xChildElement As XmlElement
        Dim xElement2 As XmlElement
        'Dim xChildElement2 As XmlElement (暫定無第二層無子層需求)
        Dim xElement3 As XmlElement
        Dim xChildElement3 As XmlElement
        Dim xElement4 As XmlElement
        Dim xChildElement4 As XmlElement
        Try
            '建立一個 XmlDocument 物件並加入 Declaration
            xdoc = New XmlDocument
            xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", "yes"))

            '建立根節點物件並加入 XmlDocument 中 (第0層)
            xElement = xdoc.CreateElement("try")
            xdoc.AppendChild(xElement)

            '第1層節點try
            xChildElement = xdoc.CreateElement("trx_name")
            xChildElement.InnerText = "BUContinuousRunRpt"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("type_id")
            xChildElement.InnerText = "I"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("abbr_no")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("chamber_id")
            xChildElement.InnerText = "A"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("data_clock")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("eqp_id")
            xChildElement.InnerText = "A2CVD10"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("eqp_run_id")
            xElement.AppendChild(xChildElement)

            '第2層節點trace_sets
            xElement2 = xdoc.CreateElement("trace_sets")
            xElement.AppendChild(xElement2)

            '第3層節點trace_set
            xElement3 = xdoc.CreateElement("trace_set")
            xElement2.AppendChild(xElement3)
            xChildElement3 = xdoc.CreateElement("sv_name")
            xChildElement3.InnerText = "TRAVRDATETIME,STIME.SETPNAME,N2,PRESSURE"
            xElement3.AppendChild(xChildElement3)
            xElement4 = xdoc.CreateElement("traces")
            xElement3.AppendChild(xElement4)

            '第4層節點traces
            xChildElement4 = xdoc.CreateElement("trace")
            xChildElement4.InnerText = Today
            xElement4.AppendChild(xChildElement4)
            xChildElement4 = xdoc.CreateElement("trace")
            xChildElement4.InnerText = Format(Now, "HH.mm.ss")
            xElement4.AppendChild(xChildElement4)

            '輸出檔案位置與名稱 
            xdoc.Save("C:\temp\XML_Create.xml")
        Catch ex As Exception
            MessageBox.Show(ex.Message & System.Environment.NewLine & ex.StackTrace)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        '讀出ini檔,區段裡的值:
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\test.ini"
        '參數一 Section Name
        '參數二 於.ini中的項目
        '參數三 項目的內容
        '參數四 .ini檔的名稱
        '參數五 大小
        '參數六 路徑
        GetPrivateProfileString("City", "City_1", "", sKeyValue, nSize, sinifilename)
        Label1.Text = sKeyValue.ToString

        GetPrivateProfileString("City", "City_2", "", sKeyValue, nSize, sinifilename)
        Label2.Text = sKeyValue.ToString

        GetPrivateProfileString("City", "City_3", "", sKeyValue, nSize, sinifilename)
        Label3.Text = sKeyValue.ToString

        GetPrivateProfileString("food", "food_1", "", sKeyValue, nSize, sinifilename)
        Label4.Text = sKeyValue.ToString

        GetPrivateProfileString("food", "food_2", "", sKeyValue, nSize, sinifilename)
        Label5.Text = sKeyValue.ToString

        GetPrivateProfileString("food", "food_3", "", sKeyValue, nSize, sinifilename)
        Label6.Text = sKeyValue.ToString

        'sKeyValue = sKeyValue.Append(TextBox1.Text)

        'WritePrivateProfileString("food", "food_3", sKeyValue, sinifilename)

        WritePrivateProfileString("food", "food_3", TextBox1.Text, sinifilename)

    End Sub
    '2.宣告方法:
    '* lpAppName：指向包含Section 名稱的字符串地址
    '* lpKeyName：指向包含Key 名稱的字符串地址
    '* lpDefault：如果Key 值沒有找到，缺省返回缺省的字符串
    '* lpReturnedString：用於保存返回字符串的緩衝區
    '* nSize： 緩衝區的長度
    '* lpFileName ：ini 文件的文件名
    Public Declare Function GetPrivateProfileString Lib "kernel32" _
     Alias "GetPrivateProfileStringA" ( _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpApplicationName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpDefault As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpReturnedString As StringBuilder, _
     ByVal nSize As UInt32, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As UInt32
    Public Declare Function WritePrivateProfileString Lib "kernel32" _
     Alias "WritePrivateProfileStringA" ( _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpApplicationName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpReturnedString As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As UInt32

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '引導使用者輸入資料
        TextBox11.Text = "192.168.3.195"
        TextBox12.Text = "1"
        TextBox13.Text = "40012"

        '3.使用方式:
        '讀出ini檔,區段裡的值:
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\test.ini"
        '參數一 Section Name
        '參數二 於.ini中的項目
        '參數三 項目的內容
        '參數四 .ini檔的名稱
        '參數五 大小
        '參數六 路徑
        GetPrivateProfileString("City", "City_1", "", sKeyValue, nSize, sinifilename)
        Label1.Text = sKeyValue.ToString

        GetPrivateProfileString("City", "City_2", "", sKeyValue, nSize, sinifilename)
        Label2.Text = sKeyValue.ToString

        GetPrivateProfileString("City", "City_3", "", sKeyValue, nSize, sinifilename)
        Label3.Text = sKeyValue.ToString

        GetPrivateProfileString("food", "food_1", "", sKeyValue, nSize, sinifilename)
        Label4.Text = sKeyValue.ToString

        GetPrivateProfileString("food", "food_2", "", sKeyValue, nSize, sinifilename)
        Label5.Text = sKeyValue.ToString

        GetPrivateProfileString("food", "food_3", "", sKeyValue, nSize, sinifilename)
        Label6.Text = sKeyValue.ToString



        GetPrivateProfileString("goods", "goods_2", "", sKeyValue, nSize, sinifilename)
        'Label8.Text = sKeyValue.ToString

        GetPrivateProfileString("goods", "goods_3", "", sKeyValue, nSize, sinifilename)
        'Label9.Text = sKeyValue.ToString
    End Sub


    Private Sub PictureBox1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox1.Paint
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\test.ini"

        GetPrivateProfileString("goods", "goods_1", "", sKeyValue, nSize, sinifilename)
        'Label7.Text = sKeyValue.ToString

        Dim drawstring1 = "F1"
        Dim drawstring2 = sKeyValue.ToString
        Dim x1 = 0, y1 = 0, x2 = 8, y2 = 15    '定義字體左上角位置
        Dim drawfont1 As New Font("Helvetica", 11, FontStyle.Bold)    '定義字型
        Dim drawfont2 As New Font("Microsoft JhengHei", 13, FontStyle.Bold)    '定義字型

        PictureBox1.BackColor = Color.FromArgb(0, 136, 254)
        Dim mycolor1 As New SolidBrush(Color.FromArgb(255, 255, 255))    '定義字體顏色
        Dim mycolor2 As New SolidBrush(Color.FromArgb(255, 255, 255))    '定義字體顏色
        e.Graphics.DrawString(drawstring1, drawfont1, mycolor1, x1, y1)
        e.Graphics.DrawString(drawstring2, drawfont2, mycolor2, x2, y2)
    End Sub

    Private Sub PictureBox2_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox2.Paint
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\test.ini"

        GetPrivateProfileString("goods", "goods_2", "", sKeyValue, nSize, sinifilename)
        'Label7.Text = sKeyValue.ToString

        Dim drawstring1 = "F2"
        Dim drawstring2 = sKeyValue.ToString
        Dim x1 = 0, y1 = 0, x2 = 8, y2 = 15    '定義字體左上角位置
        Dim drawfont1 As New Font("Helvetica", 11, FontStyle.Bold)    '定義字型
        Dim drawfont2 As New Font("Microsoft JhengHei", 13, FontStyle.Bold)    '定義字型

        PictureBox2.BackColor = Color.FromArgb(0, 136, 254)
        Dim mycolor1 As New SolidBrush(Color.FromArgb(255, 255, 255))    '定義字體顏色
        Dim mycolor2 As New SolidBrush(Color.FromArgb(255, 255, 255))    '定義字體顏色
        e.Graphics.DrawString(drawstring1, drawfont1, mycolor1, x1, y1)
        e.Graphics.DrawString(drawstring2, drawfont2, mycolor2, x2, y2)
    End Sub

    Private Sub PictureBox3_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox3.Paint
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\test.ini"

        GetPrivateProfileString("goods", "goods_3", "", sKeyValue, nSize, sinifilename)
        'Label7.Text = sKeyValue.ToString

        Dim drawstring1 = "F3"
        Dim drawstring2 = sKeyValue.ToString
        Dim x1 = 0, y1 = 0, x2 = 8, y2 = 15    '定義字體左上角位置
        Dim drawfont1 As New Font("Helvetica", 11, FontStyle.Bold)    '定義字型
        Dim drawfont2 As New Font("Microsoft JhengHei", 13, FontStyle.Bold)    '定義字型

        PictureBox3.BackColor = Color.FromArgb(0, 136, 254)
        Dim mycolor1 As New SolidBrush(Color.FromArgb(255, 255, 255))    '定義字體顏色
        Dim mycolor2 As New SolidBrush(Color.FromArgb(255, 255, 255))    '定義字體顏色
        e.Graphics.DrawString(drawstring1, drawfont1, mycolor1, x1, y1)
        e.Graphics.DrawString(drawstring2, drawfont2, mycolor2, x2, y2)
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim value As Short
        Dim dip As String
        Dim id As Byte
        dip = TextBox11.Text
        id = TextBox12.Text
        Dim bi As Integer
        bi = CInt(TextBox13.Text) - 40001
        Dim address(2) As Byte
        Dim bii As Short = bi / 256
        If bii * 256 > bi Then
            bii = bii - 1
        End If
        address(0) = bii
        address(1) = bi Mod 256
        tconnect(dip, id, address(0), address(1), value)
        TextBox14.Text = value
    End Sub

    Public Sub tconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)
        Dim myTcpClient As New TcpClient(IP, 502)                                        'Device IP 與 port
        Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                   '與Device 連線
        If myTcpClient.Connected Then
            Label11.Text = "Server Connected ..."
        Else
            Label11.Text = "Server Not Connected ..."
        End If

        Dim byttrs(11) As Byte                                                           '指令集 MODBUS TCP 與 RTU 指令串不相同
        byttrs(0) = 0                                                                    'Transaction Identifier Hi
        byttrs(1) = 1                                                                    'Transaction Identifier Lo
        byttrs(2) = 0                                                                    'Protocol Identifier Hi
        byttrs(3) = 0                                                                    'Protocol Identifier Lo
        byttrs(4) = 0                                                                    'Message Length Hi
        byttrs(5) = 1                                                                    'Message Length Lo
        byttrs(6) = id                                                                   'The Unit Identifier 
        byttrs(7) = 4                                                                    'The Function Code ("4" 為讀取Holding Registers)
        byttrs(8) = adh                                                                  'Starting Address Hi
        byttrs(9) = adl                                                                  'Starting Address Lo
        byttrs(10) = 0                                                                   'No. of Points Hi
        byttrs(11) = 1                                                                   'No. of Points Lo

        myNetworkStream.Write(byttrs, 0, byttrs.Length)                                  '送出命令
        Dim inStream(11) As Byte
        myNetworkStream.Read(inStream, 0, inStream.Length)                               '接收資料
        'Dim value As Integer = 0                                                         '初始值設為0
        value = (value << 8) + inStream(9)                                               '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + inStream(10)                                              '把解析出的byte除以除以 2 的 8 次方
        myTcpClient.Close()
    End Sub

    Public Sub twconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)
        Dim myTcpClient As New TcpClient(IP, 502)                                        'Device IP 與 port
        Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                   '與Device 連線
        If myTcpClient.Connected Then
            Label11.Text = "Server Connected ..."
        Else
            Label11.Text = "Server Not Connected ..."
        End If

        Dim address(2) As Byte
        Dim vi As Short = value / 256
        If vi * 256 > value Then
            vi = vi - 1
        End If
        address(0) = vi
        address(1) = value Mod 256

        Dim byttrs(13) As Byte                                                           '指令集 MODBUS TCP 與 RTU 指令串不相同
        byttrs(0) = 0                                                                    'Transaction Identifier Hi
        byttrs(1) = 1                                                                    'Transaction Identifier Lo
        byttrs(2) = 0                                                                    'Protocol Identifier Hi
        byttrs(3) = 0                                                                    'Protocol Identifier Lo
        byttrs(4) = 0                                                                    'Message Length Hi
        byttrs(5) = 1                                                                    'Message Length Lo
        byttrs(6) = id                                                                   'The Unit Identifier 
        byttrs(7) = 6                                                                    'The Function Code ("4" 為讀取Holding Registers)
        byttrs(8) = adh                                                                  'Starting Address Hi
        byttrs(9) = adl                                                                  'Starting Address Lo
        byttrs(10) = address(0)                                                          'value Hi
        byttrs(11) = address(1)                                                          'value Lo
        byttrs(12) = 0                                                                   'No. of Points Hi
        byttrs(13) = 1                                                                   'No. of Points Lo

        myNetworkStream.Write(byttrs, 0, byttrs.Length)                                  '送出命令
        Dim inStream(13) As Byte
        myNetworkStream.Read(inStream, 0, inStream.Length)                               '接收資料
        'Dim value As Integer = 0                                                         '初始值設為0
        value = 0
        value = (value << 8) + inStream(10)                                               '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + inStream(11)                                              '把解析出的byte除以除以 2 的 8 次方
        myTcpClient.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim value As Short
        Dim dip As String
        Dim id As Byte
        dip = TextBox11.Text
        id = TextBox12.Text
        Dim bi As Integer
        bi = CInt(TextBox13.Text) - 40001
        Dim address(2) As Byte
        Dim bii As Short = bi / 256
        If bii * 256 > bi Then
            bii = bii - 1
        End If
        address(0) = bii
        address(1) = bi Mod 256

        tconnect(dip, id, address(0), address(1), value)
        TextBox2.Text = value
        value = TextBox3.Text
        twconnect(dip, id, address(0), address(1), value)
        tconnect(dip, id, address(0), address(1), value)
        TextBox2.Text = value
    End Sub
End Class
