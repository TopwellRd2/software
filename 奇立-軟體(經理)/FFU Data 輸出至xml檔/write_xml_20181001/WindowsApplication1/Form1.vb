﻿Imports System.Xml

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim xdoc As XmlDocument
        Dim xElement As XmlElement
        Dim xChildElement As XmlElement
        Dim xElement2 As XmlElement
        'Dim xChildElement2 As XmlElement (暫定無第二層無子層需求)
        Dim xElement3 As XmlElement
        Dim xChildElement3 As XmlElement
        Dim xElement4 As XmlElement
        Dim xChildElement4 As XmlElement
        Try
            '建立一個 XmlDocument 物件並加入 Declaration
            xdoc = New XmlDocument
            xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", "yes"))

            '建立根節點物件並加入 XmlDocument 中 (第0層)
            xElement = xdoc.CreateElement("try")
            xdoc.AppendChild(xElement)

            '第1層節點try
            xChildElement = xdoc.CreateElement("trx_name")
            xChildElement.InnerText = "BUContinuousRunRpt"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("type_id")
            xChildElement.InnerText = "I"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("abbr_no")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("chamber_id")
            xChildElement.InnerText = "A"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("data_clock")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("eqp_id")
            xChildElement.InnerText = "A2CVD10"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("eqp_run_id")
            xElement.AppendChild(xChildElement)

            '第2層節點trace_sets
            xElement2 = xdoc.CreateElement("trace_sets")
            xElement.AppendChild(xElement2)

            '第3層節點trace_set
            xElement3 = xdoc.CreateElement("trace_set")
            xElement2.AppendChild(xElement3)
            xChildElement3 = xdoc.CreateElement("sv_name")
            xChildElement3.InnerText = "TRAVRDATETIME,STIME.SETPNAME,N2,PRESSURE"
            xElement3.AppendChild(xChildElement3)
            xElement4 = xdoc.CreateElement("traces")
            xElement3.AppendChild(xElement4)

            '第4層節點traces
            xChildElement4 = xdoc.CreateElement("trace")
            xChildElement4.InnerText = Today
            xElement4.AppendChild(xChildElement4)
            xChildElement4 = xdoc.CreateElement("trace")
            xChildElement4.InnerText = Format(Now, "HH.mm.ss")
            xElement4.AppendChild(xChildElement4)

            '輸出檔案位置與名稱 
            xdoc.Save("C:\temp\XML_Create.xml")
        Catch ex As Exception
            MessageBox.Show(ex.Message & System.Environment.NewLine & ex.StackTrace)
        End Try
    End Sub
End Class
