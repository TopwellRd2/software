﻿Imports System.Xml
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.Net
Imports System.Net.Sockets
Imports System.ComponentModel

Public Class FFU_Detail
    Dim value(440) As Single
    Dim dip As String = Form1.GWIP.Text                                                   '與Form1視窗同步顯示Gateway是否斷線資訊
    Dim id As Byte = 1
    Dim svalue As Integer
    Dim sa As Integer
    Dim f1 As Integer = 0

    Private Sub FFU_Detail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim index As Integer = 0
        Dim flag As Integer

        For i = 0 To 439                                                                  '讀取Form1裡的Value資料
            value(i) = Form1.value(i)
        Next

        GW01.Visible = Form1.GW01.Visible                                                 '與Form1視窗同步顯示Gateway是否斷線資訊

        Dim ONOFF As TextBox
        Dim SRPM As TextBox
        Dim MA As TextBox
        Dim ACODE As TextBox
        Dim RPM As TextBox
        Dim DPT As TextBox
        For index = 1 To 44                                                               '把value填到畫面的資訊欄裡
            flag = (index - 1) * 10
            ONOFF = Me.Controls("ON_" & (index).ToString)                                 'FFU Status
            ONOFF.Text = value(flag)
            SRPM = Me.Controls("SRPM_" & (index).ToString)                                'FFU 設定轉速
            SRPM.Text = value(flag + 1)
            MA = Me.Controls("MA_" & (index).ToString)                                    'FFU 電流 (單位為A,原設計為mA但調整匯不同變數名稱就不變動了)
            MA.Text = value(flag + 2)
            ACODE = Me.Controls("ACode_" & (index).ToString)                              'FFU Alarm Code
            ACODE.Text = value(flag + 3)
            RPM = Me.Controls("RPM_" & (index).ToString)                                  'FFU 轉速
            RPM.Text = value(flag + 4)
            DPT = Me.Controls("DPT_" & (index).ToString)                                  'FFU 壓差資料(單位為pa)
            DPT.Text = value(flag + 8)
        Next

        '讀出ini檔,區段裡的值:
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\ffu.ini"
        Dim fs As String
        Dim DPTH As TextBox
        Dim DPTL As TextBox
        For index = 1 To 44
            fs = "FFU" + index.ToString
            DPTH = Me.Controls("DPTH_" & (index).ToString)
            DPTH.Text = Form1.FDPTH(index)                                                'FFU 壓差High資料(單位為pa)
            DPTL = Me.Controls("DPTL_" & (index).ToString)
            DPTL.Text = Form1.FDPTL(index)                                                'FFU 壓差Kow資料(單位為pa)
        Next
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing   '關閉視窗出現確認畫面
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub

    Public Sub tconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)   '讀取Modbus TCP/IP 資料
        Dim myTcpClient As New TcpClient(IP, 502)                                         'Device IP 與 port
        Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                    '與Device 連線
        If myTcpClient.Connected Then
            Label11.Text = "Server Connected ..."
        Else
            Label11.Text = "Server Not Connected ..."
        End If

        Dim byttrs(11) As Byte                                                            '指令集 MODBUS TCP 與 RTU 指令串不相同
        byttrs(0) = 0                                                                     'Transaction Identifier Hi
        byttrs(1) = 1                                                                     'Transaction Identifier Lo
        byttrs(2) = 0                                                                     'Protocol Identifier Hi
        byttrs(3) = 0                                                                     'Protocol Identifier Lo
        byttrs(4) = 0                                                                     'Message Length Hi
        byttrs(5) = 1                                                                     'Message Length Lo
        byttrs(6) = id                                                                    'The Unit Identifier 
        byttrs(7) = 4                                                                     'The Function Code ("4" 為讀取Holding Registers)
        byttrs(8) = adh                                                                   'Starting Address Hi
        byttrs(9) = adl                                                                   'Starting Address Lo
        byttrs(10) = 0                                                                    'No. of Points Hi
        byttrs(11) = 1                                                                    'No. of Points Lo

        myNetworkStream.Write(byttrs, 0, byttrs.Length)                                   '送出命令
        Dim inStream(11) As Byte
        myNetworkStream.Read(inStream, 0, inStream.Length)                                '接收資料
        value = (value << 8) + inStream(9)                                                '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + inStream(10)                                               '把解析出的byte除以除以 2 的 8 次方
        myTcpClient.Close()
    End Sub

    Public Sub twconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)   '寫入Modbus TCP/IP 資料
        Dim myTcpClient As New TcpClient(IP, 502)                                         'Device IP 與 port
        Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                    '與Device 連線
        If myTcpClient.Connected Then                                                     '在寫入資料時先判別連線狀態
            Label573.Visible = False
            Dim address(2) As Byte
            Dim vi As Short = value / 256
            If vi * 256 > value Then
                vi = vi - 1
            End If
            address(0) = vi
            address(1) = value Mod 256

            Dim byttrs(13) As Byte                                                        '指令集 MODBUS TCP 與 RTU 指令串不相同
            byttrs(0) = 0                                                                 'Transaction Identifier Hi
            byttrs(1) = 1                                                                 'Transaction Identifier Lo
            byttrs(2) = 0                                                                 'Protocol Identifier Hi
            byttrs(3) = 0                                                                 'Protocol Identifier Lo
            byttrs(4) = 0                                                                 'Message Length Hi
            byttrs(5) = 1                                                                 'Message Length Lo
            byttrs(6) = id                                                                'The Unit Identifier 
            byttrs(7) = 6                                                                 'The Function Code ("4" 為讀取Holding Registers)
            byttrs(8) = adh                                                               'Starting Address Hi
            byttrs(9) = adl                                                               'Starting Address Lo
            byttrs(10) = address(0)                                                       'value Hi
            byttrs(11) = address(1)                                                       'value Lo
            byttrs(12) = 0                                                                'No. of Points Hi
            byttrs(13) = 1                                                                'No. of Points Lo

            myNetworkStream.Write(byttrs, 0, byttrs.Length)                               '送出命令
            Dim inStream(13) As Byte
            myNetworkStream.Read(inStream, 0, inStream.Length)                            '接收資料
            value = 0
            value = (value << 8) + inStream(10)                                           '把解析出的byte除以除以 2 的 8 次方
            value = (value << 8) + inStream(11)                                           '把解析出的byte除以除以 2 的 8 次方
        Else
            Label573.Text = "Server Not Connected ..."
        End If
        myTcpClient.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        For i = 0 To 439                                                                  '讀取Form1裡的Value資料
            value(i) = Form1.value(i)
        Next
        GW01.Visible = Form1.GW01.Visible                                                 '與Form1視窗同步顯示Gateway是否斷線資訊
        Dim MA As TextBox
        Dim ACODE As TextBox
        Dim RPM As TextBox
        Dim DPT As TextBox
        Dim flag As Integer = 0

        For index = 1 To 44
            flag = (index - 1) * 10
            If f1 = 0 Then                                                                '以防程式一啟動就切到這畫面無法顯示正確之狀態與設定轉速,下次讀取再顯示一次
                Dim ONOFF As TextBox
                Dim SRPM As TextBox
                ONOFF = Me.Controls("ON_" & (index).ToString)
                ONOFF.Text = value(flag)
                SRPM = Me.Controls("SRPM_" & (index).ToString)
                SRPM.Text = value(flag + 1)
                f1 = 1
            End If

            MA = Me.Controls("MA_" & (index).ToString)                                    'FFU 電流 (單位為A,原設計為mA但調整匯不同變數名稱就不變動了)
            MA.Text = value(flag + 2)
            ACODE = Me.Controls("ACode_" & (index).ToString)                              'FFU Alarm Code
            ACODE.Text = value(flag + 3)
            RPM = Me.Controls("RPM_" & (index).ToString)                                  'FFU 轉速
            RPM.Text = value(flag + 4)
            DPT = Me.Controls("DPT_" & (index).ToString)                                  'FFU 壓差資料(單位為pa)
            DPT.Text = value(flag + 8)
        Next
        DPTA()
        ACODEA()
    End Sub

    Private Sub Button82_Click(sender As Object, e As EventArgs) Handles Button82.Click   'ID44 轉速設定
        svalue = SRPM_44.Text
        sa = 40442
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click     'ID1 ON/OFF設定
        svalue = ON_1.Text
        sa = 40011
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click     'ID1 轉速設定
        svalue = SRPM_1.Text
        sa = 40012
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click     'ID2 ON/OFF設定
        svalue = ON_2.Text
        sa = 40021
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click     'ID3 ON/OFF設定
        svalue = ON_3.Text
        sa = 40031
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click     'ID4 ON/OFF設定
        svalue = ON_4.Text
        sa = 40041
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click     'ID5 ON/OFF設定
        svalue = ON_5.Text
        sa = 40051
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click   'ID6 ON/OFF設定
        svalue = ON_6.Text
        sa = 40061
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click   'ID7 ON/OFF設定
        svalue = ON_7.Text
        sa = 40071
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click   'ID8 ON/OFF設定
        svalue = ON_8.Text
        sa = 40081
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click   'ID9 ON/OFF設定
        svalue = ON_9.Text
        sa = 40091
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click   'ID10 ON/OFF設定
        svalue = ON_10.Text
        sa = 40101
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click   'ID20 ON/OFF設定
        svalue = ON_20.Text
        sa = 40201
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click   'ID19 ON/OFF設定
        svalue = ON_19.Text
        sa = 40191
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click   'ID18 ON/OFF設定
        svalue = ON_18.Text
        sa = 40181
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click   'ID17 ON/OFF設定
        svalue = ON_17.Text
        sa = 40171
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click   'ID16 ON/OFF設定
        svalue = ON_16.Text
        sa = 40161
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click   'ID15 ON/OFF設定
        svalue = ON_15.Text
        sa = 40151
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button33_Click(sender As Object, e As EventArgs) Handles Button33.Click   'ID14 ON/OFF設定
        svalue = ON_14.Text
        sa = 40141
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click   'ID13 ON/OFF設定
        svalue = ON_13.Text
        sa = 40131
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click   'ID12 ON/OFF設定
        svalue = ON_12.Text
        sa = 40121
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button39_Click(sender As Object, e As EventArgs) Handles Button39.Click   'ID11 ON/OFF設定
        svalue = ON_11.Text
        sa = 40111
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button59_Click(sender As Object, e As EventArgs) Handles Button59.Click   'ID21 ON/OFF設定
        svalue = ON_21.Text
        sa = 40211
        SETVALUE(sa, svalue)
    End Sub

    Public Sub SETVALUE(saddress As Integer, svalue As Integer)                           '寫入資料到設備(啟停/轉速)
        Dim bii As Short
        Dim biii As Short
        Dim address(2) As Byte
        If svalue >= 600 And svalue <= 1300 Or svalue <= 1 Then
            biii = saddress - 40001
            bii = biii / 256
            If bii * 256 > biii Then
                bii = bii - 1
            End If
            address(0) = bii
            address(1) = biii Mod 256
            twconnect(dip, id, address(0), address(1), svalue)
        Else
            MessageBox.Show("FFU 設定轉速範圍為600~1300 ,該設定超出此範圍")
        End If
    End Sub

    Private Sub Button41_Click(sender As Object, e As EventArgs) Handles Button41.Click   'ID30 ON/OFF設定
        svalue = ON_30.Text
        sa = 40301
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button57_Click(sender As Object, e As EventArgs) Handles Button57.Click   'ID22 ON/OFF設定
        svalue = ON_22.Text
        sa = 40221
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button55_Click(sender As Object, e As EventArgs) Handles Button55.Click   'ID23 ON/OFF設定
        svalue = ON_23.Text
        sa = 40231
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button53_Click(sender As Object, e As EventArgs) Handles Button53.Click   'ID24 ON/OFF設定
        svalue = ON_24.Text
        sa = 40241
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button51_Click(sender As Object, e As EventArgs) Handles Button51.Click   'ID25 ON/OFF設定
        svalue = ON_25.Text
        sa = 40251
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button49_Click(sender As Object, e As EventArgs) Handles Button49.Click   'ID26 ON/OFF設定
        svalue = ON_26.Text
        sa = 40261
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button47_Click(sender As Object, e As EventArgs) Handles Button47.Click   'ID27 ON/OFF設定
        svalue = ON_27.Text
        sa = 40271
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button45_Click(sender As Object, e As EventArgs) Handles Button45.Click   'ID28 ON/OFF設定
        svalue = ON_28.Text
        sa = 40281
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button43_Click(sender As Object, e As EventArgs) Handles Button43.Click   'ID29 ON/OFF設定
        svalue = ON_29.Text
        sa = 40291
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button61_Click(sender As Object, e As EventArgs) Handles Button61.Click   'ID40 ON/OFF設定
        svalue = ON_40.Text
        sa = 40401
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button63_Click(sender As Object, e As EventArgs) Handles Button63.Click   'ID39 ON/OFF設定
        svalue = ON_39.Text
        sa = 40391
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button65_Click(sender As Object, e As EventArgs) Handles Button65.Click   'ID38 ON/OFF設定
        svalue = ON_38.Text
        sa = 40381
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button67_Click(sender As Object, e As EventArgs) Handles Button67.Click   'ID37 ON/OFF設定
        svalue = ON_37.Text
        sa = 40371
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button69_Click(sender As Object, e As EventArgs) Handles Button69.Click   'ID36 ON/OFF設定
        svalue = ON_36.Text
        sa = 40361
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button71_Click(sender As Object, e As EventArgs) Handles Button71.Click   'ID35 ON/OFF設定
        svalue = ON_35.Text
        sa = 40351
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button73_Click(sender As Object, e As EventArgs) Handles Button73.Click   'ID34 ON/OFF設定
        svalue = ON_34.Text
        sa = 40341
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button75_Click(sender As Object, e As EventArgs) Handles Button75.Click   'ID33 ON/OFF設定
        svalue = ON_33.Text
        sa = 40331
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button77_Click(sender As Object, e As EventArgs) Handles Button77.Click   'ID32 ON/OFF設定
        svalue = ON_32.Text
        sa = 40321
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button79_Click(sender As Object, e As EventArgs) Handles Button79.Click   'ID31 ON/OFF設定
        svalue = ON_31.Text
        sa = 40311
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button87_Click(sender As Object, e As EventArgs) Handles Button87.Click   'ID41 ON/OFF設定
        svalue = ON_41.Text
        sa = 40411
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button85_Click(sender As Object, e As EventArgs) Handles Button85.Click   'ID42 ON/OFF設定
        svalue = ON_42.Text
        sa = 40421
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button83_Click(sender As Object, e As EventArgs) Handles Button83.Click   'ID43 ON/OFF設定
        svalue = ON_43.Text
        sa = 40431
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button81_Click(sender As Object, e As EventArgs) Handles Button81.Click   'ID44 ON/OFF設定
        svalue = ON_44.Text
        sa = 40441
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click     'ID2 轉速設定
        svalue = SRPM_2.Text
        sa = 40022
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click     'ID3 轉速設定
        svalue = SRPM_3.Text
        sa = 40032
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click     'ID4 轉速設定
        svalue = SRPM_4.Text
        sa = 40042
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click   'ID5 轉速設定
        svalue = SRPM_5.Text
        sa = 40052
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click   'ID6 轉速設定
        svalue = SRPM_6.Text
        sa = 40062
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click   'ID7 轉速設定
        svalue = SRPM_7.Text
        sa = 40072
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click   'ID8 轉速設定
        svalue = SRPM_8.Text
        sa = 40082
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click   'ID9 轉速設定
        svalue = SRPM_9.Text
        sa = 40092
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click   'ID10 轉速設定
        svalue = SRPM_10.Text
        sa = 40102
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click   'ID20 轉速設定
        svalue = SRPM_20.Text
        sa = 40202
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click   'ID19 轉速設定
        svalue = SRPM_19.Text
        sa = 40192
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click   'ID18 轉速設定
        svalue = SRPM_18.Text
        sa = 40182
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click   'ID17 轉速設定
        svalue = SRPM_17.Text
        sa = 40172
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click   'ID16 轉速設定
        svalue = SRPM_16.Text
        sa = 40162
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button32_Click(sender As Object, e As EventArgs) Handles Button32.Click   'ID15 轉速設定
        svalue = SRPM_15.Text
        sa = 40152
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button34_Click(sender As Object, e As EventArgs) Handles Button34.Click   'ID14 轉速設定
        svalue = SRPM_14.Text
        sa = 40142
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs) Handles Button36.Click   'ID13 轉速設定
        svalue = SRPM_13.Text
        sa = 40132
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click   'ID12 轉速設定
        svalue = SRPM_12.Text
        sa = 40122
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button40_Click(sender As Object, e As EventArgs) Handles Button40.Click   'ID11 轉速設定
        svalue = SRPM_11.Text
        sa = 40112
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button60_Click(sender As Object, e As EventArgs) Handles Button60.Click   'ID21 轉速設定
        svalue = SRPM_21.Text
        sa = 40212
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button58_Click(sender As Object, e As EventArgs) Handles Button58.Click   'ID22 轉速設定
        svalue = SRPM_22.Text
        sa = 40222
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button56_Click(sender As Object, e As EventArgs) Handles Button56.Click   'ID23 轉速設定
        svalue = SRPM_23.Text
        sa = 40232
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button54_Click(sender As Object, e As EventArgs) Handles Button54.Click   'ID24 轉速設定
        svalue = SRPM_24.Text
        sa = 40242
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button52_Click(sender As Object, e As EventArgs) Handles Button52.Click   'ID25 轉速設定
        svalue = SRPM_25.Text
        sa = 40252
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button50_Click(sender As Object, e As EventArgs) Handles Button50.Click   'ID26 轉速設定
        svalue = SRPM_26.Text
        sa = 40262
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button48_Click(sender As Object, e As EventArgs) Handles Button48.Click   'ID27 轉速設定
        svalue = SRPM_27.Text
        sa = 40272
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button46_Click(sender As Object, e As EventArgs) Handles Button46.Click   'ID28 轉速設定
        svalue = SRPM_28.Text
        sa = 40282
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button44_Click(sender As Object, e As EventArgs) Handles Button44.Click   'ID29 轉速設定
        svalue = SRPM_29.Text
        sa = 40292
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button42_Click(sender As Object, e As EventArgs) Handles Button42.Click   'ID30 轉速設定
        svalue = SRPM_30.Text
        sa = 40302
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button62_Click(sender As Object, e As EventArgs) Handles Button62.Click   'ID40 轉速設定
        svalue = SRPM_40.Text
        sa = 40402
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button64_Click(sender As Object, e As EventArgs) Handles Button64.Click   'ID39 轉速設定
        svalue = SRPM_39.Text
        sa = 40392
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button66_Click(sender As Object, e As EventArgs) Handles Button66.Click   'ID38 轉速設定
        svalue = SRPM_38.Text
        sa = 40382
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button68_Click(sender As Object, e As EventArgs) Handles Button68.Click   'ID37 轉速設定
        svalue = SRPM_37.Text
        sa = 40372
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button70_Click(sender As Object, e As EventArgs) Handles Button70.Click   'ID36 轉速設定
        svalue = SRPM_36.Text
        sa = 40362
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button72_Click(sender As Object, e As EventArgs) Handles Button72.Click   'ID35 轉速設定
        svalue = SRPM_35.Text
        sa = 40352
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button74_Click(sender As Object, e As EventArgs) Handles Button74.Click   'ID34 轉速設定
        svalue = SRPM_34.Text
        sa = 40342
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button76_Click(sender As Object, e As EventArgs) Handles Button76.Click   'ID33 轉速設定
        svalue = SRPM_33.Text
        sa = 40332
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button78_Click(sender As Object, e As EventArgs) Handles Button78.Click   'ID32 轉速設定
        svalue = SRPM_32.Text
        sa = 40322
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button80_Click(sender As Object, e As EventArgs) Handles Button80.Click   'ID31 轉速設定
        svalue = SRPM_31.Text
        sa = 40312
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button88_Click(sender As Object, e As EventArgs) Handles Button88.Click   'ID41 轉速設定
        svalue = SRPM_41.Text
        sa = 40412
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button86_Click(sender As Object, e As EventArgs) Handles Button86.Click   'ID42 轉速設定
        svalue = SRPM_42.Text
        sa = 40422
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button84_Click(sender As Object, e As EventArgs) Handles Button84.Click   'ID43 轉速設定
        svalue = SRPM_43.Text
        sa = 40432
        SETVALUE(sa, svalue)
    End Sub

    Private Sub Button89_Click(sender As Object, e As EventArgs) Handles Button89.Click   'ID44 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU44", DPTH_44.Text, sinifilename)
    End Sub

    Private Sub Button90_Click(sender As Object, e As EventArgs) Handles Button90.Click   'ID44 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU44", DPTL_44.Text, sinifilename)
    End Sub

    Private Sub Button91_Click(sender As Object, e As EventArgs) Handles Button91.Click   'ID1 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU1", DPTL_1.Text, sinifilename)
    End Sub

    Private Sub Button92_Click(sender As Object, e As EventArgs) Handles Button92.Click   'ID1 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU1", DPTH_1.Text, sinifilename)
    End Sub

    Private Sub Button93_Click(sender As Object, e As EventArgs) Handles Button93.Click   'ID2 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU2", DPTL_2.Text, sinifilename)
    End Sub

    Private Sub Button95_Click(sender As Object, e As EventArgs) Handles Button95.Click   'ID3 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU3", DPTL_3.Text, sinifilename)
    End Sub

    Private Sub Button97_Click(sender As Object, e As EventArgs) Handles Button97.Click   'ID4 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU4", DPTL_4.Text, sinifilename)
    End Sub

    Private Sub Button99_Click(sender As Object, e As EventArgs) Handles Button99.Click   'ID5 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU5", DPTL_5.Text, sinifilename)
    End Sub

    Private Sub Button101_Click(sender As Object, e As EventArgs) Handles Button101.Click 'ID6 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU6", DPTL_6.Text, sinifilename)
    End Sub

    Private Sub Button109_Click(sender As Object, e As EventArgs) Handles Button109.Click 'ID7 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU7", DPTL_7.Text, sinifilename)
    End Sub

    Private Sub Button107_Click(sender As Object, e As EventArgs) Handles Button107.Click 'ID8 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU8", DPTL_8.Text, sinifilename)
    End Sub

    Private Sub Button105_Click(sender As Object, e As EventArgs) Handles Button105.Click 'ID9 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU9", DPTL_9.Text, sinifilename)
    End Sub

    Private Sub Button103_Click(sender As Object, e As EventArgs) Handles Button103.Click 'ID10 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU10", DPTL_10.Text, sinifilename)
    End Sub

    Private Sub Button129_Click(sender As Object, e As EventArgs) Handles Button129.Click 'ID11 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU11", DPTL_11.Text, sinifilename)
    End Sub

    Private Sub Button127_Click(sender As Object, e As EventArgs) Handles Button127.Click 'ID12 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU12", DPTL_12.Text, sinifilename)
    End Sub

    Private Sub Button125_Click(sender As Object, e As EventArgs) Handles Button125.Click 'ID13 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU13", DPTL_13.Text, sinifilename)
    End Sub

    Private Sub Button123_Click(sender As Object, e As EventArgs) Handles Button123.Click 'ID14 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU14", DPTL_14.Text, sinifilename)
    End Sub

    Private Sub Button121_Click(sender As Object, e As EventArgs) Handles Button121.Click 'ID15 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU15", DPTL_15.Text, sinifilename)
    End Sub

    Private Sub Button119_Click(sender As Object, e As EventArgs) Handles Button119.Click 'ID16 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU16", DPTL_16.Text, sinifilename)
    End Sub

    Private Sub Button117_Click(sender As Object, e As EventArgs) Handles Button117.Click 'ID17 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU17", DPTL_17.Text, sinifilename)
    End Sub

    Private Sub Button115_Click(sender As Object, e As EventArgs) Handles Button115.Click 'ID18 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU18", DPTL_18.Text, sinifilename)
    End Sub

    Private Sub Button113_Click(sender As Object, e As EventArgs) Handles Button113.Click 'ID19 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU19", DPTL_19.Text, sinifilename)
    End Sub

    Private Sub Button111_Click(sender As Object, e As EventArgs) Handles Button111.Click 'ID20 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU20", DPTL_20.Text, sinifilename)
    End Sub

    Private Sub Button149_Click(sender As Object, e As EventArgs) Handles Button149.Click 'ID21 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU21", DPTL_21.Text, sinifilename)
    End Sub

    Private Sub Button147_Click(sender As Object, e As EventArgs) Handles Button147.Click 'ID22 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU22", DPTL_22.Text, sinifilename)
    End Sub

    Private Sub Button145_Click(sender As Object, e As EventArgs) Handles Button145.Click 'ID23 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU23", DPTL_23.Text, sinifilename)
    End Sub

    Private Sub Button143_Click(sender As Object, e As EventArgs) Handles Button143.Click 'ID24 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU24", DPTL_24.Text, sinifilename)
    End Sub

    Private Sub Button141_Click(sender As Object, e As EventArgs) Handles Button141.Click 'ID25 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU25", DPTL_25.Text, sinifilename)
    End Sub

    Private Sub Button139_Click(sender As Object, e As EventArgs) Handles Button139.Click 'ID26 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU26", DPTL_26.Text, sinifilename)
    End Sub

    Private Sub Button137_Click(sender As Object, e As EventArgs) Handles Button137.Click 'ID27 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU27", DPTL_27.Text, sinifilename)
    End Sub

    Private Sub Button135_Click(sender As Object, e As EventArgs) Handles Button135.Click 'ID28 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU28", DPTL_28.Text, sinifilename)
    End Sub

    Private Sub Button133_Click(sender As Object, e As EventArgs) Handles Button133.Click 'ID29 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU29", DPTL_29.Text, sinifilename)
    End Sub

    Private Sub Button131_Click(sender As Object, e As EventArgs) Handles Button131.Click 'ID30 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU30", DPTL_30.Text, sinifilename)
    End Sub

    Private Sub Button169_Click(sender As Object, e As EventArgs) Handles Button169.Click 'ID31 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU31", DPTL_31.Text, sinifilename)
    End Sub

    Private Sub Button167_Click(sender As Object, e As EventArgs) Handles Button167.Click 'ID32 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU32", DPTL_32.Text, sinifilename)
    End Sub

    Private Sub Button165_Click(sender As Object, e As EventArgs) Handles Button165.Click 'ID33 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU33", DPTL_33.Text, sinifilename)
    End Sub

    Private Sub Button163_Click(sender As Object, e As EventArgs) Handles Button163.Click 'ID34 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU34", DPTL_34.Text, sinifilename)
    End Sub

    Private Sub Button161_Click(sender As Object, e As EventArgs) Handles Button161.Click 'ID35 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU35", DPTL_35.Text, sinifilename)
    End Sub

    Private Sub Button159_Click(sender As Object, e As EventArgs) Handles Button159.Click 'ID36 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU36", DPTL_36.Text, sinifilename)
    End Sub

    Private Sub Button157_Click(sender As Object, e As EventArgs) Handles Button157.Click 'ID37 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU37", DPTL_37.Text, sinifilename)
    End Sub

    Private Sub Button155_Click(sender As Object, e As EventArgs) Handles Button155.Click 'ID38 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU38", DPTL_38.Text, sinifilename)
    End Sub

    Private Sub Button153_Click(sender As Object, e As EventArgs) Handles Button153.Click 'ID39 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU39", DPTL_39.Text, sinifilename)
    End Sub

    Private Sub Button151_Click(sender As Object, e As EventArgs) Handles Button151.Click 'ID40 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU40", DPTL_40.Text, sinifilename)
    End Sub

    Private Sub Button175_Click(sender As Object, e As EventArgs) Handles Button175.Click 'ID41 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU41", DPTL_41.Text, sinifilename)
    End Sub

    Private Sub Button173_Click(sender As Object, e As EventArgs) Handles Button173.Click 'ID42 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU42", DPTL_42.Text, sinifilename)
    End Sub

    Private Sub Button171_Click(sender As Object, e As EventArgs) Handles Button171.Click 'ID43 DPT Low設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTL", "FFU43", DPTL_43.Text, sinifilename)
    End Sub

    Private Sub Button94_Click(sender As Object, e As EventArgs) Handles Button94.Click   'ID2 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU2", DPTH_2.Text, sinifilename)
    End Sub

    Private Sub Button96_Click(sender As Object, e As EventArgs) Handles Button96.Click   'ID3 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU3", DPTH_3.Text, sinifilename)
    End Sub

    Private Sub Button98_Click(sender As Object, e As EventArgs) Handles Button98.Click   'ID4 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU4", DPTH_4.Text, sinifilename)
    End Sub

    Private Sub Button100_Click(sender As Object, e As EventArgs) Handles Button100.Click 'ID5 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU5", DPTH_5.Text, sinifilename)
    End Sub

    Private Sub Button102_Click(sender As Object, e As EventArgs) Handles Button102.Click 'ID6 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU6", DPTH_6.Text, sinifilename)
    End Sub

    Private Sub Button110_Click(sender As Object, e As EventArgs) Handles Button110.Click 'ID7 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU7", DPTH_7.Text, sinifilename)
    End Sub

    Private Sub Button108_Click(sender As Object, e As EventArgs) Handles Button108.Click 'ID8 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU8", DPTH_8.Text, sinifilename)
    End Sub

    Private Sub Button106_Click(sender As Object, e As EventArgs) Handles Button106.Click 'ID9 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU9", DPTH_9.Text, sinifilename)
    End Sub

    Private Sub Button104_Click(sender As Object, e As EventArgs) Handles Button104.Click 'ID10 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU10", DPTH_10.Text, sinifilename)
    End Sub

    Private Sub Button130_Click(sender As Object, e As EventArgs) Handles Button130.Click 'ID11 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU11", DPTH_11.Text, sinifilename)
    End Sub

    Private Sub Button128_Click(sender As Object, e As EventArgs) Handles Button128.Click 'ID12 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU12", DPTH_12.Text, sinifilename)
    End Sub

    Private Sub Button126_Click(sender As Object, e As EventArgs) Handles Button126.Click 'ID13 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU13", DPTH_13.Text, sinifilename)
    End Sub

    Private Sub Button124_Click(sender As Object, e As EventArgs) Handles Button124.Click 'ID14 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU14", DPTH_14.Text, sinifilename)
    End Sub

    Private Sub Button122_Click(sender As Object, e As EventArgs) Handles Button122.Click 'ID15 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU15", DPTH_15.Text, sinifilename)
    End Sub

    Private Sub Button120_Click(sender As Object, e As EventArgs) Handles Button120.Click 'ID16 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU16", DPTH_16.Text, sinifilename)
    End Sub

    Private Sub Button118_Click(sender As Object, e As EventArgs) Handles Button118.Click 'ID17 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU17", DPTH_17.Text, sinifilename)
    End Sub

    Private Sub Button116_Click(sender As Object, e As EventArgs) Handles Button116.Click 'ID18 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU18", DPTH_18.Text, sinifilename)
    End Sub

    Private Sub Button114_Click(sender As Object, e As EventArgs) Handles Button114.Click 'ID19 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU19", DPTH_19.Text, sinifilename)
    End Sub

    Private Sub Button112_Click(sender As Object, e As EventArgs) Handles Button112.Click 'ID20 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU20", DPTH_20.Text, sinifilename)
    End Sub

    Private Sub Button150_Click(sender As Object, e As EventArgs) Handles Button150.Click 'ID21 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU21", DPTH_21.Text, sinifilename)
    End Sub

    Private Sub Button148_Click(sender As Object, e As EventArgs) Handles Button148.Click 'ID22 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU22", DPTH_22.Text, sinifilename)
    End Sub

    Private Sub Button146_Click(sender As Object, e As EventArgs) Handles Button146.Click 'ID23 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU23", DPTH_23.Text, sinifilename)
    End Sub

    Private Sub Button144_Click(sender As Object, e As EventArgs) Handles Button144.Click 'ID24 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU24", DPTH_24.Text, sinifilename)
    End Sub

    Private Sub Button142_Click(sender As Object, e As EventArgs) Handles Button142.Click 'ID25 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU25", DPTH_25.Text, sinifilename)
    End Sub

    Private Sub Button140_Click(sender As Object, e As EventArgs) Handles Button140.Click 'ID26 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU26", DPTH_26.Text, sinifilename)
    End Sub

    Private Sub Button138_Click(sender As Object, e As EventArgs) Handles Button138.Click 'ID27 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU27", DPTH_27.Text, sinifilename)
    End Sub

    Private Sub Button136_Click(sender As Object, e As EventArgs) Handles Button136.Click 'ID28 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU28", DPTH_28.Text, sinifilename)
    End Sub

    Private Sub Button134_Click(sender As Object, e As EventArgs) Handles Button134.Click 'ID29 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU29", DPTH_29.Text, sinifilename)
    End Sub

    Private Sub Button132_Click(sender As Object, e As EventArgs) Handles Button132.Click 'ID30 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU30", DPTH_30.Text, sinifilename)
    End Sub

    Private Sub Button170_Click(sender As Object, e As EventArgs) Handles Button170.Click 'ID31 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU31", DPTH_31.Text, sinifilename)
    End Sub

    Private Sub Button168_Click(sender As Object, e As EventArgs) Handles Button168.Click 'ID32 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU32", DPTH_32.Text, sinifilename)
    End Sub

    Private Sub Button166_Click(sender As Object, e As EventArgs) Handles Button166.Click 'ID33 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU33", DPTH_33.Text, sinifilename)
    End Sub

    Private Sub Button164_Click(sender As Object, e As EventArgs) Handles Button164.Click 'ID34 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU34", DPTH_34.Text, sinifilename)
    End Sub

    Private Sub Button162_Click(sender As Object, e As EventArgs) Handles Button162.Click 'ID35 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU35", DPTH_35.Text, sinifilename)
    End Sub

    Private Sub Button160_Click(sender As Object, e As EventArgs) Handles Button160.Click 'ID36 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU36", DPTH_36.Text, sinifilename)
    End Sub

    Private Sub Button158_Click(sender As Object, e As EventArgs) Handles Button158.Click 'ID37 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU37", DPTH_37.Text, sinifilename)
    End Sub

    Private Sub Button156_Click(sender As Object, e As EventArgs) Handles Button156.Click 'ID38 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU38", DPTH_38.Text, sinifilename)
    End Sub

    Private Sub Button154_Click(sender As Object, e As EventArgs) Handles Button154.Click 'ID39 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU39", DPTH_39.Text, sinifilename)
    End Sub

    Private Sub Button152_Click(sender As Object, e As EventArgs) Handles Button152.Click 'ID40 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU40", DPTH_40.Text, sinifilename)
    End Sub

    Private Sub Button176_Click(sender As Object, e As EventArgs) Handles Button176.Click 'ID41 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU41", DPTH_41.Text, sinifilename)
    End Sub

    Private Sub Button174_Click(sender As Object, e As EventArgs) Handles Button174.Click 'ID42 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU42", DPTH_42.Text, sinifilename)
    End Sub

    Private Sub Button172_Click(sender As Object, e As EventArgs) Handles Button172.Click 'ID43 DPT High設定
        Dim sinifilename As String = Application.StartupPath & "\\FFU.ini"
        Form1.WritePrivateProfileString("DPTH", "FFU43", DPTH_43.Text, sinifilename)
    End Sub

    Public Sub DPTA()   '判別是否各FFU是否有DPT警報
        Dim DPTA As TextBox
        Dim flag As Integer
        Dim HA, La As Short
        For index = 1 To 44
            flag = (index - 1) * 10
            HA = Form1.FDPTH(index)
            La = Form1.FDPTL(index)
            DPTA = Me.Controls("DPT_" & (index).ToString)
            If value(flag + 8) > HA Or value(flag + 8) < La Then
                DPTA.BackColor = Color.Yellow
            Else
                DPTA.BackColor = Color.White
            End If
        Next
    End Sub

    Public Sub ACODEA()   '判別是否各FFU是否有DPT警報
        Dim ACODE As TextBox
        Dim flag As Integer
        Dim HA, La As Short
        For index = 1 To 44
            flag = (index - 1) * 10
            HA = Form1.FDPTH(index)
            La = Form1.FDPTL(index)
            ACODE = Me.Controls("ACode_" & (index).ToString)
            If value(flag + 3) > 0 Then
                ACODE.BackColor = Color.Yellow
            Else
                ACODE.BackColor = Color.White
            End If
        Next
    End Sub

End Class