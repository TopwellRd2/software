﻿Imports System.Xml

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim xdoc As XmlDocument
        Dim xElement As XmlElement
        Dim xChildElement As XmlElement
        Dim xElement2 As XmlElement
        Dim xChildElement2 As XmlElement
        Try
            '建立一個 XmlDocument 物件並加入 Declaration
            xdoc = New XmlDocument
            xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", "yes"))
            '建立根節點物件並加入 XmlDocument 中 (第0層)
            xElement = xdoc.CreateElement("sections")
            '在sections寫入一個屬性
            xElement.SetAttribute("data_name", "部門人員資訊")
            xdoc.AppendChild(xElement)
            '在sections下寫入一個節點名稱為section(第1層)
            xChildElement = xdoc.CreateElement("section")
            xChildElement.SetAttribute("department", "人資部")
            xChildElement.SetAttribute("department_code", "2200")
            xElement.AppendChild(xChildElement)
            '第2層節點
            xElement2 = xdoc.CreateElement("users")
            xChildElement.AppendChild(xElement2)
            '第3層節點
            xChildElement2 = xdoc.CreateElement("user")
            xChildElement2.SetAttribute("Name", "孫小美")
            xChildElement2.SetAttribute("Year", "16")
            xChildElement2.SetAttribute("Sex", "女")
            xElement2.AppendChild(xChildElement2)
            '在sections下寫入一個節點名稱為section(第1層)
            xChildElement = xdoc.CreateElement("section")
            xChildElement.SetAttribute("department", "資訊部")
            xChildElement.SetAttribute("department_code", "3100")
            xElement.AppendChild(xChildElement)
            '第2層節點
            xElement2 = xdoc.CreateElement("users")
            xChildElement.AppendChild(xElement2)
            '第3層節點
            xChildElement2 = xdoc.CreateElement("user")
            xChildElement2.SetAttribute("Name", "張寶成")
            xChildElement2.SetAttribute("Year", "23")
            xChildElement2.SetAttribute("Sex", "男")
            xElement2.AppendChild(xChildElement2)
            xChildElement2 = xdoc.CreateElement("user")
            xChildElement2.SetAttribute("Name", "蕭瀟瀟")
            xChildElement2.SetAttribute("Year", "26")
            xChildElement2.SetAttribute("Sex", "男")
            xElement2.AppendChild(xChildElement2)
            xdoc.Save("C:\temp\XML_Create.xml")
        Catch ex As Exception
            MessageBox.Show(ex.Message & System.Environment.NewLine & ex.StackTrace)
        End Try
    End Sub
End Class
