﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.G01 = New System.Windows.Forms.TextBox()
        Me.G02 = New System.Windows.Forms.TextBox()
        Me.G03 = New System.Windows.Forms.TextBox()
        Me.G04 = New System.Windows.Forms.TextBox()
        Me.G05 = New System.Windows.Forms.TextBox()
        Me.G06 = New System.Windows.Forms.TextBox()
        Me.G07 = New System.Windows.Forms.TextBox()
        Me.G08 = New System.Windows.Forms.TextBox()
        Me.G09 = New System.Windows.Forms.TextBox()
        Me.G10 = New System.Windows.Forms.TextBox()
        Me.G11 = New System.Windows.Forms.TextBox()
        Me.G12 = New System.Windows.Forms.TextBox()
        Me.G13 = New System.Windows.Forms.TextBox()
        Me.G14 = New System.Windows.Forms.TextBox()
        Me.G15 = New System.Windows.Forms.TextBox()
        Me.G16 = New System.Windows.Forms.TextBox()
        Me.G17 = New System.Windows.Forms.TextBox()
        Me.G18 = New System.Windows.Forms.TextBox()
        Me.G19 = New System.Windows.Forms.TextBox()
        Me.G20 = New System.Windows.Forms.TextBox()
        Me.G21 = New System.Windows.Forms.TextBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.BK1 = New System.ComponentModel.BackgroundWorker()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.GW01 = New System.Windows.Forms.Label()
        Me.GWIP = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("新細明體", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Button1.Location = New System.Drawing.Point(1, 602)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(189, 45)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "輸出xml檔"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PictureBox4
        '
        Me.PictureBox4.ErrorImage = CType(resources.GetObject("PictureBox4.ErrorImage"), System.Drawing.Image)
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.InitialImage = CType(resources.GetObject("PictureBox4.InitialImage"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(1, 2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(1861, 560)
        Me.PictureBox4.TabIndex = 12
        Me.PictureBox4.TabStop = False
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("新細明體", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Button7.Location = New System.Drawing.Point(1, 557)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(189, 45)
        Me.Button7.TabIndex = 368
        Me.Button7.Text = "FFU Detail 資訊"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 10000
        '
        'G01
        '
        Me.G01.BackColor = System.Drawing.Color.Red
        Me.G01.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G01.Location = New System.Drawing.Point(138, 314)
        Me.G01.Name = "G01"
        Me.G01.Size = New System.Drawing.Size(80, 22)
        Me.G01.TabIndex = 374
        Me.G01.Text = "Group1 Alarm"
        Me.G01.Visible = False
        '
        'G02
        '
        Me.G02.BackColor = System.Drawing.Color.Red
        Me.G02.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G02.Location = New System.Drawing.Point(195, 400)
        Me.G02.Name = "G02"
        Me.G02.Size = New System.Drawing.Size(80, 22)
        Me.G02.TabIndex = 375
        Me.G02.Text = "Group2 Alarm"
        Me.G02.Visible = False
        '
        'G03
        '
        Me.G03.BackColor = System.Drawing.Color.Red
        Me.G03.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G03.Location = New System.Drawing.Point(270, 314)
        Me.G03.Name = "G03"
        Me.G03.Size = New System.Drawing.Size(80, 22)
        Me.G03.TabIndex = 376
        Me.G03.Text = "Group3 Alarm"
        Me.G03.Visible = False
        '
        'G04
        '
        Me.G04.BackColor = System.Drawing.Color.Red
        Me.G04.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G04.Location = New System.Drawing.Point(336, 400)
        Me.G04.Name = "G04"
        Me.G04.Size = New System.Drawing.Size(80, 22)
        Me.G04.TabIndex = 377
        Me.G04.Text = "Group4 Alarm"
        Me.G04.Visible = False
        '
        'G05
        '
        Me.G05.BackColor = System.Drawing.Color.Red
        Me.G05.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G05.Location = New System.Drawing.Point(1098, 98)
        Me.G05.Name = "G05"
        Me.G05.Size = New System.Drawing.Size(80, 22)
        Me.G05.TabIndex = 378
        Me.G05.Text = "Group5 Alarm"
        Me.G05.Visible = False
        '
        'G06
        '
        Me.G06.BackColor = System.Drawing.Color.Red
        Me.G06.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G06.Location = New System.Drawing.Point(1040, 192)
        Me.G06.Name = "G06"
        Me.G06.Size = New System.Drawing.Size(80, 22)
        Me.G06.TabIndex = 379
        Me.G06.Text = "Group6 Alarm"
        Me.G06.Visible = False
        '
        'G07
        '
        Me.G07.BackColor = System.Drawing.Color.Red
        Me.G07.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G07.Location = New System.Drawing.Point(977, 103)
        Me.G07.Name = "G07"
        Me.G07.Size = New System.Drawing.Size(80, 22)
        Me.G07.TabIndex = 380
        Me.G07.Text = "Group7 Alarm"
        Me.G07.Visible = False
        '
        'G08
        '
        Me.G08.BackColor = System.Drawing.Color.Red
        Me.G08.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G08.Location = New System.Drawing.Point(926, 187)
        Me.G08.Name = "G08"
        Me.G08.Size = New System.Drawing.Size(80, 22)
        Me.G08.TabIndex = 381
        Me.G08.Text = "Group8 Alarm"
        Me.G08.Visible = False
        '
        'G09
        '
        Me.G09.BackColor = System.Drawing.Color.Red
        Me.G09.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G09.Location = New System.Drawing.Point(910, 225)
        Me.G09.Name = "G09"
        Me.G09.Size = New System.Drawing.Size(80, 22)
        Me.G09.TabIndex = 382
        Me.G09.Text = "Group9 Alarm"
        Me.G09.Visible = False
        '
        'G10
        '
        Me.G10.BackColor = System.Drawing.Color.Red
        Me.G10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G10.Location = New System.Drawing.Point(848, 111)
        Me.G10.Name = "G10"
        Me.G10.Size = New System.Drawing.Size(80, 22)
        Me.G10.TabIndex = 383
        Me.G10.Text = "Group10 Alarm"
        Me.G10.Visible = False
        '
        'G11
        '
        Me.G11.BackColor = System.Drawing.Color.Red
        Me.G11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G11.Location = New System.Drawing.Point(762, 111)
        Me.G11.Name = "G11"
        Me.G11.Size = New System.Drawing.Size(80, 22)
        Me.G11.TabIndex = 384
        Me.G11.Text = "Group11 Alarm"
        Me.G11.Visible = False
        '
        'G12
        '
        Me.G12.BackColor = System.Drawing.Color.Red
        Me.G12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G12.Location = New System.Drawing.Point(714, 183)
        Me.G12.Name = "G12"
        Me.G12.Size = New System.Drawing.Size(80, 22)
        Me.G12.TabIndex = 385
        Me.G12.Text = "Group12 Alarm"
        Me.G12.Visible = False
        '
        'G13
        '
        Me.G13.BackColor = System.Drawing.Color.Red
        Me.G13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G13.Location = New System.Drawing.Point(776, 213)
        Me.G13.Name = "G13"
        Me.G13.Size = New System.Drawing.Size(80, 22)
        Me.G13.TabIndex = 386
        Me.G13.Text = "Group13 Alarm"
        Me.G13.Visible = False
        '
        'G14
        '
        Me.G14.BackColor = System.Drawing.Color.Red
        Me.G14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G14.Location = New System.Drawing.Point(719, 267)
        Me.G14.Name = "G14"
        Me.G14.Size = New System.Drawing.Size(80, 22)
        Me.G14.TabIndex = 387
        Me.G14.Text = "Group14 Alarm"
        Me.G14.Visible = False
        '
        'G15
        '
        Me.G15.BackColor = System.Drawing.Color.Red
        Me.G15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G15.Location = New System.Drawing.Point(655, 207)
        Me.G15.Name = "G15"
        Me.G15.Size = New System.Drawing.Size(80, 22)
        Me.G15.TabIndex = 388
        Me.G15.Text = "Group15 Alarm"
        Me.G15.Visible = False
        '
        'G16
        '
        Me.G16.BackColor = System.Drawing.Color.Red
        Me.G16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G16.Location = New System.Drawing.Point(572, 267)
        Me.G16.Name = "G16"
        Me.G16.Size = New System.Drawing.Size(80, 22)
        Me.G16.TabIndex = 389
        Me.G16.Text = "Group16 Alarm"
        Me.G16.Visible = False
        '
        'G17
        '
        Me.G17.BackColor = System.Drawing.Color.Red
        Me.G17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G17.Location = New System.Drawing.Point(532, 194)
        Me.G17.Name = "G17"
        Me.G17.Size = New System.Drawing.Size(80, 22)
        Me.G17.TabIndex = 390
        Me.G17.Text = "Group17 Alarm"
        Me.G17.Visible = False
        '
        'G18
        '
        Me.G18.BackColor = System.Drawing.Color.Red
        Me.G18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G18.Location = New System.Drawing.Point(642, 144)
        Me.G18.Name = "G18"
        Me.G18.Size = New System.Drawing.Size(80, 22)
        Me.G18.TabIndex = 391
        Me.G18.Text = "Group18 Alarm"
        Me.G18.Visible = False
        '
        'G19
        '
        Me.G19.BackColor = System.Drawing.Color.Red
        Me.G19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G19.Location = New System.Drawing.Point(532, 114)
        Me.G19.Name = "G19"
        Me.G19.Size = New System.Drawing.Size(80, 22)
        Me.G19.TabIndex = 392
        Me.G19.Text = "Group19 Alarm"
        Me.G19.Visible = False
        '
        'G20
        '
        Me.G20.BackColor = System.Drawing.Color.Red
        Me.G20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G20.Location = New System.Drawing.Point(449, 180)
        Me.G20.Name = "G20"
        Me.G20.Size = New System.Drawing.Size(80, 22)
        Me.G20.TabIndex = 393
        Me.G20.Text = "Group20 Alarm"
        Me.G20.Visible = False
        '
        'G21
        '
        Me.G21.BackColor = System.Drawing.Color.Red
        Me.G21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.G21.Location = New System.Drawing.Point(388, 114)
        Me.G21.Name = "G21"
        Me.G21.Size = New System.Drawing.Size(80, 22)
        Me.G21.TabIndex = 394
        Me.G21.Text = "Group21 Alarm"
        Me.G21.Visible = False
        '
        'Button9
        '
        Me.Button9.Font = New System.Drawing.Font("新細明體", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Button9.Location = New System.Drawing.Point(196, 602)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(218, 45)
        Me.Button9.TabIndex = 395
        Me.Button9.Text = "連線測試"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'BK1
        '
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.HotTrack
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.InitialImage = Nothing
        Me.PictureBox5.Location = New System.Drawing.Point(3, 4)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(102, 38)
        Me.PictureBox5.TabIndex = 396
        Me.PictureBox5.TabStop = False
        '
        'GW01
        '
        Me.GW01.AutoSize = True
        Me.GW01.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.GW01.Font = New System.Drawing.Font("新細明體", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GW01.ForeColor = System.Drawing.Color.Red
        Me.GW01.Location = New System.Drawing.Point(420, 571)
        Me.GW01.Name = "GW01"
        Me.GW01.Size = New System.Drawing.Size(201, 24)
        Me.GW01.TabIndex = 397
        Me.GW01.Text = "GateWay通訊異常"
        Me.GW01.Visible = False
        '
        'GWIP
        '
        Me.GWIP.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GWIP.Location = New System.Drawing.Point(314, 573)
        Me.GWIP.Name = "GWIP"
        Me.GWIP.Size = New System.Drawing.Size(100, 22)
        Me.GWIP.TabIndex = 399
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("新細明體", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(196, 571)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(116, 24)
        Me.Label21.TabIndex = 398
        Me.Label21.Text = "Gateway IP"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(154, 376)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(17, 12)
        Me.Label8.TabIndex = 405
        Me.Label8.Text = "01"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(154, 349)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(17, 12)
        Me.Label9.TabIndex = 406
        Me.Label9.Text = "02"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(216, 349)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(17, 12)
        Me.Label10.TabIndex = 408
        Me.Label10.Text = "04"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(216, 376)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(17, 12)
        Me.Label11.TabIndex = 407
        Me.Label11.Text = "03"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(302, 376)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(17, 12)
        Me.Label12.TabIndex = 410
        Me.Label12.Text = "06"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(279, 376)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(17, 12)
        Me.Label13.TabIndex = 409
        Me.Label13.Text = "05"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(325, 376)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(17, 12)
        Me.Label14.TabIndex = 411
        Me.Label14.Text = "07"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(357, 349)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(17, 12)
        Me.Label15.TabIndex = 413
        Me.Label15.Text = "09"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(357, 376)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(17, 12)
        Me.Label16.TabIndex = 412
        Me.Label16.Text = "08"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(407, 137)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(17, 12)
        Me.Label17.TabIndex = 415
        Me.Label17.Text = "44"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(407, 166)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(17, 12)
        Me.Label18.TabIndex = 414
        Me.Label18.Text = "43"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(468, 135)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(17, 12)
        Me.Label19.TabIndex = 417
        Me.Label19.Text = "42"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(468, 166)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(17, 12)
        Me.Label20.TabIndex = 416
        Me.Label20.Text = "41"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(539, 137)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(17, 12)
        Me.Label24.TabIndex = 419
        Me.Label24.Text = "40"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(539, 166)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(17, 12)
        Me.Label25.TabIndex = 418
        Me.Label25.Text = "39"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(604, 137)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(17, 12)
        Me.Label26.TabIndex = 421
        Me.Label26.Text = "38"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(604, 166)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(17, 12)
        Me.Label27.TabIndex = 420
        Me.Label27.Text = "37"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(634, 192)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(17, 12)
        Me.Label28.TabIndex = 422
        Me.Label28.Text = "36"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label29.ForeColor = System.Drawing.Color.Black
        Me.Label29.Location = New System.Drawing.Point(587, 230)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(17, 12)
        Me.Label29.TabIndex = 424
        Me.Label29.Text = "35"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label30.ForeColor = System.Drawing.Color.Black
        Me.Label30.Location = New System.Drawing.Point(587, 249)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(17, 12)
        Me.Label30.TabIndex = 423
        Me.Label30.Text = "34"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label31.ForeColor = System.Drawing.Color.Black
        Me.Label31.Location = New System.Drawing.Point(661, 230)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(17, 12)
        Me.Label31.TabIndex = 426
        Me.Label31.Text = "33"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label32.ForeColor = System.Drawing.Color.Black
        Me.Label32.Location = New System.Drawing.Point(661, 249)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(17, 12)
        Me.Label32.TabIndex = 425
        Me.Label32.Text = "32"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label33.ForeColor = System.Drawing.Color.Black
        Me.Label33.Location = New System.Drawing.Point(753, 230)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(17, 12)
        Me.Label33.TabIndex = 428
        Me.Label33.Text = "31"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label34.ForeColor = System.Drawing.Color.Black
        Me.Label34.Location = New System.Drawing.Point(753, 249)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(17, 12)
        Me.Label34.TabIndex = 427
        Me.Label34.Text = "30"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label35.ForeColor = System.Drawing.Color.Black
        Me.Label35.Location = New System.Drawing.Point(825, 249)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(17, 12)
        Me.Label35.TabIndex = 431
        Me.Label35.Text = "27"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label36.ForeColor = System.Drawing.Color.Black
        Me.Label36.Location = New System.Drawing.Point(802, 249)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(17, 12)
        Me.Label36.TabIndex = 430
        Me.Label36.Text = "28"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label37.ForeColor = System.Drawing.Color.Black
        Me.Label37.Location = New System.Drawing.Point(779, 249)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(17, 12)
        Me.Label37.TabIndex = 429
        Me.Label37.Text = "29"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label38.ForeColor = System.Drawing.Color.Black
        Me.Label38.Location = New System.Drawing.Point(825, 192)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(17, 12)
        Me.Label38.TabIndex = 432
        Me.Label38.Text = "26"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label39.ForeColor = System.Drawing.Color.Black
        Me.Label39.Location = New System.Drawing.Point(823, 166)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(17, 12)
        Me.Label39.TabIndex = 435
        Me.Label39.Text = "23"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label40.ForeColor = System.Drawing.Color.Black
        Me.Label40.Location = New System.Drawing.Point(800, 166)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(17, 12)
        Me.Label40.TabIndex = 434
        Me.Label40.Text = "24"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label41.ForeColor = System.Drawing.Color.Black
        Me.Label41.Location = New System.Drawing.Point(777, 166)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(17, 12)
        Me.Label41.TabIndex = 433
        Me.Label41.Text = "25"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label42.ForeColor = System.Drawing.Color.Black
        Me.Label42.Location = New System.Drawing.Point(899, 166)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(17, 12)
        Me.Label42.TabIndex = 438
        Me.Label42.Text = "20"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label43.ForeColor = System.Drawing.Color.Black
        Me.Label43.Location = New System.Drawing.Point(876, 166)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(17, 12)
        Me.Label43.TabIndex = 437
        Me.Label43.Text = "21"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label44.ForeColor = System.Drawing.Color.Black
        Me.Label44.Location = New System.Drawing.Point(853, 166)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(17, 12)
        Me.Label44.TabIndex = 436
        Me.Label44.Text = "22"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label45.ForeColor = System.Drawing.Color.Black
        Me.Label45.Location = New System.Drawing.Point(887, 228)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(17, 12)
        Me.Label45.TabIndex = 440
        Me.Label45.Text = "18"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label46.ForeColor = System.Drawing.Color.Black
        Me.Label46.Location = New System.Drawing.Point(864, 228)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(17, 12)
        Me.Label46.TabIndex = 439
        Me.Label46.Text = "19"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label47.ForeColor = System.Drawing.Color.Black
        Me.Label47.Location = New System.Drawing.Point(924, 137)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(17, 12)
        Me.Label47.TabIndex = 442
        Me.Label47.Text = "17"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label48.ForeColor = System.Drawing.Color.Black
        Me.Label48.Location = New System.Drawing.Point(924, 165)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(17, 12)
        Me.Label48.TabIndex = 441
        Me.Label48.Text = "16"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label49.ForeColor = System.Drawing.Color.Black
        Me.Label49.Location = New System.Drawing.Point(977, 138)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(17, 12)
        Me.Label49.TabIndex = 444
        Me.Label49.Text = "15"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label50.ForeColor = System.Drawing.Color.Black
        Me.Label50.Location = New System.Drawing.Point(977, 166)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(17, 12)
        Me.Label50.TabIndex = 443
        Me.Label50.Text = "14"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label51.ForeColor = System.Drawing.Color.Black
        Me.Label51.Location = New System.Drawing.Point(1038, 138)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(17, 12)
        Me.Label51.TabIndex = 446
        Me.Label51.Text = "13"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label52.ForeColor = System.Drawing.Color.Black
        Me.Label52.Location = New System.Drawing.Point(1038, 166)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(17, 12)
        Me.Label52.TabIndex = 445
        Me.Label52.Text = "12"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label53.ForeColor = System.Drawing.Color.Black
        Me.Label53.Location = New System.Drawing.Point(1096, 137)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(17, 12)
        Me.Label53.TabIndex = 448
        Me.Label53.Text = "11"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label54.ForeColor = System.Drawing.Color.Black
        Me.Label54.Location = New System.Drawing.Point(1096, 165)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(17, 12)
        Me.Label54.TabIndex = 447
        Me.Label54.Text = "10"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label55.Location = New System.Drawing.Point(446, 624)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(59, 16)
        Me.Label55.TabIndex = 450
        Me.Label55.Text = "Label55"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label56.Location = New System.Drawing.Point(446, 602)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(59, 16)
        Me.Label56.TabIndex = 449
        Me.Label56.Text = "Label56"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1845, 652)
        Me.Controls.Add(Me.Label55)
        Me.Controls.Add(Me.Label56)
        Me.Controls.Add(Me.Label53)
        Me.Controls.Add(Me.Label54)
        Me.Controls.Add(Me.Label51)
        Me.Controls.Add(Me.Label52)
        Me.Controls.Add(Me.Label49)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.Label48)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Label46)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label43)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.GWIP)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.GW01)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.G21)
        Me.Controls.Add(Me.G20)
        Me.Controls.Add(Me.G19)
        Me.Controls.Add(Me.G18)
        Me.Controls.Add(Me.G17)
        Me.Controls.Add(Me.G16)
        Me.Controls.Add(Me.G15)
        Me.Controls.Add(Me.G14)
        Me.Controls.Add(Me.G13)
        Me.Controls.Add(Me.G12)
        Me.Controls.Add(Me.G11)
        Me.Controls.Add(Me.G10)
        Me.Controls.Add(Me.G09)
        Me.Controls.Add(Me.G08)
        Me.Controls.Add(Me.G07)
        Me.Controls.Add(Me.G06)
        Me.Controls.Add(Me.G05)
        Me.Controls.Add(Me.G04)
        Me.Controls.Add(Me.G03)
        Me.Controls.Add(Me.G02)
        Me.Controls.Add(Me.G01)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox4)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents G01 As System.Windows.Forms.TextBox
    Friend WithEvents G02 As System.Windows.Forms.TextBox
    Friend WithEvents G03 As System.Windows.Forms.TextBox
    Friend WithEvents G04 As System.Windows.Forms.TextBox
    Friend WithEvents G05 As System.Windows.Forms.TextBox
    Friend WithEvents G06 As System.Windows.Forms.TextBox
    Friend WithEvents G07 As System.Windows.Forms.TextBox
    Friend WithEvents G08 As System.Windows.Forms.TextBox
    Friend WithEvents G09 As System.Windows.Forms.TextBox
    Friend WithEvents G10 As System.Windows.Forms.TextBox
    Friend WithEvents G11 As System.Windows.Forms.TextBox
    Friend WithEvents G12 As System.Windows.Forms.TextBox
    Friend WithEvents G13 As System.Windows.Forms.TextBox
    Friend WithEvents G14 As System.Windows.Forms.TextBox
    Friend WithEvents G15 As System.Windows.Forms.TextBox
    Friend WithEvents G16 As System.Windows.Forms.TextBox
    Friend WithEvents G17 As System.Windows.Forms.TextBox
    Friend WithEvents G18 As System.Windows.Forms.TextBox
    Friend WithEvents G19 As System.Windows.Forms.TextBox
    Friend WithEvents G20 As System.Windows.Forms.TextBox
    Friend WithEvents G21 As System.Windows.Forms.TextBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents BK1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents GW01 As System.Windows.Forms.Label
    Friend WithEvents GWIP As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label

End Class
