﻿Imports System.Xml
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.Net
Imports System.Net.Sockets
Imports System.ComponentModel

Public Class Form1
    Dim t As Thread
    Public value(440) As Single                     '開放資料給FFU_Detail顯示使用
    Dim connectFlag As Integer
    Dim tflag As Integer
    Dim ss(44, 14400) As String
    Dim stime As String
    Dim etime As String
    Dim GIP As String
    Dim TOOL_ID As String
    Dim Chamber_ID As String
    Dim stime1, stime2 As String                    'stime1<註記開始記錄日期(建立資料夾使用)> , stime2<註記開始記錄日期(建立檔案名稱使用)>
    Dim myTcpClient As New TcpClient                'Gateway連線
    Dim d As DateTime = DateTime.Now                '紀錄目前時間(辦別時間差使用,為了每分鐘匯出檔案)
    Public FDPTH(45) As String
    Public FDPTL(45) As String
    'Dim sinifilename As String = Application.StartupPath & "\\ffu.ini"

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click   '手動輸出xml檔
        Dim sf As String
        Dim eqp_id As String
        For index = 1 To 9                                                                                      '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
            eqp_id = "F5ESULD10"
            sf = "FFU" + index.ToString
            outputxml(index, sf, eqp_id)
            sf = ""
        Next
        For index = 10 To 13                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
            eqp_id = "F5ESDEV10"
            sf = "FFU" + index.ToString
            outputxml(index, sf, eqp_id)
            sf = ""
        Next
        For index = 14 To 22                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
            eqp_id = "F5ES1CV14"
            sf = "FFU" + index.ToString
            outputxml(index, sf, eqp_id)
            sf = ""
        Next
        For index = 23 To 44                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
            eqp_id = "F5ES1CV15"
            sf = "FFU" + index.ToString
            outputxml(index, sf, eqp_id)
            sf = ""
        Next
    End Sub

    '2.宣告方法:
    '* lpAppName：指向包含Section 名稱的字符串地址
    '* lpKeyName：指向包含Key 名稱的字符串地址
    '* lpDefault：如果Key 值沒有找到，缺省返回缺省的字符串
    '* lpReturnedString：用於保存返回字符串的緩衝區
    '* nSize： 緩衝區的長度
    '* lpFileName ：ini 文件的文件名

    Public Declare Function GetPrivateProfileString Lib "kernel32" _
     Alias "GetPrivateProfileStringA" ( _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpApplicationName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpDefault As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpReturnedString As StringBuilder, _
     ByVal nSize As UInt32, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As UInt32
    Public Declare Function WritePrivateProfileString Lib "kernel32" _
     Alias "WritePrivateProfileStringA" ( _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpApplicationName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpReturnedString As String, _
     <MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As UInt32

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '3.使用方式:
        '讀出ini檔,區段裡的值:
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\ffu.ini"
        '參數一 Section Name
        '參數二 於.ini中的項目
        '參數三 項目的內容
        '參數四 .ini檔的名稱
        '參數五 大小
        '參數六 路徑

        '讀取GatewayIP資料
        GetPrivateProfileString("Gateway_IP", "GW1", "", sKeyValue, nSize, sinifilename)
        GWIP.Text = sKeyValue.ToString
        GIP = GWIP.Text

        '讀取FFU DPT設定資料
        Dim fs As String
        For i = 1 To 44
            fs = "FFU" + i.ToString
            GetPrivateProfileString("DPTH", fs, "", sKeyValue, nSize, sinifilename)
            FDPTH(i) = sKeyValue.ToString
        Next
        For i = 1 To 44
            fs = "FFU" + i.ToString
            GetPrivateProfileString("DPTL", fs, "", sKeyValue, nSize, sinifilename)
            FDPTL(i) = sKeyValue.ToString
        Next

        '開啟背景程式
        BK1.WorkerReportsProgress = True
        BK1.WorkerSupportsCancellation = True

        'Gateway連線測試
        Dim myTcpClient As New TcpClient
        Try
            myTcpClient.Connect(GIP, 502)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Gateway連接錯誤")
        End Try
        If myTcpClient.Connected Then
            connectFlag = 1
            Me.GW01.Visible = False
            myTcpClient.Close()
        Else
            connectFlag = 0
            Me.GW01.Visible = True
        End If
    End Sub

    Public Sub tconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)   '讀取MODBUS TCP/IP 副程式
        'Dim myTcpClient As New TcpClient(IP, 502)                                        'Device IP 與 port
        'Dim myTcpClient As New TcpClient
        'Try
        '    myTcpClient.Connect(IP, 502)
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Gateway 連接錯誤")
        'End Try
        If myTcpClient.Connected Then
            Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()

            Dim byttrs(11) As Byte                                                           '指令集 MODBUS TCP 與 RTU 指令串不相同
            byttrs(0) = 0                                                                    'Transaction Identifier Hi
            byttrs(1) = 1                                                                    'Transaction Identifier Lo
            byttrs(2) = 0                                                                    'Protocol Identifier Hi
            byttrs(3) = 0                                                                    'Protocol Identifier Lo
            byttrs(4) = 0                                                                    'Message Length Hi
            byttrs(5) = 1                                                                    'Message Length Lo
            byttrs(6) = id                                                                   'The Unit Identifier 
            byttrs(7) = 4                                                                    'The Function Code ("4" 為讀取Holding Registers)
            byttrs(8) = adh                                                                  'Starting Address Hi
            byttrs(9) = adl                                                                  'Starting Address Lo
            byttrs(10) = 0                                                                   'No. of Points Hi
            byttrs(11) = 1                                                                   'No. of Points Lo

            myNetworkStream.Write(byttrs, 0, byttrs.Length)                                  '送出命令
            Dim inStream(11) As Byte
            myNetworkStream.Read(inStream, 0, inStream.Length)                               '接收資料

            value = (value << 8) + inStream(9)                                               '把解析出的byte除以除以 2 的 8 次方
            value = (value << 8) + inStream(10)                                              '把解析出的byte除以除以 2 的 8 次方
        End If
    End Sub

    Public Sub twconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)   '寫入MODBUS TCP/IP 副程式
        'Dim myTcpClient As New TcpClient(IP, 502)                                        'Device IP 與 port
        'Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                   '與Device 連線
        'If myTcpClient.Connected Then
        '    Me.Label11.Text = "Server Connected ..."
        'Else
        '    Me.Label11.Text = "Server Not Connected ..."
        'End If
        'Dim myTcpClient As New TcpClient
        'Try
        '    myTcpClient.Connect(IP, 502)
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Gateway 連接錯誤")
        'End Try
        If myTcpClient.Connected Then                                                        '判別與設備是否連線中
            Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                   '與Device 連線
            Dim address(2) As Byte
            Dim vi As Short = value / 256
            If vi * 256 > value Then
                vi = vi - 1
            End If
            address(0) = vi
            address(1) = value Mod 256

            Dim byttrs(13) As Byte                                                           '指令集 MODBUS TCP 與 RTU 指令串不相同
            byttrs(0) = 0                                                                    'Transaction Identifier Hi
            byttrs(1) = 1                                                                    'Transaction Identifier Lo
            byttrs(2) = 0                                                                    'Protocol Identifier Hi
            byttrs(3) = 0                                                                    'Protocol Identifier Lo
            byttrs(4) = 0                                                                    'Message Length Hi
            byttrs(5) = 1                                                                    'Message Length Lo
            byttrs(6) = id                                                                   'The Unit Identifier 
            byttrs(7) = 6                                                                    'The Function Code ("4" 為讀取Holding Registers)
            byttrs(8) = adh                                                                  'Starting Address Hi
            byttrs(9) = adl                                                                  'Starting Address Lo
            byttrs(10) = address(0)                                                          'value Hi
            byttrs(11) = address(1)                                                          'value Lo
            byttrs(12) = 0                                                                   'No. of Points Hi
            byttrs(13) = 1                                                                   'No. of Points Lo

            myNetworkStream.Write(byttrs, 0, byttrs.Length)                                  '送出命令
            Dim inStream(13) As Byte
            myNetworkStream.Read(inStream, 0, inStream.Length)                               '接收資料
            value = 0
            value = (value << 8) + inStream(10)                                              '把解析出的byte除以除以 2 的 8 次方
            value = (value << 8) + inStream(11)                                              '把解析出的byte除以除以 2 的 8 次方)
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        FFU_Detail.Show()                                                                    '叫出FFU Detail設定畫面
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing   '關閉視窗出現確認畫面
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        GIP = GWIP.Text
        '判別是否跨1分鐘, 每分鐘劃出檔案
        Dim d1, d2 As DateTime
        d1 = d.AddMinutes(+1)
        d2 = DateTime.Now
        Label56.Text = d1.ToString
        Label55.Text = d2.ToString
        If d1 <= d2 Then
            Dim sf As String
            Dim eqp_id As String
            For index = 1 To 9                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
                eqp_id = "F5ESULD10"
                sf = "FFU" + index.ToString
                outputxml(index, sf, eqp_id)
                sf = ""
            Next
            For index = 10 To 13                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
                eqp_id = "F5ESDEV10"
                sf = "FFU" + index.ToString
                outputxml(index, sf, eqp_id)
                sf = ""
            Next
            For index = 14 To 22                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
                eqp_id = "F5ES1CV14"
                sf = "FFU" + index.ToString
                outputxml(index, sf, eqp_id)
                sf = ""
            Next
            For index = 23 To 44                                                                                     '把資料輸出至xml檔,每台FFU一個資料檔(依時間建立資料夾)
                eqp_id = "F5ES1CV15"
                sf = "FFU" + index.ToString
                outputxml(index, sf, eqp_id)
                sf = ""
            Next
            stime = ""
            For i = 0 To 44
                For j = 0 To 14400
                    ss(i, j) = ""
                Next
            Next
            d = DateTime.Now                                                                                         '執行完清出矩陣紀錄資料,旗標也清除
            tflag = 0
            stime = ""                                               '註記開始記錄時間(Date資料使用)-清除時間戳記
            stime1 = ""                                              '註記開始記錄日期(建立資料夾使用)-清除時間戳記
            stime2 = ""                                              '註記開始記錄時間(建立檔案名稱使用)-清除時間戳記
            etime = ""                                               '註記資料讀取後時間戳記(Date資料使用)-清除時間戳記
        End If
        If connectFlag = 1 Then
            Me.GW01.Visible = False                                                                                  '設備連線正常,不顯示設備異常警訊
            If BK1.IsBusy Then                                                                                       '判別背景程式是否忙碌中 True:告知使用者忙碌中 False:執行讀取作業並記錄時間戳記
                MessageBox.Show("系統執行中")
                Button9.PerformClick()
            Else
                Try
                    myTcpClient.Connect(GIP, 502)
                Catch ex As Exception
                    'MessageBox.Show(ex.Message, "Gateway 連接錯誤")
                End Try
                If myTcpClient.Connected Then
                    Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                                   '與Device 連線
                    BK1.RunWorkerAsync()                                                                             '啟動背景執行
                    If stime = "" Then
                        stime = Format(DateTime.Today, "yyyy-MM-dd") + " " + Format(Now, "HH:mm:ss.fff")             '註記開始記錄時間(Date資料使用)
                        stime1 = Format(DateTime.Today, "yyyyMMdd")                                                  '註記開始記錄日期(建立資料夾使用)
                        stime2 = stime1 + "_" + Format(Now, "HHmmssfff")                                             '註記開始記錄時間(建立檔案名稱使用)
                    End If
                    etime = Format(DateTime.Today, "yyyy-MM-dd") + " " + Format(Now, "HH:mm:ss.fff")                 '註記資料讀取後時間戳記(Date資料使用)
                    Dim fl As Integer = 0
                    Dim fm, fr, fp As Integer
                    For FI = 1 To 44
                        fl = (FI - 1) * 10
                        fm = fl + 2
                        fr = fl + 4
                        fp = fl + 8
                        ss(FI, tflag) = Format(DateTime.Today, "yyyy-MM-dd") + " " + Format(Now, "HH:mm:ss.fff") + "," + Format(DateTime.Today, "yyyy-MM-dd") + " " + Format(Now, "HH:mm:ss.fff").ToString + ":" + value(fm).ToString + "," + value(fr).ToString + "," + value(fp).ToString '完整資料格式 開始日期 時間,結束日期 時間:Data(MA,RPM,DPT)
                    Next
                    tflag = tflag + 1                                                                                 '資料旗標
                    'myTcpClient.Close()
                End If
            End If
        Else
            Me.GW01.Visible = True
        End If

        '讀取FFU DPT設定資料
        Dim sKeyValue As New StringBuilder(1024)
        Dim KeyValue As New String(" ", 100)
        Dim nSize As UInt32 = Convert.ToUInt32(1024)
        Dim sinifilename As String = Application.StartupPath & "\\ffu.ini"
        Dim fs As String
        For i = 1 To 44
            fs = "FFU" + i.ToString
            GetPrivateProfileString("DPTH", fs, "", sKeyValue, nSize, sinifilename)
            FDPTH(i) = sKeyValue.ToString
        Next
        For i = 1 To 44
            fs = "FFU" + i.ToString
            GetPrivateProfileString("DPTL", fs, "", sKeyValue, nSize, sinifilename)
            FDPTL(i) = sKeyValue.ToString
        Next

        If value(3) + value(13) > 0 Or value(8) < FDPTL(1) Or value(8) > FDPTH(1) Or value(18) < FDPTL(2) Or value(18) > FDPTH(2) Then                    'Gropp1 ID1,ID2
            G01.Visible = True
        Else
            G01.Visible = False
        End If
        If value(23) + value(33) > 0 Or value(28) < FDPTL(3) Or value(28) > FDPTH(3) Or value(38) < FDPTL(4) Or value(38) > FDPTH(4) Then                   'Gropp2 ID3,4
            G02.Visible = True
        Else
            G02.Visible = False
        End If
        If value(43) + value(53) + value(63) > 0 Or value(48) < FDPTL(5) Or value(48) > FDPTH(5) Or value(58) < FDPTL(6) Or value(58) > FDPTH(6) Or value(68) < FDPTL(7) Or value(68) > FDPTH(7) Then       'Gropp3 ID5,6,7
            G03.Visible = True
        Else
            G03.Visible = False
        End If
        If value(73) + value(83) > 0 Or value(78) < FDPTL(8) Or value(78) > FDPTH(8) Or value(88) < FDPTL(9) Or value(88) > FDPTH(9) Then                   'Gropp4 ID8,9
            G04.Visible = True
        Else
            G04.Visible = False
        End If
        If value(93) + value(103) > 0 Or value(98) < FDPTL(10) Or value(98) > FDPTH(10) Or value(108) < FDPTL(11) Or value(108) > FDPTH(11) Then                  'Gropp5 ID10,11
            G05.Visible = True
        Else
            G05.Visible = False
        End If
        If value(113) + value(123) > 0 Or value(118) < FDPTL(12) Or value(118) > FDPTH(12) Or value(128) < FDPTL(13) Or value(128) > FDPTH(13) Then                 'Gropp6 ID12,13
            G06.Visible = True
        Else
            G06.Visible = False
        End If
        If value(133) + value(143) > 0 Or value(138) < FDPTL(14) Or value(138) > FDPTH(14) Or value(148) < FDPTL(15) Or value(148) > FDPTH(15) Then                 'Gropp7 ID14,15
            G07.Visible = True
        Else
            G07.Visible = False
        End If
        If value(153) + value(163) > 0 Or value(158) < FDPTL(16) Or value(158) > FDPTH(16) Or value(168) < FDPTL(17) Or value(168) > FDPTH(17) Then                 'Gropp8 ID16,17
            G08.Visible = True
        Else
            G08.Visible = False
        End If
        If value(173) + value(183) > 0 Or value(178) < FDPTL(18) Or value(178) > FDPTH(18) Or value(188) < FDPTL(19) Or value(188) > FDPTH(19) Then                 'Gropp9 ID18,19
            G09.Visible = True
        Else
            G09.Visible = False
        End If
        If value(193) + value(203) + value(213) > 0 Or value(198) < FDPTL(20) Or value(198) > FDPTH(20) Or value(208) < FDPTL(21) Or value(208) > FDPTH(21) Or value(218) < FDPTL(22) Or value(218) > FDPTH(22) Then    'Gropp10 ID20,21,22
            G10.Visible = True
        Else
            G10.Visible = False
        End If
        If value(223) + value(233) + value(243) > 0 Or value(228) < FDPTL(23) Or value(228) > FDPTH(23) Or value(238) < FDPTL(24) Or value(238) > FDPTH(24) Or value(248) < FDPTL(25) Or value(248) > FDPTH(25) Then    'Gropp11 ID23,24,25
            G11.Visible = True
        Else
            G11.Visible = False
        End If
        If value(253) > 0 Or value(258) < FDPTL(26) Or value(258) > FDPTH(26) Then                              'Gropp12 ID26
            G12.Visible = True
        Else
            G12.Visible = False
        End If
        If value(263) + value(273) + value(283) > 0 Or value(268) < FDPTL(27) Or value(268) > FDPTH(27) Or value(278) < FDPTL(28) Or value(278) > FDPTH(28) Or value(288) < FDPTL(29) Or value(288) > FDPTH(29) Then    'Gropp13 ID27,28,29
            G13.Visible = True
        Else
            G13.Visible = False
        End If
        If value(293) + value(303) > 0 Or value(298) < FDPTL(30) Or value(298) > FDPTH(30) Or value(308) < FDPTL(31) Or value(308) > FDPTH(31) Then                 'Gropp14 ID30,31
            G14.Visible = True
        Else
            G14.Visible = False
        End If
        If value(313) + value(323) > 0 Or value(318) < FDPTL(32) Or value(318) > FDPTH(32) Or value(328) < FDPTL(33) Or value(328) > FDPTH(33) Then                 'Gropp15 ID32,33
            G15.Visible = True
        Else
            G15.Visible = False
        End If
        If value(333) + value(343) > 0 Or value(338) < FDPTL(34) Or value(338) > FDPTH(34) Or value(348) < FDPTL(35) Or value(348) > FDPTH(35) Then                 'Gropp16 ID34,35
            G16.Visible = True
        Else
            G16.Visible = False
        End If
        If value(353) > 0 Or value(358) < FDPTL(36) Or value(358) > FDPTH(36) Then                              'Gropp17 ID36
            G17.Visible = True
        Else
            G17.Visible = False
        End If
        If value(363) + value(373) > 0 Or value(368) < FDPTL(37) Or value(368) > FDPTH(37) Or value(378) < FDPTL(38) Or value(378) > FDPTH(38) Then                 'Gropp18 ID37,38
            G18.Visible = True
        Else
            G18.Visible = False
        End If
        If value(383) + value(393) > 0 Or value(388) < FDPTL(39) Or value(388) > FDPTH(39) Or value(398) < FDPTL(40) Or value(398) > FDPTH(40) Then                 'Gropp19 ID39,40
            G19.Visible = True
        Else
            G19.Visible = False
        End If
        If value(403) + value(413) > 0 Or value(408) < FDPTL(41) Or value(408) > FDPTH(41) Or value(418) < FDPTL(42) Or value(418) > FDPTH(42) Then                 'Gropp20 ID41,42
            G20.Visible = True
        Else
            G20.Visible = False
        End If
        If value(423) + value(433) > 0 Or value(428) < FDPTL(43) Or value(428) > FDPTH(43) Or value(438) < FDPTL(44) Or value(438) > FDPTH(44) Then                 'Gropp21 ID43,44
            G21.Visible = True
        Else
            G21.Visible = False
        End If
    End Sub

    Private Sub BackgroundProcess()                         '背景程式 執行讀取Gateway資料
        GIP = GWIP.Text
        Dim id As Byte = 1
        Dim bi As Integer
        Dim address(2) As Byte
        Dim bii As Short
        Dim index As Integer = 0
        Dim biii As Short
        For bi = 40011 To 40448                             '每次讀取一個位置資料 FFU 1 ~ FFU 44
            biii = bi - 40001
            bii = biii / 256
            If bii * 256 > biii Then
                bii = bii - 1
            End If
            address(0) = bii
            address(1) = biii Mod 256
            tconnect(GIP, id, address(0), address(1), value(index))
            index = index + 1
        Next
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click     '連線測試鈕
        Dim myTcpClient1 As New TcpClient                                                 'Device IP 與 port
        Dim myNetworkStream1 As NetworkStream                                              '與Device 連線
        GIP = GWIP.Text
        Try
            myTcpClient1.Connect(GIP, 502)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Gateway 連接錯誤")
        End Try
        Try
            myNetworkStream1 = myTcpClient1.GetStream()                                     '與Device 連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Gateway 連接錯誤")
            myTcpClient1.Close()
        End Try
        If myTcpClient1.Connected Then                                                     '是否顯示Gateway連線異常 True:不顯示,連線旗標=1 False:顯示,連線旗標=0
            Me.GW01.Visible = False
            connectFlag = 1
            MessageBox.Show("連線測試正常")
        Else
            Me.GW01.Visible = True
            connectFlag = 0
        End If
    End Sub

    Private Sub BK1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BK1.DoWork                         '背景程式 執行讀取Gateway資料
        GIP = GWIP.Text
        Dim id As Byte = 1
        Dim bi As Integer
        Dim address(2) As Byte
        Dim bii As Short
        Dim index As Integer = 0
        Dim biii As Short
        For bi = 40011 To 40449                                                                                                      '每次讀取一個位置資料 FFU 1 ~ FFU 44
            biii = bi - 40001
            bii = biii / 256
            If bii * 256 > biii Then
                bii = bii - 1
            End If
            address(0) = bii
            address(1) = biii Mod 256
            tconnect(GIP, id, address(0), address(1), value(index))
            index = index + 1
        Next
        index = index + 1
        Dim flag As Integer
        For indexx = 1 To 44
            flag = (indexx - 1) * 10
            value(flag + 2) = value(flag + 2) / 100
        Next
    End Sub

    Public Sub outputxml(ffid As Integer, chamber_id As String, eqp_id As String)  '匯出xml檔案資格式
        TOOL_ID = "F5E"
        Dim xdoc As XmlDocument
        Dim xElement As XmlElement
        Dim xChildElement As XmlElement
        Dim xElement2 As XmlElement
        'Dim xChildElement2 As XmlElement (暫定無第二層無子層需求)
        Dim xElement3 As XmlElement
        Dim xChildElement3 As XmlElement
        Dim xElement4 As XmlElement
        Dim xChildElement4 As XmlElement
        'Dim ss As String
        Try
            '建立一個 XmlDocument 物件並加入 Declaration
            xdoc = New XmlDocument
            xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", "yes"))

            '建立根節點物件並加入 XmlDocument 中 (第0層)
            xElement = xdoc.CreateElement("trx")
            xdoc.AppendChild(xElement)

            '第1層節點try
            xChildElement = xdoc.CreateElement("trx_name")
            xChildElement.InnerText = "BUContinuousRunRpt"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("type_id")
            xChildElement.InnerText = "I"
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("abbr_no")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("chamber_id")
            xChildElement.InnerText = chamber_id
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("data_clock")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("eqp_id")
            xChildElement.InnerText = eqp_id
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("eqp_run_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("glass_start_time")
            xChildElement.InnerText = stime
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("glass_end_time")
            xChildElement.InnerText = etime
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("lm_time")
            xChildElement.InnerText = Format(DateTime.Today, "yyyy-MM-dd") + " " + Format(Now, "HH:mm:ss.fff")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("logon_time")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("lot_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("lot_run_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("model_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("op_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("product_code")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("recipe_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("foup_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("seq_recipe_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("sloy_count")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("sheet_id")
            xElement.AppendChild(xChildElement)
            xChildElement = xdoc.CreateElement("slot_no")
            xElement.AppendChild(xChildElement)

            '第2層節點trace_sets
            xElement2 = xdoc.CreateElement("trace_sets")
            xElement.AppendChild(xElement2)

            '第3層節點trace_set
            xElement3 = xdoc.CreateElement("trace_set")
            xElement2.AppendChild(xElement3)
            xChildElement3 = xdoc.CreateElement("sv_name")
            xChildElement3.InnerText = "TRAVRDATETIME,STIME.SETPNAME,A,RPM,DPT"
            xElement3.AppendChild(xChildElement3)
            xElement4 = xdoc.CreateElement("traces")
            xElement3.AppendChild(xElement4)

            '第4層節點traces
            xChildElement4 = xdoc.CreateElement("trace")

            For index = 0 To tflag - 1                                          '根據旗標紀錄所要紀錄之DATA數量
                xChildElement4.InnerText = ss(ffid, index)
                If index < tflag - 1 Then
                    xElement4.AppendChild(xChildElement4)
                    xChildElement4 = xdoc.CreateElement("trace")
                End If
            Next

            xElement4.AppendChild(xChildElement4)                               '結束該層資料格式

            '輸出檔案位置與名稱 
            Dim sss As String
            sss = "D:\APC\" + TOOL_ID + "\" + stime1
            If Dir(sss, vbDirectory) = "" Then                                  '目錄不存在時
                MkDir(sss)                                                      '建立目錄(資料夾為記錄時間)
            End If
            sss = sss + "\CONTITRACE_" + eqp_id + "_" + chamber_id + "_" + stime2 + ".xml"
            xdoc.Save(sss)
        Catch ex As Exception
            MessageBox.Show(ex.Message & System.Environment.NewLine & ex.StackTrace)
        End Try
    End Sub

End Class
