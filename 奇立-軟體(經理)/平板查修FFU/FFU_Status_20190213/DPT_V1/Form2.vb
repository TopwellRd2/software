﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms

Public Class Form2

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Dim pa, db, sb As Integer
        'Dim spn As SerialPort
        Select Case ComboBox2.Text
            Case "None"
                pa = 0
            Case "Odd"
                pa = 1
            Case "Even"
                pa = 2
        End Select

        Select Case ComboBox3.Text
            Case "6"
                db = 6
            Case "7"
                db = 7
            Case "8"
                db = 8
        End Select

        Select Case ComboBox4.Text
            Case "1"
                sb = 1
            Case "1.5"
                sb = 3
            Case "2"
                sb = 2
        End Select
        'Select Case ComboBox5.Text
        '    Case "1"
        '        spn = Form1.SerialPort1
        '        Call Form1.SerialPortSet(spn, 1, ComboBox1.Text, TextBox9.Text, pa, db, sb)
        '        Form1.COM1.Text = ComboBox1.Text                                               '顯示 COM
        '        Form1.ID1.Text = TextBox10.Text                                                '設定站號
        '        Form1.PORT1.Text = "(" & Form1.SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
        '    Case "2"
        '        spn = Form1.SerialPort2
        '        Call Form1.SerialPortSet(spn, 2, ComboBox1.Text, TextBox9.Text, pa, db, sb)
        '        Form1.COM2.Text = ComboBox1.Text                                               '顯示 COM
        '        Form1.ID2.Text = TextBox10.Text                                                '設定站號
        '        Form1.PORT2.Text = "(" & Form1.SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
        '    Case "3"
        '        spn = Form1.SerialPort3
        '        Call Form1.SerialPortSet(spn, 3, ComboBox1.Text, TextBox9.Text, pa, db, sb)
        '        Form1.COM3.Text = ComboBox1.Text                                               '顯示 COM
        '        Form1.ID3.Text = TextBox10.Text                                                '設定站號
        '        Form1.PORT3.Text = "(" & Form1.SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
        '    Case "4"
        '        spn = Form1.SerialPort4
        '        Call Form1.SerialPortSet(spn, 4, ComboBox1.Text, TextBox9.Text, pa, db, sb)
        '        Form1.COM4.Text = ComboBox1.Text                                               '顯示 COM
        '        Form1.ID4.Text = TextBox10.Text                                                '設定站號
        '        Form1.PORT4.Text = "(" & Form1.SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
        'End Select
    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '預設顯示 虛擬Port COM1
        Dim portCount As Integer
        For portCount = 1 To 20
            ComboBox1.Items.Add("COM" & portCount)
        Next
        ComboBox1.SelectedIndex = 0

        ''預設顯示 Parity "Even"
        ComboBox2.Items.Add("None")
        ComboBox2.Items.Add("Odd")
        ComboBox2.Items.Add("Even")
        ComboBox2.SelectedIndex = 0

        '預設顯示 DataBits "7"
        ComboBox3.Items.Add("8")
        ComboBox3.Items.Add("7")
        ComboBox3.Items.Add("6")
        ComboBox3.SelectedIndex = 0

        '預設顯示 Stop Bits "2"
        ComboBox4.Items.Add("1")
        ComboBox4.Items.Add("1.5")
        ComboBox4.Items.Add("2")
        ComboBox4.SelectedIndex = 0

        ''預設顯示 PLC No.
        'For portCount = 1 To 4
        '    ComboBox5.Items.Add(portCount)
        'Next
        'ComboBox5.SelectedIndex = 0

        '預設顯示 BaudRate "9600"
        TextBox9.Text = "9600"

        '預設顯示 PLC通訊站號 "1"
        TextBox10.Text = "1"

        ''PLC 通訊埠設定
        'For i = 1 To 4
        '    ComboBox5.Text = CStr(i)
        Button19.PerformClick()
        'Next
        'ComboBox5.Text = "1"
    End Sub
End Class