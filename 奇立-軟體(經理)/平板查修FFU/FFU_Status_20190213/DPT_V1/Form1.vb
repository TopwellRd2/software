﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms

Public Class Form1
    Dim start As Boolean = False  '判斷是否需要開啟自動讀取資料
    Dim PLCID(3) As String
    Dim connects As Integer = 0   '判斷使用的通訊方式 0:serial port / 1:TCP/IP

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ''MessageBox("關閉")
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            Application.Run()
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '預設顯示 虛擬Port COM1
        Dim portCount As Integer
        For portCount = 1 To 20
            ComboBox1.Items.Add("COM" & portCount)
        Next
        ComboBox1.SelectedIndex = 0

        ''預設顯示 Parity "Even"
        ComboBox2.Items.Add("None")
        ComboBox2.Items.Add("Odd")
        ComboBox2.Items.Add("Even")
        ComboBox2.SelectedIndex = 0

        '預設顯示 DataBits "7"
        ComboBox3.Items.Add("8")
        ComboBox3.Items.Add("7")
        ComboBox3.Items.Add("6")
        ComboBox3.SelectedIndex = 0

        '預設顯示 Stop Bits "2"
        ComboBox4.Items.Add("1")
        ComboBox4.Items.Add("1.5")
        ComboBox4.Items.Add("2")
        ComboBox4.SelectedIndex = 0

        '預設顯示 BaudRate "9600"
        TextBox9.Text = "9600"

        '設定serial port 1
        Button19.PerformClick()

        '現在時間
        Label9.Text = DateTime.Now
        'Timer開啟()->顯示日期時間之Timer
        Timer1.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        'Function 3 (讀取AI) data request is always 8 bytes:
        Dim byttrs(7) As Byte

        'Function 3 (讀取AI) response buffer:  一次只要一個資料所以長度為 (5 + 2 * 比數)-1 -> (5 + 2*1)-1 = 6
        'Dim response As Byte() = New Byte((5 + 2 * 1) - 1) {}
        Dim response(7) As Byte

        '開啟Serialport & Clear out/in buffers
        If SerialPort1.IsOpen = False Then
            SerialPort1.Open()
            SerialPort1.DiscardOutBuffer()                                               'Clear out buffers
            SerialPort1.DiscardInBuffer()                                                'Clear in buffers
        End If

        Dim lonCRC As Long
        Dim intCnt As Integer
        Dim intBit As Integer
        Dim intTemp As Integer
        Dim value As Short

        'byttrs(0) = 1                                                                   'ID 1"
        byttrs(0) = ID1.Text                                                             'ID
        byttrs(1) = 4                                                                    '讀取 word (AI)  tm-AD5C 設定為"4",一般MODBUS為"6""  
        byttrs(2) = 0                                                                    'Starting Address Hi"
        byttrs(3) = 6                                                                    'Starting Address Lo"
        byttrs(4) = 0                                                                    'No. of Points Hi"
        byttrs(5) = 1                                                                    'No. of Points Lo

        ''計算CRC MODBUS通訊需要的代碼
        ''A procedure for generating an LRC is:
        '1. Add all bytes in the message, excluding the starting ‘colon’ and ending
        '   CRLF. Add them into an 8–bit field, so that carries will be discarded.
        '2. Subtract the final field value from FF hex (all 1’s), to produce the    
        '   ones–complement.
        '3. Add 1 to produce the twos–complement.
        lonCRC = &HFFFF&
        For intCnt = 0 To 5
            lonCRC = lonCRC Xor byttrs(intCnt)
            For intBit = 0 To 7
                intTemp = lonCRC Mod 2
                lonCRC = lonCRC \ 2
                If intTemp = 1 Then
                    lonCRC = lonCRC Xor &HA001&
                End If
            Next intBit
        Next intCnt
        byttrs(6) = lonCRC Mod 256
        byttrs(7) = lonCRC \ 256

        SerialPort1.Write(byttrs, 0, byttrs.Length)                                      '送出命令字串

        Dim i As Integer = 0
        Thread.Sleep(200)
        While (SerialPort1.BytesToRead <> 0)                                             '解析設備回覆的字串
            response(i) = Byte.Parse(SerialPort1.ReadByte())
            i += 1
        End While

        value = 0                                                                        '初始值設為0
        value = (value << 8) + response(3)                                               '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + response(4)                                               '把解析出的byte除以除以 2 的 8 次方
        RPMS_1.Text = value / 10

        SerialPort1.Close()
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Dim pa, db, sb As Integer
        Dim spn As SerialPort
        Select Case ComboBox2.Text
            Case "None"
                pa = 0
            Case "Odd"
                pa = 1
            Case "Even"
                pa = 2
        End Select

        Select Case ComboBox3.Text
            Case "6"
                db = 6
            Case "7"
                db = 7
            Case "8"
                db = 8
        End Select

        Select Case ComboBox4.Text
            Case "1"
                sb = 1
            Case "1.5"
                sb = 3
            Case "2"
                sb = 2
        End Select

        spn = SerialPort1
        Call SerialPortSet(spn, 1, ComboBox1.Text, TextBox9.Text, pa, db, sb)

    End Sub

    Public Sub SerialPortSet(ByRef sp As SerialPort, no As Integer, com As String, br As String, pa As String, db As String, sb As String) 'Set PLC No. Serial Port
        sp.Close()
        sp.PortName = com
        sp.BaudRate = br                                                                 'Set BaudRate
        sp.Parity = pa                                                                   'Set Parity
        sp.DataBits = db                                                                 'Set DataBits
        sp.StopBits = sb                                                                 'Set Stop Bits
        Try
            If sp.IsOpen = False Then
                sp.Open()
            End If
        Catch ex As Exception
            MsgBox("PLC No." & no & " : " & sp.PortName.ToString & " 開啟失敗 , 通訊埠 " & com & " 不存在 / 重複開啟 .", MsgBoxStyle.Critical)
        End Try
        sp.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick        '兩秒執行一次 timer1.Interval = 2000 ms
        Label9.Text = DateTime.Now
        'Button1.PerformClick()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If start = False Then                                                            '切換成Auto modle
            start = True
            Timer1.Enabled = True
            Button3.Text = "Auto modle"
        Else                                                                             '切換成Manually modle
            start = False
            Timer1.Enabled = False
            Button3.Text = "Manually modle"
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Dim value As Short
        getdata(SerialPort1, 1, 0, value)
        Status_1.Text = value
        getdata(SerialPort1, 1, 1, value)
        RPMS_1.Text = value
        getdata(SerialPort1, 1, 3, value)
        RPM_1.Text = value
        getdata(SerialPort1, 1, 4, value)
        AlarmCode_1.Text = value
    End Sub

    Public Sub getdata(sp As SerialPort, id As Byte, channel As Byte, ByRef value As Short) 'Get Device Data
        'Function 3 (讀取AI) data request is always 8 bytes:
        Dim byttrs(7) As Byte

        'Function 3 (讀取AI) response buffer:  一次只要一個資料所以長度為 (5 + 2 * 比數)-1 -> (5 + 2*1)-1 = 6
        Dim response As Byte() = New Byte((5 + 2 * 1) - 1) {}

        '開啟Serialport & Clear out/in buffers
        If sp.IsOpen = False Then
            sp.Open()
            sp.DiscardOutBuffer()                                                        'Clear out buffers
            sp.DiscardInBuffer()                                                         'Clear in buffers
        End If

        Dim lonCRC As Long
        Dim intCnt As Integer
        Dim intBit As Integer
        Dim intTemp As Integer

        byttrs(0) = id                                                                   'ID
        byttrs(1) = 4                                                                    '讀取 word (AI)  tm-AD5C 設定為"4",一般MODBUS為"6""  
        byttrs(2) = 0                                                                    'Starting Address Hi"
        byttrs(3) = channel                                                              'Starting Address Lo"
        byttrs(4) = 0                                                                    'No. of Points Hi"
        byttrs(5) = 1                                                                    'No. of Points Lo
        ''byttrs(1) 讀取4xxxx的位置 FunctionCode為6, tm-ad5c需設定成"4"
        ''byttrs(2)&byttrs(3)為讀取位置起始值, tm-ad5c 只有五個channel 所以只要byttrs(4)調整0~4即可

        '計算CRC MODBUS通訊需要的代碼
        lonCRC = &HFFFF&
        For intCnt = 0 To 5
            lonCRC = lonCRC Xor byttrs(intCnt)
            For intBit = 0 To 7
                intTemp = lonCRC Mod 2
                lonCRC = lonCRC \ 2
                If intTemp = 1 Then
                    lonCRC = lonCRC Xor &HA001&
                End If
            Next intBit
        Next intCnt
        byttrs(6) = lonCRC Mod 256
        byttrs(7) = lonCRC \ 256

        sp.Write(byttrs, 0, byttrs.Length)                                      '送出命令字串

        Dim i As Integer = 0
        Thread.Sleep(200)
        While (sp.BytesToRead <> 0)                                             '解析設備回覆的字串
            response(i) = Byte.Parse(sp.ReadByte())
            i += 1
        End While
        value = 0                                                                        '初始值設為0
        value = (value << 8) + response(3)                                               '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + response(4)                                               '把解析出的byte除以除以 2 的 8 次方
        sp.Close()
    End Sub

    Public Sub Tdata(RH As Integer, RL As Integer, EH As Short, EL As Short, ByRef value As Short) 'Get Device Data
        value = (value - RL) * (EH - EL) / (RH - RL) + EL
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Form2.Show()
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Dim value As Short
        getdata(SerialPort1, 1, 0, value)
        Status_1.Text = value
        getdata(SerialPort1, 1, 1, value)
        RPMS_1.Text = value
        getdata(SerialPort1, 1, 4, value)
        RPM_1.Text = value
        getdata(SerialPort1, 1, 3, value)
        AlarmCode_1.Text = value

        getdata(SerialPort1, 64, 0, value)
        Status_64.Text = value
        getdata(SerialPort1, 64, 1, value)
        RPMS_64.Text = value
        getdata(SerialPort1, 64, 6, value)
        RPM_64.Text = value
        getdata(SerialPort1, 64, 7, value)
        AlarmCode_64.Text = value

        getdata(SerialPort1, 65, 0, value)
        Status_64.Text = value
        getdata(SerialPort1, 65, 1, value)
        RPMS_64.Text = value
        getdata(SerialPort1, 65, 6, value)
        RPM_64.Text = value
        getdata(SerialPort1, 65, 7, value)
        AlarmCode_64.Text = value


    End Sub
End Class
