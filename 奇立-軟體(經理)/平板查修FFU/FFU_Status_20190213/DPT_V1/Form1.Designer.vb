﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.RPM_2 = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RPM_1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.AlarmCode_2 = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PORT1 = New System.Windows.Forms.Label()
        Me.COM1 = New System.Windows.Forms.Label()
        Me.ID2 = New System.Windows.Forms.Label()
        Me.PORT2 = New System.Windows.Forms.Label()
        Me.COM2 = New System.Windows.Forms.Label()
        Me.ID3 = New System.Windows.Forms.Label()
        Me.PORT3 = New System.Windows.Forms.Label()
        Me.COM3 = New System.Windows.Forms.Label()
        Me.ID4 = New System.Windows.Forms.Label()
        Me.PORT4 = New System.Windows.Forms.Label()
        Me.COM4 = New System.Windows.Forms.Label()
        Me.Status_1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RPMS_1 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_1 = New System.Windows.Forms.TextBox()
        Me.Status_2 = New System.Windows.Forms.TextBox()
        Me.RPMS_2 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Status_3 = New System.Windows.Forms.TextBox()
        Me.RPMS_3 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_3 = New System.Windows.Forms.TextBox()
        Me.RPM_3 = New System.Windows.Forms.TextBox()
        Me.ID1 = New System.Windows.Forms.Label()
        Me.Status_4 = New System.Windows.Forms.TextBox()
        Me.RPMS_4 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_4 = New System.Windows.Forms.TextBox()
        Me.RPM_4 = New System.Windows.Forms.TextBox()
        Me.Status_5 = New System.Windows.Forms.TextBox()
        Me.RPMS_5 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_5 = New System.Windows.Forms.TextBox()
        Me.RPM_5 = New System.Windows.Forms.TextBox()
        Me.Status_6 = New System.Windows.Forms.TextBox()
        Me.RPMS_6 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_6 = New System.Windows.Forms.TextBox()
        Me.RPM_6 = New System.Windows.Forms.TextBox()
        Me.Status_7 = New System.Windows.Forms.TextBox()
        Me.RPMS_7 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_7 = New System.Windows.Forms.TextBox()
        Me.RPM_7 = New System.Windows.Forms.TextBox()
        Me.Status_8 = New System.Windows.Forms.TextBox()
        Me.RPMS_8 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_8 = New System.Windows.Forms.TextBox()
        Me.RPM_8 = New System.Windows.Forms.TextBox()
        Me.Status_9 = New System.Windows.Forms.TextBox()
        Me.RPMS_9 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_9 = New System.Windows.Forms.TextBox()
        Me.RPM_9 = New System.Windows.Forms.TextBox()
        Me.Status_10 = New System.Windows.Forms.TextBox()
        Me.RPMS_10 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_10 = New System.Windows.Forms.TextBox()
        Me.RPM_10 = New System.Windows.Forms.TextBox()
        Me.Status_12 = New System.Windows.Forms.TextBox()
        Me.RPMS_12 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_12 = New System.Windows.Forms.TextBox()
        Me.RPM_12 = New System.Windows.Forms.TextBox()
        Me.Status_11 = New System.Windows.Forms.TextBox()
        Me.RPMS_11 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_11 = New System.Windows.Forms.TextBox()
        Me.RPM_11 = New System.Windows.Forms.TextBox()
        Me.Status_24 = New System.Windows.Forms.TextBox()
        Me.RPMS_24 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_24 = New System.Windows.Forms.TextBox()
        Me.RPM_24 = New System.Windows.Forms.TextBox()
        Me.Status_23 = New System.Windows.Forms.TextBox()
        Me.RPMS_23 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_23 = New System.Windows.Forms.TextBox()
        Me.RPM_23 = New System.Windows.Forms.TextBox()
        Me.Status_22 = New System.Windows.Forms.TextBox()
        Me.RPMS_22 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_22 = New System.Windows.Forms.TextBox()
        Me.RPM_22 = New System.Windows.Forms.TextBox()
        Me.Status_21 = New System.Windows.Forms.TextBox()
        Me.RPMS_21 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_21 = New System.Windows.Forms.TextBox()
        Me.RPM_21 = New System.Windows.Forms.TextBox()
        Me.Status_20 = New System.Windows.Forms.TextBox()
        Me.RPMS_20 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_20 = New System.Windows.Forms.TextBox()
        Me.RPM_20 = New System.Windows.Forms.TextBox()
        Me.Status_19 = New System.Windows.Forms.TextBox()
        Me.RPMS_19 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_19 = New System.Windows.Forms.TextBox()
        Me.RPM_19 = New System.Windows.Forms.TextBox()
        Me.Status_18 = New System.Windows.Forms.TextBox()
        Me.RPMS_18 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_18 = New System.Windows.Forms.TextBox()
        Me.RPM_18 = New System.Windows.Forms.TextBox()
        Me.Status_17 = New System.Windows.Forms.TextBox()
        Me.RPMS_17 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_17 = New System.Windows.Forms.TextBox()
        Me.RPM_17 = New System.Windows.Forms.TextBox()
        Me.Status_16 = New System.Windows.Forms.TextBox()
        Me.RPMS_16 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_16 = New System.Windows.Forms.TextBox()
        Me.RPM_16 = New System.Windows.Forms.TextBox()
        Me.Status_15 = New System.Windows.Forms.TextBox()
        Me.RPMS_15 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_15 = New System.Windows.Forms.TextBox()
        Me.RPM_15 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Status_14 = New System.Windows.Forms.TextBox()
        Me.RPMS_14 = New System.Windows.Forms.TextBox()
        Me.RPMS_13 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_13 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.AlarmCode_14 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Status_13 = New System.Windows.Forms.TextBox()
        Me.RPM_14 = New System.Windows.Forms.TextBox()
        Me.RPM_13 = New System.Windows.Forms.TextBox()
        Me.Status_36 = New System.Windows.Forms.TextBox()
        Me.RPMS_36 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_36 = New System.Windows.Forms.TextBox()
        Me.RPM_36 = New System.Windows.Forms.TextBox()
        Me.Status_35 = New System.Windows.Forms.TextBox()
        Me.RPMS_35 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_35 = New System.Windows.Forms.TextBox()
        Me.RPM_35 = New System.Windows.Forms.TextBox()
        Me.Status_34 = New System.Windows.Forms.TextBox()
        Me.RPMS_34 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_34 = New System.Windows.Forms.TextBox()
        Me.RPM_34 = New System.Windows.Forms.TextBox()
        Me.Status_33 = New System.Windows.Forms.TextBox()
        Me.RPMS_33 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_33 = New System.Windows.Forms.TextBox()
        Me.RPM_33 = New System.Windows.Forms.TextBox()
        Me.Status_32 = New System.Windows.Forms.TextBox()
        Me.RPMS_32 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_32 = New System.Windows.Forms.TextBox()
        Me.RPM_32 = New System.Windows.Forms.TextBox()
        Me.Status_31 = New System.Windows.Forms.TextBox()
        Me.RPMS_31 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_31 = New System.Windows.Forms.TextBox()
        Me.RPM_31 = New System.Windows.Forms.TextBox()
        Me.Status_30 = New System.Windows.Forms.TextBox()
        Me.RPMS_30 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_30 = New System.Windows.Forms.TextBox()
        Me.RPM_30 = New System.Windows.Forms.TextBox()
        Me.Status_29 = New System.Windows.Forms.TextBox()
        Me.RPMS_29 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_29 = New System.Windows.Forms.TextBox()
        Me.RPM_29 = New System.Windows.Forms.TextBox()
        Me.Status_28 = New System.Windows.Forms.TextBox()
        Me.RPMS_28 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_28 = New System.Windows.Forms.TextBox()
        Me.RPM_28 = New System.Windows.Forms.TextBox()
        Me.Status_27 = New System.Windows.Forms.TextBox()
        Me.RPMS_27 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_27 = New System.Windows.Forms.TextBox()
        Me.RPM_27 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Status_26 = New System.Windows.Forms.TextBox()
        Me.RPMS_26 = New System.Windows.Forms.TextBox()
        Me.RPMS_25 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_25 = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.AlarmCode_26 = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Status_25 = New System.Windows.Forms.TextBox()
        Me.RPM_26 = New System.Windows.Forms.TextBox()
        Me.RPM_25 = New System.Windows.Forms.TextBox()
        Me.Status_72 = New System.Windows.Forms.TextBox()
        Me.RPMS_72 = New System.Windows.Forms.TextBox()
        Me.TextBox149 = New System.Windows.Forms.TextBox()
        Me.RPM_72 = New System.Windows.Forms.TextBox()
        Me.Status_71 = New System.Windows.Forms.TextBox()
        Me.RPMS_71 = New System.Windows.Forms.TextBox()
        Me.TextBox153 = New System.Windows.Forms.TextBox()
        Me.RPM_71 = New System.Windows.Forms.TextBox()
        Me.Status_70 = New System.Windows.Forms.TextBox()
        Me.RPMS_70 = New System.Windows.Forms.TextBox()
        Me.TextBox157 = New System.Windows.Forms.TextBox()
        Me.RPM_70 = New System.Windows.Forms.TextBox()
        Me.Status_69 = New System.Windows.Forms.TextBox()
        Me.RPMS_69 = New System.Windows.Forms.TextBox()
        Me.TextBox161 = New System.Windows.Forms.TextBox()
        Me.RPM_69 = New System.Windows.Forms.TextBox()
        Me.Status_68 = New System.Windows.Forms.TextBox()
        Me.RPMS_68 = New System.Windows.Forms.TextBox()
        Me.TextBox165 = New System.Windows.Forms.TextBox()
        Me.RPM_68 = New System.Windows.Forms.TextBox()
        Me.Status_67 = New System.Windows.Forms.TextBox()
        Me.RPMS_67 = New System.Windows.Forms.TextBox()
        Me.TextBox169 = New System.Windows.Forms.TextBox()
        Me.RPM_67 = New System.Windows.Forms.TextBox()
        Me.Status_66 = New System.Windows.Forms.TextBox()
        Me.RPMS_66 = New System.Windows.Forms.TextBox()
        Me.TextBox173 = New System.Windows.Forms.TextBox()
        Me.RPM_66 = New System.Windows.Forms.TextBox()
        Me.Status_65 = New System.Windows.Forms.TextBox()
        Me.RPMS_65 = New System.Windows.Forms.TextBox()
        Me.TextBox177 = New System.Windows.Forms.TextBox()
        Me.RPM_65 = New System.Windows.Forms.TextBox()
        Me.Status_64 = New System.Windows.Forms.TextBox()
        Me.RPMS_64 = New System.Windows.Forms.TextBox()
        Me.RPM_64 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_64 = New System.Windows.Forms.TextBox()
        Me.Status_63 = New System.Windows.Forms.TextBox()
        Me.RPMS_63 = New System.Windows.Forms.TextBox()
        Me.TextBox185 = New System.Windows.Forms.TextBox()
        Me.RPM_63 = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Status_62 = New System.Windows.Forms.TextBox()
        Me.RPMS_62 = New System.Windows.Forms.TextBox()
        Me.RPMS_61 = New System.Windows.Forms.TextBox()
        Me.TextBox190 = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TextBox191 = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Status_61 = New System.Windows.Forms.TextBox()
        Me.RPM_62 = New System.Windows.Forms.TextBox()
        Me.RPM_61 = New System.Windows.Forms.TextBox()
        Me.Status_60 = New System.Windows.Forms.TextBox()
        Me.RPMS_60 = New System.Windows.Forms.TextBox()
        Me.TextBox197 = New System.Windows.Forms.TextBox()
        Me.RPM_60 = New System.Windows.Forms.TextBox()
        Me.Status_59 = New System.Windows.Forms.TextBox()
        Me.RPMS_59 = New System.Windows.Forms.TextBox()
        Me.TextBox201 = New System.Windows.Forms.TextBox()
        Me.RPM_59 = New System.Windows.Forms.TextBox()
        Me.Status_58 = New System.Windows.Forms.TextBox()
        Me.RPMS_58 = New System.Windows.Forms.TextBox()
        Me.TextBox205 = New System.Windows.Forms.TextBox()
        Me.RPM_58 = New System.Windows.Forms.TextBox()
        Me.Status_57 = New System.Windows.Forms.TextBox()
        Me.RPMS_57 = New System.Windows.Forms.TextBox()
        Me.TextBox209 = New System.Windows.Forms.TextBox()
        Me.RPM_57 = New System.Windows.Forms.TextBox()
        Me.Status_56 = New System.Windows.Forms.TextBox()
        Me.RPMS_56 = New System.Windows.Forms.TextBox()
        Me.TextBox213 = New System.Windows.Forms.TextBox()
        Me.RPM_56 = New System.Windows.Forms.TextBox()
        Me.Status_55 = New System.Windows.Forms.TextBox()
        Me.RPMS_55 = New System.Windows.Forms.TextBox()
        Me.TextBox217 = New System.Windows.Forms.TextBox()
        Me.RPM_55 = New System.Windows.Forms.TextBox()
        Me.Status_54 = New System.Windows.Forms.TextBox()
        Me.RPMS_54 = New System.Windows.Forms.TextBox()
        Me.TextBox221 = New System.Windows.Forms.TextBox()
        Me.RPM_54 = New System.Windows.Forms.TextBox()
        Me.Status_53 = New System.Windows.Forms.TextBox()
        Me.RPMS_53 = New System.Windows.Forms.TextBox()
        Me.TextBox225 = New System.Windows.Forms.TextBox()
        Me.RPM_53 = New System.Windows.Forms.TextBox()
        Me.Status_52 = New System.Windows.Forms.TextBox()
        Me.RPMS_52 = New System.Windows.Forms.TextBox()
        Me.TextBox229 = New System.Windows.Forms.TextBox()
        Me.RPM_52 = New System.Windows.Forms.TextBox()
        Me.Status_51 = New System.Windows.Forms.TextBox()
        Me.RPMS_51 = New System.Windows.Forms.TextBox()
        Me.TextBox233 = New System.Windows.Forms.TextBox()
        Me.RPM_51 = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Status_50 = New System.Windows.Forms.TextBox()
        Me.RPMS_50 = New System.Windows.Forms.TextBox()
        Me.RPMS_49 = New System.Windows.Forms.TextBox()
        Me.TextBox238 = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.TextBox239 = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Status_49 = New System.Windows.Forms.TextBox()
        Me.RPM_50 = New System.Windows.Forms.TextBox()
        Me.RPM_49 = New System.Windows.Forms.TextBox()
        Me.Status_48 = New System.Windows.Forms.TextBox()
        Me.RPMS_48 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_48 = New System.Windows.Forms.TextBox()
        Me.RPM_48 = New System.Windows.Forms.TextBox()
        Me.Status_47 = New System.Windows.Forms.TextBox()
        Me.RPMS_47 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_47 = New System.Windows.Forms.TextBox()
        Me.RPM_47 = New System.Windows.Forms.TextBox()
        Me.Status_46 = New System.Windows.Forms.TextBox()
        Me.RPMS_46 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_46 = New System.Windows.Forms.TextBox()
        Me.RPM_46 = New System.Windows.Forms.TextBox()
        Me.Status_45 = New System.Windows.Forms.TextBox()
        Me.RPMS_45 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_45 = New System.Windows.Forms.TextBox()
        Me.RPM_45 = New System.Windows.Forms.TextBox()
        Me.Status_44 = New System.Windows.Forms.TextBox()
        Me.RPMS_44 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_44 = New System.Windows.Forms.TextBox()
        Me.RPM_44 = New System.Windows.Forms.TextBox()
        Me.Status_43 = New System.Windows.Forms.TextBox()
        Me.RPMS_43 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_43 = New System.Windows.Forms.TextBox()
        Me.RPM_43 = New System.Windows.Forms.TextBox()
        Me.Status_42 = New System.Windows.Forms.TextBox()
        Me.RPMS_42 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_42 = New System.Windows.Forms.TextBox()
        Me.RPM_42 = New System.Windows.Forms.TextBox()
        Me.Status_41 = New System.Windows.Forms.TextBox()
        Me.RPMS_41 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_41 = New System.Windows.Forms.TextBox()
        Me.RPM_41 = New System.Windows.Forms.TextBox()
        Me.Status_40 = New System.Windows.Forms.TextBox()
        Me.RPMS_40 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_40 = New System.Windows.Forms.TextBox()
        Me.RPM_40 = New System.Windows.Forms.TextBox()
        Me.Status_39 = New System.Windows.Forms.TextBox()
        Me.RPMS_39 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_39 = New System.Windows.Forms.TextBox()
        Me.RPM_39 = New System.Windows.Forms.TextBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Status_38 = New System.Windows.Forms.TextBox()
        Me.RPMS_38 = New System.Windows.Forms.TextBox()
        Me.RPMS_37 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_37 = New System.Windows.Forms.TextBox()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.AlarmCode_38 = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Status_37 = New System.Windows.Forms.TextBox()
        Me.RPM_38 = New System.Windows.Forms.TextBox()
        Me.RPM_37 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 270)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 12)
        Me.Label14.TabIndex = 274
        Me.Label14.Text = "Update Status"
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(98, 202)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(101, 23)
        Me.Button19.TabIndex = 273
        Me.Button19.Text = "設定Set"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(97, 176)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox4.TabIndex = 270
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(97, 149)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox3.TabIndex = 269
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(97, 122)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox2.TabIndex = 268
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(98, 66)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox1.TabIndex = 267
        '
        'RPM_2
        '
        Me.RPM_2.Location = New System.Drawing.Point(422, 60)
        Me.RPM_2.Name = "RPM_2"
        Me.RPM_2.Size = New System.Drawing.Size(55, 22)
        Me.RPM_2.TabIndex = 265
        Me.RPM_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(98, 265)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(99, 23)
        Me.Button3.TabIndex = 264
        Me.Button3.Text = "Manual Update"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(44, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 12)
        Me.Label7.TabIndex = 263
        Me.Label7.Text = "Stop Bits"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(47, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 12)
        Me.Label6.TabIndex = 262
        Me.Label6.Text = "DataBits"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(59, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 12)
        Me.Label5.TabIndex = 261
        Me.Label5.Text = "Parity"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 12)
        Me.Label4.TabIndex = 260
        Me.Label4.Text = "BaudRate"
        '
        'RPM_1
        '
        Me.RPM_1.Location = New System.Drawing.Point(422, 32)
        Me.RPM_1.Name = "RPM_1"
        Me.RPM_1.Size = New System.Drawing.Size(55, 22)
        Me.RPM_1.TabIndex = 259
        Me.RPM_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 12)
        Me.Label3.TabIndex = 258
        Me.Label3.Text = "Select COM port"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label9.Location = New System.Drawing.Point(126, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 16)
        Me.Label9.TabIndex = 276
        Me.Label9.Text = "Label9"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 16)
        Me.Label8.TabIndex = 275
        Me.Label8.Text = "Date  / Time"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(352, 13)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(46, 12)
        Me.Label30.TabIndex = 280
        Me.Label30.Text = "RPM Set"
        '
        'AlarmCode_2
        '
        Me.AlarmCode_2.Location = New System.Drawing.Point(501, 60)
        Me.AlarmCode_2.Name = "AlarmCode_2"
        Me.AlarmCode_2.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_2.TabIndex = 279
        Me.AlarmCode_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(97, 293)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 23)
        Me.Button1.TabIndex = 281
        Me.Button1.Text = "讀取"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PORT1
        '
        Me.PORT1.AutoSize = True
        Me.PORT1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT1.Location = New System.Drawing.Point(223, 69)
        Me.PORT1.Name = "PORT1"
        Me.PORT1.Size = New System.Drawing.Size(35, 13)
        Me.PORT1.TabIndex = 287
        Me.PORT1.Text = "ID:02"
        '
        'COM1
        '
        Me.COM1.AutoSize = True
        Me.COM1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM1.Location = New System.Drawing.Point(223, 41)
        Me.COM1.Name = "COM1"
        Me.COM1.Size = New System.Drawing.Size(35, 13)
        Me.COM1.TabIndex = 286
        Me.COM1.Text = "ID:01"
        '
        'ID2
        '
        Me.ID2.AutoSize = True
        Me.ID2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID2.Location = New System.Drawing.Point(223, 181)
        Me.ID2.Name = "ID2"
        Me.ID2.Size = New System.Drawing.Size(35, 13)
        Me.ID2.TabIndex = 291
        Me.ID2.Text = "ID:06"
        '
        'PORT2
        '
        Me.PORT2.AutoSize = True
        Me.PORT2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT2.Location = New System.Drawing.Point(223, 153)
        Me.PORT2.Name = "PORT2"
        Me.PORT2.Size = New System.Drawing.Size(35, 13)
        Me.PORT2.TabIndex = 290
        Me.PORT2.Text = "ID:05"
        '
        'COM2
        '
        Me.COM2.AutoSize = True
        Me.COM2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM2.Location = New System.Drawing.Point(223, 125)
        Me.COM2.Name = "COM2"
        Me.COM2.Size = New System.Drawing.Size(35, 13)
        Me.COM2.TabIndex = 289
        Me.COM2.Text = "ID:04"
        '
        'ID3
        '
        Me.ID3.AutoSize = True
        Me.ID3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID3.Location = New System.Drawing.Point(223, 265)
        Me.ID3.Name = "ID3"
        Me.ID3.Size = New System.Drawing.Size(35, 13)
        Me.ID3.TabIndex = 294
        Me.ID3.Text = "ID:09"
        '
        'PORT3
        '
        Me.PORT3.AutoSize = True
        Me.PORT3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT3.Location = New System.Drawing.Point(223, 237)
        Me.PORT3.Name = "PORT3"
        Me.PORT3.Size = New System.Drawing.Size(35, 13)
        Me.PORT3.TabIndex = 293
        Me.PORT3.Text = "ID:08"
        '
        'COM3
        '
        Me.COM3.AutoSize = True
        Me.COM3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM3.Location = New System.Drawing.Point(223, 209)
        Me.COM3.Name = "COM3"
        Me.COM3.Size = New System.Drawing.Size(35, 13)
        Me.COM3.TabIndex = 292
        Me.COM3.Text = "ID:07"
        '
        'ID4
        '
        Me.ID4.AutoSize = True
        Me.ID4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID4.Location = New System.Drawing.Point(223, 349)
        Me.ID4.Name = "ID4"
        Me.ID4.Size = New System.Drawing.Size(35, 13)
        Me.ID4.TabIndex = 297
        Me.ID4.Text = "ID:12"
        '
        'PORT4
        '
        Me.PORT4.AutoSize = True
        Me.PORT4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT4.Location = New System.Drawing.Point(223, 321)
        Me.PORT4.Name = "PORT4"
        Me.PORT4.Size = New System.Drawing.Size(35, 13)
        Me.PORT4.TabIndex = 296
        Me.PORT4.Text = "ID:11"
        '
        'COM4
        '
        Me.COM4.AutoSize = True
        Me.COM4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM4.Location = New System.Drawing.Point(223, 293)
        Me.COM4.Name = "COM4"
        Me.COM4.Size = New System.Drawing.Size(35, 13)
        Me.COM4.TabIndex = 295
        Me.COM4.Text = "ID:10"
        '
        'Status_1
        '
        Me.Status_1.Location = New System.Drawing.Point(264, 32)
        Me.Status_1.Name = "Status_1"
        Me.Status_1.Size = New System.Drawing.Size(55, 22)
        Me.Status_1.TabIndex = 277
        Me.Status_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(287, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 12)
        Me.Label1.TabIndex = 278
        Me.Label1.Text = "Status"
        '
        'RPMS_1
        '
        Me.RPMS_1.Location = New System.Drawing.Point(343, 32)
        Me.RPMS_1.Name = "RPMS_1"
        Me.RPMS_1.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_1.TabIndex = 300
        Me.RPMS_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_1
        '
        Me.AlarmCode_1.Location = New System.Drawing.Point(501, 32)
        Me.AlarmCode_1.Name = "AlarmCode_1"
        Me.AlarmCode_1.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_1.TabIndex = 298
        Me.AlarmCode_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_2
        '
        Me.Status_2.Location = New System.Drawing.Point(264, 60)
        Me.Status_2.Name = "Status_2"
        Me.Status_2.Size = New System.Drawing.Size(55, 22)
        Me.Status_2.TabIndex = 305
        Me.Status_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_2
        '
        Me.RPMS_2.Location = New System.Drawing.Point(343, 60)
        Me.RPMS_2.Name = "RPMS_2"
        Me.RPMS_2.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_2.TabIndex = 303
        Me.RPMS_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(97, 94)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(100, 22)
        Me.TextBox9.TabIndex = 310
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(98, 232)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(99, 23)
        Me.Button6.TabIndex = 317
        Me.Button6.Text = "Set Form"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(494, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 12)
        Me.Label11.TabIndex = 319
        Me.Label11.Text = "Alarm Code"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(448, 13)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(29, 12)
        Me.Label22.TabIndex = 318
        Me.Label22.Text = "RPM"
        '
        'Status_3
        '
        Me.Status_3.Location = New System.Drawing.Point(264, 90)
        Me.Status_3.Name = "Status_3"
        Me.Status_3.Size = New System.Drawing.Size(55, 22)
        Me.Status_3.TabIndex = 324
        Me.Status_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_3
        '
        Me.RPMS_3.Location = New System.Drawing.Point(343, 90)
        Me.RPMS_3.Name = "RPMS_3"
        Me.RPMS_3.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_3.TabIndex = 323
        Me.RPMS_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_3
        '
        Me.AlarmCode_3.Location = New System.Drawing.Point(501, 90)
        Me.AlarmCode_3.Name = "AlarmCode_3"
        Me.AlarmCode_3.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_3.TabIndex = 321
        Me.AlarmCode_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_3
        '
        Me.RPM_3.Location = New System.Drawing.Point(422, 90)
        Me.RPM_3.Name = "RPM_3"
        Me.RPM_3.Size = New System.Drawing.Size(55, 22)
        Me.RPM_3.TabIndex = 320
        Me.RPM_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID1
        '
        Me.ID1.AutoSize = True
        Me.ID1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID1.Location = New System.Drawing.Point(223, 97)
        Me.ID1.Name = "ID1"
        Me.ID1.Size = New System.Drawing.Size(35, 13)
        Me.ID1.TabIndex = 288
        Me.ID1.Text = "ID:03"
        '
        'Status_4
        '
        Me.Status_4.Location = New System.Drawing.Point(264, 118)
        Me.Status_4.Name = "Status_4"
        Me.Status_4.Size = New System.Drawing.Size(55, 22)
        Me.Status_4.TabIndex = 329
        Me.Status_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_4
        '
        Me.RPMS_4.Location = New System.Drawing.Point(343, 118)
        Me.RPMS_4.Name = "RPMS_4"
        Me.RPMS_4.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_4.TabIndex = 328
        Me.RPMS_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_4
        '
        Me.AlarmCode_4.Location = New System.Drawing.Point(501, 118)
        Me.AlarmCode_4.Name = "AlarmCode_4"
        Me.AlarmCode_4.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_4.TabIndex = 327
        Me.AlarmCode_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_4
        '
        Me.RPM_4.Location = New System.Drawing.Point(422, 118)
        Me.RPM_4.Name = "RPM_4"
        Me.RPM_4.Size = New System.Drawing.Size(55, 22)
        Me.RPM_4.TabIndex = 326
        Me.RPM_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_5
        '
        Me.Status_5.Location = New System.Drawing.Point(264, 146)
        Me.Status_5.Name = "Status_5"
        Me.Status_5.Size = New System.Drawing.Size(55, 22)
        Me.Status_5.TabIndex = 334
        Me.Status_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_5
        '
        Me.RPMS_5.Location = New System.Drawing.Point(343, 146)
        Me.RPMS_5.Name = "RPMS_5"
        Me.RPMS_5.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_5.TabIndex = 333
        Me.RPMS_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_5
        '
        Me.AlarmCode_5.Location = New System.Drawing.Point(501, 146)
        Me.AlarmCode_5.Name = "AlarmCode_5"
        Me.AlarmCode_5.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_5.TabIndex = 332
        Me.AlarmCode_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_5
        '
        Me.RPM_5.Location = New System.Drawing.Point(422, 146)
        Me.RPM_5.Name = "RPM_5"
        Me.RPM_5.Size = New System.Drawing.Size(55, 22)
        Me.RPM_5.TabIndex = 331
        Me.RPM_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_6
        '
        Me.Status_6.Location = New System.Drawing.Point(264, 174)
        Me.Status_6.Name = "Status_6"
        Me.Status_6.Size = New System.Drawing.Size(55, 22)
        Me.Status_6.TabIndex = 339
        Me.Status_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_6
        '
        Me.RPMS_6.Location = New System.Drawing.Point(343, 174)
        Me.RPMS_6.Name = "RPMS_6"
        Me.RPMS_6.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_6.TabIndex = 338
        Me.RPMS_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_6
        '
        Me.AlarmCode_6.Location = New System.Drawing.Point(501, 174)
        Me.AlarmCode_6.Name = "AlarmCode_6"
        Me.AlarmCode_6.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_6.TabIndex = 337
        Me.AlarmCode_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_6
        '
        Me.RPM_6.Location = New System.Drawing.Point(422, 174)
        Me.RPM_6.Name = "RPM_6"
        Me.RPM_6.Size = New System.Drawing.Size(55, 22)
        Me.RPM_6.TabIndex = 336
        Me.RPM_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_7
        '
        Me.Status_7.Location = New System.Drawing.Point(264, 202)
        Me.Status_7.Name = "Status_7"
        Me.Status_7.Size = New System.Drawing.Size(55, 22)
        Me.Status_7.TabIndex = 344
        Me.Status_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_7
        '
        Me.RPMS_7.Location = New System.Drawing.Point(343, 202)
        Me.RPMS_7.Name = "RPMS_7"
        Me.RPMS_7.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_7.TabIndex = 343
        Me.RPMS_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_7
        '
        Me.AlarmCode_7.Location = New System.Drawing.Point(501, 202)
        Me.AlarmCode_7.Name = "AlarmCode_7"
        Me.AlarmCode_7.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_7.TabIndex = 342
        Me.AlarmCode_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_7
        '
        Me.RPM_7.Location = New System.Drawing.Point(422, 202)
        Me.RPM_7.Name = "RPM_7"
        Me.RPM_7.Size = New System.Drawing.Size(55, 22)
        Me.RPM_7.TabIndex = 341
        Me.RPM_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_8
        '
        Me.Status_8.Location = New System.Drawing.Point(264, 230)
        Me.Status_8.Name = "Status_8"
        Me.Status_8.Size = New System.Drawing.Size(55, 22)
        Me.Status_8.TabIndex = 349
        Me.Status_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_8
        '
        Me.RPMS_8.Location = New System.Drawing.Point(343, 230)
        Me.RPMS_8.Name = "RPMS_8"
        Me.RPMS_8.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_8.TabIndex = 348
        Me.RPMS_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_8
        '
        Me.AlarmCode_8.Location = New System.Drawing.Point(501, 230)
        Me.AlarmCode_8.Name = "AlarmCode_8"
        Me.AlarmCode_8.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_8.TabIndex = 347
        Me.AlarmCode_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_8
        '
        Me.RPM_8.Location = New System.Drawing.Point(422, 230)
        Me.RPM_8.Name = "RPM_8"
        Me.RPM_8.Size = New System.Drawing.Size(55, 22)
        Me.RPM_8.TabIndex = 346
        Me.RPM_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_9
        '
        Me.Status_9.Location = New System.Drawing.Point(264, 258)
        Me.Status_9.Name = "Status_9"
        Me.Status_9.Size = New System.Drawing.Size(55, 22)
        Me.Status_9.TabIndex = 354
        Me.Status_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_9
        '
        Me.RPMS_9.Location = New System.Drawing.Point(343, 258)
        Me.RPMS_9.Name = "RPMS_9"
        Me.RPMS_9.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_9.TabIndex = 353
        Me.RPMS_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_9
        '
        Me.AlarmCode_9.Location = New System.Drawing.Point(501, 258)
        Me.AlarmCode_9.Name = "AlarmCode_9"
        Me.AlarmCode_9.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_9.TabIndex = 352
        Me.AlarmCode_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_9
        '
        Me.RPM_9.Location = New System.Drawing.Point(422, 258)
        Me.RPM_9.Name = "RPM_9"
        Me.RPM_9.Size = New System.Drawing.Size(55, 22)
        Me.RPM_9.TabIndex = 351
        Me.RPM_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_10
        '
        Me.Status_10.Location = New System.Drawing.Point(264, 286)
        Me.Status_10.Name = "Status_10"
        Me.Status_10.Size = New System.Drawing.Size(55, 22)
        Me.Status_10.TabIndex = 359
        Me.Status_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_10
        '
        Me.RPMS_10.Location = New System.Drawing.Point(343, 286)
        Me.RPMS_10.Name = "RPMS_10"
        Me.RPMS_10.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_10.TabIndex = 358
        Me.RPMS_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_10
        '
        Me.AlarmCode_10.Location = New System.Drawing.Point(501, 286)
        Me.AlarmCode_10.Name = "AlarmCode_10"
        Me.AlarmCode_10.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_10.TabIndex = 357
        Me.AlarmCode_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_10
        '
        Me.RPM_10.Location = New System.Drawing.Point(422, 286)
        Me.RPM_10.Name = "RPM_10"
        Me.RPM_10.Size = New System.Drawing.Size(55, 22)
        Me.RPM_10.TabIndex = 356
        Me.RPM_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_12
        '
        Me.Status_12.Location = New System.Drawing.Point(264, 342)
        Me.Status_12.Name = "Status_12"
        Me.Status_12.Size = New System.Drawing.Size(55, 22)
        Me.Status_12.TabIndex = 369
        Me.Status_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_12
        '
        Me.RPMS_12.Location = New System.Drawing.Point(343, 342)
        Me.RPMS_12.Name = "RPMS_12"
        Me.RPMS_12.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_12.TabIndex = 368
        Me.RPMS_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_12
        '
        Me.AlarmCode_12.Location = New System.Drawing.Point(501, 342)
        Me.AlarmCode_12.Name = "AlarmCode_12"
        Me.AlarmCode_12.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_12.TabIndex = 367
        Me.AlarmCode_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_12
        '
        Me.RPM_12.Location = New System.Drawing.Point(422, 342)
        Me.RPM_12.Name = "RPM_12"
        Me.RPM_12.Size = New System.Drawing.Size(55, 22)
        Me.RPM_12.TabIndex = 366
        Me.RPM_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_11
        '
        Me.Status_11.Location = New System.Drawing.Point(264, 314)
        Me.Status_11.Name = "Status_11"
        Me.Status_11.Size = New System.Drawing.Size(55, 22)
        Me.Status_11.TabIndex = 365
        Me.Status_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_11
        '
        Me.RPMS_11.Location = New System.Drawing.Point(343, 314)
        Me.RPMS_11.Name = "RPMS_11"
        Me.RPMS_11.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_11.TabIndex = 364
        Me.RPMS_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_11
        '
        Me.AlarmCode_11.Location = New System.Drawing.Point(501, 314)
        Me.AlarmCode_11.Name = "AlarmCode_11"
        Me.AlarmCode_11.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_11.TabIndex = 363
        Me.AlarmCode_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_11
        '
        Me.RPM_11.Location = New System.Drawing.Point(422, 314)
        Me.RPM_11.Name = "RPM_11"
        Me.RPM_11.Size = New System.Drawing.Size(55, 22)
        Me.RPM_11.TabIndex = 362
        Me.RPM_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_24
        '
        Me.Status_24.Location = New System.Drawing.Point(609, 342)
        Me.Status_24.Name = "Status_24"
        Me.Status_24.Size = New System.Drawing.Size(55, 22)
        Me.Status_24.TabIndex = 433
        Me.Status_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_24
        '
        Me.RPMS_24.Location = New System.Drawing.Point(688, 342)
        Me.RPMS_24.Name = "RPMS_24"
        Me.RPMS_24.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_24.TabIndex = 432
        Me.RPMS_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_24
        '
        Me.AlarmCode_24.Location = New System.Drawing.Point(846, 342)
        Me.AlarmCode_24.Name = "AlarmCode_24"
        Me.AlarmCode_24.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_24.TabIndex = 431
        Me.AlarmCode_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_24
        '
        Me.RPM_24.Location = New System.Drawing.Point(767, 342)
        Me.RPM_24.Name = "RPM_24"
        Me.RPM_24.Size = New System.Drawing.Size(55, 22)
        Me.RPM_24.TabIndex = 430
        Me.RPM_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_23
        '
        Me.Status_23.Location = New System.Drawing.Point(609, 314)
        Me.Status_23.Name = "Status_23"
        Me.Status_23.Size = New System.Drawing.Size(55, 22)
        Me.Status_23.TabIndex = 429
        Me.Status_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_23
        '
        Me.RPMS_23.Location = New System.Drawing.Point(688, 314)
        Me.RPMS_23.Name = "RPMS_23"
        Me.RPMS_23.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_23.TabIndex = 428
        Me.RPMS_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_23
        '
        Me.AlarmCode_23.Location = New System.Drawing.Point(846, 314)
        Me.AlarmCode_23.Name = "AlarmCode_23"
        Me.AlarmCode_23.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_23.TabIndex = 427
        Me.AlarmCode_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_23
        '
        Me.RPM_23.Location = New System.Drawing.Point(767, 314)
        Me.RPM_23.Name = "RPM_23"
        Me.RPM_23.Size = New System.Drawing.Size(55, 22)
        Me.RPM_23.TabIndex = 426
        Me.RPM_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_22
        '
        Me.Status_22.Location = New System.Drawing.Point(609, 286)
        Me.Status_22.Name = "Status_22"
        Me.Status_22.Size = New System.Drawing.Size(55, 22)
        Me.Status_22.TabIndex = 425
        Me.Status_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_22
        '
        Me.RPMS_22.Location = New System.Drawing.Point(688, 286)
        Me.RPMS_22.Name = "RPMS_22"
        Me.RPMS_22.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_22.TabIndex = 424
        Me.RPMS_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_22
        '
        Me.AlarmCode_22.Location = New System.Drawing.Point(846, 286)
        Me.AlarmCode_22.Name = "AlarmCode_22"
        Me.AlarmCode_22.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_22.TabIndex = 423
        Me.AlarmCode_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_22
        '
        Me.RPM_22.Location = New System.Drawing.Point(767, 286)
        Me.RPM_22.Name = "RPM_22"
        Me.RPM_22.Size = New System.Drawing.Size(55, 22)
        Me.RPM_22.TabIndex = 422
        Me.RPM_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_21
        '
        Me.Status_21.Location = New System.Drawing.Point(609, 258)
        Me.Status_21.Name = "Status_21"
        Me.Status_21.Size = New System.Drawing.Size(55, 22)
        Me.Status_21.TabIndex = 421
        Me.Status_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_21
        '
        Me.RPMS_21.Location = New System.Drawing.Point(688, 258)
        Me.RPMS_21.Name = "RPMS_21"
        Me.RPMS_21.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_21.TabIndex = 420
        Me.RPMS_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_21
        '
        Me.AlarmCode_21.Location = New System.Drawing.Point(846, 258)
        Me.AlarmCode_21.Name = "AlarmCode_21"
        Me.AlarmCode_21.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_21.TabIndex = 419
        Me.AlarmCode_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_21
        '
        Me.RPM_21.Location = New System.Drawing.Point(767, 258)
        Me.RPM_21.Name = "RPM_21"
        Me.RPM_21.Size = New System.Drawing.Size(55, 22)
        Me.RPM_21.TabIndex = 418
        Me.RPM_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_20
        '
        Me.Status_20.Location = New System.Drawing.Point(609, 230)
        Me.Status_20.Name = "Status_20"
        Me.Status_20.Size = New System.Drawing.Size(55, 22)
        Me.Status_20.TabIndex = 417
        Me.Status_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_20
        '
        Me.RPMS_20.Location = New System.Drawing.Point(688, 230)
        Me.RPMS_20.Name = "RPMS_20"
        Me.RPMS_20.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_20.TabIndex = 416
        Me.RPMS_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_20
        '
        Me.AlarmCode_20.Location = New System.Drawing.Point(846, 230)
        Me.AlarmCode_20.Name = "AlarmCode_20"
        Me.AlarmCode_20.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_20.TabIndex = 415
        Me.AlarmCode_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_20
        '
        Me.RPM_20.Location = New System.Drawing.Point(767, 230)
        Me.RPM_20.Name = "RPM_20"
        Me.RPM_20.Size = New System.Drawing.Size(55, 22)
        Me.RPM_20.TabIndex = 414
        Me.RPM_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_19
        '
        Me.Status_19.Location = New System.Drawing.Point(609, 202)
        Me.Status_19.Name = "Status_19"
        Me.Status_19.Size = New System.Drawing.Size(55, 22)
        Me.Status_19.TabIndex = 413
        Me.Status_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_19
        '
        Me.RPMS_19.Location = New System.Drawing.Point(688, 202)
        Me.RPMS_19.Name = "RPMS_19"
        Me.RPMS_19.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_19.TabIndex = 412
        Me.RPMS_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_19
        '
        Me.AlarmCode_19.Location = New System.Drawing.Point(846, 202)
        Me.AlarmCode_19.Name = "AlarmCode_19"
        Me.AlarmCode_19.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_19.TabIndex = 411
        Me.AlarmCode_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_19
        '
        Me.RPM_19.Location = New System.Drawing.Point(767, 202)
        Me.RPM_19.Name = "RPM_19"
        Me.RPM_19.Size = New System.Drawing.Size(55, 22)
        Me.RPM_19.TabIndex = 410
        Me.RPM_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_18
        '
        Me.Status_18.Location = New System.Drawing.Point(609, 174)
        Me.Status_18.Name = "Status_18"
        Me.Status_18.Size = New System.Drawing.Size(55, 22)
        Me.Status_18.TabIndex = 409
        Me.Status_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_18
        '
        Me.RPMS_18.Location = New System.Drawing.Point(688, 174)
        Me.RPMS_18.Name = "RPMS_18"
        Me.RPMS_18.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_18.TabIndex = 408
        Me.RPMS_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_18
        '
        Me.AlarmCode_18.Location = New System.Drawing.Point(846, 174)
        Me.AlarmCode_18.Name = "AlarmCode_18"
        Me.AlarmCode_18.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_18.TabIndex = 407
        Me.AlarmCode_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_18
        '
        Me.RPM_18.Location = New System.Drawing.Point(767, 174)
        Me.RPM_18.Name = "RPM_18"
        Me.RPM_18.Size = New System.Drawing.Size(55, 22)
        Me.RPM_18.TabIndex = 406
        Me.RPM_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_17
        '
        Me.Status_17.Location = New System.Drawing.Point(609, 146)
        Me.Status_17.Name = "Status_17"
        Me.Status_17.Size = New System.Drawing.Size(55, 22)
        Me.Status_17.TabIndex = 405
        Me.Status_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_17
        '
        Me.RPMS_17.Location = New System.Drawing.Point(688, 146)
        Me.RPMS_17.Name = "RPMS_17"
        Me.RPMS_17.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_17.TabIndex = 404
        Me.RPMS_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_17
        '
        Me.AlarmCode_17.Location = New System.Drawing.Point(846, 146)
        Me.AlarmCode_17.Name = "AlarmCode_17"
        Me.AlarmCode_17.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_17.TabIndex = 403
        Me.AlarmCode_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_17
        '
        Me.RPM_17.Location = New System.Drawing.Point(767, 146)
        Me.RPM_17.Name = "RPM_17"
        Me.RPM_17.Size = New System.Drawing.Size(55, 22)
        Me.RPM_17.TabIndex = 402
        Me.RPM_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_16
        '
        Me.Status_16.Location = New System.Drawing.Point(609, 118)
        Me.Status_16.Name = "Status_16"
        Me.Status_16.Size = New System.Drawing.Size(55, 22)
        Me.Status_16.TabIndex = 401
        Me.Status_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_16
        '
        Me.RPMS_16.Location = New System.Drawing.Point(688, 118)
        Me.RPMS_16.Name = "RPMS_16"
        Me.RPMS_16.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_16.TabIndex = 400
        Me.RPMS_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_16
        '
        Me.AlarmCode_16.Location = New System.Drawing.Point(846, 118)
        Me.AlarmCode_16.Name = "AlarmCode_16"
        Me.AlarmCode_16.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_16.TabIndex = 399
        Me.AlarmCode_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_16
        '
        Me.RPM_16.Location = New System.Drawing.Point(767, 118)
        Me.RPM_16.Name = "RPM_16"
        Me.RPM_16.Size = New System.Drawing.Size(55, 22)
        Me.RPM_16.TabIndex = 398
        Me.RPM_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_15
        '
        Me.Status_15.Location = New System.Drawing.Point(609, 90)
        Me.Status_15.Name = "Status_15"
        Me.Status_15.Size = New System.Drawing.Size(55, 22)
        Me.Status_15.TabIndex = 397
        Me.Status_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_15
        '
        Me.RPMS_15.Location = New System.Drawing.Point(688, 90)
        Me.RPMS_15.Name = "RPMS_15"
        Me.RPMS_15.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_15.TabIndex = 396
        Me.RPMS_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_15
        '
        Me.AlarmCode_15.Location = New System.Drawing.Point(846, 90)
        Me.AlarmCode_15.Name = "AlarmCode_15"
        Me.AlarmCode_15.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_15.TabIndex = 395
        Me.AlarmCode_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_15
        '
        Me.RPM_15.Location = New System.Drawing.Point(767, 90)
        Me.RPM_15.Name = "RPM_15"
        Me.RPM_15.Size = New System.Drawing.Size(55, 22)
        Me.RPM_15.TabIndex = 394
        Me.RPM_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(839, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 12)
        Me.Label2.TabIndex = 393
        Me.Label2.Text = "Alarm Code"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(793, 13)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 12)
        Me.Label10.TabIndex = 392
        Me.Label10.Text = "RPM"
        '
        'Status_14
        '
        Me.Status_14.Location = New System.Drawing.Point(609, 60)
        Me.Status_14.Name = "Status_14"
        Me.Status_14.Size = New System.Drawing.Size(55, 22)
        Me.Status_14.TabIndex = 391
        Me.Status_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_14
        '
        Me.RPMS_14.Location = New System.Drawing.Point(688, 60)
        Me.RPMS_14.Name = "RPMS_14"
        Me.RPMS_14.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_14.TabIndex = 390
        Me.RPMS_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_13
        '
        Me.RPMS_13.Location = New System.Drawing.Point(688, 32)
        Me.RPMS_13.Name = "RPMS_13"
        Me.RPMS_13.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_13.TabIndex = 389
        Me.RPMS_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_13
        '
        Me.AlarmCode_13.Location = New System.Drawing.Point(846, 32)
        Me.AlarmCode_13.Name = "AlarmCode_13"
        Me.AlarmCode_13.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_13.TabIndex = 388
        Me.AlarmCode_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(568, 349)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 387
        Me.Label12.Text = "ID:24"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(568, 321)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 13)
        Me.Label13.TabIndex = 386
        Me.Label13.Text = "ID:23"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(568, 293)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 13)
        Me.Label16.TabIndex = 385
        Me.Label16.Text = "ID:22"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(568, 265)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(35, 13)
        Me.Label17.TabIndex = 384
        Me.Label17.Text = "ID:21"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(568, 237)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(35, 13)
        Me.Label18.TabIndex = 383
        Me.Label18.Text = "ID:20"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(568, 209)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(35, 13)
        Me.Label19.TabIndex = 382
        Me.Label19.Text = "ID:19"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(568, 181)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(35, 13)
        Me.Label20.TabIndex = 381
        Me.Label20.Text = "ID:18"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(568, 153)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(35, 13)
        Me.Label21.TabIndex = 380
        Me.Label21.Text = "ID:17"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(568, 125)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(35, 13)
        Me.Label23.TabIndex = 379
        Me.Label23.Text = "ID:16"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label24.Location = New System.Drawing.Point(568, 97)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(35, 13)
        Me.Label24.TabIndex = 378
        Me.Label24.Text = "ID:15"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label25.Location = New System.Drawing.Point(568, 69)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(35, 13)
        Me.Label25.TabIndex = 377
        Me.Label25.Text = "ID:14"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label26.Location = New System.Drawing.Point(568, 41)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(35, 13)
        Me.Label26.TabIndex = 376
        Me.Label26.Text = "ID:13"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(697, 13)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(46, 12)
        Me.Label27.TabIndex = 375
        Me.Label27.Text = "RPM Set"
        '
        'AlarmCode_14
        '
        Me.AlarmCode_14.Location = New System.Drawing.Point(846, 60)
        Me.AlarmCode_14.Name = "AlarmCode_14"
        Me.AlarmCode_14.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_14.TabIndex = 374
        Me.AlarmCode_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(632, 13)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(32, 12)
        Me.Label28.TabIndex = 373
        Me.Label28.Text = "Status"
        '
        'Status_13
        '
        Me.Status_13.Location = New System.Drawing.Point(609, 32)
        Me.Status_13.Name = "Status_13"
        Me.Status_13.Size = New System.Drawing.Size(55, 22)
        Me.Status_13.TabIndex = 372
        Me.Status_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_14
        '
        Me.RPM_14.Location = New System.Drawing.Point(767, 60)
        Me.RPM_14.Name = "RPM_14"
        Me.RPM_14.Size = New System.Drawing.Size(55, 22)
        Me.RPM_14.TabIndex = 371
        Me.RPM_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_13
        '
        Me.RPM_13.Location = New System.Drawing.Point(767, 32)
        Me.RPM_13.Name = "RPM_13"
        Me.RPM_13.Size = New System.Drawing.Size(55, 22)
        Me.RPM_13.TabIndex = 370
        Me.RPM_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_36
        '
        Me.Status_36.Location = New System.Drawing.Point(958, 342)
        Me.Status_36.Name = "Status_36"
        Me.Status_36.Size = New System.Drawing.Size(55, 22)
        Me.Status_36.TabIndex = 497
        Me.Status_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_36
        '
        Me.RPMS_36.Location = New System.Drawing.Point(1037, 342)
        Me.RPMS_36.Name = "RPMS_36"
        Me.RPMS_36.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_36.TabIndex = 496
        Me.RPMS_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_36
        '
        Me.AlarmCode_36.Location = New System.Drawing.Point(1195, 342)
        Me.AlarmCode_36.Name = "AlarmCode_36"
        Me.AlarmCode_36.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_36.TabIndex = 495
        Me.AlarmCode_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_36
        '
        Me.RPM_36.Location = New System.Drawing.Point(1116, 342)
        Me.RPM_36.Name = "RPM_36"
        Me.RPM_36.Size = New System.Drawing.Size(55, 22)
        Me.RPM_36.TabIndex = 494
        Me.RPM_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_35
        '
        Me.Status_35.Location = New System.Drawing.Point(958, 314)
        Me.Status_35.Name = "Status_35"
        Me.Status_35.Size = New System.Drawing.Size(55, 22)
        Me.Status_35.TabIndex = 493
        Me.Status_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_35
        '
        Me.RPMS_35.Location = New System.Drawing.Point(1037, 314)
        Me.RPMS_35.Name = "RPMS_35"
        Me.RPMS_35.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_35.TabIndex = 492
        Me.RPMS_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_35
        '
        Me.AlarmCode_35.Location = New System.Drawing.Point(1195, 314)
        Me.AlarmCode_35.Name = "AlarmCode_35"
        Me.AlarmCode_35.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_35.TabIndex = 491
        Me.AlarmCode_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_35
        '
        Me.RPM_35.Location = New System.Drawing.Point(1116, 314)
        Me.RPM_35.Name = "RPM_35"
        Me.RPM_35.Size = New System.Drawing.Size(55, 22)
        Me.RPM_35.TabIndex = 490
        Me.RPM_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_34
        '
        Me.Status_34.Location = New System.Drawing.Point(958, 286)
        Me.Status_34.Name = "Status_34"
        Me.Status_34.Size = New System.Drawing.Size(55, 22)
        Me.Status_34.TabIndex = 489
        Me.Status_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_34
        '
        Me.RPMS_34.Location = New System.Drawing.Point(1037, 286)
        Me.RPMS_34.Name = "RPMS_34"
        Me.RPMS_34.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_34.TabIndex = 488
        Me.RPMS_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_34
        '
        Me.AlarmCode_34.Location = New System.Drawing.Point(1195, 286)
        Me.AlarmCode_34.Name = "AlarmCode_34"
        Me.AlarmCode_34.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_34.TabIndex = 487
        Me.AlarmCode_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_34
        '
        Me.RPM_34.Location = New System.Drawing.Point(1116, 286)
        Me.RPM_34.Name = "RPM_34"
        Me.RPM_34.Size = New System.Drawing.Size(55, 22)
        Me.RPM_34.TabIndex = 486
        Me.RPM_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_33
        '
        Me.Status_33.Location = New System.Drawing.Point(958, 258)
        Me.Status_33.Name = "Status_33"
        Me.Status_33.Size = New System.Drawing.Size(55, 22)
        Me.Status_33.TabIndex = 485
        Me.Status_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_33
        '
        Me.RPMS_33.Location = New System.Drawing.Point(1037, 258)
        Me.RPMS_33.Name = "RPMS_33"
        Me.RPMS_33.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_33.TabIndex = 484
        Me.RPMS_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_33
        '
        Me.AlarmCode_33.Location = New System.Drawing.Point(1195, 258)
        Me.AlarmCode_33.Name = "AlarmCode_33"
        Me.AlarmCode_33.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_33.TabIndex = 483
        Me.AlarmCode_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_33
        '
        Me.RPM_33.Location = New System.Drawing.Point(1116, 258)
        Me.RPM_33.Name = "RPM_33"
        Me.RPM_33.Size = New System.Drawing.Size(55, 22)
        Me.RPM_33.TabIndex = 482
        Me.RPM_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_32
        '
        Me.Status_32.Location = New System.Drawing.Point(958, 230)
        Me.Status_32.Name = "Status_32"
        Me.Status_32.Size = New System.Drawing.Size(55, 22)
        Me.Status_32.TabIndex = 481
        Me.Status_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_32
        '
        Me.RPMS_32.Location = New System.Drawing.Point(1037, 230)
        Me.RPMS_32.Name = "RPMS_32"
        Me.RPMS_32.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_32.TabIndex = 480
        Me.RPMS_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_32
        '
        Me.AlarmCode_32.Location = New System.Drawing.Point(1195, 230)
        Me.AlarmCode_32.Name = "AlarmCode_32"
        Me.AlarmCode_32.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_32.TabIndex = 479
        Me.AlarmCode_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_32
        '
        Me.RPM_32.Location = New System.Drawing.Point(1116, 230)
        Me.RPM_32.Name = "RPM_32"
        Me.RPM_32.Size = New System.Drawing.Size(55, 22)
        Me.RPM_32.TabIndex = 478
        Me.RPM_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_31
        '
        Me.Status_31.Location = New System.Drawing.Point(958, 202)
        Me.Status_31.Name = "Status_31"
        Me.Status_31.Size = New System.Drawing.Size(55, 22)
        Me.Status_31.TabIndex = 477
        Me.Status_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_31
        '
        Me.RPMS_31.Location = New System.Drawing.Point(1037, 202)
        Me.RPMS_31.Name = "RPMS_31"
        Me.RPMS_31.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_31.TabIndex = 476
        Me.RPMS_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_31
        '
        Me.AlarmCode_31.Location = New System.Drawing.Point(1195, 202)
        Me.AlarmCode_31.Name = "AlarmCode_31"
        Me.AlarmCode_31.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_31.TabIndex = 475
        Me.AlarmCode_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_31
        '
        Me.RPM_31.Location = New System.Drawing.Point(1116, 202)
        Me.RPM_31.Name = "RPM_31"
        Me.RPM_31.Size = New System.Drawing.Size(55, 22)
        Me.RPM_31.TabIndex = 474
        Me.RPM_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_30
        '
        Me.Status_30.Location = New System.Drawing.Point(958, 174)
        Me.Status_30.Name = "Status_30"
        Me.Status_30.Size = New System.Drawing.Size(55, 22)
        Me.Status_30.TabIndex = 473
        Me.Status_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_30
        '
        Me.RPMS_30.Location = New System.Drawing.Point(1037, 174)
        Me.RPMS_30.Name = "RPMS_30"
        Me.RPMS_30.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_30.TabIndex = 472
        Me.RPMS_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_30
        '
        Me.AlarmCode_30.Location = New System.Drawing.Point(1195, 174)
        Me.AlarmCode_30.Name = "AlarmCode_30"
        Me.AlarmCode_30.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_30.TabIndex = 471
        Me.AlarmCode_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_30
        '
        Me.RPM_30.Location = New System.Drawing.Point(1116, 174)
        Me.RPM_30.Name = "RPM_30"
        Me.RPM_30.Size = New System.Drawing.Size(55, 22)
        Me.RPM_30.TabIndex = 470
        Me.RPM_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_29
        '
        Me.Status_29.Location = New System.Drawing.Point(958, 146)
        Me.Status_29.Name = "Status_29"
        Me.Status_29.Size = New System.Drawing.Size(55, 22)
        Me.Status_29.TabIndex = 469
        Me.Status_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_29
        '
        Me.RPMS_29.Location = New System.Drawing.Point(1037, 146)
        Me.RPMS_29.Name = "RPMS_29"
        Me.RPMS_29.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_29.TabIndex = 468
        Me.RPMS_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_29
        '
        Me.AlarmCode_29.Location = New System.Drawing.Point(1195, 146)
        Me.AlarmCode_29.Name = "AlarmCode_29"
        Me.AlarmCode_29.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_29.TabIndex = 467
        Me.AlarmCode_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_29
        '
        Me.RPM_29.Location = New System.Drawing.Point(1116, 146)
        Me.RPM_29.Name = "RPM_29"
        Me.RPM_29.Size = New System.Drawing.Size(55, 22)
        Me.RPM_29.TabIndex = 466
        Me.RPM_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_28
        '
        Me.Status_28.Location = New System.Drawing.Point(958, 118)
        Me.Status_28.Name = "Status_28"
        Me.Status_28.Size = New System.Drawing.Size(55, 22)
        Me.Status_28.TabIndex = 465
        Me.Status_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_28
        '
        Me.RPMS_28.Location = New System.Drawing.Point(1037, 118)
        Me.RPMS_28.Name = "RPMS_28"
        Me.RPMS_28.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_28.TabIndex = 464
        Me.RPMS_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_28
        '
        Me.AlarmCode_28.Location = New System.Drawing.Point(1195, 118)
        Me.AlarmCode_28.Name = "AlarmCode_28"
        Me.AlarmCode_28.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_28.TabIndex = 463
        Me.AlarmCode_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_28
        '
        Me.RPM_28.Location = New System.Drawing.Point(1116, 118)
        Me.RPM_28.Name = "RPM_28"
        Me.RPM_28.Size = New System.Drawing.Size(55, 22)
        Me.RPM_28.TabIndex = 462
        Me.RPM_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_27
        '
        Me.Status_27.Location = New System.Drawing.Point(958, 90)
        Me.Status_27.Name = "Status_27"
        Me.Status_27.Size = New System.Drawing.Size(55, 22)
        Me.Status_27.TabIndex = 461
        Me.Status_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_27
        '
        Me.RPMS_27.Location = New System.Drawing.Point(1037, 90)
        Me.RPMS_27.Name = "RPMS_27"
        Me.RPMS_27.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_27.TabIndex = 460
        Me.RPMS_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_27
        '
        Me.AlarmCode_27.Location = New System.Drawing.Point(1195, 90)
        Me.AlarmCode_27.Name = "AlarmCode_27"
        Me.AlarmCode_27.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_27.TabIndex = 459
        Me.AlarmCode_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_27
        '
        Me.RPM_27.Location = New System.Drawing.Point(1116, 90)
        Me.RPM_27.Name = "RPM_27"
        Me.RPM_27.Size = New System.Drawing.Size(55, 22)
        Me.RPM_27.TabIndex = 458
        Me.RPM_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(1188, 13)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(62, 12)
        Me.Label29.TabIndex = 457
        Me.Label29.Text = "Alarm Code"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(1142, 13)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(29, 12)
        Me.Label31.TabIndex = 456
        Me.Label31.Text = "RPM"
        '
        'Status_26
        '
        Me.Status_26.Location = New System.Drawing.Point(958, 60)
        Me.Status_26.Name = "Status_26"
        Me.Status_26.Size = New System.Drawing.Size(55, 22)
        Me.Status_26.TabIndex = 455
        Me.Status_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_26
        '
        Me.RPMS_26.Location = New System.Drawing.Point(1037, 60)
        Me.RPMS_26.Name = "RPMS_26"
        Me.RPMS_26.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_26.TabIndex = 454
        Me.RPMS_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_25
        '
        Me.RPMS_25.Location = New System.Drawing.Point(1037, 32)
        Me.RPMS_25.Name = "RPMS_25"
        Me.RPMS_25.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_25.TabIndex = 453
        Me.RPMS_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_25
        '
        Me.AlarmCode_25.Location = New System.Drawing.Point(1195, 32)
        Me.AlarmCode_25.Name = "AlarmCode_25"
        Me.AlarmCode_25.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_25.TabIndex = 452
        Me.AlarmCode_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label32.Location = New System.Drawing.Point(917, 349)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(35, 13)
        Me.Label32.TabIndex = 451
        Me.Label32.Text = "ID:36"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label33.Location = New System.Drawing.Point(917, 321)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(35, 13)
        Me.Label33.TabIndex = 450
        Me.Label33.Text = "ID:35"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label34.Location = New System.Drawing.Point(917, 293)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(35, 13)
        Me.Label34.TabIndex = 449
        Me.Label34.Text = "ID:34"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label35.Location = New System.Drawing.Point(917, 265)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(35, 13)
        Me.Label35.TabIndex = 448
        Me.Label35.Text = "ID:33"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label36.Location = New System.Drawing.Point(917, 237)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(35, 13)
        Me.Label36.TabIndex = 447
        Me.Label36.Text = "ID:32"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label37.Location = New System.Drawing.Point(917, 209)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(35, 13)
        Me.Label37.TabIndex = 446
        Me.Label37.Text = "ID:31"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label38.Location = New System.Drawing.Point(917, 181)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(35, 13)
        Me.Label38.TabIndex = 445
        Me.Label38.Text = "ID:30"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label39.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label39.Location = New System.Drawing.Point(917, 153)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(35, 13)
        Me.Label39.TabIndex = 444
        Me.Label39.Text = "ID:29"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label40.Location = New System.Drawing.Point(917, 125)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(35, 13)
        Me.Label40.TabIndex = 443
        Me.Label40.Text = "ID:28"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label41.Location = New System.Drawing.Point(917, 97)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(35, 13)
        Me.Label41.TabIndex = 442
        Me.Label41.Text = "ID:27"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label42.Location = New System.Drawing.Point(917, 69)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(35, 13)
        Me.Label42.TabIndex = 441
        Me.Label42.Text = "ID:26"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label43.Location = New System.Drawing.Point(917, 41)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(35, 13)
        Me.Label43.TabIndex = 440
        Me.Label43.Text = "ID:25"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(1046, 13)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(46, 12)
        Me.Label44.TabIndex = 439
        Me.Label44.Text = "RPM Set"
        '
        'AlarmCode_26
        '
        Me.AlarmCode_26.Location = New System.Drawing.Point(1195, 60)
        Me.AlarmCode_26.Name = "AlarmCode_26"
        Me.AlarmCode_26.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_26.TabIndex = 438
        Me.AlarmCode_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(981, 13)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(32, 12)
        Me.Label45.TabIndex = 437
        Me.Label45.Text = "Status"
        '
        'Status_25
        '
        Me.Status_25.Location = New System.Drawing.Point(958, 32)
        Me.Status_25.Name = "Status_25"
        Me.Status_25.Size = New System.Drawing.Size(55, 22)
        Me.Status_25.TabIndex = 436
        Me.Status_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_26
        '
        Me.RPM_26.Location = New System.Drawing.Point(1116, 60)
        Me.RPM_26.Name = "RPM_26"
        Me.RPM_26.Size = New System.Drawing.Size(55, 22)
        Me.RPM_26.TabIndex = 435
        Me.RPM_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_25
        '
        Me.RPM_25.Location = New System.Drawing.Point(1116, 32)
        Me.RPM_25.Name = "RPM_25"
        Me.RPM_25.Size = New System.Drawing.Size(55, 22)
        Me.RPM_25.TabIndex = 434
        Me.RPM_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_72
        '
        Me.Status_72.Location = New System.Drawing.Point(958, 708)
        Me.Status_72.Name = "Status_72"
        Me.Status_72.Size = New System.Drawing.Size(55, 22)
        Me.Status_72.TabIndex = 689
        Me.Status_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_72
        '
        Me.RPMS_72.Location = New System.Drawing.Point(1037, 708)
        Me.RPMS_72.Name = "RPMS_72"
        Me.RPMS_72.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_72.TabIndex = 688
        Me.RPMS_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox149
        '
        Me.TextBox149.Location = New System.Drawing.Point(1195, 708)
        Me.TextBox149.Name = "TextBox149"
        Me.TextBox149.Size = New System.Drawing.Size(55, 22)
        Me.TextBox149.TabIndex = 687
        Me.TextBox149.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_72
        '
        Me.RPM_72.Location = New System.Drawing.Point(1116, 708)
        Me.RPM_72.Name = "RPM_72"
        Me.RPM_72.Size = New System.Drawing.Size(55, 22)
        Me.RPM_72.TabIndex = 686
        Me.RPM_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_71
        '
        Me.Status_71.Location = New System.Drawing.Point(958, 680)
        Me.Status_71.Name = "Status_71"
        Me.Status_71.Size = New System.Drawing.Size(55, 22)
        Me.Status_71.TabIndex = 685
        Me.Status_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_71
        '
        Me.RPMS_71.Location = New System.Drawing.Point(1037, 680)
        Me.RPMS_71.Name = "RPMS_71"
        Me.RPMS_71.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_71.TabIndex = 684
        Me.RPMS_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox153
        '
        Me.TextBox153.Location = New System.Drawing.Point(1195, 680)
        Me.TextBox153.Name = "TextBox153"
        Me.TextBox153.Size = New System.Drawing.Size(55, 22)
        Me.TextBox153.TabIndex = 683
        Me.TextBox153.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_71
        '
        Me.RPM_71.Location = New System.Drawing.Point(1116, 680)
        Me.RPM_71.Name = "RPM_71"
        Me.RPM_71.Size = New System.Drawing.Size(55, 22)
        Me.RPM_71.TabIndex = 682
        Me.RPM_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_70
        '
        Me.Status_70.Location = New System.Drawing.Point(958, 652)
        Me.Status_70.Name = "Status_70"
        Me.Status_70.Size = New System.Drawing.Size(55, 22)
        Me.Status_70.TabIndex = 681
        Me.Status_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_70
        '
        Me.RPMS_70.Location = New System.Drawing.Point(1037, 652)
        Me.RPMS_70.Name = "RPMS_70"
        Me.RPMS_70.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_70.TabIndex = 680
        Me.RPMS_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox157
        '
        Me.TextBox157.Location = New System.Drawing.Point(1195, 652)
        Me.TextBox157.Name = "TextBox157"
        Me.TextBox157.Size = New System.Drawing.Size(55, 22)
        Me.TextBox157.TabIndex = 679
        Me.TextBox157.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_70
        '
        Me.RPM_70.Location = New System.Drawing.Point(1116, 652)
        Me.RPM_70.Name = "RPM_70"
        Me.RPM_70.Size = New System.Drawing.Size(55, 22)
        Me.RPM_70.TabIndex = 678
        Me.RPM_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_69
        '
        Me.Status_69.Location = New System.Drawing.Point(958, 624)
        Me.Status_69.Name = "Status_69"
        Me.Status_69.Size = New System.Drawing.Size(55, 22)
        Me.Status_69.TabIndex = 677
        Me.Status_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_69
        '
        Me.RPMS_69.Location = New System.Drawing.Point(1037, 624)
        Me.RPMS_69.Name = "RPMS_69"
        Me.RPMS_69.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_69.TabIndex = 676
        Me.RPMS_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox161
        '
        Me.TextBox161.Location = New System.Drawing.Point(1195, 624)
        Me.TextBox161.Name = "TextBox161"
        Me.TextBox161.Size = New System.Drawing.Size(55, 22)
        Me.TextBox161.TabIndex = 675
        Me.TextBox161.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_69
        '
        Me.RPM_69.Location = New System.Drawing.Point(1116, 624)
        Me.RPM_69.Name = "RPM_69"
        Me.RPM_69.Size = New System.Drawing.Size(55, 22)
        Me.RPM_69.TabIndex = 674
        Me.RPM_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_68
        '
        Me.Status_68.Location = New System.Drawing.Point(958, 596)
        Me.Status_68.Name = "Status_68"
        Me.Status_68.Size = New System.Drawing.Size(55, 22)
        Me.Status_68.TabIndex = 673
        Me.Status_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_68
        '
        Me.RPMS_68.Location = New System.Drawing.Point(1037, 596)
        Me.RPMS_68.Name = "RPMS_68"
        Me.RPMS_68.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_68.TabIndex = 672
        Me.RPMS_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox165
        '
        Me.TextBox165.Location = New System.Drawing.Point(1195, 596)
        Me.TextBox165.Name = "TextBox165"
        Me.TextBox165.Size = New System.Drawing.Size(55, 22)
        Me.TextBox165.TabIndex = 671
        Me.TextBox165.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_68
        '
        Me.RPM_68.Location = New System.Drawing.Point(1116, 596)
        Me.RPM_68.Name = "RPM_68"
        Me.RPM_68.Size = New System.Drawing.Size(55, 22)
        Me.RPM_68.TabIndex = 670
        Me.RPM_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_67
        '
        Me.Status_67.Location = New System.Drawing.Point(958, 568)
        Me.Status_67.Name = "Status_67"
        Me.Status_67.Size = New System.Drawing.Size(55, 22)
        Me.Status_67.TabIndex = 669
        Me.Status_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_67
        '
        Me.RPMS_67.Location = New System.Drawing.Point(1037, 568)
        Me.RPMS_67.Name = "RPMS_67"
        Me.RPMS_67.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_67.TabIndex = 668
        Me.RPMS_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox169
        '
        Me.TextBox169.Location = New System.Drawing.Point(1195, 568)
        Me.TextBox169.Name = "TextBox169"
        Me.TextBox169.Size = New System.Drawing.Size(55, 22)
        Me.TextBox169.TabIndex = 667
        Me.TextBox169.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_67
        '
        Me.RPM_67.Location = New System.Drawing.Point(1116, 568)
        Me.RPM_67.Name = "RPM_67"
        Me.RPM_67.Size = New System.Drawing.Size(55, 22)
        Me.RPM_67.TabIndex = 666
        Me.RPM_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_66
        '
        Me.Status_66.Location = New System.Drawing.Point(958, 540)
        Me.Status_66.Name = "Status_66"
        Me.Status_66.Size = New System.Drawing.Size(55, 22)
        Me.Status_66.TabIndex = 665
        Me.Status_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_66
        '
        Me.RPMS_66.Location = New System.Drawing.Point(1037, 540)
        Me.RPMS_66.Name = "RPMS_66"
        Me.RPMS_66.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_66.TabIndex = 664
        Me.RPMS_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox173
        '
        Me.TextBox173.Location = New System.Drawing.Point(1195, 540)
        Me.TextBox173.Name = "TextBox173"
        Me.TextBox173.Size = New System.Drawing.Size(55, 22)
        Me.TextBox173.TabIndex = 663
        Me.TextBox173.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_66
        '
        Me.RPM_66.Location = New System.Drawing.Point(1116, 540)
        Me.RPM_66.Name = "RPM_66"
        Me.RPM_66.Size = New System.Drawing.Size(55, 22)
        Me.RPM_66.TabIndex = 662
        Me.RPM_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_65
        '
        Me.Status_65.Location = New System.Drawing.Point(958, 512)
        Me.Status_65.Name = "Status_65"
        Me.Status_65.Size = New System.Drawing.Size(55, 22)
        Me.Status_65.TabIndex = 661
        Me.Status_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_65
        '
        Me.RPMS_65.Location = New System.Drawing.Point(1037, 512)
        Me.RPMS_65.Name = "RPMS_65"
        Me.RPMS_65.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_65.TabIndex = 660
        Me.RPMS_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox177
        '
        Me.TextBox177.Location = New System.Drawing.Point(1195, 512)
        Me.TextBox177.Name = "TextBox177"
        Me.TextBox177.Size = New System.Drawing.Size(55, 22)
        Me.TextBox177.TabIndex = 659
        Me.TextBox177.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_65
        '
        Me.RPM_65.Location = New System.Drawing.Point(1116, 512)
        Me.RPM_65.Name = "RPM_65"
        Me.RPM_65.Size = New System.Drawing.Size(55, 22)
        Me.RPM_65.TabIndex = 658
        Me.RPM_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_64
        '
        Me.Status_64.Location = New System.Drawing.Point(958, 484)
        Me.Status_64.Name = "Status_64"
        Me.Status_64.Size = New System.Drawing.Size(55, 22)
        Me.Status_64.TabIndex = 657
        Me.Status_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_64
        '
        Me.RPMS_64.Location = New System.Drawing.Point(1037, 484)
        Me.RPMS_64.Name = "RPMS_64"
        Me.RPMS_64.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_64.TabIndex = 656
        Me.RPMS_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_64
        '
        Me.RPM_64.Location = New System.Drawing.Point(1116, 484)
        Me.RPM_64.Name = "RPM_64"
        Me.RPM_64.Size = New System.Drawing.Size(55, 22)
        Me.RPM_64.TabIndex = 655
        Me.RPM_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_64
        '
        Me.AlarmCode_64.Location = New System.Drawing.Point(1195, 484)
        Me.AlarmCode_64.Name = "AlarmCode_64"
        Me.AlarmCode_64.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_64.TabIndex = 654
        Me.AlarmCode_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_63
        '
        Me.Status_63.Location = New System.Drawing.Point(958, 456)
        Me.Status_63.Name = "Status_63"
        Me.Status_63.Size = New System.Drawing.Size(55, 22)
        Me.Status_63.TabIndex = 653
        Me.Status_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_63
        '
        Me.RPMS_63.Location = New System.Drawing.Point(1037, 456)
        Me.RPMS_63.Name = "RPMS_63"
        Me.RPMS_63.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_63.TabIndex = 652
        Me.RPMS_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox185
        '
        Me.TextBox185.Location = New System.Drawing.Point(1195, 456)
        Me.TextBox185.Name = "TextBox185"
        Me.TextBox185.Size = New System.Drawing.Size(55, 22)
        Me.TextBox185.TabIndex = 651
        Me.TextBox185.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_63
        '
        Me.RPM_63.Location = New System.Drawing.Point(1116, 456)
        Me.RPM_63.Name = "RPM_63"
        Me.RPM_63.Size = New System.Drawing.Size(55, 22)
        Me.RPM_63.TabIndex = 650
        Me.RPM_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(1188, 379)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(62, 12)
        Me.Label46.TabIndex = 649
        Me.Label46.Text = "Alarm Code"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(1142, 379)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(29, 12)
        Me.Label47.TabIndex = 648
        Me.Label47.Text = "RPM"
        '
        'Status_62
        '
        Me.Status_62.Location = New System.Drawing.Point(958, 426)
        Me.Status_62.Name = "Status_62"
        Me.Status_62.Size = New System.Drawing.Size(55, 22)
        Me.Status_62.TabIndex = 647
        Me.Status_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_62
        '
        Me.RPMS_62.Location = New System.Drawing.Point(1037, 426)
        Me.RPMS_62.Name = "RPMS_62"
        Me.RPMS_62.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_62.TabIndex = 646
        Me.RPMS_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_61
        '
        Me.RPMS_61.Location = New System.Drawing.Point(1037, 398)
        Me.RPMS_61.Name = "RPMS_61"
        Me.RPMS_61.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_61.TabIndex = 645
        Me.RPMS_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox190
        '
        Me.TextBox190.Location = New System.Drawing.Point(1195, 398)
        Me.TextBox190.Name = "TextBox190"
        Me.TextBox190.Size = New System.Drawing.Size(55, 22)
        Me.TextBox190.TabIndex = 644
        Me.TextBox190.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label48.Location = New System.Drawing.Point(917, 715)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(35, 13)
        Me.Label48.TabIndex = 643
        Me.Label48.Text = "ID:72"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label49.Location = New System.Drawing.Point(917, 687)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(35, 13)
        Me.Label49.TabIndex = 642
        Me.Label49.Text = "ID:71"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label50.Location = New System.Drawing.Point(917, 659)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(35, 13)
        Me.Label50.TabIndex = 641
        Me.Label50.Text = "ID:70"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label51.Location = New System.Drawing.Point(917, 631)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(35, 13)
        Me.Label51.TabIndex = 640
        Me.Label51.Text = "ID:69"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label52.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label52.Location = New System.Drawing.Point(917, 603)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(35, 13)
        Me.Label52.TabIndex = 639
        Me.Label52.Text = "ID:68"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label53.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label53.Location = New System.Drawing.Point(917, 575)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(35, 13)
        Me.Label53.TabIndex = 638
        Me.Label53.Text = "ID:67"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label54.Location = New System.Drawing.Point(917, 547)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(35, 13)
        Me.Label54.TabIndex = 637
        Me.Label54.Text = "ID:66"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label55.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label55.Location = New System.Drawing.Point(917, 519)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(35, 13)
        Me.Label55.TabIndex = 636
        Me.Label55.Text = "ID:65"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label56.Location = New System.Drawing.Point(917, 491)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(35, 13)
        Me.Label56.TabIndex = 635
        Me.Label56.Text = "ID:64"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label57.Location = New System.Drawing.Point(917, 463)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(35, 13)
        Me.Label57.TabIndex = 634
        Me.Label57.Text = "ID:63"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label58.Location = New System.Drawing.Point(917, 435)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(35, 13)
        Me.Label58.TabIndex = 633
        Me.Label58.Text = "ID:62"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label59.Location = New System.Drawing.Point(917, 407)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(35, 13)
        Me.Label59.TabIndex = 632
        Me.Label59.Text = "ID:61"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(1046, 379)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(46, 12)
        Me.Label60.TabIndex = 631
        Me.Label60.Text = "RPM Set"
        '
        'TextBox191
        '
        Me.TextBox191.Location = New System.Drawing.Point(1195, 426)
        Me.TextBox191.Name = "TextBox191"
        Me.TextBox191.Size = New System.Drawing.Size(55, 22)
        Me.TextBox191.TabIndex = 630
        Me.TextBox191.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(981, 379)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(32, 12)
        Me.Label61.TabIndex = 629
        Me.Label61.Text = "Status"
        '
        'Status_61
        '
        Me.Status_61.Location = New System.Drawing.Point(958, 398)
        Me.Status_61.Name = "Status_61"
        Me.Status_61.Size = New System.Drawing.Size(55, 22)
        Me.Status_61.TabIndex = 628
        Me.Status_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_62
        '
        Me.RPM_62.Location = New System.Drawing.Point(1116, 426)
        Me.RPM_62.Name = "RPM_62"
        Me.RPM_62.Size = New System.Drawing.Size(55, 22)
        Me.RPM_62.TabIndex = 627
        Me.RPM_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_61
        '
        Me.RPM_61.Location = New System.Drawing.Point(1116, 398)
        Me.RPM_61.Name = "RPM_61"
        Me.RPM_61.Size = New System.Drawing.Size(55, 22)
        Me.RPM_61.TabIndex = 626
        Me.RPM_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_60
        '
        Me.Status_60.Location = New System.Drawing.Point(609, 708)
        Me.Status_60.Name = "Status_60"
        Me.Status_60.Size = New System.Drawing.Size(55, 22)
        Me.Status_60.TabIndex = 625
        Me.Status_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_60
        '
        Me.RPMS_60.Location = New System.Drawing.Point(688, 708)
        Me.RPMS_60.Name = "RPMS_60"
        Me.RPMS_60.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_60.TabIndex = 624
        Me.RPMS_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox197
        '
        Me.TextBox197.Location = New System.Drawing.Point(846, 708)
        Me.TextBox197.Name = "TextBox197"
        Me.TextBox197.Size = New System.Drawing.Size(55, 22)
        Me.TextBox197.TabIndex = 623
        Me.TextBox197.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_60
        '
        Me.RPM_60.Location = New System.Drawing.Point(767, 708)
        Me.RPM_60.Name = "RPM_60"
        Me.RPM_60.Size = New System.Drawing.Size(55, 22)
        Me.RPM_60.TabIndex = 622
        Me.RPM_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_59
        '
        Me.Status_59.Location = New System.Drawing.Point(609, 680)
        Me.Status_59.Name = "Status_59"
        Me.Status_59.Size = New System.Drawing.Size(55, 22)
        Me.Status_59.TabIndex = 621
        Me.Status_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_59
        '
        Me.RPMS_59.Location = New System.Drawing.Point(688, 680)
        Me.RPMS_59.Name = "RPMS_59"
        Me.RPMS_59.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_59.TabIndex = 620
        Me.RPMS_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox201
        '
        Me.TextBox201.Location = New System.Drawing.Point(846, 680)
        Me.TextBox201.Name = "TextBox201"
        Me.TextBox201.Size = New System.Drawing.Size(55, 22)
        Me.TextBox201.TabIndex = 619
        Me.TextBox201.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_59
        '
        Me.RPM_59.Location = New System.Drawing.Point(767, 680)
        Me.RPM_59.Name = "RPM_59"
        Me.RPM_59.Size = New System.Drawing.Size(55, 22)
        Me.RPM_59.TabIndex = 618
        Me.RPM_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_58
        '
        Me.Status_58.Location = New System.Drawing.Point(609, 652)
        Me.Status_58.Name = "Status_58"
        Me.Status_58.Size = New System.Drawing.Size(55, 22)
        Me.Status_58.TabIndex = 617
        Me.Status_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_58
        '
        Me.RPMS_58.Location = New System.Drawing.Point(688, 652)
        Me.RPMS_58.Name = "RPMS_58"
        Me.RPMS_58.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_58.TabIndex = 616
        Me.RPMS_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox205
        '
        Me.TextBox205.Location = New System.Drawing.Point(846, 652)
        Me.TextBox205.Name = "TextBox205"
        Me.TextBox205.Size = New System.Drawing.Size(55, 22)
        Me.TextBox205.TabIndex = 615
        Me.TextBox205.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_58
        '
        Me.RPM_58.Location = New System.Drawing.Point(767, 652)
        Me.RPM_58.Name = "RPM_58"
        Me.RPM_58.Size = New System.Drawing.Size(55, 22)
        Me.RPM_58.TabIndex = 614
        Me.RPM_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_57
        '
        Me.Status_57.Location = New System.Drawing.Point(609, 624)
        Me.Status_57.Name = "Status_57"
        Me.Status_57.Size = New System.Drawing.Size(55, 22)
        Me.Status_57.TabIndex = 613
        Me.Status_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_57
        '
        Me.RPMS_57.Location = New System.Drawing.Point(688, 624)
        Me.RPMS_57.Name = "RPMS_57"
        Me.RPMS_57.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_57.TabIndex = 612
        Me.RPMS_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox209
        '
        Me.TextBox209.Location = New System.Drawing.Point(846, 624)
        Me.TextBox209.Name = "TextBox209"
        Me.TextBox209.Size = New System.Drawing.Size(55, 22)
        Me.TextBox209.TabIndex = 611
        Me.TextBox209.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_57
        '
        Me.RPM_57.Location = New System.Drawing.Point(767, 624)
        Me.RPM_57.Name = "RPM_57"
        Me.RPM_57.Size = New System.Drawing.Size(55, 22)
        Me.RPM_57.TabIndex = 610
        Me.RPM_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_56
        '
        Me.Status_56.Location = New System.Drawing.Point(609, 596)
        Me.Status_56.Name = "Status_56"
        Me.Status_56.Size = New System.Drawing.Size(55, 22)
        Me.Status_56.TabIndex = 609
        Me.Status_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_56
        '
        Me.RPMS_56.Location = New System.Drawing.Point(688, 596)
        Me.RPMS_56.Name = "RPMS_56"
        Me.RPMS_56.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_56.TabIndex = 608
        Me.RPMS_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox213
        '
        Me.TextBox213.Location = New System.Drawing.Point(846, 596)
        Me.TextBox213.Name = "TextBox213"
        Me.TextBox213.Size = New System.Drawing.Size(55, 22)
        Me.TextBox213.TabIndex = 607
        Me.TextBox213.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_56
        '
        Me.RPM_56.Location = New System.Drawing.Point(767, 596)
        Me.RPM_56.Name = "RPM_56"
        Me.RPM_56.Size = New System.Drawing.Size(55, 22)
        Me.RPM_56.TabIndex = 606
        Me.RPM_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_55
        '
        Me.Status_55.Location = New System.Drawing.Point(609, 568)
        Me.Status_55.Name = "Status_55"
        Me.Status_55.Size = New System.Drawing.Size(55, 22)
        Me.Status_55.TabIndex = 605
        Me.Status_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_55
        '
        Me.RPMS_55.Location = New System.Drawing.Point(688, 568)
        Me.RPMS_55.Name = "RPMS_55"
        Me.RPMS_55.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_55.TabIndex = 604
        Me.RPMS_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox217
        '
        Me.TextBox217.Location = New System.Drawing.Point(846, 568)
        Me.TextBox217.Name = "TextBox217"
        Me.TextBox217.Size = New System.Drawing.Size(55, 22)
        Me.TextBox217.TabIndex = 603
        Me.TextBox217.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_55
        '
        Me.RPM_55.Location = New System.Drawing.Point(767, 568)
        Me.RPM_55.Name = "RPM_55"
        Me.RPM_55.Size = New System.Drawing.Size(55, 22)
        Me.RPM_55.TabIndex = 602
        Me.RPM_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_54
        '
        Me.Status_54.Location = New System.Drawing.Point(609, 540)
        Me.Status_54.Name = "Status_54"
        Me.Status_54.Size = New System.Drawing.Size(55, 22)
        Me.Status_54.TabIndex = 601
        Me.Status_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_54
        '
        Me.RPMS_54.Location = New System.Drawing.Point(688, 540)
        Me.RPMS_54.Name = "RPMS_54"
        Me.RPMS_54.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_54.TabIndex = 600
        Me.RPMS_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox221
        '
        Me.TextBox221.Location = New System.Drawing.Point(846, 540)
        Me.TextBox221.Name = "TextBox221"
        Me.TextBox221.Size = New System.Drawing.Size(55, 22)
        Me.TextBox221.TabIndex = 599
        Me.TextBox221.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_54
        '
        Me.RPM_54.Location = New System.Drawing.Point(767, 540)
        Me.RPM_54.Name = "RPM_54"
        Me.RPM_54.Size = New System.Drawing.Size(55, 22)
        Me.RPM_54.TabIndex = 598
        Me.RPM_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_53
        '
        Me.Status_53.Location = New System.Drawing.Point(609, 512)
        Me.Status_53.Name = "Status_53"
        Me.Status_53.Size = New System.Drawing.Size(55, 22)
        Me.Status_53.TabIndex = 597
        Me.Status_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_53
        '
        Me.RPMS_53.Location = New System.Drawing.Point(688, 512)
        Me.RPMS_53.Name = "RPMS_53"
        Me.RPMS_53.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_53.TabIndex = 596
        Me.RPMS_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox225
        '
        Me.TextBox225.Location = New System.Drawing.Point(846, 512)
        Me.TextBox225.Name = "TextBox225"
        Me.TextBox225.Size = New System.Drawing.Size(55, 22)
        Me.TextBox225.TabIndex = 595
        Me.TextBox225.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_53
        '
        Me.RPM_53.Location = New System.Drawing.Point(767, 512)
        Me.RPM_53.Name = "RPM_53"
        Me.RPM_53.Size = New System.Drawing.Size(55, 22)
        Me.RPM_53.TabIndex = 594
        Me.RPM_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_52
        '
        Me.Status_52.Location = New System.Drawing.Point(609, 484)
        Me.Status_52.Name = "Status_52"
        Me.Status_52.Size = New System.Drawing.Size(55, 22)
        Me.Status_52.TabIndex = 593
        Me.Status_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_52
        '
        Me.RPMS_52.Location = New System.Drawing.Point(688, 484)
        Me.RPMS_52.Name = "RPMS_52"
        Me.RPMS_52.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_52.TabIndex = 592
        Me.RPMS_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox229
        '
        Me.TextBox229.Location = New System.Drawing.Point(846, 484)
        Me.TextBox229.Name = "TextBox229"
        Me.TextBox229.Size = New System.Drawing.Size(55, 22)
        Me.TextBox229.TabIndex = 591
        Me.TextBox229.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_52
        '
        Me.RPM_52.Location = New System.Drawing.Point(767, 484)
        Me.RPM_52.Name = "RPM_52"
        Me.RPM_52.Size = New System.Drawing.Size(55, 22)
        Me.RPM_52.TabIndex = 590
        Me.RPM_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_51
        '
        Me.Status_51.Location = New System.Drawing.Point(609, 456)
        Me.Status_51.Name = "Status_51"
        Me.Status_51.Size = New System.Drawing.Size(55, 22)
        Me.Status_51.TabIndex = 589
        Me.Status_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_51
        '
        Me.RPMS_51.Location = New System.Drawing.Point(688, 456)
        Me.RPMS_51.Name = "RPMS_51"
        Me.RPMS_51.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_51.TabIndex = 588
        Me.RPMS_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox233
        '
        Me.TextBox233.Location = New System.Drawing.Point(846, 456)
        Me.TextBox233.Name = "TextBox233"
        Me.TextBox233.Size = New System.Drawing.Size(55, 22)
        Me.TextBox233.TabIndex = 587
        Me.TextBox233.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_51
        '
        Me.RPM_51.Location = New System.Drawing.Point(767, 456)
        Me.RPM_51.Name = "RPM_51"
        Me.RPM_51.Size = New System.Drawing.Size(55, 22)
        Me.RPM_51.TabIndex = 586
        Me.RPM_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(839, 379)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(62, 12)
        Me.Label62.TabIndex = 585
        Me.Label62.Text = "Alarm Code"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(793, 379)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(29, 12)
        Me.Label63.TabIndex = 584
        Me.Label63.Text = "RPM"
        '
        'Status_50
        '
        Me.Status_50.Location = New System.Drawing.Point(609, 426)
        Me.Status_50.Name = "Status_50"
        Me.Status_50.Size = New System.Drawing.Size(55, 22)
        Me.Status_50.TabIndex = 583
        Me.Status_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_50
        '
        Me.RPMS_50.Location = New System.Drawing.Point(688, 426)
        Me.RPMS_50.Name = "RPMS_50"
        Me.RPMS_50.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_50.TabIndex = 582
        Me.RPMS_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_49
        '
        Me.RPMS_49.Location = New System.Drawing.Point(688, 398)
        Me.RPMS_49.Name = "RPMS_49"
        Me.RPMS_49.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_49.TabIndex = 581
        Me.RPMS_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox238
        '
        Me.TextBox238.Location = New System.Drawing.Point(846, 398)
        Me.TextBox238.Name = "TextBox238"
        Me.TextBox238.Size = New System.Drawing.Size(55, 22)
        Me.TextBox238.TabIndex = 580
        Me.TextBox238.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label64.Location = New System.Drawing.Point(568, 715)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(35, 13)
        Me.Label64.TabIndex = 579
        Me.Label64.Text = "ID:60"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label65.Location = New System.Drawing.Point(568, 687)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(35, 13)
        Me.Label65.TabIndex = 578
        Me.Label65.Text = "ID:59"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label66.Location = New System.Drawing.Point(568, 659)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(35, 13)
        Me.Label66.TabIndex = 577
        Me.Label66.Text = "ID:58"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label67.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label67.Location = New System.Drawing.Point(568, 631)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(35, 13)
        Me.Label67.TabIndex = 576
        Me.Label67.Text = "ID:57"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label68.Location = New System.Drawing.Point(568, 603)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(35, 13)
        Me.Label68.TabIndex = 575
        Me.Label68.Text = "ID:56"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label69.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label69.Location = New System.Drawing.Point(568, 575)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(35, 13)
        Me.Label69.TabIndex = 574
        Me.Label69.Text = "ID:55"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label70.Location = New System.Drawing.Point(568, 547)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(35, 13)
        Me.Label70.TabIndex = 573
        Me.Label70.Text = "ID:54"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label71.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label71.Location = New System.Drawing.Point(568, 519)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(35, 13)
        Me.Label71.TabIndex = 572
        Me.Label71.Text = "ID:53"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label72.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label72.Location = New System.Drawing.Point(568, 491)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(35, 13)
        Me.Label72.TabIndex = 571
        Me.Label72.Text = "ID:52"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label73.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label73.Location = New System.Drawing.Point(568, 463)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(35, 13)
        Me.Label73.TabIndex = 570
        Me.Label73.Text = "ID:51"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label74.Location = New System.Drawing.Point(568, 435)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(35, 13)
        Me.Label74.TabIndex = 569
        Me.Label74.Text = "ID:50"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label75.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label75.Location = New System.Drawing.Point(568, 407)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(35, 13)
        Me.Label75.TabIndex = 568
        Me.Label75.Text = "ID:49"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(697, 379)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(46, 12)
        Me.Label76.TabIndex = 567
        Me.Label76.Text = "RPM Set"
        '
        'TextBox239
        '
        Me.TextBox239.Location = New System.Drawing.Point(846, 426)
        Me.TextBox239.Name = "TextBox239"
        Me.TextBox239.Size = New System.Drawing.Size(55, 22)
        Me.TextBox239.TabIndex = 566
        Me.TextBox239.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(632, 379)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(32, 12)
        Me.Label77.TabIndex = 565
        Me.Label77.Text = "Status"
        '
        'Status_49
        '
        Me.Status_49.Location = New System.Drawing.Point(609, 398)
        Me.Status_49.Name = "Status_49"
        Me.Status_49.Size = New System.Drawing.Size(55, 22)
        Me.Status_49.TabIndex = 564
        Me.Status_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_50
        '
        Me.RPM_50.Location = New System.Drawing.Point(767, 426)
        Me.RPM_50.Name = "RPM_50"
        Me.RPM_50.Size = New System.Drawing.Size(55, 22)
        Me.RPM_50.TabIndex = 563
        Me.RPM_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_49
        '
        Me.RPM_49.Location = New System.Drawing.Point(767, 398)
        Me.RPM_49.Name = "RPM_49"
        Me.RPM_49.Size = New System.Drawing.Size(55, 22)
        Me.RPM_49.TabIndex = 562
        Me.RPM_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_48
        '
        Me.Status_48.Location = New System.Drawing.Point(264, 708)
        Me.Status_48.Name = "Status_48"
        Me.Status_48.Size = New System.Drawing.Size(55, 22)
        Me.Status_48.TabIndex = 561
        Me.Status_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_48
        '
        Me.RPMS_48.Location = New System.Drawing.Point(343, 708)
        Me.RPMS_48.Name = "RPMS_48"
        Me.RPMS_48.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_48.TabIndex = 560
        Me.RPMS_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_48
        '
        Me.AlarmCode_48.Location = New System.Drawing.Point(501, 708)
        Me.AlarmCode_48.Name = "AlarmCode_48"
        Me.AlarmCode_48.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_48.TabIndex = 559
        Me.AlarmCode_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_48
        '
        Me.RPM_48.Location = New System.Drawing.Point(422, 708)
        Me.RPM_48.Name = "RPM_48"
        Me.RPM_48.Size = New System.Drawing.Size(55, 22)
        Me.RPM_48.TabIndex = 558
        Me.RPM_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_47
        '
        Me.Status_47.Location = New System.Drawing.Point(264, 680)
        Me.Status_47.Name = "Status_47"
        Me.Status_47.Size = New System.Drawing.Size(55, 22)
        Me.Status_47.TabIndex = 557
        Me.Status_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_47
        '
        Me.RPMS_47.Location = New System.Drawing.Point(343, 680)
        Me.RPMS_47.Name = "RPMS_47"
        Me.RPMS_47.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_47.TabIndex = 556
        Me.RPMS_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_47
        '
        Me.AlarmCode_47.Location = New System.Drawing.Point(501, 680)
        Me.AlarmCode_47.Name = "AlarmCode_47"
        Me.AlarmCode_47.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_47.TabIndex = 555
        Me.AlarmCode_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_47
        '
        Me.RPM_47.Location = New System.Drawing.Point(422, 680)
        Me.RPM_47.Name = "RPM_47"
        Me.RPM_47.Size = New System.Drawing.Size(55, 22)
        Me.RPM_47.TabIndex = 554
        Me.RPM_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_46
        '
        Me.Status_46.Location = New System.Drawing.Point(264, 652)
        Me.Status_46.Name = "Status_46"
        Me.Status_46.Size = New System.Drawing.Size(55, 22)
        Me.Status_46.TabIndex = 553
        Me.Status_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_46
        '
        Me.RPMS_46.Location = New System.Drawing.Point(343, 652)
        Me.RPMS_46.Name = "RPMS_46"
        Me.RPMS_46.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_46.TabIndex = 552
        Me.RPMS_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_46
        '
        Me.AlarmCode_46.Location = New System.Drawing.Point(501, 652)
        Me.AlarmCode_46.Name = "AlarmCode_46"
        Me.AlarmCode_46.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_46.TabIndex = 551
        Me.AlarmCode_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_46
        '
        Me.RPM_46.Location = New System.Drawing.Point(422, 652)
        Me.RPM_46.Name = "RPM_46"
        Me.RPM_46.Size = New System.Drawing.Size(55, 22)
        Me.RPM_46.TabIndex = 550
        Me.RPM_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_45
        '
        Me.Status_45.Location = New System.Drawing.Point(264, 624)
        Me.Status_45.Name = "Status_45"
        Me.Status_45.Size = New System.Drawing.Size(55, 22)
        Me.Status_45.TabIndex = 549
        Me.Status_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_45
        '
        Me.RPMS_45.Location = New System.Drawing.Point(343, 624)
        Me.RPMS_45.Name = "RPMS_45"
        Me.RPMS_45.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_45.TabIndex = 548
        Me.RPMS_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_45
        '
        Me.AlarmCode_45.Location = New System.Drawing.Point(501, 624)
        Me.AlarmCode_45.Name = "AlarmCode_45"
        Me.AlarmCode_45.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_45.TabIndex = 547
        Me.AlarmCode_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_45
        '
        Me.RPM_45.Location = New System.Drawing.Point(422, 624)
        Me.RPM_45.Name = "RPM_45"
        Me.RPM_45.Size = New System.Drawing.Size(55, 22)
        Me.RPM_45.TabIndex = 546
        Me.RPM_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_44
        '
        Me.Status_44.Location = New System.Drawing.Point(264, 596)
        Me.Status_44.Name = "Status_44"
        Me.Status_44.Size = New System.Drawing.Size(55, 22)
        Me.Status_44.TabIndex = 545
        Me.Status_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_44
        '
        Me.RPMS_44.Location = New System.Drawing.Point(343, 596)
        Me.RPMS_44.Name = "RPMS_44"
        Me.RPMS_44.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_44.TabIndex = 544
        Me.RPMS_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_44
        '
        Me.AlarmCode_44.Location = New System.Drawing.Point(501, 596)
        Me.AlarmCode_44.Name = "AlarmCode_44"
        Me.AlarmCode_44.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_44.TabIndex = 543
        Me.AlarmCode_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_44
        '
        Me.RPM_44.Location = New System.Drawing.Point(422, 596)
        Me.RPM_44.Name = "RPM_44"
        Me.RPM_44.Size = New System.Drawing.Size(55, 22)
        Me.RPM_44.TabIndex = 542
        Me.RPM_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_43
        '
        Me.Status_43.Location = New System.Drawing.Point(264, 568)
        Me.Status_43.Name = "Status_43"
        Me.Status_43.Size = New System.Drawing.Size(55, 22)
        Me.Status_43.TabIndex = 541
        Me.Status_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_43
        '
        Me.RPMS_43.Location = New System.Drawing.Point(343, 568)
        Me.RPMS_43.Name = "RPMS_43"
        Me.RPMS_43.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_43.TabIndex = 540
        Me.RPMS_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_43
        '
        Me.AlarmCode_43.Location = New System.Drawing.Point(501, 568)
        Me.AlarmCode_43.Name = "AlarmCode_43"
        Me.AlarmCode_43.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_43.TabIndex = 539
        Me.AlarmCode_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_43
        '
        Me.RPM_43.Location = New System.Drawing.Point(422, 568)
        Me.RPM_43.Name = "RPM_43"
        Me.RPM_43.Size = New System.Drawing.Size(55, 22)
        Me.RPM_43.TabIndex = 538
        Me.RPM_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_42
        '
        Me.Status_42.Location = New System.Drawing.Point(264, 540)
        Me.Status_42.Name = "Status_42"
        Me.Status_42.Size = New System.Drawing.Size(55, 22)
        Me.Status_42.TabIndex = 537
        Me.Status_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_42
        '
        Me.RPMS_42.Location = New System.Drawing.Point(343, 540)
        Me.RPMS_42.Name = "RPMS_42"
        Me.RPMS_42.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_42.TabIndex = 536
        Me.RPMS_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_42
        '
        Me.AlarmCode_42.Location = New System.Drawing.Point(501, 540)
        Me.AlarmCode_42.Name = "AlarmCode_42"
        Me.AlarmCode_42.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_42.TabIndex = 535
        Me.AlarmCode_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_42
        '
        Me.RPM_42.Location = New System.Drawing.Point(422, 540)
        Me.RPM_42.Name = "RPM_42"
        Me.RPM_42.Size = New System.Drawing.Size(55, 22)
        Me.RPM_42.TabIndex = 534
        Me.RPM_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_41
        '
        Me.Status_41.Location = New System.Drawing.Point(264, 512)
        Me.Status_41.Name = "Status_41"
        Me.Status_41.Size = New System.Drawing.Size(55, 22)
        Me.Status_41.TabIndex = 533
        Me.Status_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_41
        '
        Me.RPMS_41.Location = New System.Drawing.Point(343, 512)
        Me.RPMS_41.Name = "RPMS_41"
        Me.RPMS_41.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_41.TabIndex = 532
        Me.RPMS_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_41
        '
        Me.AlarmCode_41.Location = New System.Drawing.Point(501, 512)
        Me.AlarmCode_41.Name = "AlarmCode_41"
        Me.AlarmCode_41.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_41.TabIndex = 531
        Me.AlarmCode_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_41
        '
        Me.RPM_41.Location = New System.Drawing.Point(422, 512)
        Me.RPM_41.Name = "RPM_41"
        Me.RPM_41.Size = New System.Drawing.Size(55, 22)
        Me.RPM_41.TabIndex = 530
        Me.RPM_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_40
        '
        Me.Status_40.Location = New System.Drawing.Point(264, 484)
        Me.Status_40.Name = "Status_40"
        Me.Status_40.Size = New System.Drawing.Size(55, 22)
        Me.Status_40.TabIndex = 529
        Me.Status_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_40
        '
        Me.RPMS_40.Location = New System.Drawing.Point(343, 484)
        Me.RPMS_40.Name = "RPMS_40"
        Me.RPMS_40.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_40.TabIndex = 528
        Me.RPMS_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_40
        '
        Me.AlarmCode_40.Location = New System.Drawing.Point(501, 484)
        Me.AlarmCode_40.Name = "AlarmCode_40"
        Me.AlarmCode_40.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_40.TabIndex = 527
        Me.AlarmCode_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_40
        '
        Me.RPM_40.Location = New System.Drawing.Point(422, 484)
        Me.RPM_40.Name = "RPM_40"
        Me.RPM_40.Size = New System.Drawing.Size(55, 22)
        Me.RPM_40.TabIndex = 526
        Me.RPM_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_39
        '
        Me.Status_39.Location = New System.Drawing.Point(264, 456)
        Me.Status_39.Name = "Status_39"
        Me.Status_39.Size = New System.Drawing.Size(55, 22)
        Me.Status_39.TabIndex = 525
        Me.Status_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_39
        '
        Me.RPMS_39.Location = New System.Drawing.Point(343, 456)
        Me.RPMS_39.Name = "RPMS_39"
        Me.RPMS_39.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_39.TabIndex = 524
        Me.RPMS_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_39
        '
        Me.AlarmCode_39.Location = New System.Drawing.Point(501, 456)
        Me.AlarmCode_39.Name = "AlarmCode_39"
        Me.AlarmCode_39.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_39.TabIndex = 523
        Me.AlarmCode_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_39
        '
        Me.RPM_39.Location = New System.Drawing.Point(422, 456)
        Me.RPM_39.Name = "RPM_39"
        Me.RPM_39.Size = New System.Drawing.Size(55, 22)
        Me.RPM_39.TabIndex = 522
        Me.RPM_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(494, 379)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(62, 12)
        Me.Label78.TabIndex = 521
        Me.Label78.Text = "Alarm Code"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(448, 379)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(29, 12)
        Me.Label79.TabIndex = 520
        Me.Label79.Text = "RPM"
        '
        'Status_38
        '
        Me.Status_38.Location = New System.Drawing.Point(264, 426)
        Me.Status_38.Name = "Status_38"
        Me.Status_38.Size = New System.Drawing.Size(55, 22)
        Me.Status_38.TabIndex = 519
        Me.Status_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_38
        '
        Me.RPMS_38.Location = New System.Drawing.Point(343, 426)
        Me.RPMS_38.Name = "RPMS_38"
        Me.RPMS_38.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_38.TabIndex = 518
        Me.RPMS_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_37
        '
        Me.RPMS_37.Location = New System.Drawing.Point(343, 398)
        Me.RPMS_37.Name = "RPMS_37"
        Me.RPMS_37.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_37.TabIndex = 517
        Me.RPMS_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_37
        '
        Me.AlarmCode_37.Location = New System.Drawing.Point(501, 398)
        Me.AlarmCode_37.Name = "AlarmCode_37"
        Me.AlarmCode_37.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_37.TabIndex = 516
        Me.AlarmCode_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label80.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label80.Location = New System.Drawing.Point(223, 715)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(35, 13)
        Me.Label80.TabIndex = 515
        Me.Label80.Text = "ID:48"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label81.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label81.Location = New System.Drawing.Point(223, 687)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(35, 13)
        Me.Label81.TabIndex = 514
        Me.Label81.Text = "ID:47"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label82.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label82.Location = New System.Drawing.Point(223, 659)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(35, 13)
        Me.Label82.TabIndex = 513
        Me.Label82.Text = "ID:46"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label83.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label83.Location = New System.Drawing.Point(223, 631)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(35, 13)
        Me.Label83.TabIndex = 512
        Me.Label83.Text = "ID:45"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label84.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label84.Location = New System.Drawing.Point(223, 603)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(35, 13)
        Me.Label84.TabIndex = 511
        Me.Label84.Text = "ID:44"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label85.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label85.Location = New System.Drawing.Point(223, 575)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(35, 13)
        Me.Label85.TabIndex = 510
        Me.Label85.Text = "ID:43"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label86.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label86.Location = New System.Drawing.Point(223, 547)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(35, 13)
        Me.Label86.TabIndex = 509
        Me.Label86.Text = "ID:42"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label87.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label87.Location = New System.Drawing.Point(223, 519)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(35, 13)
        Me.Label87.TabIndex = 508
        Me.Label87.Text = "ID:41"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label88.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label88.Location = New System.Drawing.Point(223, 491)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(35, 13)
        Me.Label88.TabIndex = 507
        Me.Label88.Text = "ID:40"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label89.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label89.Location = New System.Drawing.Point(223, 463)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(35, 13)
        Me.Label89.TabIndex = 506
        Me.Label89.Text = "ID:39"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label90.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label90.Location = New System.Drawing.Point(223, 435)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(35, 13)
        Me.Label90.TabIndex = 505
        Me.Label90.Text = "ID:38"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label91.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label91.Location = New System.Drawing.Point(223, 407)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(35, 13)
        Me.Label91.TabIndex = 504
        Me.Label91.Text = "ID:37"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(352, 379)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(46, 12)
        Me.Label92.TabIndex = 503
        Me.Label92.Text = "RPM Set"
        '
        'AlarmCode_38
        '
        Me.AlarmCode_38.Location = New System.Drawing.Point(501, 426)
        Me.AlarmCode_38.Name = "AlarmCode_38"
        Me.AlarmCode_38.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_38.TabIndex = 502
        Me.AlarmCode_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(287, 379)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(32, 12)
        Me.Label93.TabIndex = 501
        Me.Label93.Text = "Status"
        '
        'Status_37
        '
        Me.Status_37.Location = New System.Drawing.Point(264, 398)
        Me.Status_37.Name = "Status_37"
        Me.Status_37.Size = New System.Drawing.Size(55, 22)
        Me.Status_37.TabIndex = 500
        Me.Status_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_38
        '
        Me.RPM_38.Location = New System.Drawing.Point(422, 426)
        Me.RPM_38.Name = "RPM_38"
        Me.RPM_38.Size = New System.Drawing.Size(55, 22)
        Me.RPM_38.TabIndex = 499
        Me.RPM_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_37
        '
        Me.RPM_37.Location = New System.Drawing.Point(422, 398)
        Me.RPM_37.Name = "RPM_37"
        Me.RPM_37.Size = New System.Drawing.Size(55, 22)
        Me.RPM_37.TabIndex = 498
        Me.RPM_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(99, 322)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 23)
        Me.Button2.TabIndex = 690
        Me.Button2.Text = "讀取"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.SkyBlue
        Me.ClientSize = New System.Drawing.Size(1270, 744)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Status_72)
        Me.Controls.Add(Me.RPMS_72)
        Me.Controls.Add(Me.TextBox149)
        Me.Controls.Add(Me.RPM_72)
        Me.Controls.Add(Me.Status_71)
        Me.Controls.Add(Me.RPMS_71)
        Me.Controls.Add(Me.TextBox153)
        Me.Controls.Add(Me.RPM_71)
        Me.Controls.Add(Me.Status_70)
        Me.Controls.Add(Me.RPMS_70)
        Me.Controls.Add(Me.TextBox157)
        Me.Controls.Add(Me.RPM_70)
        Me.Controls.Add(Me.Status_69)
        Me.Controls.Add(Me.RPMS_69)
        Me.Controls.Add(Me.TextBox161)
        Me.Controls.Add(Me.RPM_69)
        Me.Controls.Add(Me.Status_68)
        Me.Controls.Add(Me.RPMS_68)
        Me.Controls.Add(Me.TextBox165)
        Me.Controls.Add(Me.RPM_68)
        Me.Controls.Add(Me.Status_67)
        Me.Controls.Add(Me.RPMS_67)
        Me.Controls.Add(Me.TextBox169)
        Me.Controls.Add(Me.RPM_67)
        Me.Controls.Add(Me.Status_66)
        Me.Controls.Add(Me.RPMS_66)
        Me.Controls.Add(Me.TextBox173)
        Me.Controls.Add(Me.RPM_66)
        Me.Controls.Add(Me.Status_65)
        Me.Controls.Add(Me.RPMS_65)
        Me.Controls.Add(Me.TextBox177)
        Me.Controls.Add(Me.RPM_65)
        Me.Controls.Add(Me.Status_64)
        Me.Controls.Add(Me.RPMS_64)
        Me.Controls.Add(Me.RPM_64)
        Me.Controls.Add(Me.AlarmCode_64)
        Me.Controls.Add(Me.Status_63)
        Me.Controls.Add(Me.RPMS_63)
        Me.Controls.Add(Me.TextBox185)
        Me.Controls.Add(Me.RPM_63)
        Me.Controls.Add(Me.Label46)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.Status_62)
        Me.Controls.Add(Me.RPMS_62)
        Me.Controls.Add(Me.RPMS_61)
        Me.Controls.Add(Me.TextBox190)
        Me.Controls.Add(Me.Label48)
        Me.Controls.Add(Me.Label49)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.Label51)
        Me.Controls.Add(Me.Label52)
        Me.Controls.Add(Me.Label53)
        Me.Controls.Add(Me.Label54)
        Me.Controls.Add(Me.Label55)
        Me.Controls.Add(Me.Label56)
        Me.Controls.Add(Me.Label57)
        Me.Controls.Add(Me.Label58)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.TextBox191)
        Me.Controls.Add(Me.Label61)
        Me.Controls.Add(Me.Status_61)
        Me.Controls.Add(Me.RPM_62)
        Me.Controls.Add(Me.RPM_61)
        Me.Controls.Add(Me.Status_60)
        Me.Controls.Add(Me.RPMS_60)
        Me.Controls.Add(Me.TextBox197)
        Me.Controls.Add(Me.RPM_60)
        Me.Controls.Add(Me.Status_59)
        Me.Controls.Add(Me.RPMS_59)
        Me.Controls.Add(Me.TextBox201)
        Me.Controls.Add(Me.RPM_59)
        Me.Controls.Add(Me.Status_58)
        Me.Controls.Add(Me.RPMS_58)
        Me.Controls.Add(Me.TextBox205)
        Me.Controls.Add(Me.RPM_58)
        Me.Controls.Add(Me.Status_57)
        Me.Controls.Add(Me.RPMS_57)
        Me.Controls.Add(Me.TextBox209)
        Me.Controls.Add(Me.RPM_57)
        Me.Controls.Add(Me.Status_56)
        Me.Controls.Add(Me.RPMS_56)
        Me.Controls.Add(Me.TextBox213)
        Me.Controls.Add(Me.RPM_56)
        Me.Controls.Add(Me.Status_55)
        Me.Controls.Add(Me.RPMS_55)
        Me.Controls.Add(Me.TextBox217)
        Me.Controls.Add(Me.RPM_55)
        Me.Controls.Add(Me.Status_54)
        Me.Controls.Add(Me.RPMS_54)
        Me.Controls.Add(Me.TextBox221)
        Me.Controls.Add(Me.RPM_54)
        Me.Controls.Add(Me.Status_53)
        Me.Controls.Add(Me.RPMS_53)
        Me.Controls.Add(Me.TextBox225)
        Me.Controls.Add(Me.RPM_53)
        Me.Controls.Add(Me.Status_52)
        Me.Controls.Add(Me.RPMS_52)
        Me.Controls.Add(Me.TextBox229)
        Me.Controls.Add(Me.RPM_52)
        Me.Controls.Add(Me.Status_51)
        Me.Controls.Add(Me.RPMS_51)
        Me.Controls.Add(Me.TextBox233)
        Me.Controls.Add(Me.RPM_51)
        Me.Controls.Add(Me.Label62)
        Me.Controls.Add(Me.Label63)
        Me.Controls.Add(Me.Status_50)
        Me.Controls.Add(Me.RPMS_50)
        Me.Controls.Add(Me.RPMS_49)
        Me.Controls.Add(Me.TextBox238)
        Me.Controls.Add(Me.Label64)
        Me.Controls.Add(Me.Label65)
        Me.Controls.Add(Me.Label66)
        Me.Controls.Add(Me.Label67)
        Me.Controls.Add(Me.Label68)
        Me.Controls.Add(Me.Label69)
        Me.Controls.Add(Me.Label70)
        Me.Controls.Add(Me.Label71)
        Me.Controls.Add(Me.Label72)
        Me.Controls.Add(Me.Label73)
        Me.Controls.Add(Me.Label74)
        Me.Controls.Add(Me.Label75)
        Me.Controls.Add(Me.Label76)
        Me.Controls.Add(Me.TextBox239)
        Me.Controls.Add(Me.Label77)
        Me.Controls.Add(Me.Status_49)
        Me.Controls.Add(Me.RPM_50)
        Me.Controls.Add(Me.RPM_49)
        Me.Controls.Add(Me.Status_48)
        Me.Controls.Add(Me.RPMS_48)
        Me.Controls.Add(Me.AlarmCode_48)
        Me.Controls.Add(Me.RPM_48)
        Me.Controls.Add(Me.Status_47)
        Me.Controls.Add(Me.RPMS_47)
        Me.Controls.Add(Me.AlarmCode_47)
        Me.Controls.Add(Me.RPM_47)
        Me.Controls.Add(Me.Status_46)
        Me.Controls.Add(Me.RPMS_46)
        Me.Controls.Add(Me.AlarmCode_46)
        Me.Controls.Add(Me.RPM_46)
        Me.Controls.Add(Me.Status_45)
        Me.Controls.Add(Me.RPMS_45)
        Me.Controls.Add(Me.AlarmCode_45)
        Me.Controls.Add(Me.RPM_45)
        Me.Controls.Add(Me.Status_44)
        Me.Controls.Add(Me.RPMS_44)
        Me.Controls.Add(Me.AlarmCode_44)
        Me.Controls.Add(Me.RPM_44)
        Me.Controls.Add(Me.Status_43)
        Me.Controls.Add(Me.RPMS_43)
        Me.Controls.Add(Me.AlarmCode_43)
        Me.Controls.Add(Me.RPM_43)
        Me.Controls.Add(Me.Status_42)
        Me.Controls.Add(Me.RPMS_42)
        Me.Controls.Add(Me.AlarmCode_42)
        Me.Controls.Add(Me.RPM_42)
        Me.Controls.Add(Me.Status_41)
        Me.Controls.Add(Me.RPMS_41)
        Me.Controls.Add(Me.AlarmCode_41)
        Me.Controls.Add(Me.RPM_41)
        Me.Controls.Add(Me.Status_40)
        Me.Controls.Add(Me.RPMS_40)
        Me.Controls.Add(Me.AlarmCode_40)
        Me.Controls.Add(Me.RPM_40)
        Me.Controls.Add(Me.Status_39)
        Me.Controls.Add(Me.RPMS_39)
        Me.Controls.Add(Me.AlarmCode_39)
        Me.Controls.Add(Me.RPM_39)
        Me.Controls.Add(Me.Label78)
        Me.Controls.Add(Me.Label79)
        Me.Controls.Add(Me.Status_38)
        Me.Controls.Add(Me.RPMS_38)
        Me.Controls.Add(Me.RPMS_37)
        Me.Controls.Add(Me.AlarmCode_37)
        Me.Controls.Add(Me.Label80)
        Me.Controls.Add(Me.Label81)
        Me.Controls.Add(Me.Label82)
        Me.Controls.Add(Me.Label83)
        Me.Controls.Add(Me.Label84)
        Me.Controls.Add(Me.Label85)
        Me.Controls.Add(Me.Label86)
        Me.Controls.Add(Me.Label87)
        Me.Controls.Add(Me.Label88)
        Me.Controls.Add(Me.Label89)
        Me.Controls.Add(Me.Label90)
        Me.Controls.Add(Me.Label91)
        Me.Controls.Add(Me.Label92)
        Me.Controls.Add(Me.AlarmCode_38)
        Me.Controls.Add(Me.Label93)
        Me.Controls.Add(Me.Status_37)
        Me.Controls.Add(Me.RPM_38)
        Me.Controls.Add(Me.RPM_37)
        Me.Controls.Add(Me.Status_36)
        Me.Controls.Add(Me.RPMS_36)
        Me.Controls.Add(Me.AlarmCode_36)
        Me.Controls.Add(Me.RPM_36)
        Me.Controls.Add(Me.Status_35)
        Me.Controls.Add(Me.RPMS_35)
        Me.Controls.Add(Me.AlarmCode_35)
        Me.Controls.Add(Me.RPM_35)
        Me.Controls.Add(Me.Status_34)
        Me.Controls.Add(Me.RPMS_34)
        Me.Controls.Add(Me.AlarmCode_34)
        Me.Controls.Add(Me.RPM_34)
        Me.Controls.Add(Me.Status_33)
        Me.Controls.Add(Me.RPMS_33)
        Me.Controls.Add(Me.AlarmCode_33)
        Me.Controls.Add(Me.RPM_33)
        Me.Controls.Add(Me.Status_32)
        Me.Controls.Add(Me.RPMS_32)
        Me.Controls.Add(Me.AlarmCode_32)
        Me.Controls.Add(Me.RPM_32)
        Me.Controls.Add(Me.Status_31)
        Me.Controls.Add(Me.RPMS_31)
        Me.Controls.Add(Me.AlarmCode_31)
        Me.Controls.Add(Me.RPM_31)
        Me.Controls.Add(Me.Status_30)
        Me.Controls.Add(Me.RPMS_30)
        Me.Controls.Add(Me.AlarmCode_30)
        Me.Controls.Add(Me.RPM_30)
        Me.Controls.Add(Me.Status_29)
        Me.Controls.Add(Me.RPMS_29)
        Me.Controls.Add(Me.AlarmCode_29)
        Me.Controls.Add(Me.RPM_29)
        Me.Controls.Add(Me.Status_28)
        Me.Controls.Add(Me.RPMS_28)
        Me.Controls.Add(Me.AlarmCode_28)
        Me.Controls.Add(Me.RPM_28)
        Me.Controls.Add(Me.Status_27)
        Me.Controls.Add(Me.RPMS_27)
        Me.Controls.Add(Me.AlarmCode_27)
        Me.Controls.Add(Me.RPM_27)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Status_26)
        Me.Controls.Add(Me.RPMS_26)
        Me.Controls.Add(Me.RPMS_25)
        Me.Controls.Add(Me.AlarmCode_25)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label43)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.AlarmCode_26)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Status_25)
        Me.Controls.Add(Me.RPM_26)
        Me.Controls.Add(Me.RPM_25)
        Me.Controls.Add(Me.Status_24)
        Me.Controls.Add(Me.RPMS_24)
        Me.Controls.Add(Me.AlarmCode_24)
        Me.Controls.Add(Me.RPM_24)
        Me.Controls.Add(Me.Status_23)
        Me.Controls.Add(Me.RPMS_23)
        Me.Controls.Add(Me.AlarmCode_23)
        Me.Controls.Add(Me.RPM_23)
        Me.Controls.Add(Me.Status_22)
        Me.Controls.Add(Me.RPMS_22)
        Me.Controls.Add(Me.AlarmCode_22)
        Me.Controls.Add(Me.RPM_22)
        Me.Controls.Add(Me.Status_21)
        Me.Controls.Add(Me.RPMS_21)
        Me.Controls.Add(Me.AlarmCode_21)
        Me.Controls.Add(Me.RPM_21)
        Me.Controls.Add(Me.Status_20)
        Me.Controls.Add(Me.RPMS_20)
        Me.Controls.Add(Me.AlarmCode_20)
        Me.Controls.Add(Me.RPM_20)
        Me.Controls.Add(Me.Status_19)
        Me.Controls.Add(Me.RPMS_19)
        Me.Controls.Add(Me.AlarmCode_19)
        Me.Controls.Add(Me.RPM_19)
        Me.Controls.Add(Me.Status_18)
        Me.Controls.Add(Me.RPMS_18)
        Me.Controls.Add(Me.AlarmCode_18)
        Me.Controls.Add(Me.RPM_18)
        Me.Controls.Add(Me.Status_17)
        Me.Controls.Add(Me.RPMS_17)
        Me.Controls.Add(Me.AlarmCode_17)
        Me.Controls.Add(Me.RPM_17)
        Me.Controls.Add(Me.Status_16)
        Me.Controls.Add(Me.RPMS_16)
        Me.Controls.Add(Me.AlarmCode_16)
        Me.Controls.Add(Me.RPM_16)
        Me.Controls.Add(Me.Status_15)
        Me.Controls.Add(Me.RPMS_15)
        Me.Controls.Add(Me.AlarmCode_15)
        Me.Controls.Add(Me.RPM_15)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Status_14)
        Me.Controls.Add(Me.RPMS_14)
        Me.Controls.Add(Me.RPMS_13)
        Me.Controls.Add(Me.AlarmCode_13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.AlarmCode_14)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Status_13)
        Me.Controls.Add(Me.RPM_14)
        Me.Controls.Add(Me.RPM_13)
        Me.Controls.Add(Me.Status_12)
        Me.Controls.Add(Me.RPMS_12)
        Me.Controls.Add(Me.AlarmCode_12)
        Me.Controls.Add(Me.RPM_12)
        Me.Controls.Add(Me.Status_11)
        Me.Controls.Add(Me.RPMS_11)
        Me.Controls.Add(Me.AlarmCode_11)
        Me.Controls.Add(Me.RPM_11)
        Me.Controls.Add(Me.Status_10)
        Me.Controls.Add(Me.RPMS_10)
        Me.Controls.Add(Me.AlarmCode_10)
        Me.Controls.Add(Me.RPM_10)
        Me.Controls.Add(Me.Status_9)
        Me.Controls.Add(Me.RPMS_9)
        Me.Controls.Add(Me.AlarmCode_9)
        Me.Controls.Add(Me.RPM_9)
        Me.Controls.Add(Me.Status_8)
        Me.Controls.Add(Me.RPMS_8)
        Me.Controls.Add(Me.AlarmCode_8)
        Me.Controls.Add(Me.RPM_8)
        Me.Controls.Add(Me.Status_7)
        Me.Controls.Add(Me.RPMS_7)
        Me.Controls.Add(Me.AlarmCode_7)
        Me.Controls.Add(Me.RPM_7)
        Me.Controls.Add(Me.Status_6)
        Me.Controls.Add(Me.RPMS_6)
        Me.Controls.Add(Me.AlarmCode_6)
        Me.Controls.Add(Me.RPM_6)
        Me.Controls.Add(Me.Status_5)
        Me.Controls.Add(Me.RPMS_5)
        Me.Controls.Add(Me.AlarmCode_5)
        Me.Controls.Add(Me.RPM_5)
        Me.Controls.Add(Me.Status_4)
        Me.Controls.Add(Me.RPMS_4)
        Me.Controls.Add(Me.AlarmCode_4)
        Me.Controls.Add(Me.RPM_4)
        Me.Controls.Add(Me.Status_3)
        Me.Controls.Add(Me.RPMS_3)
        Me.Controls.Add(Me.AlarmCode_3)
        Me.Controls.Add(Me.RPM_3)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.TextBox9)
        Me.Controls.Add(Me.Status_2)
        Me.Controls.Add(Me.RPMS_2)
        Me.Controls.Add(Me.RPMS_1)
        Me.Controls.Add(Me.AlarmCode_1)
        Me.Controls.Add(Me.ID4)
        Me.Controls.Add(Me.PORT4)
        Me.Controls.Add(Me.COM4)
        Me.Controls.Add(Me.ID3)
        Me.Controls.Add(Me.PORT3)
        Me.Controls.Add(Me.COM3)
        Me.Controls.Add(Me.ID2)
        Me.Controls.Add(Me.PORT2)
        Me.Controls.Add(Me.COM2)
        Me.Controls.Add(Me.ID1)
        Me.Controls.Add(Me.PORT1)
        Me.Controls.Add(Me.COM1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.AlarmCode_2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Status_1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.RPM_2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.RPM_1)
        Me.Controls.Add(Me.Label3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modbus Client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents RPM_2 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents RPM_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_2 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PORT1 As System.Windows.Forms.Label
    Friend WithEvents COM1 As System.Windows.Forms.Label
    Friend WithEvents ID2 As System.Windows.Forms.Label
    Friend WithEvents PORT2 As System.Windows.Forms.Label
    Friend WithEvents COM2 As System.Windows.Forms.Label
    Friend WithEvents ID3 As System.Windows.Forms.Label
    Friend WithEvents PORT3 As System.Windows.Forms.Label
    Friend WithEvents COM3 As System.Windows.Forms.Label
    Friend WithEvents ID4 As System.Windows.Forms.Label
    Friend WithEvents PORT4 As System.Windows.Forms.Label
    Friend WithEvents COM4 As System.Windows.Forms.Label
    Friend WithEvents Status_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RPMS_1 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_1 As System.Windows.Forms.TextBox
    Friend WithEvents Status_2 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Status_3 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_3 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_3 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_3 As System.Windows.Forms.TextBox
    Friend WithEvents ID1 As System.Windows.Forms.Label
    Friend WithEvents Status_4 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_4 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_4 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_4 As System.Windows.Forms.TextBox
    Friend WithEvents Status_5 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_5 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_5 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_5 As System.Windows.Forms.TextBox
    Friend WithEvents Status_6 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_6 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_6 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_6 As System.Windows.Forms.TextBox
    Friend WithEvents Status_7 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_7 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_7 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_7 As System.Windows.Forms.TextBox
    Friend WithEvents Status_8 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_8 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_8 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_8 As System.Windows.Forms.TextBox
    Friend WithEvents Status_9 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_9 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_9 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_9 As System.Windows.Forms.TextBox
    Friend WithEvents Status_10 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_10 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_10 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_10 As System.Windows.Forms.TextBox
    Friend WithEvents Status_12 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_12 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_12 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_12 As System.Windows.Forms.TextBox
    Friend WithEvents Status_11 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_11 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_11 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_11 As System.Windows.Forms.TextBox
    Friend WithEvents Status_24 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_24 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_24 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_24 As System.Windows.Forms.TextBox
    Friend WithEvents Status_23 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_23 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_23 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_23 As System.Windows.Forms.TextBox
    Friend WithEvents Status_22 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_22 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_22 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_22 As System.Windows.Forms.TextBox
    Friend WithEvents Status_21 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_21 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_21 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_21 As System.Windows.Forms.TextBox
    Friend WithEvents Status_20 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_20 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_20 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_20 As System.Windows.Forms.TextBox
    Friend WithEvents Status_19 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_19 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_19 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_19 As System.Windows.Forms.TextBox
    Friend WithEvents Status_18 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_18 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_18 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_18 As System.Windows.Forms.TextBox
    Friend WithEvents Status_17 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_17 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_17 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_17 As System.Windows.Forms.TextBox
    Friend WithEvents Status_16 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_16 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_16 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_16 As System.Windows.Forms.TextBox
    Friend WithEvents Status_15 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_15 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_15 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_15 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Status_14 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_14 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_13 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_13 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_14 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Status_13 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_14 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_13 As System.Windows.Forms.TextBox
    Friend WithEvents Status_36 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_36 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_36 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_36 As System.Windows.Forms.TextBox
    Friend WithEvents Status_35 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_35 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_35 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_35 As System.Windows.Forms.TextBox
    Friend WithEvents Status_34 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_34 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_34 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_34 As System.Windows.Forms.TextBox
    Friend WithEvents Status_33 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_33 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_33 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_33 As System.Windows.Forms.TextBox
    Friend WithEvents Status_32 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_32 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_32 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_32 As System.Windows.Forms.TextBox
    Friend WithEvents Status_31 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_31 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_31 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_31 As System.Windows.Forms.TextBox
    Friend WithEvents Status_30 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_30 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_30 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_30 As System.Windows.Forms.TextBox
    Friend WithEvents Status_29 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_29 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_29 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_29 As System.Windows.Forms.TextBox
    Friend WithEvents Status_28 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_28 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_28 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_28 As System.Windows.Forms.TextBox
    Friend WithEvents Status_27 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_27 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_27 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_27 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Status_26 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_26 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_25 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_25 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_26 As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Status_25 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_26 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_25 As System.Windows.Forms.TextBox
    Friend WithEvents Status_72 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_72 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox149 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_72 As System.Windows.Forms.TextBox
    Friend WithEvents Status_71 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_71 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox153 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_71 As System.Windows.Forms.TextBox
    Friend WithEvents Status_70 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_70 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox157 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_70 As System.Windows.Forms.TextBox
    Friend WithEvents Status_69 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_69 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox161 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_69 As System.Windows.Forms.TextBox
    Friend WithEvents Status_68 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_68 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox165 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_68 As System.Windows.Forms.TextBox
    Friend WithEvents Status_67 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_67 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox169 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_67 As System.Windows.Forms.TextBox
    Friend WithEvents Status_66 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_66 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox173 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_66 As System.Windows.Forms.TextBox
    Friend WithEvents Status_65 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_65 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox177 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_65 As System.Windows.Forms.TextBox
    Friend WithEvents Status_64 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_64 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_64 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_64 As System.Windows.Forms.TextBox
    Friend WithEvents Status_63 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_63 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox185 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_63 As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Status_62 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_62 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_61 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox190 As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents TextBox191 As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Status_61 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_62 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_61 As System.Windows.Forms.TextBox
    Friend WithEvents Status_60 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_60 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox197 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_60 As System.Windows.Forms.TextBox
    Friend WithEvents Status_59 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_59 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox201 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_59 As System.Windows.Forms.TextBox
    Friend WithEvents Status_58 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_58 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox205 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_58 As System.Windows.Forms.TextBox
    Friend WithEvents Status_57 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_57 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox209 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_57 As System.Windows.Forms.TextBox
    Friend WithEvents Status_56 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_56 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox213 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_56 As System.Windows.Forms.TextBox
    Friend WithEvents Status_55 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_55 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox217 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_55 As System.Windows.Forms.TextBox
    Friend WithEvents Status_54 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_54 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox221 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_54 As System.Windows.Forms.TextBox
    Friend WithEvents Status_53 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_53 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox225 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_53 As System.Windows.Forms.TextBox
    Friend WithEvents Status_52 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_52 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox229 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_52 As System.Windows.Forms.TextBox
    Friend WithEvents Status_51 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_51 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox233 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_51 As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Status_50 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_50 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_49 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox238 As System.Windows.Forms.TextBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents TextBox239 As System.Windows.Forms.TextBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Status_49 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_50 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_49 As System.Windows.Forms.TextBox
    Friend WithEvents Status_48 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_48 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_48 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_48 As System.Windows.Forms.TextBox
    Friend WithEvents Status_47 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_47 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_47 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_47 As System.Windows.Forms.TextBox
    Friend WithEvents Status_46 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_46 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_46 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_46 As System.Windows.Forms.TextBox
    Friend WithEvents Status_45 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_45 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_45 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_45 As System.Windows.Forms.TextBox
    Friend WithEvents Status_44 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_44 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_44 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_44 As System.Windows.Forms.TextBox
    Friend WithEvents Status_43 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_43 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_43 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_43 As System.Windows.Forms.TextBox
    Friend WithEvents Status_42 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_42 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_42 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_42 As System.Windows.Forms.TextBox
    Friend WithEvents Status_41 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_41 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_41 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_41 As System.Windows.Forms.TextBox
    Friend WithEvents Status_40 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_40 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_40 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_40 As System.Windows.Forms.TextBox
    Friend WithEvents Status_39 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_39 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_39 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_39 As System.Windows.Forms.TextBox
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Status_38 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_38 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_37 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_37 As System.Windows.Forms.TextBox
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_38 As System.Windows.Forms.TextBox
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Status_37 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_38 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_37 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button

End Class
