﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms

Public Class Form3

    Private Sub Form3_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ''MessageBox("關閉")
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            Application.Run()
        End If
    End Sub

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SerialPort3 = Form1.SerialPort1
    End Sub

    Public Sub getdata(sp As SerialPort, id As Byte, channel As Byte, ByRef value As Short, ByRef status As Short) 'Get Device Data
        'Function 3 (讀取AI) data request is always 8 bytes:
        Dim byttrs(7) As Byte

        'Function 3 (讀取AI) response buffer:  一次只要一個資料所以長度為 (5 + 2 * 比數)-1 -> (5 + 2*1)-1 = 6
        Dim response As Byte() = New Byte((5 + 2 * 1) - 1) {}

        '開啟Serialport & Clear out/in buffers
        'If sp.IsOpen = False Then
        '    sp.Open()
        '    sp.DiscardOutBuffer()                                                        'Clear out buffers
        '    sp.DiscardInBuffer()                                                         'Clear in buffers
        'End If

        Dim lonCRC As Long
        Dim intCnt As Integer
        Dim intBit As Integer
        Dim intTemp As Integer

        byttrs(0) = id                                                                   'ID
        byttrs(1) = 3                                                                    '讀取 word (AI)  tm-AD5C 設定為"4",一般MODBUS為"3""  
        byttrs(2) = 0                                                                    'Starting Address Hi"
        byttrs(3) = channel                                                              'Starting Address Lo"
        byttrs(4) = 0                                                                    'No. of Points Hi"
        byttrs(5) = 1                                                                    'No. of Points Lo
        ''byttrs(1) 讀取4xxxx的位置 FunctionCode為3, tm-ad5c需設定成"4"
        ''byttrs(2)&byttrs(3)為讀取位置起始值, tm-ad5c 只有五個channel 所以只要byttrs(4)調整0~4即可

        '計算CRC MODBUS通訊需要的代碼
        lonCRC = &HFFFF&
        For intCnt = 0 To 5
            lonCRC = lonCRC Xor byttrs(intCnt)
            For intBit = 0 To 7
                intTemp = lonCRC Mod 2
                lonCRC = lonCRC \ 2
                If intTemp = 1 Then
                    lonCRC = lonCRC Xor &HA001&
                End If
            Next intBit
        Next intCnt
        byttrs(6) = lonCRC Mod 256
        byttrs(7) = lonCRC \ 256

        sp.Write(byttrs, 0, byttrs.Length)                                               '送出命令字串

        Dim i As Integer = 0
        Thread.Sleep(50)
        While (sp.BytesToRead <> 0)                                                      '解析設備回覆的字串
            response(i) = Byte.Parse(sp.ReadByte())
            i += 1
        End While
        Dim sc As Integer
        sc = 0                                                                        '初始值設為0
        sc = (value << 8) + response(5)                                               '把解析出的byte除以除以 2 的 8 次方
        sc = (value << 8) + response(6)                                               '把解析出的byte除以除以 2 的 8 次方
        If sc = 0 Then
            status = 0
        End If
        value = 0                                                                        '初始值設為0
        value = (value << 8) + response(3)                                               '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + response(4)                                               '把解析出的byte除以除以 2 的 8 次方
        'sp.Close()
    End Sub

    Public Sub Tdata(RH As Integer, RL As Integer, EH As Short, EL As Short, ByRef value As Short) 'Get Device Data
        value = (value - RL) * (EH - EL) / (RH - RL) + EL
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim value As Short
        Dim status As Short
        Dim tStatus As TextBox
        'Dim tRPMS As TextBox
        'Dim tRPM As TextBox
        'Dim tAlarmCode As TextBox
        Dim tid As Label

        '開啟Serialport & Clear out/in buffers
        If SerialPort3.IsOpen = False Then
            SerialPort3.Open()
            SerialPort3.DiscardOutBuffer()                                                        'Clear out buffers
            SerialPort3.DiscardInBuffer()                                                         'Clear in buffers
        End If

        TextBox1.Text = "掃描中"
        TextBox1.BackColor = Color.YellowGreen
        For index = 1 To 72
            status = 1
            tStatus = Me.Controls("Status_" & (index).ToString)
            tid = Me.Controls("ID_" & (index).ToString)
            tid.ForeColor = Color.Black
            getdata(SerialPort3, index, 0, value, status)
            If status <> 0 Then
            Else
                getdata(SerialPort3, index, 0, value, status)
                If status <> 0 Then
                Else
                    tid.ForeColor = Color.Red
                End If
            End If
        Next

        TextBox1.Text = "掃描結束"
        TextBox1.BackColor = Color.White

        '關閉Serialport
        SerialPort3.Close()
    End Sub
End Class
