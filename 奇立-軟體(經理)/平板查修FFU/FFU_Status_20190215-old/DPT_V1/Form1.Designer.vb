﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.RPM_2 = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RPM_1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.AlarmCode_2 = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.ID_2 = New System.Windows.Forms.Label()
        Me.ID_1 = New System.Windows.Forms.Label()
        Me.ID_6 = New System.Windows.Forms.Label()
        Me.ID_5 = New System.Windows.Forms.Label()
        Me.ID_4 = New System.Windows.Forms.Label()
        Me.ID_9 = New System.Windows.Forms.Label()
        Me.ID_8 = New System.Windows.Forms.Label()
        Me.ID_7 = New System.Windows.Forms.Label()
        Me.ID_12 = New System.Windows.Forms.Label()
        Me.ID_11 = New System.Windows.Forms.Label()
        Me.ID_10 = New System.Windows.Forms.Label()
        Me.Status_1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RPMS_1 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_1 = New System.Windows.Forms.TextBox()
        Me.Status_2 = New System.Windows.Forms.TextBox()
        Me.RPMS_2 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Status_3 = New System.Windows.Forms.TextBox()
        Me.RPMS_3 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_3 = New System.Windows.Forms.TextBox()
        Me.RPM_3 = New System.Windows.Forms.TextBox()
        Me.ID_3 = New System.Windows.Forms.Label()
        Me.Status_4 = New System.Windows.Forms.TextBox()
        Me.RPMS_4 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_4 = New System.Windows.Forms.TextBox()
        Me.RPM_4 = New System.Windows.Forms.TextBox()
        Me.Status_5 = New System.Windows.Forms.TextBox()
        Me.RPMS_5 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_5 = New System.Windows.Forms.TextBox()
        Me.RPM_5 = New System.Windows.Forms.TextBox()
        Me.Status_6 = New System.Windows.Forms.TextBox()
        Me.RPMS_6 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_6 = New System.Windows.Forms.TextBox()
        Me.RPM_6 = New System.Windows.Forms.TextBox()
        Me.Status_7 = New System.Windows.Forms.TextBox()
        Me.RPMS_7 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_7 = New System.Windows.Forms.TextBox()
        Me.RPM_7 = New System.Windows.Forms.TextBox()
        Me.Status_8 = New System.Windows.Forms.TextBox()
        Me.RPMS_8 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_8 = New System.Windows.Forms.TextBox()
        Me.RPM_8 = New System.Windows.Forms.TextBox()
        Me.Status_9 = New System.Windows.Forms.TextBox()
        Me.RPMS_9 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_9 = New System.Windows.Forms.TextBox()
        Me.RPM_9 = New System.Windows.Forms.TextBox()
        Me.Status_10 = New System.Windows.Forms.TextBox()
        Me.RPMS_10 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_10 = New System.Windows.Forms.TextBox()
        Me.RPM_10 = New System.Windows.Forms.TextBox()
        Me.Status_12 = New System.Windows.Forms.TextBox()
        Me.RPMS_12 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_12 = New System.Windows.Forms.TextBox()
        Me.RPM_12 = New System.Windows.Forms.TextBox()
        Me.Status_11 = New System.Windows.Forms.TextBox()
        Me.RPMS_11 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_11 = New System.Windows.Forms.TextBox()
        Me.RPM_11 = New System.Windows.Forms.TextBox()
        Me.Status_24 = New System.Windows.Forms.TextBox()
        Me.RPMS_24 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_24 = New System.Windows.Forms.TextBox()
        Me.RPM_24 = New System.Windows.Forms.TextBox()
        Me.Status_23 = New System.Windows.Forms.TextBox()
        Me.RPMS_23 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_23 = New System.Windows.Forms.TextBox()
        Me.RPM_23 = New System.Windows.Forms.TextBox()
        Me.Status_22 = New System.Windows.Forms.TextBox()
        Me.RPMS_22 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_22 = New System.Windows.Forms.TextBox()
        Me.RPM_22 = New System.Windows.Forms.TextBox()
        Me.Status_21 = New System.Windows.Forms.TextBox()
        Me.RPMS_21 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_21 = New System.Windows.Forms.TextBox()
        Me.RPM_21 = New System.Windows.Forms.TextBox()
        Me.Status_20 = New System.Windows.Forms.TextBox()
        Me.RPMS_20 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_20 = New System.Windows.Forms.TextBox()
        Me.RPM_20 = New System.Windows.Forms.TextBox()
        Me.Status_19 = New System.Windows.Forms.TextBox()
        Me.RPMS_19 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_19 = New System.Windows.Forms.TextBox()
        Me.RPM_19 = New System.Windows.Forms.TextBox()
        Me.Status_18 = New System.Windows.Forms.TextBox()
        Me.RPMS_18 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_18 = New System.Windows.Forms.TextBox()
        Me.RPM_18 = New System.Windows.Forms.TextBox()
        Me.Status_17 = New System.Windows.Forms.TextBox()
        Me.RPMS_17 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_17 = New System.Windows.Forms.TextBox()
        Me.RPM_17 = New System.Windows.Forms.TextBox()
        Me.Status_16 = New System.Windows.Forms.TextBox()
        Me.RPMS_16 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_16 = New System.Windows.Forms.TextBox()
        Me.RPM_16 = New System.Windows.Forms.TextBox()
        Me.Status_15 = New System.Windows.Forms.TextBox()
        Me.RPMS_15 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_15 = New System.Windows.Forms.TextBox()
        Me.RPM_15 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Status_14 = New System.Windows.Forms.TextBox()
        Me.RPMS_14 = New System.Windows.Forms.TextBox()
        Me.RPMS_13 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_13 = New System.Windows.Forms.TextBox()
        Me.ID_24 = New System.Windows.Forms.Label()
        Me.ID_23 = New System.Windows.Forms.Label()
        Me.ID_22 = New System.Windows.Forms.Label()
        Me.ID_21 = New System.Windows.Forms.Label()
        Me.ID_20 = New System.Windows.Forms.Label()
        Me.ID_19 = New System.Windows.Forms.Label()
        Me.ID_18 = New System.Windows.Forms.Label()
        Me.ID_17 = New System.Windows.Forms.Label()
        Me.ID_16 = New System.Windows.Forms.Label()
        Me.ID_15 = New System.Windows.Forms.Label()
        Me.ID_14 = New System.Windows.Forms.Label()
        Me.ID_13 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.AlarmCode_14 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Status_13 = New System.Windows.Forms.TextBox()
        Me.RPM_14 = New System.Windows.Forms.TextBox()
        Me.RPM_13 = New System.Windows.Forms.TextBox()
        Me.Status_36 = New System.Windows.Forms.TextBox()
        Me.RPMS_36 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_36 = New System.Windows.Forms.TextBox()
        Me.RPM_36 = New System.Windows.Forms.TextBox()
        Me.Status_35 = New System.Windows.Forms.TextBox()
        Me.RPMS_35 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_35 = New System.Windows.Forms.TextBox()
        Me.RPM_35 = New System.Windows.Forms.TextBox()
        Me.Status_34 = New System.Windows.Forms.TextBox()
        Me.RPMS_34 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_34 = New System.Windows.Forms.TextBox()
        Me.RPM_34 = New System.Windows.Forms.TextBox()
        Me.Status_33 = New System.Windows.Forms.TextBox()
        Me.RPMS_33 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_33 = New System.Windows.Forms.TextBox()
        Me.RPM_33 = New System.Windows.Forms.TextBox()
        Me.Status_32 = New System.Windows.Forms.TextBox()
        Me.RPMS_32 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_32 = New System.Windows.Forms.TextBox()
        Me.RPM_32 = New System.Windows.Forms.TextBox()
        Me.Status_31 = New System.Windows.Forms.TextBox()
        Me.RPMS_31 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_31 = New System.Windows.Forms.TextBox()
        Me.RPM_31 = New System.Windows.Forms.TextBox()
        Me.Status_30 = New System.Windows.Forms.TextBox()
        Me.RPMS_30 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_30 = New System.Windows.Forms.TextBox()
        Me.RPM_30 = New System.Windows.Forms.TextBox()
        Me.Status_29 = New System.Windows.Forms.TextBox()
        Me.RPMS_29 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_29 = New System.Windows.Forms.TextBox()
        Me.RPM_29 = New System.Windows.Forms.TextBox()
        Me.Status_28 = New System.Windows.Forms.TextBox()
        Me.RPMS_28 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_28 = New System.Windows.Forms.TextBox()
        Me.RPM_28 = New System.Windows.Forms.TextBox()
        Me.Status_27 = New System.Windows.Forms.TextBox()
        Me.RPMS_27 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_27 = New System.Windows.Forms.TextBox()
        Me.RPM_27 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Status_26 = New System.Windows.Forms.TextBox()
        Me.RPMS_26 = New System.Windows.Forms.TextBox()
        Me.RPMS_25 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_25 = New System.Windows.Forms.TextBox()
        Me.ID_36 = New System.Windows.Forms.Label()
        Me.ID_35 = New System.Windows.Forms.Label()
        Me.ID_34 = New System.Windows.Forms.Label()
        Me.ID_33 = New System.Windows.Forms.Label()
        Me.ID_32 = New System.Windows.Forms.Label()
        Me.ID_31 = New System.Windows.Forms.Label()
        Me.ID_30 = New System.Windows.Forms.Label()
        Me.ID_29 = New System.Windows.Forms.Label()
        Me.ID_28 = New System.Windows.Forms.Label()
        Me.ID_27 = New System.Windows.Forms.Label()
        Me.ID_26 = New System.Windows.Forms.Label()
        Me.ID_25 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.AlarmCode_26 = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Status_25 = New System.Windows.Forms.TextBox()
        Me.RPM_26 = New System.Windows.Forms.TextBox()
        Me.RPM_25 = New System.Windows.Forms.TextBox()
        Me.Status_72 = New System.Windows.Forms.TextBox()
        Me.RPMS_72 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_72 = New System.Windows.Forms.TextBox()
        Me.RPM_72 = New System.Windows.Forms.TextBox()
        Me.Status_71 = New System.Windows.Forms.TextBox()
        Me.RPMS_71 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_71 = New System.Windows.Forms.TextBox()
        Me.RPM_71 = New System.Windows.Forms.TextBox()
        Me.Status_70 = New System.Windows.Forms.TextBox()
        Me.RPMS_70 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_70 = New System.Windows.Forms.TextBox()
        Me.RPM_70 = New System.Windows.Forms.TextBox()
        Me.Status_69 = New System.Windows.Forms.TextBox()
        Me.RPMS_69 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_69 = New System.Windows.Forms.TextBox()
        Me.RPM_69 = New System.Windows.Forms.TextBox()
        Me.Status_68 = New System.Windows.Forms.TextBox()
        Me.RPMS_68 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_68 = New System.Windows.Forms.TextBox()
        Me.RPM_68 = New System.Windows.Forms.TextBox()
        Me.Status_67 = New System.Windows.Forms.TextBox()
        Me.RPMS_67 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_67 = New System.Windows.Forms.TextBox()
        Me.RPM_67 = New System.Windows.Forms.TextBox()
        Me.Status_66 = New System.Windows.Forms.TextBox()
        Me.RPMS_66 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_66 = New System.Windows.Forms.TextBox()
        Me.RPM_66 = New System.Windows.Forms.TextBox()
        Me.Status_65 = New System.Windows.Forms.TextBox()
        Me.RPMS_65 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_65 = New System.Windows.Forms.TextBox()
        Me.RPM_65 = New System.Windows.Forms.TextBox()
        Me.Status_64 = New System.Windows.Forms.TextBox()
        Me.RPMS_64 = New System.Windows.Forms.TextBox()
        Me.RPM_64 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_64 = New System.Windows.Forms.TextBox()
        Me.Status_63 = New System.Windows.Forms.TextBox()
        Me.RPMS_63 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_63 = New System.Windows.Forms.TextBox()
        Me.RPM_63 = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Status_62 = New System.Windows.Forms.TextBox()
        Me.RPMS_62 = New System.Windows.Forms.TextBox()
        Me.RPMS_61 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_61 = New System.Windows.Forms.TextBox()
        Me.ID_72 = New System.Windows.Forms.Label()
        Me.ID_71 = New System.Windows.Forms.Label()
        Me.ID_70 = New System.Windows.Forms.Label()
        Me.ID_69 = New System.Windows.Forms.Label()
        Me.ID_68 = New System.Windows.Forms.Label()
        Me.ID_67 = New System.Windows.Forms.Label()
        Me.ID_66 = New System.Windows.Forms.Label()
        Me.ID_65 = New System.Windows.Forms.Label()
        Me.ID_64 = New System.Windows.Forms.Label()
        Me.ID_63 = New System.Windows.Forms.Label()
        Me.ID_62 = New System.Windows.Forms.Label()
        Me.ID_61 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.AlarmCode_62 = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Status_61 = New System.Windows.Forms.TextBox()
        Me.RPM_62 = New System.Windows.Forms.TextBox()
        Me.RPM_61 = New System.Windows.Forms.TextBox()
        Me.Status_60 = New System.Windows.Forms.TextBox()
        Me.RPMS_60 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_60 = New System.Windows.Forms.TextBox()
        Me.RPM_60 = New System.Windows.Forms.TextBox()
        Me.Status_59 = New System.Windows.Forms.TextBox()
        Me.RPMS_59 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_59 = New System.Windows.Forms.TextBox()
        Me.RPM_59 = New System.Windows.Forms.TextBox()
        Me.Status_58 = New System.Windows.Forms.TextBox()
        Me.RPMS_58 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_58 = New System.Windows.Forms.TextBox()
        Me.RPM_58 = New System.Windows.Forms.TextBox()
        Me.Status_57 = New System.Windows.Forms.TextBox()
        Me.RPMS_57 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_57 = New System.Windows.Forms.TextBox()
        Me.RPM_57 = New System.Windows.Forms.TextBox()
        Me.Status_56 = New System.Windows.Forms.TextBox()
        Me.RPMS_56 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_56 = New System.Windows.Forms.TextBox()
        Me.RPM_56 = New System.Windows.Forms.TextBox()
        Me.Status_55 = New System.Windows.Forms.TextBox()
        Me.RPMS_55 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_55 = New System.Windows.Forms.TextBox()
        Me.RPM_55 = New System.Windows.Forms.TextBox()
        Me.Status_54 = New System.Windows.Forms.TextBox()
        Me.RPMS_54 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_54 = New System.Windows.Forms.TextBox()
        Me.RPM_54 = New System.Windows.Forms.TextBox()
        Me.Status_53 = New System.Windows.Forms.TextBox()
        Me.RPMS_53 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_53 = New System.Windows.Forms.TextBox()
        Me.RPM_53 = New System.Windows.Forms.TextBox()
        Me.Status_52 = New System.Windows.Forms.TextBox()
        Me.RPMS_52 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_52 = New System.Windows.Forms.TextBox()
        Me.RPM_52 = New System.Windows.Forms.TextBox()
        Me.Status_51 = New System.Windows.Forms.TextBox()
        Me.RPMS_51 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_51 = New System.Windows.Forms.TextBox()
        Me.RPM_51 = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Status_50 = New System.Windows.Forms.TextBox()
        Me.RPMS_50 = New System.Windows.Forms.TextBox()
        Me.RPMS_49 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_49 = New System.Windows.Forms.TextBox()
        Me.ID_60 = New System.Windows.Forms.Label()
        Me.ID_59 = New System.Windows.Forms.Label()
        Me.ID_58 = New System.Windows.Forms.Label()
        Me.ID_57 = New System.Windows.Forms.Label()
        Me.ID_56 = New System.Windows.Forms.Label()
        Me.ID_55 = New System.Windows.Forms.Label()
        Me.ID_54 = New System.Windows.Forms.Label()
        Me.ID_53 = New System.Windows.Forms.Label()
        Me.ID_52 = New System.Windows.Forms.Label()
        Me.ID_51 = New System.Windows.Forms.Label()
        Me.ID_50 = New System.Windows.Forms.Label()
        Me.ID_49 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.AlarmCode_50 = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Status_49 = New System.Windows.Forms.TextBox()
        Me.RPM_50 = New System.Windows.Forms.TextBox()
        Me.RPM_49 = New System.Windows.Forms.TextBox()
        Me.Status_48 = New System.Windows.Forms.TextBox()
        Me.RPMS_48 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_48 = New System.Windows.Forms.TextBox()
        Me.RPM_48 = New System.Windows.Forms.TextBox()
        Me.Status_47 = New System.Windows.Forms.TextBox()
        Me.RPMS_47 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_47 = New System.Windows.Forms.TextBox()
        Me.RPM_47 = New System.Windows.Forms.TextBox()
        Me.Status_46 = New System.Windows.Forms.TextBox()
        Me.RPMS_46 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_46 = New System.Windows.Forms.TextBox()
        Me.RPM_46 = New System.Windows.Forms.TextBox()
        Me.Status_45 = New System.Windows.Forms.TextBox()
        Me.RPMS_45 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_45 = New System.Windows.Forms.TextBox()
        Me.RPM_45 = New System.Windows.Forms.TextBox()
        Me.Status_44 = New System.Windows.Forms.TextBox()
        Me.RPMS_44 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_44 = New System.Windows.Forms.TextBox()
        Me.RPM_44 = New System.Windows.Forms.TextBox()
        Me.Status_43 = New System.Windows.Forms.TextBox()
        Me.RPMS_43 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_43 = New System.Windows.Forms.TextBox()
        Me.RPM_43 = New System.Windows.Forms.TextBox()
        Me.Status_42 = New System.Windows.Forms.TextBox()
        Me.RPMS_42 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_42 = New System.Windows.Forms.TextBox()
        Me.RPM_42 = New System.Windows.Forms.TextBox()
        Me.Status_41 = New System.Windows.Forms.TextBox()
        Me.RPMS_41 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_41 = New System.Windows.Forms.TextBox()
        Me.RPM_41 = New System.Windows.Forms.TextBox()
        Me.Status_40 = New System.Windows.Forms.TextBox()
        Me.RPMS_40 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_40 = New System.Windows.Forms.TextBox()
        Me.RPM_40 = New System.Windows.Forms.TextBox()
        Me.Status_39 = New System.Windows.Forms.TextBox()
        Me.RPMS_39 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_39 = New System.Windows.Forms.TextBox()
        Me.RPM_39 = New System.Windows.Forms.TextBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Status_38 = New System.Windows.Forms.TextBox()
        Me.RPMS_38 = New System.Windows.Forms.TextBox()
        Me.RPMS_37 = New System.Windows.Forms.TextBox()
        Me.AlarmCode_37 = New System.Windows.Forms.TextBox()
        Me.ID_48 = New System.Windows.Forms.Label()
        Me.ID_47 = New System.Windows.Forms.Label()
        Me.ID_46 = New System.Windows.Forms.Label()
        Me.ID_45 = New System.Windows.Forms.Label()
        Me.ID_44 = New System.Windows.Forms.Label()
        Me.ID_43 = New System.Windows.Forms.Label()
        Me.ID_42 = New System.Windows.Forms.Label()
        Me.ID_41 = New System.Windows.Forms.Label()
        Me.ID_40 = New System.Windows.Forms.Label()
        Me.ID_39 = New System.Windows.Forms.Label()
        Me.ID_38 = New System.Windows.Forms.Label()
        Me.ID_37 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.AlarmCode_38 = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Status_37 = New System.Windows.Forms.TextBox()
        Me.RPM_38 = New System.Windows.Forms.TextBox()
        Me.RPM_37 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 270)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 12)
        Me.Label14.TabIndex = 274
        Me.Label14.Text = "Update Status"
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(98, 202)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(101, 23)
        Me.Button19.TabIndex = 273
        Me.Button19.Text = "設定Set"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(97, 176)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox4.TabIndex = 270
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(97, 149)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox3.TabIndex = 269
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(97, 122)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox2.TabIndex = 268
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(98, 66)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox1.TabIndex = 267
        '
        'RPM_2
        '
        Me.RPM_2.Location = New System.Drawing.Point(422, 60)
        Me.RPM_2.Name = "RPM_2"
        Me.RPM_2.Size = New System.Drawing.Size(55, 22)
        Me.RPM_2.TabIndex = 265
        Me.RPM_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(98, 265)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(99, 23)
        Me.Button3.TabIndex = 264
        Me.Button3.Text = "Manual Update"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(44, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 12)
        Me.Label7.TabIndex = 263
        Me.Label7.Text = "Stop Bits"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(47, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 12)
        Me.Label6.TabIndex = 262
        Me.Label6.Text = "DataBits"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(59, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 12)
        Me.Label5.TabIndex = 261
        Me.Label5.Text = "Parity"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 12)
        Me.Label4.TabIndex = 260
        Me.Label4.Text = "BaudRate"
        '
        'RPM_1
        '
        Me.RPM_1.Location = New System.Drawing.Point(422, 32)
        Me.RPM_1.Name = "RPM_1"
        Me.RPM_1.Size = New System.Drawing.Size(55, 22)
        Me.RPM_1.TabIndex = 259
        Me.RPM_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 12)
        Me.Label3.TabIndex = 258
        Me.Label3.Text = "Select COM port"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label9.Location = New System.Drawing.Point(126, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 16)
        Me.Label9.TabIndex = 276
        Me.Label9.Text = "Label9"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 16)
        Me.Label8.TabIndex = 275
        Me.Label8.Text = "Date  / Time"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(352, 13)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(46, 12)
        Me.Label30.TabIndex = 280
        Me.Label30.Text = "RPM Set"
        '
        'AlarmCode_2
        '
        Me.AlarmCode_2.Location = New System.Drawing.Point(501, 60)
        Me.AlarmCode_2.Name = "AlarmCode_2"
        Me.AlarmCode_2.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_2.TabIndex = 279
        Me.AlarmCode_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'ID_2
        '
        Me.ID_2.AutoSize = True
        Me.ID_2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_2.Location = New System.Drawing.Point(223, 69)
        Me.ID_2.Name = "ID_2"
        Me.ID_2.Size = New System.Drawing.Size(35, 13)
        Me.ID_2.TabIndex = 287
        Me.ID_2.Text = "ID:02"
        '
        'ID_1
        '
        Me.ID_1.AutoSize = True
        Me.ID_1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_1.Location = New System.Drawing.Point(223, 41)
        Me.ID_1.Name = "ID_1"
        Me.ID_1.Size = New System.Drawing.Size(35, 13)
        Me.ID_1.TabIndex = 286
        Me.ID_1.Text = "ID:01"
        '
        'ID_6
        '
        Me.ID_6.AutoSize = True
        Me.ID_6.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_6.Location = New System.Drawing.Point(223, 181)
        Me.ID_6.Name = "ID_6"
        Me.ID_6.Size = New System.Drawing.Size(35, 13)
        Me.ID_6.TabIndex = 291
        Me.ID_6.Text = "ID:06"
        '
        'ID_5
        '
        Me.ID_5.AutoSize = True
        Me.ID_5.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_5.Location = New System.Drawing.Point(223, 153)
        Me.ID_5.Name = "ID_5"
        Me.ID_5.Size = New System.Drawing.Size(35, 13)
        Me.ID_5.TabIndex = 290
        Me.ID_5.Text = "ID:05"
        '
        'ID_4
        '
        Me.ID_4.AutoSize = True
        Me.ID_4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_4.Location = New System.Drawing.Point(223, 125)
        Me.ID_4.Name = "ID_4"
        Me.ID_4.Size = New System.Drawing.Size(35, 13)
        Me.ID_4.TabIndex = 289
        Me.ID_4.Text = "ID:04"
        '
        'ID_9
        '
        Me.ID_9.AutoSize = True
        Me.ID_9.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_9.Location = New System.Drawing.Point(223, 265)
        Me.ID_9.Name = "ID_9"
        Me.ID_9.Size = New System.Drawing.Size(35, 13)
        Me.ID_9.TabIndex = 294
        Me.ID_9.Text = "ID:09"
        '
        'ID_8
        '
        Me.ID_8.AutoSize = True
        Me.ID_8.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_8.Location = New System.Drawing.Point(223, 237)
        Me.ID_8.Name = "ID_8"
        Me.ID_8.Size = New System.Drawing.Size(35, 13)
        Me.ID_8.TabIndex = 293
        Me.ID_8.Text = "ID:08"
        '
        'ID_7
        '
        Me.ID_7.AutoSize = True
        Me.ID_7.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_7.Location = New System.Drawing.Point(223, 209)
        Me.ID_7.Name = "ID_7"
        Me.ID_7.Size = New System.Drawing.Size(35, 13)
        Me.ID_7.TabIndex = 292
        Me.ID_7.Text = "ID:07"
        '
        'ID_12
        '
        Me.ID_12.AutoSize = True
        Me.ID_12.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_12.Location = New System.Drawing.Point(223, 349)
        Me.ID_12.Name = "ID_12"
        Me.ID_12.Size = New System.Drawing.Size(35, 13)
        Me.ID_12.TabIndex = 297
        Me.ID_12.Text = "ID:12"
        '
        'ID_11
        '
        Me.ID_11.AutoSize = True
        Me.ID_11.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_11.Location = New System.Drawing.Point(223, 321)
        Me.ID_11.Name = "ID_11"
        Me.ID_11.Size = New System.Drawing.Size(35, 13)
        Me.ID_11.TabIndex = 296
        Me.ID_11.Text = "ID:11"
        '
        'ID_10
        '
        Me.ID_10.AutoSize = True
        Me.ID_10.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_10.Location = New System.Drawing.Point(223, 293)
        Me.ID_10.Name = "ID_10"
        Me.ID_10.Size = New System.Drawing.Size(35, 13)
        Me.ID_10.TabIndex = 295
        Me.ID_10.Text = "ID:10"
        '
        'Status_1
        '
        Me.Status_1.Location = New System.Drawing.Point(264, 32)
        Me.Status_1.Name = "Status_1"
        Me.Status_1.Size = New System.Drawing.Size(55, 22)
        Me.Status_1.TabIndex = 277
        Me.Status_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(287, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 12)
        Me.Label1.TabIndex = 278
        Me.Label1.Text = "Status"
        '
        'RPMS_1
        '
        Me.RPMS_1.Location = New System.Drawing.Point(343, 32)
        Me.RPMS_1.Name = "RPMS_1"
        Me.RPMS_1.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_1.TabIndex = 300
        Me.RPMS_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_1
        '
        Me.AlarmCode_1.Location = New System.Drawing.Point(501, 32)
        Me.AlarmCode_1.Name = "AlarmCode_1"
        Me.AlarmCode_1.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_1.TabIndex = 298
        Me.AlarmCode_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_2
        '
        Me.Status_2.Location = New System.Drawing.Point(264, 60)
        Me.Status_2.Name = "Status_2"
        Me.Status_2.Size = New System.Drawing.Size(55, 22)
        Me.Status_2.TabIndex = 305
        Me.Status_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_2
        '
        Me.RPMS_2.Location = New System.Drawing.Point(343, 60)
        Me.RPMS_2.Name = "RPMS_2"
        Me.RPMS_2.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_2.TabIndex = 303
        Me.RPMS_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(97, 94)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(100, 22)
        Me.TextBox9.TabIndex = 310
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(98, 232)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(99, 23)
        Me.Button6.TabIndex = 317
        Me.Button6.Text = "Set Serial Port Form"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(494, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 12)
        Me.Label11.TabIndex = 319
        Me.Label11.Text = "Alarm Code"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(448, 13)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(29, 12)
        Me.Label22.TabIndex = 318
        Me.Label22.Text = "RPM"
        '
        'Status_3
        '
        Me.Status_3.Location = New System.Drawing.Point(264, 90)
        Me.Status_3.Name = "Status_3"
        Me.Status_3.Size = New System.Drawing.Size(55, 22)
        Me.Status_3.TabIndex = 324
        Me.Status_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_3
        '
        Me.RPMS_3.Location = New System.Drawing.Point(343, 90)
        Me.RPMS_3.Name = "RPMS_3"
        Me.RPMS_3.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_3.TabIndex = 323
        Me.RPMS_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_3
        '
        Me.AlarmCode_3.Location = New System.Drawing.Point(501, 90)
        Me.AlarmCode_3.Name = "AlarmCode_3"
        Me.AlarmCode_3.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_3.TabIndex = 321
        Me.AlarmCode_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_3
        '
        Me.RPM_3.Location = New System.Drawing.Point(422, 90)
        Me.RPM_3.Name = "RPM_3"
        Me.RPM_3.Size = New System.Drawing.Size(55, 22)
        Me.RPM_3.TabIndex = 320
        Me.RPM_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID_3
        '
        Me.ID_3.AutoSize = True
        Me.ID_3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_3.Location = New System.Drawing.Point(223, 97)
        Me.ID_3.Name = "ID_3"
        Me.ID_3.Size = New System.Drawing.Size(35, 13)
        Me.ID_3.TabIndex = 288
        Me.ID_3.Text = "ID:03"
        '
        'Status_4
        '
        Me.Status_4.Location = New System.Drawing.Point(264, 118)
        Me.Status_4.Name = "Status_4"
        Me.Status_4.Size = New System.Drawing.Size(55, 22)
        Me.Status_4.TabIndex = 329
        Me.Status_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_4
        '
        Me.RPMS_4.Location = New System.Drawing.Point(343, 118)
        Me.RPMS_4.Name = "RPMS_4"
        Me.RPMS_4.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_4.TabIndex = 328
        Me.RPMS_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_4
        '
        Me.AlarmCode_4.Location = New System.Drawing.Point(501, 118)
        Me.AlarmCode_4.Name = "AlarmCode_4"
        Me.AlarmCode_4.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_4.TabIndex = 327
        Me.AlarmCode_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_4
        '
        Me.RPM_4.Location = New System.Drawing.Point(422, 118)
        Me.RPM_4.Name = "RPM_4"
        Me.RPM_4.Size = New System.Drawing.Size(55, 22)
        Me.RPM_4.TabIndex = 326
        Me.RPM_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_5
        '
        Me.Status_5.Location = New System.Drawing.Point(264, 146)
        Me.Status_5.Name = "Status_5"
        Me.Status_5.Size = New System.Drawing.Size(55, 22)
        Me.Status_5.TabIndex = 334
        Me.Status_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_5
        '
        Me.RPMS_5.Location = New System.Drawing.Point(343, 146)
        Me.RPMS_5.Name = "RPMS_5"
        Me.RPMS_5.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_5.TabIndex = 333
        Me.RPMS_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_5
        '
        Me.AlarmCode_5.Location = New System.Drawing.Point(501, 146)
        Me.AlarmCode_5.Name = "AlarmCode_5"
        Me.AlarmCode_5.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_5.TabIndex = 332
        Me.AlarmCode_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_5
        '
        Me.RPM_5.Location = New System.Drawing.Point(422, 146)
        Me.RPM_5.Name = "RPM_5"
        Me.RPM_5.Size = New System.Drawing.Size(55, 22)
        Me.RPM_5.TabIndex = 331
        Me.RPM_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_6
        '
        Me.Status_6.Location = New System.Drawing.Point(264, 174)
        Me.Status_6.Name = "Status_6"
        Me.Status_6.Size = New System.Drawing.Size(55, 22)
        Me.Status_6.TabIndex = 339
        Me.Status_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_6
        '
        Me.RPMS_6.Location = New System.Drawing.Point(343, 174)
        Me.RPMS_6.Name = "RPMS_6"
        Me.RPMS_6.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_6.TabIndex = 338
        Me.RPMS_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_6
        '
        Me.AlarmCode_6.Location = New System.Drawing.Point(501, 174)
        Me.AlarmCode_6.Name = "AlarmCode_6"
        Me.AlarmCode_6.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_6.TabIndex = 337
        Me.AlarmCode_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_6
        '
        Me.RPM_6.Location = New System.Drawing.Point(422, 174)
        Me.RPM_6.Name = "RPM_6"
        Me.RPM_6.Size = New System.Drawing.Size(55, 22)
        Me.RPM_6.TabIndex = 336
        Me.RPM_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_7
        '
        Me.Status_7.Location = New System.Drawing.Point(264, 202)
        Me.Status_7.Name = "Status_7"
        Me.Status_7.Size = New System.Drawing.Size(55, 22)
        Me.Status_7.TabIndex = 344
        Me.Status_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_7
        '
        Me.RPMS_7.Location = New System.Drawing.Point(343, 202)
        Me.RPMS_7.Name = "RPMS_7"
        Me.RPMS_7.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_7.TabIndex = 343
        Me.RPMS_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_7
        '
        Me.AlarmCode_7.Location = New System.Drawing.Point(501, 202)
        Me.AlarmCode_7.Name = "AlarmCode_7"
        Me.AlarmCode_7.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_7.TabIndex = 342
        Me.AlarmCode_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_7
        '
        Me.RPM_7.Location = New System.Drawing.Point(422, 202)
        Me.RPM_7.Name = "RPM_7"
        Me.RPM_7.Size = New System.Drawing.Size(55, 22)
        Me.RPM_7.TabIndex = 341
        Me.RPM_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_8
        '
        Me.Status_8.Location = New System.Drawing.Point(264, 230)
        Me.Status_8.Name = "Status_8"
        Me.Status_8.Size = New System.Drawing.Size(55, 22)
        Me.Status_8.TabIndex = 349
        Me.Status_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_8
        '
        Me.RPMS_8.Location = New System.Drawing.Point(343, 230)
        Me.RPMS_8.Name = "RPMS_8"
        Me.RPMS_8.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_8.TabIndex = 348
        Me.RPMS_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_8
        '
        Me.AlarmCode_8.Location = New System.Drawing.Point(501, 230)
        Me.AlarmCode_8.Name = "AlarmCode_8"
        Me.AlarmCode_8.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_8.TabIndex = 347
        Me.AlarmCode_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_8
        '
        Me.RPM_8.Location = New System.Drawing.Point(422, 230)
        Me.RPM_8.Name = "RPM_8"
        Me.RPM_8.Size = New System.Drawing.Size(55, 22)
        Me.RPM_8.TabIndex = 346
        Me.RPM_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_9
        '
        Me.Status_9.Location = New System.Drawing.Point(264, 258)
        Me.Status_9.Name = "Status_9"
        Me.Status_9.Size = New System.Drawing.Size(55, 22)
        Me.Status_9.TabIndex = 354
        Me.Status_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_9
        '
        Me.RPMS_9.Location = New System.Drawing.Point(343, 258)
        Me.RPMS_9.Name = "RPMS_9"
        Me.RPMS_9.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_9.TabIndex = 353
        Me.RPMS_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_9
        '
        Me.AlarmCode_9.Location = New System.Drawing.Point(501, 258)
        Me.AlarmCode_9.Name = "AlarmCode_9"
        Me.AlarmCode_9.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_9.TabIndex = 352
        Me.AlarmCode_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_9
        '
        Me.RPM_9.Location = New System.Drawing.Point(422, 258)
        Me.RPM_9.Name = "RPM_9"
        Me.RPM_9.Size = New System.Drawing.Size(55, 22)
        Me.RPM_9.TabIndex = 351
        Me.RPM_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_10
        '
        Me.Status_10.Location = New System.Drawing.Point(264, 286)
        Me.Status_10.Name = "Status_10"
        Me.Status_10.Size = New System.Drawing.Size(55, 22)
        Me.Status_10.TabIndex = 359
        Me.Status_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_10
        '
        Me.RPMS_10.Location = New System.Drawing.Point(343, 286)
        Me.RPMS_10.Name = "RPMS_10"
        Me.RPMS_10.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_10.TabIndex = 358
        Me.RPMS_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_10
        '
        Me.AlarmCode_10.Location = New System.Drawing.Point(501, 286)
        Me.AlarmCode_10.Name = "AlarmCode_10"
        Me.AlarmCode_10.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_10.TabIndex = 357
        Me.AlarmCode_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_10
        '
        Me.RPM_10.Location = New System.Drawing.Point(422, 286)
        Me.RPM_10.Name = "RPM_10"
        Me.RPM_10.Size = New System.Drawing.Size(55, 22)
        Me.RPM_10.TabIndex = 356
        Me.RPM_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_12
        '
        Me.Status_12.Location = New System.Drawing.Point(264, 342)
        Me.Status_12.Name = "Status_12"
        Me.Status_12.Size = New System.Drawing.Size(55, 22)
        Me.Status_12.TabIndex = 369
        Me.Status_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_12
        '
        Me.RPMS_12.Location = New System.Drawing.Point(343, 342)
        Me.RPMS_12.Name = "RPMS_12"
        Me.RPMS_12.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_12.TabIndex = 368
        Me.RPMS_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_12
        '
        Me.AlarmCode_12.Location = New System.Drawing.Point(501, 342)
        Me.AlarmCode_12.Name = "AlarmCode_12"
        Me.AlarmCode_12.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_12.TabIndex = 367
        Me.AlarmCode_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_12
        '
        Me.RPM_12.Location = New System.Drawing.Point(422, 342)
        Me.RPM_12.Name = "RPM_12"
        Me.RPM_12.Size = New System.Drawing.Size(55, 22)
        Me.RPM_12.TabIndex = 366
        Me.RPM_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_11
        '
        Me.Status_11.Location = New System.Drawing.Point(264, 314)
        Me.Status_11.Name = "Status_11"
        Me.Status_11.Size = New System.Drawing.Size(55, 22)
        Me.Status_11.TabIndex = 365
        Me.Status_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_11
        '
        Me.RPMS_11.Location = New System.Drawing.Point(343, 314)
        Me.RPMS_11.Name = "RPMS_11"
        Me.RPMS_11.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_11.TabIndex = 364
        Me.RPMS_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_11
        '
        Me.AlarmCode_11.Location = New System.Drawing.Point(501, 314)
        Me.AlarmCode_11.Name = "AlarmCode_11"
        Me.AlarmCode_11.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_11.TabIndex = 363
        Me.AlarmCode_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_11
        '
        Me.RPM_11.Location = New System.Drawing.Point(422, 314)
        Me.RPM_11.Name = "RPM_11"
        Me.RPM_11.Size = New System.Drawing.Size(55, 22)
        Me.RPM_11.TabIndex = 362
        Me.RPM_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_24
        '
        Me.Status_24.Location = New System.Drawing.Point(609, 342)
        Me.Status_24.Name = "Status_24"
        Me.Status_24.Size = New System.Drawing.Size(55, 22)
        Me.Status_24.TabIndex = 433
        Me.Status_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_24
        '
        Me.RPMS_24.Location = New System.Drawing.Point(688, 342)
        Me.RPMS_24.Name = "RPMS_24"
        Me.RPMS_24.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_24.TabIndex = 432
        Me.RPMS_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_24
        '
        Me.AlarmCode_24.Location = New System.Drawing.Point(846, 342)
        Me.AlarmCode_24.Name = "AlarmCode_24"
        Me.AlarmCode_24.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_24.TabIndex = 431
        Me.AlarmCode_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_24
        '
        Me.RPM_24.Location = New System.Drawing.Point(767, 342)
        Me.RPM_24.Name = "RPM_24"
        Me.RPM_24.Size = New System.Drawing.Size(55, 22)
        Me.RPM_24.TabIndex = 430
        Me.RPM_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_23
        '
        Me.Status_23.Location = New System.Drawing.Point(609, 314)
        Me.Status_23.Name = "Status_23"
        Me.Status_23.Size = New System.Drawing.Size(55, 22)
        Me.Status_23.TabIndex = 429
        Me.Status_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_23
        '
        Me.RPMS_23.Location = New System.Drawing.Point(688, 314)
        Me.RPMS_23.Name = "RPMS_23"
        Me.RPMS_23.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_23.TabIndex = 428
        Me.RPMS_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_23
        '
        Me.AlarmCode_23.Location = New System.Drawing.Point(846, 314)
        Me.AlarmCode_23.Name = "AlarmCode_23"
        Me.AlarmCode_23.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_23.TabIndex = 427
        Me.AlarmCode_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_23
        '
        Me.RPM_23.Location = New System.Drawing.Point(767, 314)
        Me.RPM_23.Name = "RPM_23"
        Me.RPM_23.Size = New System.Drawing.Size(55, 22)
        Me.RPM_23.TabIndex = 426
        Me.RPM_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_22
        '
        Me.Status_22.Location = New System.Drawing.Point(609, 286)
        Me.Status_22.Name = "Status_22"
        Me.Status_22.Size = New System.Drawing.Size(55, 22)
        Me.Status_22.TabIndex = 425
        Me.Status_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_22
        '
        Me.RPMS_22.Location = New System.Drawing.Point(688, 286)
        Me.RPMS_22.Name = "RPMS_22"
        Me.RPMS_22.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_22.TabIndex = 424
        Me.RPMS_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_22
        '
        Me.AlarmCode_22.Location = New System.Drawing.Point(846, 286)
        Me.AlarmCode_22.Name = "AlarmCode_22"
        Me.AlarmCode_22.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_22.TabIndex = 423
        Me.AlarmCode_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_22
        '
        Me.RPM_22.Location = New System.Drawing.Point(767, 286)
        Me.RPM_22.Name = "RPM_22"
        Me.RPM_22.Size = New System.Drawing.Size(55, 22)
        Me.RPM_22.TabIndex = 422
        Me.RPM_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_21
        '
        Me.Status_21.Location = New System.Drawing.Point(609, 258)
        Me.Status_21.Name = "Status_21"
        Me.Status_21.Size = New System.Drawing.Size(55, 22)
        Me.Status_21.TabIndex = 421
        Me.Status_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_21
        '
        Me.RPMS_21.Location = New System.Drawing.Point(688, 258)
        Me.RPMS_21.Name = "RPMS_21"
        Me.RPMS_21.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_21.TabIndex = 420
        Me.RPMS_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_21
        '
        Me.AlarmCode_21.Location = New System.Drawing.Point(846, 258)
        Me.AlarmCode_21.Name = "AlarmCode_21"
        Me.AlarmCode_21.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_21.TabIndex = 419
        Me.AlarmCode_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_21
        '
        Me.RPM_21.Location = New System.Drawing.Point(767, 258)
        Me.RPM_21.Name = "RPM_21"
        Me.RPM_21.Size = New System.Drawing.Size(55, 22)
        Me.RPM_21.TabIndex = 418
        Me.RPM_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_20
        '
        Me.Status_20.Location = New System.Drawing.Point(609, 230)
        Me.Status_20.Name = "Status_20"
        Me.Status_20.Size = New System.Drawing.Size(55, 22)
        Me.Status_20.TabIndex = 417
        Me.Status_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_20
        '
        Me.RPMS_20.Location = New System.Drawing.Point(688, 230)
        Me.RPMS_20.Name = "RPMS_20"
        Me.RPMS_20.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_20.TabIndex = 416
        Me.RPMS_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_20
        '
        Me.AlarmCode_20.Location = New System.Drawing.Point(846, 230)
        Me.AlarmCode_20.Name = "AlarmCode_20"
        Me.AlarmCode_20.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_20.TabIndex = 415
        Me.AlarmCode_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_20
        '
        Me.RPM_20.Location = New System.Drawing.Point(767, 230)
        Me.RPM_20.Name = "RPM_20"
        Me.RPM_20.Size = New System.Drawing.Size(55, 22)
        Me.RPM_20.TabIndex = 414
        Me.RPM_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_19
        '
        Me.Status_19.Location = New System.Drawing.Point(609, 202)
        Me.Status_19.Name = "Status_19"
        Me.Status_19.Size = New System.Drawing.Size(55, 22)
        Me.Status_19.TabIndex = 413
        Me.Status_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_19
        '
        Me.RPMS_19.Location = New System.Drawing.Point(688, 202)
        Me.RPMS_19.Name = "RPMS_19"
        Me.RPMS_19.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_19.TabIndex = 412
        Me.RPMS_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_19
        '
        Me.AlarmCode_19.Location = New System.Drawing.Point(846, 202)
        Me.AlarmCode_19.Name = "AlarmCode_19"
        Me.AlarmCode_19.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_19.TabIndex = 411
        Me.AlarmCode_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_19
        '
        Me.RPM_19.Location = New System.Drawing.Point(767, 202)
        Me.RPM_19.Name = "RPM_19"
        Me.RPM_19.Size = New System.Drawing.Size(55, 22)
        Me.RPM_19.TabIndex = 410
        Me.RPM_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_18
        '
        Me.Status_18.Location = New System.Drawing.Point(609, 174)
        Me.Status_18.Name = "Status_18"
        Me.Status_18.Size = New System.Drawing.Size(55, 22)
        Me.Status_18.TabIndex = 409
        Me.Status_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_18
        '
        Me.RPMS_18.Location = New System.Drawing.Point(688, 174)
        Me.RPMS_18.Name = "RPMS_18"
        Me.RPMS_18.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_18.TabIndex = 408
        Me.RPMS_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_18
        '
        Me.AlarmCode_18.Location = New System.Drawing.Point(846, 174)
        Me.AlarmCode_18.Name = "AlarmCode_18"
        Me.AlarmCode_18.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_18.TabIndex = 407
        Me.AlarmCode_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_18
        '
        Me.RPM_18.Location = New System.Drawing.Point(767, 174)
        Me.RPM_18.Name = "RPM_18"
        Me.RPM_18.Size = New System.Drawing.Size(55, 22)
        Me.RPM_18.TabIndex = 406
        Me.RPM_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_17
        '
        Me.Status_17.Location = New System.Drawing.Point(609, 146)
        Me.Status_17.Name = "Status_17"
        Me.Status_17.Size = New System.Drawing.Size(55, 22)
        Me.Status_17.TabIndex = 405
        Me.Status_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_17
        '
        Me.RPMS_17.Location = New System.Drawing.Point(688, 146)
        Me.RPMS_17.Name = "RPMS_17"
        Me.RPMS_17.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_17.TabIndex = 404
        Me.RPMS_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_17
        '
        Me.AlarmCode_17.Location = New System.Drawing.Point(846, 146)
        Me.AlarmCode_17.Name = "AlarmCode_17"
        Me.AlarmCode_17.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_17.TabIndex = 403
        Me.AlarmCode_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_17
        '
        Me.RPM_17.Location = New System.Drawing.Point(767, 146)
        Me.RPM_17.Name = "RPM_17"
        Me.RPM_17.Size = New System.Drawing.Size(55, 22)
        Me.RPM_17.TabIndex = 402
        Me.RPM_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_16
        '
        Me.Status_16.Location = New System.Drawing.Point(609, 118)
        Me.Status_16.Name = "Status_16"
        Me.Status_16.Size = New System.Drawing.Size(55, 22)
        Me.Status_16.TabIndex = 401
        Me.Status_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_16
        '
        Me.RPMS_16.Location = New System.Drawing.Point(688, 118)
        Me.RPMS_16.Name = "RPMS_16"
        Me.RPMS_16.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_16.TabIndex = 400
        Me.RPMS_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_16
        '
        Me.AlarmCode_16.Location = New System.Drawing.Point(846, 118)
        Me.AlarmCode_16.Name = "AlarmCode_16"
        Me.AlarmCode_16.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_16.TabIndex = 399
        Me.AlarmCode_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_16
        '
        Me.RPM_16.Location = New System.Drawing.Point(767, 118)
        Me.RPM_16.Name = "RPM_16"
        Me.RPM_16.Size = New System.Drawing.Size(55, 22)
        Me.RPM_16.TabIndex = 398
        Me.RPM_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_15
        '
        Me.Status_15.Location = New System.Drawing.Point(609, 90)
        Me.Status_15.Name = "Status_15"
        Me.Status_15.Size = New System.Drawing.Size(55, 22)
        Me.Status_15.TabIndex = 397
        Me.Status_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_15
        '
        Me.RPMS_15.Location = New System.Drawing.Point(688, 90)
        Me.RPMS_15.Name = "RPMS_15"
        Me.RPMS_15.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_15.TabIndex = 396
        Me.RPMS_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_15
        '
        Me.AlarmCode_15.Location = New System.Drawing.Point(846, 90)
        Me.AlarmCode_15.Name = "AlarmCode_15"
        Me.AlarmCode_15.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_15.TabIndex = 395
        Me.AlarmCode_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_15
        '
        Me.RPM_15.Location = New System.Drawing.Point(767, 90)
        Me.RPM_15.Name = "RPM_15"
        Me.RPM_15.Size = New System.Drawing.Size(55, 22)
        Me.RPM_15.TabIndex = 394
        Me.RPM_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(839, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 12)
        Me.Label2.TabIndex = 393
        Me.Label2.Text = "Alarm Code"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(793, 13)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 12)
        Me.Label10.TabIndex = 392
        Me.Label10.Text = "RPM"
        '
        'Status_14
        '
        Me.Status_14.Location = New System.Drawing.Point(609, 60)
        Me.Status_14.Name = "Status_14"
        Me.Status_14.Size = New System.Drawing.Size(55, 22)
        Me.Status_14.TabIndex = 391
        Me.Status_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_14
        '
        Me.RPMS_14.Location = New System.Drawing.Point(688, 60)
        Me.RPMS_14.Name = "RPMS_14"
        Me.RPMS_14.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_14.TabIndex = 390
        Me.RPMS_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_13
        '
        Me.RPMS_13.Location = New System.Drawing.Point(688, 32)
        Me.RPMS_13.Name = "RPMS_13"
        Me.RPMS_13.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_13.TabIndex = 389
        Me.RPMS_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_13
        '
        Me.AlarmCode_13.Location = New System.Drawing.Point(846, 32)
        Me.AlarmCode_13.Name = "AlarmCode_13"
        Me.AlarmCode_13.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_13.TabIndex = 388
        Me.AlarmCode_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID_24
        '
        Me.ID_24.AutoSize = True
        Me.ID_24.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_24.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_24.Location = New System.Drawing.Point(568, 349)
        Me.ID_24.Name = "ID_24"
        Me.ID_24.Size = New System.Drawing.Size(35, 13)
        Me.ID_24.TabIndex = 387
        Me.ID_24.Text = "ID:24"
        '
        'ID_23
        '
        Me.ID_23.AutoSize = True
        Me.ID_23.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_23.Location = New System.Drawing.Point(568, 321)
        Me.ID_23.Name = "ID_23"
        Me.ID_23.Size = New System.Drawing.Size(35, 13)
        Me.ID_23.TabIndex = 386
        Me.ID_23.Text = "ID:23"
        '
        'ID_22
        '
        Me.ID_22.AutoSize = True
        Me.ID_22.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_22.Location = New System.Drawing.Point(568, 293)
        Me.ID_22.Name = "ID_22"
        Me.ID_22.Size = New System.Drawing.Size(35, 13)
        Me.ID_22.TabIndex = 385
        Me.ID_22.Text = "ID:22"
        '
        'ID_21
        '
        Me.ID_21.AutoSize = True
        Me.ID_21.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_21.Location = New System.Drawing.Point(568, 265)
        Me.ID_21.Name = "ID_21"
        Me.ID_21.Size = New System.Drawing.Size(35, 13)
        Me.ID_21.TabIndex = 384
        Me.ID_21.Text = "ID:21"
        '
        'ID_20
        '
        Me.ID_20.AutoSize = True
        Me.ID_20.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_20.Location = New System.Drawing.Point(568, 237)
        Me.ID_20.Name = "ID_20"
        Me.ID_20.Size = New System.Drawing.Size(35, 13)
        Me.ID_20.TabIndex = 383
        Me.ID_20.Text = "ID:20"
        '
        'ID_19
        '
        Me.ID_19.AutoSize = True
        Me.ID_19.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_19.Location = New System.Drawing.Point(568, 209)
        Me.ID_19.Name = "ID_19"
        Me.ID_19.Size = New System.Drawing.Size(35, 13)
        Me.ID_19.TabIndex = 382
        Me.ID_19.Text = "ID:19"
        '
        'ID_18
        '
        Me.ID_18.AutoSize = True
        Me.ID_18.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_18.Location = New System.Drawing.Point(568, 181)
        Me.ID_18.Name = "ID_18"
        Me.ID_18.Size = New System.Drawing.Size(35, 13)
        Me.ID_18.TabIndex = 381
        Me.ID_18.Text = "ID:18"
        '
        'ID_17
        '
        Me.ID_17.AutoSize = True
        Me.ID_17.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_17.Location = New System.Drawing.Point(568, 153)
        Me.ID_17.Name = "ID_17"
        Me.ID_17.Size = New System.Drawing.Size(35, 13)
        Me.ID_17.TabIndex = 380
        Me.ID_17.Text = "ID:17"
        '
        'ID_16
        '
        Me.ID_16.AutoSize = True
        Me.ID_16.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_16.Location = New System.Drawing.Point(568, 125)
        Me.ID_16.Name = "ID_16"
        Me.ID_16.Size = New System.Drawing.Size(35, 13)
        Me.ID_16.TabIndex = 379
        Me.ID_16.Text = "ID:16"
        '
        'ID_15
        '
        Me.ID_15.AutoSize = True
        Me.ID_15.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_15.Location = New System.Drawing.Point(568, 97)
        Me.ID_15.Name = "ID_15"
        Me.ID_15.Size = New System.Drawing.Size(35, 13)
        Me.ID_15.TabIndex = 378
        Me.ID_15.Text = "ID:15"
        '
        'ID_14
        '
        Me.ID_14.AutoSize = True
        Me.ID_14.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_14.Location = New System.Drawing.Point(568, 69)
        Me.ID_14.Name = "ID_14"
        Me.ID_14.Size = New System.Drawing.Size(35, 13)
        Me.ID_14.TabIndex = 377
        Me.ID_14.Text = "ID:14"
        '
        'ID_13
        '
        Me.ID_13.AutoSize = True
        Me.ID_13.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_13.Location = New System.Drawing.Point(568, 41)
        Me.ID_13.Name = "ID_13"
        Me.ID_13.Size = New System.Drawing.Size(35, 13)
        Me.ID_13.TabIndex = 376
        Me.ID_13.Text = "ID:13"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(697, 13)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(46, 12)
        Me.Label27.TabIndex = 375
        Me.Label27.Text = "RPM Set"
        '
        'AlarmCode_14
        '
        Me.AlarmCode_14.Location = New System.Drawing.Point(846, 60)
        Me.AlarmCode_14.Name = "AlarmCode_14"
        Me.AlarmCode_14.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_14.TabIndex = 374
        Me.AlarmCode_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(632, 13)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(32, 12)
        Me.Label28.TabIndex = 373
        Me.Label28.Text = "Status"
        '
        'Status_13
        '
        Me.Status_13.Location = New System.Drawing.Point(609, 32)
        Me.Status_13.Name = "Status_13"
        Me.Status_13.Size = New System.Drawing.Size(55, 22)
        Me.Status_13.TabIndex = 372
        Me.Status_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_14
        '
        Me.RPM_14.Location = New System.Drawing.Point(767, 60)
        Me.RPM_14.Name = "RPM_14"
        Me.RPM_14.Size = New System.Drawing.Size(55, 22)
        Me.RPM_14.TabIndex = 371
        Me.RPM_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_13
        '
        Me.RPM_13.Location = New System.Drawing.Point(767, 32)
        Me.RPM_13.Name = "RPM_13"
        Me.RPM_13.Size = New System.Drawing.Size(55, 22)
        Me.RPM_13.TabIndex = 370
        Me.RPM_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_36
        '
        Me.Status_36.Location = New System.Drawing.Point(958, 342)
        Me.Status_36.Name = "Status_36"
        Me.Status_36.Size = New System.Drawing.Size(55, 22)
        Me.Status_36.TabIndex = 497
        Me.Status_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_36
        '
        Me.RPMS_36.Location = New System.Drawing.Point(1037, 342)
        Me.RPMS_36.Name = "RPMS_36"
        Me.RPMS_36.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_36.TabIndex = 496
        Me.RPMS_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_36
        '
        Me.AlarmCode_36.Location = New System.Drawing.Point(1195, 342)
        Me.AlarmCode_36.Name = "AlarmCode_36"
        Me.AlarmCode_36.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_36.TabIndex = 495
        Me.AlarmCode_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_36
        '
        Me.RPM_36.Location = New System.Drawing.Point(1116, 342)
        Me.RPM_36.Name = "RPM_36"
        Me.RPM_36.Size = New System.Drawing.Size(55, 22)
        Me.RPM_36.TabIndex = 494
        Me.RPM_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_35
        '
        Me.Status_35.Location = New System.Drawing.Point(958, 314)
        Me.Status_35.Name = "Status_35"
        Me.Status_35.Size = New System.Drawing.Size(55, 22)
        Me.Status_35.TabIndex = 493
        Me.Status_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_35
        '
        Me.RPMS_35.Location = New System.Drawing.Point(1037, 314)
        Me.RPMS_35.Name = "RPMS_35"
        Me.RPMS_35.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_35.TabIndex = 492
        Me.RPMS_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_35
        '
        Me.AlarmCode_35.Location = New System.Drawing.Point(1195, 314)
        Me.AlarmCode_35.Name = "AlarmCode_35"
        Me.AlarmCode_35.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_35.TabIndex = 491
        Me.AlarmCode_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_35
        '
        Me.RPM_35.Location = New System.Drawing.Point(1116, 314)
        Me.RPM_35.Name = "RPM_35"
        Me.RPM_35.Size = New System.Drawing.Size(55, 22)
        Me.RPM_35.TabIndex = 490
        Me.RPM_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_34
        '
        Me.Status_34.Location = New System.Drawing.Point(958, 286)
        Me.Status_34.Name = "Status_34"
        Me.Status_34.Size = New System.Drawing.Size(55, 22)
        Me.Status_34.TabIndex = 489
        Me.Status_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_34
        '
        Me.RPMS_34.Location = New System.Drawing.Point(1037, 286)
        Me.RPMS_34.Name = "RPMS_34"
        Me.RPMS_34.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_34.TabIndex = 488
        Me.RPMS_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_34
        '
        Me.AlarmCode_34.Location = New System.Drawing.Point(1195, 286)
        Me.AlarmCode_34.Name = "AlarmCode_34"
        Me.AlarmCode_34.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_34.TabIndex = 487
        Me.AlarmCode_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_34
        '
        Me.RPM_34.Location = New System.Drawing.Point(1116, 286)
        Me.RPM_34.Name = "RPM_34"
        Me.RPM_34.Size = New System.Drawing.Size(55, 22)
        Me.RPM_34.TabIndex = 486
        Me.RPM_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_33
        '
        Me.Status_33.Location = New System.Drawing.Point(958, 258)
        Me.Status_33.Name = "Status_33"
        Me.Status_33.Size = New System.Drawing.Size(55, 22)
        Me.Status_33.TabIndex = 485
        Me.Status_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_33
        '
        Me.RPMS_33.Location = New System.Drawing.Point(1037, 258)
        Me.RPMS_33.Name = "RPMS_33"
        Me.RPMS_33.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_33.TabIndex = 484
        Me.RPMS_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_33
        '
        Me.AlarmCode_33.Location = New System.Drawing.Point(1195, 258)
        Me.AlarmCode_33.Name = "AlarmCode_33"
        Me.AlarmCode_33.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_33.TabIndex = 483
        Me.AlarmCode_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_33
        '
        Me.RPM_33.Location = New System.Drawing.Point(1116, 258)
        Me.RPM_33.Name = "RPM_33"
        Me.RPM_33.Size = New System.Drawing.Size(55, 22)
        Me.RPM_33.TabIndex = 482
        Me.RPM_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_32
        '
        Me.Status_32.Location = New System.Drawing.Point(958, 230)
        Me.Status_32.Name = "Status_32"
        Me.Status_32.Size = New System.Drawing.Size(55, 22)
        Me.Status_32.TabIndex = 481
        Me.Status_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_32
        '
        Me.RPMS_32.Location = New System.Drawing.Point(1037, 230)
        Me.RPMS_32.Name = "RPMS_32"
        Me.RPMS_32.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_32.TabIndex = 480
        Me.RPMS_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_32
        '
        Me.AlarmCode_32.Location = New System.Drawing.Point(1195, 230)
        Me.AlarmCode_32.Name = "AlarmCode_32"
        Me.AlarmCode_32.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_32.TabIndex = 479
        Me.AlarmCode_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_32
        '
        Me.RPM_32.Location = New System.Drawing.Point(1116, 230)
        Me.RPM_32.Name = "RPM_32"
        Me.RPM_32.Size = New System.Drawing.Size(55, 22)
        Me.RPM_32.TabIndex = 478
        Me.RPM_32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_31
        '
        Me.Status_31.Location = New System.Drawing.Point(958, 202)
        Me.Status_31.Name = "Status_31"
        Me.Status_31.Size = New System.Drawing.Size(55, 22)
        Me.Status_31.TabIndex = 477
        Me.Status_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_31
        '
        Me.RPMS_31.Location = New System.Drawing.Point(1037, 202)
        Me.RPMS_31.Name = "RPMS_31"
        Me.RPMS_31.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_31.TabIndex = 476
        Me.RPMS_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_31
        '
        Me.AlarmCode_31.Location = New System.Drawing.Point(1195, 202)
        Me.AlarmCode_31.Name = "AlarmCode_31"
        Me.AlarmCode_31.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_31.TabIndex = 475
        Me.AlarmCode_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_31
        '
        Me.RPM_31.Location = New System.Drawing.Point(1116, 202)
        Me.RPM_31.Name = "RPM_31"
        Me.RPM_31.Size = New System.Drawing.Size(55, 22)
        Me.RPM_31.TabIndex = 474
        Me.RPM_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_30
        '
        Me.Status_30.Location = New System.Drawing.Point(958, 174)
        Me.Status_30.Name = "Status_30"
        Me.Status_30.Size = New System.Drawing.Size(55, 22)
        Me.Status_30.TabIndex = 473
        Me.Status_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_30
        '
        Me.RPMS_30.Location = New System.Drawing.Point(1037, 174)
        Me.RPMS_30.Name = "RPMS_30"
        Me.RPMS_30.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_30.TabIndex = 472
        Me.RPMS_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_30
        '
        Me.AlarmCode_30.Location = New System.Drawing.Point(1195, 174)
        Me.AlarmCode_30.Name = "AlarmCode_30"
        Me.AlarmCode_30.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_30.TabIndex = 471
        Me.AlarmCode_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_30
        '
        Me.RPM_30.Location = New System.Drawing.Point(1116, 174)
        Me.RPM_30.Name = "RPM_30"
        Me.RPM_30.Size = New System.Drawing.Size(55, 22)
        Me.RPM_30.TabIndex = 470
        Me.RPM_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_29
        '
        Me.Status_29.Location = New System.Drawing.Point(958, 146)
        Me.Status_29.Name = "Status_29"
        Me.Status_29.Size = New System.Drawing.Size(55, 22)
        Me.Status_29.TabIndex = 469
        Me.Status_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_29
        '
        Me.RPMS_29.Location = New System.Drawing.Point(1037, 146)
        Me.RPMS_29.Name = "RPMS_29"
        Me.RPMS_29.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_29.TabIndex = 468
        Me.RPMS_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_29
        '
        Me.AlarmCode_29.Location = New System.Drawing.Point(1195, 146)
        Me.AlarmCode_29.Name = "AlarmCode_29"
        Me.AlarmCode_29.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_29.TabIndex = 467
        Me.AlarmCode_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_29
        '
        Me.RPM_29.Location = New System.Drawing.Point(1116, 146)
        Me.RPM_29.Name = "RPM_29"
        Me.RPM_29.Size = New System.Drawing.Size(55, 22)
        Me.RPM_29.TabIndex = 466
        Me.RPM_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_28
        '
        Me.Status_28.Location = New System.Drawing.Point(958, 118)
        Me.Status_28.Name = "Status_28"
        Me.Status_28.Size = New System.Drawing.Size(55, 22)
        Me.Status_28.TabIndex = 465
        Me.Status_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_28
        '
        Me.RPMS_28.Location = New System.Drawing.Point(1037, 118)
        Me.RPMS_28.Name = "RPMS_28"
        Me.RPMS_28.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_28.TabIndex = 464
        Me.RPMS_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_28
        '
        Me.AlarmCode_28.Location = New System.Drawing.Point(1195, 118)
        Me.AlarmCode_28.Name = "AlarmCode_28"
        Me.AlarmCode_28.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_28.TabIndex = 463
        Me.AlarmCode_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_28
        '
        Me.RPM_28.Location = New System.Drawing.Point(1116, 118)
        Me.RPM_28.Name = "RPM_28"
        Me.RPM_28.Size = New System.Drawing.Size(55, 22)
        Me.RPM_28.TabIndex = 462
        Me.RPM_28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_27
        '
        Me.Status_27.Location = New System.Drawing.Point(958, 90)
        Me.Status_27.Name = "Status_27"
        Me.Status_27.Size = New System.Drawing.Size(55, 22)
        Me.Status_27.TabIndex = 461
        Me.Status_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_27
        '
        Me.RPMS_27.Location = New System.Drawing.Point(1037, 90)
        Me.RPMS_27.Name = "RPMS_27"
        Me.RPMS_27.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_27.TabIndex = 460
        Me.RPMS_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_27
        '
        Me.AlarmCode_27.Location = New System.Drawing.Point(1195, 90)
        Me.AlarmCode_27.Name = "AlarmCode_27"
        Me.AlarmCode_27.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_27.TabIndex = 459
        Me.AlarmCode_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_27
        '
        Me.RPM_27.Location = New System.Drawing.Point(1116, 90)
        Me.RPM_27.Name = "RPM_27"
        Me.RPM_27.Size = New System.Drawing.Size(55, 22)
        Me.RPM_27.TabIndex = 458
        Me.RPM_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(1188, 13)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(62, 12)
        Me.Label29.TabIndex = 457
        Me.Label29.Text = "Alarm Code"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(1142, 13)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(29, 12)
        Me.Label31.TabIndex = 456
        Me.Label31.Text = "RPM"
        '
        'Status_26
        '
        Me.Status_26.Location = New System.Drawing.Point(958, 60)
        Me.Status_26.Name = "Status_26"
        Me.Status_26.Size = New System.Drawing.Size(55, 22)
        Me.Status_26.TabIndex = 455
        Me.Status_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_26
        '
        Me.RPMS_26.Location = New System.Drawing.Point(1037, 60)
        Me.RPMS_26.Name = "RPMS_26"
        Me.RPMS_26.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_26.TabIndex = 454
        Me.RPMS_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_25
        '
        Me.RPMS_25.Location = New System.Drawing.Point(1037, 32)
        Me.RPMS_25.Name = "RPMS_25"
        Me.RPMS_25.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_25.TabIndex = 453
        Me.RPMS_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_25
        '
        Me.AlarmCode_25.Location = New System.Drawing.Point(1195, 32)
        Me.AlarmCode_25.Name = "AlarmCode_25"
        Me.AlarmCode_25.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_25.TabIndex = 452
        Me.AlarmCode_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID_36
        '
        Me.ID_36.AutoSize = True
        Me.ID_36.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_36.Location = New System.Drawing.Point(917, 349)
        Me.ID_36.Name = "ID_36"
        Me.ID_36.Size = New System.Drawing.Size(35, 13)
        Me.ID_36.TabIndex = 451
        Me.ID_36.Text = "ID:36"
        '
        'ID_35
        '
        Me.ID_35.AutoSize = True
        Me.ID_35.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_35.Location = New System.Drawing.Point(917, 321)
        Me.ID_35.Name = "ID_35"
        Me.ID_35.Size = New System.Drawing.Size(35, 13)
        Me.ID_35.TabIndex = 450
        Me.ID_35.Text = "ID:35"
        '
        'ID_34
        '
        Me.ID_34.AutoSize = True
        Me.ID_34.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_34.Location = New System.Drawing.Point(917, 293)
        Me.ID_34.Name = "ID_34"
        Me.ID_34.Size = New System.Drawing.Size(35, 13)
        Me.ID_34.TabIndex = 449
        Me.ID_34.Text = "ID:34"
        '
        'ID_33
        '
        Me.ID_33.AutoSize = True
        Me.ID_33.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_33.Location = New System.Drawing.Point(917, 265)
        Me.ID_33.Name = "ID_33"
        Me.ID_33.Size = New System.Drawing.Size(35, 13)
        Me.ID_33.TabIndex = 448
        Me.ID_33.Text = "ID:33"
        '
        'ID_32
        '
        Me.ID_32.AutoSize = True
        Me.ID_32.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_32.Location = New System.Drawing.Point(917, 237)
        Me.ID_32.Name = "ID_32"
        Me.ID_32.Size = New System.Drawing.Size(35, 13)
        Me.ID_32.TabIndex = 447
        Me.ID_32.Text = "ID:32"
        '
        'ID_31
        '
        Me.ID_31.AutoSize = True
        Me.ID_31.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_31.Location = New System.Drawing.Point(917, 209)
        Me.ID_31.Name = "ID_31"
        Me.ID_31.Size = New System.Drawing.Size(35, 13)
        Me.ID_31.TabIndex = 446
        Me.ID_31.Text = "ID:31"
        '
        'ID_30
        '
        Me.ID_30.AutoSize = True
        Me.ID_30.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_30.Location = New System.Drawing.Point(917, 181)
        Me.ID_30.Name = "ID_30"
        Me.ID_30.Size = New System.Drawing.Size(35, 13)
        Me.ID_30.TabIndex = 445
        Me.ID_30.Text = "ID:30"
        '
        'ID_29
        '
        Me.ID_29.AutoSize = True
        Me.ID_29.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_29.Location = New System.Drawing.Point(917, 153)
        Me.ID_29.Name = "ID_29"
        Me.ID_29.Size = New System.Drawing.Size(35, 13)
        Me.ID_29.TabIndex = 444
        Me.ID_29.Text = "ID:29"
        '
        'ID_28
        '
        Me.ID_28.AutoSize = True
        Me.ID_28.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_28.Location = New System.Drawing.Point(917, 125)
        Me.ID_28.Name = "ID_28"
        Me.ID_28.Size = New System.Drawing.Size(35, 13)
        Me.ID_28.TabIndex = 443
        Me.ID_28.Text = "ID:28"
        '
        'ID_27
        '
        Me.ID_27.AutoSize = True
        Me.ID_27.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_27.Location = New System.Drawing.Point(917, 97)
        Me.ID_27.Name = "ID_27"
        Me.ID_27.Size = New System.Drawing.Size(35, 13)
        Me.ID_27.TabIndex = 442
        Me.ID_27.Text = "ID:27"
        '
        'ID_26
        '
        Me.ID_26.AutoSize = True
        Me.ID_26.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_26.Location = New System.Drawing.Point(917, 69)
        Me.ID_26.Name = "ID_26"
        Me.ID_26.Size = New System.Drawing.Size(35, 13)
        Me.ID_26.TabIndex = 441
        Me.ID_26.Text = "ID:26"
        '
        'ID_25
        '
        Me.ID_25.AutoSize = True
        Me.ID_25.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_25.Location = New System.Drawing.Point(917, 41)
        Me.ID_25.Name = "ID_25"
        Me.ID_25.Size = New System.Drawing.Size(35, 13)
        Me.ID_25.TabIndex = 440
        Me.ID_25.Text = "ID:25"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(1046, 13)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(46, 12)
        Me.Label44.TabIndex = 439
        Me.Label44.Text = "RPM Set"
        '
        'AlarmCode_26
        '
        Me.AlarmCode_26.Location = New System.Drawing.Point(1195, 60)
        Me.AlarmCode_26.Name = "AlarmCode_26"
        Me.AlarmCode_26.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_26.TabIndex = 438
        Me.AlarmCode_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(981, 13)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(32, 12)
        Me.Label45.TabIndex = 437
        Me.Label45.Text = "Status"
        '
        'Status_25
        '
        Me.Status_25.Location = New System.Drawing.Point(958, 32)
        Me.Status_25.Name = "Status_25"
        Me.Status_25.Size = New System.Drawing.Size(55, 22)
        Me.Status_25.TabIndex = 436
        Me.Status_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_26
        '
        Me.RPM_26.Location = New System.Drawing.Point(1116, 60)
        Me.RPM_26.Name = "RPM_26"
        Me.RPM_26.Size = New System.Drawing.Size(55, 22)
        Me.RPM_26.TabIndex = 435
        Me.RPM_26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_25
        '
        Me.RPM_25.Location = New System.Drawing.Point(1116, 32)
        Me.RPM_25.Name = "RPM_25"
        Me.RPM_25.Size = New System.Drawing.Size(55, 22)
        Me.RPM_25.TabIndex = 434
        Me.RPM_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_72
        '
        Me.Status_72.Location = New System.Drawing.Point(958, 708)
        Me.Status_72.Name = "Status_72"
        Me.Status_72.Size = New System.Drawing.Size(55, 22)
        Me.Status_72.TabIndex = 689
        Me.Status_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_72
        '
        Me.RPMS_72.Location = New System.Drawing.Point(1037, 708)
        Me.RPMS_72.Name = "RPMS_72"
        Me.RPMS_72.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_72.TabIndex = 688
        Me.RPMS_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_72
        '
        Me.AlarmCode_72.Location = New System.Drawing.Point(1195, 708)
        Me.AlarmCode_72.Name = "AlarmCode_72"
        Me.AlarmCode_72.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_72.TabIndex = 687
        Me.AlarmCode_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_72
        '
        Me.RPM_72.Location = New System.Drawing.Point(1116, 708)
        Me.RPM_72.Name = "RPM_72"
        Me.RPM_72.Size = New System.Drawing.Size(55, 22)
        Me.RPM_72.TabIndex = 686
        Me.RPM_72.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_71
        '
        Me.Status_71.Location = New System.Drawing.Point(958, 680)
        Me.Status_71.Name = "Status_71"
        Me.Status_71.Size = New System.Drawing.Size(55, 22)
        Me.Status_71.TabIndex = 685
        Me.Status_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_71
        '
        Me.RPMS_71.Location = New System.Drawing.Point(1037, 680)
        Me.RPMS_71.Name = "RPMS_71"
        Me.RPMS_71.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_71.TabIndex = 684
        Me.RPMS_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_71
        '
        Me.AlarmCode_71.Location = New System.Drawing.Point(1195, 680)
        Me.AlarmCode_71.Name = "AlarmCode_71"
        Me.AlarmCode_71.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_71.TabIndex = 683
        Me.AlarmCode_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_71
        '
        Me.RPM_71.Location = New System.Drawing.Point(1116, 680)
        Me.RPM_71.Name = "RPM_71"
        Me.RPM_71.Size = New System.Drawing.Size(55, 22)
        Me.RPM_71.TabIndex = 682
        Me.RPM_71.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_70
        '
        Me.Status_70.Location = New System.Drawing.Point(958, 652)
        Me.Status_70.Name = "Status_70"
        Me.Status_70.Size = New System.Drawing.Size(55, 22)
        Me.Status_70.TabIndex = 681
        Me.Status_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_70
        '
        Me.RPMS_70.Location = New System.Drawing.Point(1037, 652)
        Me.RPMS_70.Name = "RPMS_70"
        Me.RPMS_70.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_70.TabIndex = 680
        Me.RPMS_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_70
        '
        Me.AlarmCode_70.Location = New System.Drawing.Point(1195, 652)
        Me.AlarmCode_70.Name = "AlarmCode_70"
        Me.AlarmCode_70.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_70.TabIndex = 679
        Me.AlarmCode_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_70
        '
        Me.RPM_70.Location = New System.Drawing.Point(1116, 652)
        Me.RPM_70.Name = "RPM_70"
        Me.RPM_70.Size = New System.Drawing.Size(55, 22)
        Me.RPM_70.TabIndex = 678
        Me.RPM_70.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_69
        '
        Me.Status_69.Location = New System.Drawing.Point(958, 624)
        Me.Status_69.Name = "Status_69"
        Me.Status_69.Size = New System.Drawing.Size(55, 22)
        Me.Status_69.TabIndex = 677
        Me.Status_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_69
        '
        Me.RPMS_69.Location = New System.Drawing.Point(1037, 624)
        Me.RPMS_69.Name = "RPMS_69"
        Me.RPMS_69.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_69.TabIndex = 676
        Me.RPMS_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_69
        '
        Me.AlarmCode_69.Location = New System.Drawing.Point(1195, 624)
        Me.AlarmCode_69.Name = "AlarmCode_69"
        Me.AlarmCode_69.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_69.TabIndex = 675
        Me.AlarmCode_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_69
        '
        Me.RPM_69.Location = New System.Drawing.Point(1116, 624)
        Me.RPM_69.Name = "RPM_69"
        Me.RPM_69.Size = New System.Drawing.Size(55, 22)
        Me.RPM_69.TabIndex = 674
        Me.RPM_69.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_68
        '
        Me.Status_68.Location = New System.Drawing.Point(958, 596)
        Me.Status_68.Name = "Status_68"
        Me.Status_68.Size = New System.Drawing.Size(55, 22)
        Me.Status_68.TabIndex = 673
        Me.Status_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_68
        '
        Me.RPMS_68.Location = New System.Drawing.Point(1037, 596)
        Me.RPMS_68.Name = "RPMS_68"
        Me.RPMS_68.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_68.TabIndex = 672
        Me.RPMS_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_68
        '
        Me.AlarmCode_68.Location = New System.Drawing.Point(1195, 596)
        Me.AlarmCode_68.Name = "AlarmCode_68"
        Me.AlarmCode_68.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_68.TabIndex = 671
        Me.AlarmCode_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_68
        '
        Me.RPM_68.Location = New System.Drawing.Point(1116, 596)
        Me.RPM_68.Name = "RPM_68"
        Me.RPM_68.Size = New System.Drawing.Size(55, 22)
        Me.RPM_68.TabIndex = 670
        Me.RPM_68.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_67
        '
        Me.Status_67.Location = New System.Drawing.Point(958, 568)
        Me.Status_67.Name = "Status_67"
        Me.Status_67.Size = New System.Drawing.Size(55, 22)
        Me.Status_67.TabIndex = 669
        Me.Status_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_67
        '
        Me.RPMS_67.Location = New System.Drawing.Point(1037, 568)
        Me.RPMS_67.Name = "RPMS_67"
        Me.RPMS_67.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_67.TabIndex = 668
        Me.RPMS_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_67
        '
        Me.AlarmCode_67.Location = New System.Drawing.Point(1195, 568)
        Me.AlarmCode_67.Name = "AlarmCode_67"
        Me.AlarmCode_67.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_67.TabIndex = 667
        Me.AlarmCode_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_67
        '
        Me.RPM_67.Location = New System.Drawing.Point(1116, 568)
        Me.RPM_67.Name = "RPM_67"
        Me.RPM_67.Size = New System.Drawing.Size(55, 22)
        Me.RPM_67.TabIndex = 666
        Me.RPM_67.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_66
        '
        Me.Status_66.Location = New System.Drawing.Point(958, 540)
        Me.Status_66.Name = "Status_66"
        Me.Status_66.Size = New System.Drawing.Size(55, 22)
        Me.Status_66.TabIndex = 665
        Me.Status_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_66
        '
        Me.RPMS_66.Location = New System.Drawing.Point(1037, 540)
        Me.RPMS_66.Name = "RPMS_66"
        Me.RPMS_66.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_66.TabIndex = 664
        Me.RPMS_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_66
        '
        Me.AlarmCode_66.Location = New System.Drawing.Point(1195, 540)
        Me.AlarmCode_66.Name = "AlarmCode_66"
        Me.AlarmCode_66.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_66.TabIndex = 663
        Me.AlarmCode_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_66
        '
        Me.RPM_66.Location = New System.Drawing.Point(1116, 540)
        Me.RPM_66.Name = "RPM_66"
        Me.RPM_66.Size = New System.Drawing.Size(55, 22)
        Me.RPM_66.TabIndex = 662
        Me.RPM_66.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_65
        '
        Me.Status_65.Location = New System.Drawing.Point(958, 512)
        Me.Status_65.Name = "Status_65"
        Me.Status_65.Size = New System.Drawing.Size(55, 22)
        Me.Status_65.TabIndex = 661
        Me.Status_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_65
        '
        Me.RPMS_65.Location = New System.Drawing.Point(1037, 512)
        Me.RPMS_65.Name = "RPMS_65"
        Me.RPMS_65.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_65.TabIndex = 660
        Me.RPMS_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_65
        '
        Me.AlarmCode_65.Location = New System.Drawing.Point(1195, 512)
        Me.AlarmCode_65.Name = "AlarmCode_65"
        Me.AlarmCode_65.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_65.TabIndex = 659
        Me.AlarmCode_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_65
        '
        Me.RPM_65.Location = New System.Drawing.Point(1116, 512)
        Me.RPM_65.Name = "RPM_65"
        Me.RPM_65.Size = New System.Drawing.Size(55, 22)
        Me.RPM_65.TabIndex = 658
        Me.RPM_65.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_64
        '
        Me.Status_64.Location = New System.Drawing.Point(958, 484)
        Me.Status_64.Name = "Status_64"
        Me.Status_64.Size = New System.Drawing.Size(55, 22)
        Me.Status_64.TabIndex = 657
        Me.Status_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_64
        '
        Me.RPMS_64.Location = New System.Drawing.Point(1037, 484)
        Me.RPMS_64.Name = "RPMS_64"
        Me.RPMS_64.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_64.TabIndex = 656
        Me.RPMS_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_64
        '
        Me.RPM_64.Location = New System.Drawing.Point(1116, 484)
        Me.RPM_64.Name = "RPM_64"
        Me.RPM_64.Size = New System.Drawing.Size(55, 22)
        Me.RPM_64.TabIndex = 655
        Me.RPM_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_64
        '
        Me.AlarmCode_64.Location = New System.Drawing.Point(1195, 484)
        Me.AlarmCode_64.Name = "AlarmCode_64"
        Me.AlarmCode_64.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_64.TabIndex = 654
        Me.AlarmCode_64.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_63
        '
        Me.Status_63.Location = New System.Drawing.Point(958, 456)
        Me.Status_63.Name = "Status_63"
        Me.Status_63.Size = New System.Drawing.Size(55, 22)
        Me.Status_63.TabIndex = 653
        Me.Status_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_63
        '
        Me.RPMS_63.Location = New System.Drawing.Point(1037, 456)
        Me.RPMS_63.Name = "RPMS_63"
        Me.RPMS_63.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_63.TabIndex = 652
        Me.RPMS_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_63
        '
        Me.AlarmCode_63.Location = New System.Drawing.Point(1195, 456)
        Me.AlarmCode_63.Name = "AlarmCode_63"
        Me.AlarmCode_63.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_63.TabIndex = 651
        Me.AlarmCode_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_63
        '
        Me.RPM_63.Location = New System.Drawing.Point(1116, 456)
        Me.RPM_63.Name = "RPM_63"
        Me.RPM_63.Size = New System.Drawing.Size(55, 22)
        Me.RPM_63.TabIndex = 650
        Me.RPM_63.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(1188, 379)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(62, 12)
        Me.Label46.TabIndex = 649
        Me.Label46.Text = "Alarm Code"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(1142, 379)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(29, 12)
        Me.Label47.TabIndex = 648
        Me.Label47.Text = "RPM"
        '
        'Status_62
        '
        Me.Status_62.Location = New System.Drawing.Point(958, 426)
        Me.Status_62.Name = "Status_62"
        Me.Status_62.Size = New System.Drawing.Size(55, 22)
        Me.Status_62.TabIndex = 647
        Me.Status_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_62
        '
        Me.RPMS_62.Location = New System.Drawing.Point(1037, 426)
        Me.RPMS_62.Name = "RPMS_62"
        Me.RPMS_62.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_62.TabIndex = 646
        Me.RPMS_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_61
        '
        Me.RPMS_61.Location = New System.Drawing.Point(1037, 398)
        Me.RPMS_61.Name = "RPMS_61"
        Me.RPMS_61.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_61.TabIndex = 645
        Me.RPMS_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_61
        '
        Me.AlarmCode_61.Location = New System.Drawing.Point(1195, 398)
        Me.AlarmCode_61.Name = "AlarmCode_61"
        Me.AlarmCode_61.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_61.TabIndex = 644
        Me.AlarmCode_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID_72
        '
        Me.ID_72.AutoSize = True
        Me.ID_72.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_72.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_72.Location = New System.Drawing.Point(917, 715)
        Me.ID_72.Name = "ID_72"
        Me.ID_72.Size = New System.Drawing.Size(35, 13)
        Me.ID_72.TabIndex = 643
        Me.ID_72.Text = "ID:72"
        '
        'ID_71
        '
        Me.ID_71.AutoSize = True
        Me.ID_71.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_71.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_71.Location = New System.Drawing.Point(917, 687)
        Me.ID_71.Name = "ID_71"
        Me.ID_71.Size = New System.Drawing.Size(35, 13)
        Me.ID_71.TabIndex = 642
        Me.ID_71.Text = "ID:71"
        '
        'ID_70
        '
        Me.ID_70.AutoSize = True
        Me.ID_70.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_70.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_70.Location = New System.Drawing.Point(917, 659)
        Me.ID_70.Name = "ID_70"
        Me.ID_70.Size = New System.Drawing.Size(35, 13)
        Me.ID_70.TabIndex = 641
        Me.ID_70.Text = "ID:70"
        '
        'ID_69
        '
        Me.ID_69.AutoSize = True
        Me.ID_69.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_69.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_69.Location = New System.Drawing.Point(917, 631)
        Me.ID_69.Name = "ID_69"
        Me.ID_69.Size = New System.Drawing.Size(35, 13)
        Me.ID_69.TabIndex = 640
        Me.ID_69.Text = "ID:69"
        '
        'ID_68
        '
        Me.ID_68.AutoSize = True
        Me.ID_68.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_68.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_68.Location = New System.Drawing.Point(917, 603)
        Me.ID_68.Name = "ID_68"
        Me.ID_68.Size = New System.Drawing.Size(35, 13)
        Me.ID_68.TabIndex = 639
        Me.ID_68.Text = "ID:68"
        '
        'ID_67
        '
        Me.ID_67.AutoSize = True
        Me.ID_67.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_67.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_67.Location = New System.Drawing.Point(917, 575)
        Me.ID_67.Name = "ID_67"
        Me.ID_67.Size = New System.Drawing.Size(35, 13)
        Me.ID_67.TabIndex = 638
        Me.ID_67.Text = "ID:67"
        '
        'ID_66
        '
        Me.ID_66.AutoSize = True
        Me.ID_66.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_66.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_66.Location = New System.Drawing.Point(917, 547)
        Me.ID_66.Name = "ID_66"
        Me.ID_66.Size = New System.Drawing.Size(35, 13)
        Me.ID_66.TabIndex = 637
        Me.ID_66.Text = "ID:66"
        '
        'ID_65
        '
        Me.ID_65.AutoSize = True
        Me.ID_65.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_65.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_65.Location = New System.Drawing.Point(917, 519)
        Me.ID_65.Name = "ID_65"
        Me.ID_65.Size = New System.Drawing.Size(35, 13)
        Me.ID_65.TabIndex = 636
        Me.ID_65.Text = "ID:65"
        '
        'ID_64
        '
        Me.ID_64.AutoSize = True
        Me.ID_64.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_64.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_64.Location = New System.Drawing.Point(917, 491)
        Me.ID_64.Name = "ID_64"
        Me.ID_64.Size = New System.Drawing.Size(35, 13)
        Me.ID_64.TabIndex = 635
        Me.ID_64.Text = "ID:64"
        '
        'ID_63
        '
        Me.ID_63.AutoSize = True
        Me.ID_63.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_63.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_63.Location = New System.Drawing.Point(917, 463)
        Me.ID_63.Name = "ID_63"
        Me.ID_63.Size = New System.Drawing.Size(35, 13)
        Me.ID_63.TabIndex = 634
        Me.ID_63.Text = "ID:63"
        '
        'ID_62
        '
        Me.ID_62.AutoSize = True
        Me.ID_62.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_62.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_62.Location = New System.Drawing.Point(917, 435)
        Me.ID_62.Name = "ID_62"
        Me.ID_62.Size = New System.Drawing.Size(35, 13)
        Me.ID_62.TabIndex = 633
        Me.ID_62.Text = "ID:62"
        '
        'ID_61
        '
        Me.ID_61.AutoSize = True
        Me.ID_61.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_61.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_61.Location = New System.Drawing.Point(917, 407)
        Me.ID_61.Name = "ID_61"
        Me.ID_61.Size = New System.Drawing.Size(35, 13)
        Me.ID_61.TabIndex = 632
        Me.ID_61.Text = "ID:61"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(1046, 379)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(46, 12)
        Me.Label60.TabIndex = 631
        Me.Label60.Text = "RPM Set"
        '
        'AlarmCode_62
        '
        Me.AlarmCode_62.Location = New System.Drawing.Point(1195, 426)
        Me.AlarmCode_62.Name = "AlarmCode_62"
        Me.AlarmCode_62.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_62.TabIndex = 630
        Me.AlarmCode_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(981, 379)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(32, 12)
        Me.Label61.TabIndex = 629
        Me.Label61.Text = "Status"
        '
        'Status_61
        '
        Me.Status_61.Location = New System.Drawing.Point(958, 398)
        Me.Status_61.Name = "Status_61"
        Me.Status_61.Size = New System.Drawing.Size(55, 22)
        Me.Status_61.TabIndex = 628
        Me.Status_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_62
        '
        Me.RPM_62.Location = New System.Drawing.Point(1116, 426)
        Me.RPM_62.Name = "RPM_62"
        Me.RPM_62.Size = New System.Drawing.Size(55, 22)
        Me.RPM_62.TabIndex = 627
        Me.RPM_62.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_61
        '
        Me.RPM_61.Location = New System.Drawing.Point(1116, 398)
        Me.RPM_61.Name = "RPM_61"
        Me.RPM_61.Size = New System.Drawing.Size(55, 22)
        Me.RPM_61.TabIndex = 626
        Me.RPM_61.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_60
        '
        Me.Status_60.Location = New System.Drawing.Point(609, 708)
        Me.Status_60.Name = "Status_60"
        Me.Status_60.Size = New System.Drawing.Size(55, 22)
        Me.Status_60.TabIndex = 625
        Me.Status_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_60
        '
        Me.RPMS_60.Location = New System.Drawing.Point(688, 708)
        Me.RPMS_60.Name = "RPMS_60"
        Me.RPMS_60.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_60.TabIndex = 624
        Me.RPMS_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_60
        '
        Me.AlarmCode_60.Location = New System.Drawing.Point(846, 708)
        Me.AlarmCode_60.Name = "AlarmCode_60"
        Me.AlarmCode_60.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_60.TabIndex = 623
        Me.AlarmCode_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_60
        '
        Me.RPM_60.Location = New System.Drawing.Point(767, 708)
        Me.RPM_60.Name = "RPM_60"
        Me.RPM_60.Size = New System.Drawing.Size(55, 22)
        Me.RPM_60.TabIndex = 622
        Me.RPM_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_59
        '
        Me.Status_59.Location = New System.Drawing.Point(609, 680)
        Me.Status_59.Name = "Status_59"
        Me.Status_59.Size = New System.Drawing.Size(55, 22)
        Me.Status_59.TabIndex = 621
        Me.Status_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_59
        '
        Me.RPMS_59.Location = New System.Drawing.Point(688, 680)
        Me.RPMS_59.Name = "RPMS_59"
        Me.RPMS_59.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_59.TabIndex = 620
        Me.RPMS_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_59
        '
        Me.AlarmCode_59.Location = New System.Drawing.Point(846, 680)
        Me.AlarmCode_59.Name = "AlarmCode_59"
        Me.AlarmCode_59.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_59.TabIndex = 619
        Me.AlarmCode_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_59
        '
        Me.RPM_59.Location = New System.Drawing.Point(767, 680)
        Me.RPM_59.Name = "RPM_59"
        Me.RPM_59.Size = New System.Drawing.Size(55, 22)
        Me.RPM_59.TabIndex = 618
        Me.RPM_59.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_58
        '
        Me.Status_58.Location = New System.Drawing.Point(609, 652)
        Me.Status_58.Name = "Status_58"
        Me.Status_58.Size = New System.Drawing.Size(55, 22)
        Me.Status_58.TabIndex = 617
        Me.Status_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_58
        '
        Me.RPMS_58.Location = New System.Drawing.Point(688, 652)
        Me.RPMS_58.Name = "RPMS_58"
        Me.RPMS_58.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_58.TabIndex = 616
        Me.RPMS_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_58
        '
        Me.AlarmCode_58.Location = New System.Drawing.Point(846, 652)
        Me.AlarmCode_58.Name = "AlarmCode_58"
        Me.AlarmCode_58.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_58.TabIndex = 615
        Me.AlarmCode_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_58
        '
        Me.RPM_58.Location = New System.Drawing.Point(767, 652)
        Me.RPM_58.Name = "RPM_58"
        Me.RPM_58.Size = New System.Drawing.Size(55, 22)
        Me.RPM_58.TabIndex = 614
        Me.RPM_58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_57
        '
        Me.Status_57.Location = New System.Drawing.Point(609, 624)
        Me.Status_57.Name = "Status_57"
        Me.Status_57.Size = New System.Drawing.Size(55, 22)
        Me.Status_57.TabIndex = 613
        Me.Status_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_57
        '
        Me.RPMS_57.Location = New System.Drawing.Point(688, 624)
        Me.RPMS_57.Name = "RPMS_57"
        Me.RPMS_57.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_57.TabIndex = 612
        Me.RPMS_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_57
        '
        Me.AlarmCode_57.Location = New System.Drawing.Point(846, 624)
        Me.AlarmCode_57.Name = "AlarmCode_57"
        Me.AlarmCode_57.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_57.TabIndex = 611
        Me.AlarmCode_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_57
        '
        Me.RPM_57.Location = New System.Drawing.Point(767, 624)
        Me.RPM_57.Name = "RPM_57"
        Me.RPM_57.Size = New System.Drawing.Size(55, 22)
        Me.RPM_57.TabIndex = 610
        Me.RPM_57.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_56
        '
        Me.Status_56.Location = New System.Drawing.Point(609, 596)
        Me.Status_56.Name = "Status_56"
        Me.Status_56.Size = New System.Drawing.Size(55, 22)
        Me.Status_56.TabIndex = 609
        Me.Status_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_56
        '
        Me.RPMS_56.Location = New System.Drawing.Point(688, 596)
        Me.RPMS_56.Name = "RPMS_56"
        Me.RPMS_56.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_56.TabIndex = 608
        Me.RPMS_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_56
        '
        Me.AlarmCode_56.Location = New System.Drawing.Point(846, 596)
        Me.AlarmCode_56.Name = "AlarmCode_56"
        Me.AlarmCode_56.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_56.TabIndex = 607
        Me.AlarmCode_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_56
        '
        Me.RPM_56.Location = New System.Drawing.Point(767, 596)
        Me.RPM_56.Name = "RPM_56"
        Me.RPM_56.Size = New System.Drawing.Size(55, 22)
        Me.RPM_56.TabIndex = 606
        Me.RPM_56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_55
        '
        Me.Status_55.Location = New System.Drawing.Point(609, 568)
        Me.Status_55.Name = "Status_55"
        Me.Status_55.Size = New System.Drawing.Size(55, 22)
        Me.Status_55.TabIndex = 605
        Me.Status_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_55
        '
        Me.RPMS_55.Location = New System.Drawing.Point(688, 568)
        Me.RPMS_55.Name = "RPMS_55"
        Me.RPMS_55.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_55.TabIndex = 604
        Me.RPMS_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_55
        '
        Me.AlarmCode_55.Location = New System.Drawing.Point(846, 568)
        Me.AlarmCode_55.Name = "AlarmCode_55"
        Me.AlarmCode_55.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_55.TabIndex = 603
        Me.AlarmCode_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_55
        '
        Me.RPM_55.Location = New System.Drawing.Point(767, 568)
        Me.RPM_55.Name = "RPM_55"
        Me.RPM_55.Size = New System.Drawing.Size(55, 22)
        Me.RPM_55.TabIndex = 602
        Me.RPM_55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_54
        '
        Me.Status_54.Location = New System.Drawing.Point(609, 540)
        Me.Status_54.Name = "Status_54"
        Me.Status_54.Size = New System.Drawing.Size(55, 22)
        Me.Status_54.TabIndex = 601
        Me.Status_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_54
        '
        Me.RPMS_54.Location = New System.Drawing.Point(688, 540)
        Me.RPMS_54.Name = "RPMS_54"
        Me.RPMS_54.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_54.TabIndex = 600
        Me.RPMS_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_54
        '
        Me.AlarmCode_54.Location = New System.Drawing.Point(846, 540)
        Me.AlarmCode_54.Name = "AlarmCode_54"
        Me.AlarmCode_54.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_54.TabIndex = 599
        Me.AlarmCode_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_54
        '
        Me.RPM_54.Location = New System.Drawing.Point(767, 540)
        Me.RPM_54.Name = "RPM_54"
        Me.RPM_54.Size = New System.Drawing.Size(55, 22)
        Me.RPM_54.TabIndex = 598
        Me.RPM_54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_53
        '
        Me.Status_53.Location = New System.Drawing.Point(609, 512)
        Me.Status_53.Name = "Status_53"
        Me.Status_53.Size = New System.Drawing.Size(55, 22)
        Me.Status_53.TabIndex = 597
        Me.Status_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_53
        '
        Me.RPMS_53.Location = New System.Drawing.Point(688, 512)
        Me.RPMS_53.Name = "RPMS_53"
        Me.RPMS_53.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_53.TabIndex = 596
        Me.RPMS_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_53
        '
        Me.AlarmCode_53.Location = New System.Drawing.Point(846, 512)
        Me.AlarmCode_53.Name = "AlarmCode_53"
        Me.AlarmCode_53.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_53.TabIndex = 595
        Me.AlarmCode_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_53
        '
        Me.RPM_53.Location = New System.Drawing.Point(767, 512)
        Me.RPM_53.Name = "RPM_53"
        Me.RPM_53.Size = New System.Drawing.Size(55, 22)
        Me.RPM_53.TabIndex = 594
        Me.RPM_53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_52
        '
        Me.Status_52.Location = New System.Drawing.Point(609, 484)
        Me.Status_52.Name = "Status_52"
        Me.Status_52.Size = New System.Drawing.Size(55, 22)
        Me.Status_52.TabIndex = 593
        Me.Status_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_52
        '
        Me.RPMS_52.Location = New System.Drawing.Point(688, 484)
        Me.RPMS_52.Name = "RPMS_52"
        Me.RPMS_52.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_52.TabIndex = 592
        Me.RPMS_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_52
        '
        Me.AlarmCode_52.Location = New System.Drawing.Point(846, 484)
        Me.AlarmCode_52.Name = "AlarmCode_52"
        Me.AlarmCode_52.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_52.TabIndex = 591
        Me.AlarmCode_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_52
        '
        Me.RPM_52.Location = New System.Drawing.Point(767, 484)
        Me.RPM_52.Name = "RPM_52"
        Me.RPM_52.Size = New System.Drawing.Size(55, 22)
        Me.RPM_52.TabIndex = 590
        Me.RPM_52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_51
        '
        Me.Status_51.Location = New System.Drawing.Point(609, 456)
        Me.Status_51.Name = "Status_51"
        Me.Status_51.Size = New System.Drawing.Size(55, 22)
        Me.Status_51.TabIndex = 589
        Me.Status_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_51
        '
        Me.RPMS_51.Location = New System.Drawing.Point(688, 456)
        Me.RPMS_51.Name = "RPMS_51"
        Me.RPMS_51.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_51.TabIndex = 588
        Me.RPMS_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_51
        '
        Me.AlarmCode_51.Location = New System.Drawing.Point(846, 456)
        Me.AlarmCode_51.Name = "AlarmCode_51"
        Me.AlarmCode_51.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_51.TabIndex = 587
        Me.AlarmCode_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_51
        '
        Me.RPM_51.Location = New System.Drawing.Point(767, 456)
        Me.RPM_51.Name = "RPM_51"
        Me.RPM_51.Size = New System.Drawing.Size(55, 22)
        Me.RPM_51.TabIndex = 586
        Me.RPM_51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(839, 379)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(62, 12)
        Me.Label62.TabIndex = 585
        Me.Label62.Text = "Alarm Code"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(793, 379)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(29, 12)
        Me.Label63.TabIndex = 584
        Me.Label63.Text = "RPM"
        '
        'Status_50
        '
        Me.Status_50.Location = New System.Drawing.Point(609, 426)
        Me.Status_50.Name = "Status_50"
        Me.Status_50.Size = New System.Drawing.Size(55, 22)
        Me.Status_50.TabIndex = 583
        Me.Status_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_50
        '
        Me.RPMS_50.Location = New System.Drawing.Point(688, 426)
        Me.RPMS_50.Name = "RPMS_50"
        Me.RPMS_50.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_50.TabIndex = 582
        Me.RPMS_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_49
        '
        Me.RPMS_49.Location = New System.Drawing.Point(688, 398)
        Me.RPMS_49.Name = "RPMS_49"
        Me.RPMS_49.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_49.TabIndex = 581
        Me.RPMS_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_49
        '
        Me.AlarmCode_49.Location = New System.Drawing.Point(846, 398)
        Me.AlarmCode_49.Name = "AlarmCode_49"
        Me.AlarmCode_49.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_49.TabIndex = 580
        Me.AlarmCode_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID_60
        '
        Me.ID_60.AutoSize = True
        Me.ID_60.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_60.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_60.Location = New System.Drawing.Point(568, 715)
        Me.ID_60.Name = "ID_60"
        Me.ID_60.Size = New System.Drawing.Size(35, 13)
        Me.ID_60.TabIndex = 579
        Me.ID_60.Text = "ID:60"
        '
        'ID_59
        '
        Me.ID_59.AutoSize = True
        Me.ID_59.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_59.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_59.Location = New System.Drawing.Point(568, 687)
        Me.ID_59.Name = "ID_59"
        Me.ID_59.Size = New System.Drawing.Size(35, 13)
        Me.ID_59.TabIndex = 578
        Me.ID_59.Text = "ID:59"
        '
        'ID_58
        '
        Me.ID_58.AutoSize = True
        Me.ID_58.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_58.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_58.Location = New System.Drawing.Point(568, 659)
        Me.ID_58.Name = "ID_58"
        Me.ID_58.Size = New System.Drawing.Size(35, 13)
        Me.ID_58.TabIndex = 577
        Me.ID_58.Text = "ID:58"
        '
        'ID_57
        '
        Me.ID_57.AutoSize = True
        Me.ID_57.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_57.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_57.Location = New System.Drawing.Point(568, 631)
        Me.ID_57.Name = "ID_57"
        Me.ID_57.Size = New System.Drawing.Size(35, 13)
        Me.ID_57.TabIndex = 576
        Me.ID_57.Text = "ID:57"
        '
        'ID_56
        '
        Me.ID_56.AutoSize = True
        Me.ID_56.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_56.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_56.Location = New System.Drawing.Point(568, 603)
        Me.ID_56.Name = "ID_56"
        Me.ID_56.Size = New System.Drawing.Size(35, 13)
        Me.ID_56.TabIndex = 575
        Me.ID_56.Text = "ID:56"
        '
        'ID_55
        '
        Me.ID_55.AutoSize = True
        Me.ID_55.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_55.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_55.Location = New System.Drawing.Point(568, 575)
        Me.ID_55.Name = "ID_55"
        Me.ID_55.Size = New System.Drawing.Size(35, 13)
        Me.ID_55.TabIndex = 574
        Me.ID_55.Text = "ID:55"
        '
        'ID_54
        '
        Me.ID_54.AutoSize = True
        Me.ID_54.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_54.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_54.Location = New System.Drawing.Point(568, 547)
        Me.ID_54.Name = "ID_54"
        Me.ID_54.Size = New System.Drawing.Size(35, 13)
        Me.ID_54.TabIndex = 573
        Me.ID_54.Text = "ID:54"
        '
        'ID_53
        '
        Me.ID_53.AutoSize = True
        Me.ID_53.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_53.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_53.Location = New System.Drawing.Point(568, 519)
        Me.ID_53.Name = "ID_53"
        Me.ID_53.Size = New System.Drawing.Size(35, 13)
        Me.ID_53.TabIndex = 572
        Me.ID_53.Text = "ID:53"
        '
        'ID_52
        '
        Me.ID_52.AutoSize = True
        Me.ID_52.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_52.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_52.Location = New System.Drawing.Point(568, 491)
        Me.ID_52.Name = "ID_52"
        Me.ID_52.Size = New System.Drawing.Size(35, 13)
        Me.ID_52.TabIndex = 571
        Me.ID_52.Text = "ID:52"
        '
        'ID_51
        '
        Me.ID_51.AutoSize = True
        Me.ID_51.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_51.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_51.Location = New System.Drawing.Point(568, 463)
        Me.ID_51.Name = "ID_51"
        Me.ID_51.Size = New System.Drawing.Size(35, 13)
        Me.ID_51.TabIndex = 570
        Me.ID_51.Text = "ID:51"
        '
        'ID_50
        '
        Me.ID_50.AutoSize = True
        Me.ID_50.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_50.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_50.Location = New System.Drawing.Point(568, 435)
        Me.ID_50.Name = "ID_50"
        Me.ID_50.Size = New System.Drawing.Size(35, 13)
        Me.ID_50.TabIndex = 569
        Me.ID_50.Text = "ID:50"
        '
        'ID_49
        '
        Me.ID_49.AutoSize = True
        Me.ID_49.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_49.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_49.Location = New System.Drawing.Point(568, 407)
        Me.ID_49.Name = "ID_49"
        Me.ID_49.Size = New System.Drawing.Size(35, 13)
        Me.ID_49.TabIndex = 568
        Me.ID_49.Text = "ID:49"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(697, 379)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(46, 12)
        Me.Label76.TabIndex = 567
        Me.Label76.Text = "RPM Set"
        '
        'AlarmCode_50
        '
        Me.AlarmCode_50.Location = New System.Drawing.Point(846, 426)
        Me.AlarmCode_50.Name = "AlarmCode_50"
        Me.AlarmCode_50.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_50.TabIndex = 566
        Me.AlarmCode_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(632, 379)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(32, 12)
        Me.Label77.TabIndex = 565
        Me.Label77.Text = "Status"
        '
        'Status_49
        '
        Me.Status_49.Location = New System.Drawing.Point(609, 398)
        Me.Status_49.Name = "Status_49"
        Me.Status_49.Size = New System.Drawing.Size(55, 22)
        Me.Status_49.TabIndex = 564
        Me.Status_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_50
        '
        Me.RPM_50.Location = New System.Drawing.Point(767, 426)
        Me.RPM_50.Name = "RPM_50"
        Me.RPM_50.Size = New System.Drawing.Size(55, 22)
        Me.RPM_50.TabIndex = 563
        Me.RPM_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_49
        '
        Me.RPM_49.Location = New System.Drawing.Point(767, 398)
        Me.RPM_49.Name = "RPM_49"
        Me.RPM_49.Size = New System.Drawing.Size(55, 22)
        Me.RPM_49.TabIndex = 562
        Me.RPM_49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_48
        '
        Me.Status_48.Location = New System.Drawing.Point(264, 708)
        Me.Status_48.Name = "Status_48"
        Me.Status_48.Size = New System.Drawing.Size(55, 22)
        Me.Status_48.TabIndex = 561
        Me.Status_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_48
        '
        Me.RPMS_48.Location = New System.Drawing.Point(343, 708)
        Me.RPMS_48.Name = "RPMS_48"
        Me.RPMS_48.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_48.TabIndex = 560
        Me.RPMS_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_48
        '
        Me.AlarmCode_48.Location = New System.Drawing.Point(501, 708)
        Me.AlarmCode_48.Name = "AlarmCode_48"
        Me.AlarmCode_48.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_48.TabIndex = 559
        Me.AlarmCode_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_48
        '
        Me.RPM_48.Location = New System.Drawing.Point(422, 708)
        Me.RPM_48.Name = "RPM_48"
        Me.RPM_48.Size = New System.Drawing.Size(55, 22)
        Me.RPM_48.TabIndex = 558
        Me.RPM_48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_47
        '
        Me.Status_47.Location = New System.Drawing.Point(264, 680)
        Me.Status_47.Name = "Status_47"
        Me.Status_47.Size = New System.Drawing.Size(55, 22)
        Me.Status_47.TabIndex = 557
        Me.Status_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_47
        '
        Me.RPMS_47.Location = New System.Drawing.Point(343, 680)
        Me.RPMS_47.Name = "RPMS_47"
        Me.RPMS_47.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_47.TabIndex = 556
        Me.RPMS_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_47
        '
        Me.AlarmCode_47.Location = New System.Drawing.Point(501, 680)
        Me.AlarmCode_47.Name = "AlarmCode_47"
        Me.AlarmCode_47.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_47.TabIndex = 555
        Me.AlarmCode_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_47
        '
        Me.RPM_47.Location = New System.Drawing.Point(422, 680)
        Me.RPM_47.Name = "RPM_47"
        Me.RPM_47.Size = New System.Drawing.Size(55, 22)
        Me.RPM_47.TabIndex = 554
        Me.RPM_47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_46
        '
        Me.Status_46.Location = New System.Drawing.Point(264, 652)
        Me.Status_46.Name = "Status_46"
        Me.Status_46.Size = New System.Drawing.Size(55, 22)
        Me.Status_46.TabIndex = 553
        Me.Status_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_46
        '
        Me.RPMS_46.Location = New System.Drawing.Point(343, 652)
        Me.RPMS_46.Name = "RPMS_46"
        Me.RPMS_46.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_46.TabIndex = 552
        Me.RPMS_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_46
        '
        Me.AlarmCode_46.Location = New System.Drawing.Point(501, 652)
        Me.AlarmCode_46.Name = "AlarmCode_46"
        Me.AlarmCode_46.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_46.TabIndex = 551
        Me.AlarmCode_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_46
        '
        Me.RPM_46.Location = New System.Drawing.Point(422, 652)
        Me.RPM_46.Name = "RPM_46"
        Me.RPM_46.Size = New System.Drawing.Size(55, 22)
        Me.RPM_46.TabIndex = 550
        Me.RPM_46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_45
        '
        Me.Status_45.Location = New System.Drawing.Point(264, 624)
        Me.Status_45.Name = "Status_45"
        Me.Status_45.Size = New System.Drawing.Size(55, 22)
        Me.Status_45.TabIndex = 549
        Me.Status_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_45
        '
        Me.RPMS_45.Location = New System.Drawing.Point(343, 624)
        Me.RPMS_45.Name = "RPMS_45"
        Me.RPMS_45.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_45.TabIndex = 548
        Me.RPMS_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_45
        '
        Me.AlarmCode_45.Location = New System.Drawing.Point(501, 624)
        Me.AlarmCode_45.Name = "AlarmCode_45"
        Me.AlarmCode_45.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_45.TabIndex = 547
        Me.AlarmCode_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_45
        '
        Me.RPM_45.Location = New System.Drawing.Point(422, 624)
        Me.RPM_45.Name = "RPM_45"
        Me.RPM_45.Size = New System.Drawing.Size(55, 22)
        Me.RPM_45.TabIndex = 546
        Me.RPM_45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_44
        '
        Me.Status_44.Location = New System.Drawing.Point(264, 596)
        Me.Status_44.Name = "Status_44"
        Me.Status_44.Size = New System.Drawing.Size(55, 22)
        Me.Status_44.TabIndex = 545
        Me.Status_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_44
        '
        Me.RPMS_44.Location = New System.Drawing.Point(343, 596)
        Me.RPMS_44.Name = "RPMS_44"
        Me.RPMS_44.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_44.TabIndex = 544
        Me.RPMS_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_44
        '
        Me.AlarmCode_44.Location = New System.Drawing.Point(501, 596)
        Me.AlarmCode_44.Name = "AlarmCode_44"
        Me.AlarmCode_44.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_44.TabIndex = 543
        Me.AlarmCode_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_44
        '
        Me.RPM_44.Location = New System.Drawing.Point(422, 596)
        Me.RPM_44.Name = "RPM_44"
        Me.RPM_44.Size = New System.Drawing.Size(55, 22)
        Me.RPM_44.TabIndex = 542
        Me.RPM_44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_43
        '
        Me.Status_43.Location = New System.Drawing.Point(264, 568)
        Me.Status_43.Name = "Status_43"
        Me.Status_43.Size = New System.Drawing.Size(55, 22)
        Me.Status_43.TabIndex = 541
        Me.Status_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_43
        '
        Me.RPMS_43.Location = New System.Drawing.Point(343, 568)
        Me.RPMS_43.Name = "RPMS_43"
        Me.RPMS_43.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_43.TabIndex = 540
        Me.RPMS_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_43
        '
        Me.AlarmCode_43.Location = New System.Drawing.Point(501, 568)
        Me.AlarmCode_43.Name = "AlarmCode_43"
        Me.AlarmCode_43.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_43.TabIndex = 539
        Me.AlarmCode_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_43
        '
        Me.RPM_43.Location = New System.Drawing.Point(422, 568)
        Me.RPM_43.Name = "RPM_43"
        Me.RPM_43.Size = New System.Drawing.Size(55, 22)
        Me.RPM_43.TabIndex = 538
        Me.RPM_43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_42
        '
        Me.Status_42.Location = New System.Drawing.Point(264, 540)
        Me.Status_42.Name = "Status_42"
        Me.Status_42.Size = New System.Drawing.Size(55, 22)
        Me.Status_42.TabIndex = 537
        Me.Status_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_42
        '
        Me.RPMS_42.Location = New System.Drawing.Point(343, 540)
        Me.RPMS_42.Name = "RPMS_42"
        Me.RPMS_42.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_42.TabIndex = 536
        Me.RPMS_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_42
        '
        Me.AlarmCode_42.Location = New System.Drawing.Point(501, 540)
        Me.AlarmCode_42.Name = "AlarmCode_42"
        Me.AlarmCode_42.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_42.TabIndex = 535
        Me.AlarmCode_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_42
        '
        Me.RPM_42.Location = New System.Drawing.Point(422, 540)
        Me.RPM_42.Name = "RPM_42"
        Me.RPM_42.Size = New System.Drawing.Size(55, 22)
        Me.RPM_42.TabIndex = 534
        Me.RPM_42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_41
        '
        Me.Status_41.Location = New System.Drawing.Point(264, 512)
        Me.Status_41.Name = "Status_41"
        Me.Status_41.Size = New System.Drawing.Size(55, 22)
        Me.Status_41.TabIndex = 533
        Me.Status_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_41
        '
        Me.RPMS_41.Location = New System.Drawing.Point(343, 512)
        Me.RPMS_41.Name = "RPMS_41"
        Me.RPMS_41.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_41.TabIndex = 532
        Me.RPMS_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_41
        '
        Me.AlarmCode_41.Location = New System.Drawing.Point(501, 512)
        Me.AlarmCode_41.Name = "AlarmCode_41"
        Me.AlarmCode_41.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_41.TabIndex = 531
        Me.AlarmCode_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_41
        '
        Me.RPM_41.Location = New System.Drawing.Point(422, 512)
        Me.RPM_41.Name = "RPM_41"
        Me.RPM_41.Size = New System.Drawing.Size(55, 22)
        Me.RPM_41.TabIndex = 530
        Me.RPM_41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_40
        '
        Me.Status_40.Location = New System.Drawing.Point(264, 484)
        Me.Status_40.Name = "Status_40"
        Me.Status_40.Size = New System.Drawing.Size(55, 22)
        Me.Status_40.TabIndex = 529
        Me.Status_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_40
        '
        Me.RPMS_40.Location = New System.Drawing.Point(343, 484)
        Me.RPMS_40.Name = "RPMS_40"
        Me.RPMS_40.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_40.TabIndex = 528
        Me.RPMS_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_40
        '
        Me.AlarmCode_40.Location = New System.Drawing.Point(501, 484)
        Me.AlarmCode_40.Name = "AlarmCode_40"
        Me.AlarmCode_40.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_40.TabIndex = 527
        Me.AlarmCode_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_40
        '
        Me.RPM_40.Location = New System.Drawing.Point(422, 484)
        Me.RPM_40.Name = "RPM_40"
        Me.RPM_40.Size = New System.Drawing.Size(55, 22)
        Me.RPM_40.TabIndex = 526
        Me.RPM_40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Status_39
        '
        Me.Status_39.Location = New System.Drawing.Point(264, 456)
        Me.Status_39.Name = "Status_39"
        Me.Status_39.Size = New System.Drawing.Size(55, 22)
        Me.Status_39.TabIndex = 525
        Me.Status_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_39
        '
        Me.RPMS_39.Location = New System.Drawing.Point(343, 456)
        Me.RPMS_39.Name = "RPMS_39"
        Me.RPMS_39.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_39.TabIndex = 524
        Me.RPMS_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_39
        '
        Me.AlarmCode_39.Location = New System.Drawing.Point(501, 456)
        Me.AlarmCode_39.Name = "AlarmCode_39"
        Me.AlarmCode_39.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_39.TabIndex = 523
        Me.AlarmCode_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_39
        '
        Me.RPM_39.Location = New System.Drawing.Point(422, 456)
        Me.RPM_39.Name = "RPM_39"
        Me.RPM_39.Size = New System.Drawing.Size(55, 22)
        Me.RPM_39.TabIndex = 522
        Me.RPM_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(494, 379)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(62, 12)
        Me.Label78.TabIndex = 521
        Me.Label78.Text = "Alarm Code"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(448, 379)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(29, 12)
        Me.Label79.TabIndex = 520
        Me.Label79.Text = "RPM"
        '
        'Status_38
        '
        Me.Status_38.Location = New System.Drawing.Point(264, 426)
        Me.Status_38.Name = "Status_38"
        Me.Status_38.Size = New System.Drawing.Size(55, 22)
        Me.Status_38.TabIndex = 519
        Me.Status_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_38
        '
        Me.RPMS_38.Location = New System.Drawing.Point(343, 426)
        Me.RPMS_38.Name = "RPMS_38"
        Me.RPMS_38.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_38.TabIndex = 518
        Me.RPMS_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPMS_37
        '
        Me.RPMS_37.Location = New System.Drawing.Point(343, 398)
        Me.RPMS_37.Name = "RPMS_37"
        Me.RPMS_37.Size = New System.Drawing.Size(55, 22)
        Me.RPMS_37.TabIndex = 517
        Me.RPMS_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AlarmCode_37
        '
        Me.AlarmCode_37.Location = New System.Drawing.Point(501, 398)
        Me.AlarmCode_37.Name = "AlarmCode_37"
        Me.AlarmCode_37.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_37.TabIndex = 516
        Me.AlarmCode_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ID_48
        '
        Me.ID_48.AutoSize = True
        Me.ID_48.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_48.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_48.Location = New System.Drawing.Point(223, 715)
        Me.ID_48.Name = "ID_48"
        Me.ID_48.Size = New System.Drawing.Size(35, 13)
        Me.ID_48.TabIndex = 515
        Me.ID_48.Text = "ID:48"
        '
        'ID_47
        '
        Me.ID_47.AutoSize = True
        Me.ID_47.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_47.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_47.Location = New System.Drawing.Point(223, 687)
        Me.ID_47.Name = "ID_47"
        Me.ID_47.Size = New System.Drawing.Size(35, 13)
        Me.ID_47.TabIndex = 514
        Me.ID_47.Text = "ID:47"
        '
        'ID_46
        '
        Me.ID_46.AutoSize = True
        Me.ID_46.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_46.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_46.Location = New System.Drawing.Point(223, 659)
        Me.ID_46.Name = "ID_46"
        Me.ID_46.Size = New System.Drawing.Size(35, 13)
        Me.ID_46.TabIndex = 513
        Me.ID_46.Text = "ID:46"
        '
        'ID_45
        '
        Me.ID_45.AutoSize = True
        Me.ID_45.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_45.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_45.Location = New System.Drawing.Point(223, 631)
        Me.ID_45.Name = "ID_45"
        Me.ID_45.Size = New System.Drawing.Size(35, 13)
        Me.ID_45.TabIndex = 512
        Me.ID_45.Text = "ID:45"
        '
        'ID_44
        '
        Me.ID_44.AutoSize = True
        Me.ID_44.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_44.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_44.Location = New System.Drawing.Point(223, 603)
        Me.ID_44.Name = "ID_44"
        Me.ID_44.Size = New System.Drawing.Size(35, 13)
        Me.ID_44.TabIndex = 511
        Me.ID_44.Text = "ID:44"
        '
        'ID_43
        '
        Me.ID_43.AutoSize = True
        Me.ID_43.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_43.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_43.Location = New System.Drawing.Point(223, 575)
        Me.ID_43.Name = "ID_43"
        Me.ID_43.Size = New System.Drawing.Size(35, 13)
        Me.ID_43.TabIndex = 510
        Me.ID_43.Text = "ID:43"
        '
        'ID_42
        '
        Me.ID_42.AutoSize = True
        Me.ID_42.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_42.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_42.Location = New System.Drawing.Point(223, 547)
        Me.ID_42.Name = "ID_42"
        Me.ID_42.Size = New System.Drawing.Size(35, 13)
        Me.ID_42.TabIndex = 509
        Me.ID_42.Text = "ID:42"
        '
        'ID_41
        '
        Me.ID_41.AutoSize = True
        Me.ID_41.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_41.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_41.Location = New System.Drawing.Point(223, 519)
        Me.ID_41.Name = "ID_41"
        Me.ID_41.Size = New System.Drawing.Size(35, 13)
        Me.ID_41.TabIndex = 508
        Me.ID_41.Text = "ID:41"
        '
        'ID_40
        '
        Me.ID_40.AutoSize = True
        Me.ID_40.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_40.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_40.Location = New System.Drawing.Point(223, 491)
        Me.ID_40.Name = "ID_40"
        Me.ID_40.Size = New System.Drawing.Size(35, 13)
        Me.ID_40.TabIndex = 507
        Me.ID_40.Text = "ID:40"
        '
        'ID_39
        '
        Me.ID_39.AutoSize = True
        Me.ID_39.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_39.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_39.Location = New System.Drawing.Point(223, 463)
        Me.ID_39.Name = "ID_39"
        Me.ID_39.Size = New System.Drawing.Size(35, 13)
        Me.ID_39.TabIndex = 506
        Me.ID_39.Text = "ID:39"
        '
        'ID_38
        '
        Me.ID_38.AutoSize = True
        Me.ID_38.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_38.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_38.Location = New System.Drawing.Point(223, 435)
        Me.ID_38.Name = "ID_38"
        Me.ID_38.Size = New System.Drawing.Size(35, 13)
        Me.ID_38.TabIndex = 505
        Me.ID_38.Text = "ID:38"
        '
        'ID_37
        '
        Me.ID_37.AutoSize = True
        Me.ID_37.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID_37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID_37.Location = New System.Drawing.Point(223, 407)
        Me.ID_37.Name = "ID_37"
        Me.ID_37.Size = New System.Drawing.Size(35, 13)
        Me.ID_37.TabIndex = 504
        Me.ID_37.Text = "ID:37"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(352, 379)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(46, 12)
        Me.Label92.TabIndex = 503
        Me.Label92.Text = "RPM Set"
        '
        'AlarmCode_38
        '
        Me.AlarmCode_38.Location = New System.Drawing.Point(501, 426)
        Me.AlarmCode_38.Name = "AlarmCode_38"
        Me.AlarmCode_38.Size = New System.Drawing.Size(55, 22)
        Me.AlarmCode_38.TabIndex = 502
        Me.AlarmCode_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(287, 379)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(32, 12)
        Me.Label93.TabIndex = 501
        Me.Label93.Text = "Status"
        '
        'Status_37
        '
        Me.Status_37.Location = New System.Drawing.Point(264, 398)
        Me.Status_37.Name = "Status_37"
        Me.Status_37.Size = New System.Drawing.Size(55, 22)
        Me.Status_37.TabIndex = 500
        Me.Status_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_38
        '
        Me.RPM_38.Location = New System.Drawing.Point(422, 426)
        Me.RPM_38.Name = "RPM_38"
        Me.RPM_38.Size = New System.Drawing.Size(55, 22)
        Me.RPM_38.TabIndex = 499
        Me.RPM_38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RPM_37
        '
        Me.RPM_37.Location = New System.Drawing.Point(422, 398)
        Me.RPM_37.Name = "RPM_37"
        Me.RPM_37.Size = New System.Drawing.Size(55, 22)
        Me.RPM_37.TabIndex = 498
        Me.RPM_37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(97, 294)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 23)
        Me.Button2.TabIndex = 690
        Me.Button2.Text = "讀取"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.Location = New System.Drawing.Point(97, 323)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(99, 22)
        Me.TextBox1.TabIndex = 691
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox2.Location = New System.Drawing.Point(96, 351)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(99, 22)
        Me.TextBox2.TabIndex = 692
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.SkyBlue
        Me.ClientSize = New System.Drawing.Size(1270, 744)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Status_72)
        Me.Controls.Add(Me.RPMS_72)
        Me.Controls.Add(Me.AlarmCode_72)
        Me.Controls.Add(Me.RPM_72)
        Me.Controls.Add(Me.Status_71)
        Me.Controls.Add(Me.RPMS_71)
        Me.Controls.Add(Me.AlarmCode_71)
        Me.Controls.Add(Me.RPM_71)
        Me.Controls.Add(Me.Status_70)
        Me.Controls.Add(Me.RPMS_70)
        Me.Controls.Add(Me.AlarmCode_70)
        Me.Controls.Add(Me.RPM_70)
        Me.Controls.Add(Me.Status_69)
        Me.Controls.Add(Me.RPMS_69)
        Me.Controls.Add(Me.AlarmCode_69)
        Me.Controls.Add(Me.RPM_69)
        Me.Controls.Add(Me.Status_68)
        Me.Controls.Add(Me.RPMS_68)
        Me.Controls.Add(Me.AlarmCode_68)
        Me.Controls.Add(Me.RPM_68)
        Me.Controls.Add(Me.Status_67)
        Me.Controls.Add(Me.RPMS_67)
        Me.Controls.Add(Me.AlarmCode_67)
        Me.Controls.Add(Me.RPM_67)
        Me.Controls.Add(Me.Status_66)
        Me.Controls.Add(Me.RPMS_66)
        Me.Controls.Add(Me.AlarmCode_66)
        Me.Controls.Add(Me.RPM_66)
        Me.Controls.Add(Me.Status_65)
        Me.Controls.Add(Me.RPMS_65)
        Me.Controls.Add(Me.AlarmCode_65)
        Me.Controls.Add(Me.RPM_65)
        Me.Controls.Add(Me.Status_64)
        Me.Controls.Add(Me.RPMS_64)
        Me.Controls.Add(Me.RPM_64)
        Me.Controls.Add(Me.AlarmCode_64)
        Me.Controls.Add(Me.Status_63)
        Me.Controls.Add(Me.RPMS_63)
        Me.Controls.Add(Me.AlarmCode_63)
        Me.Controls.Add(Me.RPM_63)
        Me.Controls.Add(Me.Label46)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.Status_62)
        Me.Controls.Add(Me.RPMS_62)
        Me.Controls.Add(Me.RPMS_61)
        Me.Controls.Add(Me.AlarmCode_61)
        Me.Controls.Add(Me.ID_72)
        Me.Controls.Add(Me.ID_71)
        Me.Controls.Add(Me.ID_70)
        Me.Controls.Add(Me.ID_69)
        Me.Controls.Add(Me.ID_68)
        Me.Controls.Add(Me.ID_67)
        Me.Controls.Add(Me.ID_66)
        Me.Controls.Add(Me.ID_65)
        Me.Controls.Add(Me.ID_64)
        Me.Controls.Add(Me.ID_63)
        Me.Controls.Add(Me.ID_62)
        Me.Controls.Add(Me.ID_61)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.AlarmCode_62)
        Me.Controls.Add(Me.Label61)
        Me.Controls.Add(Me.Status_61)
        Me.Controls.Add(Me.RPM_62)
        Me.Controls.Add(Me.RPM_61)
        Me.Controls.Add(Me.Status_60)
        Me.Controls.Add(Me.RPMS_60)
        Me.Controls.Add(Me.AlarmCode_60)
        Me.Controls.Add(Me.RPM_60)
        Me.Controls.Add(Me.Status_59)
        Me.Controls.Add(Me.RPMS_59)
        Me.Controls.Add(Me.AlarmCode_59)
        Me.Controls.Add(Me.RPM_59)
        Me.Controls.Add(Me.Status_58)
        Me.Controls.Add(Me.RPMS_58)
        Me.Controls.Add(Me.AlarmCode_58)
        Me.Controls.Add(Me.RPM_58)
        Me.Controls.Add(Me.Status_57)
        Me.Controls.Add(Me.RPMS_57)
        Me.Controls.Add(Me.AlarmCode_57)
        Me.Controls.Add(Me.RPM_57)
        Me.Controls.Add(Me.Status_56)
        Me.Controls.Add(Me.RPMS_56)
        Me.Controls.Add(Me.AlarmCode_56)
        Me.Controls.Add(Me.RPM_56)
        Me.Controls.Add(Me.Status_55)
        Me.Controls.Add(Me.RPMS_55)
        Me.Controls.Add(Me.AlarmCode_55)
        Me.Controls.Add(Me.RPM_55)
        Me.Controls.Add(Me.Status_54)
        Me.Controls.Add(Me.RPMS_54)
        Me.Controls.Add(Me.AlarmCode_54)
        Me.Controls.Add(Me.RPM_54)
        Me.Controls.Add(Me.Status_53)
        Me.Controls.Add(Me.RPMS_53)
        Me.Controls.Add(Me.AlarmCode_53)
        Me.Controls.Add(Me.RPM_53)
        Me.Controls.Add(Me.Status_52)
        Me.Controls.Add(Me.RPMS_52)
        Me.Controls.Add(Me.AlarmCode_52)
        Me.Controls.Add(Me.RPM_52)
        Me.Controls.Add(Me.Status_51)
        Me.Controls.Add(Me.RPMS_51)
        Me.Controls.Add(Me.AlarmCode_51)
        Me.Controls.Add(Me.RPM_51)
        Me.Controls.Add(Me.Label62)
        Me.Controls.Add(Me.Label63)
        Me.Controls.Add(Me.Status_50)
        Me.Controls.Add(Me.RPMS_50)
        Me.Controls.Add(Me.RPMS_49)
        Me.Controls.Add(Me.AlarmCode_49)
        Me.Controls.Add(Me.ID_60)
        Me.Controls.Add(Me.ID_59)
        Me.Controls.Add(Me.ID_58)
        Me.Controls.Add(Me.ID_57)
        Me.Controls.Add(Me.ID_56)
        Me.Controls.Add(Me.ID_55)
        Me.Controls.Add(Me.ID_54)
        Me.Controls.Add(Me.ID_53)
        Me.Controls.Add(Me.ID_52)
        Me.Controls.Add(Me.ID_51)
        Me.Controls.Add(Me.ID_50)
        Me.Controls.Add(Me.ID_49)
        Me.Controls.Add(Me.Label76)
        Me.Controls.Add(Me.AlarmCode_50)
        Me.Controls.Add(Me.Label77)
        Me.Controls.Add(Me.Status_49)
        Me.Controls.Add(Me.RPM_50)
        Me.Controls.Add(Me.RPM_49)
        Me.Controls.Add(Me.Status_48)
        Me.Controls.Add(Me.RPMS_48)
        Me.Controls.Add(Me.AlarmCode_48)
        Me.Controls.Add(Me.RPM_48)
        Me.Controls.Add(Me.Status_47)
        Me.Controls.Add(Me.RPMS_47)
        Me.Controls.Add(Me.AlarmCode_47)
        Me.Controls.Add(Me.RPM_47)
        Me.Controls.Add(Me.Status_46)
        Me.Controls.Add(Me.RPMS_46)
        Me.Controls.Add(Me.AlarmCode_46)
        Me.Controls.Add(Me.RPM_46)
        Me.Controls.Add(Me.Status_45)
        Me.Controls.Add(Me.RPMS_45)
        Me.Controls.Add(Me.AlarmCode_45)
        Me.Controls.Add(Me.RPM_45)
        Me.Controls.Add(Me.Status_44)
        Me.Controls.Add(Me.RPMS_44)
        Me.Controls.Add(Me.AlarmCode_44)
        Me.Controls.Add(Me.RPM_44)
        Me.Controls.Add(Me.Status_43)
        Me.Controls.Add(Me.RPMS_43)
        Me.Controls.Add(Me.AlarmCode_43)
        Me.Controls.Add(Me.RPM_43)
        Me.Controls.Add(Me.Status_42)
        Me.Controls.Add(Me.RPMS_42)
        Me.Controls.Add(Me.AlarmCode_42)
        Me.Controls.Add(Me.RPM_42)
        Me.Controls.Add(Me.Status_41)
        Me.Controls.Add(Me.RPMS_41)
        Me.Controls.Add(Me.AlarmCode_41)
        Me.Controls.Add(Me.RPM_41)
        Me.Controls.Add(Me.Status_40)
        Me.Controls.Add(Me.RPMS_40)
        Me.Controls.Add(Me.AlarmCode_40)
        Me.Controls.Add(Me.RPM_40)
        Me.Controls.Add(Me.Status_39)
        Me.Controls.Add(Me.RPMS_39)
        Me.Controls.Add(Me.AlarmCode_39)
        Me.Controls.Add(Me.RPM_39)
        Me.Controls.Add(Me.Label78)
        Me.Controls.Add(Me.Label79)
        Me.Controls.Add(Me.Status_38)
        Me.Controls.Add(Me.RPMS_38)
        Me.Controls.Add(Me.RPMS_37)
        Me.Controls.Add(Me.AlarmCode_37)
        Me.Controls.Add(Me.ID_48)
        Me.Controls.Add(Me.ID_47)
        Me.Controls.Add(Me.ID_46)
        Me.Controls.Add(Me.ID_45)
        Me.Controls.Add(Me.ID_44)
        Me.Controls.Add(Me.ID_43)
        Me.Controls.Add(Me.ID_42)
        Me.Controls.Add(Me.ID_41)
        Me.Controls.Add(Me.ID_40)
        Me.Controls.Add(Me.ID_39)
        Me.Controls.Add(Me.ID_38)
        Me.Controls.Add(Me.ID_37)
        Me.Controls.Add(Me.Label92)
        Me.Controls.Add(Me.AlarmCode_38)
        Me.Controls.Add(Me.Label93)
        Me.Controls.Add(Me.Status_37)
        Me.Controls.Add(Me.RPM_38)
        Me.Controls.Add(Me.RPM_37)
        Me.Controls.Add(Me.Status_36)
        Me.Controls.Add(Me.RPMS_36)
        Me.Controls.Add(Me.AlarmCode_36)
        Me.Controls.Add(Me.RPM_36)
        Me.Controls.Add(Me.Status_35)
        Me.Controls.Add(Me.RPMS_35)
        Me.Controls.Add(Me.AlarmCode_35)
        Me.Controls.Add(Me.RPM_35)
        Me.Controls.Add(Me.Status_34)
        Me.Controls.Add(Me.RPMS_34)
        Me.Controls.Add(Me.AlarmCode_34)
        Me.Controls.Add(Me.RPM_34)
        Me.Controls.Add(Me.Status_33)
        Me.Controls.Add(Me.RPMS_33)
        Me.Controls.Add(Me.AlarmCode_33)
        Me.Controls.Add(Me.RPM_33)
        Me.Controls.Add(Me.Status_32)
        Me.Controls.Add(Me.RPMS_32)
        Me.Controls.Add(Me.AlarmCode_32)
        Me.Controls.Add(Me.RPM_32)
        Me.Controls.Add(Me.Status_31)
        Me.Controls.Add(Me.RPMS_31)
        Me.Controls.Add(Me.AlarmCode_31)
        Me.Controls.Add(Me.RPM_31)
        Me.Controls.Add(Me.Status_30)
        Me.Controls.Add(Me.RPMS_30)
        Me.Controls.Add(Me.AlarmCode_30)
        Me.Controls.Add(Me.RPM_30)
        Me.Controls.Add(Me.Status_29)
        Me.Controls.Add(Me.RPMS_29)
        Me.Controls.Add(Me.AlarmCode_29)
        Me.Controls.Add(Me.RPM_29)
        Me.Controls.Add(Me.Status_28)
        Me.Controls.Add(Me.RPMS_28)
        Me.Controls.Add(Me.AlarmCode_28)
        Me.Controls.Add(Me.RPM_28)
        Me.Controls.Add(Me.Status_27)
        Me.Controls.Add(Me.RPMS_27)
        Me.Controls.Add(Me.AlarmCode_27)
        Me.Controls.Add(Me.RPM_27)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Status_26)
        Me.Controls.Add(Me.RPMS_26)
        Me.Controls.Add(Me.RPMS_25)
        Me.Controls.Add(Me.AlarmCode_25)
        Me.Controls.Add(Me.ID_36)
        Me.Controls.Add(Me.ID_35)
        Me.Controls.Add(Me.ID_34)
        Me.Controls.Add(Me.ID_33)
        Me.Controls.Add(Me.ID_32)
        Me.Controls.Add(Me.ID_31)
        Me.Controls.Add(Me.ID_30)
        Me.Controls.Add(Me.ID_29)
        Me.Controls.Add(Me.ID_28)
        Me.Controls.Add(Me.ID_27)
        Me.Controls.Add(Me.ID_26)
        Me.Controls.Add(Me.ID_25)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.AlarmCode_26)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Status_25)
        Me.Controls.Add(Me.RPM_26)
        Me.Controls.Add(Me.RPM_25)
        Me.Controls.Add(Me.Status_24)
        Me.Controls.Add(Me.RPMS_24)
        Me.Controls.Add(Me.AlarmCode_24)
        Me.Controls.Add(Me.RPM_24)
        Me.Controls.Add(Me.Status_23)
        Me.Controls.Add(Me.RPMS_23)
        Me.Controls.Add(Me.AlarmCode_23)
        Me.Controls.Add(Me.RPM_23)
        Me.Controls.Add(Me.Status_22)
        Me.Controls.Add(Me.RPMS_22)
        Me.Controls.Add(Me.AlarmCode_22)
        Me.Controls.Add(Me.RPM_22)
        Me.Controls.Add(Me.Status_21)
        Me.Controls.Add(Me.RPMS_21)
        Me.Controls.Add(Me.AlarmCode_21)
        Me.Controls.Add(Me.RPM_21)
        Me.Controls.Add(Me.Status_20)
        Me.Controls.Add(Me.RPMS_20)
        Me.Controls.Add(Me.AlarmCode_20)
        Me.Controls.Add(Me.RPM_20)
        Me.Controls.Add(Me.Status_19)
        Me.Controls.Add(Me.RPMS_19)
        Me.Controls.Add(Me.AlarmCode_19)
        Me.Controls.Add(Me.RPM_19)
        Me.Controls.Add(Me.Status_18)
        Me.Controls.Add(Me.RPMS_18)
        Me.Controls.Add(Me.AlarmCode_18)
        Me.Controls.Add(Me.RPM_18)
        Me.Controls.Add(Me.Status_17)
        Me.Controls.Add(Me.RPMS_17)
        Me.Controls.Add(Me.AlarmCode_17)
        Me.Controls.Add(Me.RPM_17)
        Me.Controls.Add(Me.Status_16)
        Me.Controls.Add(Me.RPMS_16)
        Me.Controls.Add(Me.AlarmCode_16)
        Me.Controls.Add(Me.RPM_16)
        Me.Controls.Add(Me.Status_15)
        Me.Controls.Add(Me.RPMS_15)
        Me.Controls.Add(Me.AlarmCode_15)
        Me.Controls.Add(Me.RPM_15)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Status_14)
        Me.Controls.Add(Me.RPMS_14)
        Me.Controls.Add(Me.RPMS_13)
        Me.Controls.Add(Me.AlarmCode_13)
        Me.Controls.Add(Me.ID_24)
        Me.Controls.Add(Me.ID_23)
        Me.Controls.Add(Me.ID_22)
        Me.Controls.Add(Me.ID_21)
        Me.Controls.Add(Me.ID_20)
        Me.Controls.Add(Me.ID_19)
        Me.Controls.Add(Me.ID_18)
        Me.Controls.Add(Me.ID_17)
        Me.Controls.Add(Me.ID_16)
        Me.Controls.Add(Me.ID_15)
        Me.Controls.Add(Me.ID_14)
        Me.Controls.Add(Me.ID_13)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.AlarmCode_14)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Status_13)
        Me.Controls.Add(Me.RPM_14)
        Me.Controls.Add(Me.RPM_13)
        Me.Controls.Add(Me.Status_12)
        Me.Controls.Add(Me.RPMS_12)
        Me.Controls.Add(Me.AlarmCode_12)
        Me.Controls.Add(Me.RPM_12)
        Me.Controls.Add(Me.Status_11)
        Me.Controls.Add(Me.RPMS_11)
        Me.Controls.Add(Me.AlarmCode_11)
        Me.Controls.Add(Me.RPM_11)
        Me.Controls.Add(Me.Status_10)
        Me.Controls.Add(Me.RPMS_10)
        Me.Controls.Add(Me.AlarmCode_10)
        Me.Controls.Add(Me.RPM_10)
        Me.Controls.Add(Me.Status_9)
        Me.Controls.Add(Me.RPMS_9)
        Me.Controls.Add(Me.AlarmCode_9)
        Me.Controls.Add(Me.RPM_9)
        Me.Controls.Add(Me.Status_8)
        Me.Controls.Add(Me.RPMS_8)
        Me.Controls.Add(Me.AlarmCode_8)
        Me.Controls.Add(Me.RPM_8)
        Me.Controls.Add(Me.Status_7)
        Me.Controls.Add(Me.RPMS_7)
        Me.Controls.Add(Me.AlarmCode_7)
        Me.Controls.Add(Me.RPM_7)
        Me.Controls.Add(Me.Status_6)
        Me.Controls.Add(Me.RPMS_6)
        Me.Controls.Add(Me.AlarmCode_6)
        Me.Controls.Add(Me.RPM_6)
        Me.Controls.Add(Me.Status_5)
        Me.Controls.Add(Me.RPMS_5)
        Me.Controls.Add(Me.AlarmCode_5)
        Me.Controls.Add(Me.RPM_5)
        Me.Controls.Add(Me.Status_4)
        Me.Controls.Add(Me.RPMS_4)
        Me.Controls.Add(Me.AlarmCode_4)
        Me.Controls.Add(Me.RPM_4)
        Me.Controls.Add(Me.Status_3)
        Me.Controls.Add(Me.RPMS_3)
        Me.Controls.Add(Me.AlarmCode_3)
        Me.Controls.Add(Me.RPM_3)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.TextBox9)
        Me.Controls.Add(Me.Status_2)
        Me.Controls.Add(Me.RPMS_2)
        Me.Controls.Add(Me.RPMS_1)
        Me.Controls.Add(Me.AlarmCode_1)
        Me.Controls.Add(Me.ID_12)
        Me.Controls.Add(Me.ID_11)
        Me.Controls.Add(Me.ID_10)
        Me.Controls.Add(Me.ID_9)
        Me.Controls.Add(Me.ID_8)
        Me.Controls.Add(Me.ID_7)
        Me.Controls.Add(Me.ID_6)
        Me.Controls.Add(Me.ID_5)
        Me.Controls.Add(Me.ID_4)
        Me.Controls.Add(Me.ID_3)
        Me.Controls.Add(Me.ID_2)
        Me.Controls.Add(Me.ID_1)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.AlarmCode_2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Status_1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.RPM_2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.RPM_1)
        Me.Controls.Add(Me.Label3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modbus Client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents RPM_2 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents RPM_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_2 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents ID_2 As System.Windows.Forms.Label
    Friend WithEvents ID_1 As System.Windows.Forms.Label
    Friend WithEvents ID_6 As System.Windows.Forms.Label
    Friend WithEvents ID_5 As System.Windows.Forms.Label
    Friend WithEvents ID_4 As System.Windows.Forms.Label
    Friend WithEvents ID_9 As System.Windows.Forms.Label
    Friend WithEvents ID_8 As System.Windows.Forms.Label
    Friend WithEvents ID_7 As System.Windows.Forms.Label
    Friend WithEvents ID_12 As System.Windows.Forms.Label
    Friend WithEvents ID_11 As System.Windows.Forms.Label
    Friend WithEvents ID_10 As System.Windows.Forms.Label
    Friend WithEvents Status_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RPMS_1 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_1 As System.Windows.Forms.TextBox
    Friend WithEvents Status_2 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Status_3 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_3 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_3 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_3 As System.Windows.Forms.TextBox
    Friend WithEvents ID_3 As System.Windows.Forms.Label
    Friend WithEvents Status_4 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_4 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_4 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_4 As System.Windows.Forms.TextBox
    Friend WithEvents Status_5 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_5 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_5 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_5 As System.Windows.Forms.TextBox
    Friend WithEvents Status_6 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_6 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_6 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_6 As System.Windows.Forms.TextBox
    Friend WithEvents Status_7 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_7 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_7 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_7 As System.Windows.Forms.TextBox
    Friend WithEvents Status_8 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_8 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_8 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_8 As System.Windows.Forms.TextBox
    Friend WithEvents Status_9 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_9 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_9 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_9 As System.Windows.Forms.TextBox
    Friend WithEvents Status_10 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_10 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_10 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_10 As System.Windows.Forms.TextBox
    Friend WithEvents Status_12 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_12 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_12 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_12 As System.Windows.Forms.TextBox
    Friend WithEvents Status_11 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_11 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_11 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_11 As System.Windows.Forms.TextBox
    Friend WithEvents Status_24 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_24 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_24 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_24 As System.Windows.Forms.TextBox
    Friend WithEvents Status_23 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_23 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_23 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_23 As System.Windows.Forms.TextBox
    Friend WithEvents Status_22 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_22 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_22 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_22 As System.Windows.Forms.TextBox
    Friend WithEvents Status_21 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_21 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_21 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_21 As System.Windows.Forms.TextBox
    Friend WithEvents Status_20 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_20 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_20 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_20 As System.Windows.Forms.TextBox
    Friend WithEvents Status_19 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_19 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_19 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_19 As System.Windows.Forms.TextBox
    Friend WithEvents Status_18 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_18 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_18 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_18 As System.Windows.Forms.TextBox
    Friend WithEvents Status_17 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_17 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_17 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_17 As System.Windows.Forms.TextBox
    Friend WithEvents Status_16 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_16 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_16 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_16 As System.Windows.Forms.TextBox
    Friend WithEvents Status_15 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_15 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_15 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_15 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Status_14 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_14 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_13 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_13 As System.Windows.Forms.TextBox
    Friend WithEvents ID_24 As System.Windows.Forms.Label
    Friend WithEvents ID_23 As System.Windows.Forms.Label
    Friend WithEvents ID_22 As System.Windows.Forms.Label
    Friend WithEvents ID_21 As System.Windows.Forms.Label
    Friend WithEvents ID_20 As System.Windows.Forms.Label
    Friend WithEvents ID_19 As System.Windows.Forms.Label
    Friend WithEvents ID_18 As System.Windows.Forms.Label
    Friend WithEvents ID_17 As System.Windows.Forms.Label
    Friend WithEvents ID_16 As System.Windows.Forms.Label
    Friend WithEvents ID_15 As System.Windows.Forms.Label
    Friend WithEvents ID_14 As System.Windows.Forms.Label
    Friend WithEvents ID_13 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_14 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Status_13 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_14 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_13 As System.Windows.Forms.TextBox
    Friend WithEvents Status_36 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_36 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_36 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_36 As System.Windows.Forms.TextBox
    Friend WithEvents Status_35 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_35 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_35 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_35 As System.Windows.Forms.TextBox
    Friend WithEvents Status_34 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_34 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_34 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_34 As System.Windows.Forms.TextBox
    Friend WithEvents Status_33 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_33 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_33 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_33 As System.Windows.Forms.TextBox
    Friend WithEvents Status_32 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_32 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_32 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_32 As System.Windows.Forms.TextBox
    Friend WithEvents Status_31 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_31 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_31 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_31 As System.Windows.Forms.TextBox
    Friend WithEvents Status_30 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_30 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_30 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_30 As System.Windows.Forms.TextBox
    Friend WithEvents Status_29 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_29 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_29 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_29 As System.Windows.Forms.TextBox
    Friend WithEvents Status_28 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_28 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_28 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_28 As System.Windows.Forms.TextBox
    Friend WithEvents Status_27 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_27 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_27 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_27 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Status_26 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_26 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_25 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_25 As System.Windows.Forms.TextBox
    Friend WithEvents ID_36 As System.Windows.Forms.Label
    Friend WithEvents ID_35 As System.Windows.Forms.Label
    Friend WithEvents ID_34 As System.Windows.Forms.Label
    Friend WithEvents ID_33 As System.Windows.Forms.Label
    Friend WithEvents ID_32 As System.Windows.Forms.Label
    Friend WithEvents ID_31 As System.Windows.Forms.Label
    Friend WithEvents ID_30 As System.Windows.Forms.Label
    Friend WithEvents ID_29 As System.Windows.Forms.Label
    Friend WithEvents ID_28 As System.Windows.Forms.Label
    Friend WithEvents ID_27 As System.Windows.Forms.Label
    Friend WithEvents ID_26 As System.Windows.Forms.Label
    Friend WithEvents ID_25 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_26 As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Status_25 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_26 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_25 As System.Windows.Forms.TextBox
    Friend WithEvents Status_72 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_72 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_72 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_72 As System.Windows.Forms.TextBox
    Friend WithEvents Status_71 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_71 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_71 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_71 As System.Windows.Forms.TextBox
    Friend WithEvents Status_70 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_70 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_70 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_70 As System.Windows.Forms.TextBox
    Friend WithEvents Status_69 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_69 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_69 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_69 As System.Windows.Forms.TextBox
    Friend WithEvents Status_68 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_68 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_68 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_68 As System.Windows.Forms.TextBox
    Friend WithEvents Status_67 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_67 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_67 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_67 As System.Windows.Forms.TextBox
    Friend WithEvents Status_66 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_66 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_66 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_66 As System.Windows.Forms.TextBox
    Friend WithEvents Status_65 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_65 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_65 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_65 As System.Windows.Forms.TextBox
    Friend WithEvents Status_64 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_64 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_64 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_64 As System.Windows.Forms.TextBox
    Friend WithEvents Status_63 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_63 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_63 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_63 As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Status_62 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_62 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_61 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_61 As System.Windows.Forms.TextBox
    Friend WithEvents ID_72 As System.Windows.Forms.Label
    Friend WithEvents ID_71 As System.Windows.Forms.Label
    Friend WithEvents ID_70 As System.Windows.Forms.Label
    Friend WithEvents ID_69 As System.Windows.Forms.Label
    Friend WithEvents ID_68 As System.Windows.Forms.Label
    Friend WithEvents ID_67 As System.Windows.Forms.Label
    Friend WithEvents ID_66 As System.Windows.Forms.Label
    Friend WithEvents ID_65 As System.Windows.Forms.Label
    Friend WithEvents ID_64 As System.Windows.Forms.Label
    Friend WithEvents ID_63 As System.Windows.Forms.Label
    Friend WithEvents ID_62 As System.Windows.Forms.Label
    Friend WithEvents ID_61 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_62 As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Status_61 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_62 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_61 As System.Windows.Forms.TextBox
    Friend WithEvents Status_60 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_60 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_60 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_60 As System.Windows.Forms.TextBox
    Friend WithEvents Status_59 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_59 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_59 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_59 As System.Windows.Forms.TextBox
    Friend WithEvents Status_58 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_58 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_58 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_58 As System.Windows.Forms.TextBox
    Friend WithEvents Status_57 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_57 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_57 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_57 As System.Windows.Forms.TextBox
    Friend WithEvents Status_56 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_56 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_56 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_56 As System.Windows.Forms.TextBox
    Friend WithEvents Status_55 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_55 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_55 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_55 As System.Windows.Forms.TextBox
    Friend WithEvents Status_54 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_54 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_54 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_54 As System.Windows.Forms.TextBox
    Friend WithEvents Status_53 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_53 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_53 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_53 As System.Windows.Forms.TextBox
    Friend WithEvents Status_52 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_52 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_52 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_52 As System.Windows.Forms.TextBox
    Friend WithEvents Status_51 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_51 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_51 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_51 As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Status_50 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_50 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_49 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_49 As System.Windows.Forms.TextBox
    Friend WithEvents ID_60 As System.Windows.Forms.Label
    Friend WithEvents ID_59 As System.Windows.Forms.Label
    Friend WithEvents ID_58 As System.Windows.Forms.Label
    Friend WithEvents ID_57 As System.Windows.Forms.Label
    Friend WithEvents ID_56 As System.Windows.Forms.Label
    Friend WithEvents ID_55 As System.Windows.Forms.Label
    Friend WithEvents ID_54 As System.Windows.Forms.Label
    Friend WithEvents ID_53 As System.Windows.Forms.Label
    Friend WithEvents ID_52 As System.Windows.Forms.Label
    Friend WithEvents ID_51 As System.Windows.Forms.Label
    Friend WithEvents ID_50 As System.Windows.Forms.Label
    Friend WithEvents ID_49 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_50 As System.Windows.Forms.TextBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Status_49 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_50 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_49 As System.Windows.Forms.TextBox
    Friend WithEvents Status_48 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_48 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_48 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_48 As System.Windows.Forms.TextBox
    Friend WithEvents Status_47 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_47 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_47 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_47 As System.Windows.Forms.TextBox
    Friend WithEvents Status_46 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_46 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_46 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_46 As System.Windows.Forms.TextBox
    Friend WithEvents Status_45 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_45 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_45 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_45 As System.Windows.Forms.TextBox
    Friend WithEvents Status_44 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_44 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_44 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_44 As System.Windows.Forms.TextBox
    Friend WithEvents Status_43 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_43 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_43 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_43 As System.Windows.Forms.TextBox
    Friend WithEvents Status_42 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_42 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_42 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_42 As System.Windows.Forms.TextBox
    Friend WithEvents Status_41 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_41 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_41 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_41 As System.Windows.Forms.TextBox
    Friend WithEvents Status_40 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_40 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_40 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_40 As System.Windows.Forms.TextBox
    Friend WithEvents Status_39 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_39 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_39 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_39 As System.Windows.Forms.TextBox
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Status_38 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_38 As System.Windows.Forms.TextBox
    Friend WithEvents RPMS_37 As System.Windows.Forms.TextBox
    Friend WithEvents AlarmCode_37 As System.Windows.Forms.TextBox
    Friend WithEvents ID_48 As System.Windows.Forms.Label
    Friend WithEvents ID_47 As System.Windows.Forms.Label
    Friend WithEvents ID_46 As System.Windows.Forms.Label
    Friend WithEvents ID_45 As System.Windows.Forms.Label
    Friend WithEvents ID_44 As System.Windows.Forms.Label
    Friend WithEvents ID_43 As System.Windows.Forms.Label
    Friend WithEvents ID_42 As System.Windows.Forms.Label
    Friend WithEvents ID_41 As System.Windows.Forms.Label
    Friend WithEvents ID_40 As System.Windows.Forms.Label
    Friend WithEvents ID_39 As System.Windows.Forms.Label
    Friend WithEvents ID_38 As System.Windows.Forms.Label
    Friend WithEvents ID_37 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents AlarmCode_38 As System.Windows.Forms.TextBox
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Status_37 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_38 As System.Windows.Forms.TextBox
    Friend WithEvents RPM_37 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox

End Class
