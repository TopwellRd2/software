﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms

Public Class Form1
    Dim start As Boolean = False  '判斷是否需要開啟自動讀取資料
    Dim PLCID(4) As String

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ''MessageBox("關閉")
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            Application.Run()
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '預設顯示 虛擬Port COM1
        Dim portCount As Integer
        For portCount = 1 To 20
            ComboBox1.Items.Add("COM" & portCount)
        Next
        ComboBox1.SelectedIndex = 0

        ''預設顯示 Parity "Even"
        ComboBox2.Items.Add("None")
        ComboBox2.Items.Add("Odd")
        ComboBox2.Items.Add("Even")
        ComboBox2.SelectedIndex = 0

        '預設顯示 DataBits "7"
        ComboBox3.Items.Add("8")
        ComboBox3.Items.Add("7")
        ComboBox3.Items.Add("6")
        ComboBox3.SelectedIndex = 0

        '預設顯示 Stop Bits "2"
        ComboBox4.Items.Add("1")
        ComboBox4.Items.Add("1.5")
        ComboBox4.Items.Add("2")
        ComboBox4.SelectedIndex = 0

        '預設顯示 PLC No.
        For portCount = 1 To 4
            ComboBox5.Items.Add(portCount)
        Next
        ComboBox5.SelectedIndex = 0

        '預設顯示 BaudRate "9600"
        TextBox3.Text = "9600"

        '預設顯示 存取位置 "0"
        TextBox1.Text = "0"

        '預設顯示 PLC通訊站號 "01"
        TextBox7.Text = "01"

        'PLC 通訊埠設定
        For i = 1 To 4
            ComboBox5.Text = CStr(i)
            Button19.PerformClick()
        Next
        ComboBox5.Text = "1"

        '現在時間
        Label9.Text = DateTime.Now
        'Timer開啟()->顯示日期時間之Timer
        Timer1.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        'Function 3 request is always 8 bytes:
        Dim byttrs(7) As Byte

        'Function 3 response buffer:  一次只要一個資料所以長度為 (5 + 2 * 比數)-1 -> (5 + 2*1)-1 = 6
        Dim response As Byte() = New Byte((5 + 2 * 1) - 1) {}

        '開啟Serialport & Clear out/in buffers
        If SerialPort1.IsOpen = False Then
            SerialPort1.Open()
            SerialPort1.DiscardOutBuffer()                                               'Clear out buffers
            SerialPort1.DiscardInBuffer()                                                'Clear in buffers
        End If

        Dim lonCRC As Long
        Dim intCnt As Integer
        Dim intBit As Integer
        Dim intLeng As Integer
        Dim intTemp As Integer
        Dim bytTemp As Byte
        Dim bytRes() As Byte
        Dim recALL As String
        'Retry:
        byttrs(0) = 1     ''ID 62 Hex 3E"
        byttrs(1) = 3     ''讀取 word (AI)"
        byttrs(2) = 0     ''Starting Address Hi"
        byttrs(3) = 0     ''Starting Address Lo"
        byttrs(4) = 0     ''No. of Points Hi"
        byttrs(5) = 1     ''No. of Points Lo

        '        GoTo Tp

        'Tp:
        lonCRC = &HFFFF&
        For intCnt = 0 To 5
            lonCRC = lonCRC Xor byttrs(intCnt)
            For intBit = 0 To 7
                intTemp = lonCRC Mod 2
                lonCRC = lonCRC \ 2
                If intTemp = 1 Then
                    lonCRC = lonCRC Xor &HA001&
                End If
            Next intBit
        Next intCnt

        byttrs(6) = lonCRC Mod 256
        byttrs(7) = lonCRC \ 256

        TextBox2.Text = byttrs(6)
        TextBox4.Text = byttrs(7)

        SerialPort1.Write(byttrs, 0, byttrs.Length)

        Dim i As Integer = 0
        Thread.Sleep(200)
        While (SerialPort1.BytesToRead <> 0)
            response(i) = Byte.Parse(SerialPort1.ReadByte())
            'Thread.Sleep(200)
            i += 1
            'Application.DoEvents()
        End While
        recALL = ""
        'Decimal.op_Implicit(response(3))
        'Decimal.op_Implicit(response(4))
        'ConvertByteDecimal(response(4))
        'For i = 0 To (response.Length - 1)
        If Mid(response(3), 1, 1) = 1 Then
            recALL = "-" & Val("&H" & response(4) & Mid(response(3), 2, 2))
            'recALL = Mid(response(3), 2, 2) & response(4)
            'recALL = "-" & recALL
        Else
            recALL = Val("&H" & response(4) & response(3))
        End If

        'For i = 3 To 4
        '    recALL = recALL & Val("&H" & response(i))
        'Next
        TextBox8.Text = recALL
        'Debug.Print(byttrs.ToString())
        'Dim ts As String = Hex("62") & "0300000001" & Hex(byttrs(6)) & Hex(byttrs(7))
        'SerialPort1.Write(ts)
        'Thread.Sleep(130)
        ' ''讀取資料時有誤~要確認-20171122
        'recALL = SerialPort1.ReadExisting
        'If Len(recALL) = 0 Then
        '    TextBox8.Text = "通訊失敗"
        'Else

        '    TextBox8.Text = recALL
        'End If
        SerialPort1.Close()

        'Dim Text2 = ""
        'For intCnt = 0 To UBound(bytRes)
        '    If intCnt <> 0 Then
        '        Text2 = Text2 + ","
        '    End If
        '    Text2 = Text2 & bytRes(intCnt)
        'Next intCnt
        'TextBox8.Text = Val("&H" & Hex(bytRes(3)) & Hex(bytRes(4)))

        'Dim CMT3, cmm1, cmm2, cmm3, FCOMM As String
        'Dim UN, ICF, DNA, DA2, SA2, SID As String
        'Dim R As String
        'Dim recALL, Value, ve_cal As String
        'Dim k As Integer

        'UN = PLCID(0) '模組編號(站號模組內部軟體設定)
        'ICF = "00"
        'DNA = "00"
        'DA2 = "00"
        'SA2 = "00"
        'SID = "00"

        'CMT3 = Hex(TextBox1.Text) '輸入碼轉換16進制
        'k = Len(CMT3)

        'Select Case k
        '    Case "1"
        '        CMT3 = "000" & CMT3
        '    Case "2"
        '        CMT3 = "00" & CMT3
        '    Case "3"
        '        CMT3 = "0" & CMT3
        '    Case "4"
        '        CMT3 = CMT3
        'End Select

        'FCOMM = "010182" & CMT3 & "000001"  'FinsCommd讀取DM區1筆資料
        'cmm1 = "@" & UN & "FA0" & ICF & DA2 & SA2 & SID & FCOMM

        ''計算FCS
        'R = ""
        'Call FCS(cmm1, R$)

        'cmm2 = cmm1 + R + "*"
        'cmm3 = cmm2 & Chr(13)
        'Label2.Text = cmm3
        'If SerialPort1.IsOpen = False Then
        '    SerialPort1.Open()
        'End If
        'SerialPort1.Write(cmm3)

        'Thread.Sleep(130)

        'recALL = SerialPort1.ReadExisting
        'SerialPort1.Close()

        'If Len(recALL) = 0 Then
        '    Label10.Text = "通訊失敗"
        '    TextBox8.Text = "錯誤"
        'Else
        '    ve_cal = Mid(recALL, 24, 4)
        '    Value = Val("&H" & Mid(recALL, 24, 4)) ' 16進製轉10進制 Val(&h...)
        '    Label10.Text = recALL
        '    TextBox8.Text = Value
        'End If
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Dim pa, db, sb As Integer
        Dim spn As SerialPort
        Select Case ComboBox2.Text
            Case "None"
                pa = 0
            Case "Odd"
                pa = 1
            Case "Even"
                pa = 2
        End Select

        Select Case ComboBox3.Text
            Case "6"
                db = 6
            Case "7"
                db = 7
            Case "8"
                db = 8
        End Select

        Select Case ComboBox4.Text
            Case "1"
                sb = 1
            Case "1.5"
                sb = 3
            Case "2"
                sb = 2
        End Select
        Select Case ComboBox5.Text
            Case "1"
                spn = SerialPort1
                Call SerialPortSet(spn, 1, ComboBox1.Text, TextBox3.Text, pa, db, sb)
                COM1.Text = ComboBox1.Text   '顯示 COM
                PLCID(0) = TextBox7.Text     '設定站號
                PORT1.Text = "(" & SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
                ID1.Text = PLCID(0)          '顯示站號
            Case "2"
                spn = SerialPort2
                Call SerialPortSet(spn, 2, ComboBox1.Text, TextBox3.Text, pa, db, sb)
                COM2.Text = ComboBox1.Text   '顯示 COM
                PLCID(1) = TextBox7.Text     '設定站號
                PORT2.Text = "(" & SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
                ID2.Text = PLCID(1)          '顯示站號
            Case "3"
                spn = SerialPort3
                Call SerialPortSet(spn, 3, ComboBox1.Text, TextBox3.Text, pa, db, sb)
                COM3.Text = ComboBox1.Text   '顯示 COM
                PLCID(2) = TextBox7.Text     '設定站號
                PORT3.Text = "(" & SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
                ID3.Text = PLCID(0)          '顯示站號
            Case "4"
                spn = SerialPort4
                Call SerialPortSet(spn, 4, ComboBox1.Text, TextBox3.Text, pa, db, sb)
                COM4.Text = ComboBox1.Text   '顯示 COM
                PLCID(3) = TextBox7.Text     '設定站號
                PORT4.Text = "(" & SerialPort1.BaudRate & "," & ComboBox2.Text & "," & ComboBox3.Text & "," & ComboBox4.Text & ")"  '顯示 COMPORT Detail
                ID4.Text = PLCID(0)          '顯示站號
        End Select
    End Sub

    Public Sub SerialPortSet(ByRef sp As SerialPort, no As Integer, com As String, br As String, pa As String, db As String, sb As String) 'Set PLC No. Serial Port
        sp.Close()
        sp.PortName = com
        sp.BaudRate = br 'Set BaudRate
        sp.Parity = pa    'Set Parity
        sp.DataBits = db  'Set DataBits
        sp.StopBits = sb  'Set Stop Bits
        Try
            If sp.IsOpen = False Then
                sp.Open()
            End If
        Catch ex As Exception
            MsgBox("PLC No." & no & " : " & sp.PortName.ToString & " 開啟失敗 , 通訊埠 " & com & " 不存在 / 重複開啟 .", MsgBoxStyle.Critical)
            'MessageBox.Show(String.Format("出問題啦:{0}", ex.ToString()))
        End Try
        sp.Close()
    End Sub

    Public Sub ConvertByteDecimal(ByVal byteVal As Byte)
        Dim decimalVal As Decimal

        ' Byte to decimal conversion will not overflow.
        decimalVal = System.Convert.ToDecimal(byteVal)
        System.Console.WriteLine("The byte as a decimal is {0}.", _
                                  decimalVal)

        ' Decimal to byte conversion can overflow.
        Try
            byteVal = System.Convert.ToByte(decimalVal)
            System.Console.WriteLine("The Decimal as a byte is {0}.", _
                                      byteVal)
        Catch exception As System.OverflowException
            System.Console.WriteLine( _
                "Overflow in decimal-to-byte conversion.")
        End Try
    End Sub

    Function FHexToInt6(ByVal str As String) As String
        Dim text1 As String
        text1 = str
        Dim text2 As String
        text2 = Mid(text1, 5, 2)
        Dim text3 As String
        text3 = Mid(text1, 3, 2)
        Dim text4 As String
        text4 = Mid(text1, 1, 2)
        FHexToInt6 = Val("&H" & text2 & text3 & text4)
        Exit Function
    End Function

End Class
