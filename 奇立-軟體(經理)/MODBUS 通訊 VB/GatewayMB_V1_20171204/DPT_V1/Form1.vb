﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Public Class Form1
    Dim start As Boolean = False  '判斷是否需要開啟自動讀取資料

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ''MessageBox("關閉")
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            Application.Run()
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '現在時間
        Label9.Text = DateTime.Now
        'Timer開啟()->顯示日期時間之Timer
        'Timer1.Enabled = True

        '引導使用者輸入資料
        TextBox11.Text = "192.168.1.3"
        TextBox12.Text = "1"
        TextBox13.Text = "40001"
        TextBox21.Text = "192.168.1.3"
        TextBox22.Text = "1"
        TextBox23.Text = "40001"
        TextBox31.Text = "192.168.1.3"
        TextBox32.Text = "1"
        TextBox33.Text = "40023"
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick        '兩秒執行一次 timer1.Interval = 2000 ms
        Label9.Text = DateTime.Now
        Button8.PerformClick()
        Button1.PerformClick()
        Button2.PerformClick()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If start = False Then                                                            '切換成Auto modle
            start = True
            Timer1.Enabled = True
            Button3.Text = "Auto modle"
        Else                                                                             '切換成Manually modle
            start = False
            Timer1.Enabled = False
            Button3.Text = "Manually modle"
        End If
    End Sub

    Public Sub Tdata(RH As Integer, RL As Integer, EH As Short, EL As Short, ByRef value As Short) 'Get Device Data
        value = (value - RL) * (EH - EL) / (RH - RL) + EL
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim value As Short
        Dim dip As String
        Dim id As Byte
        dip = TextBox11.Text
        id = TextBox12.Text
        Dim addr As Integer = CInt(TextBox13.Text) - 40001
        Dim address(2) As Byte
        address(0) = addr / 256
        address(1) = addr Mod 256
        tconnect(dip, id, address(0), address(1), value)
        TextBox14.Text = value / 10
    End Sub
    Public Sub tconnect(IP As String, id As Byte, adh As Byte, adl As Byte, ByRef value As Short)
        Dim myTcpClient As New TcpClient(IP, 502)                             'Device IP 與 port
        Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                   '與Device 連線
        If myTcpClient.Connected Then
            Label11.Text = "Server Connected ..."
        Else
            Label11.Text = "Server Not Connected ..."
        End If

        Dim byttrs(11) As Byte                                                           '指令集 MODBUS TCP 與 RTU 指令串不相同
        byttrs(0) = 0                                                                    'Transaction Identifier Hi
        byttrs(1) = 1                                                                    'Transaction Identifier Lo
        byttrs(2) = 0                                                                    'Protocol Identifier Hi
        byttrs(3) = 0                                                                    'Protocol Identifier Lo
        byttrs(4) = 0                                                                    'Message Length Hi
        byttrs(5) = 1                                                                    'Message Length Lo
        byttrs(6) = id                                                                   'The Unit Identifier 
        byttrs(7) = 4                                                                    'The Function Code ("4" 為讀取Holding Registers)
        byttrs(8) = adh                                                                    'Starting Address Hi
        byttrs(9) = adl                                                                  'Starting Address Lo
        byttrs(10) = 0                                                                   'No. of Points Hi
        byttrs(11) = 1                                                                   'No. of Points Lo

        myNetworkStream.Write(byttrs, 0, byttrs.Length)                                  '送出命令
        Dim inStream(11) As Byte
        myNetworkStream.Read(inStream, 0, inStream.Length)                               '接收資料
        'Dim value As Integer = 0                                                         '初始值設為0
        value = (value << 8) + inStream(9)                                               '把解析出的byte除以除以 2 的 8 次方
        value = (value << 8) + inStream(10)                                              '把解析出的byte除以除以 2 的 8 次方
        myTcpClient.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim value As Short
        Dim dip As String
        Dim id As Byte
        dip = TextBox21.Text
        id = TextBox22.Text
        Dim addr As Integer = CInt(TextBox23.Text) - 40001
        Dim address(2) As Byte
        address(0) = addr / 256
        address(1) = addr Mod 256
        tconnect(dip, id, address(0), address(1), value)
        TextBox24.Text = value / 10
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim no As Byte = Convert.ToByte(TextBox34.Text)
        'Dim no As Byte = 6
        Dim value(no) As Short
        Dim dip As String
        Dim id As Byte
        dip = TextBox31.Text
        id = TextBox32.Text
        Dim addr As Integer = CInt(TextBox33.Text) - 40001
        Dim address(2) As Byte
        address(0) = addr / 256
        address(1) = addr Mod 256
        tconnects(dip, id, address(0), address(1), no, value)

        ''DPT-40023,Temp-40027,RH-40028
        TextBox35.Text = value(0) / 10
        TextBox36.Text = value(4) / 10
        TextBox37.Text = value(5) / 10
    End Sub

    Public Sub tconnects(IP As String, id As Byte, adh As Byte, adl As Byte, no As Byte, ByRef value() As Short)
        Dim myTcpClient As New TcpClient(IP, 502)                                        'Device IP 與 port
        Dim myNetworkStream As NetworkStream = myTcpClient.GetStream()                   '與Device 連線
        If myTcpClient.Connected Then
            Label11.Text = "Server Connected ..."
        Else
            Label11.Text = "Server Not Connected ..."
        End If

        Dim byttrs(11) As Byte                                                           '指令集 MODBUS TCP 與 RTU 指令串不相同
        byttrs(0) = 0                                                                    'MBAP Header Fields-Transaction Identifier Hi
        byttrs(1) = 1                                                                    'MBAP Header Fields-Transaction Identifier Lo
        byttrs(2) = 0                                                                    'MBAP Header Fields-Protocol Identifier Hi
        byttrs(3) = 0                                                                    'MBAP Header Fields-Protocol Identifier Lo
        byttrs(4) = 0                                                                    'MBAP Header Fields-Message Length Hi
        byttrs(5) = 1                                                                    'MBAP Header Fields-Message Length Lo (固定填入"1")
        byttrs(6) = id                                                                   'MBAP Header Fields-The Unit Identifier 
        byttrs(7) = 4                                                                    'Modbus TCP/IP PDU-The Function Code ("4" 為讀取Holding Registers)
        byttrs(8) = adh                                                                  'Modbus TCP/IP PDU-Starting Address Hi
        byttrs(9) = adl                                                                  'Modbus TCP/IP PDU-Starting Address Lo
        byttrs(10) = 0                                                                   'Modbus TCP/IP PDU-No. of Points Hi
        byttrs(11) = no                                                                  'Modbus TCP/IP PDU-No. of Points Lo

        myNetworkStream.Write(byttrs, 0, byttrs.Length)                                  '送出命令

        ''''Modbus Response ADU:MBAP Header Fields + Field Name''
        ''''Transaction ID High Order[0] + Transaction ID Low Order[1] + Protocol Identifier High Order[2] + Protocol Identifier Low Order[3] + 
        ''''Length High Order[4] + Length Low Order[5] + Unit Identifier[6]
        ''''Function Code[7] + Byte Count[8] + Data High(0) + Data Low(0)...+ Data High(n) + Data Low(n)
        ''''p.s.Length:The server calculates the size of the MODBUS PDU plus the Unit Identifier byte.
        ''''           This value is set in the "Length" field.
        Dim inStream(9 + no * 2) As Byte
        myNetworkStream.Read(inStream, 0, inStream.Length)                               '接收資料
        For i = 0 To no - 1
            value(i) = (value(i) << 8) + inStream(9 + i * 2)                             '把解析出的byte除以除以 2 的 8 次方
            value(i) = (value(i) << 8) + inStream(10 + i * 2)                            '把解析出的byte除以除以 2 的 8 次方
        Next
        myTcpClient.Close()
    End Sub

End Class
