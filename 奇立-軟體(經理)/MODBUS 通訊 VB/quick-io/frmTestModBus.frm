VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form Form1 
   Caption         =   "Test"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   7560
   StartUpPosition =   3  '系統預設值
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   615
      Left            =   5280
      TabIndex        =   31
      Top             =   7080
      Width           =   1695
   End
   Begin VB.Timer Timer1 
      Left            =   7080
      Top             =   1680
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   495
      Left            =   5340
      TabIndex        =   29
      Top             =   5880
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   5280
      TabIndex        =   28
      Top             =   5040
      Width           =   1575
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   7
      Left            =   4200
      TabIndex        =   27
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   6
      Left            =   3720
      TabIndex        =   26
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   5
      Left            =   3240
      TabIndex        =   25
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   4
      Left            =   2760
      TabIndex        =   24
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   3
      Left            =   2280
      TabIndex        =   23
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   2
      Left            =   1800
      TabIndex        =   22
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   1
      Left            =   1320
      TabIndex        =   21
      Top             =   5040
      Width           =   255
   End
   Begin VB.CheckBox Out_Check1 
      Height          =   375
      Index           =   0
      Left            =   840
      TabIndex        =   20
      Top             =   5040
      Width           =   255
   End
   Begin VB.ComboBox Combo2 
      Height          =   300
      ItemData        =   "frmTestModBus.frx":0000
      Left            =   1920
      List            =   "frmTestModBus.frx":000A
      TabIndex        =   19
      Text            =   "1"
      Top             =   360
      Width           =   975
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      ItemData        =   "frmTestModBus.frx":0014
      Left            =   4800
      List            =   "frmTestModBus.frx":0036
      TabIndex        =   16
      Text            =   "9600,n,8,2"
      Top             =   360
      Width           =   1575
   End
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   14
      Top             =   4080
      Width           =   6615
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   5760
      TabIndex        =   8
      Top             =   2160
      Width           =   495
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   5160
      TabIndex        =   7
      Top             =   2160
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  '置中對齊
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   4320
      TabIndex        =   6
      Text            =   "0"
      Top             =   2160
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  '置中對齊
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   3720
      TabIndex        =   5
      Text            =   "0"
      Top             =   2160
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  '置中對齊
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   2760
      TabIndex        =   4
      Text            =   "0"
      Top             =   2160
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  '置中對齊
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   2160
      TabIndex        =   3
      Text            =   "0"
      Top             =   2160
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  '置中對齊
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1320
      TabIndex        =   2
      Text            =   "6"
      Top             =   2160
      Width           =   495
   End
   Begin VB.CommandButton cmdTrans 
      Caption         =   "傳送出去"
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5040
      TabIndex        =   1
      Top             =   2880
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  '置中對齊
      BeginProperty Font 
         Name            =   "新細明體"
         Size            =   12
         Charset         =   136
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Text            =   "1"
      Top             =   2160
      Width           =   495
   End
   Begin MSCommLib.MSComm mscRS485 
      Left            =   120
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   0   'False
      BaudRate        =   19200
      StopBits        =   2
      InputMode       =   1
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   7
      Left            =   4800
      Top             =   5640
      Width           =   375
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   6
      Left            =   4200
      Top             =   5640
      Width           =   375
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   5
      Left            =   3600
      Top             =   5640
      Width           =   375
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   4
      Left            =   3000
      Top             =   5640
      Width           =   375
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   3
      Left            =   2400
      Top             =   5640
      Width           =   375
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   2
      Left            =   1800
      Top             =   5640
      Width           =   375
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   1
      Left            =   1200
      Top             =   5640
      Width           =   375
   End
   Begin VB.Label Label8 
      Caption         =   "Label8"
      Height          =   495
      Left            =   1680
      TabIndex        =   30
      Top             =   3360
      Width           =   1815
   End
   Begin VB.Shape DI_led 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '實心
      Height          =   375
      Index           =   0
      Left            =   600
      Top             =   5640
      Width           =   375
   End
   Begin VB.Line Line1 
      X1              =   1560
      X2              =   1560
      Y1              =   1560
      Y2              =   2040
   End
   Begin VB.Label Label7 
      Caption         =   "comport"
      Height          =   255
      Left            =   1200
      TabIndex        =   18
      Top             =   360
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "Boud rate"
      Height          =   255
      Index           =   1
      Left            =   3840
      TabIndex        =   17
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "RETURN"
      Height          =   255
      Left            =   360
      TabIndex        =   15
      Top             =   3720
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "CRC CHECK"
      Height          =   255
      Left            =   5160
      TabIndex        =   13
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "LENGTH"
      Height          =   255
      Left            =   3840
      TabIndex        =   12
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "START NUMBER"
      Height          =   255
      Index           =   0
      Left            =   2040
      TabIndex        =   11
      Top             =   1800
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "FUNCTION CODE"
      Height          =   255
      Left            =   960
      TabIndex        =   10
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "ADDRESS"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1800
      Width           =   855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim byttrs(7) As Byte

 
               
Private Sub cmdTrans_Click()
        

    Dim strCommand As String
    Dim strTemp As String
    Dim OverFlag As Boolean
    Dim strReturn As String
    Dim TimeOver As Single
    Dim OverCount As Integer
    Dim intType As Integer
    
   
    
    Dim lonCRC As Long
    Dim intCnt As Integer
    Dim intBit As Integer
    Dim intLeng As Integer
    Dim intTemp As Integer
    Dim bytTemp As Byte
    Dim bytRes() As Byte
    
    mscRS485.Settings = Combo1.Text
    
    
Retry:
    
    
    byttrs(0) = Text1(0)
    byttrs(1) = Text1(1)
    byttrs(2) = Text1(2)
    byttrs(3) = Text1(3)
    byttrs(4) = Text1(4)
   
    byttrs(5) = Text1(5)
    
    GoTo Tp
    
    byttrs(0) = 1
    byttrs(1) = 16
    byttrs(2) = 0
    byttrs(3) = 3
    byttrs(4) = 0
    byttrs(5) = 1
    byttrs(6) = 2
    byttrs(7) = 56
    byttrs(8) = 78
     
Tp:
        
     
    lonCRC = &HFFFF&

    For intCnt = 0 To 5
        lonCRC = lonCRC Xor byttrs(intCnt)
        For intBit = 0 To 7
            intTemp = lonCRC Mod 2
            lonCRC = lonCRC \ 2
            If intTemp = 1 Then
                lonCRC = lonCRC Xor &HA001&
            End If
        Next intBit
    Next intCnt

    byttrs(6) = lonCRC Mod 256
    byttrs(7) = lonCRC \ 256
    
    
      
    Text1(6) = byttrs(6)
    Text1(7) = byttrs(7)
      
    Debug.Print byttrs
    mscRS485.Output = byttrs
   
    
   
   
    TimeOver = Timer()
    OverFlag = False
     
    
   
    Do While (Timer() - TimeOver) < 0.2
       DoEvents
    Loop
    
    bytRes = mscRS485.Input
        
    Text2 = ""
    For intCnt = 0 To UBound(bytRes)
        If intCnt <> 0 Then
            Text2 = Text2 + ","
        End If
        Text2 = Text2 & bytRes(intCnt)
    Next intCnt
    Label8.Caption = Val("&H" & Hex(bytRes(3)) & Hex(bytRes(4)))
    'GoTo Retry
     
    cmdTrans.SetFocus
End Sub


Private Sub Combo2_Click()
    mscRS485.PortOpen = False
    mscRS485.CommPort = Combo2.Text
    mscRS485.PortOpen = True
End Sub

Private Sub Command1_Click()
'Exit Sub
Dim i As Integer
Dim out_val As Integer

For i = 0 To 7
If Out_Check1(i) = 1 Then
   out_val = out_val + 2 ^ i
End If
   
Next i


Text1(5).Text = out_val
End Sub

Private Sub Command2_Click()
Dim i As Integer
For i = 7 To 0 Step -1
    If Val(Label8.Caption) \ (2 ^ i) = 1 Then
       DI_led(i).FillColor = &HFF& '&H00FFFFFF&
       Label8.Caption = Val(Label8.Caption) - 2 ^ 7
    Else
      DI_led(i).FillColor = &HFFFFFF
    End If
Next i
'Shape1.FillColor = &HFF& '&H00FFFFFF&
End Sub

Private Sub Command3_Click()
If Timer1.Interval = 0 Then
   Timer1.Interval = 100
Else
   Timer1.Interval = 0
End If
End Sub

Private Sub Form_Activate()
    cmdTrans.SetFocus
End Sub

Private Sub Form_Load()
    mscRS485.PortOpen = True
End Sub

Private Sub Timer1_Timer()
If Text1(1) = 6 Then
    Call Command1_Click
    Call cmdTrans_Click
   Text1(0) = 1
   Text1(1) = 3
   Text1(2) = 0
   Text1(3) = 0
   Text1(4) = 0
   Text1(5) = 1
   
Else
     Call cmdTrans_Click
   Text1(0) = 1
   Text1(1) = 6
   Text1(2) = 0
   Text1(3) = 1
   Text1(4) = 0
   Text1(5) = 0
    Call Command2_Click
End If
End Sub
