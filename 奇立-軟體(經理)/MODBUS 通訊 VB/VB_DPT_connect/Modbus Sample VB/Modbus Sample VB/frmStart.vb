﻿Imports System.Net
'Imports System.Drawing
'Imports System.Collections
'Imports System.ComponentModel
'Imports System.Windows.Forms
'Imports System.Data
'Imports ModbusTCP
Imports Modbus_Sample_VB.ModbusTCP

'Namespace Modbus
Public Class frmStart
    Inherits System.Windows.Forms.Form
    Private MBmaster As ModbusTCP.Master
    Private txtData As TextBox
    Private labData As Label
    Private data As Byte()

    '    Private components As System.ComponentModel.IContainer

    '    Public Sub New()
    '        InitializeComponent()
    '    End Sub

    '    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    '        If disposing Then
    '            If components IsNot Nothing Then
    '                components.Dispose()
    '            End If
    '        End If
    '        MyBase.Dispose(disposing)
    '    End Sub

    '#Region "Vom Windows Form-Designer generierter Code"
    '    ''' <summary>
    '    ''' Erforderliche Methode für die Designerunterstützung. 
    '    ''' Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    '    ''' </summary>
    '    Private Sub InitializeComponent()
    '        Me.label4 = New System.Windows.Forms.Label()
    '        Me.txtUnit = New System.Windows.Forms.TextBox()
    '        Me.btnWriteMultipleReg = New System.Windows.Forms.Button()
    '        Me.btnWriteMultipleCoils = New System.Windows.Forms.Button()
    '        Me.btnWriteSingleReg = New System.Windows.Forms.Button()
    '        Me.groupBox1 = New System.Windows.Forms.GroupBox()
    '        Me.radWord = New System.Windows.Forms.RadioButton()
    '        Me.radBytes = New System.Windows.Forms.RadioButton()
    '        Me.radBits = New System.Windows.Forms.RadioButton()
    '        Me.btnReadInpReg = New System.Windows.Forms.Button()
    '        Me.btnReadHoldReg = New System.Windows.Forms.Button()
    '        Me.btnReadDisInp = New System.Windows.Forms.Button()
    '        Me.label3 = New System.Windows.Forms.Label()
    '        Me.grpData = New System.Windows.Forms.GroupBox()
    '        Me.btnWriteSingleCoil = New System.Windows.Forms.Button()
    '        Me.txtSize = New System.Windows.Forms.TextBox()
    '        Me.label2 = New System.Windows.Forms.Label()
    '        Me.txtStartAdress = New System.Windows.Forms.TextBox()
    '        Me.label1 = New System.Windows.Forms.Label()
    '        Me.grpExchange = New System.Windows.Forms.GroupBox()
    '        Me.btnReadCoils = New System.Windows.Forms.Button()
    '        Me.btnConnect = New System.Windows.Forms.Button()
    '        Me.txtIP = New System.Windows.Forms.TextBox()
    '        Me.grpStart = New System.Windows.Forms.GroupBox()
    '        Me.groupBox1.SuspendLayout()
    '        Me.grpExchange.SuspendLayout()
    '        Me.grpStart.SuspendLayout()
    '        Me.SuspendLayout()
    '        '
    '        'label4
    '        '
    '        Me.label4.Location = New System.Drawing.Point(13, 27)
    '        Me.label4.Name = "label4"
    '        Me.label4.Size = New System.Drawing.Size(74, 14)
    '        Me.label4.TabIndex = 25
    '        Me.label4.Text = "Unit"
    '        '
    '        'txtUnit
    '        '
    '        Me.txtUnit.Location = New System.Drawing.Point(87, 25)
    '        Me.txtUnit.Name = "txtUnit"
    '        Me.txtUnit.Size = New System.Drawing.Size(50, 20)
    '        Me.txtUnit.TabIndex = 24
    '        Me.txtUnit.Text = "0"
    '        Me.txtUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '        '
    '        'btnWriteMultipleReg
    '        '
    '        Me.btnWriteMultipleReg.Location = New System.Drawing.Point(573, 76)
    '        Me.btnWriteMultipleReg.Name = "btnWriteMultipleReg"
    '        Me.btnWriteMultipleReg.Size = New System.Drawing.Size(87, 35)
    '        Me.btnWriteMultipleReg.TabIndex = 23
    '        Me.btnWriteMultipleReg.Text = "Write multiple register"
    '        '
    '        'btnWriteMultipleCoils
    '        '
    '        Me.btnWriteMultipleCoils.Location = New System.Drawing.Point(573, 28)
    '        Me.btnWriteMultipleCoils.Name = "btnWriteMultipleCoils"
    '        Me.btnWriteMultipleCoils.Size = New System.Drawing.Size(87, 34)
    '        Me.btnWriteMultipleCoils.TabIndex = 22
    '        Me.btnWriteMultipleCoils.Text = "Write multiple coils"
    '        '
    '        'btnWriteSingleReg
    '        '
    '        Me.btnWriteSingleReg.Location = New System.Drawing.Point(473, 76)
    '        Me.btnWriteSingleReg.Name = "btnWriteSingleReg"
    '        Me.btnWriteSingleReg.Size = New System.Drawing.Size(87, 35)
    '        Me.btnWriteSingleReg.TabIndex = 21
    '        Me.btnWriteSingleReg.Text = "Write single register"
    '        '
    '        'groupBox1
    '        '
    '        Me.groupBox1.Controls.Add(Me.radWord)
    '        Me.groupBox1.Controls.Add(Me.radBytes)
    '        Me.groupBox1.Controls.Add(Me.radBits)
    '        Me.groupBox1.Location = New System.Drawing.Point(160, 21)
    '        Me.groupBox1.Name = "groupBox1"
    '        Me.groupBox1.Size = New System.Drawing.Size(87, 90)
    '        Me.groupBox1.TabIndex = 20
    '        Me.groupBox1.TabStop = False
    '        Me.groupBox1.Text = "Show as"
    '        '
    '        'radWord
    '        '
    '        Me.radWord.Location = New System.Drawing.Point(13, 62)
    '        Me.radWord.Name = "radWord"
    '        Me.radWord.Size = New System.Drawing.Size(67, 21)
    '        Me.radWord.TabIndex = 2
    '        Me.radWord.Text = "Word"
    '        '
    '        'radBytes
    '        '
    '        Me.radBytes.Location = New System.Drawing.Point(13, 42)
    '        Me.radBytes.Name = "radBytes"
    '        Me.radBytes.Size = New System.Drawing.Size(67, 20)
    '        Me.radBytes.TabIndex = 1
    '        Me.radBytes.Text = "Bytes"
    '        '
    '        'radBits
    '        '
    '        Me.radBits.Location = New System.Drawing.Point(13, 21)
    '        Me.radBits.Name = "radBits"
    '        Me.radBits.Size = New System.Drawing.Size(67, 21)
    '        Me.radBits.TabIndex = 0
    '        Me.radBits.Text = "Bits"
    '        '
    '        'btnReadInpReg
    '        '
    '        Me.btnReadInpReg.Location = New System.Drawing.Point(373, 76)
    '        Me.btnReadInpReg.Name = "btnReadInpReg"
    '        Me.btnReadInpReg.Size = New System.Drawing.Size(87, 35)
    '        Me.btnReadInpReg.TabIndex = 18
    '        Me.btnReadInpReg.Text = "Read input register"
    '        '
    '        'btnReadHoldReg
    '        '
    '        Me.btnReadHoldReg.Location = New System.Drawing.Point(373, 28)
    '        Me.btnReadHoldReg.Name = "btnReadHoldReg"
    '        Me.btnReadHoldReg.Size = New System.Drawing.Size(87, 34)
    '        Me.btnReadHoldReg.TabIndex = 17
    '        Me.btnReadHoldReg.Text = "Read holding register"
    '        '
    '        'btnReadDisInp
    '        '
    '        Me.btnReadDisInp.Location = New System.Drawing.Point(273, 76)
    '        Me.btnReadDisInp.Name = "btnReadDisInp"
    '        Me.btnReadDisInp.Size = New System.Drawing.Size(87, 35)
    '        Me.btnReadDisInp.TabIndex = 16
    '        Me.btnReadDisInp.Text = "Read discrete inputs"
    '        '
    '        'label3
    '        '
    '        Me.label3.Location = New System.Drawing.Point(13, 78)
    '        Me.label3.Name = "label3"
    '        Me.label3.Size = New System.Drawing.Size(74, 14)
    '        Me.label3.TabIndex = 15
    '        Me.label3.Text = "Size"
    '        '
    '        'grpData
    '        '
    '        Me.grpData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
    '                    Or System.Windows.Forms.AnchorStyles.Left) _
    '                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    '        Me.grpData.Location = New System.Drawing.Point(6, 193)
    '        Me.grpData.Name = "grpData"
    '        Me.grpData.Size = New System.Drawing.Size(740, 271)
    '        Me.grpData.TabIndex = 13
    '        Me.grpData.TabStop = False
    '        Me.grpData.Text = "Data"
    '        Me.grpData.Visible = False
    '        '
    '        'btnWriteSingleCoil
    '        '
    '        Me.btnWriteSingleCoil.Location = New System.Drawing.Point(473, 28)
    '        Me.btnWriteSingleCoil.Name = "btnWriteSingleCoil"
    '        Me.btnWriteSingleCoil.Size = New System.Drawing.Size(87, 34)
    '        Me.btnWriteSingleCoil.TabIndex = 19
    '        Me.btnWriteSingleCoil.Text = "Write single coil"
    '        '
    '        'txtSize
    '        '
    '        Me.txtSize.Location = New System.Drawing.Point(87, 78)
    '        Me.txtSize.Name = "txtSize"
    '        Me.txtSize.Size = New System.Drawing.Size(50, 20)
    '        Me.txtSize.TabIndex = 14
    '        Me.txtSize.Text = "32"
    '        Me.txtSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '        '
    '        'label2
    '        '
    '        Me.label2.Location = New System.Drawing.Point(13, 53)
    '        Me.label2.Name = "label2"
    '        Me.label2.Size = New System.Drawing.Size(74, 14)
    '        Me.label2.TabIndex = 13
    '        Me.label2.Text = "Start Adress"
    '        '
    '        'txtStartAdress
    '        '
    '        Me.txtStartAdress.Location = New System.Drawing.Point(87, 51)
    '        Me.txtStartAdress.Name = "txtStartAdress"
    '        Me.txtStartAdress.Size = New System.Drawing.Size(50, 20)
    '        Me.txtStartAdress.TabIndex = 12
    '        Me.txtStartAdress.Text = "0"
    '        Me.txtStartAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '        '
    '        'label1
    '        '
    '        Me.label1.Location = New System.Drawing.Point(13, 28)
    '        Me.label1.Name = "label1"
    '        Me.label1.Size = New System.Drawing.Size(74, 14)
    '        Me.label1.TabIndex = 7
    '        Me.label1.Text = "IP Address"
    '        '
    '        'grpExchange
    '        '
    '        Me.grpExchange.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
    '                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    '        Me.grpExchange.Controls.Add(Me.label4)
    '        Me.grpExchange.Controls.Add(Me.txtUnit)
    '        Me.grpExchange.Controls.Add(Me.btnWriteMultipleReg)
    '        Me.grpExchange.Controls.Add(Me.btnWriteMultipleCoils)
    '        Me.grpExchange.Controls.Add(Me.btnWriteSingleReg)
    '        Me.grpExchange.Controls.Add(Me.groupBox1)
    '        Me.grpExchange.Controls.Add(Me.btnWriteSingleCoil)
    '        Me.grpExchange.Controls.Add(Me.btnReadInpReg)
    '        Me.grpExchange.Controls.Add(Me.btnReadHoldReg)
    '        Me.grpExchange.Controls.Add(Me.btnReadDisInp)
    '        Me.grpExchange.Controls.Add(Me.label3)
    '        Me.grpExchange.Controls.Add(Me.txtSize)
    '        Me.grpExchange.Controls.Add(Me.label2)
    '        Me.grpExchange.Controls.Add(Me.txtStartAdress)
    '        Me.grpExchange.Controls.Add(Me.btnReadCoils)
    '        Me.grpExchange.Location = New System.Drawing.Point(6, 68)
    '        Me.grpExchange.Name = "grpExchange"
    '        Me.grpExchange.Size = New System.Drawing.Size(740, 118)
    '        Me.grpExchange.TabIndex = 15
    '        Me.grpExchange.TabStop = False
    '        Me.grpExchange.Text = "Data exhange"
    '        Me.grpExchange.Visible = False
    '        '
    '        'btnReadCoils
    '        '
    '        Me.btnReadCoils.Location = New System.Drawing.Point(273, 28)
    '        Me.btnReadCoils.Name = "btnReadCoils"
    '        Me.btnReadCoils.Size = New System.Drawing.Size(87, 34)
    '        Me.btnReadCoils.TabIndex = 11
    '        Me.btnReadCoils.Text = "Read coils"
    '        '
    '        'btnConnect
    '        '
    '        Me.btnConnect.Location = New System.Drawing.Point(187, 21)
    '        Me.btnConnect.Name = "btnConnect"
    '        Me.btnConnect.Size = New System.Drawing.Size(86, 28)
    '        Me.btnConnect.TabIndex = 6
    '        Me.btnConnect.Text = "Connect"
    '        '
    '        'txtIP
    '        '
    '        Me.txtIP.Location = New System.Drawing.Point(93, 25)
    '        Me.txtIP.Name = "txtIP"
    '        Me.txtIP.Size = New System.Drawing.Size(87, 20)
    '        Me.txtIP.TabIndex = 5
    '        Me.txtIP.Text = "192.168.100.1"
    '        Me.txtIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '        '
    '        'grpStart
    '        '
    '        Me.grpStart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
    '                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    '        Me.grpStart.Controls.Add(Me.label1)
    '        Me.grpStart.Controls.Add(Me.btnConnect)
    '        Me.grpStart.Controls.Add(Me.txtIP)
    '        Me.grpStart.Location = New System.Drawing.Point(6, 6)
    '        Me.grpStart.Name = "grpStart"
    '        Me.grpStart.Size = New System.Drawing.Size(740, 55)
    '        Me.grpStart.TabIndex = 14
    '        Me.grpStart.TabStop = False
    '        Me.grpStart.Text = "Start communication"
    '        '
    '        'frmStart
    '        '
    '        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    '        Me.ClientSize = New System.Drawing.Size(753, 471)
    '        Me.Controls.Add(Me.grpData)
    '        Me.Controls.Add(Me.grpExchange)
    '        Me.Controls.Add(Me.grpStart)
    '        Me.Name = "frmStart"
    '        Me.Text = "ModbusTCP Tester"
    '        Me.groupBox1.ResumeLayout(False)
    '        Me.grpExchange.ResumeLayout(False)
    '        Me.grpExchange.PerformLayout()
    '        Me.grpStart.ResumeLayout(False)
    '        Me.grpStart.PerformLayout()
    '        Me.ResumeLayout(False)

    '    End Sub
    '#End Region

    '    ''' <summary>
    '    ''' Der Haupteinstiegspunkt für die Anwendung.
    '    ''' </summary>
    '    <STAThread()> _
    '    Private Shared Sub Main()
    '        Application.Run(New frmStart())
    '    End Sub


    ' ------------------------------------------------------------------------
    ' Programm start
    ' ------------------------------------------------------------------------
    Private Sub frmStart_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set standard format byte, make some textboxes
        radBytes.Checked = True
        data = New Byte(-1) {}
        ResizeData()
    End Sub

    ' ------------------------------------------------------------------------
    ' Programm stop
    ' ------------------------------------------------------------------------
    Private Sub frmStart_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If MBmaster IsNot Nothing Then
            MBmaster.Dispose()
            MBmaster = Nothing
        End If
        Application.[Exit]()
    End Sub

    ' ------------------------------------------------------------------------
    ' Button connect
    ' ------------------------------------------------------------------------
    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Try
            ' Create new modbus master and add event functions
            MBmaster = New Master(txtIP.Text, 502)
            'MBmaster.OnResponseData += New ModbusTCP.Master.ResponseData(AddressOf MBmaster_OnResponseData)
            'MBmaster.OnException += New ModbusTCP.Master.ExceptionData(AddressOf MBmaster_OnException)
            AddHandler MBmaster.OnResponseData, New ModbusTCP.Master.ResponseData(AddressOf MBmaster_OnResponseData)
            AddHandler MBmaster.OnException, New ModbusTCP.Master.ExceptionData(AddressOf MBmaster_OnException)

            ' Show additional fields, enable watchdog
            grpExchange.Visible = True
            grpData.Visible = True
        Catch [error] As SystemException
            MessageBox.Show([error].Message)
        End Try
    End Sub

    ' ------------------------------------------------------------------------
    ' Button read coils
    ' ------------------------------------------------------------------------
    Private Sub btnReadCoils_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReadCoils.Click
        Dim ID As UShort = 1
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()
        Dim Length As Byte = Convert.ToByte(txtSize.Text)

        MBmaster.ReadCoils(ID, unit, StartAddress, Length)
    End Sub

    ' ------------------------------------------------------------------------
    ' Button read discrete inputs
    ' ------------------------------------------------------------------------
    Private Sub btnReadDisInp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReadDisInp.Click
        Dim ID As UShort = 2
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()
        Dim Length As Byte = Convert.ToByte(txtSize.Text)

        MBmaster.ReadDiscreteInputs(ID, unit, StartAddress, Length)
    End Sub

    ' ------------------------------------------------------------------------
    ' Button read holding register
    ' ------------------------------------------------------------------------
    Private Sub btnReadHoldReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReadHoldReg.Click
        Dim ID As UShort = 3
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()
        Dim Length As Byte = Convert.ToByte(txtSize.Text)

        MBmaster.ReadHoldingRegister(ID, unit, StartAddress, Length)
    End Sub

    ' ------------------------------------------------------------------------
    ' Button read holding register
    ' ------------------------------------------------------------------------
    Private Sub btnReadInpReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReadInpReg.Click
        Dim ID As UShort = 4
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()
        Dim Length As Byte = Convert.ToByte(txtSize.Text)

        MBmaster.ReadInputRegister(ID, unit, StartAddress, Length)
    End Sub

    ' ------------------------------------------------------------------------
    ' Button write single coil
    ' ------------------------------------------------------------------------
    Private Sub btnWriteSingleCoil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWriteSingleCoil.Click
        Dim ID As UShort = 5
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()

        data = GetData(1)
        txtSize.Text = "1"

        MBmaster.WriteSingleCoils(ID, unit, StartAddress, Convert.ToBoolean(data(0)))
    End Sub

    ' ------------------------------------------------------------------------
    ' Button write multiple coils
    ' ------------------------------------------------------------------------	
    Private Sub btnWriteMultipleCoils_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWriteMultipleCoils.Click
        Dim ID As UShort = 6
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()

        data = GetData(Convert.ToByte(txtSize.Text))
        MBmaster.WriteMultipleCoils(ID, unit, StartAddress, Convert.ToByte(txtSize.Text), data)
    End Sub

    ' ------------------------------------------------------------------------
    ' Button write single register
    ' ------------------------------------------------------------------------
    Private Sub btnWriteSingleReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWriteSingleReg.Click
        Dim ID As UShort = 7
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()

        data = GetData(2)
        txtSize.Text = "1"
        txtData.Text = data(0).ToString()

        MBmaster.WriteSingleRegister(ID, unit, StartAddress, data)
    End Sub

    ' ------------------------------------------------------------------------
    ' Button write multiple register
    ' ------------------------------------------------------------------------	
    Private Sub btnWriteMultipleReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWriteMultipleReg.Click
        Dim ID As UShort = 8
        Dim unit As Byte = Convert.ToByte(txtUnit.Text)
        Dim StartAddress As UShort = ReadStartAdr()

        data = GetData(Convert.ToByte(txtSize.Text))
        MBmaster.WriteMultipleRegister(ID, unit, StartAddress, data)
    End Sub

    ' ------------------------------------------------------------------------
    ' Event for response data
    ' ------------------------------------------------------------------------
    Private Sub MBmaster_OnResponseData(ByVal ID As UShort, ByVal unit As Byte, ByVal [function] As Byte, ByVal values As Byte())
        ' ------------------------------------------------------------------
        ' Seperate calling threads
        If Me.InvokeRequired Then
            Me.BeginInvoke(New Master.ResponseData(AddressOf MBmaster_OnResponseData), New Object() {ID, unit, [function], values})
            Return
        End If

        ' ------------------------------------------------------------------------
        ' Identify requested data
        Select Case ID
            Case 1
                grpData.Text = "Read coils"
                data = values
                ShowAs(Nothing, Nothing)
                Exit Select
            Case 2
                grpData.Text = "Read discrete inputs"
                data = values
                ShowAs(Nothing, Nothing)
                Exit Select
            Case 3
                grpData.Text = "Read holding register"
                data = values
                ShowAs(Nothing, Nothing)
                Exit Select
            Case 4
                grpData.Text = "Read input register"
                data = values
                ShowAs(Nothing, Nothing)
                Exit Select
            Case 5
                grpData.Text = "Write single coil"
                Exit Select
            Case 6
                grpData.Text = "Write multiple coils"
                Exit Select
            Case 7
                grpData.Text = "Write single register"
                Exit Select
            Case 8
                grpData.Text = "Write multiple register"
                Exit Select
        End Select
    End Sub

    ' ------------------------------------------------------------------------
    ' Modbus TCP slave exception
    ' ------------------------------------------------------------------------
    Private Sub MBmaster_OnException(ByVal id As UShort, ByVal unit As Byte, ByVal [function] As Byte, ByVal exception As Byte)
        Dim exc As String = "Modbus says error: "
        Select Case exception
            Case Master.excIllegalFunction
                exc += "Illegal function!"
                Exit Select
            Case Master.excIllegalDataAdr
                exc += "Illegal data adress!"
                Exit Select
            Case Master.excIllegalDataVal
                exc += "Illegal data value!"
                Exit Select
            Case Master.excSlaveDeviceFailure
                exc += "Slave device failure!"
                Exit Select
            Case Master.excAck
                exc += "Acknoledge!"
                Exit Select
            Case Master.excGatePathUnavailable
                exc += "Gateway path unavailbale!"
                Exit Select
            Case Master.excExceptionTimeout
                exc += "Slave timed out!"
                Exit Select
            Case Master.excExceptionConnectionLost
                exc += "Connection is lost!"
                Exit Select
            Case Master.excExceptionNotConnected
                exc += "Not connected!"
                Exit Select
        End Select

        MessageBox.Show(exc, "Modbus slave exception")
    End Sub

    ' ------------------------------------------------------------------------
    ' Generate new number of text boxes
    ' ------------------------------------------------------------------------
    Private Sub ResizeData()
        ' Create as many textboxes as fit into window
        grpData.Controls.Clear()
        Dim x As Integer = 0
        Dim y As Integer = 10
        Dim z As Integer = 20
        While y < grpData.Size.Width - 100
            labData = New Label()
            grpData.Controls.Add(labData)
            labData.Size = New System.Drawing.Size(30, 20)
            labData.Location = New System.Drawing.Point(y, z)
            labData.Text = Convert.ToString(x + 1)

            txtData = New TextBox()
            grpData.Controls.Add(txtData)
            txtData.Size = New System.Drawing.Size(50, 20)
            txtData.Location = New System.Drawing.Point(y + 30, z)
            txtData.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
            txtData.Tag = x

            x += 1
            z = z + txtData.Size.Height + 5
            If z > grpData.Size.Height - 40 Then
                y = y + 100
                z = 20
            End If
        End While
    End Sub

    ' ------------------------------------------------------------------------
    ' Resize form elements
    ' ------------------------------------------------------------------------
    Private Sub frmStart_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        If grpData.Visible = True Then
            ResizeData()
        End If
    End Sub

    ' ------------------------------------------------------------------------
    ' Read start address
    ' ------------------------------------------------------------------------
    Private Function ReadStartAdr() As UShort
        ' Convert hex numbers into decimal
        If txtStartAdress.Text.IndexOf("0x", 0, txtStartAdress.Text.Length) = 0 Then
            Dim str As String = txtStartAdress.Text.Replace("0x", "")
            Dim hex As UShort = Convert.ToUInt16(str, 16)
            Return hex
        Else
            Return Convert.ToUInt16(txtStartAdress.Text)
        End If
    End Function

    ' ------------------------------------------------------------------------
    ' Read values from textboxes
    ' ------------------------------------------------------------------------
    Private Function GetData(ByVal num As Integer) As Byte()
        Dim bits As Boolean() = New Boolean(num - 1) {}
        Dim data As Byte() = New [Byte](num - 1) {}
        Dim word As Integer() = New Integer(num - 1) {}

        ' ------------------------------------------------------------------------
        ' Convert data from text boxes
        For Each ctrl As Control In grpData.Controls
            If TypeOf ctrl Is TextBox Then
                Dim x As Integer = Convert.ToInt16(ctrl.Tag)
                If radBits.Checked Then
                    If (x <= bits.GetUpperBound(0)) AndAlso (ctrl.Text <> "") Then
                        bits(x) = Convert.ToBoolean(Convert.ToByte(ctrl.Text))
                    Else
                        Exit For
                    End If
                End If
                If radBytes.Checked Then
                    If (x <= data.GetUpperBound(0)) AndAlso (ctrl.Text <> "") Then
                        data(x) = Convert.ToByte(ctrl.Text)
                    Else
                        Exit For
                    End If
                End If
                If radWord.Checked Then
                    If (x <= data.GetUpperBound(0)) AndAlso (ctrl.Text <> "") Then
                        Try
                            word(x) = Convert.ToInt16(ctrl.Text)
                        Catch generatedExceptionName As SystemException
                            word(x) = Convert.ToUInt16(ctrl.Text)
                        End Try


                    Else
                        Exit For
                    End If
                End If
            End If
        Next
        If radBits.Checked Then
            Dim numBytes As Integer = CByte(num \ 8 + (If(num Mod 8 > 0, 1, 0)))
            data = New [Byte](numBytes - 1) {}
            Dim bitArray As New BitArray(bits)
            bitArray.CopyTo(data, 0)
        End If
        If radWord.Checked Or radChar.Checked Then
            data = New [Byte](num * 2 - 1) {}
            For x As Integer = 0 To num - 1
                Dim dat As Byte() = BitConverter.GetBytes(CShort(IPAddress.HostToNetworkOrder(CShort(word(x)))))
                data(x * 2) = dat(0)
                data(x * 2 + 1) = dat(1)
            Next
        End If
        Return data
    End Function

    ' ------------------------------------------------------------------------
    ' Show values in selected way
    ' ------------------------------------------------------------------------
    Private Sub ShowAs(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBits.CheckedChanged, radBytes.CheckedChanged, radWord.CheckedChanged, radChar.CheckedChanged
        Dim rad As RadioButton
        If TypeOf sender Is RadioButton Then
            rad = DirectCast(sender, RadioButton)
            If rad.Checked = False Then
                Return
            End If
        End If

        Dim bits As Boolean() = New Boolean(0) {}
        Dim word As Integer() = New Integer(0) {}
        Dim _char As String() = New String(0) {}

        ' Convert data to selected data type
        If radBits.Checked = True Then
            Dim bitArray As New BitArray(data)
            bits = New Boolean(bitArray.Count - 1) {}
            bitArray.CopyTo(bits, 0)
        End If
        If radWord.Checked = True Or radChar.Checked Then
            If data.Length < 2 Then
                Return
            End If
            word = New Integer(data.Length \ 2 - 1) {}
            Dim x As Integer = 0
            While x < data.Length
                word(x \ 2) = data(x) * 256 + data(x + 1)
                x = x + 2
            End While
        End If
        'If radChar.Checked = True Then
        '    If data.Length < 2 Then
        '        Return
        '    End If
        '    _char = New String(data.Length \ 2 - 1) {}
        '    Dim x As Integer = 0
        '    While x < data.Length
        '        _char(x \ 2) = Chr(data(x) * 256 + data(x + 1))
        '        x = x + 2
        '    End While
        'End If

        ' ------------------------------------------------------------------------
        ' Put new data into text boxes
        For Each ctrl As Control In grpData.Controls
            If TypeOf ctrl Is TextBox Then
                Dim x As Integer = Convert.ToInt16(ctrl.Tag)
                If radBits.Checked Then
                    If x <= bits.GetUpperBound(0) Then
                        ctrl.Text = Convert.ToByte(bits(x)).ToString()
                        ctrl.Visible = True
                    Else
                        ctrl.Text = ""
                    End If
                End If
                If radBytes.Checked Then
                    If x <= data.GetUpperBound(0) Then
                        ctrl.Text = data(x).ToString()
                        ctrl.Visible = True
                    Else
                        ctrl.Text = ""
                    End If
                End If
                If radWord.Checked Then
                    If x <= word.GetUpperBound(0) Then
                        ctrl.Text = word(x).ToString()
                        ctrl.Visible = True
                    Else
                        ctrl.Text = ""
                    End If
                End If
                If radChar.Checked Then
                    'If x <= _char.GetUpperBound(0) Then
                    '    ctrl.Text = _char(x).ToString()
                    '    ctrl.Visible = True
                    'Else
                    '    ctrl.Text = ""
                    'End If
                    If x <= word.GetUpperBound(0) Then
                        ctrl.Text = Chr(word(x)).ToString()
                        ctrl.Visible = True
                    Else
                        ctrl.Text = ""
                    End If
                End If
            End If
        Next
    End Sub

End Class
'End Namespace