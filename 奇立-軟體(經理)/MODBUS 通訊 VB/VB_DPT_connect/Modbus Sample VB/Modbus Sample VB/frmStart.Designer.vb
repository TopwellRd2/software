﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.label4 = New System.Windows.Forms.Label()
        Me.txtUnit = New System.Windows.Forms.TextBox()
        Me.btnWriteMultipleReg = New System.Windows.Forms.Button()
        Me.btnWriteMultipleCoils = New System.Windows.Forms.Button()
        Me.btnWriteSingleReg = New System.Windows.Forms.Button()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.radChar = New System.Windows.Forms.RadioButton()
        Me.radWord = New System.Windows.Forms.RadioButton()
        Me.radBytes = New System.Windows.Forms.RadioButton()
        Me.radBits = New System.Windows.Forms.RadioButton()
        Me.btnReadInpReg = New System.Windows.Forms.Button()
        Me.btnReadHoldReg = New System.Windows.Forms.Button()
        Me.btnReadDisInp = New System.Windows.Forms.Button()
        Me.label3 = New System.Windows.Forms.Label()
        Me.grpData = New System.Windows.Forms.GroupBox()
        Me.btnWriteSingleCoil = New System.Windows.Forms.Button()
        Me.txtSize = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtStartAdress = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.grpExchange = New System.Windows.Forms.GroupBox()
        Me.btnReadCoils = New System.Windows.Forms.Button()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.txtIP = New System.Windows.Forms.TextBox()
        Me.grpStart = New System.Windows.Forms.GroupBox()
        Me.groupBox1.SuspendLayout()
        Me.grpExchange.SuspendLayout()
        Me.grpStart.SuspendLayout()
        Me.SuspendLayout()
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(13, 27)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(74, 14)
        Me.label4.TabIndex = 25
        Me.label4.Text = "Unit"
        '
        'txtUnit
        '
        Me.txtUnit.Location = New System.Drawing.Point(87, 25)
        Me.txtUnit.Name = "txtUnit"
        Me.txtUnit.Size = New System.Drawing.Size(50, 20)
        Me.txtUnit.TabIndex = 24
        Me.txtUnit.Text = "0"
        Me.txtUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnWriteMultipleReg
        '
        Me.btnWriteMultipleReg.Location = New System.Drawing.Point(573, 76)
        Me.btnWriteMultipleReg.Name = "btnWriteMultipleReg"
        Me.btnWriteMultipleReg.Size = New System.Drawing.Size(87, 35)
        Me.btnWriteMultipleReg.TabIndex = 23
        Me.btnWriteMultipleReg.Text = "Write multiple register"
        '
        'btnWriteMultipleCoils
        '
        Me.btnWriteMultipleCoils.Location = New System.Drawing.Point(573, 28)
        Me.btnWriteMultipleCoils.Name = "btnWriteMultipleCoils"
        Me.btnWriteMultipleCoils.Size = New System.Drawing.Size(87, 34)
        Me.btnWriteMultipleCoils.TabIndex = 22
        Me.btnWriteMultipleCoils.Text = "Write multiple coils"
        '
        'btnWriteSingleReg
        '
        Me.btnWriteSingleReg.Location = New System.Drawing.Point(473, 76)
        Me.btnWriteSingleReg.Name = "btnWriteSingleReg"
        Me.btnWriteSingleReg.Size = New System.Drawing.Size(87, 35)
        Me.btnWriteSingleReg.TabIndex = 21
        Me.btnWriteSingleReg.Text = "Write single register"
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.radChar)
        Me.groupBox1.Controls.Add(Me.radWord)
        Me.groupBox1.Controls.Add(Me.radBytes)
        Me.groupBox1.Controls.Add(Me.radBits)
        Me.groupBox1.Location = New System.Drawing.Point(160, 8)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(87, 102)
        Me.groupBox1.TabIndex = 20
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Show as"
        '
        'radChar
        '
        Me.radChar.Location = New System.Drawing.Point(13, 75)
        Me.radChar.Name = "radChar"
        Me.radChar.Size = New System.Drawing.Size(67, 21)
        Me.radChar.TabIndex = 3
        Me.radChar.Text = "Char"
        '
        'radWord
        '
        Me.radWord.Location = New System.Drawing.Point(13, 54)
        Me.radWord.Name = "radWord"
        Me.radWord.Size = New System.Drawing.Size(67, 21)
        Me.radWord.TabIndex = 2
        Me.radWord.Text = "Word"
        '
        'radBytes
        '
        Me.radBytes.Location = New System.Drawing.Point(13, 36)
        Me.radBytes.Name = "radBytes"
        Me.radBytes.Size = New System.Drawing.Size(67, 20)
        Me.radBytes.TabIndex = 1
        Me.radBytes.Text = "Bytes"
        '
        'radBits
        '
        Me.radBits.Location = New System.Drawing.Point(13, 15)
        Me.radBits.Name = "radBits"
        Me.radBits.Size = New System.Drawing.Size(67, 21)
        Me.radBits.TabIndex = 0
        Me.radBits.Text = "Bits"
        '
        'btnReadInpReg
        '
        Me.btnReadInpReg.Location = New System.Drawing.Point(373, 76)
        Me.btnReadInpReg.Name = "btnReadInpReg"
        Me.btnReadInpReg.Size = New System.Drawing.Size(87, 35)
        Me.btnReadInpReg.TabIndex = 18
        Me.btnReadInpReg.Text = "Read input register"
        '
        'btnReadHoldReg
        '
        Me.btnReadHoldReg.Location = New System.Drawing.Point(373, 28)
        Me.btnReadHoldReg.Name = "btnReadHoldReg"
        Me.btnReadHoldReg.Size = New System.Drawing.Size(87, 34)
        Me.btnReadHoldReg.TabIndex = 17
        Me.btnReadHoldReg.Text = "Read holding register"
        '
        'btnReadDisInp
        '
        Me.btnReadDisInp.Location = New System.Drawing.Point(273, 76)
        Me.btnReadDisInp.Name = "btnReadDisInp"
        Me.btnReadDisInp.Size = New System.Drawing.Size(87, 35)
        Me.btnReadDisInp.TabIndex = 16
        Me.btnReadDisInp.Text = "Read discrete inputs"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(13, 78)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(74, 14)
        Me.label3.TabIndex = 15
        Me.label3.Text = "Size"
        '
        'grpData
        '
        Me.grpData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpData.Location = New System.Drawing.Point(6, 193)
        Me.grpData.Name = "grpData"
        Me.grpData.Size = New System.Drawing.Size(740, 271)
        Me.grpData.TabIndex = 13
        Me.grpData.TabStop = False
        Me.grpData.Text = "Data"
        Me.grpData.Visible = False
        '
        'btnWriteSingleCoil
        '
        Me.btnWriteSingleCoil.Location = New System.Drawing.Point(473, 28)
        Me.btnWriteSingleCoil.Name = "btnWriteSingleCoil"
        Me.btnWriteSingleCoil.Size = New System.Drawing.Size(87, 34)
        Me.btnWriteSingleCoil.TabIndex = 19
        Me.btnWriteSingleCoil.Text = "Write single coil"
        '
        'txtSize
        '
        Me.txtSize.Location = New System.Drawing.Point(87, 78)
        Me.txtSize.Name = "txtSize"
        Me.txtSize.Size = New System.Drawing.Size(50, 20)
        Me.txtSize.TabIndex = 14
        Me.txtSize.Text = "32"
        Me.txtSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(13, 53)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(74, 14)
        Me.label2.TabIndex = 13
        Me.label2.Text = "Start Adress"
        '
        'txtStartAdress
        '
        Me.txtStartAdress.Location = New System.Drawing.Point(87, 51)
        Me.txtStartAdress.Name = "txtStartAdress"
        Me.txtStartAdress.Size = New System.Drawing.Size(50, 20)
        Me.txtStartAdress.TabIndex = 12
        Me.txtStartAdress.Text = "0"
        Me.txtStartAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(13, 28)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(74, 14)
        Me.label1.TabIndex = 7
        Me.label1.Text = "IP Address"
        '
        'grpExchange
        '
        Me.grpExchange.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpExchange.Controls.Add(Me.label4)
        Me.grpExchange.Controls.Add(Me.txtUnit)
        Me.grpExchange.Controls.Add(Me.btnWriteMultipleReg)
        Me.grpExchange.Controls.Add(Me.btnWriteMultipleCoils)
        Me.grpExchange.Controls.Add(Me.btnWriteSingleReg)
        Me.grpExchange.Controls.Add(Me.groupBox1)
        Me.grpExchange.Controls.Add(Me.btnWriteSingleCoil)
        Me.grpExchange.Controls.Add(Me.btnReadInpReg)
        Me.grpExchange.Controls.Add(Me.btnReadHoldReg)
        Me.grpExchange.Controls.Add(Me.btnReadDisInp)
        Me.grpExchange.Controls.Add(Me.label3)
        Me.grpExchange.Controls.Add(Me.txtSize)
        Me.grpExchange.Controls.Add(Me.label2)
        Me.grpExchange.Controls.Add(Me.txtStartAdress)
        Me.grpExchange.Controls.Add(Me.btnReadCoils)
        Me.grpExchange.Location = New System.Drawing.Point(6, 68)
        Me.grpExchange.Name = "grpExchange"
        Me.grpExchange.Size = New System.Drawing.Size(740, 118)
        Me.grpExchange.TabIndex = 15
        Me.grpExchange.TabStop = False
        Me.grpExchange.Text = "Data exhange"
        Me.grpExchange.Visible = False
        '
        'btnReadCoils
        '
        Me.btnReadCoils.Location = New System.Drawing.Point(273, 28)
        Me.btnReadCoils.Name = "btnReadCoils"
        Me.btnReadCoils.Size = New System.Drawing.Size(87, 34)
        Me.btnReadCoils.TabIndex = 11
        Me.btnReadCoils.Text = "Read coils"
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(187, 21)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(86, 28)
        Me.btnConnect.TabIndex = 6
        Me.btnConnect.Text = "Connect"
        '
        'txtIP
        '
        Me.txtIP.Location = New System.Drawing.Point(93, 25)
        Me.txtIP.Name = "txtIP"
        Me.txtIP.Size = New System.Drawing.Size(87, 20)
        Me.txtIP.TabIndex = 5
        Me.txtIP.Text = "127.0.0.1"
        Me.txtIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'grpStart
        '
        Me.grpStart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpStart.Controls.Add(Me.label1)
        Me.grpStart.Controls.Add(Me.btnConnect)
        Me.grpStart.Controls.Add(Me.txtIP)
        Me.grpStart.Location = New System.Drawing.Point(6, 6)
        Me.grpStart.Name = "grpStart"
        Me.grpStart.Size = New System.Drawing.Size(740, 55)
        Me.grpStart.TabIndex = 14
        Me.grpStart.TabStop = False
        Me.grpStart.Text = "Start communication"
        '
        'frmStart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(753, 471)
        Me.Controls.Add(Me.grpData)
        Me.Controls.Add(Me.grpExchange)
        Me.Controls.Add(Me.grpStart)
        Me.Name = "frmStart"
        Me.Text = "ModbusTCP Tester"
        Me.groupBox1.ResumeLayout(False)
        Me.grpExchange.ResumeLayout(False)
        Me.grpExchange.PerformLayout()
        Me.grpStart.ResumeLayout(False)
        Me.grpStart.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents txtUnit As System.Windows.Forms.TextBox
    Private WithEvents btnWriteMultipleReg As System.Windows.Forms.Button
    Private WithEvents btnWriteMultipleCoils As System.Windows.Forms.Button
    Private WithEvents btnWriteSingleReg As System.Windows.Forms.Button
    Private WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents radWord As System.Windows.Forms.RadioButton
    Private WithEvents radBytes As System.Windows.Forms.RadioButton
    Private WithEvents radBits As System.Windows.Forms.RadioButton
    Private WithEvents btnReadInpReg As System.Windows.Forms.Button
    Private WithEvents btnReadHoldReg As System.Windows.Forms.Button
    Private WithEvents btnReadDisInp As System.Windows.Forms.Button
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents grpData As System.Windows.Forms.GroupBox
    Private WithEvents btnWriteSingleCoil As System.Windows.Forms.Button
    Private WithEvents txtSize As System.Windows.Forms.TextBox
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents txtStartAdress As System.Windows.Forms.TextBox
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents grpExchange As System.Windows.Forms.GroupBox
    Private WithEvents btnReadCoils As System.Windows.Forms.Button
    Private WithEvents btnConnect As System.Windows.Forms.Button
    Private WithEvents txtIP As System.Windows.Forms.TextBox
    Private WithEvents grpStart As System.Windows.Forms.GroupBox
    Private WithEvents radChar As System.Windows.Forms.RadioButton
End Class
