﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.SerialPort2 = New System.IO.Ports.SerialPort(Me.components)
        Me.SerialPort3 = New System.IO.Ports.SerialPort(Me.components)
        Me.SerialPort4 = New System.IO.Ports.SerialPort(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.ID1 = New System.Windows.Forms.Label()
        Me.PORT1 = New System.Windows.Forms.Label()
        Me.COM1 = New System.Windows.Forms.Label()
        Me.ID2 = New System.Windows.Forms.Label()
        Me.PORT2 = New System.Windows.Forms.Label()
        Me.COM2 = New System.Windows.Forms.Label()
        Me.ID3 = New System.Windows.Forms.Label()
        Me.PORT3 = New System.Windows.Forms.Label()
        Me.COM3 = New System.Windows.Forms.Label()
        Me.ID4 = New System.Windows.Forms.Label()
        Me.PORT4 = New System.Windows.Forms.Label()
        Me.COM4 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 269)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 12)
        Me.Label14.TabIndex = 274
        Me.Label14.Text = "Update Status"
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(97, 235)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(101, 23)
        Me.Button19.TabIndex = 273
        Me.Button19.Text = "設定Set"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'ComboBox5
        '
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(98, 39)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox5.TabIndex = 272
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(45, 43)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 12)
        Me.Label11.TabIndex = 271
        Me.Label11.Text = "PLC No."
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(97, 176)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox4.TabIndex = 270
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(97, 149)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox3.TabIndex = 269
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(97, 122)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox2.TabIndex = 268
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(98, 66)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBox1.TabIndex = 267
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(62, 205)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(29, 12)
        Me.Label15.TabIndex = 266
        Me.Label15.Text = "站號"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(98, 203)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 22)
        Me.TextBox7.TabIndex = 265
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(98, 264)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(99, 23)
        Me.Button3.TabIndex = 264
        Me.Button3.Text = "Manual Update"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(44, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 12)
        Me.Label7.TabIndex = 263
        Me.Label7.Text = "Stop Bits"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(47, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 12)
        Me.Label6.TabIndex = 262
        Me.Label6.Text = "DataBits"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(59, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 12)
        Me.Label5.TabIndex = 261
        Me.Label5.Text = "Parity"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 12)
        Me.Label4.TabIndex = 260
        Me.Label4.Text = "BaudRate"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(98, 93)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 22)
        Me.TextBox3.TabIndex = 259
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 12)
        Me.Label3.TabIndex = 258
        Me.Label3.Text = "Select COM port"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label9.Location = New System.Drawing.Point(126, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 16)
        Me.Label9.TabIndex = 276
        Me.Label9.Text = "Label9"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 16)
        Me.Label8.TabIndex = 275
        Me.Label8.Text = "Date  / Time"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(261, 67)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(41, 12)
        Me.Label30.TabIndex = 280
        Me.Label30.Text = "數    值"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(307, 62)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(100, 22)
        Me.TextBox8.TabIndex = 279
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(261, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 278
        Me.Label1.Text = "位    置"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(307, 33)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 22)
        Me.TextBox1.TabIndex = 277
        '
        'Timer1
        '
        Me.Timer1.Interval = 2000
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(431, 62)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 281
        Me.Button1.Text = "讀取"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(261, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 283
        Me.Label2.Text = "C R C"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(307, 87)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 22)
        Me.TextBox2.TabIndex = 282
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(261, 119)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 12)
        Me.Label10.TabIndex = 285
        Me.Label10.Text = "Check"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(307, 114)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(100, 22)
        Me.TextBox4.TabIndex = 284
        '
        'ID1
        '
        Me.ID1.AutoSize = True
        Me.ID1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID1.Location = New System.Drawing.Point(260, 191)
        Me.ID1.Name = "ID1"
        Me.ID1.Size = New System.Drawing.Size(20, 13)
        Me.ID1.TabIndex = 288
        Me.ID1.Text = "ID"
        '
        'PORT1
        '
        Me.PORT1.AutoSize = True
        Me.PORT1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT1.Location = New System.Drawing.Point(260, 170)
        Me.PORT1.Name = "PORT1"
        Me.PORT1.Size = New System.Drawing.Size(38, 13)
        Me.PORT1.TabIndex = 287
        Me.PORT1.Text = "PORT"
        '
        'COM1
        '
        Me.COM1.AutoSize = True
        Me.COM1.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM1.Location = New System.Drawing.Point(260, 149)
        Me.COM1.Name = "COM1"
        Me.COM1.Size = New System.Drawing.Size(35, 13)
        Me.COM1.TabIndex = 286
        Me.COM1.Text = "COM"
        '
        'ID2
        '
        Me.ID2.AutoSize = True
        Me.ID2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID2.Location = New System.Drawing.Point(260, 266)
        Me.ID2.Name = "ID2"
        Me.ID2.Size = New System.Drawing.Size(20, 13)
        Me.ID2.TabIndex = 291
        Me.ID2.Text = "ID"
        '
        'PORT2
        '
        Me.PORT2.AutoSize = True
        Me.PORT2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT2.Location = New System.Drawing.Point(260, 245)
        Me.PORT2.Name = "PORT2"
        Me.PORT2.Size = New System.Drawing.Size(38, 13)
        Me.PORT2.TabIndex = 290
        Me.PORT2.Text = "PORT"
        '
        'COM2
        '
        Me.COM2.AutoSize = True
        Me.COM2.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM2.Location = New System.Drawing.Point(260, 224)
        Me.COM2.Name = "COM2"
        Me.COM2.Size = New System.Drawing.Size(35, 13)
        Me.COM2.TabIndex = 289
        Me.COM2.Text = "COM"
        '
        'ID3
        '
        Me.ID3.AutoSize = True
        Me.ID3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID3.Location = New System.Drawing.Point(260, 338)
        Me.ID3.Name = "ID3"
        Me.ID3.Size = New System.Drawing.Size(20, 13)
        Me.ID3.TabIndex = 294
        Me.ID3.Text = "ID"
        '
        'PORT3
        '
        Me.PORT3.AutoSize = True
        Me.PORT3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT3.Location = New System.Drawing.Point(260, 317)
        Me.PORT3.Name = "PORT3"
        Me.PORT3.Size = New System.Drawing.Size(38, 13)
        Me.PORT3.TabIndex = 293
        Me.PORT3.Text = "PORT"
        '
        'COM3
        '
        Me.COM3.AutoSize = True
        Me.COM3.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM3.Location = New System.Drawing.Point(260, 296)
        Me.COM3.Name = "COM3"
        Me.COM3.Size = New System.Drawing.Size(35, 13)
        Me.COM3.TabIndex = 292
        Me.COM3.Text = "COM"
        '
        'ID4
        '
        Me.ID4.AutoSize = True
        Me.ID4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ID4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ID4.Location = New System.Drawing.Point(260, 415)
        Me.ID4.Name = "ID4"
        Me.ID4.Size = New System.Drawing.Size(20, 13)
        Me.ID4.TabIndex = 297
        Me.ID4.Text = "ID"
        '
        'PORT4
        '
        Me.PORT4.AutoSize = True
        Me.PORT4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.PORT4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PORT4.Location = New System.Drawing.Point(260, 394)
        Me.PORT4.Name = "PORT4"
        Me.PORT4.Size = New System.Drawing.Size(38, 13)
        Me.PORT4.TabIndex = 296
        Me.PORT4.Text = "PORT"
        '
        'COM4
        '
        Me.COM4.AutoSize = True
        Me.COM4.Font = New System.Drawing.Font("新細明體", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.COM4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.COM4.Location = New System.Drawing.Point(260, 373)
        Me.COM4.Name = "COM4"
        Me.COM4.Size = New System.Drawing.Size(35, 13)
        Me.COM4.TabIndex = 295
        Me.COM4.Text = "COM"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(411, 205)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 298
        Me.Button2.Text = "Test Byte"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(896, 433)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ID4)
        Me.Controls.Add(Me.PORT4)
        Me.Controls.Add(Me.COM4)
        Me.Controls.Add(Me.ID3)
        Me.Controls.Add(Me.PORT3)
        Me.Controls.Add(Me.COM3)
        Me.Controls.Add(Me.ID2)
        Me.Controls.Add(Me.PORT2)
        Me.Controls.Add(Me.COM2)
        Me.Controls.Add(Me.ID1)
        Me.Controls.Add(Me.PORT1)
        Me.Controls.Add(Me.COM1)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.TextBox8)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.ComboBox5)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.Label3)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents SerialPort2 As System.IO.Ports.SerialPort
    Friend WithEvents SerialPort3 As System.IO.Ports.SerialPort
    Friend WithEvents SerialPort4 As System.IO.Ports.SerialPort
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ID1 As System.Windows.Forms.Label
    Friend WithEvents PORT1 As System.Windows.Forms.Label
    Friend WithEvents COM1 As System.Windows.Forms.Label
    Friend WithEvents ID2 As System.Windows.Forms.Label
    Friend WithEvents PORT2 As System.Windows.Forms.Label
    Friend WithEvents COM2 As System.Windows.Forms.Label
    Friend WithEvents ID3 As System.Windows.Forms.Label
    Friend WithEvents PORT3 As System.Windows.Forms.Label
    Friend WithEvents COM3 As System.Windows.Forms.Label
    Friend WithEvents ID4 As System.Windows.Forms.Label
    Friend WithEvents PORT4 As System.Windows.Forms.Label
    Friend WithEvents COM4 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button

End Class
