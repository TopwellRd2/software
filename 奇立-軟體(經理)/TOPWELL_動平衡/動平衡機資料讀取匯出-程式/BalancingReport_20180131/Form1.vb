﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.Data.SqlClient
'Imports Microsoft.SqlServer.Dts.Runtime     'SQL連線方式

Public Class Form1

    Dim line As Integer = 0
    Dim conn = New SqlConnection                                                         '設"conn"為新的SQL連線
    Dim strinsert As String
    Dim response(500) As Byte                                                            '讀取的資料暫存區
    Dim presponse(500) As Byte                                                           '讀取的資料(上一筆)
    Dim data(500) As String                                                              '動平衡機解譯後資料
    Dim pdata(500) As String                                                             '(上一筆)動平衡機解譯後資料
    Dim len As Integer = 0

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ''MessageBox("關閉")
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            Application.Run()
        End If
        conn.Close()
        SerialPort1.Close()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '預設顯示 虛擬Port COM1
        For Each sp As String In SerialPort.GetPortNames()
            ComboBox1.Items.Add(sp)
        Next
        ComboBox1.Sorted = True     '排序
        ComboBox1.SelectedIndex = 0 '第一個是預設選項

        ''預設顯示 Parity "Even"
        ComboBox2.Items.Add("None")
        ComboBox2.Items.Add("Odd")
        ComboBox2.Items.Add("Even")
        ComboBox2.SelectedIndex = 0

        '預設顯示 DataBits "7"
        ComboBox3.Items.Add("8")
        ComboBox3.Items.Add("7")
        ComboBox3.Items.Add("6")
        ComboBox3.SelectedIndex = 0

        '預設顯示 Stop Bits "2"
        ComboBox4.Items.Add("1")
        ComboBox4.Items.Add("1.5")
        ComboBox4.Items.Add("2")
        ComboBox4.SelectedIndex = 0

        '預設顯示 BaudRate "2400"
        TextBox11.Text = "2400"

        Thread.Sleep(4000)                                                                                                                       '緩衝開機啟動SQL還沒Ready

        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=PES-ACER\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=SB1708-04-PC\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源

        Try
            conn.Open()                                                                                                                          '開啟SQL連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
        Setport()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click                                                            '解析讀取資料
        '上一筆資料寫入TextBox
        Analysis()
        Dim lst, rst As String
        TextBox3.Text = data(0)                                                                                                                  '轉速
        TextBox4.Text = data(1)                                                                                                                  '左不平衡量
        TextBox5.Text = data(2)                                                                                                                  '左不平衡角度
        TextBox6.Text = data(3)                                                                                                                  '右不平衡量
        TextBox7.Text = data(4)                                                                                                                  '右不平衡角度
        TextBox8.Text = data(498)                                                                                                                '時間
        TextBox1.Text = data(499)                                                                                                                '案號
        TextBox2.Text = data(500)                                                                                                                'Device ID

        'If TextBox8.Text = "" Then
        '    TextBox8.Text = DateTime.Now
        'End If

        Select Case data(17)
            Case "0.0000"
                lst = "GO"
            Case "1.0000"
                lst = "NG"
            Case "2.0000"
                lst = "OVER"
        End Select
        Select Case data(18)
            Case "0.0000"
                rst = "GO"
            Case "1.0000"
                rst = "NG"
            Case "2.0000"
                rst = "OVER"
        End Select
        TextBox9.Text = lst
        TextBox10.Text = rst

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click                                                            '資料寫入SQL資料庫
        Dim ss As String
        Dim tmp As TextBox
        For i As Integer = 1 To 10
            tmp = Me.Controls("TextBox" & (i).ToString)
            If i = 1 Then                                                                                                                        '資料以一列表示
                ss = "'" & tmp.Text & "'"
            Else
                ss = ss & ",'" & tmp.Text & "'"
            End If
        Next
        ss = ss & ",'" & DateTime.Today.Year() & "/" & DateTime.Today.Month() & "/" & DateTime.Today.Day() & "'"

        strinsert = "INSERT INTO 動平衡資料.dbo.RawData (案號,Device_SN,轉速,左不平衡量,左不平衡角度,右不平衡量,右不平衡角度,時間,左面結果,右面結果,日期)" '預寫入之Table
        strinsert = strinsert & " Values(" & ss & ")"                                                                                            '預定填入之資料
        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                                                                                 '定義cmnd為SqlCommand指令
        cmnd.ExecuteNonQuery()                                                                                                                   '資料更新
        len = 0
        data(498) = TextBox8.Text                                                                                                                '紀錄上一筆資料用 時間
        data(499) = TextBox1.Text                                                                                                                '紀錄上一筆資料用 案號
        data(500) = TextBox2.Text                                                                                                                '紀錄上一筆資料用 Device ID
        For i As Integer = 1 To 10
            tmp = Me.Controls("TextBox" & (i).ToString)
            tmp.Text = ""
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        strinsert = "SELECT 案號,Device_SN,時間 as 日期,轉速,左不平衡量,左不平衡角度,右不平衡量,右不平衡角度,左面結果,右面結果 "                         '需要匯出之欄位
        strinsert = strinsert & "FROM [動平衡資料].[dbo].[RawData] "                                                                              '資料來源
        strinsert = strinsert & "where 日期 = '" & DateTime.Today.Year() & "/" & DateTime.Today.Month() & "/" & DateTime.Today.Day() & "'"       '當天日期"
        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                                                                                 '定義cmnd為SqlCommand指令
        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)

        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        Dim FileName As String = DateTime.Today.Year() & DateTime.Today.Month() & DateTime.Today.Day() & ".csv"
        Dim FilePath As String = SavePath + FileName

        Try
            Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)                                                  '建立CSV檔
            '寫入欄位名稱
            If set1.Columns.Count > 0 Then
                sw.Write(set1.Columns.Item(0).ColumnName.ToString)
            End If
            For i As Integer = 1 To set1.Columns.Count - 1
                sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
            Next
            sw.Write(sw.NewLine)

            '寫入各欄位資料
            For i As Integer = 0 To set1.Rows.Count - 1
                For j As Integer = 0 To set1.Columns.Count - 1
                    If j = 0 Then
                        sw.Write(set1.Rows(i)(j))
                    Else
                        sw.Write("," + set1.Rows(i)(j).ToString)
                    End If
                Next
                sw.Write(sw.NewLine)
            Next
            sw.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "檔案已開啟")
        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Setport()
        SerialPort1.Close()
        conn.close()
        Try
            conn.Open()                                                                                                                          '開啟SQL連線
            SerialPort1.Open()
            TextBox12.Text = " SQL & Serial Port 已經正確建立連接!"
        Catch ex As Exception
            MessageBox.Show(ex.Message, "SQL or Serial port 連接錯誤")
        End Try
        'Try
        '    SerialPort1.Open()                                                                                                                          '開啟SQL連線
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Serial port 連接錯誤")
        'End Try

    End Sub

    Public Sub SerialPortSet(ByRef sp As SerialPort, no As Integer, com As String, br As String, pa As String, db As String, sb As String) 'Set PLC No. Serial Port
        sp.Close()
        sp.PortName = com
        sp.BaudRate = br                                                                 'Set BaudRate
        sp.Parity = pa                                                                   'Set Parity
        sp.DataBits = db                                                                 'Set DataBits
        sp.StopBits = sb                                                                 'Set Stop Bits
        Try
            If sp.IsOpen = False Then
                sp.Open()
                TextBox12.Text = ComboBox1.Text & "  已經正確建立連接!"
                Timer1.Enabled = True
            End If
        Catch ex As Exception
            MsgBox("PLC No." & no & " : " & sp.PortName.ToString & " 開啟失敗 , 通訊埠 " & com & " 不存在 / 重複開啟 .", MsgBoxStyle.Critical)
        End Try
        sp.Close()
    End Sub

    Public Sub updata()                                                                  '讀取設備資料(動平衡停止時會自動傳送資料)
        '開啟Serialport & Clear out/in buffers
        If SerialPort1.IsOpen = False Then
            SerialPort1.Open()
            SerialPort1.DiscardOutBuffer()                                               'Clear out buffers
            SerialPort1.DiscardInBuffer()                                                'Clear in buffers
        End If

        If len < 300 Then                                                                '因資料可能一次沒有抓完,所以第二次抓取的不計錄(第一次抓取的已有所需要資料)
            Dim i As Integer = 0
            If SerialPort1.BytesToRead > 300 Then                                        '一直等到長度大於300,才把資料從記憶體搬出
                While (SerialPort1.BytesToRead <> 0)                                     '解析設備回覆的字串
                    response(i) = Byte.Parse(SerialPort1.ReadByte())
                    i += 1
                End While
                len = i - 1
            End If
        End If
    End Sub

    Public Sub Analysis()                                                                '分析原始資料
        Dim zs As String = ""
        Dim j As Integer = 0
        For ii = 0 To 500
            If response(ii) <> "0" Then
                zs = zs & Chr(response(ii))
            Else
                data(j) = zs
                j = j + 1
                zs = ""
            End If
        Next
        'If CInt(data(0)) <> "0" Then
        '    For i = 0 To 500
        '        presponse(i) = response(i)
        '    Next
        'End If
    End Sub

    Public Sub showdata()                                                                '秀出資料
        If len > 0 Then
            '上一筆資料寫入TextBox
            Analysis()
            Dim lst, rst As String
            TextBox3.Text = data(0)
            TextBox4.Text = data(1)
            TextBox5.Text = data(2)
            TextBox6.Text = data(3)
            TextBox7.Text = data(4)
            If TextBox8.Text = "" Or len = 0 Then
                TextBox8.Text = DateTime.Now
            End If

            Select Case data(17)
                Case "0.0000"
                    lst = "GO"
                Case "1.0000"
                    lst = "NG"
                Case "2.0000"
                    lst = "OVER"
            End Select
            Select Case data(18)
                Case "0.0000"
                    rst = "GO"
                Case "1.0000"
                    rst = "NG"
                Case "2.0000"
                    rst = "OVER"
            End Select
            TextBox9.Text = lst
            TextBox10.Text = rst
            If CInt(data(0)) <> "0" Then
                For i = 0 To 500
                    presponse(i) = response(i)
                Next
            End If
            'If presponse(0) <> response(0) Then
            '    If presponse(1) <> response(1) Then
            '        If presponse(10) <> response(10) Then
            '            For i = 0 To 300
            '                presponse(i) = response(i)
            '            Next
            '        End If
            '    End If
            'End If
        End If
    End Sub

    Public Sub Setport()                                                                                                                         '設定Serial Port
        Dim pa, db, sb As Integer
        Dim spn As SerialPort
        Select Case ComboBox2.Text
            Case "None"
                pa = 0
            Case "Odd"
                pa = 1
            Case "Even"
                pa = 2
        End Select

        Select Case ComboBox3.Text
            Case "6"
                db = 6
            Case "7"
                db = 7
            Case "8"
                db = 8
        End Select

        Select Case ComboBox4.Text
            Case "1"
                sb = 1
            Case "1.5"
                sb = 3
            Case "2"
                sb = 2
        End Select
        spn = SerialPort1
        Call SerialPortSet(spn, 1, ComboBox1.Text, TextBox11.Text, pa, db, sb)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick                                                                'timer自動執行程式
        updata()                                                                                                                                 '定期讀取資料
        showdata()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        len = 0
        If TextBox8.Text <> "" Then
            data(498) = TextBox8.Text                                                                                                                '紀錄上一筆資料用 時間
            data(499) = TextBox1.Text                                                                                                                '紀錄上一筆資料用 案號
            data(500) = TextBox2.Text                                                                                                                '紀錄上一筆資料用 Device ID
        End If
        Dim tmp As TextBox
        For i As Integer = 1 To 10
            tmp = Me.Controls("TextBox" & (i).ToString)
            tmp.Text = ""
        Next
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick                                                                '單傳顯示當下時間使用
        Label8.Text = DateTime.Now
    End Sub
End Class
