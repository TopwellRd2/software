--從天源產線將資料備份回192.168.0.201,使用日期來判斷更新,避免重複資料(編號都會重複)造成更新不完全
INSERT INTO [61.216.115.80-奇立備份主機].[DC].[dbo].[DC] 
SELECT * FROM [DC].[dbo].[DC] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [61.216.115.80-奇立備份主機].[DC].[dbo].[DC] A WHERE [備註] = 'C')

INSERT INTO [61.216.115.80-奇立備份主機].[AC].[dbo].[AC] 
SELECT * FROM [AC].[dbo].[AC] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [61.216.115.80-奇立備份主機].[AC].[dbo].[AC] A WHERE [備註] = 'C')

INSERT INTO [61.216.115.80-奇立備份主機].[EBM].[dbo].[EBM] 
SELECT * FROM [EBM].[dbo].[EBM] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [61.216.115.80-奇立備份主機].[EBM].[dbo].[EBM] A WHERE [備註] = 'C')

INSERT INTO [61.216.115.80-奇立備份主機].[FCU].[dbo].[FCU] 
SELECT * FROM [FCU].[dbo].[FCU] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [61.216.115.80-奇立備份主機].[FCU].[dbo].[FCU] A WHERE [備註] = 'C')

--目前只提供奇立製一產線可調整此TABLE,所以由奇立製一產線將資料備份回192.168.0.201,而天源主機從192.168.0.201更新
INSERT INTO [QRCODE].[dbo].[QR_INFO] 
SELECT * FROM [61.216.115.80-奇立備份主機].[QRCODE].[dbo].[QR_INFO] B
WHERE B.[item_no] NOT IN (SELECT A.[item_no] FROM [QRCODE].[dbo].[QR_INFO] A)