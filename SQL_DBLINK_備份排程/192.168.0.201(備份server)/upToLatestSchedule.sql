--從奇立製一產線將資料備份回192.168.0.201,使用日期來判斷更新,避免重複資料(編號都會重複)造成更新不完全
INSERT INTO [DC].[dbo].[DC] 
SELECT * FROM [192.168.3.99-奇立製一產線].[DC].[dbo].[DC] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [DC].[dbo].[DC] A WHERE [備註] IN ('A','B','NA'))

INSERT INTO [AC].[dbo].[AC] 
SELECT * FROM [192.168.3.99-奇立製一產線].[AC].[dbo].[AC] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [AC].[dbo].[AC] A WHERE [備註] IN ('A','B','NA'))

INSERT INTO [EBM].[dbo].[EBM] 
SELECT * FROM [192.168.3.99-奇立製一產線].[EBM].[dbo].[EBM] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [EBM].[dbo].[EBM] A WHERE [備註] IN ('A','B','NA'))

INSERT INTO [FCU].[dbo].[FCU] 
SELECT * FROM [192.168.3.99-奇立製一產線].[FCU].[dbo].[FCU] B
WHERE B.[日期] NOT IN (SELECT A.[日期] FROM [FCU].[dbo].[FCU] A WHERE [備註] IN ('A','B','NA')) 

--目前只提供奇立製一產線可調整此TABLE,所以將資料備份回192.168.0.201,而天源主機由192.168.0.201更新
INSERT INTO [QRCODE].[dbo].[QR_INFO] 
SELECT * FROM [192.168.3.99-奇立製一產線].[QRCODE].[dbo].[QR_INFO] B
WHERE B.[item_no] NOT IN (SELECT A.[item_no] FROM [QRCODE].[dbo].[QR_INFO] A)
