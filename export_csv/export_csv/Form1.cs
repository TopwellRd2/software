﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;


namespace export_csv
{
    public partial class Form1 : Form
    {
        private System.Timers.Timer timer;

        SqlConnection sqlConnection;

        const string FILE_PATH = "D:/CsvTemp";

        DataTable dataTable = new DataTable();

        private delegate void UpdateUI(String str); //宣告委派
        private delegate void CloseWin(); //宣告委派

        public Form1()
        {
            InitializeComponent();
            
            string connectionString = GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            string strSQL =   //依據最後一筆新增的項次,來決定撈最後幾筆資料
                            "SELECT * FROM ( " +
                            "SELECT TOP(CONVERT(INT, (SELECT[項次] " +
                            "                           FROM[A].[dbo].[A] " +
                            "                          WHERE CONVERT(VARCHAR, 時間) = (SELECT MAX([時間]) " +
                            "                                                            FROM [A].[dbo].[A] " +
                            "                                                           WHERE CONVERT(VARCHAR, 日期) = (SELECT MAX([日期]) " +
                            "                                                                                             FROM [A].[dbo].[A] ) ) " +
                            "                                                                                              AND CONVERT(VARCHAR, 日期) = (SELECT MAX([日期]) " +
                            "	                                                                                                                           FROM [A].[dbo].[A])))) " +
                            "        [案號],[測試名稱],[使用控制器],[馬達名稱],[風扇製造商],[箱體尺寸], " +
                            "        [測試電壓],[條碼編號],[馬達規格],[使用風扇],[測試者],[日期],[時間], " +
                            "        [設定轉速],[項次],[目前轉速],[整機風量],[整機消耗功率],[A段壓差], " +
                            "        [B段壓差],[總壓差],[箱內壓差],[電壓],[電流],[控制器電流],[功率因素], " +
                            "        [A風門開度],[B風門開度],[整機效率],[電流諧波],[電壓諧波] " +
                            "        FROM [A].[dbo].[A] " +
                            "        ORDER BY 日期 DESC, 時間 DESC,項次 DESC " + 
                            ") A " + 
                            " ORDER BY 日期, 時間 ";

            try
            {
                SqlCommand command = new SqlCommand(strSQL, sqlConnection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dataTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
                updateText("匯入失敗");
            }

            timer = new System.Timers.Timer();
            timer.Interval = 2000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Tick);

            timer.Start();
        }

        private string GetConnectionString()
        {
            return "Data Source=(local);"
                + "Integrated Security=SSPI;";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;

            if (!Directory.Exists(FILE_PATH))
            {
                Directory.CreateDirectory(FILE_PATH);
            }

            SaveToCSV(dataTable, FILE_PATH);
            updateText("匯出完成");

            Thread.Sleep(1000);
            closeForm();

        }
            public void SaveToCSV(DataTable Table, string FilePath)
        {
            string data = "";
            String FileName = FilePath + "/" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".csv";
            StreamWriter wr = new StreamWriter(FileName, false, System.Text.Encoding.Default);
            foreach (DataColumn column in Table.Columns)
            {
                data += column.ColumnName + ",";
            }
            data += "\n";
            wr.Write(data);

            data = "";
            foreach (DataRow row in Table.Rows)
            {
                foreach (DataColumn column in Table.Columns)
                {
                    data += row[column].ToString().Trim() + ",";
                }
                data += "\n";
                wr.Write(data);
                data = "";
            }
            data += "\n";

            wr.Dispose();
            wr.Close();
        }

        private void closeForm()
        {
            if (this.InvokeRequired)
            {
                CloseWin uu = new CloseWin(closeForm);
                this.Invoke(uu);
            }
            else
            {
                this.Close();
            }
        }

        private void updateText(String str)
        {      
            if (this.InvokeRequired)
            {
                UpdateUI uu = new UpdateUI(updateText);
                this.Invoke(uu, str);
            }
            else
            {
                InfoLabel.Text = str;
            }
        }
    }
}
