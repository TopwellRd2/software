SELECT *
FROM(
SELECT TOP(CONVERT(INT,(SELECT [項次]
FROM [A].[dbo].[A]
WHERE CONVERT(VARCHAR,時間) = (SELECT MAX([時間]) 
                                 FROM [A].[dbo].[A]
								WHERE   CONVERT(VARCHAR,日期) = (SELECT MAX([日期]) 
								                                   FROM [A].[dbo].[A] ) ) 
							      AND CONVERT(VARCHAR,日期) = (SELECT MAX([日期]) 
								                                 FROM [A].[dbo].[A]))))
       [案號],[測試名稱],[使用控制器],[馬達名稱],[風扇製造商],[箱體尺寸],
	   [測試電壓],[條碼編號],[馬達規格],[使用風扇],[測試者],[日期],[時間],
	   [設定轉速],[項次],[目前轉速],[整機風量],[整機消耗功率],[A段壓差],
	   [B段壓差],[總壓差],[箱內壓差],[電壓],[電流],[控制器電流],[功率因素],
	   [A風門開度],[B風門開度],[整機效率],[電流諧波],[電壓諧波]
FROM [A].[dbo].[A]
ORDER BY 日期 desc,時間 desc,項次 desc
) A 
ORDER BY 日期, 時間