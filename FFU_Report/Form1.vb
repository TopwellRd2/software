﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.SqlServer.Dts.Runtime                                                                                                       'SQL連線方式

Public Class Form1
    Dim conn = New SqlConnection                                                                                                              '設"conn"為新的SQL連線
    Dim strinsert As String
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ''預設顯示 Table "AC"
        ComboBox1.Items.Add("AC")                                                                                                             '新增 "AC" table 選擇
        ComboBox1.Items.Add("DC")                                                                                                             '新增 "DC" table 選擇
        ComboBox1.Items.Add("EBM")                                                                                                            '新增 "EBM" table 選擇
        ComboBox1.SelectedIndex = 1                                                                                                           '預設顯示 Table "AC"
        'TextBox1.Text = 1                                                                                                                     '初始化從第一筆開始顯示
        'TextBox2.Text = 30000                                                                                                                 '預設顯示30000筆資料
        'DB.Text = "(local)"
        'DB.Text = Environment.MachineName
        DB.Text = "192.168.3.99"
        from_DateTimePicker.Value = Now().ToString("yyyy/MM/dd 00:00:00")
        to_DateTimePicker.Value = Now().ToString("yyyy/MM/dd 23:59:59")
        conn.ConnectionString = "Data Source=" & DB.Text & ";Initial Catalog=" & ComboBox1.Text & ";Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=192.168.3.99;Initial Catalog=" & ComboBox1.Text & ";Persist Security Info=True;User ID=sa;Password=12"
        Try
            conn.Open()                                                                                                                       '開啟SQL連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
    End Sub

    'Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed                                        '關閉視窗確認功能
    '    If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
    '        System.Windows.Forms.Application.Run()
    '    End If
    'End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing                                     '關閉視窗出現確認畫面
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        Dim table As String = ComboBox1.Text
        conn.Close()    '先切斷連線再測試
        'conn.ConnectionString = "Data Source=WORK_PC\SQLEXPRESS;Initial Catalog=" & table & ";Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        conn.ConnectionString = "Data Source=" & DB.Text & ";Initial Catalog=" & table & ";Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=192.168.3.99;Initial Catalog=" & ComboBox1.Text & ";Persist Security Info=True;User ID=sa;Password=12"
        Try
            conn.Open()                                                                  '開啟連線
            MessageBox.Show("已經正確建立連接!", "連接正確")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        Dim table As String = ComboBox1.Text
        conn.Close()    '先切斷連線再測試
        conn.ConnectionString = "Data Source=" & DB.Text & ";Initial Catalog=" & table & ";Persist Security Info=True;User ID=sa;Password=12"   '指定SQL Server MDF檔來源
        Try
            conn.Open()                                                                  '開啟連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
        strinsert = "WITH OrderedOrders AS ( SELECT *, ROW_NUMBER() OVER (ORDER BY [日期] + [時間]) AS RowNumber "                                              '資料查詢加上ndex
        strinsert = strinsert & "FROM [" & table & "].[dbo].[" & table & "] "                                                      '資料來源
        strinsert = strinsert & "where [產品代碼] like '%" & TextBox0.Text & "%' ) "                                                                     '案號篩選
        strinsert = strinsert & "SELECT DISTINCT * FROM  OrderedOrders "
        'strinsert = strinsert & "where RowNumber BETWEEN " & TextBox1.Text & " and " & TextBox2.Text
        strinsert = strinsert & "WHERE [日期] + ' ' + [時間] BETWEEN '" & from_DateTimePicker.Value.ToString("yyyy/MM/dd HH:mm:ss") & "' AND '" & to_DateTimePicker.Value.ToString("yyyy/MM/dd HH:mm:ss") & "'"
        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)
        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)

        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        If Not IO.Directory.Exists(SavePath) Then
            '如不存在，建立資料夾
            IO.Directory.CreateDirectory(SavePath)
        End If
        'Dim FileName As String = DateTime.Today.Year()
        ' ''為了使輸出檔案名稱為yyyymmdd.csv
        'Dim dl As String
        'dl = DateTime.Today.Month()
        'If Len(dl) = 1 Then                                                             '套入今天之月份
        '    FileName = FileName & "0" & dl
        'Else
        '    FileName = FileName & dl
        'End If
        'dl = DateTime.Today.Day()
        'If Len(dl) = 1 Then                                                             '套入今天之日期
        '    FileName = FileName & "0" & dl & ".csv"
        'Else
        '    FileName = FileName & dl & ".csv"
        'End If

        Dim stime As String
        stime = Format(DateTime.Today, "yyyyMMdd") + "_" + Format(Now, "HHmmss") + ".csv"
        Dim ss As String = TextBox0.Text
        If Len(ss) = 0 Then
            ss = "All"
        End If
        Dim FilePath As String = SavePath + ss + "_" + stime

        Try
            Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)                                                  '建立CSV檔
            '寫入欄位名稱
            If set1.Columns.Count > 0 Then
                sw.Write(set1.Columns.Item(0).ColumnName.ToString)
            End If
            For i As Integer = 1 To set1.Columns.Count - 1
                sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
            Next
            sw.Write(sw.NewLine)

            '寫入各欄位資料
            For i As Integer = 0 To set1.Rows.Count - 1
                For j As Integer = 0 To set1.Columns.Count - 1
                    If j = 0 Then
                        sw.Write(set1.Rows(i)(j))
                    Else
                        sw.Write("," + set1.Rows(i)(j).ToString)
                    End If
                Next
                sw.Write(sw.NewLine)
            Next
            sw.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
            Return
        End Try
        MessageBox.Show("匯出完成,已匯出至 C:\Temp\", "匯出成功")
    End Sub

End Class
