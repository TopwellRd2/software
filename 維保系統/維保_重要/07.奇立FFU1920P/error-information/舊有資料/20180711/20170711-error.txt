如需叫用 Just-In-Time (JIT) 偵錯的詳細資料，
請參閱本訊息結尾處 (而非這個對話方塊) 的資訊。

************** 例外狀況文字 **************
System.IndexOutOfRangeException: 索引在陣列的界限之外。
   於 System.Data.Common.StringStorage.Get(Int32 recordNo)
   於 System.Data.DataRow.get_Item(String columnName)
   於 FFU_1920P.FrmGateway通訊.計算某Gateway下FFU數量(String 名稱)
   於 FFU_1920P.FrmGateway通訊.更新目前DT內容()
   於 FFU_1920P.FrmGateway通訊.查詢(Boolean 是否卷軸歸零)
   於 FFU_1920P.FrmGateway通訊.Timer1_Tick(Object sender, EventArgs e)
   於 System.Windows.Forms.Timer.OnTick(EventArgs e)
   於 System.Windows.Forms.Timer.TimerNativeWindow.WndProc(Message& m)
   於 System.Windows.Forms.NativeWindow.Callback(IntPtr hWnd, Int32 msg, IntPtr wparam, IntPtr lparam)


************** 已載入的組件 **************
mscorlib
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2600.0 built by: NET471REL1LAST
    程式碼庫: file:///C:/Windows/Microsoft.NET/Framework64/v4.0.30319/mscorlib.dll
----------------------------------------
FFU(1920P)
    組件版本: 1.0.0.0
    Win32 版本: 1.0.0.0
    程式碼庫: file:///C:/%E5%A5%87%E7%AB%8BFFU1920P/FFU(1920P).exe
----------------------------------------
Microsoft.VisualBasic
    組件版本: 10.0.0.0
    Win32 版本: 14.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/Microsoft.VisualBasic/v4.0_10.0.0.0__b03f5f7f11d50a3a/Microsoft.VisualBasic.dll
----------------------------------------
System
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System/v4.0_4.0.0.0__b77a5c561934e089/System.dll
----------------------------------------
System.Core
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2600.0 built by: NET471REL1LAST
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Core/v4.0_4.0.0.0__b77a5c561934e089/System.Core.dll
----------------------------------------
System.Windows.Forms
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Windows.Forms/v4.0_4.0.0.0__b77a5c561934e089/System.Windows.Forms.dll
----------------------------------------
System.Drawing
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Drawing/v4.0_4.0.0.0__b03f5f7f11d50a3a/System.Drawing.dll
----------------------------------------
System.Configuration
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Configuration/v4.0_4.0.0.0__b03f5f7f11d50a3a/System.Configuration.dll
----------------------------------------
System.Xml
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Xml/v4.0_4.0.0.0__b77a5c561934e089/System.Xml.dll
----------------------------------------
System.Runtime.Remoting
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Runtime.Remoting/v4.0_4.0.0.0__b77a5c561934e089/System.Runtime.Remoting.dll
----------------------------------------
System.Data
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_64/System.Data/v4.0_4.0.0.0__b77a5c561934e089/System.Data.dll
----------------------------------------
mscorlib.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/mscorlib.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/mscorlib.resources.dll
----------------------------------------
System.Data.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Data.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/System.Data.resources.dll
----------------------------------------
System.Transactions
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_64/System.Transactions/v4.0_4.0.0.0__b77a5c561934e089/System.Transactions.dll
----------------------------------------
System.EnterpriseServices
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_64/System.EnterpriseServices/v4.0_4.0.0.0__b03f5f7f11d50a3a/System.EnterpriseServices.dll
----------------------------------------
System.Numerics
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Numerics/v4.0_4.0.0.0__b77a5c561934e089/System.Numerics.dll
----------------------------------------
ModbusTCPMaster
    組件版本: 1.0.0.2
    Win32 版本: 1.0.0.2
    程式碼庫: file:///C:/%E5%A5%87%E7%AB%8BFFU1920P/ModbusTCPMaster.DLL
----------------------------------------
Modbus
    組件版本: 1.0.0.1
    Win32 版本: 1.0.0.1
    程式碼庫: file:///C:/%E5%A5%87%E7%AB%8BFFU1920P/Modbus.DLL
----------------------------------------
log4net
    組件版本: 1.2.10.0
    Win32 版本: 1.2.10.0
    程式碼庫: file:///C:/%E5%A5%87%E7%AB%8BFFU1920P/log4net.DLL
----------------------------------------
System.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/System.resources.dll
----------------------------------------
System.Windows.Forms.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2556.0 built by: NET471REL1
    程式碼庫: file:///C:/WINDOWS/Microsoft.Net/assembly/GAC_MSIL/System.Windows.Forms.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/System.Windows.Forms.resources.dll
----------------------------------------

************** JIT 偵錯 **************
若要啟用 Just-In-Time (JIT) 偵錯功能，則必須在
此應用程式或電腦的 .config 檔案中，設定
system.windows.forms 區段內的 jitDebugging 值。
且該應用程式也必須在啟用偵錯的狀態下進行
編譯。

例如:

<configuration>
    <system.windows.forms jitDebugging="true" />
</configuration>

當 JIT 偵錯功能啟用後，會將所有未處理的例外狀況
傳送給電腦上已註冊的 JIT 偵錯工具進行處
理，而不是使用這個對話方塊來處理。


