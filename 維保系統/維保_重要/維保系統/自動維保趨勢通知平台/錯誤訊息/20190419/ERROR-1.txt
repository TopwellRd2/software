如需叫用 Just-In-Time (JIT) 偵錯的詳細資料，
請參閱本訊息結尾處 (而非這個對話方塊) 的資訊。

************** 例外狀況文字 **************
System.IO.FileNotFoundException: 無法載入檔案或組件 'ModbusRTU, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null' 或其相依性的其中之一。 系統找不到指定的檔案。
檔案名稱: 'ModbusRTU, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
   於 SQLWebMonitor.Form1.初始化()
   於 SQLWebMonitor.Form1.Form1_Load(Object sender, EventArgs e)
   於 System.EventHandler.Invoke(Object sender, EventArgs e)
   於 System.Windows.Forms.Form.OnLoad(EventArgs e)
   於 System.Windows.Forms.Form.OnCreateControl()
   於 System.Windows.Forms.Control.CreateControl(Boolean fIgnoreVisible)
   於 System.Windows.Forms.Control.CreateControl()
   於 System.Windows.Forms.Control.WmShowWindow(Message& m)
   於 System.Windows.Forms.Control.WndProc(Message& m)
   於 System.Windows.Forms.ScrollableControl.WndProc(Message& m)
   於 System.Windows.Forms.Form.WmShowWindow(Message& m)
   於 System.Windows.Forms.Form.WndProc(Message& m)
   於 System.Windows.Forms.Control.ControlNativeWindow.OnMessage(Message& m)
   於 System.Windows.Forms.Control.ControlNativeWindow.WndProc(Message& m)
   於 System.Windows.Forms.NativeWindow.Callback(IntPtr hWnd, Int32 msg, IntPtr wparam, IntPtr lparam)

警告: 組件繫結記錄切換為 OFF。
若要記錄組件繫結失敗，請將登錄值 [HKLM\Software\Microsoft\Fusion!EnableLog] (DWORD) 設為 1。
注意: 與組件繫結失敗記錄相關的效能會有部分負面影響。
若要關閉此功能，請移除登錄值 [HKLM\Software\Microsoft\Fusion!EnableLog]。



************** 已載入的組件 **************
mscorlib
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.NET/Framework/v4.0.30319/mscorlib.dll
----------------------------------------
WebMonitor
    組件版本: 1.0.0.0
    Win32 版本: 1.0.0.0
    程式碼庫: file:///C:/SQL%E8%BD%89%E6%8F%9B%E7%A8%8B%E5%BC%8F/WebMonitor.exe
----------------------------------------
Microsoft.VisualBasic
    組件版本: 10.0.0.0
    Win32 版本: 14.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/Microsoft.VisualBasic/v4.0_10.0.0.0__b03f5f7f11d50a3a/Microsoft.VisualBasic.dll
----------------------------------------
System
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System/v4.0_4.0.0.0__b77a5c561934e089/System.dll
----------------------------------------
System.Core
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Core/v4.0_4.0.0.0__b77a5c561934e089/System.Core.dll
----------------------------------------
System.Windows.Forms
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2094.0 built by: NET47REL1LAST
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Windows.Forms/v4.0_4.0.0.0__b77a5c561934e089/System.Windows.Forms.dll
----------------------------------------
System.Drawing
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Drawing/v4.0_4.0.0.0__b03f5f7f11d50a3a/System.Drawing.dll
----------------------------------------
System.Configuration
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Configuration/v4.0_4.0.0.0__b03f5f7f11d50a3a/System.Configuration.dll
----------------------------------------
System.Xml
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Xml/v4.0_4.0.0.0__b77a5c561934e089/System.Xml.dll
----------------------------------------
System.Runtime.Remoting
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Runtime.Remoting/v4.0_4.0.0.0__b77a5c561934e089/System.Runtime.Remoting.dll
----------------------------------------
System.Data
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_32/System.Data/v4.0_4.0.0.0__b77a5c561934e089/System.Data.dll
----------------------------------------
System.Data.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Data.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/System.Data.resources.dll
----------------------------------------
System.Transactions
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_32/System.Transactions/v4.0_4.0.0.0__b77a5c561934e089/System.Transactions.dll
----------------------------------------
System.EnterpriseServices
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_32/System.EnterpriseServices/v4.0_4.0.0.0__b03f5f7f11d50a3a/System.EnterpriseServices.dll
----------------------------------------
mscorlib.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/mscorlib.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/mscorlib.resources.dll
----------------------------------------
System.Windows.Forms.resources
    組件版本: 4.0.0.0
    Win32 版本: 4.7.2046.0 built by: NET47REL1
    程式碼庫: file:///C:/Windows/Microsoft.Net/assembly/GAC_MSIL/System.Windows.Forms.resources/v4.0_4.0.0.0_zh-Hant_b77a5c561934e089/System.Windows.Forms.resources.dll
----------------------------------------

************** JIT 偵錯 **************
若要啟用 Just-In-Time (JIT) 偵錯功能，則必須在
此應用程式或電腦的 .config 檔案中，設定
system.windows.forms 區段內的 jitDebugging 值。
且該應用程式也必須在啟用偵錯的狀態下進行
編譯。

例如:

<configuration>
    <system.windows.forms jitDebugging="true" />
</configuration>

當 JIT 偵錯功能啟用後，會將所有未處理的例外狀況
傳送給電腦上已註冊的 JIT 偵錯工具進行處
理，而不是使用這個對話方塊來處理。


