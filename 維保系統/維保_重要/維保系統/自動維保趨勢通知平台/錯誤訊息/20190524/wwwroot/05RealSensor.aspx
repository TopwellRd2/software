﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="05RealSensor.aspx.vb" Inherits="SQLWeb._05RealSensor" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業
    </title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        var factoryImage = new Image();
        var isImage = false;
        var factoryname = "";
        var nowx = 0, nowy = 0;
        var v1, v2, v3, v4, v5;

        var sensorList = new Array();

        function drawclear() {
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function draw() {
            var fg = document.getElementById("myCanvasFG");
            var ctx = fg.getContext("2d");

            ctx.drawImage(factoryImage, nowx, nowy, 1390, 900, 0, 0, 1390, 900);

            ctx.fillStyle = "#ff3333"; 
            ctx.font = "16Px 新細明體"; 
            
            var temp, x, y,value,stat, pos1, pos2,pos3,pos4;
            console.log("nowx=" + nowx + ",nowy=" + nowy + ",sensor length:" + sensorList.length);
            for (var a = 0; a < sensorList.length ; a++) {
                // 繪製圓形 參數依序為 x、y、半徑r、起始角度、結束角度、順時針繪製
                temp = sensorList[a]; //temp = item.id + ":" + item.x + "," + item.y + "_" + item.value+";"+ item.stat;
                pos1 = temp.indexOf(":");
                pos2 = temp.indexOf(",");
                pos3 = temp.indexOf("_");
                pos4 = temp.indexOf(";");
                x = parseInt(temp.substr(pos1 + 1, pos2 - pos1 - 1)) - nowx;
                y = parseInt(temp.substr(pos2 + 1, pos3 - pos2 - 1)) - nowy;
                value = temp.substr(pos3 + 1, pos4 - pos3 - 1);
                stat = parseInt(temp.substr(pos4 + 1));

                ctx.fillStyle = "#ff3333";
                ctx.fillText(value, x + 12, y + 5); // 數字  

                ctx.beginPath();
                ctx.arc(x, y, 10, 0, Math.PI * 2, true);
                if (stat == 1)
                    ctx.fillStyle = "#aaaaaa";
                else if (stat == 2)
                    ctx.fillStyle = "#000000";
                else if (stat == 3)
                    ctx.fillStyle = "#ff9800";
                else if (stat == 0)
                    ctx.fillStyle = "#ff0000";
                ctx.closePath();
                ctx.fill();

               // if (a < 10)
               //     console.log("a=" + a + ":" + stat);
            }
        }

        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left+ nowx;
            var y = e.pageY - offset.top + nowy;
            //document.getElementById("txtX").innerText = x ;
            //document.getElementById("txtY").innerText = y;
            console.log("x=" + x + ",y=" + y);

            var temp, tx, ty, value, pos1, pos2, pos3, id;
            console.log("sensorList=" + sensorList.length);
            for (var a = 0; a < sensorList.length ; a++) { 
                temp = sensorList[a]; // item.id + ":" + item.x + "," + item.y +"_"+ item.value;
                pos1 = temp.indexOf(":");
                pos2 = temp.indexOf(",");
                pos3 = temp.indexOf("_");
                id = temp.substr(0, pos1);
                console.log("temp=" + temp+",id=" + id);
                tx = parseInt(temp.substr(pos1 + 1, pos2 - pos1 - 1)) ;
                ty = parseInt(temp.substr(pos2 + 1, pos3 - pos2 - 1)) ;
                
                document.getElementById("txtX").innerText = x ;
                document.getElementById("txtY").innerText = y;

                if (x >= (tx-10) && x < (tx + 10) && y >= (ty-10) && y < (ty + 10)) {
                    // document.getElementById("txtName").innerText = id; 
                    getSensorContent(id);
                    return ;
                }                
            }
            document.getElementById("txtName").innerText = "";
            document.getElementById("txtTag").innerText = "";
            document.getElementById("txtValue").innerText ="";
        }

        function getSensorContent(id) {
            var actionUrl;
            actionUrl = "wf.aspx?c=A16&d=" + id;
            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var temp = GetData2(actionUrl); // 抓取json 
            console.log("temp:" + temp);
            // item.id + ":" + item.x + "," + item.y + "_" + item.value + ";" + item.tag + "^" + item.name;
            var tx, ty, name, tag, value, pos1, pos2, pos3, pos4, pos5;
            pos1 = temp.indexOf(":");
            pos2 = temp.indexOf(",");
            pos3 = temp.indexOf("_"); 
            pos4 = temp.indexOf(";");
            pos5 = temp.indexOf("^");
            //id = temp.substr(0, pos1);
            //console.log("temp=" + temp + ",id=" + id);
            tx = temp.substr(pos1 + 1, pos2 - pos1 - 1);
            ty = temp.substr(pos2 + 1, pos3 - pos2 - 1);
            value = temp.substr(pos3 + 1, pos4 - pos3 - 1);
            tag = temp.substr(pos4 + 1, pos5 - pos4 - 1);
            name = temp.substr(pos5 + 1);
            console.log("value=" + value + ",tag=" + tag + ",name=" + name);
            document.getElementById("txtX").innerText = tx;
            document.getElementById("txtY").innerText = ty;
            document.getElementById("txtName").innerText = name;
            document.getElementById("txtTag").innerText = tag;
            document.getElementById("txtValue").innerText = value;
        }

        function GetData2(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.id + ":" + item.x + "," + item.y + "_" + item.value+ ";" + item.tag+ "^" + item.name ;
                          
                        });
                    }
                }
            });
            return temp;
        }


        function CanvasMap_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            console.log(x + ":" + y);
            //console.log("now:" + nowx + ":" + nowy);
            nowx = x - 35;
            if (nowx < 0)
                nowx = 0;
            else if (nowx > 430)
                nowx = 430;
            nowy = y - 22;
            if (nowy < 0)
                nowy = 0;
            else if (nowy > 454)
                nowy = 454;
            nowx *= 20;
            nowy *= 20; 

            drawRadar();
            GetsensorList();
            draw();
        }

        function QueryNewFactory() {
            ReadMap();
            drawRadar();
            sensorList = [];
            GetsensorList();
            draw(); 
        }

        function GetsensorList() {  
            var actionUrl;
            var d = document.getElementById("DDarea").value;
            //var g = document.getElementById("DDclass").value;
            //var h = document.getElementById("DDdevice").value;
            //  h:X , i:Y  
            actionUrl = "wf.aspx?c=A15&d=" + d + "&h=" + nowx + "&i=" + nowy;
            console.log(actionUrl);
             
            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData1(actionUrl); // 抓取json 
            console.log("sensorList:" + sensorList.length)
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料";
        }

        function GetData1(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.id + ":" + item.x + "," + item.y + "_" + item.value+";"+ item.stat;
 
                            result.push(temp); 
                        });
                    }
                }
            });
            return result;
        }

        function ReadMap() { // 廠區
            isImage = false;
            factoryname = document.getElementById("DDarea").value;

            $.blockUI({
                message: '<h1>讀取中...請稍待</h1>'
            });

            if (factoryname == "") return;

            factoryImage.src = "/Area/" + factoryname + ".jpg"; //"1tb.jpg"  //
            factoryImage.onload = function () {
                drawRadar();
            };

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            isImage = true;
        }

        function drawRadar() {
            //if (isImage == false) return;
            var mapfg = document.getElementById("myCanvasMap");
            var ctx = mapfg.getContext("2d");

            ctx.drawImage(factoryImage, 0, 0, 10000, 10000, 0, 0, 500, 500);

            ctx.strokeStyle = "#ff3333";
            ctx.beginPath();
            ctx.rect(nowx / 20, nowy / 20, 69, 45);
            ctx.stroke();
        }
         
        function getAlarmcount() {
            var actionUrl;
            var d = document.getElementById("DDarea").value; 
            actionUrl = "wf.aspx?c=A17&d=" + d;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            sensorList = GetData3(actionUrl); // 抓取json  

            document.getElementById("countTotal").innerText = v1;
            document.getElementById("countNormal").innerText = v2;
            document.getElementById("countAlarm").innerText = v3;
            document.getElementById("countDisable").innerText = v4;
            document.getElementById("countBreak").innerText = v5; 
        }

        function GetData3(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { 
                        $.each(data, function (i, item) {
                            //temp = item.id + ":" + item.x + "," + item.y + "_" + item.value;
                            v1 = item.v1;
                            v2 = item.v2;
                            v3 = item.v3;
                            v4 = item.v4;
                            v5 = item.v5;
                            console.log("v1=" + v1 + ",v2=" + v1 + ",v3=" + v3 + ",v4=" + v4 + ",v5=" + v5);
                            result.push(item.v1);
                            result.push(item.v2);
                            result.push(item.v3);
                            result.push(item.v4);
                            result.push(item.v5);
                        });
                    }
                }
            });
            return result;
        }

        $(document).ready(function () {
            ReadMap(); 
            GetsensorList();
            draw();
            getAlarmcount();
        });
         
        var myVar = setInterval(redrawFunc,15000);
        function redrawFunc() {
            GetsensorList();
            draw();

            getAlarmcount();
        }
    </script>
    <style>
        #myCanvasFG {
            position: static;
            border: 1px solid black;
        }

        #myCanvasMap {
            position: static;
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 1890px">
                <tr>
                    <td style="width: 500px; vertical-align: top">
                        <table>
                            <tr>
                                <td colspan="2">廠區
                                   <asp:DropDownList ID="DDarea" runat="server" Width="160px">
                                   </asp:DropDownList><br /> 
                                    <img id="btnQuery" onclick="QueryNewFactory();" src="images/buttom.jpg" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 點選感應器紅點來顯示詳細訊息<br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <canvas id="myCanvasMap" width="500" height="500" onmousedown="CanvasMap_mouoseup(event)"></canvas>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 130px;">
                                    <table>
                                        <tr> 
                                            <td style="color:blue">總數</td><td><label id="countTotal">0</label></td>
                                        </tr>
                                        <tr> 
                                            <td style="color:red">正常</td><td><label id="countNormal">0</label></td>
                                        </tr>
                                        <tr> 
                                            <td style="color:#ff9800">異常</td><td><label id="countAlarm">0</label></td>
                                        </tr>
                                        <tr> 
                                            <td style="color:#aaaaaa">disable</td><td><label id="countDisable">0</label></td>
                                        </tr>
                                        <tr> 
                                            <td style="color:black">斷線</td><td><label id="countBreak">0</label></td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 370px">
                                    <table>
                                        <tr>
                                            <td style="width: 50px; text-align: right">名稱:</td>
                                            <td><asp:Label ID="txtName" runat="server" Text="" ForeColor="Blue"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px; text-align: right">TAG:</td>
                                            <td><asp:Label ID="txtTag" runat="server" Text="" ForeColor="Blue"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px; text-align: right">X座標:</td>
                                            <td><asp:Label ID="txtX" runat="server" Text="" ForeColor="Blue"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px; text-align: right">Y座標:</td>
                                            <td><asp:Label ID="txtY" runat="server" Text="" ForeColor="Blue"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px; text-align: right">數值:</td>
                                            <td><asp:Label ID="txtValue" runat="server" Text="" ForeColor="Blue"></asp:Label></td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="2"> 
                                    <br />
                                    <a href="AdminSensor.aspx">回感應器管理</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="index.aspx">回首頁</a>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                    <td style="width: 1390px">
                        <canvas id="myCanvasFG" width="1390" height="900" onmousedown="CanvasFG_mouoseup(event)"></canvas>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
