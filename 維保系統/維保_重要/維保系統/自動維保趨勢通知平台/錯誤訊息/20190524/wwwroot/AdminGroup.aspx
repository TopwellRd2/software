﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminGroup.aspx.vb" Inherits="SQLWeb.AdminGroup" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
    <style type="text/css">
        .auto-style1 {
            width: 200px;
            height: 34px;
        }
        .auto-style2 {
            width: 800px;
            height: 34px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 25px">
                                    <a href="Index.aspx">首頁</a> 
                                      >>
                                    <a href="06進階警報管理.aspx">進階警報管理</a>
                                    >>群組推播設定管理
                                </td> 
                            </tr>
                            <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                          
                                            </td>
                                            <td style="width:1000px;height:220px;text-align:left;vertical-align:top;font-size:20px">
                                              
                                                <table>
                                                    <tr>
                                                        <td style="width:200px"></td>
                                                        <td style="width:800px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style1">群組列表:</td>
                                                        <td class="auto-style2">
                                                            <asp:ListBox ID="ListBox1" runat="server" Font-Size="14pt" Height="160px" Width="500px" AutoPostBack="True"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">群組名稱:</td>
                                                        <td style="width:800px">
                                                            <asp:TextBox ID="txtName" runat="server" Font-Size="18pt" Width="487px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">是否推播:</td>
                                                        <td style="width:800px">
                                                            <asp:CheckBox ID="chkIsBroadcast" runat="server" Text="打勾才會在這個群組發MAIL和簡訊" />
                                                        </td>
                                                    </tr>
                                                      <tr>
                                                        <td style="width:200px">可接收人員:</td>
                                                        <td style="width:800px">
                                                            <asp:TextBox ID="txtMember" runat="server" Font-Size="18pt" Width="598px"></asp:TextBox>
                                                            <asp:Button ID="btnAdd" runat="server" BackColor="#FFCC66" Font-Size="16pt" Height="27px" Text="瀏覽加入" Width="120px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">&nbsp;</td>
                                                        <td style="width:800px">
                                                            <asp:Button ID="btnNew" runat="server" Font-Size="16pt" Height="32px" Text="新增" Width="120px" />
                                                            <asp:Button ID="btnSave" runat="server" Font-Size="16pt" Height="32px" Text="儲存" Width="120px" />
                                                            <asp:Button ID="btnDelete" runat="server" Font-Size="16pt" Height="32px" Text="刪除" Width="120px" OnClientClick="return confirm('是否確認刪除？');" />
                                                            <br />
                                                            <asp:Label ID="txtMSG" runat="server" Font-Size="20pt" ForeColor="Red"></asp:Label>
                                                            <br />
                                                            新增: 先輸入完群組名稱後，按新增按鈕。再繼續選擇新增後的資料編輯人員<br />
                                                            儲存: 先選擇一筆資料，修改內容後，按儲存按鈕<br />
                                                            刪除: 先選擇一筆資料，按下刪除鈕</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                            
                                            </td>
                                        </tr>  
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 36px;border:dotted">  
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>