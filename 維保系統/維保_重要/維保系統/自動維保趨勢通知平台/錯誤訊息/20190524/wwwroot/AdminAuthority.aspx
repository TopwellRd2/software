﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminAuthority.aspx.vb" Inherits="SQLWeb.AdminAuthority" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 25px">
                                    <a href="Index.aspx">首頁</a> 
                                      >>
                                    <a href="08管理系統.aspx">管理系統</a>
                                    >>權限管理
                                </td> 
                            </tr>
                            <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                          
                                            </td>
                                            <td style="width:1000px;height:220px;text-align:left;vertical-align:top;font-size:20px">
                                              
                                                <table>
                                                    <tr>
                                                        <td style="width:200px"></td>
                                                        <td style="width:800px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">使用者列表:</td>
                                                        <td style="width:800px">
                                                            <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" Height="160px" Width="460px" Font-Size="14pt"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">
                                                            <asp:Label ID="txtID" runat="server" Font-Size="20pt" ForeColor="Red" Visible="False"></asp:Label>
                                                            帳號</td>
                                                        <td style="width:800px">
                                                            <asp:Label ID="txtUserName" runat="server" Font-Size="20pt" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td style="width:200px">打勾才能使用</td>
                                                        <td style="width:800px">
                                                            <asp:CheckBoxList ID="chkList" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                                <asp:ListItem>多曲線雙軌分析</asp:ListItem>
                                                                <asp:ListItem>設備不同時間點分析</asp:ListItem>
                                                                <asp:ListItem>維保趨勢通知</asp:ListItem>
                                                                <asp:ListItem>上下限警報通知</asp:ListItem> 
                                                                <asp:ListItem>帳號管理</asp:ListItem>
                                                                <asp:ListItem>感應器管理</asp:ListItem>
                                                                <asp:ListItem>廠區管理</asp:ListItem>
                                                                <asp:ListItem>設備管理</asp:ListItem>
                                                                <asp:ListItem>課別管理</asp:ListItem>
                                                                <asp:ListItem>Gateway管理</asp:ListItem>
                                                                <asp:ListItem>感應器群組管理</asp:ListItem>
                                                                <asp:ListItem>使用權限管理</asp:ListItem>
                                                                <asp:ListItem>OPC管理</asp:ListItem> 
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">&nbsp;</td>
                                                        <td style="width:800px">
                                                            <asp:Button ID="btnSave" runat="server" Font-Size="16pt" Height="32px" Text="儲存" Width="120px" />
                                                            <br />
                                                            <asp:Label ID="txtMSG" runat="server" Font-Size="20pt" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                            
                                            </td>
                                        </tr>  
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 36px;border:dotted">  
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>