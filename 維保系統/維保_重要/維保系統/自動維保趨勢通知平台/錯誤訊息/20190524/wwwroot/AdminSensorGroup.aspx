﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminSensorGroup.aspx.vb" Inherits="SQLWeb.AdminSensorGroup" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        function openSelectWindow() {
            window.open('selectSensorGroup.aspx', '_blank', 'width=630,height=420,left=400,top=200'); 
        }

    </script>    
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 25px">
                                    <a href="Index.aspx">首頁</a> 
                                      >>
                                    <a href="08管理系統.aspx">管理系統</a>
                                    >>感應器群組設定
                                </td> 
                            </tr>
                           <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                          
                                            </td>
                                            <td style="width:1000px;height:220px;text-align:left;vertical-align:top;font-size:20px">
                                              
                                                <table>
                                                    <tr>
                                                        <td style="width:200px"></td>
                                                        <td style="width:800px">此功能給&nbsp;&nbsp; -- > 多曲線雙時間軌分析&nbsp;
                                                             專用的，可以點連結一次拉一群感應器</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">連結標籤列表:</td>
                                                        <td style="width:800px">
                                                            <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" Height="260px" Width="733px"></asp:ListBox>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">標籤名稱:</td>
                                                        <td style="width:800px">
                                                            <asp:TextBox ID="txtName" runat="server" Font-Size="18pt" Width="297px" Rows="3"></asp:TextBox>&nbsp; 限制1~8個字內</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">內含的感應器:</td>
                                                        <td style="width:800px">
                                                            <asp:TextBox ID="txtSensorList" runat="server" Font-Size="18pt" Width="598px" Enabled="False"></asp:TextBox>
                                                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                            <br /> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">備註:</td>
                                                        <td style="width:800px">
                                                            <asp:TextBox ID="txtcomment" runat="server" Font-Size="18pt" Width="598px"></asp:TextBox>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">&nbsp;</td>
                                                        <td style="width:800px">
                                                            <asp:Button ID="btnNew" runat="server" Font-Size="16pt" Height="32px" Text="新增" Width="120px" />
                                                            <asp:Button ID="btnSave" runat="server" Font-Size="16pt" Height="32px" Text="儲存" Width="120px" />
                                                            <asp:Button ID="btnDelete" runat="server" Font-Size="16pt" Height="32px" Text="刪除" Width="120px" OnClientClick="return confirm('是否確認刪除？');" />
                                                        &nbsp;
                                                        
                                                            <br />
                                                            新增:&nbsp; 輸入完標籤名稱後，按下新增按鈕<br />
                                                            儲存: 點選某筆資料，修改完後，按儲存按鈕<br />
                                                            刪除: 點選某筆資料，按刪除按鈕<br />
                                                            <asp:Label ID="txtMsg" runat="server" ForeColor="#CC3300"></asp:Label>
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                            
                                            </td>
                                        </tr>  
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 36px;border:dotted">  
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>