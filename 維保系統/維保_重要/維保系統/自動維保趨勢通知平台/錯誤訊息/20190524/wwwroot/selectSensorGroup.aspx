﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="selectSensorGroup.aspx.vb" Inherits="SQLWeb.selectSensorGroup" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        //function openSelectWindow() {
        //    window.open('selectSensorGroup.aspx', '_blank', 'width=630,height=420,left=400,top=200');
        //}
         
        //function getParam() {
        //    // document.getElementById("TextBox3").value = "1,2,3";
        //    var result = document.getElementById("txtList").value;
        //    // var b = opener.document.getElementById("ttt111").innerText;
        //    opener.document.getElementById("ttt111").innerText = result; //.innerText = result;
        //    var a = opener.document.getElementById("ttt111").innerText;
        //    //var a = document.getElementById("TextBox3");
        //    //var b = document.getElementById("TextBox3").value;
        //    console.log(a + ":" + result);
        //    window.close();
        //}
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 200px;
            height: 24px;
        }
        .auto-style2 {
            width: 800px;
            height: 24px;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 25px">
                                    <a href="Index.aspx">首頁</a> 
                                      >>
                                    <a href="08管理系統.aspx">管理系統</a>
                                    >>
                                    <a href="AdminSensorGroup.aspx">感應器群組設定</a>
                                    >> 設定群組內含的感應器
                                </td> 
                            </tr>
                           <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                          
                                            </td>
                                            <td style="width:1000px;height:220px;text-align:left;vertical-align:top;font-size:20px">
                                              
                                                <table>
                                                    <tr>
                                                        <td style="width:200px"></td>
                                                        <td style="width:800px">此功能給&nbsp;&nbsp; -- > 多曲線雙時間軌分析&nbsp;
                                                             專用的，可以點連結一次拉一群感應器</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="width:200px">群組名稱:</td>
                                                        <td style="width:800px"> 
                                                            <asp:Label ID="txtGroupName" runat="server" Font-Size="X-Large" ForeColor="Maroon"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">群組內感應器列表:</td>
                                                        <td style="width:800px">
                                                          
                        <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" Height="250px" Width="620px" Font-Size="14pt">
                    
                        </asp:ListBox>
                                               
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px">&nbsp;</td>
                                                        <td style="width: 800px">
                                                            <br/>
                                                            <asp:DropDownList ID="DDarea" runat="server" Width="160px" Font-Size="Large" AutoPostBack="True">
                                                                <asp:ListItem Value="1">一廠</asp:ListItem> 
                                                            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            課別 
            <asp:DropDownList ID="DDclass" runat="server" Width="120px" Font-Size="Large" AutoPostBack="True">
                <asp:ListItem Value="1">水務</asp:ListItem> 
            </asp:DropDownList>&nbsp; &nbsp;&nbsp;
            設備 
            <asp:DropDownList ID="DDdevice" runat="server" Width="120px" Font-Size="Large" AutoPostBack="True">
                <asp:ListItem Value="1">純水設備</asp:ListItem> 
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp;  
                                                        </td>
                                                    </tr>
                                                      <tr>
                                                        <td class="auto-style1"></td>
                                                        <td class="auto-style2">
                                                          
                        <asp:ListBox ID="ListBox2" runat="server" AutoPostBack="True" Height="250px" Width="620px" Font-Size="14pt" SelectionMode="Multiple">
                   
                        </asp:ListBox>
                                               
                                                          </td>
                                                    </tr>
                                                      <tr>
                                                        <td style="width:200px">&nbsp;</td>
                                                        <td style="width:800px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px">&nbsp;</td>
                                                        <td style="width:800px">
                                                            <asp:Button ID="btnNew" runat="server" Font-Size="16pt" Height="32px" Text="新增" Width="120px" />
                                                            <asp:Button ID="btnDelete" runat="server" Font-Size="16pt" Height="32px" Text="刪除" Width="120px" OnClientClick="return confirm('是否確認刪除？');" />
                                                        &nbsp;<asp:Button ID="btnSave" runat="server" Font-Size="16pt" Height="32px" Text="儲存" Width="120px" />
                                                            &nbsp;<br />
                                                            新增: 在下方選擇感應器，按下新增加入上方的列表<br />                                                            
                                                            刪除: 點選上方某感應器，按刪除按鈕移除<br />
                                                            儲存: 確認此群組所有感應器後，按儲存按鈕儲存設定，就可以在多曲線時間軸分析頁面使用<br />
                                                            <asp:Label ID="txtMsg" runat="server" ForeColor="#CC3300"></asp:Label>
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                            
                                            </td>
                                        </tr>  
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 36px;border:dotted">  
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>




  
               
             