﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminSensorXY.aspx.vb" Inherits="SQLWeb.AdminSensorXY" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <title>奇立實業
    </title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        var factoryImage = new Image();
        var isImage = false; 
        var factoryname = "";
        var nowx = 0, nowy = 0;

        var sensorList = new Array(); 

        function drawclear() {  
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function draw() {
            var fg = document.getElementById("myCanvasFG");
            var ctx = fg.getContext("2d");

            ctx.drawImage(factoryImage, nowx, nowy, 1390, 900, 0, 0, 1390, 900);
              
            ctx.fillStyle = "#ff3333";
            ctx.beginPath();
            var temp, x, y, pos1, pos2;
            console.log("nowx=" + nowx + ",nowy=" + nowy + ",sensor length:" + sensorList.length);
            for (var a = 0; a < sensorList.length ; a++) {
                // 繪製圓形 參數依序為 x、y、半徑r、起始角度、結束角度、順時針繪製
                temp = sensorList[a]; //item.name + ":" + item.x + "," + item.y;
                pos1 = temp.indexOf(":");
                pos2 = temp.indexOf(",");
                x = parseInt(temp.substr(pos1 + 1, pos2 - pos1 - 1)) - nowx;
                y = parseInt(temp.substr(pos2 + 1)) - nowy;
                ctx.arc(x, y, 15, 0, Math.PI * 2, true);
//console.log("x=" + x + ",y=" + y + ":" + temp);
                ctx.closePath();
                ctx.fill();
            } 
        }

        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;

            document.getElementById("txtX").innerText = x + nowx;
            document.getElementById("txtY").innerText = y + nowy;
        }

        function CanvasMap_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            console.log(x + ":" + y);
            //console.log("now:" + nowx + ":" + nowy);
            nowx = x - 35;
            if (nowx < 0)
                nowx = 0;
            else if (nowx > 430)
                nowx = 430;
            nowy = y - 22;
            if (nowy < 0)
                nowy = 0;
            else if (nowy > 454)
                nowy = 454;
            nowx *= 20;
            nowy *= 20; 
           //console.log("now:" +nowx + ":" + nowy);

            drawRadar(); 
            GetsensorList();
            draw();
        }

        function GetsensorList() {
            var actionUrl = "wf.aspx?c=A13&d=" + factoryname + "&f=" + nowx + "&g=" + nowy;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData2(actionUrl); // 抓取json 
            console.log("sensorList:" + sensorList.length)
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料"; 
        }

        function GetData2(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.name + ":" + item.x + "," + item.y;
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }

        function ReadMap() { // 廠區
            isImage = false; 
            GetMapName();

            $.blockUI({ 
                message: '<h1>讀取中...請稍待</h1>'
            });

            if (factoryname == "") return;

            factoryImage.src = "/Area/" + factoryname+".jpg"; //"1tb.jpg"  //
            factoryImage.onload = function () {
                drawRadar();
            }; 

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            isImage = true;  
        }

        function drawRadar() {
            //if (isImage == false) return;
            var mapfg = document.getElementById("myCanvasMap");
            var ctx = mapfg.getContext("2d");
             
            ctx.drawImage(factoryImage, 0, 0, 10000, 10000, 0, 0, 500, 500);

            ctx.strokeStyle = "#ff3333";
            ctx.beginPath();
            ctx.rect(nowx / 20, nowy / 20, 69, 45);
            ctx.stroke(); 
        }

        function GetMapName() {
            var actionUrl;

            var getUrlString = location.href;  
            var url = new URL(getUrlString); 
            var id = url.searchParams.get('sID');

            if (id == null) {
                console.log("NULL");
                return;
            }
            else
                console.log(id);  
             
            actionUrl = "wf.aspx?c=A12&d=" + id  ; 

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            factoryname = GetData1(actionUrl);  
            console.log("factoryname:" + factoryname)
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms ";
            var a;
            //console.log(details);
  
        }

        function GetData1(actionUrl) {
            var result ;
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            result = item.value; 
                        });
                    }
                }
            });
            return result;
        }
        
        function SaveXY() {
            var x = document.getElementById("txtX").innerText;
            var y = document.getElementById("txtY").innerText;
            var getUrlString = location.href;
            var url = new URL(getUrlString);
            var id = url.searchParams.get('sID');// d:ID, f:x, g:y 

            if (id == null) {
                console.log("NULL");
                return;
            }
            else
                console.log(id);

            actionUrl = "wf.aspx?c=A14&d=" + id+"&f="+x+"&g="+y;

            $.blockUI({ 
                message: '<h1>Saving...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000); 
            var res = GetData3(actionUrl);

            drawRadar(); 
            GetsensorList();
            draw();
        }
         
        function GetData3(actionUrl) {
            var result;
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            result = item.value;
                        });
                    }
                }
            });
            return result;
        }

        $(document).ready(function (){
            ReadMap();

            GetsensorList();
            draw();
        });
    </script>
    <style>
        #myCanvasFG {
            position: static;
            border: 1px solid black;
        }

        #myCanvasMap {
            position: static;
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table style ="width:1890px" >
        <tr>
            <td style ="width:500px;vertical-align:top">
                <table> 
                    <tr>
                        <td colspan="2"> 
                            ** 需要先點選位置，按下"儲存"按鈕才會真的改變感應器位置
                        </td>
                    </tr>
                    <tr>
                        <td style ="width:150px;text-align:right">名稱:</td>
                        <td style ="width:350px">
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="txtName" runat="server" Text="Label" ForeColor="Blue"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style ="width:150px;text-align:right">TAG:</td>
                        <td style ="width:350px">
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="txtTag" runat="server" Text="Label" ForeColor="Blue"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style ="width:150px;text-align:right">X座標:</td>
                        <td style ="width:350px">
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="txtX" runat="server" Text="Label" ForeColor="Blue"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style ="width:150px;text-align:right">Y座標:</td>
                        <td style ="width:350px">
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="txtY" runat="server" Text="Label" ForeColor="Blue"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                           <img id="btnSaveXY" onclick="SaveXY();" src="images/save.jpg" />
                             &nbsp;&nbsp;&nbsp; <a href="AdminSensor.aspx">回感應器管理</a>&nbsp;&nbsp;<a href="index.aspx">回首頁</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <canvas id="myCanvasMap" width="500" height="500" onmousedown="CanvasMap_mouoseup(event)"></canvas>
                        </td>
                    </tr>
                </table>
            </td>
            <td style ="width:1390px">
                <canvas id="myCanvasFG" width="1390" height="900" onmousedown="CanvasFG_mouoseup(event)"></canvas>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
