﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="selectSensorPosition.aspx.vb" Inherits="SQLWeb.selectSensorPosition" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業
    </title> 
    <script type="text/javascript" src="js/jquery-latest.min.js"></script> 
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script> 
        var colors = new Array("blue", "orange", "green", "red", "purple", "brown", "yellow", "gray", "gold", "black");
        var factoryImage = new Image();
        var isImage = false;
        var factoryname = "";
        var scaleW, scaleH; // 和原圖的倍率 
        var nowx, nowy, scrollx, scrolly;
        var scrollstatus = -1; // -1:none, 0:x, 1:y
        var pressposition;
        var canmove = false;
        var addsensor = new Array();

        function drawbg(x,y) { // 放大鏡效果 
            //var fg = document.getElementById("myCanvasFG");
            //var ctxfg = fg.getContext("2d");

            //var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            //var imgData = ctxbg.getImageData(0, 0, 1800, 900)
            //ctxfg.putImageData(imgData, 0, 0);
             
            var bg = document.getElementById("myCanvasBG");
            var ctx = bg.getContext("2d");
            nowx = scrollx * 10;
            nowy = scrolly * 10;
            ctx.drawImage(factoryImage, nowx, nowy, 950, 950, 0, 0, 950, 950);

            var bgmap = document.getElementById("myCanvasMapBG");
            var ctxmap = bgmap.getContext("2d"); 
            ctxmap.drawImage(factoryImage, 0, 0, 10000, 10000, 0, 0, 500, 500);
           // drawScroll(ctx);

            //  ctx.drawImage(img, 10, 100, 50, 60, 10, 10, 50, 60);
            //var scaleW, scaleH; // 和原圖的倍率
          /*  var tx, ty; // 繪製放大圖
            tx = x + 10;
            ty = y + 10;
            if (tx > 750) tx -= 220;
            if (ty > 750) ty -= 220;
            ctx.drawImage(factoryImage, x * scaleW - 25, y * scaleW - 25, 50, 50, tx, ty, 200, 200);

            ctx.strokeStyle = "#3030ff"; //放大鏡外框
            ctx.beginPath();   
            ctx.rect(tx, ty, 200, 200);
            ctx.stroke();

            ctx.fillStyle = "#ff5050"; // 中心紅點
            ctx.beginPath();
            ctx.arc(tx + 100, ty + 100, 10,1 / 180 * Math.PI,359/ 180 * Math.PI,  false);
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
            */ 
            //console.log(x * scaleW + ":" + y * scaleW);
            //console.log(x + ":" + y  ); 
        } 

        function drawScroll(ctx) {
            // console.log("scroll")

            //ctx.strokeStyle = "#777777";
            //ctx.beginPath();
            //ctx.rect(0,0, 935, 15);
            //ctx.stroke();
            //ctx.beginPath();
            //ctx.rect(935, 15, 15, 935);
            //ctx.stroke();

            ctx.fillStyle = "#cccccc";
            ctx.beginPath();
            ctx.fillRect(0, 0, 935, 15);
            ctx.fillRect(935, 15, 15, 935);
            ctx.fillStyle = "#555555";
            ctx.fillRect(scrollx, 1, 30, 13); // 0~904
            ctx.fillRect(936, scrolly + 16, 13, 30); // 0~904 
        }

        //"myCanvasMap" width="500" height="500"></canvas> 
        //            <div style="visibility: hidden;">
        //                <canvas id="myCanvasMapBG"
        function drawRadar() {
            var mapfg = document.getElementById("myCanvasMap");
            var ctxmapfg = mapfg.getContext("2d");

            var ctxmapbg = myCanvasMapBG.getContext("2d");  
            var imgData = ctxmapbg.getImageData(0, 0,500, 500);
            ctxmapfg.putImageData(imgData, 0, 0);

            ctxmapfg.strokeStyle = "#ff3333";
            ctxmapfg.beginPath();
            ctxmapfg.rect(nowx/20, nowy/20, 47,47);
            ctxmapfg.stroke(); 
        }

        function draw() { 
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");

            var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            var imgData = ctxbg.getImageData(0, 0, 950, 950);
            ctxfg.putImageData(imgData, 0, 0);

            drawScroll(ctxfg);

            drawsensor(ctxfg);
        }

        function drawsensor(ctx) {
            var a;
            var x, y, t;
            console.log(addsensor.length);
            ctx.strokeStyle = "#ff3030";
            for (a = 0; a < addsensor.length; a++) {
                //var pos = id.indexOf(":");
                //id = id.substr(0, pos);
                t = addsensor[a];
                x = parseInt(t.substr(0, t.indexOf(":")));
                y = parseInt(t.substr(t.indexOf(":") + 1));

                if (x > nowx && x < (nowx + 950) && y > nowy && y < (nowy + 950)) { 
                    ctx.beginPath();
                    ctx.rect(x-nowx,y-nowy,20,20);
                    ctx.stroke();
                }
                console.log(x + "," + y);
            }
        }

        function ReadMap() { // 讀取廠區圖
            //var factoryImage = new Image();
            //var isImage = false;
            // var factoryname = "";
            isImage=false;

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });
              
            factoryname = document.getElementById("DDarea").value;
            factoryImage.src = "/Area/" + factoryname; //"1tb.jpg"  //

            setTimeout(function () {
                $.unblockUI();
            }, 1000); 
        }

        factoryImage.onload = function () {
            var w = this.width;
            var h = this.height;

           // var scaleW, scaleH; // 和原圖的倍率
            scaleW = w / 950.0; // 這是CANVAS的大小
            scaleH = h / 950.0;
            console.log(w + ":" + h);

            var fg = document.getElementById("myCanvasFG");
            var ctx = fg.getContext("2d");

            // 注意所有這種地方，比例不正確
            //應該是等比例縮小才對
            nowx = 0;
            nowy = 0;
            scrollx=0;
            scrolly=0;
           // ctx.drawImage(this, nowx, nowy, 950, 950,0,0, 950, 950); 
            drawbg(0, 0);
            draw();
             drawRadar();
           // ctx.fillText('Mood', 120, 120);

            isImage = true;
            console.log(isImage); 
        }


        function CanvasFG_mouosedown(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;

            //scrollstatus = -1; // -1:none, 0:x, 1:y
            if (x > scrollx && x < (scrollx + 30) && y < 15)
            { scrollstatus = 0; pressposition = x;}
            else if (y >( scrolly+16) && y < (scrolly + 46) && x >935)
            { scrollstatus = 1; pressposition = y; }
            else 
                scrollstatus = -1;
            //console.log(scrollstatus + ":" + x + "," + y);
        }

        function CanvasFG_mouosemove(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top; 
            var t=0;

            if (scrollstatus == 0) { //x
                if (x > 1 && x < pressposition) {
                    t = pressposition - x;
                    scrollx -=t;
                    if (scrollx < 0) scrollx = 0;
                    pressposition = x;                    
                }
                else if (x < 934 && x > pressposition) {
                    t =x - pressposition;
                    scrollx +=t;
                    if (scrollx > 904) scrollx = 904;
                    pressposition = x;
                }
            }
            else if (scrollstatus == 1) { //y  
                if (y > 16 && y < pressposition) {
                    t =pressposition - y;
                    scrolly -=t;
                    if (scrolly < 0) scrolly = 0;
                    pressposition = y;
                }
                else if (y < 934 && y > pressposition) {
                    t =y - pressposition;
                    scrolly += t;
                    if (scrolly > 904) scrolly = 904;
                    pressposition = y;
                }
            }
            
            if (t > 2) {
                canmove = true;
                draw( );
            }
            //if (isImage == true && t>5)
            //    drawbig(x,y);
        }

        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;

            if (isImage == true && canmove == true) {
                canmove = false;
                drawbg(x, y);
                draw();
                drawRadar();                
            }
            scrollstatus = -1;

            if (x < 934 && y > 16) {
                document.getElementById("txtX").value = x + nowx;
                document.getElementById("txtY").value = y + nowy;
                addsensor.push((x + nowx) + ":" + (y + nowy));
                draw();
            }
        }

        function clearContent() {
            document.getElementById("txtName").value ="";
            document.getElementById("txtTag").value = "";
            document.getElementById("txtX").value = "";
            document.getElementById("txtY").value = "";
        }
        //window.onload = function (e) {
        //    draw();
        //}
    </script>
    <style>
        #myCanvasFG {
            position: static;
            border: 1px solid black;
        }

        #myCanvasBG {
            position: static;
            border: 1px solid black;
        }
        </style>
</head>
<body style="background-color: #ddffff">
    <form id="form1" runat="server">
        <div id="divUP" style="width: 1800px; height: 40px; border: 1px solid #c3c3c3"> 
            廠區 
            <asp:DropDownList ID="DDarea" runat="server" Width="200px" Font-Size="Large">
                <asp:ListItem Value="1.jpg">一廠</asp:ListItem>
                <asp:ListItem Value="2.jpg">二廠</asp:ListItem> 
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
           
            <img id="btnReadMap" onclick="ReadMap();" src="images/buttom.jpg" /> 
                <label id="test"></label>
                <label id="test1"></label>
                <label id="test2"> </label>
                &nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                       
            <div style="visibility: hidden;">
                <canvas id="myCanvasBG" width="950" height="950"></canvas>
            </div>
        </div>
        <div id="divdown" style="width:1800px; border: 1px solid #c3c3c3"> 
        <table >
            <tr>
                <td style="vertical-align:text-top;width:500px;" > 
                     <canvas id="myCanvasMap" width="500" height="500"></canvas> 
                </td>
                <td  style="width:950px;">
                    <canvas id="myCanvasFG" width="950" height="900" onmousemove="CanvasFG_mouosemove(event)" onmousedown="CanvasFG_mouosedown(event)" onmouseup="CanvasFG_mouoseup(event)"></canvas>
                </td>
                <td style="width:350px;"><div style="visibility: hidden;">
                        <canvas id="myCanvasMapBG" width="500" height="500"></canvas>
                    </div></td>
            </tr>
        </table>
            
        </div>
    </form>
</body>
</html>
