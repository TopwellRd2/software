var css3Support = function() {
	var transitionProp,
		transformProp;
	if (!window.addEventListener) {
		return {
			transitionProp: transitionProp,
			transformProp: transformProp
		};
	}

	var prefixes = ['Webkit', 'Moz', 'ms', 'Ms', 'O'],
		doc = document.documentElement.style;

	function getStyleProp(n) {
		if (typeof doc[n] === 'string') {
			return n;
		}

		n = n.charAt(0).toUpperCase() + n.slice(1);

		var prefixed;
		for (var i = 0, len = prefixes.length; i < len; i++) {
			prefixed = prefixes[i] + n;
			if (typeof doc[prefixed] === 'string') {
				return prefixed;
			}
		}
	}

	transitionProp = getStyleProp('transition');
	transformProp = getStyleProp('transform');

	return {
		transitionProp: transitionProp,
		transformProp: transformProp
	};
}();

(function() {
	if (window.matchMedia || !window.addEventListener) {
		return;
	}
	/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */
	window.matchMedia || (window.matchMedia = function() {
		"use strict";
		var a = window.styleMedia || window.media;
		if (!a) {
			var b = document.createElement("style"),
				c = document.getElementsByTagName("script")[0],
				d = null;
			b.type = "text/css", b.id = "matchmediajs-test", c.parentNode.insertBefore(b, c), d = "getComputedStyle" in window && window.getComputedStyle(b, null) || b.currentStyle, a = {
				matchMedium: function(a) {
					var c = "@media " + a + "{ #matchmediajs-test { width: 1px; } }";
					return b.styleSheet ? b.styleSheet.cssText = c : b.textContent = c, "1px" === d.width
				}
			}
		}
		return function(b) {
			return {
				matches: a.matchMedium(b || "all"),
				media: b || "all"
			}
		}
	}());
	/*! matchMedia() polyfill addListener/removeListener extension. Author & copyright (c) 2012: Scott Jehl. Dual MIT/BSD license */
	! function() {
		if (window.matchMedia && window.matchMedia("all").addListener) return !1;
		window.matchMedia = function() {
			return function(a) {
				return {
					matches: window.styleMedia.matchMedium(a || "all"),
					media: a || "all"
				}
			}
		}();
		var a = window.matchMedia,
			b = a("only all").matches,
			c = !1,
			d = 0,
			e = [],
			f = function() {
				clearTimeout(d), d = setTimeout(function() {
					for (var b = 0, c = e.length; c > b; b++) {
						var d = e[b].mql,
							f = e[b].listeners || [],
							g = a(d.media).matches;
						if (g !== d.matches) {
							d.matches = g;
							for (var h = 0, i = f.length; i > h; h++) f[h].call(window, d)
						}
					}
				}, 30)
			};
		window.matchMedia = function(d) {
			var g = a(d),
				h = [],
				i = 0;
			return g.addListener = function(a) {
				b && (c || (c = !0, window.addEventListener("resize", f, !0)), 0 === i && (i = e.push({
					mql: g,
					listeners: h
				})), h.push(a))
			}, g.removeListener = function(a) {
				for (var b = 0, c = h.length; c > b; b++) h[b] === a && h.splice(b, 1)
			}, g
		}
	}();
})();

var dropDownMenu = function() {
	var menu = $('#J-hdsub'),
		targetPos = 0,
		defaultPos = parseInt(menu.css('top')),
		triggers = $('#J-hdnav').find('.J-hdi-i'),
		subnavs = menu.find('.J-hdsub-i'),
		hdiclass = 'hdnav-items--cur',
		duration = 300,
		delay = 500,
		timer = null,
		isDropping = false;

	function dropdown(elem) {
		elem.hover(function() {
			if (timer) {
				clearTimeout(timer);
			}
			if (!isDropping) {
				isDropping = true;
				if (menu.queue('fx').length) {
					menu.stop(true);
				}
				menu.animate({
					top: targetPos
				}, duration, function() {
					isDropping = false;
				});
			}
		}, function() {
			timer = setTimeout(function() {
				isDropping = false;
				if (menu.queue('fx').length) {
					menu.stop(true);
				}
				menu.animate({
					top: defaultPos
				}, duration);
			}, delay);
		}).click(function() {
			if (timer) {
				clearTimeout(timer);
			}
			if (!isDropping) {
				isDropping = true;
				if (menu.queue('fx').length) {
					menu.stop(true);
				}
				menu.animate({
					top: targetPos
				}, duration, function() {
					isDropping = false;
				});
			}
		});
	}

	function toggleclass() {
		subnavs.each(function(index) {
			var trigger = triggers.eq(index);
			$(this).hover(function() {
				trigger.addClass(hdiclass);
			}, function() {
				trigger.removeClass(hdiclass);
			});
		});
	}

	function init() {
		dropdown(triggers);
		dropdown(menu);
		toggleclass();
	}
	return {
		init: init
	};
}();

var fixedMenuList = function() {
	var list = $('#J-fnav-list');
	if (!list.length) {
		return;
	}
	var triggers = list.find('.J-fnav-list-m'),
		submenu = list.find('.J-fnav-list-s'),
		maxheight = 300,
		curindex = -1;

	function init() {
		triggers.each(function(index) {
			var _self = $(this),
				_submenu = submenu.eq(index);
			_self.on('click', function(e) {
				// console.log(e.type)
				if (curindex != -1) {
					submenu.eq(curindex).css('max-height', 0);
				}
				if (curindex == index) {
					curindex = -1;
					return false;
				}
				_submenu.css('max-height', maxheight);
				curindex = index;
				return false;
			});
		});
	}

	return {
		init: init
	};
}();

var toggleFixedMenu = function() {
	var openbtn = $('#J-fnav-open');
	if (!openbtn.length) {
		return;
	}
	var menu = $('#J-fnav'),
		closebtn = $('#J-fnav-back'),
		transform = css3Support.transformProp;

	function toggle(action) {
		if (action) {
			menu.css(transform, 'translateX(0)');
		} else {
			menu.css(transform, 'translateX(100%)');
		}
	}

	function init() {
		openbtn.click(function() {
			toggle(1);
		});
		closebtn.click(function() {
			toggle();
		});
		menu.click(function(e) {
			var etarget = $(e.target);
			if (etarget.is(menu)) {
				toggle();
			}
			// console.log(e)
		});
	}
	return {
		init: init
	};
}();

function PlaceholderTxt(input, label, classname) {
	var input = $(input);
	if (!input.length) {
		return;
	}
	this.inputele = input;
	this.labelele = $(label);
	this.classname = classname;

	this.init();
}

PlaceholderTxt.prototype = {
	switchLabelClass: function(status) {
		this.labelele[(status ? 'add' : 'remove') + 'Class'](this.classname);
	},
	init: function() {
		var that = this;

		this.inputele
			.focus(function() {
				that.switchLabelClass(0);
			})
			.blur(function() {
				if ($.trim(this.value) === '') {
					that.switchLabelClass(1);
				}
			});

		if ($.trim(this.inputele[0].value) === '') {
			that.switchLabelClass(1);
		}
	}
};

var mapScroll = function() {
	var wrapper = $('#J-map-pop-w');
	if (!wrapper.length) {
		return;
	}
	var filters = wrapper.find('.J-map-filter');

	function init() {
		filters.each(function() {
			new customScroll(this);
		});
	}

	return {
		init: init
	};
}();

var mapPop = function() {
	var wrapper = $('#J-map-pop-w');
	if (!wrapper.length) {
		return;
	}
	var closebtns = wrapper.find('.J-map-close'),
		citys = $('#J-map-city-w').find('.J-map-city'),
		cityClass = 'map-city--cur',
		pops = wrapper.find('.J-map-pop'),
		duration = 300,
		showPop,
		hidePop,
		isAboveIE9 = !!window.addEventListener;

	if (isAboveIE9) {
		showPop = function(btn, pop) {
			btn.addClass(cityClass);
			pop.css('visibility', 'visible').animate({
				opacity: 1
			}, duration);
		};

		hidePop = function(btn, pop) {
			pop.animate({
				opacity: 0
			}, duration, function() {
				btn.removeClass(cityClass);
				pop.css('visibility', 'hidden');
			});
		};
	} else {
		showPop = function(btn, pop) {
			btn.addClass(cityClass);
			pop.css('visibility', 'visible');
		};
		hidePop = function(btn, pop) {
			btn.removeClass(cityClass);
			pop.css('visibility', 'hidden');
		};
	}

	function init() {
		citys.each(function(index) {
			var _pop = pops.eq(index),
				$self = $(this);

			$self.click(function() {
				showPop($self, _pop);
			});

			closebtns.eq(index).click(function() {
				hidePop($self, _pop);
			});
		});
	}

	return {
		init: init
	};
}();

function touchSlider(elem, opts) {
	var sliderwrap = $(elem.sliderwrap);

	if (!sliderwrap.length) {
		return;
	}

	var slider = $(elem.slider),
		slides = slider.find(elem.slides);

	this.sliderwrap = sliderwrap;
	this.slider = slider;
	this.slides = slides;
	this.slideswidth = sliderwrap.outerWidth();
	this.slideslen = slides.length;
	this.paginations = $(elem.paginations);
	this.paginationsClass = elem.paginationsClass;
	this.nextbtn = $(elem.nextbtn);
	this.prevbtn = $(elem.prevbtn);

	this.sliderTimer = null;
	this.isAnimating = false;

	this.opts = {
		autoplay: true,
		duration: 500,
		delay: 5000
	};

	this.curindex = this.opts.curindex || 0;

	$.extend(this.opts, opts);

	this.init();
}

touchSlider.prototype = {
	_navigate: function(selected) {
		this.isAnimating && this.slider.stop(true);
		this.isAnimating = true;

		this.paginations.eq(this.curindex).removeClass(this.paginationsClass);
		this.curindex = selected;
		this.paginations.eq(this.curindex).addClass(this.paginationsClass);

		var that = this;

		this.slider.animate({
			left: -this.curindex * 100 + '%'
		// }, this.opts.duration, function() {
		}, this.opts.duration, 'linear', function() {
			that.isAnimating = false;
		});
	},
	_startSlideShow: function() {
		var that = this;
		this.sliderTimer = setTimeout(function() {
			if (that.isAnimating) {
				return;
			}
			that._navigate((that.curindex + 1 + that.slideslen) % that.slideslen);
			that._startSlideShow();
		}, this.opts.delay);
	},
	_stopSlideShow: function() {
		if (!this.sliderTimer) {
			return;
		}
		clearTimeout(this.sliderTimer);
		this.sliderTimer = null;
	},
	_autoEvents: function(elem) {
		var that = this;
		elem.hover(function() {
			that._stopSlideShow();
		}, function() {
			that._startSlideShow();
		});
	},
	_resizeEvents: function() {
		var that = this;
		$(window).resize(function() {
			that.slideswidth = that.sliderwrap.outerWidth();
		});
	},
	_touchEvents: function() {
		var tLastCoords = {},
			tCurrCoords = {},
			currposx = 0,
			that = this;

		this.sliderwrap
			.on('touchstart', function(e) {
				var event = e.originalEvent,
					touches = event.touches;

				if (touches.length == 1) {
					that._stopSlideShow();
					that.isAnimating && that.slider.stop(true);
					currposx = parseInt(that.slider.css('left'));
				}
				touches = touches[0];

				tLastCoords = {
					x: touches.pageX,
					y: touches.pageY
				};

				tCurrCoords = {};
			})
			.on('touchmove', function(e) {
				var event = e.originalEvent,
					touches = event.touches;
				if (touches.length > 1 || event.scale && event.scale !== 1) {
					return;
				}
				touches = touches[0];

				tCurrCoords = {
					x: touches.pageX - tLastCoords.x,
					y: touches.pageY - tLastCoords.y
				};

				if (Math.abs(tCurrCoords.x) < Math.abs(tCurrCoords.y)) {
					return;
				}

				e.preventDefault();
				that.slider.css('left', currposx + tCurrCoords.x);
			})
			.on('touchend', function(e) {
				var currleft = currposx + tCurrCoords.x,
					curridx = currleft / -that.slideswidth;
				if (tCurrCoords.x < 0) {
					curridx = Math.ceil(curridx);
				} else {
					curridx = Math.floor(curridx);
				}
				if (curridx < 0) {
					curridx = 0;
				} else if (curridx > that.slideslen - 1) {
					curridx = that.slideslen - 1;
				}

				that._navigate(curridx);
				that._startSlideShow();
			});
	},
	_initEvents: function() {
		var that = this,
			prevbtn = this.prevbtn,
			nextbtn = this.nextbtn;

		this.paginations.each(function(index) {
			var $self = $(this);
			$self.on('mouseenter click', function() {
				if (index == that.curindex) {
					return;
				}
				that._navigate(index);
			});
		});

		if (prevbtn.length) {
			prevbtn.click(function() {
				that._navigate((that.curindex - 1 + that.slideslen) % that.slideslen);
			});
		}

		if (nextbtn.length) {
			nextbtn.click(function() {
				that._navigate((that.curindex + 1 + that.slideslen) % that.slideslen);
			});
		}

		if (this.opts.autoplay) {
			that._autoEvents(that.sliderwrap);
		}
		if (window.addEventListener) {
			this._resizeEvents();
			this._touchEvents();
		}
	},
	init: function() {
		this.paginations.eq(this.curindex).addClass(this.paginationsClass);
		this.slider.css('left', -this.curindex * 100 + '%');
		this._initEvents();
		this.opts.autoplay && this._startSlideShow();
	}
};

var itemlistPop = function() {
	var itemwrap = $('#J-itemlist');
	if (!itemwrap.length) {
		return;
	}
	// var links = itemwrap.find('.J-itemlist-link'),
	var popInstance,
		popwrap,
		popclose,
		popcontent,
		popActiveClass = 'itemlist-pop-wrap--show',
		popIsVisible = false;

	function popinit() {
		var pophtmlstr = '<div class="itemlist-pop-wrap" id="J-itemlist-pop-wrap"><div class="itemlist-pop-close"><a href="javascript:;" id="J-itemlist-pop-close"></a></div><div class="itemlist-pop-content" id="J-itemlist-pop-content"></div></div>';
		document.body.insertAdjacentHTML('beforeend', pophtmlstr);
		popwrap = $('#J-itemlist-pop-wrap');
		popclose = $('#J-itemlist-pop-close');
		popcontent = $('#J-itemlist-pop-content');

		popclose.click(function() {
			hidePop();
		});
	}

	function showPop(src) {
		if (!popInstance) {
			popinit();
		}
		popcontent.html('<iframe src="' + src + '" frameborder="0" width="100%" height="100%" allowTransparency></iframe>');
		if (!popIsVisible) {
			popwrap.addClass(popActiveClass);
			popIsVisible = true;
		}

	}

	function hidePop() {
		popcontent.html('');
		popwrap.removeClass(popActiveClass);
		popIsVisible = false;
	}

	function init() {
		// links.each(function() {
		// 	$(this).click(function() {
		// 		showPop(this.href);
		// 		return false;
		// 	});
		// });
		itemwrap.on('click', '.J-itemlist-link', function() {
			showPop(this.href);
			return false;
		});
	}

	return {
		init: init
	};
}();

var champintroVideo = function() {
	if (!window.addEventListener) {
		return;
	}

	var ele = $('#J-champslider-yt');
	if (!ele.length) {
		return;
	}
	var ytplayer = {},
		query = 'screen and (max-width:788px)',
		laststate;

	window.onYouTubeIframeAPIReady = function() {
		ytplayer = new YT.Player('J-champslider-yt', {
			width: '100%',
			height: '100%',
			videoId: ele.data('videoid'),
			playerVars: {
				wmode: 'transparent',
				showinfo: 0
			}
		});
	};

	function init() {
		var tag = document.createElement('script');
		tag.src = 'https://www.youtube.com/iframe_api';
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		if (!window.matchMedia) {
			return;
		}

		var match = window.matchMedia(query);
		match.addListener(function(mq1) {
			if (!ytplayer.pauseVideo) {
				return;
			}

			if (mq1.matches) {
				var _state = ytplayer.getPlayerState();
				if (_state === 1 || _state === 3) {
					laststate = 1;
					ytplayer.pauseVideo();
				} else {
					laststate = 0;
				}
			} else {
				if (laststate) {
					ytplayer.playVideo();
				}
			}
		});
	}

	return {
		init: init
	};
}();


champintroVideo && champintroVideo.init();
dropDownMenu && dropDownMenu.init();
fixedMenuList && fixedMenuList.init();
toggleFixedMenu && toggleFixedMenu.init();
new touchSlider({
	sliderwrap: '#J-promo-w',
	slider: '#J-promo',
	slides: '.J-promo-item',
	paginations: '#J-promo-pg .J-promo-pg-i',
	paginationsClass: 'promo-pg__item--cur'
});
mapScroll && mapScroll.init();
mapPop && mapPop.init();
itemlistPop && itemlistPop.init();
new touchSlider({
	sliderwrap: '#J-champslider-w',
	slider: '#J-champslider',
	slides: '.J-champslider-item',
	paginations: '#J-champslider-pg .J-champslider-pg-i',
	paginationsClass: 'champslider-pg__item--cur',
	prevbtn: '#J-champslider-prev',
	nextbtn: '#J-champslider-next'
});

window.onload = function() {
	new PlaceholderTxt('#J-search', '#J-search-label', 'hdnav-search__label--active');
	new PlaceholderTxt('#J-fixed-search', '#J-fixed-search-label', 'fixed-nav-search__label--active');
	new PlaceholderTxt('#J-mapsearch', '#J-mapsearch-label', 'itemlist-mapsearch__label--active');
	new PlaceholderTxt('#J-champsearch', '#J-champsearch-label', 'champlist-champsearch__label--active');
}