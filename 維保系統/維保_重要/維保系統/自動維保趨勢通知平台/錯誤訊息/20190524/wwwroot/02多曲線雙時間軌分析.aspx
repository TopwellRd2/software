﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="02多曲線雙時間軌分析.aspx.vb" Inherits="SQLWeb._01多曲線雙時間軌分析" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業
    </title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        var isDragScroll = -1;
        var DragScroll_start;
        var scroll_len = 0;
        var scroll_pos = 0;
        var scroll_dis=0;
        
        var lineA = 680, lineB = 1080; // 兩條特徵線
        //var colors = new Array("blue", "orange", "green", "red", "purple", "brown", "yellow", "gray", "gold", "black");
        var colors = new Array("#0000ff", "#ff0000", "#ff00ff", "#772b1a", "#008800", "#c8705a", "#10cff0", "#000000", "#a0a0a0", "#c8c800");
        var isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 

        var md = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        var selectStartdate, selectEnddate;// 選擇到的時間起點和終點
        var selectStartdateint, selectEnddateint;// 選擇到的時間起點和終點
        var timeinterval = new Array();
        var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
        var DragSensorx, DragSensory; // 被拖拉的感應器位置
        var DragSensorList = new Array(); // 被拉進來的感應器列表，最多8個
        var isDisplaySensor = new Array(); // 是否要顯示這個感應器
        var sensorMax = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]; // 這個感應器的最大數值
        var sensormin = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]; // 這個感應器的最大數值
        var sensorAvg = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]; // 這個感應器的最大數值
        var sensor0 = new Array(); // 600筆資料
        var sensor1 = new Array();
        var sensor2 = new Array();
        var sensor3 = new Array();
        var sensor4 = new Array();
        var sensor5 = new Array();
        var sensor6 = new Array();
        var sensor7 = new Array();
        var sensor8 = new Array();
        var sensor9 = new Array();
        var yaxis0 = new Array(); // 720筆資料
        var yaxis1 = new Array();
        var yaxis2 = new Array();
        var yaxis3 = new Array();
        var yaxis4 = new Array();
        var yaxis5 = new Array();
        var yaxis6 = new Array();
        var yaxis7 = new Array();
        var yaxis8 = new Array();
        var yaxis9 = new Array();
        var nowCurve = -1;
        var indexsensor = new Array(sensor0, sensor1, sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8, sensor9);
        var indexyaxis = new Array(yaxis0, yaxis1, yaxis2, yaxis3, yaxis4, yaxis5, yaxis6, yaxis7, yaxis8, yaxis9);

        var isScrollRange = -1;
        var scrollmax = new Array(719,719,719,719,719,719,719,719,719);
        var scrollmin = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);

        var sensorList = new Array();// 左邊的感應器列表
        var drawtype = 1;// 1:normal,2:all

        function drawBG() { // 在隱藏的BG 繪製底下的表格
            var canBG = document.getElementById("myCanvasBG");
            var ctx = canBG.getContext("2d");
            var a;

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(0, 0, 1580, 880);

            ////Y軸刻度
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(105, 20);
            ctx.lineTo(105, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體"; 
            for (a = 0; a <= 12; a++) {// 0~120度 
                ctx.beginPath(); // 刻度
                ctx.moveTo(105, 740 - a * 60);
                ctx.lineTo(100, 740 - a * 60);
                ctx.stroke();

                //if (a < 10)
                //    ctx.fillText(tt[a], 52, 740 - a * 60 + 4); // 數字
                //else
                //    ctx.fillText(a * 10, 46, 740 - a * 60 + 4); // 數字
            }
            ctx.save();
            ctx.strokeStyle = 'gray';
            ctx.setLineDash([5, 5]);
            for (a = 1; a <= 12; a++) {// 0~120度的橫向虛線條
                ctx.beginPath(); // 刻度
                ctx.moveTo(110.5, 740.5 - a * 60);
                ctx.lineTo(1260.5, 740.5 - a * 60);
                ctx.stroke();
            }
            ctx.restore();

            //X軸刻度 
            ctx.strokeStyle = 'black';
            ctx.beginPath();
            ctx.moveTo(110, 740);
            ctx.lineTo(1260, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var interval = (selectEnddateint - selectStartdateint) / 9;
            for (a = 0; a < 10; a++) {// 分十大等分 
                ctx.beginPath(); // 刻度
                ctx.moveTo(120 + a * 116, 740);
                ctx.lineTo(120 + a * 116, 750);
                ctx.stroke();

                ctx.save();  //顯示日期刻度
                ctx.translate(115 + a * 116, 760);
                ctx.rotate(Math.PI / 180 * 45);
                if (timeinterval.length == 10) 
                    ctx.fillText(timeinterval[a], 0, 0);
                ctx.restore();

                //var s = dateToint(2018, 3, 1, 10, 20);
                //var e = dateToint(2018, 3, 1, 19, 20);
                //var interval = (e - s) / 9;
                //var a;
                //console.log("1:" + intTodate(s));
                //for (a = 1; a < 8; a++)
                //    console.log((a + 1) + ":" + intTodate(s + a * interval));
                //console.log("9:" + intTodate(e));
            }

            drawSensorCurvline(ctx);// 中間sensor曲線 

            drawrightGrid(ctx);   // 右邊的曲線分析資料
        }

        function drawrightGrid(ctx) { // 繪製右邊的曲線分析資料 
            var temp;
            var max, min, avg;
            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1280, 30, 290, 830);

            ctx.font = "18Px 新細明體";
            ctx.fillStyle = "black";
            ctx.fillText("最大/最小/平均", 1325, 20);
            ctx.font = "20Px 新細明體";
            for (a = 0; a < DragSensorList.length; a++) {
                if (nowCurve == a) {
                    ctx.fillStyle = "#FF5050";
                    ctx.fillRect(1295, 45 + a * 70, 25, 25);
                }
                ctx.fillStyle = colors[a];
                ctx.fillRect(1300, 50 + a * 70, 15, 15);

                ctx.fillStyle = "black";
                temp = sensorList[DragSensorList[a]];
                temp = temp.substr(temp.indexOf(":") + 1);
                ctx.fillText(temp, 1325, 50 + a * 70);//名稱

                if (a == 0) cal_right_max_min(sensor0, yaxis0, a);
                else if (a == 1) cal_right_max_min(sensor1, yaxis1, a);
                else if (a == 2) cal_right_max_min(sensor2, yaxis2, a);
                else if (a == 3) cal_right_max_min(sensor3, yaxis3, a);
                else if (a == 4) cal_right_max_min(sensor4, yaxis4, a);
                else if (a == 5) cal_right_max_min(sensor5, yaxis5, a);
                else if (a == 6) cal_right_max_min(sensor6, yaxis6, a);
                else if (a == 7) cal_right_max_min(sensor7, yaxis7, a);
                else if (a == 8) cal_right_max_min(sensor8, yaxis8, a);
                else if (a == 9) cal_right_max_min(sensor9, yaxis9, a);
                
                ctx.fillText(sensorMax[a].toFixed(1) + "/" + sensormin[a].toFixed(1) + "/" + sensorAvg[a].toFixed(1), 1325, 75 + a * 70); 
            }
        }

        function cal_right_max_min(sen, yax, n) {
            var left, right, max, min;

            try {
                if (lineA > lineB)
                { left = lineB; right = lineA; }
                else
                { left = lineA; right = lineB; }
                left = Math.floor((left - 380) / 2);
                right = Math.floor((right - 380) / 2);

                if (n == 0) {
                    //console.log(left + ":" + right + ":" + yax[sen[left]] + ":" + sen[left]);
                }
                max = parseFloat(yax[parseInt(sen[left])]);
                min = parseFloat(yax[parseInt(sen[left])]);
                var avg = 0, c = 0;
                var a, value;
                console.log("sen=" + sen.length + ",yax:" + yax.length);
                for (a = left-9; a <= right-10; a++) {
                    c += 1;
                    value = parseFloat(yax[parseInt(sen[a])]);
                    if (value > max)
                        max = value;
                    else if (value < min)
                        min = value; 
                    avg += value;
                    console.log("a=" + a + ",value:" + value);
                    console.log("sen=" + sen[a] + ",yax:" + yax[parseInt(sen[a])]);
                }
                sensorMax[n] = max;
                sensormin[n] = min;
                if (n == 0) {
                   // console.log(max + ":" + min);
                }
                if (c > 0)
                    sensorAvg[n] = avg / c;
                else
                    sensorAvg[n] = 0;
                //console.log("cal_right_max_min");
                //console.log(sen.length);
                //console.log(avg);
                //console.log(max);
                //console.log(min);
                //console.log(sensorAvg[n]);
            }
            catch (err) {
              console.log(err.message);
            } 
        }

        function drawSensorCurvline(ctx) {
            var a;
            var linecolor = 0;
            var startpos = 120;//繪製起點
            var src, dst, M, m, step, s, r;
            var isstart = 0;
            // 1
            if (DragSensorList.length < 1) return;
            //if (nowCurve == 0 || nowCurve == -1) {
            ctx.save();
            linecolor = 0;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            //ctx.moveTo(100, 740 - sensor0[0] * 6);// 先畫出線
            //ctx.moveTo(startpos, 740 - sensor0[0]);// 先畫出線
            src = parseFloat(yaxis0[719]) - parseFloat(yaxis0[0]);
            dst = parseFloat(yaxis0[719 - scrollmin[0]]) - parseFloat(yaxis0[719 - scrollmax[0]]);
            step = dst / src; 
            M = parseFloat( 719 - scrollmax[0] );
            s = (sensor0[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s); 
            }
           
          //  console.log(src + "~" + dst + ",step=" + step); 
           // console.log(yaxis0[719 - scrollmax[0]] + ":" + scrollmax[0]);
           // console.log(s + ":" + sensor0[0]+","+M);
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(100 + a * 2, 740 - sensor0[a] * 6);
                //ctx.lineTo(startpos + a * 2, 740 - sensor0[a]);
                s = (sensor0[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }

            }
            ctx.stroke();
            ctx.restore();

          //  drawonecurve(ctx, sensor0, yaxis0, 1);

            //ctx.save(); //這段測試用途，要拿掉!!!
            //ctx.strokeStyle = colors[3];
            //ctx.beginPath();
            //ctx.moveTo(startpos, 740 - sensor0[0]);// 先畫出線 
            //for (a = 0; a < 550; a++) { 
            //    ctx.lineTo(startpos + a * 2, 740 - sensor0[a]); 
            //}
            //ctx.stroke();
            //ctx.restore();

            //var tempyInterval  = parseInt((scrollmax[nowCurve] - scrollmin[nowCurve]) / 12); 
            //for (a = 0; a <= 12; a++) {// 0~120度 
            //    if (nowCurve == 0) {
            //        ctx.fillText(yaxis0[719 - scrollmax[nowCurve] + a * tempyInterval], 300, 740 - a * 60 + 4); // 數字                
            //        console.log(719 - scrollmax[nowCurve] + a * tempyInterval);

            //for (a = 0; a < sensor0.length; a++) {
            //    if (n == 0)
            //        sensor0[a] = parseInt((sensor0[a] - new_min) / step);
            // }

            // 2
            if (DragSensorList.length < 2) return;
            // if (nowCurve == 1 || nowCurve == -1) {
            ctx.save();
            linecolor = 1;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            // ctx.moveTo(startpos, 740 - sensor1[0]);// 先畫出線
            src = parseFloat(yaxis1[719]) - parseFloat(yaxis1[0]);
            dst = parseFloat(yaxis1[719 - scrollmin[1]]) - parseFloat(yaxis1[719 - scrollmax[1]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[1]);
            s = (sensor1[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            } 
            for (a = 0; a < 550; a++) {
                //  ctx.lineTo(startpos + a * 2, 740 - sensor1[a]);
                s = (sensor1[a] - M) / step;  
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            //  }

            // 3
            if (DragSensorList.length < 3) return;
            //if (nowCurve ==2 ||nowCurve==-1){
            ctx.save();
            linecolor = 2;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            //  ctx.moveTo(startpos, 740 - sensor2[0]);// 先畫出線
            src = parseFloat(yaxis2[719]) - parseFloat(yaxis2[0]);
            dst = parseFloat(yaxis2[719 - scrollmin[2]]) - parseFloat(yaxis2[719 - scrollmax[2]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[2]);
            s = (sensor2[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(startpos + a * 2, 740 - sensor2[a]);
                s = (sensor2[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            // }

            // 4
            if (DragSensorList.length < 3) return;
            //if (nowCurve ==3 ||nowCurve==-1){
            ctx.save();
            linecolor = 3;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            // ctx.moveTo(startpos, 740 - sensor3[0]);// 先畫出線
            src = parseFloat(yaxis3[719]) - parseFloat(yaxis3[0]);
            dst = parseFloat(yaxis3[719 - scrollmin[3]]) - parseFloat(yaxis3[719 - scrollmax[3]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[3]);
            s = (sensor3[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                //  ctx.lineTo(startpos + a * 2, 740 - sensor3[a]);
                s = (sensor3[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            // }

            // 5
            if (DragSensorList.length < 4) return;
            //  if (nowCurve ==4 ||nowCurve==-1){
            ctx.save();
            linecolor = 4;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            //ctx.moveTo(startpos, 740 - sensor4[0]);// 先畫出線
            src = parseFloat(yaxis4[719]) - parseFloat(yaxis4[0]);
            dst = parseFloat(yaxis4[719 - scrollmin[4]]) - parseFloat(yaxis4[719 - scrollmax[4]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[4]);
            s = (sensor4[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(startpos + a * 2, 740 - sensor4[a]);
                s = (sensor4[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            //  }

            // 6
            if (DragSensorList.length < 5) return;
            //  if (nowCurve ==5 ||nowCurve==-1){ 
            ctx.save();
            linecolor = 5;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
           // ctx.moveTo(startpos, 740 - sensor5[0]);// 先畫出線
            src = parseFloat(yaxis5[719]) - parseFloat(yaxis5[0]);
            dst = parseFloat(yaxis5[719 - scrollmin[5]]) - parseFloat(yaxis5[719 - scrollmax[5]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[5]);
            s = (sensor5[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                // ctx.lineTo(startpos + a * 2, 740 - sensor5[a]);
                s = (sensor5[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            // }

            // 7
            if (DragSensorList.length < 6) return;
            //  if (nowCurve ==6 ||nowCurve==-1){
            ctx.save();
            linecolor = 6;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            // ctx.moveTo(startpos, 740 - sensor6[0]);// 先畫出線
            src = parseFloat(yaxis6[719]) - parseFloat(yaxis6[0]);
            dst = parseFloat(yaxis6[719 - scrollmin[6]]) - parseFloat(yaxis6[719 - scrollmax[6]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[6]);
            s = (sensor6[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                //  ctx.lineTo(startpos + a * 2, 740 - sensor6[a]);
                s = (sensor6[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            //   }

            // 8
            if (DragSensorList.length < 7) return;
            //  if (nowCurve == 7 || nowCurve == -1) {
            ctx.save();
            linecolor = 7;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            //ctx.moveTo(startpos, 740 - sensor7[0]);// 先畫出線
            src = parseFloat(yaxis7[719]) - parseFloat(yaxis7[0]);
            dst = parseFloat(yaxis7[719 - scrollmin[7]]) - parseFloat(yaxis7[719 - scrollmax[7]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[7]);
            s = (sensor7[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(startpos + a * 2, 740 - sensor7[a]);
                s = (sensor7[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            // }
            // 9
            if (DragSensorList.length < 8) return;
            //if (nowCurve == 8 || nowCurve == -1) {
            ctx.save();
            linecolor = 8;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            //ctx.moveTo(startpos, 740 - sensor8[0]);// 先畫出線
            src = parseFloat(yaxis8[719]) - parseFloat(yaxis8[0]);
            dst = parseFloat(yaxis8[719 - scrollmin[8]]) - parseFloat(yaxis8[719 - scrollmax[8]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[8]);
            s = (sensor8[0] - M) / step;
            if (s < 0)
                isstart = 0;
            else {
                isstart = 1;
                ctx.moveTo(startpos, 740 - s);
            }
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(startpos + a * 2, 740 - sensor8[a]);
                s = (sensor8[a] - M) / step;
                if (s < 0) {
                    isstart = 0;
                    continue;
                }
                else {
                    if (isstart == 0) {
                        ctx.moveTo(startpos + a * 2, 740 - s);
                        isstart = 1;
                    }
                    else
                        ctx.lineTo(startpos + a * 2, 740 - s);
                }
            }
            ctx.stroke();
            ctx.restore();
            // }
        }

        function drawonecurve(ctx, sen, ya, n) {
            var a;
            var linecolor = 0;
            var startpos = 120;//繪製起點
            var src, dst, M, m, step, s, r; 
            if (DragSensorList.length < n) return;

            ctx.save();
            linecolor = 4;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath(); 
            src = parseFloat(ya[719]) - parseFloat(ya[n-1]);
            dst = parseFloat(ya[719 - scrollmin[n - 1]]) - parseFloat(ya[719 - scrollmax[n - 1]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[n - 1]);
            s = (sen[0] - M) / step;
            ctx.moveTo(startpos, 740 - s);// 先畫出線
            //  console.log(src + "~" + dst + ",step=" + step); 
            // console.log(yaxis0[719 - scrollmax[0]] + ":" + scrollmax[0]);
            // console.log(s + ":" + sensor0[0]+","+M);
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(100 + a * 2, 740 - sensor0[a] * 6);
                //ctx.lineTo(startpos + a * 2, 740 - sensor0[a]);
                s = (sen[a] - M) / step;
                ctx.lineTo(startpos + a * 2, 740 - s);
            }
            ctx.stroke();
            ctx.restore();
        }

        function drawFG(ctx, fg) { // 在FG 繪製特徵線和處理滑鼠事件
            // 繪製兩條特徵線
            ctx.strokeStyle = 'red'; 
            ctx.lineWidth = 3;
            ctx.beginPath(); // 第一條
            ctx.moveTo(lineA, 0);
            ctx.lineTo(lineA, 740);
            ctx.stroke();
            ctx.beginPath(); // 第二條
            ctx.moveTo(lineB, 0);
            ctx.lineTo(lineB, 740);
            ctx.stroke();

            // 繪製交點資訊
            if (isDragline == 1) {  //  1:lineA, 2:lineB 
                drawfocus(ctx, lineA);
            }
            else if (isDragline == 2) {  //  1:lineA, 2:lineB 
                drawfocus(ctx, lineB);
            }
        }
         
        function drawfocus(ctx, tx) // 繪製交點資訊,x要介於400~1510間
        {
            var bx = tx;
            tx += 20;
            if (tx > 1210) tx -= 390;
            ctx.save();
            ctx.font = "bold 20Px 新細明體";
            ctx.fillStyle = "#a8aaaa";   // 'lightgray';
            ctx.fillRect(tx, 50, 350, 30);
            ctx.fillStyle = "white";
            var t = (bx - 400) * (selectEnddateint - selectStartdateint) / 1100;
            //console.log("BX:" + bx);
            //console.log("tx:" + tx);
            //console.log("lineA:" + lineA);
            //console.log("lineB:" + lineB);
            var d = intTodate(selectStartdateint + t);
            ctx.fillText(dateformat(d), tx + 15, 72);

            var n = DragSensorList.length; // 假設有N筆資料
            ctx.fillStyle = "#c8c8c8"; // 畫外框
            ctx.fillRect(tx, 80, 350, n * 30);
            ctx.fillStyle = 'white';
            ctx.fillRect(tx + 1, 81, 348, n * 30 - 2);

            ctx.strokeStyle = "#c8c8c8";
            ctx.lineWidth = 1;
            ctx.beginPath(); // 先畫名稱和數值的分隔線
            ctx.moveTo(tx + 280, 80);
            ctx.lineTo(tx + 280, 80 + n * 30 - 2);
            ctx.stroke();
            var temp, v;
            for (var a = 0; a < n; a++) {
                ctx.beginPath(); // 畫這筆資料的底線
                ctx.moveTo(tx + 1, 80 + a * 30 + 28);
                ctx.lineTo(tx + 349, 80 + a * 30 + 28);
                ctx.stroke();

                ctx.fillStyle = colors[a]; // 資料顏色方塊
                ctx.fillRect(tx + 5, 80 + a * 30 + 10, 12, 12);

                ctx.fillStyle = "black";
                temp = sensorList[DragSensorList[a]];
                temp = temp.substr(temp.indexOf(":") + 1);
              //  console.log(sensorList.length + ":" + DragSensorList.length+":"+temp);
                ctx.fillText(temp, tx + 23, 80 + a * 30 + 22); //名稱
                if (a == 0) {
                    v = yaxis0[parseInt(sensor0[Math.floor((bx - 400) / 2)])];
                }
                else if (a == 1) {
                    v = yaxis1[parseInt(sensor1[Math.floor((bx - 400) / 2)])];
                   // ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 2) {
                    v =  yaxis2[parseInt(sensor2[Math.floor((bx - 400) / 2)])];
                  //  ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 3) {
                    v =  yaxis3[parseInt(sensor3[Math.floor((bx - 400) / 2)])];
                  //  ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 4) {
                    v =  yaxis4[parseInt(sensor4[Math.floor((bx - 400) / 2)])];
                   // ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 5) {
                    v = yaxis5[parseInt(sensor5[Math.floor((bx - 400) / 2)])] ;
                   // ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 6) {
                    v = yaxis6[parseInt(sensor6[Math.floor((bx - 400) / 2)])] ;
                  //  ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 7) {
                    v = yaxis7[parseInt(sensor7[Math.floor((bx - 400) / 2)])] ;
                   // ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 8) {
                    v =  yaxis8[parseInt(sensor8[Math.floor((bx - 400) / 2)])];
                   // ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }
                else if (a == 9) {
                    v =  yaxis9[parseInt(sensor9[Math.floor((bx - 400) / 2)])];
                  //  ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                }

               // console.log(v);
                if (isNaN(v)) {
                    ctx.fillText("NAN", tx + 283, 80 + a * 30 + 22);
                    console.log("NAN");
                }
                else {
                    ctx.fillText(v.toFixed(1), tx + 283, 80 + a * 30 + 22); //數值
                  //  console.log(v.toFixed(1));
                }
                   
            }
            ctx.restore();
        }

        function drawLeftList(ctx) { // 畫出左邊感應器列表
            ctx.fillStyle = "black";
            ctx.font = "20Px 新細明體";
            //var imgData = ctxbg.getImageData(0, 0, 1520, 880)
            //ctxfg.putImageData(imgData, 280, 0);

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1, 1, 278, 879); //clear background

            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(260, 1, 18, 879); //scroll area

          //console.log("scroll_pos:"+scroll_pos);
          //   console.log("scroll_len:"+scroll_len);
            ctx.fillStyle = "#cccccc"; 
           // ctx.fillRect(261, scroll_pos * parseInt(877 / sensorList.length)+1, 16, scroll_len); //scroll bar
            ctx.fillRect(261,  scroll_pos + 1, 16, scroll_len); //scroll bar

            //ctx.fillStyle = "black";
            // console.log("drawLeftList:" + sensorList.length); 
            var temp; //每筆30，最多29筆 
            ctx.fillStyle = "black";
            // for (var a = 0; a < sensorList.length; a++) {
            var t = parseInt(scroll_pos / parseInt(877 / sensorList.length));

            for (var a =0; a <  29; a++) {
                if ((a + t) >= sensorList.length) break;
                //ctx.fillStyle = "#ffcccc";
                //ctx.fillRect(10, a * 30 + 13, 266, 21);
                //console.log(sensorList[a]);
                temp = sensorList[a + t];
                temp = temp.substr(temp.indexOf(":") + 1);
                //ctx.fillText(sensorList[a], 10, a * 30 + 30); 
                ctx.fillText(temp, 10,  a   * 30 + 30);
                //console.log(temp+":"+a);
            }

            //for (var a = 2 ; a <=28; a++) { 
            //    ctx.fillStyle = "black"; 
            //    ctx.fillText("7CH01A_壓縮機RS相位電壓" + a, 10, a * 30 + 30);
            //    //console.log(sensorList[a]);
            //}

            //for (var a = sensorList.length;a<29; a++) {
            //    ctx.fillStyle = "#ffdddd";
            //    ctx.fillRect(10, a * 30 + 13, 266, 21);
            //    //ctx.fillStyle = "black";
            //    //ctx.fillText(a, 10, a * 30 + 30);
            //}
        }
 

        function drawclear() { // 清畫面
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function draw() { // 主繪圖函數，整合一切  
            if (isDragline == -1)
                drawBG(); // 1. 先畫好背景

            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");

            var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            var imgData = ctxbg.getImageData(0, 0, 1580, 880);
            ctxfg.putImageData(imgData, 280, 0);

            if (nowCurve >= 0)
                drawYcurve(ctxfg);

            ctxfg.beginPath();// 劃出中間分隔線
            ctxfg.moveTo(280, 0);
            ctxfg.lineTo(280, 880);
            ctxfg.strokeStyle = 'black';
            ctxfg.stroke();

            drawFG(ctxfg, fg);
            drawLeftList(ctxfg); // 畫出左邊感應器列表

            if (isDragSensor > -1)// 左邊拖拉的感應器
            {
                // isDragSensor = n;
                var a = parseInt(scroll_pos / parseInt(877 / sensorList.length)); 
                if ((a + isDragSensor) < sensorList.length) {
                    ctxfg.fillStyle = "#ffcccc";
                    ctxfg.fillRect(DragSensorx, DragSensory, 266, 21);
                    ctxfg.fillStyle = "black";
                    ctxfg.fillText(sensorList[isDragSensor + a], DragSensorx, DragSensory + 17);
                }
            }
            //document.getElementById("Button1").value = abc;
        }

        function drawYcurve(ctx) {
            //Y軸刻度
           // console.log("drawYcurve")
            ctx.fillStyle = "#d5ffff";
            ctx.fillRect(281, 1, 85, 878);
              
            //ctx.strokeStyle = colors[nowCurve];
             ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.fillStyle = colors[nowCurve];//"black";
            ctx.font = "16Px 新細明體"; 
            var a,v;
            var tempyInterval ;
            if (nowCurve > -1)
                tempyInterval = parseInt((scrollmax[nowCurve] - scrollmin[nowCurve]) / 12);
            //console.log("tempyInterval="+tempyInterval);
            for (a = 0; a <= 12; a++) {// 0~120度 
                if (nowCurve == 0) {
                    v=yaxis0[719 - scrollmax[nowCurve] + a * tempyInterval];
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 1) {
                    v = yaxis1[719 - scrollmax[nowCurve] + a * tempyInterval] ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 2) {
                    v =yaxis2[719 - scrollmax[nowCurve] + a * tempyInterval]  ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 3) {
                    v =yaxis3[719 - scrollmax[nowCurve] + a * tempyInterval]  ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 4) {
                    v =yaxis4[719 - scrollmax[nowCurve] + a * tempyInterval]  ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 5) {
                    v = yaxis5[719 - scrollmax[nowCurve] + a * tempyInterval] ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 6) {
                    v = yaxis6[719 - scrollmax[nowCurve] + a * tempyInterval] ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 7) {
                    v =  yaxis7[719 - scrollmax[nowCurve] + a * tempyInterval];
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 8) {
                    v =  yaxis8[719 - scrollmax[nowCurve] + a * tempyInterval];
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
                else if (nowCurve == 9) {
                    v = yaxis9[719 - scrollmax[nowCurve] + a * tempyInterval] ;
                    ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
                }
            }

            if (nowCurve > -1) {
                ctx.fillStyle = "#ffffff"; // scroll
                ctx.fillRect(366, 7, 15, 729);
                ctx.fillStyle = "#888888"; // scrollmin
                ctx.fillRect(367,scrollmin[nowCurve]+ 8, 13, 8);
                ctx.fillStyle = "#888888"; // scrollmax
                ctx.fillRect(367, scrollmax[nowCurve] + 8, 13, 8);
            }
        }

        function drawAllcurve_y() {
            drawtype = (drawtype + 1) % 2;
             console.log("drawtype=" + drawtype);
            if (drawtype == 1) {// 1:normal,2:all
                draw();                
                return;
            }
            
            var fg = document.getElementById("myCanvasFG");
            var ctx = fg.getContext("2d");
             
            ctx.fillStyle = "#ccffff";
            ctx.fillRect(0, 0, 1860, 880);

            YdrawAll(ctx);

            YdrawXYgrid(ctx);
        }

        function YdrawAll(ctx) {
            //ctx.fillStyle = "#d5ffff";
            //ctx.fillRect(281, 1, 85, 878);

            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.font = "16Px 新細明體";
            var a, v, n, tx, ty;
            var tempyInterval;

            for (n = 0; n < DragSensorList.length; n++) {
                ctx.fillStyle = colors[n];//"black"; 
                tempyInterval = parseInt((scrollmax[n] - scrollmin[n]) / 12);
                for (a = 0; a <= 12; a++) {// 0~120度 
                    tx = n * 80 + 10;
                    ty = 740 - a * 60 + 4;
                    if (n == 0) {
                        v=yaxis0[719 - scrollmax[n] + a * tempyInterval] ;
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字   
                    }
                    else if (n == 1) {
                        v= yaxis1[719 - scrollmax[n] + a * tempyInterval];
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字   
                    }
                    else if (n == 2) {
                        v= yaxis2[719 - scrollmax[n] + a * tempyInterval];
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字    
                    }
                    else if (n == 3) { 
                        v=yaxis3[719 - scrollmax[n] + a * tempyInterval] ;
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字    
                    }
                    else if (n == 4) {
                        v=yaxis4[719 - scrollmax[n] + a * tempyInterval] ;
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字   
                    }
                    else if (n == 5) {
                        v= yaxis5[719 - scrollmax[n] + a * tempyInterval];
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字   
                    }
                    else if (n == 6) {
                        v=yaxis6[719 - scrollmax[n] + a * tempyInterval] ;
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字   
                    }
                    else if (n == 7) {
                        v=yaxis7[719 - scrollmax[n] + a * tempyInterval] ;
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字   
                    }
                    else if (n == 8) {
                        v=yaxis8[719 - scrollmax[n] + a * tempyInterval] ;
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字  
                    }
                    else if (n == 9) {
                        v= yaxis9[719 - scrollmax[n] + a * tempyInterval];
                        ctx.fillText(v.toFixed(1), tx, ty); // 數字  
                    }
                }
            }
        }

        function YdrawXYgrid(ctx) { 
            var a; 
            var tx = DragSensorList.length * 80 +10;
            ctx.strokeStyle = 'black';//Y軸刻度
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(tx, 20);
            ctx.lineTo(tx, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            for (a = 0; a <= 12; a++) {// 0~120度 
                ctx.beginPath(); // 刻度
                ctx.moveTo(tx, 740 - a * 60);
                ctx.lineTo(tx-5, 740 - a * 60);
                ctx.stroke(); 
            }
            ctx.save();
            ctx.strokeStyle = 'gray';
            ctx.setLineDash([5, 5]);
            for (a = 1; a <= 12; a++) {// 0~120度的橫向虛線條
                ctx.beginPath(); // 刻度
                ctx.moveTo(tx, 740.5 - a * 60);
                ctx.lineTo(tx+1100 *0.7, 740.5 - a * 60);
                ctx.stroke();
            }
            ctx.restore(); 
          
            ctx.strokeStyle = 'black';  //X軸刻度 
            ctx.beginPath();
            ctx.moveTo(tx , 740);
            ctx.lineTo(tx+1100 *0.7, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var interval = (selectEnddateint - selectStartdateint) / 9;
            for (a = 0; a < 10; a++) {// 分十大等分 
                ctx.beginPath(); // 刻度
                //ctx.moveTo(tx + 10 + a * 116, 740);
                //ctx.lineTo(tx + 10 + a * 116, 750);
                ctx.moveTo(tx + 10 + a * 81, 740);
                ctx.lineTo(tx + 10 + a * 81, 750);
                ctx.stroke();

                ctx.save();  //顯示日期刻度
                ctx.translate(tx+5 + a * 81, 760);
                ctx.rotate(Math.PI / 180 * 45);
                if (timeinterval.length == 10)
                    ctx.fillText(timeinterval[a], 0, 0);
                ctx.restore(); 
            }
             
            YdrawSensorCurvline(ctx);// 中間sensor曲線 
            YdrawrightGrid(ctx);   // 右邊的曲線分析資料
        }

        function YdrawrightGrid(ctx) { // 繪製右邊的曲線分析資料 
            var temp;
            var max, min, avg;
            var tx = DragSensorList.length * 80 + 785;
            //ctx.fillStyle = "#ddffff";
            //ctx.fillRect(1280, 30, 290, 830);

            ctx.font = "18Px 新細明體";
            ctx.fillStyle = "black";
            ctx.fillText("最大/最小/平均", tx, 20);
            ctx.font = "20Px 新細明體";
            for (a = 0; a < DragSensorList.length; a++) {
                //if (nowCurve == a) {
                //    ctx.fillStyle = "#FF5050";
                //    ctx.fillRect(1295, 45 + a * 70, 25, 25);
                //}
                //ctx.fillStyle = 
                //ctx.fillRect(1300, 50 + a * 70, 15, 15);

                ctx.fillStyle =colors[a]; 
                temp = sensorList[DragSensorList[a]];
                temp = temp.substr(temp.indexOf(":") + 1);
                ctx.fillText(temp, tx, 50 + a * 70);//名稱

                if (a == 0) cal_right_max_min(sensor0, yaxis0, a);
                else if (a == 1) cal_right_max_min(sensor1, yaxis1, a);
                else if (a == 2) cal_right_max_min(sensor2, yaxis2, a);
                else if (a == 3) cal_right_max_min(sensor3, yaxis3, a);
                else if (a == 4) cal_right_max_min(sensor4, yaxis4, a);
                else if (a == 5) cal_right_max_min(sensor5, yaxis5, a);
                else if (a == 6) cal_right_max_min(sensor6, yaxis6, a);
                else if (a == 7) cal_right_max_min(sensor7, yaxis7, a);
                else if (a == 8) cal_right_max_min(sensor8, yaxis8, a);
                else if (a == 9) cal_right_max_min(sensor9, yaxis9, a);
                ctx.fillText(sensorMax[a] + "/" + sensormin[a] + "/" + sensorAvg[a].toFixed(1), tx, 75 + a * 70);
            }
        } 

        function YdrawSensorCurvline(ctx) { 
            Ydrawonecurve(ctx, sensor0, yaxis0, 1);
            Ydrawonecurve(ctx, sensor1, yaxis1, 2);
            Ydrawonecurve(ctx, sensor2, yaxis2, 3);
            Ydrawonecurve(ctx, sensor3, yaxis3, 4);
            Ydrawonecurve(ctx, sensor4, yaxis4, 5);
            Ydrawonecurve(ctx, sensor5, yaxis5, 6);
            Ydrawonecurve(ctx, sensor6, yaxis6, 7);
            Ydrawonecurve(ctx, sensor7, yaxis7, 8);
            Ydrawonecurve(ctx, sensor8, yaxis8, 9);
        }

        function Ydrawonecurve(ctx, sen, ya, n) {
            var a;
            var linecolor = 0;
            //var startpos = 120; 
            var tx = DragSensorList.length * 80 + 20;
            //ctx.lineTo(tx + 10 + a * 81, 750);
         
            var src, dst, M, m, step, s, r;
            if (DragSensorList.length < n) return;

            ctx.save(); 
            ctx.strokeStyle = colors[n-1];
            ctx.beginPath();
            src = parseFloat(ya[719]) - parseFloat(ya[n - 1]);
            dst = parseFloat(ya[719 - scrollmin[n - 1]]) - parseFloat(ya[719 - scrollmax[n - 1]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[n - 1]);
            s = (sen[0] - M) / step;
            ctx.moveTo(tx, 740 - s);// 先畫出線 
            for (a = 0; a < 550; a++) {
                s = (sen[a] - M) / step;
                ctx.lineTo(tx + a * 1.333, 740 - s);
            }
            ctx.stroke();
            ctx.restore();
        }

        function getparam() {
            // Step1 取得網址 
            console.log(location.href);
            var getUrlString = location.href;//取得網址，並存入變數
            //Step2 將網址 (字串轉成URL)
            var url = new URL(getUrlString);
            // Step3 使用URL.searchParams + get 函式  (括弧裡面帶入欲取得結果的KEY鍵值參數) 
            var id = url.searchParams.get('id');
            var a = url.searchParams.get('a');
            var b = url.searchParams.get('b');

            if (id == null)
                console.log("NULL");
            else
                console.log(id);
            console.log(url.searchParams.get('a'));
            console.log(url.searchParams.get('b'));


            var c = url.searchParams.get('c');
            var d = url.searchParams.get('d');
            if (c != null && d != null) {
                document.getElementById("test1").innerText = d;
                console.log(d);
            }
        }

        function CanvasFG_mouosedown(e) { 
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left; 
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            // document.getElementById("test1").innerText = ",now>>down";
            //console.log(y);
            // isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
            if (x > lineA - 3 && x < lineA + 3) {
                //  document.getElementById("test1").innerText = ",now>>down>>A";
                isDragline = 1;
            }
            else if (x > lineB - 3 && x < lineB + 3) {
                isDragline = 2;
            }             

            if (x > 10 && x < 260) {// var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
                var n = parseInt((y - 13) / 30);
                var ty = n * 30 + 13;
                //console.log(ty+":"+y);
                if (y > ty && y < ty + 21)
                    isDragSensor = n;
                else
                    isDragSensor = -1;
                console.log(parseInt(scroll_pos / (877 / sensorList.length)));
                console.log(isDragSensor);
            }
            
            if (x > 260 && x < 276) { // isDragScroll
                if (y > scroll_pos && y < (scroll_pos + scroll_len - 1)) {
                    isDragScroll = 1;
                    DragScroll_start = y;
                }
            }

            //ctx.fillRect(367, scrollmin[nowCurve] + 8, 13, 8);
            //ctx.fillStyle = "#888888"; // scrollmax
            //ctx.fillRect(367, scrollmax[nowCurve] + 8, 13, 8);
            if (x >= 367 && x < 380 && nowCurve >-1) {// isScrollRange
                if (y > scrollmin[nowCurve] + 8 && y < scrollmin[nowCurve] + 16)
                    isScrollRange = 1;
                else if(y > scrollmax[nowCurve] + 8 && y < scrollmax[nowCurve] + 16)
                    isScrollRange = 2;
            }

            if (x > 1320) {
                if (y > 40) {
                    nowCurve = parseInt((y - 45) / 70);
                    if (nowCurve > 8 || nowCurve > DragSensorList.length-1) nowCurve = -1;
                }
                else
                    nowCurve = -1;
                draw();
            }
        }

        function CanvasFG_mouosemove(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            //document.getElementById("test").innerText = "Coordinates: (" + x + "," + y + ")";
            //document.getElementById("test1").innerText = ",now>>move";
            //console.log("mouosemove:"+y);
            if (isDragline == 1) {// isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
                if (x > 400 && x < 1499) { // 曲線範圍內才可以拖拉
                    lineA = x;
                    draw();
                   // console.log(x + ";" + y);
                }
            }
            else if (isDragline == 2) {// isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
                if (x > 400 && x < 1499) {// 曲線範圍內才可以拖拉
                    lineB = x;
                    draw(); 
                }
            }
            if (isDragSensor > -1) {
                DragSensorx = x; DragSensory = y;
                draw();
                // console.log(x + ";" + y);
            }

            if (isDragScroll == 1) {
                //var t = parseInt(y - DragScroll_start);
                //console.log(y + "," + DragScroll_start + "," + t + "," + parseInt(877 / sensorList.length));
                //// 
                //if ((scroll_pos + t) < 0 || sensorList.length < 29)
                //    scroll_dis = 0;
                //else if ((scroll_pos + t + scroll_len) > 877) {
                //    scroll_dis = 877 - scroll_len - scroll_pos;
                //}
                //else
                //    scroll_dis = t;
                var a;
                var t = parseInt(y - DragScroll_start);
                if (t < 0 && Math.abs(t) > scroll_pos)
                    scroll_pos = 0;
                else if (t > 0 && (scroll_pos + scroll_len + t) > 877)
                    scroll_pos = 877 - scroll_len;
                else
                    scroll_pos += t;
                DragScroll_start = y;
                //console.log("scroll_pos=" + scroll_pos + " , t=" + t);
                draw();
            }

            //ctx.fillRect(367, scrollmin[nowCurve] + 8, 13, 8);
            //ctx.fillStyle = "#888888"; // scrollmax
            //ctx.fillRect(367, scrollmax[nowCurve] + 8, 13, 8);

            if (x >= 367 && x < 380 && nowCurve > -1) {// isScrollRange
                if (isScrollRange == 1) {
                    if (y >= 8 && y < 728 && y < (scrollmax[nowCurve] - 12)) {
                        scrollmin[nowCurve] = y - 8;
                        draw();
                    } 
                }
                else if (isScrollRange == 2) {
                    if (y >= 8 && y < 728 && y > (scrollmin[nowCurve] + 12)) {
                        scrollmax[nowCurve] = y - 8;
                        draw();
                    }
                }

            }
        }

        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            //  document.getElementById("test1").innerText = ",now>>up";

            if (isDragline > -1) {
                isDragline = -1;
                draw();
            }

            if (isDragScroll == 1) {
                isDragScroll = -1;
                //scroll_pos+=scroll_dis ;
                // draw();
            }

            if (isScrollRange > -1) {
                isScrollRange = -1;
            }

            if (isDragSensor > -1) { // 有選到sensor
                if (x < 280)
                    isDragSensor = -1;
                else if (x > 282) {
                    var a ;
                    var t = parseInt(scroll_pos / parseInt(877 / sensorList.length));
                    t += isDragSensor;
                    if (DragSensorList.length >= 9) {
                        isDragSensor = -1;
                        alert('超過九筆了，不可再增加');
                        draw();
                        return; // 已經滿10個了
                    }

                    for (a = 0; a < DragSensorList.length; a++) { // 先看看是否已經拉過了
                        if (DragSensorList[a] == t) break;
                    }
                    //console.log("a=" + a + ", DragSensorList.length=" + DragSensorList.length);
                    if (a >= DragSensorList.length || DragSensorList.length == 0) { // 沒拉過，加入串列
                        DragSensorList.push(t);
                        // console.log("DragSensorList length:" + DragSensorList.length + ",isDragSensor=" + isDragSensor);
                        GenSensorGrid(DragSensorList.length - 1);
                        nowCurve = DragSensorList.length - 1;
                    } 
                    isDragSensor = -1;
                    //for ( a = 0; a < DragSensorList.length; a++)
                    //    console.log("DragSensorList"+a+":"+DragSensorList[a]); 
                    draw();
                }
            }
        }

        function GetsensorList() {
            var actionUrl;
            var f = document.getElementById("DDarea").value;
            var g = document.getElementById("DDclass").value;
            var h = document.getElementById("DDdevice").value;

            DragSensorList = new Array();
            isDragSensor = -1;
            isDragline = -1;
            nowCurve = -1;

            actionUrl = "wf.aspx?c=A02&f=" + f + "&g=" + g + "&h=" + h;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData(actionUrl); // 抓取json 
            console.log("sensorList:" + sensorList.length)
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料";
            var a;
            //console.log(details);

            if (sensorList.length < 29)
                scroll_len = 877; //每筆30，最多29筆
            else {
                scroll_len = parseInt((29*877) /(sensorList.length -29) );
            }
            scroll_pos = 0;
            console.log("scroll_len=" + scroll_len);
            draw();
            // $('#test1').html(details); // 找到test1，並且把資訊顯示上去
        }

        function GetData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.ID + ":" + item.name;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }

        function getSensorGroup(n) {
            var a1 = document.getElementById("DDyear1").value;
            var a2 = document.getElementById("DDmonth1").value;
            var a3 = document.getElementById("DDday1").value;
            var a4 = document.getElementById("DDhour1").value;
            var a5 = document.getElementById("DDminute1").value;
            var b1 = document.getElementById("DDyear2").value;
            var b2 = document.getElementById("DDmonth2").value;
            var b3 = document.getElementById("DDday2").value;
            var b4 = document.getElementById("DDhour2").value;
            var b5 = document.getElementById("DDminute2").value;
            selectStartdate = a1 + a2 + a3 + a4 + a5;
            selectEnddate = b1 + b2 + b3 + b4 + b5;
            selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
            selectEnddateint = dateToint(b1, b2, b3, b4, b5);
            //console.log(selectStartdateint);
            //console.log(selectEnddateint);
            var a;

            var actionUrl, actionUrl1; // 每一個sensor 獨立重新讀取
            console.log("ID:" + n);
            //var id = (String)(sensorList[DragSensorList[n]]); // << 先讀取第一個 ，再來要包成迴圈         
            //var pos = id.indexOf(":");
            //id = id.substr(0, pos);
            //console.log(id);

            actionUrl = "wf.aspx?c=A09&d=" + n;
            actionUrl1 = "wf.aspx?c=A10&d=" + n;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            DragSensorList = [];
            DragSensorList = GetsensorGroupdata(actionUrl); // 抓取json  
            sensorList = [];
            nowCurve = -1;
            sensorList = GetsensorGroupdata1(actionUrl1);
            cal_GroupID();

            if (sensorList.length < 29)
                scroll_len = 877; //每筆30，最多29筆 

            scroll_pos = 0;
            console.log("scroll_len=" + scroll_len);

            for (a = 0; a < 10; a++) {
                if (a > DragSensorList.length - 1) break;
                GenSensorGrid(a);
                nowCurve = -1;
            }
            draw();
        }

        function cal_GroupID() {
            var a, b;  
            for (a = 0; a <= DragSensorList.length - 1; a++) { 
                sensorList[a] = DragSensorList[a].toString() + ":" + sensorList[a];
                DragSensorList[a] = a;
                console.log(sensorList[a]+","+DragSensorList[a]);
            }
        }

        function GetsensorGroupdata(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(parseInt(temp));
                        });
                    }
                }
            });
            return result;
        }

        function GetsensorGroupdata1(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }

        function GenAllSensorGrid() {
            var a1 = document.getElementById("DDyear1").value;
            var a2 = document.getElementById("DDmonth1").value;
            var a3 = document.getElementById("DDday1").value;
            var a4 = document.getElementById("DDhour1").value;
            var a5 = document.getElementById("DDminute1").value;
            var b1 = document.getElementById("DDyear2").value;
            var b2 = document.getElementById("DDmonth2").value;
            var b3 = document.getElementById("DDday2").value;
            var b4 = document.getElementById("DDhour2").value;
            var b5 = document.getElementById("DDminute2").value;
            selectStartdate = a1 + a2 + a3 + a4 + a5;
            selectEnddate = b1 + b2 + b3 + b4 + b5;
            selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
            selectEnddateint = dateToint(b1, b2, b3, b4, b5);
            //console.log(selectStartdateint);
            //console.log(selectEnddateint);
            var a;

            for (a = 0; a < 10; a++) {
                if (a > DragSensorList.length - 1) break;
                GenSensorGrid(a);
                nowCurve = -1;
            }
            draw();
        }

        function GenSensorGrid(n) {
            //DDyear1  DDmonth1  DDday1  DDhour1  DDminute1  ~ DDyear2  DDmonth2  DDday2  DDhour2  DDminute2 
            var a1 = document.getElementById("DDyear1").value;
            var a2 = document.getElementById("DDmonth1").value;
            var a3 = document.getElementById("DDday1").value;
            var a4 = document.getElementById("DDhour1").value;
            var a5 = document.getElementById("DDminute1").value;
            var b1 = document.getElementById("DDyear2").value;
            var b2 = document.getElementById("DDmonth2").value;
            var b3 = document.getElementById("DDday2").value;
            var b4 = document.getElementById("DDhour2").value;
            var b5 = document.getElementById("DDminute2").value;
            selectStartdate = a1 + a2 + a3 + a4 + a5;
            selectEnddate = b1 + b2 + b3 + b4 + b5;
            selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
            selectEnddateint = dateToint(b1, b2, b3, b4, b5);

            var a;
            var s = selectStartdateint;
            var e = selectEnddateint;
            var interval = (e - s) / 9; 

            timeinterval = new Array();// 月份由0開始
            //console.log("1:" + intTodate(s));
            timeinterval.push(dateformat(intTodate(s)));
            for (a = 1; a < 9; a++) {
               // console.log((a + 1) + ":" + intTodate(s + a * interval));
                timeinterval.push(dateformat(intTodate(s + a * interval)));
            }
            //console.log("9:" + intTodate(e));
            timeinterval.push(dateformat(intTodate(e)));

            //for (a = 0; a < timeinterval.length; a++)
            //    console.log("timeinterval:" + timeinterval[a]);

            var actionUrl; // 每一個sensor 獨立重新讀取
            console.log("ID");
            var id = (String)(sensorList[DragSensorList[n]]); // << 先讀取第一個 ，再來要包成迴圈         
            var pos = id.indexOf(":");
            id = id.substr(0, pos);
            console.log(id);

            if (id.length > 0)
                actionUrl = "wf.aspx?c=A01&d=" + selectStartdate + "&f=" + selectEnddate + "&g=" + id;
            else {
                console.log("no sensor select...");
                return;
            }
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000); 
             
            var start_date = new Date(); // 取得一開始的時間
            if (n == 0) {
                sensor0 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(0, sensor0);
            }
            else if (n == 1) {
                sensor1 = GetsensorData(actionUrl); // 抓取json
                calmax_min_avg(1, sensor1);
            }
            else if (n == 2) {
                sensor2 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(2, sensor2);
            }
            else if (n == 3) {
                sensor3 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(3, sensor3);
            }
            else if (n == 4) {
                sensor4 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(4, sensor4);
            }
            else if (n == 5) {
                sensor5 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(5, sensor5);
            }
            else if (n == 6) {
                sensor6 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(6, sensor6);
            }
            else if (n == 7) {
                sensor7 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(7, sensor7);
            }
            else if (n == 8) {
                sensor8 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(8, sensor8);
            }
            else if (n == 9) {
                sensor9 = GetsensorData(actionUrl); // 抓取json
                calmax_min_avg(9, sensor9);
            }

            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensor0.length + "筆資料";
            console.log(details);

            // draw();
        }

        function GetsensorData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(parseFloat(temp));
                        });
                    }
                }
            });
            return result;
        }

        function dateToint(y, M, d, h, m) {
            var nowdate = new Date(y, M - 1, d, h, m); // 月份由0開始
            //console.log(y + "," + M + "," + d + "," + h + "," + m);
            //console.log(nowdate);
            var ticks = ((nowdate.getTime() * 10000) + 621355968000000000);
            return ticks;//10000 are the ticks per millisecond.
        }

        function intTodate(n) {
            //var epochMicrotimeDiff = 621355968000000000; 
            var ticksToMicrotime = n - 621355968000000000;
            var tickDate = new Date(ticksToMicrotime / 10000);
            return tickDate;
        }

        function dateformat(d) { // 傳入date ，傳回2018/04/03 5:35 
            var M = d.getMonth() + 1;
            M = (M < 10) ? "0" + M : M;
            var D = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
            var H = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var m = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            var s = (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();
            var temp = d.getFullYear() + "/" + M + "/" + D + " " + H + ":" + m + ":" + s;
            //console.log("dateformat" + temp);
            return temp;
        }

        function testtime() {
            var n = dateToint(2018, 4, 10, 6, 52);
            console.log("test:" + n);

            var d = intTodate(n);
            console.log("test:" + d);

            timeinterval = new Array();
            var sdate = new Date(2018, 3, 1, 10, 20); // 月份由0開始
            var edate = new Date(2018, 3, 1, 19, 20); // 月份由0開始
            var s = dateToint(2018, 4, 1, 10, 20);
            var e = dateToint(2018, 4, 1, 19, 20);
            var interval = (e - s) / 9;
            var a;
            console.log("1:" + intTodate(s));
            timeinterval.push(dateformat(intTodate(s)));
            for (a = 1; a < 9; a++) {
                console.log((a + 1) + ":" + intTodate(s + a * interval));
                timeinterval.push(dateformat(intTodate(s + a * interval)));
            }
            console.log("9:" + intTodate(e));
            timeinterval.push(dateformat(intTodate(e)));

            for (a = 0; a < timeinterval.length; a++)
                console.log("timeinterval:" + timeinterval[a]);
        }

        function calmax_min_avg(n, sen) {
            var max = sen[0], min = sen[0], avg = 0, c = 0;
            var a;
            for (a = 1; a < sen.length; a++) {
                if (sen[a] > max)
                    max = sen[a];
                else if (sen[a] < min)
                    min = sen[a];
                avg += parseFloat(sen[a]);
            }
            sensorMax[n] = max;
            sensormin[n] = min;
            sensorAvg[n] = avg / sen.length;
            //console.log("calmax_min_avg");
            //console.log(sensorMax[n]);
            //console.log(sensormin[n]);
            //console.log(sensorAvg[n]);

            if (n == 0)
                cal_Yaxis(max, min, 0);
            else if (n == 1)
                cal_Yaxis(max, min, 1);
            else if (n == 2)
                cal_Yaxis(max, min, 2);
            else if (n == 3)
                cal_Yaxis(max, min, 3);
            else if (n == 4)
                cal_Yaxis(max, min, 4);
            else if (n == 5)
                cal_Yaxis(max, min, 5);
            else if (n == 6)
                cal_Yaxis(max, min, 6);
            else if (n == 7)
                cal_Yaxis(max, min, 7);
            else if (n == 8)
                cal_Yaxis(max, min, 8);
            else if (n == 9)
                cal_Yaxis(max, min, 9);
            console.log("n=" +n);
            console.log("yaxis0=" + yaxis0.length);
            //console.log(sen.length);
            //console.log(avg);
            //console.log(max);
            //console.log(min);
            //console.log(sensorAvg[n]);
        }

        function cal_Yaxis(max, min, n) {
            if (n == 0)
                yaxis0 = [];
            else if (n == 1)
                yaxis1 = [];
            else if (n == 2)
                yaxis2 = [];
            else if (n == 3)
                yaxis3 = [];
            else if (n == 4)
                yaxis4 = [];
            else if (n == 5)
                yaxis5 = [];
            else if (n == 6)
                yaxis6 = [];
            else if (n == 7)
                yaxis7 = [];
            else if (n == 8)
                yaxis8 = [];
            else if (n == 9)
                yaxis9 = [];


            // var yaxis0 = new Array(); // 720筆資料
            //yaxis0.push(val);
            console.log("cal_Yaxis");
            var d, new_max, new_min, step, a;
            d = max - min;
            console.log("max=" + max);
            console.log("min=" + min);
            console.log("d=" + d);
           
            new_max = parseFloat(max) + parseFloat(d / 4);
            new_min = parseFloat(min) - parseFloat(d / 4);
            
            console.log("new_max=" + new_max);
            console.log("new_min=" + new_min);
            //if (d < 50) {
            //    new_max = max + d * 0.25;
            //    new_min = min - d * 0.25;
            //}
            //else if (d < 150) {
            //    new_max = max + d * 0.25;
            //    new_min = min - d * 0.25;
            //}
            //else {
            //    new_max = max + d * 0.25;
            //    new_min = min - d * 0.25;
            //}
            d = new_max - new_min;
            step = d / 720.0;
            console.log("step=" + step);

            for (a = 0; a <= 720; a++) {
                if (n == 0)
                    yaxis0.push(parseFloat(new_min + a * step));
                else if (n == 1)
                    yaxis1.push(parseFloat(new_min + a * step));
                else if (n == 2)
                    yaxis2.push(parseFloat(new_min + a * step));
                else if (n == 3)
                    yaxis3.push(parseFloat(new_min + a * step));
                else if (n == 4)
                    yaxis4.push(parseFloat(new_min + a * step));
                else if (n == 5)
                    yaxis5.push(parseFloat(new_min + a * step));
                else if (n == 6)
                    yaxis6.push(parseFloat(new_min + a * step));
                else if (n == 7)
                    yaxis7.push(parseFloat(new_min + a * step));
                else if (n == 8)
                    yaxis8.push(parseFloat(new_min + a * step));
                else if (n == 9)
                    yaxis9.push(parseFloat(new_min + a * step));
            }
            // console.log("yaxis0=" + sax.length);
            for (a = 0; a < sensor0.length; a++) {
                if (n == 0)
                    sensor0[a] = parseFloat((sensor0[a] - new_min) / step);
                else if (n == 1)
                    sensor1[a] = parseFloat((sensor1[a] - new_min) / step);
                else if (n == 2)
                    sensor2[a] = parseFloat((sensor2[a] - new_min) / step);
                else if (n == 3)
                    sensor3[a] = parseFloat((sensor3[a] - new_min) / step);
                else if (n == 4)
                    sensor4[a] = parseFloat((sensor4[a] - new_min) / step);
                else if (n == 5)
                    sensor5[a] = parseFloat((sensor5[a] - new_min) / step);
                else if (n == 6)
                    sensor6[a] = parseFloat((sensor6[a] - new_min) / step);
                else if (n == 7)
                    sensor7[a] = parseFloat((sensor7[a] - new_min) / step);
                else if (n == 8)
                    sensor8[a] = parseFloat((sensor8[a] - new_min) / step);
                else if (n == 9)
                    sensor9[a] = parseFloat((sensor9[a] - new_min) / step);
            }
        }
         
        function MyDropDownYearFunction() { 
            //console.log(document.getElementById("DDyear1").value); 
            //console.log($("#DDyear1").val());  
            document.getElementById("DDyear2").value = document.getElementById("DDyear1").value;
        }
        function MyDropDownMonthFunction() {
            //   console.log($("#DDmonth1").text)
            //console.log(document.getElementById("DDmonth1").value);
            document.getElementById("DDmonth2").value = document.getElementById("DDmonth1").value;
        }
        function MyDropDownDayFunction() {
            //   console.log($("#DDmonth1").text)
            //console.log(document.getElementById("DDday1").value);
            document.getElementById("DDday2").value = document.getElementById("DDday1").value;
        }
        function MyDropDownHourFunction() {
            //   console.log($("#DDmonth1").text)
            //console.log(document.getElementById("DDhour1").value);
            document.getElementById("DDhour2").value = document.getElementById("DDhour1").value;
        }
        function MyDropDownMinuteFunction() {
            //   console.log($("#DDmonth1").text)
            //console.log(document.getElementById("DDminute1").value);
            document.getElementById("DDminute2").value = document.getElementById("DDminute1").value;
        }

        function removecurve() {
            if (drawtype == 2) return; // 全Y曲線情形下不可刪除
            if (nowCurve == -1) return; // 沒選到
            if (DragSensorList.length < 1) return; //沒東西

            var a;
            // 1. move all sensor n after nowCurve to sensor n-1 
            //for (a = nowCurve; a < DragSensorList.length - 1; a++)
            //    moveArray(indexsensor[a + 1], indexsensor[a]); // move a1 to a2
            moveSensor(nowCurve);

            // 2. move all yaxis n after nowCurve to yaxis n-1
            //for (a = nowCurve; a < DragSensorList.length - 1; a++)
            //    moveArray(indexyaxis[a + 1], indexyaxis[a]); // move a1 to a2
            moveYaxis(nowCurve);

            var sensorMax = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
            var sensormin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
            var sensorAvg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
            var scrollmax = new Array(719, 719, 719, 719, 719, 719, 719, 719, 719);
            var scrollmin = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
            for (a = nowCurve; a < DragSensorList.length - 1; a++) {
                sensorMax[a] = sensorMax[a + 1];
                sensormin[a] = sensormin[a + 1];
                sensorAvg[a] = sensorAvg[a + 1];
                scrollmax[a] = scrollmax[a + 1];
                scrollmin[a] = scrollmin[a + 1];
            }

            //3. remove one element from DragSensorList
            DragSensorList.splice(nowCurve, 1); //delete at pos=3,  one element

            nowCurve = -1;
            draw();
        }
        
        function moveSensor(n) {
            if (n == 0) {
                moveArray(sensor1, sensor0,1); // move a1 to a2
                moveArray(sensor2, sensor1,2);
                moveArray(sensor3, sensor2,3);
                moveArray(sensor4, sensor3,4);
                moveArray(sensor5, sensor4,5);
                moveArray(sensor6, sensor5,6);
                moveArray(sensor7, sensor6,7);
                moveArray(sensor8, sensor7,8);
            }
            else if (n == 1) {
                moveArray(sensor2, sensor1, 2);
                moveArray(sensor3, sensor2, 3);
                moveArray(sensor4, sensor3, 4);
                moveArray(sensor5, sensor4, 5);
                moveArray(sensor6, sensor5, 6);
                moveArray(sensor7, sensor6, 7);
                moveArray(sensor8, sensor7, 8);
            }
            else if (n == 2) { 
                moveArray(sensor3, sensor2, 3);
                moveArray(sensor4, sensor3, 4);
                moveArray(sensor5, sensor4, 5);
                moveArray(sensor6, sensor5, 6);
                moveArray(sensor7, sensor6, 7);
                moveArray(sensor8, sensor7, 8);
            }
            else if (n == 3) { 
                moveArray(sensor4, sensor3, 4);
                moveArray(sensor5, sensor4, 5);
                moveArray(sensor6, sensor5, 6);
                moveArray(sensor7, sensor6, 7);
                moveArray(sensor8, sensor7, 8);
            }
            else if (n == 4) { 
                moveArray(sensor5, sensor4, 5);
                moveArray(sensor6, sensor5, 6);
                moveArray(sensor7, sensor6, 7);
                moveArray(sensor8, sensor7, 8);
            }
            else if (n == 5) { 
                moveArray(sensor6, sensor5, 6);
                moveArray(sensor7, sensor6, 7);
                moveArray(sensor8, sensor7, 8);
            }
            else if (n == 6) { 
                moveArray(sensor7, sensor6, 7);
                moveArray(sensor8, sensor7, 8);
            }
            else if (n == 7) { 
                moveArray(sensor8, sensor7, 8);
            } 
        }

        function moveYaxis(n) {
            if (n == 0) {
                moveArray(yaxis1, yaxis0, 1);
                moveArray(yaxis2, yaxis1, 2);
                moveArray(yaxis3, yaxis2, 3);
                moveArray(yaxis4, yaxis3, 4);
                moveArray(yaxis5, yaxis4, 5);
                moveArray(yaxis6, yaxis5, 6);
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 1) { 
                moveArray(yaxis2, yaxis1, 2);
                moveArray(yaxis3, yaxis2, 3);
                moveArray(yaxis4, yaxis3, 4);
                moveArray(yaxis5, yaxis4, 5);
                moveArray(yaxis6, yaxis5, 6);
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 2) {
                moveArray(yaxis3, yaxis2, 3);
                moveArray(yaxis4, yaxis3, 4);
                moveArray(yaxis5, yaxis4, 5);
                moveArray(yaxis6, yaxis5, 6);
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 3) {
                moveArray(yaxis4, yaxis3, 4);
                moveArray(yaxis5, yaxis4, 5);
                moveArray(yaxis6, yaxis5, 6);
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 4) {
                moveArray(yaxis5, yaxis4, 5);
                moveArray(yaxis6, yaxis5, 6);
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 5) {
                moveArray(yaxis6, yaxis5, 6);
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 6) {
                moveArray(yaxis7, yaxis6, 7);
                moveArray(yaxis8, yaxis7, 8);
            }
            else if (n == 7) {
                moveArray(yaxis8, yaxis7, 8);
            }
        }

        function moveArray(a1,a2,n) { // move a1 to a2
            var a;
            if (n >= DragSensorList.length) return;

            for (a=0;a<a2.length;a++)
                a2[a] = a1[a];
        }

        window.onload = function (e) {
            draw();
        }


    </script>
    <style>
        #myCanvasFG {
            position: static;
            border: 1px solid black;
        }

        #myCanvasBG {
            position: static;
            border: 1px solid black;
        }
    </style>
</head>
<body style="background-color: #ddffff">
    <form id="form1" runat="server">
        <div id="divUP" style="width: 1890px; height: 75px; border: 1px solid #c3c3c3">
            <div style="font-size: 20px">
                <table>
                    <tr>
                        <td style="width: 1300px">廠區 
            <asp:DropDownList ID="DDarea" runat="server" Width="160px" Font-Size="Large">
                <asp:ListItem Value="1">一廠</asp:ListItem>
                <asp:ListItem Value="2">二廠</asp:ListItem>
                <asp:ListItem Value="3">三廠</asp:ListItem>
                <asp:ListItem Value="4">四廠</asp:ListItem>
                <asp:ListItem Value="5">五廠</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            課別 
            <asp:DropDownList ID="DDclass" runat="server" Width="120px" Font-Size="Large" AutoPostBack="True">
                <asp:ListItem Value="1">水務</asp:ListItem>
                <asp:ListItem Value="2">氣化</asp:ListItem>
                <asp:ListItem Value="3">空調</asp:ListItem>
                <asp:ListItem Value="4">電力</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp; &nbsp;&nbsp;
            設備 
            <asp:DropDownList ID="DDdevice" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">純水設備</asp:ListItem>
                <asp:ListItem Value="2">淨化設備</asp:ListItem>
                <asp:ListItem Value="3">冷卻設備</asp:ListItem>
                <asp:ListItem Value="4">過濾設備</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp;  
            
            
            <img id="btnGetsensorList" onclick="GetsensorList();" src="images/buttom.jpg" />
                            &nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">回首頁</asp:HyperLink>

                            <label id="test"></label>
                            <label id="test1"></label>
                            &nbsp;&nbsp;
                <label id="test2">拖拉tag到右邊 </label>

                            <br />

                            日期:
            <asp:DropDownList ID="DDyear1" runat="server" onchange="javascript:MyDropDownYearFunction();" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
                            年&nbsp; 
            <asp:DropDownList ID="DDmonth1" runat="server" onchange="javascript:MyDropDownMonthFunction();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            月&nbsp; 
            <asp:DropDownList ID="DDday1" runat="server" onchange="javascript:MyDropDownDayFunction();" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                            日&nbsp; 
            <asp:DropDownList ID="DDhour1" runat="server" onchange="javascript:MyDropDownHourFunction();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            時&nbsp; 
            <asp:DropDownList ID="DDminute1" runat="server" onchange="javascript:MyDropDownMinuteFunction();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            分&nbsp;～ 
            <asp:DropDownList ID="DDyear2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            年&nbsp; 
            <asp:DropDownList ID="DDmonth2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            月&nbsp; 
            <asp:DropDownList ID="DDday2" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                            日&nbsp; 
            <asp:DropDownList ID="DDhour2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            時&nbsp; 
            <asp:DropDownList ID="DDminute2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                            分
            
            <img id="btnGenSensorGrid" onclick="GenAllSensorGrid();" src="images/buttom.jpg" />
                        </td>
                        <td style="width: 560px; text-align: left; vertical-align: top;">
                            <asp:Literal ID="Literal1" runat="server"></asp:Literal> 
                            <%--  <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="index.aspx">臨時維修工班1</asp:HyperLink>、
                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="index.aspx">水務課常用</asp:HyperLink>、
                            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="index.aspx">研磨區專用</asp:HyperLink>、
                            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="index.aspx">玻璃切割區</asp:HyperLink>、
                            <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="index.aspx">無塵室曝光區</asp:HyperLink>、
                            <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="index.aspx">玻璃堆疊區</asp:HyperLink>、
                            <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="index.aspx">產線更新工班２</asp:HyperLink>、--%>
                        
                        </td>
                    </tr>
                </table>
            </div>

            <div style="visibility: hidden;">
                <canvas id="myCanvasBG" width="1580" height="880"></canvas>
            </div>
            <div style="position:absolute;left:1650px;top:780px">
<img id="btndrawAllcurve" onclick="drawAllcurve_y();" src="images/allcurves.jpg" /> 
            </div> 
            <div style="position:absolute;left:1650px;top:820px"> 
<img id="btnRemovecurve" onclick="removecurve();" src="images/removecurves.jpg" />
            </div> 
        </div>
        <canvas id="myCanvasFG" width="1890" height="880" onmousemove="CanvasFG_mouosemove(event)" onmousedown="CanvasFG_mouosedown(event)" onmouseup="CanvasFG_mouoseup(event)"></canvas>
    </form>
</body>
</html>
