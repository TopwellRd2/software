﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminFactory.aspx.vb" Inherits="SQLWeb.AdminFactory" %>

<%@ Register Src="~/ComfirmButton.ascx" TagPrefix="uc1" TagName="ComfirmButton" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: center" colspan="2">
                        <a href="index.aspx">回首頁</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="08管理系統.aspx">回管理首頁</a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%; font-size: 30px">功能列表 
                    </td>
                    <td style="width: 75%;" rowspan="2">
                        <div style="text-align: left">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right" class="auto-style2">
                                        <br />
                                        廠區列表<br />
                                        <br />
                                        <br />
                                        <br />
                                        &nbsp;</td>
                                    <td>
                                        <asp:ListBox ID="ListBox1" runat="server" Height="140px" Width="548px" AutoPostBack="True"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">
                                        <asp:Label ID="txtID" runat="server" ForeColor="#CC6600"></asp:Label>
                                        <asp:Label ID="txtState" runat="server" Text="無" ForeColor="#3333CC" Width="64px"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:Label ID="txtMSG" runat="server" ForeColor="#C04000" Width="300px" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                       &nbsp;&nbsp;&nbsp;&nbsp;  
                                        <asp:Button ID="cmdNew" runat="server" Text="新增" BackColor="#FFFFC0" />&nbsp;
                                        <uc1:ComfirmButton ID="cmdSave" runat="server" CommandName="儲存" />
                                        <uc1:ComfirmButton ID="cmdDelete" runat="server" CommandName="刪除" /> 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">名稱: 
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="txtName" runat="server" Width="360px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;">備註:</td>
                                    <td style="text-align: left;" class="auto-style1">
                                        <asp:TextBox ID="txtComment" runat="server" Width="500px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td style="width: 25%; font-size: 25px; vertical-align: top">
                        <asp:HyperLink ID="HLAdminUser" runat="server" NavigateUrl="AdminUser.aspx">帳號管理</asp:HyperLink>
                        <br />
                        <asp:HyperLink ID="HLSensor" runat="server" NavigateUrl="AdminSensor.aspx">感應器管理</asp:HyperLink>
                        <br />
                        <asp:HyperLink ID="HLFactory" runat="server" NavigateUrl="AdminFactory.aspx">廠區管理</asp:HyperLink>
                        <br />
                        <asp:HyperLink ID="HLDevice" runat="server" NavigateUrl="AdminDevice.aspx">設備管理</asp:HyperLink>
                        <br />
                        <asp:HyperLink ID="HLClass" runat="server" NavigateUrl="AdminClass.aspx">課別管理</asp:HyperLink>
                        <br />
                        <asp:HyperLink ID="HLOPC" runat="server" NavigateUrl="AdminOPC.aspx" >OPC管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLGateway" runat="server" NavigateUrl="AdminGateway.aspx" >Gateway管理</asp:HyperLink> <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">底部
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>


