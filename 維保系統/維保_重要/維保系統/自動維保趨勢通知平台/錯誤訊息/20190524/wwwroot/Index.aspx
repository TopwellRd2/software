﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Index.aspx.vb" Inherits="SQLWeb.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
        <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        var testcount = 0;
        //方法1
        //function openSelectWindow() {
        //    var w = window.open('02多曲線同時顯示.aspx', '_blank', 'width=1900,height=920,left=0,top=0');
        //    //w.document.getElementById("aa").value = 123;
        //    //w.document.getElementById("bb").innerText = 555;
        //    //w.document.getElementById("cc").innerText = 666666666666;

        //    setTimeout(function () { PassValueToForm(w); }, 1000);
        //}
        //function PassValueToForm(PassForm) {
        //    if (PassForm.document.getElementById("aa")) {
        //        PassForm.document.getElementById("aa").value = 123;
        //    } else {
        //        if (testcount <= 10) //超過10次就不試了
        //            setTimeout(function () { PassValueToForm(PassForm); }, 1000);
        //    }
        //    testcount++;
        //}

        //方法二：cookie传参
        //function setCookie(cname, cvalue, exdays) {
        //    var d = new Date();
        //    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        //    var expires = "expires=" + d.toUTCString();
        //    document.cookie = cname + "=" + cvalue + "; " + expires;
        //}

        //function getCookie(cname) {
        //    var name = cname + "=";
        //    var ca = document.cookie.split(";");
        //    for (var i = 0; i < ca.length; i++) {
        //        var c = ca[i];
        //        while (c.charAt(0) == ' ') {
        //            c = c.substring(1);
        //        }
        //        if (c.indexof(cname) == 0) {
        //            return c.substring(name.length, c.length);
        //        }
        //    }
        //    return "";
        //}

    </script>    

    <style type="text/css">
        .auto-style1 {
            width: 1087px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 30px">首頁: 
                                    <asp:LinkButton ID="btnLogin" runat="server">登入</asp:LinkButton>
                                </td> 
                            </tr>
                            <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="08管理系統.aspx"><img src="images/008.jpg" width="150" height="150"/>
                                                <br />
                                                管理系統</a> 
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="02多曲線雙時間軌分析.aspx">
                                                    <img src="images/002.jpg" width="150" height="150"/>
                                                <br />
                                                多曲線雙時間軌分析  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="03設備不同時間點分析.aspx"><img src="images/003.jpg" width="150" height="150"/>
                                                <br />
                                                設備不同時間點分析  </a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="04維保趨勢通知.aspx"><img src="images/004.jpg" width="150" height="150"/>
                                                <br />
                                                維保趨勢通知  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="05RealSensor.aspx"><img src="images/005.jpg" width="150" height="150"/>
                                                <br />
                                                數據監控紀錄  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="06進階警報管理.aspx"><img src="images/006.jpg" width="150" height="150"/>
                                                <br />
                                                進階警報管理  </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="07上下限警報通知.aspx"><img src="images/007.jpg" width="150" height="150"/>
                                                <br />
                                                上下限警報通知 </a> 
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                               
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                               
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 14px; border: dotted; "  >
                                   <div style="overflow: auto;   height:250px; border: 1px silver solid"> 
                                       <table>
                                           <tr>
                                               <td class="auto-style1">                                                    
                                                   20190327 修正webmonitor設定錯誤，已可正確讀寫感應器資訊<br />                                                   
                                                   20190320 新增感應器種類: 轉速、警報碼(此功能需搭配webmonitor更新)<br />
                                                   20190318 修正設備不同時間點分析>>>最大最小平均，有最大最小，平均卻NAN 計算錯誤<br />
                                                   20190318 修正多曲線雙時間軌分析>>>最大最小平均，有最大最小，平均卻NAN 計算錯誤<br />
                                                   20190318 修正設備不同時間點分析>>>縱向可拖動紅棒，起終點時間的判定有問題,導致顯示的資訊有誤<br />
                                                   20190318 修正多曲線雙時間軌分析>>>縱向可拖動紅棒，起終點時間的判定有問題,導致顯示的資訊有誤<br />
                                                   20190317 修正設備不同時間點分析>>>低於Y座標=0的部分不顯示，別凸出去遮住底下的文字 <br />  
                                                   20190317 修正多曲線雙時間軌分析>>>低於Y座標=0的部分不顯示，別凸出去遮住底下的文字 <br />  
                                                   20190317 更新即時感應器顯示畫面狀態顏色 <br />  
                                                   20190315 更新即時感應器顯示畫面統計數字  首頁>>數據監控紀錄 <br />  
                                                   20190303 修正gateway名稱在webmonitor問題，之前此錯誤造成設定完成無法擷取資料<br />  
                                                   20190303 AdminSensor修正gateway名稱下拉選單問題<br />  
                                                   20190301 AdminSensor感應器種類縮進panel裡面，Gateway/TCP選項才會出現 <br />  
                                                   20190301 AdminSensor修正COM port顯示 <br />  
                                                   20190218 開放即時感應器資訊-->首頁>>數據監控紀錄 <br />  
                                                   20190214 開放設定感應器XY位置<br />  
                                                   20190201 確認issue6- 設備列表需要有其對應的課別篩選設定<br />  
                                                   20190201 確認issue11- 設備不同時間點分析->Y軸比率顯示問題<br />  
                                                   20190201 確認issue15- 感測器列表List 需要有調回功能(回首頁 & 回管理首頁)<br />  
                                                   20190201 確認issue17- "ModbusTester"使用方式 >> 之前有給新版，但這只是測試用途，該功能後來已整合入webmonitor內<br />  
                                                   20190201 確認issue19- "維保趨勢通知"多一個查詢鈕,設備位置欄位置會跑掉<br />                                                   
                                                   20190201 確認issue21- 多曲線雙時間軌分析、Y軸不見，問題已處理<br />                                                  
                                                   20190201 確認issue26-多曲線雙時間軌分析->資料顯示有誤? (擷取畫面並不是目前的新版畫面，也無法重現問題)<br />                                                   
                                                   20190201 確認issue29-"多曲線雙時間軌分析" & "設備不同時間點分析"  Y軸Zoo in功能顯示錯誤 >> 有在MAIL解釋這是因為Y軸縮放時拉的太近<br />   
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EX:某曲線的Y範圍在30~50，但卻把Y軸縮放到40~45顯示，曲線就會超過<br /> 
                                                   20181211 修正&nbsp;&nbsp;&nbsp;維保趨勢通知不正確儲存問題<br /> 
                                                   20181211 修正&nbsp;&nbsp;&nbsp;上下限警報通知不正確儲存問題<br /> 
                                                   20181113 無資料不顯示問題，已修正，但會造成當機，還在偵錯中...<br />  
                                                   20181113 修正畫面未滿板問題<br /> 
                                                   20181113 webMonitor新增測試modbus功能<br /> 
                                                   20181112 首頁上方額外新增登出連結<br /> 
                                                   20181027 開放相關頁面的權限功能(有權限的使用者才能進入，暫定帳號test，密碼123為最高權限)<br /> 
                                                   20181026 新增&nbsp;&nbsp;&nbsp; 首頁 >> 管理系統 >>權限管理<br /> 
                                                   20181024 修正&nbsp;&nbsp;&nbsp; 首頁 >> 管理系統 >>感應器管理>> gateway的感應器種類<br /> 
                                                   20181023 新增 "中繼網頁監控" 程式 (負責在維保網頁系統下監控數值和發進階警報)<br />
                                                   20181020 修正 &nbsp; 首頁 &gt;&gt; 上下限警報通知 (課別>>設備連動)<br />
                                                   20181020 修正 &nbsp; 首頁 &gt;&gt; 維保趨勢通知 (課別>>設備連動)<br />
                                                   20181020 修正 &nbsp; 首頁 &gt;&gt; 設備不同時間點分析 (課別>>設備連動)<br />
                                                   20181020 修正 &nbsp; 首頁 &gt;&gt; 多曲線雙時間軌分析 (課別>>設備連動)<br />
                                                   20181020 修正設備管理畫面;&nbsp; 首頁 &gt;&gt; 管理系統 &gt;&gt; 設備管理<br />
                                                   20181005 修正FFU 簡體/繁體錯誤<br />
                                                   20181002開放&nbsp;&nbsp; 首頁 &gt;&gt; 進階警報管理 &gt;&gt; 群組推播設定管理 &gt;&gt;群組推播可接收人員設定<br />
                                                   20181001開放&nbsp;&nbsp; 首頁 &gt;&gt; 進階警報管理 &gt;&gt; 群組推播設定管理<br />
                                                   20180930開放&nbsp;&nbsp; 首頁 &gt;&gt; 進階警報管理 &gt;&gt; 簡訊設定管理 &gt;&gt; 簡訊可接收人員設定<br />
                                                   20180929開放&nbsp;&nbsp; 首頁 &gt;&gt; 進階警報管理 &gt;&gt; 簡訊設定管理 <br />
                                                   20180928開放&nbsp;&nbsp; 首頁 &gt;&gt; 進階警報管理 &gt;&gt;Email設定管理 &gt;&gt; Email可接收人員設定<br />
                                                   20180927開放&nbsp;&nbsp; 首頁 &gt;&gt; 進階警報管理 &gt;&gt;Email設定管理<br />
                                                   20180926 IIS端收集modbus、gateway資訊監控程式，尚有問題處理中，即將開放
                                                   <br />           
                                                   20180925 開放感應器群組在02多曲線雙時間軌分析.aspx顯示功能
                                                   <br />                                       
                                                   此功能在 : 首頁 >> 管理系統 >>感應器群組設定          
                                                   <br />                                                 
                                                   20180924 開放感應器群組編輯功能
                                                   <br />     
                                                   20180922 修正 維保趨勢通知-小數下一位顯示
                                                   <br />       
                                                   20180922 修正 上下限警報通知-小數下一位顯示
                                                   <br />      
                                                   20180921 修正 設備不同時間點分析分析-小數下一位顯示
                                                   <br />          
                                                   20180921 修正 多曲線雙時間軌分析-小數下一位顯示
                                                   <br />          
                                                   20180920 修正感應器管理:加入 gateway管理畫面
                                                   <br />                 
                                                  20180920 修正感應器管理:IP設定錯誤(IP須從gateway管理畫面修改)
                                                   <br />           
                                                  20180919 多曲線時間軌分析: 某曲線移除不用
                                                   <br />        
                                                  20180918 多曲線時間軌分析:多曲線同時顯示
                                                   <br /> 
                                                   20180916 設備不同時間點分析:加入曲線縮放卷軸
                                                    <label onclick="openSelectWindow();">test</label>
                                                   <br />                                                            
                                                   20180915 多曲線時間軌分析:加入曲線縮放卷軸
                                                   <br />                                                   
                                                   20180914 維保趨勢通知 加入區間選擇
                                                   <br />
                                                   20180913 上下限警報通知加入區間選擇
                                                   <br />
                                                   20180911 簡體版FFU完成
                                                   <br />
                                                   20180910 右方列表，點某一個切換Y，但所有曲線都必須顯示
                                                   <br />
                                                   20180909 設備不同時間點分析--特徵線完成
                                                   <br />
                                                   20180907 上下擴張25%範圍
                                                   <br />
                                                   20180907 首頁 >> 管理系統 >>感應器管理  基本功能完成
                                                   <br />
                                                   20180906 首頁 >> 管理系統 >>感應器群組設定  基本功能完成
                                                   <br />
                                                   20180905 設備不同時間點分析--特徵線尚有問題修正中
                                                   <br />
                                                   20180904 開放設備不同時間點分析
                                                   <br />
                                                   20180831 修正特徵線資料顯示格還是太小、右方名稱裁切到問題
                                                   <br />
                                                   20180831 修正超過九筆錯誤問題<br />
                                                   20180829 上下限警報: 修正左方卷軸、Y座標、X座標、曲線錯誤、特徵線紀錄、特徵線位置<br />
                                                   20180828 維保趨勢通知:修正特徵線和Y軸座標搭配問題<br />
                                                   20180827 維保趨勢通知:修正左方卷軸、三條特徵線錯誤、X軸座標錯誤、Y軸座標<br />
                                                   20180825 多曲線時間軌分析修正:左方數量太多出現卷軸<br />
                                                   20180823 多曲線時間軌分析修正:交點區域太小問題<br />
                                                   右方點選不同感應器單獨顯示、右方點選第一個感應器上方顯示全部感應器<br />
                                                   20180823 多曲線時間軌分析修正:Y軸問題、右方點選不同感應器Y軸跟著更換<br />
                                                   20180612 完成上下限警報通知<br />
                                                   20180612 完成維保趨勢通知<br />
                                                   20180612 新增: 感應器列表 (在管理畫面中..)<br />
                                                   20180612 修改SQL轉換程式錯誤<br />
                                               </td>
                                           </tr>
                                       </table>
                                       </div>
                                     
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

