var lineA = 680, lineB = 1080; // 兩條特徵線
var colors = new Array("blue", "orange", "green", "red", "purple", "brown", "yellow", "gray", "gold", "black");
var isDragline = -1; // -1:沒有, 1:lineA, 2:lineB
var abc = 0;

function drawBG() { // 在隱藏的BG 繪製底下的表格
    var canBG = document.getElementById("myCanvasBG");
    var ctx = canBG.getContext("2d");
    var a;

    //Y軸刻度
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(90, 20);
    ctx.lineTo(90, 740);
    ctx.stroke();
    ctx.fillStyle = "black";
    ctx.font = "16Px 新細明體";
    var tt = [1, 2, 3, 4, 5, 20, 30, 40, 50, 60, 70, 80, 90];
    for (a = 0; a <= 12; a++) {// 0~120度 
        ctx.beginPath(); // 刻度
        ctx.moveTo(90, 740 - a * 60);
        ctx.lineTo(80, 740 - a * 60);
        ctx.stroke();

        if (a < 10)
            //ctx.fillText(a * 10, 52, 740 - a * 60+4 ); // 數字
            ctx.fillText(tt[a], 52, 740 - a * 60 + 4); // 數字
        else
            ctx.fillText(a * 10, 46, 740 - a * 60 + 4); // 數字
    }
    ctx.save();
    ctx.strokeStyle = 'gray';
    ctx.setLineDash([5, 5]);
    for (a = 1; a <= 12; a++) {// 0~120度的橫向虛線條
        ctx.beginPath(); // 刻度
        ctx.moveTo(90.5, 740.5 - a * 60);
        ctx.lineTo(1250.5, 740.5 - a * 60);
        ctx.stroke();
    }
    ctx.restore();

    //X軸刻度 
    ctx.strokeStyle = 'black';
    ctx.beginPath();
    ctx.moveTo(90, 740);
    ctx.lineTo(1250, 740);
    ctx.stroke();
    ctx.fillStyle = "black";
    ctx.font = "16Px 新細明體";
    for (a = 0; a < 10; a++) {// 分十大等分 
        ctx.beginPath(); // 刻度
        ctx.moveTo(100 + a * 125, 740);
        ctx.lineTo(100 + a * 125, 750);
        ctx.stroke();

        ctx.save();  //顯示日期刻度
        ctx.translate(95 + a * 125, 760);
        ctx.rotate(Math.PI / 180 * 45);
        ctx.fillText("2018/01/21 12:25:" + a * 5, 0, 0);
        ctx.restore();
    }

    // 中間的曲線
    //var items = [[1, 2], [3, 4], [5, 6], [1, 2], [3, 4], [5, 6], [1, 2], [3, 4], [5, 6], [5, 6]];
    var c1 = [70, 79, 50, 55, 60, 70, 80, 90, 85, 75];
    var c2 = [5, 5, 2, 5, 9, 12, 8, 7, 6, 9];
    var c3 = [55, 55, 90, 35, 40, 50, 30, 70, 95, 110];

    drawBGline(ctx, c1, 0);
    drawBGline(ctx, c2, 1);
    drawBGline(ctx, c3, 2);

    // 右邊的曲線分析資料
    ctx.font = "20Px 新細明體";
    for (a = 0; a < 10; a++) {
        ctx.fillStyle = colors[a];
        ctx.fillRect(1300, 50 + a * 70, 15, 15);

        ctx.fillStyle = "black";
        ctx.fillText("sensor" + (a + 1), 1325, 50 + a * 70);

        ctx.fillText("25.2 /89.3 /125.7 ", 1325, 75 + a * 70);
    }
}
function drawBGline(ctx, c, linecolor) {
    var a;
    ctx.save();
    ctx.strokeStyle = colors[linecolor];
    ctx.beginPath();
    ctx.moveTo(100, 740 - c[0] * 6);// 先畫出線
    for (a = 1; a < 10; a++) {
        ctx.lineTo(100 + a * 125, 740 - c[a] * 6);
    }

    ctx.stroke();
    ctx.fillStyle = colors[linecolor];
    for (a = 0; a < 10; a++) {// 绘制一个填充圆                
        ctx.beginPath();
        ctx.arc(100 + a * 125, 740 - c[a] * 6, 5, 0 * 0.0175, 360 * 0.0175, true);
        ctx.fill();
    }
    ctx.restore();
    //console.log("color:"+linecolor);
}

function drawFG(ctx, fg) { // 在FG 繪製特徵線和處理滑鼠事件
    // 繪製兩條特徵線
    ctx.strokeStyle = 'red';
    ctx.lineWidth = 3;
    ctx.beginPath(); // 第一條
    ctx.moveTo(lineA, 0);
    ctx.lineTo(lineA, 740);
    ctx.stroke();
    ctx.beginPath(); // 第二條
    ctx.moveTo(lineB, 0);
    ctx.lineTo(lineB, 740);
    ctx.stroke();

    // 繪製交點資訊
    if (isDragline == 1) {  //  1:lineA, 2:lineB 
        drawfocus(ctx, lineA);
    }
    else if (isDragline == 2) {  //  1:lineA, 2:lineB 
        drawfocus(ctx, lineB);
    }
}
function drawfocus(ctx, tx) // 繪製交點資訊,x要介於380~1500間
{
    tx += 20;
    if (tx > 1280) tx -= 230;
    ctx.save();
    ctx.font = "bold 20Px 新細明體";
    ctx.fillStyle = "#a8aaaa";   // 'lightgray';
    ctx.fillRect(tx, 50, 200, 30);
    ctx.fillStyle = "white";
    ctx.fillText("2018/01/21 12:25:30", tx + 15, 72);

    var n = 7; // 假設有三筆資料
    ctx.fillStyle = "#c8c8c8";
    ctx.fillRect(tx, 80, 200, n * 30);
    ctx.fillStyle = 'white';
    ctx.fillRect(tx + 1, 81, 198, n * 30 - 2);

    ctx.strokeStyle = "#c8c8c8";
    ctx.lineWidth = 1;
    ctx.beginPath(); // 先畫名稱和數值的分隔線
    ctx.moveTo(tx + 140, 80);
    ctx.lineTo(tx + 140, 80 + n * 30 - 2);
    ctx.stroke();
    for (var a = 0; a < n; a++) {
        ctx.beginPath(); // 畫這筆資料的底線
        ctx.moveTo(tx + 1, 80 + a * 30 + 28);
        ctx.lineTo(tx + 199, 80 + a * 30 + 28);
        ctx.stroke();

        ctx.fillStyle = colors[a]; // 資料顏色方塊
        ctx.fillRect(tx + 5, 80 + a * 30 + 10, 12, 12);

        ctx.fillStyle = "black";
        ctx.fillText("Sensor" + a, tx + 23, 80 + a * 30 + 22); //名稱
        ctx.fillText(Math.floor(Math.random() * 100 + 12), tx + 143, 80 + a * 30 + 22); //數值
    }

    ctx.restore();
}

function drawLeftList(ctx) { // 畫出左邊感應器列表
    ctx.fillStyle = "black";
    ctx.font = "20Px 新細明體";
    for (var a = 0; a < 30; a++) {
        ctx.fillText("1UL4_PLB_52F" + a, 10, a * 30 + 30);
    }
}

function drawclear() { // 清畫面
    var fg = document.getElementById("myCanvasFG");
    var ctxfg = fg.getContext("2d");
    ctxfg.clearRect(0, 0, fg.width, fg.height);
}

function draw() { // 主繪圖函數，整合一切
    abc += 1;

    if (isDragline == -1)
        drawBG(); // 1. 先畫好背景

    var fg = document.getElementById("myCanvasFG");
    var ctxfg = fg.getContext("2d");

    var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
    var imgData = ctxbg.getImageData(0, 0, 1520, 880)
    ctxfg.putImageData(imgData, 280, 0);

    ctxfg.beginPath();// 劃出中間分隔線
    ctxfg.moveTo(280, 0);
    ctxfg.lineTo(280, 880);
    ctxfg.strokeStyle = 'black';
    ctxfg.stroke();

    drawFG(ctxfg, fg);
    drawLeftList(ctxfg); // 畫出左邊感應器列表

    ctxfg.fillText("abc:" + abc, 400, 100); //名稱
    //document.getElementById("Button1").value = abc;
}


function getparam() {
    // Step1 取得網址 
    console.log(location.href);
    var getUrlString = location.href;//取得網址，並存入變數
    //Step2 將網址 (字串轉成URL)
    var url = new URL(getUrlString);
    // Step3 使用URL.searchParams + get 函式  (括弧裡面帶入欲取得結果的KEY鍵值參數) 
    var id = url.searchParams.get('id');
    var a = url.searchParams.get('a');
    var b = url.searchParams.get('b');

    if (id == null)
        console.log("NULL");
    else
        console.log(id);
    console.log(url.searchParams.get('a'));
    console.log(url.searchParams.get('b'));


    var c = url.searchParams.get('c');
    var d = url.searchParams.get('d');
    if (c != null && d != null) {
        document.getElementById("test1").innerText = d;
        console.log(d);
    }
}