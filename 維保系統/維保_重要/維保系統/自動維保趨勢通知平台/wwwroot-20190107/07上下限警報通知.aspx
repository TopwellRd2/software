﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="07上下限警報通知.aspx.vb" Inherits="SQLWeb._04上下限警報通知" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>奇立實業</title>
    <script src='http://code.jquery.com/jquery-1.8.2.js'></script>    
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>
    <script> 
        var isDragScroll = -1;
        var DragScroll_start;
        var scroll_len = 0;
        var scroll_pos = 0;
        var scroll_dis = 0;

        var lineA = 120, lineB = 110, lineC = 100, lineD = 60; // 4條特徵線
        var lineHH, lineH, lineLL, lineL;
        var colors = new Array("blue", "orange", "green", "red", "purple", "brown", "yellow", "gray", "gold", "black");
        var isDragline = -1; // -1:沒有, 4:HH, 3:H, 2:LL ,1:L

        var selectStartdateint, selectEnddateint;// 選擇到的時間起點和終點
        var timeinterval = new Array();
        var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
        var selectedID=-1;
        var DragSensorx, DragSensory; // 被拖拉的感應器位置
        var DragSensor = -1; // 被拉進來的感應器 
        var sensorName = "";
        var isDisplaySensor = new Array(); // 是否要顯示這個感應器
        var sensorMax = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensormin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensorAvg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensor0 = new Array(); // 600筆資料 
        var yaxis0 = new Array(); // 720筆資料  

        var sensorList = new Array();// 左邊的感應器列表

        function drawBG() { // 在隱藏的BG 繪製底下的表格
            var canBG = document.getElementById("myCanvasBG");
            var ctx = canBG.getContext("2d");
            var a;

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(0, 0, 1580, 880);

            //Y軸刻度
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(90, 20);
            ctx.lineTo(90, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";

            for (a = 0; a <= 12; a++) {// 0~140度
                ctx.beginPath(); // 刻度
                ctx.moveTo(90, 740 - a * 60);
                ctx.lineTo(80, 740 - a * 60);
                ctx.stroke();
                // ctx.fillText(a * 10, 46, 740 - a * 50 + 4); // 數字
            }
            ctx.save();
            ctx.strokeStyle = 'gray';// 0~140度的橫向虛線條
            ctx.setLineDash([5, 5]);
            for (a = 1; a <= 12; a++) {
                ctx.beginPath(); // 刻度
                ctx.moveTo(90.5, 740.5 - a * 60);
                ctx.lineTo(1250.5, 740.5 - a * 60);
                ctx.stroke();
            }
            ctx.restore();

            ////X軸刻度
            //ctx.strokeStyle = 'black';
            //ctx.beginPath();
            //ctx.moveTo(90, 740);
            //ctx.lineTo(1250, 740);
            //ctx.stroke();
            //ctx.fillStyle = "black";
            //ctx.font = "16Px 新細明體";
            //for (a = 0; a <8; a++) {// 分8大等分
            //    ctx.beginPath(); // 刻度
            //    ctx.moveTo(240+a*142, 740);
            //    ctx.lineTo(240+a*142, 750);
            //    ctx.stroke();

            //    ctx.save();  //顯示日期刻度
            //    ctx.translate(235 + a * 142, 766);
            //    //ctx.rotate(Math.PI / 180 * 45);
            //    if (a<7)
            //        ctx.fillText(a * 3 + 3, 0, 0);
            //    else
            //        ctx.fillText(a * 3 + 3 + " hr", 0, 0);
            //    ctx.restore();
            //}

            //X軸刻度 
            ctx.strokeStyle = 'black';
            ctx.beginPath();
            ctx.moveTo(90, 740);
            ctx.lineTo(1250, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var interval = (selectEnddateint - selectStartdateint) / 9;
            for (a = 0; a < 10; a++) {// 分十大等分 
                ctx.beginPath(); // 刻度
                ctx.moveTo(100 + a * 122, 740);
                ctx.lineTo(100 + a * 122, 750);
                ctx.stroke();

                ctx.save();  //顯示日期刻度
                ctx.translate(95 + a * 122, 760);
                ctx.rotate(Math.PI / 180 * 45);
                if (timeinterval.length == 10)
                    ctx.fillText(timeinterval[a], 0, 0);
                ctx.restore();
            }

            // 中間的曲線 
            drawSensorCurvline(ctx);// 中間sensor曲線 

            //右方文字
            ctx.save();   
            ctx.fillText("目前查詢:", 1370, 100);
            ctx.fillText(sensorName, 1370, 130);
            ctx.restore();
        }

        function drawSensorCurvline(ctx) {
            var a; 
            //console.log("drawSensorCurvline:" + DragSensor);
            if (DragSensor == -1) return;
            ctx.save(); 
            ctx.strokeStyle = colors[0];
            ctx.beginPath();
            ctx.moveTo(100, 740 - sensor0[0] );// 先畫出線
            console.log("sensor0[0]:" + sensor0[0]);
            for (a = 0; a < 550; a++) {
                ctx.lineTo(100 + a * 2, 740 - sensor0[a] );
            }
              
            ctx.stroke();
            ctx.restore();

            //drawDeadban(ctx, c);
        }

        function drawBGline(ctx, c, linecolor) { // 中間的曲線
            var a;
            ctx.save();
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - c[0] * 5);// 先畫出線
            for (a = 1; a < c.length ; a++) {
                ctx.lineTo(100 + a * 19, 740 - c[a] * 5);
            }
            ctx.stroke();
            /*
            ctx.fillStyle = colors[linecolor];
            for (a = 0; a < c.length ; a++) {// 绘制一个填充圆
                ctx.beginPath();
                ctx.arc(100 + a * 41, 740 - c[a] * 6, 5, 0 * 0.0175, 360 * 0.0175, true);
                ctx.fill();
            }*/
            ctx.restore();

            drawDeadban(ctx, c);
        }

        function drawDeadban(ctx, c) {//繪製干擾的deadban框
            var a;
            ctx.save();
            ctx.strokeStyle = 'gray';
            ctx.beginPath();
            for (a = 0; a < c.length ; a++) {
                if (c[a] >= 110) {
                    //ctx.lineTo(100 + a * 20, 740 - c[a] * 6);
                    ctx.rect(100 + a * 19 - 7, 740 - c[a] * 5 - 10, 15, 60);
                }
            }
            ctx.stroke();
            ctx.restore();
        }

        function drawLeftList(ctx) { // 畫出左邊感應器列表
            ctx.fillStyle = "black";
            ctx.font = "20Px 新細明體";

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1, 1, 278, 879); //clear background

            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(260, 1, 18, 879); //scroll area

            ctx.fillStyle = "#cccccc";
            ctx.fillRect(261, scroll_pos + 1, 16, scroll_len); //scroll bar

            //var temp; //每筆30，最多29筆 
            //for (var a = 0; a < sensorList.length; a++) {
            //    //ctx.fillStyle = "#ffcccc";
            //    //ctx.fillRect(10, a * 30 + 13, 266, 21); 
            //    ctx.fillStyle = "black";
            //    temp = sensorList[a];
            //    temp = temp.substr(temp.indexOf(":") + 1);
            //    //ctx.fillText(sensorList[a], 10, a * 30 + 30); 
            //    ctx.fillText(temp, 10, a * 30 + 30);
            //    //console.log(sensorList[a]);
            //}

            var temp; //每筆30，最多29筆 
            var t = parseInt(scroll_pos / parseInt(877 / sensorList.length));
            ctx.fillStyle = "black";
            for (var a = 0; a < 29; a++) {
                if ((a + t) >= sensorList.length) break;
                temp = sensorList[a + t];
                temp = temp.substr(temp.indexOf(":") + 1);
                ctx.fillText(temp, 10, a * 30 + 30);
            }
        }

        function drawFG(ctx, fg) { // 在FG 繪製特徵線和處理滑鼠事件
            // 繪製3條特徵線，往右偏移280
            ctx.font = "16Px 新細明體";
            ctx.lineWidth = 3;
            ctx.strokeStyle = 'green';
            ctx.fillStyle = 'green';
            ctx.beginPath(); // 第1條 
            ctx.moveTo(280 + 95, 740 - lineA);
            ctx.lineTo(280 + 1250, 740 - lineA);
            ctx.stroke();
            ctx.fillText("LL軸", 280 + 1285, 740 - lineA + 3);
             
            ctx.strokeStyle = 'orange';
            ctx.fillStyle = 'orange';
            ctx.beginPath(); // 第2條 
            ctx.moveTo(280 + 95, 740 - lineB);
            ctx.lineTo(280 + 1250, 740 - lineB);
            ctx.stroke();
            ctx.fillText("L軸", 280 + 1285, 740 - lineB + 3);

            ctx.strokeStyle = 'red';
            ctx.fillStyle = 'red';
            ctx.beginPath(); // 第3條 
            ctx.moveTo(280 + 95, 740 - lineC);
            ctx.lineTo(280 + 1250, 740 - lineC);
            ctx.stroke();
            ctx.fillText("H軸", 280 + 1285, 740 - lineC + 3);

            ctx.strokeStyle = 'purple';
            ctx.fillStyle = 'purple';
            ctx.beginPath(); // 第4條 
            ctx.moveTo(280 + 95, 740 - lineD);
            ctx.lineTo(280 + 1250, 740 - lineD);
            ctx.stroke();
            ctx.fillText("HH軸", 280 + 1285, 740 - lineD + 3);
            //  ctx.lineTo(80, 740 - a * 60);

            // 繪製警報點方框
            if (isDragline == 1) {  //  1:lineA, 2:lineB
                drawAlarmRec(ctx, lineA);
            }
            else if (isDragline == 2) {  //  1:lineA, 2:lineB
                drawAlarmRec(ctx, lineB);
            }
        }

        function drawAlarmRec(ctx, tx) // 繪製警報點方框,x要介於90~1250
        {

        }

        function drawclear() { // 清畫面
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function draw() { // 主繪圖函數，整合一切 
            if (isDragline == -1)
                drawBG(); // 1. 先畫好背景

            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");

            var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            var imgData = ctxbg.getImageData(0, 0, 1580, 880)
            ctxfg.putImageData(imgData, 280, 0);

            ctxfg.beginPath();// 劃出中間分隔線
            ctxfg.moveTo(280, 0);
            ctxfg.lineTo(280, 880);
            ctxfg.strokeStyle = 'black';
            ctxfg.stroke();

            drawFG(ctxfg, fg);
            drawLeftList(ctxfg); // 畫出左邊感應器列表

            drawYcurve(ctxfg);

            if (isDragSensor > -1)// 左邊拖拉的感應器
            {
                var a = parseInt(scroll_pos / parseInt(877 / sensorList.length));
                if ((a + isDragSensor) < sensorList.length) {
                    ctxfg.fillStyle = "#ffcccc";
                    ctxfg.fillRect(DragSensorx, DragSensory, 266, 21);
                    ctxfg.fillStyle = "black";
                    ctxfg.fillText(sensorList[isDragSensor + a], DragSensorx, DragSensory + 17);
                }
            }
        }

        function drawYcurve(ctx) {
            //Y軸刻度
            // console.log("drawYcurve")
            ctx.fillStyle = "#ddffff";
            ctx.fillRect(281, 1, 89, 740);

            //ctx.strokeStyle = colors[nowCurve];
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.fillStyle = colors[0];//"black";
            ctx.font = "16Px 新細明體";
            var a,v;
            for (a = 0; a <= 12; a++) {// 0~120度  
                v=yaxis0[a * 60];
                ctx.fillText(v.toFixed(1), 300, 740 - a * 60 + 4); // 數字                
            }
        }

        function GetsensorList() {
            var actionUrl;
            var f = document.getElementById("DDarea").value;
            var g = document.getElementById("DDclass").value;
            var h = document.getElementById("DDdevice").value;

            sensor0 = new Array();
            isDragSensor = -1;
            isDragline = -1;

            actionUrl = "wf.aspx?c=A02&f=" + f + "&g=" + g + "&h=" + h;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData(actionUrl); // 抓取json 
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料";
            var a;
            // console.log(details);

            if (sensorList.length < 29)
                scroll_len = 877; //每筆30，最多29筆
            else {
                scroll_len = parseInt((29 * 877) / (sensorList.length - 29));
            }
            scroll_pos = 0;
            console.log("scroll_len=" + scroll_len);

            draw();
            // $('#test1').html(details); // 找到test1，並且把資訊顯示上去
        }

        function GetData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.ID + ":" + item.name;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }


        function CanvasFG_mouosedown(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            // document.getElementById("test1").innerText = ",now>>down";

            // document.getElementById("test1").innerText = ",now>>down>>A";
            //280 + 95~280 + 1250
            // isDragline = -1; // -1:沒有, 1:lineA, 2:lineB , 3:lineC
            if (y > (740 - lineA - 3) && y < (740 - lineA + 3) && x > 375) {
                isDragline = 1;
                console.log("lineA isDragline");
            }
            else if (y > (740 - lineB - 3) && y < (740 - lineB + 3) && x > 375) {
                isDragline = 2;
                console.log("lineA isDragline");
            }
            else if (y > (740 - lineC - 3) && y < (740 - lineC + 3) && x > 375) {
                isDragline = 3;
                console.log("lineC isDragline");
            }
            else if (y > (740 - lineD - 3) && y < (740 - lineD + 3) && x > 375) {
                isDragline = 4;
                console.log("lineD isDragline");
            }

            if (x > 10 && x < 260) {
                var n = parseInt((y - 13) / 30);
                var ty = n * 30 + 13;
                //console.log(ty+":"+y);
                if (y > ty && y < ty + 21)
                    // document.getElementById("test1").innerText = ",now>>down>>sensor" +n;
                    isDragSensor = n;
                else
                    isDragSensor = -1;
            }

            if (x > 260 && x < 276) { // isDragScroll
                if (y > scroll_pos && y < (scroll_pos + scroll_len - 1)) {
                    isDragScroll = 1;
                    DragScroll_start = y;
                }
            }
        }

        function CanvasFG_mouosemove(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            // document.getElementById("test").innerText = "Coordinates: (" + x + "," + y + ")";
            // document.getElementById("test1").innerText = ",now>>move";

            //lineA = 150, lineB = 120, lineC = 100, lineD = 60; // 4條特徵線
            // var isDragline = -1; // -1:沒有, 1:lineA LL, 2:lineB L, 3:lineC H  ,4:lineD  H 
            if (isDragline == 1) { 
                if (x > 380 && y < 740 && y > (740 - lineB + 2)) { // 曲線範圍內才可以拖拉
                    lineA =  740 -y;
                    draw();
                    //console.log(y);
                } 
            }
            else if (isDragline == 2) { 
                if (x > 380 && y < (740 - lineA - 2) && y > (740 - lineC + 2)) {// 曲線範圍內才可以拖拉
                    lineB = 740 - y;
                    draw();
                    //console.log("lineB" + y);
                }
            }
            else if (isDragline == 3) { 
                if (x > 380 && y < (740 - lineB - 2) && y > (740 - lineD + 2)) {// 曲線範圍內才可以拖拉
                    lineC = 740 - y;
                    draw();
                    // console.log("lineC" + y);
                }
            }
            else if (isDragline == 4) { 
                if (x > 380 && y < (740 - lineC - 2)) {// 曲線範圍內才可以拖拉
                    lineD =740 - y;
                    draw();
                    //console.log(y);
                }
            }

            if (isDragSensor > -1) {
                DragSensorx = x; DragSensory = y;
                draw();
                // console.log(x + ";" + y);
            }

            if (isDragScroll == 1) {
                var a;
                var t = parseInt(y - DragScroll_start);
                if (t < 0 && Math.abs(t) > scroll_pos)
                    scroll_pos = 0;
                else if (t > 0 && (scroll_pos + scroll_len + t) > 877)
                    scroll_pos = 877 - scroll_len;
                else
                    scroll_pos += t;
                DragScroll_start = y;
                console.log("scroll_pos=" + scroll_pos + " , t=" + t);
                draw();
            }
        }

        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            // document.getElementById("test1").innerText = ",now>>up";

            if (isDragline > -1) {
                isDragline = -1;
                SetSensorline();
            }

            if (isDragScroll == 1) {
                isDragScroll = -1;
            }

            if (isDragSensor > -1) { // 有選到sensor
                if (x < 280)
                    isDragSensor = -1;
                else if (x > 282) {
                    DragSensor = isDragSensor;
                    isDragSensor = -1;
                    var t = parseInt(scroll_pos / parseInt(877 / sensorList.length));
                    t += DragSensor;
                    GenSensorGrid(t);

                    //document.getElementById("test1").innerText = ",isDragSensor";
                    //console.log(isDragSensor); 
                    //for ( a = 0; a < DragSensorList.length; a++)
                    //    console.log("DragSensorList"+a+":"+DragSensorList[a]); 
                    draw();
                }
            }
        }

        function get4digiInt(n) {
            var s = String(n);
            if (s.length == 1)
                s = "000" + s;
            else if (s.length == 2)
                s = "00" + s;
            else if (s.length == 3)
                s = "0" + s;            
            return s;
        }        

        function get6digifloat(n) {
            var t = n.toFixed(1); // 小數一位
            var s = String(t);
            if (s.length == 3)
                s = "000" + s;
            else if (s.length == 4)
                s = "00" + s;
            else if (s.length == 5)
                s = "0" + s;
            return s;
        }

        function SetSensorline() {
            var d;   
            lineLL = yaxis0[lineA]; //parseInt(yaxis0[lineA]); //parseInt((740 - lineA) / 5);
            lineL = yaxis0[lineB]; //parseInt(yaxis0[lineB]); // parseInt((740 - lineB) / 5);
            lineH = yaxis0[lineC];//parseInt(yaxis0[lineC]); // parseInt((740 - lineC) / 5);
            lineHH = yaxis0[lineD];//parseInt(yaxis0[lineD]); // parseInt((740 - lineC) / 5);

           // d = get4digiInt(lineHH) + get4digiInt(lineH) + get4digiInt(lineL) + get4digiInt(lineLL);
            d = get6digifloat(lineHH) + get6digifloat(lineH) + get6digifloat(lineL) + get6digifloat(lineLL);
            //console.log("lineHH=" + lineHH);
            //console.log("lineH=" + lineH);
            //console.log("lineL=" + lineL);
            //console.log("lineLL=" + lineLL);
            //console.log("d=" + d);
            var actionUrl; // 每一個sensor 獨立重新讀取       
            actionUrl = "wf.aspx?c=A06&d=" + d + "&g=" + selectedID; 
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>寫入中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            var start_date = new Date(); // 取得一開始的時間
            var temp = SetSensorData(actionUrl); // 抓取json  
            console.log("temp=" + temp);
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensor0.length + "筆資料";
            console.log(details);
            // draw();
        }

        function SetSensorData(actionUrl) { 
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            return temp;
                        });
                    }
                }
            });
            return "";
        }

        function getStartDateInt() {
            // selectStartdateint, selectEnddateint
            var nowdate = new Date(); // 月份由0開始 
            //console.log(nowdate);
            var ticks = ((nowdate.getTime() * 10000) + 621355968000000000);
            return ticks;//10000 are the ticks per millisecond.
        }

        function getEndDateInt(d) {
            // selectStartdateint, selectEnddateint
            var nowdate = new Date(); // 月份由0開始 
            if (d == 1)
                nowdate.setHours(nowdate.getHours() - 24);
            else if (d == 2)
                nowdate.setHours(nowdate.getHours() - 168);
            else if (d == 3)
                nowdate.setHours(nowdate.getHours() - 720);

         //   console.log(nowdate);
            var ticks = ((nowdate.getTime() * 10000) + 621355968000000000);
            return ticks;//10000 are the ticks per millisecond.
        }

        function intTodate(n) {
            //var epochMicrotimeDiff = 621355968000000000; 
            var ticksToMicrotime = n - 621355968000000000;
            var tickDate = new Date(ticksToMicrotime / 10000);
            return tickDate;
        }

        function dateformat(d) { // 傳入date ，傳回2018/04/03 5:35 
            var M = d.getMonth() + 1;
            M = (M < 10) ? "0" + M : M;
            var D = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
            var H = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var m = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            var s = (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();
            var temp = d.getFullYear() + "/" + M + "/" + D + " " + H + ":" + m + ":" + s;
         //   console.log("dateformat" + temp);
            return temp;
        }

        function dateToint(y, M, d, h, m) {
            var nowdate = new Date(y, M - 1, d, h, m); // 月份由0開始
            console.log(y + "," + M + "," + d + "," + h + "," + m);
            console.log(nowdate);
            var ticks = ((nowdate.getTime() * 10000) + 621355968000000000);
            return ticks;//10000 are the ticks per millisecond.
        }

        function GenSensorGrid(n) {
            var d = 1;
            var radios = document.getElementsByName('myCheck');
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    if (i == 0)
                        d = 1; // console.log("today");
                    else if (i == 1)
                        d = 2; //console.log("week");
                    else if (i == 2)
                        d = 3;//console.log("month");
                    // draw();
                    if (i == 3)
                        d = 4;// console.log("range");
                    break;
                }
            }
            
            if (d == 4) {
                var a1 = document.getElementById("DDyear1").value;
                var a2 = document.getElementById("DDmonth1").value;
                var a3 = document.getElementById("DDday1").value;
                var a4 = document.getElementById("DDhour1").value;
                var a5 = document.getElementById("DDminute1").value;
                var b1 = document.getElementById("DDyear2").value;
                var b2 = document.getElementById("DDmonth2").value;
                var b3 = document.getElementById("DDday2").value;
                var b4 = document.getElementById("DDhour2").value;
                var b5 = document.getElementById("DDminute2").value;
                selectStartdate = a1 + a2 + a3 + a4 + a5;
                selectEnddate = b1 + b2 + b3 + b4 + b5;
                selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
                selectEnddateint = dateToint(b1, b2, b3, b4, b5);
            }
            else {
                selectStartdateint = getStartDateInt();
                selectEnddateint = getEndDateInt(d);
                //console.log(selectStartdateint);
                //console.log(selectEnddateint);
            }
            var s = selectStartdateint;
            var e = selectEnddateint;
            var interval = (e - s) / 9;
            var a;

            timeinterval = new Array();// 月份由0開始
         //   console.log("1:" + intTodate(s));
            timeinterval.push(dateformat(intTodate(s)));
            for (a = 1; a < 9; a++) {
                console.log((a + 1) + ":" + intTodate(s + a * interval));
                timeinterval.push(dateformat(intTodate(s + a * interval)));
            }
           // console.log("9:" + intTodate(e));
            timeinterval.push(dateformat(intTodate(e)));

            var actionUrl; // 每一個sensor 獨立重新讀取
            var actionUrl1; // 每一個sensor 獨立重新讀取
           // console.log("ID");
            var id = (String)(sensorList[n]);
            var pos = id.indexOf(":");
            id = id.substr(0, pos);
            selectedID = id; 
            console.log(id);
            sensorName = (String)(sensorList[n]);
            sensorName = sensorName.substr( pos+1);

            if (id.length > 0) {
                if (d == 4)
                    actionUrl = "wf.aspx?c=A01&d=" + selectStartdate + "&f=" + selectEnddate + "&g=" + id;
                else
                    actionUrl = "wf.aspx?c=A04&d=" + d + "&g=" + id;
                actionUrl1 = "wf.aspx?c=A05&g=" + id;
            }
            else {
                console.log("no sensor select...");
                return;
            }
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            var start_date = new Date(); // 取得一開始的時間
            sensor0 = GetsensorData(actionUrl); // 抓取json  
            calmax_min_avg(sensor0);

            GetsensorHHLL(actionUrl1);
            //var lineA = 120, lineB = 110, lineC = 100, lineD = 60; // 4條特徵線
            //var lineHH, lineH, lineLL, lineL;
            if (parseFloat(lineLL) >= parseFloat(lineL))
                lineL = parseFloat(lineLL) + 5;
            if (parseFloat(lineL) >= parseFloat(lineH))
                lineH = parseFloat(lineL) + 5;
            if (parseFloat(lineH) >= parseFloat(lineHH))
                lineHH = parseFloat(lineH) + 5;
            console.log(lineLL + "," + lineL + "," + lineH + "," + lineHH);

            lineA = findpos(lineLL);
            lineB = findpos(lineL);
            lineC = findpos(lineH);
            lineD = findpos(lineHH);
            console.log(lineA + "," + lineB + "," + lineC + "," + lineD);

            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensor0.length + "筆資料";
            console.log(details);  
        }
         
        function findpos(val) {
            var a, m = -1;
            for (a = 0; a <= 720; a++) { //yaxis0 
                if (val >= yaxis0[a]) m = a;
            }
            if (m < 0) m = 0;
            return m;
        }

        function GetsensorData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(parseFloat(temp));
                        });
                    }
                }
            });
            return result;
        }
         
        function GetsensorHHLL(actionUrl) {
           // var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            //temp = item.ID + ":" + item.name;// 大小寫必須一樣
                            if (item.name == "HH") { lineHH =parseFloat( item.value); console.log("lineHH:" + lineHH); }
                            else if (item.name == "H") { lineH = parseFloat(item.value); console.log("lineH:" + lineH); }
                            else if (item.name == "L") { lineL =parseFloat( item.value); console.log("lineL:" + lineL); }
                            else if (item.name == "LL") { lineLL =parseFloat( item.value); console.log("lineLL:" + lineLL); }
                            console.log(item.name + ":" + item.value);
                            //lineHH, lineH, lineLL, lineL;
                            //result.push(temp);
                        });
                    }
                }
            });
           // return result;
        }

        function checkOption() { //myCheck1
            var radios = document.getElementsByName('myCheck');
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    // alert(radios[i].value);// today,week,month
                    if (i == 0)
                        console.log("today");
                    else if (i == 1)
                        console.log("week");
                    else if (i == 2)
                        console.log("month"); 
                    else if (i == 3)
                        console.log("range"); 
                    break;
                }
            } 
        }

        function calmax_min_avg(sen) {
            var max = sen[0], min = sen[0], avg = 0, c = 0;
            var a;
            for (a = 1; a < sen.length; a++) {
                if (sen[a] > max)
                    max = sen[a];
                else if (sen[a] < min)
                    min = sen[a];
                avg += parseFloat(sen[a]);
            }
            sensorMax[0] = max;
            sensormin[0] = min;
            sensorAvg[0] = avg / sen.length;
            //console.log("calmax_min_avg");
            //console.log(sensorMax[0]);
            //console.log(sensormin[0]);
            //console.log(sensorAvg[0]);

            cal_Yaxis(max, min);
            //console.log("yaxis0=" + yaxis0.length);
            //console.log(sen.length);
            //console.log(avg);
            //console.log(max);
            //console.log(min);
            //console.log(sensorAvg[n]);
        } 

        function cal_Yaxis(max, min) {
            yaxis0 = []; 
            //console.log("cal_Yaxis");
            var d, new_max, new_min, step, a;
            d = max - min;
            //console.log("max=" + max);
            //console.log("min=" + min);
            //console.log("d=" + d);

            new_max = parseFloat(max) + parseFloat(d / 4);
            new_min = parseFloat(min) - parseFloat(d / 4);
             
            //console.log("new_max=" + new_max);
            //console.log("new_min=" + new_min);

            d = new_max - new_min;
            step = d / 720.0;
            //console.log("step=" + step);

            for (a = 0; a <= 720; a++) {
                yaxis0.push(parseFloat(new_min + a * step));
            }
            // console.log("yaxis0=" + sax.length);
            for (a = 0; a < sensor0.length; a++) {
                sensor0[a] = parseFloat((sensor0[a] - new_min) / step);
            }
        }

        function MyDropDownYearFunction() {
            console.log(document.getElementById("DDyear1").value);
            console.log($("#DDyear1").val());
            document.getElementById("DDyear2").value = document.getElementById("DDyear1").value;
        }
        function MyDropDownMonthFunction() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDmonth1").value);
            document.getElementById("DDmonth2").value = document.getElementById("DDmonth1").value;
        }
        function MyDropDownDayFunction() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDday1").value);
            document.getElementById("DDday2").value = document.getElementById("DDday1").value;
        }
        function MyDropDownHourFunction() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDhour1").value);
            document.getElementById("DDhour2").value = document.getElementById("DDhour1").value;
        }
        function MyDropDownMinuteFunction() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDminute1").value);
            document.getElementById("DDminute2").value = document.getElementById("DDminute1").value;
        }

        window.onload = function (e) {
            draw();
        }
    </script>
    <style>
        #myCanvasFG {
            position: static;
            border: 1px solid black;
        }

        #myCanvasBG {
            position: static;
            border: 1px solid black;
        }
    </style>
</head>
<body style= "background-color:#ddffff">
    <form id="form1" runat="server">
    <div id="divUP" style="width:1890px;height:80px;border:1px solid #c3c3c3"> 
        <div style="font-size:20px">
            
            廠區 
            <asp:DropDownList ID="DDarea" runat="server" Width="160px" Font-Size="Large">
                <asp:ListItem Value="1">一廠</asp:ListItem>
                <asp:ListItem Value="2">二廠</asp:ListItem>
                <asp:ListItem Value="3">三廠</asp:ListItem>
                <asp:ListItem Value="4">四廠</asp:ListItem>
                <asp:ListItem Value="5">五廠</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            課別 
            <asp:DropDownList ID="DDclass" runat="server" Width="120px" Font-Size="Large" AutoPostBack="True">
                <asp:ListItem Value="1">水務</asp:ListItem>
                <asp:ListItem Value="2">氣化</asp:ListItem>
                <asp:ListItem Value="3">空調</asp:ListItem>
                <asp:ListItem Value="4">電力</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp; &nbsp;&nbsp;
            設備 
            <asp:DropDownList ID="DDdevice" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">純水設備</asp:ListItem>
                <asp:ListItem Value="2">淨化設備</asp:ListItem>
                <asp:ListItem Value="3">冷卻設備</asp:ListItem>
                <asp:ListItem Value="4">過濾設備</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            <img id="btnGetsensorList" onclick="GetsensorList();" src="images/buttom.jpg" />
             
            &nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">回首頁</asp:HyperLink>
            <br />

            日期
            <input type="radio" name="myCheck" id ="today"  checked="checked" onclick="checkOption()" value="today"/>24H &nbsp;&nbsp;&nbsp; 
            <input type="radio" name="myCheck" id ="week" onclick="checkOption()" value="week"/>過去一週 &nbsp;&nbsp;&nbsp; 
            <input type="radio" name="myCheck" id ="month" onclick="checkOption()" value="month"/>過去一月 &nbsp;&nbsp;&nbsp; 
       <input type="radio" name="myCheck" id ="range" onclick="checkOption()" value="range"/>區間
             <asp:DropDownList ID="DDyear1" runat="server" onchange="javascript:MyDropDownYearFunction();"  Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth1" runat="server" onchange="javascript:MyDropDownMonthFunction();" Width="70px" Font-Size="Large" >
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday1" runat="server" onchange="javascript:MyDropDownDayFunction();" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour1" runat="server" onchange="javascript:MyDropDownHourFunction();"  Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute1" runat="server"  onchange="javascript:MyDropDownMinuteFunction();"  Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp;～ 
            <asp:DropDownList ID="DDyear2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday2" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute2" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分
       <br />
            <label id="test"></label>
            <label id="test1"></label> 
        </div>

        <div style="visibility:hidden;">
            <canvas id="myCanvasBG" width="1580" height="880"></canvas>
        </div>
    </div>
    <canvas id="myCanvasFG" width="1890" height="880" onmousemove="CanvasFG_mouosemove(event)" onmousedown="CanvasFG_mouosedown(event)" onmouseup="CanvasFG_mouoseup(event)"></canvas>

    </form>
</body>
</html>
