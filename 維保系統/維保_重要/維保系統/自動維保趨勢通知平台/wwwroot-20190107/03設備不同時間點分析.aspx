﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="03設備不同時間點分析.aspx.vb" Inherits="SQLWeb._03設備不同時間點分析" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業
    </title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        var isDragScroll = -1;
        var DragScroll_start;
        var scroll_len = 0;
        var scroll_pos = 0;
        var scroll_dis = 0;

        var lineA = 560 ; // 兩條特徵線
        var colors = new Array("blue", "red", "orange", "green", "purple", "brown", "yellow", "gray", "gold", "black");
        var isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 

        var md = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        var selectStartdate, selectEnddate;// 選擇到的時間起點和終點
        var selectStartdateint, selectEnddateint;// 選擇到的時間起點和終點
        var timeinterval = new Array();
        var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
        var DragSensorx, DragSensory; // 被拖拉的感應器位置
        var DragSensorList = new Array(); // 被拉進來的感應器列表，最多8個
        var isDisplaySensor = new Array(); // 是否要顯示這個感應器
        var sensorMax = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensormin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensorAvg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensor0 = new Array(); // 600筆資料
        var sensor1 = new Array(); 
        var yaxis0 = new Array(); // 720筆資料 
        var nowCurve = -1;
        var sensorName = "";

        var isScrollRange = -1;
        var scrollmax = new Array(719, 719, 719, 719, 719, 719, 719, 719, 719);
        var scrollmin = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        1
        var sensorList = new Array();// 左邊的感應器列表

        function drawBG() { // 在隱藏的BG 繪製底下的表格
            var canBG = document.getElementById("myCanvasBG");
            var ctx = canBG.getContext("2d");
            var a;

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(0, 0, 1560, 850);

            ////Y軸刻度
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(105, 20);
            ctx.lineTo(105, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體"; 
            for (a = 0; a <= 12; a++) {// 0~120度 
                ctx.beginPath(); // 刻度
                ctx.moveTo(105, 740 - a * 60);
                ctx.lineTo(100, 740 - a * 60);
                ctx.stroke(); 
            }
            ctx.save();
            ctx.strokeStyle = 'gray';
            ctx.setLineDash([5, 5]);
            for (a = 1; a <= 12; a++) {// 0~120度的橫向虛線條
                ctx.beginPath(); // 刻度
                ctx.moveTo(110, 740.5 - a * 60);
                ctx.lineTo(1260.5, 740.5 - a * 60);
                ctx.stroke();
            }
            ctx.restore();

            //X軸刻度 
            ctx.strokeStyle = 'black';
            ctx.beginPath();
            ctx.moveTo(110, 740);
            ctx.lineTo(1260, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            for (a = 0; a < 10; a++) {// 分十大等分 
                ctx.beginPath(); // 刻度
                ctx.moveTo(120 + a * 116, 740);
                ctx.lineTo(120 + a * 116, 750);
                ctx.stroke(); 
            }
            ctx.fillStyle = colors[0];
            for (a = 0; a < 10; a++) {// 分十大等分  
                ctx.save();  //顯示日期刻度
                ctx.translate(100+ a * 116, 760);
                ctx.rotate(Math.PI / 180 * 45);
                ctx.fillText(timeinterval[a], 0, 0);
                ctx.restore();
            }
            ctx.fillStyle = colors[1];
            for (a = 0; a < 10; a++) {// 分十大等分  
                ctx.save();  //顯示日期刻度
                ctx.translate(120 + a * 116, 760);
                ctx.rotate(Math.PI / 180 * 45);
                ctx.fillText(timeinterval[a+10], 0, 0);
                ctx.restore();
            }

            drawSensorCurvline(ctx);// 中間sensor曲線 

            //右方文字
            ctx.save();
            ctx.fillText("目前查詢:", 1355, 100);
            ctx.fillText(sensorName, 1355, 130);
            ctx.restore();
        }

        function drawrightGrid(ctx) { // 繪製右邊的曲線分析資料
            ctx.font = "20Px 新細明體";
            var temp;
            var max, min, avg;
            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1280, 30, 230, 830);
            for (a = 0; a < DragSensorList.length; a++) {
                ctx.fillStyle = colors[a];
                ctx.fillRect(1300, 50 + a * 70, 15, 15);

                ctx.fillStyle = "black";
                temp = sensorList[DragSensorList[a]];
                temp = temp.substr(temp.indexOf(":") + 1);
                ctx.fillText(temp, 1325, 50 + a * 70);//名稱

                if (a == 0)
                    cal_right_max_min(sensor0, max, min, avg, a);
                else if (a == 1)
                    cal_right_max_min(sensor1, max, min, avg, a);
                ctx.fillText(sensorMax[a].toFixed(1) + "/" + sensormin[a].toFixed(1) + "/" + sensorAvg[a].toFixed(1), 1325, 75 + a * 70);
            }
        }

        function cal_right_max_min(sen, yax, n) {
            var left, right, max, min;
            if (lineA > lineB)
            { left = lineB; right = lineA; }
            else
            { left = lineA; right = lineB; }
            left = Math.floor((left - 380) / 2);
            right = Math.floor((right - 380) / 2);

            max = parseFloat(yax[sen[left]]);
            min = parseFloat(yax[sen[left]]);
            var avg = 0, c = 0;
            var a, value;
            for (a = left + 1; a <= right; a++) {
                c += 1;
                value = parseFloat(yax[sen[a]]);
                if (value > max)
                    max = value;
                else if (value < min)
                    min = value;
                avg += value;
            }
            sensorMax[n] = max;
            sensormin[n] = min;
            if (c > 0)
                sensorAvg[n] = avg / c;
            else
                sensorAvg[n] = 0;
            //console.log("cal_right_max_min");
            //console.log(sen.length);
            //console.log(avg);
            //console.log(max);
            //console.log(min);
            //console.log(sensorAvg[n]);
        }

        function drawSensorCurvline(ctx) {
            //var a;
            //var linecolor = 0;
            //var startpos = 120;//繪製起點
            //// 1
            //if (isDragSensor < 0) return;
            //ctx.save();
            //ctx.strokeStyle = colors[0];
            //ctx.beginPath();
            ////ctx.moveTo(100, 740 - sensor0[0] * 6);// 先畫出線
            //ctx.moveTo(startpos, 740 - sensor0[0]);// 先畫出線
            //for (a = 0; a < 550; a++) {
            //    //ctx.lineTo(100 + a * 2, 740 - sensor0[a] * 6);
            //    ctx.lineTo(startpos + a * 2, 740 - sensor0[a]);
            //}
            //ctx.stroke();
            //ctx.restore();

            drawonecurve(ctx, sensor0, yaxis0, 1);
            drawonecurve(ctx, sensor1, yaxis0, 2);

            // 2
            //ctx.save();
            //ctx.strokeStyle = colors[1];
            //ctx.beginPath();
            //ctx.moveTo(startpos, 740 - sensor1[0]);// 先畫出線
            //for (a = 0; a < 550; a++) {
            //    ctx.lineTo(startpos + a * 2, 740 - sensor1[a]);
            //}
            //ctx.stroke();
            //ctx.restore();
        }

        function drawonecurve(ctx, sen, ya, n) {
            var a;
            var linecolor = 0;
            var startpos = 120;//繪製起點
            var src, dst, M, m, step, s, r;
            
            ctx.save();
            linecolor = n-1;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            src = parseFloat(ya[719]) - parseFloat(ya[n - 1]);
            dst = parseFloat(ya[719 - scrollmin[0]]) - parseFloat(ya[719 - scrollmax[0]]);
            step = dst / src;
            M = parseFloat(719 - scrollmax[0]);
            s = (sen[0] - M) / step;
            ctx.moveTo(startpos, 740 - s);// 先畫出線
            //  console.log(src + "~" + dst + ",step=" + step); 
            // console.log(yaxis0[719 - scrollmax[0]] + ":" + scrollmax[0]);
            // console.log(s + ":" + sensor0[0]+","+M);
            for (a = 0; a < 550; a++) {
                //ctx.lineTo(100 + a * 2, 740 - sensor0[a] * 6);
                //ctx.lineTo(startpos + a * 2, 740 - sensor0[a]);
                s = (sen[a] - M) / step;
                ctx.lineTo(startpos + a * 2, 740 - s);
            }
            ctx.stroke();
            ctx.restore();
        }
       
        function drawclear() { // 清畫面
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function drawFG(ctx, fg) { // 在FG 繪製特徵線和處理滑鼠事件
            // 繪製 特徵線
            ctx.strokeStyle = 'red';
            ctx.lineWidth = 3;
            ctx.beginPath();  
            ctx.moveTo(lineA, 0);
            ctx.lineTo(lineA, 740);
            ctx.stroke(); 

            // 繪製交點資訊
            if (isDragline == 1) {  //  1:lineA, 2:lineB 
                drawfocus(ctx, lineA);
            }
            //else if (isDragline == 2) {  //  1:lineA, 2:lineB 
            //    drawfocus(ctx, lineB);
            //}
        }

        function drawfocus(ctx, tx) // 繪製交點資訊,x要介於 0~1180間
        {
            var bx = tx;
            tx += 20;
            if (tx > 980) tx -= 290;
            ctx.save();
            ctx.font = "bold 20Px 新細明體";
            ctx.fillStyle = "#a8aaaa";   // 'lightgray';
            ctx.fillRect(tx, 50, 250, 30);
            ctx.fillStyle = "white";
            var t = (bx - 120) * (selectEnddateint - selectStartdateint) / 1100;
            //console.log("BX:" + bx);
            //console.log("tx:" + tx);
            //console.log("lineA:" + lineA);
            //console.log("lineB:" + lineB);
            var d = intTodate(selectStartdateint + t);
            ctx.fillText(dateformat(d), tx + 15, 72);

            var n =2; // 假設有N筆資料
            ctx.fillStyle = "#c8c8c8"; // 畫外框
            ctx.fillRect(tx, 80, 250, n * 30);
            ctx.fillStyle = 'white';
            ctx.fillRect(tx + 1, 81, 248, n * 30 - 2);

            ctx.strokeStyle = "#c8c8c8";
            ctx.lineWidth = 1;
            ctx.beginPath(); // 先畫名稱和數值的分隔線
            ctx.moveTo(tx + 80, 80);
            ctx.lineTo(tx + 80, 80 + n * 30 - 2);
            ctx.stroke();
            var temp,v;
            for (var a = 0; a < n; a++) {
                ctx.beginPath(); // 畫這筆資料的底線
                ctx.moveTo(tx + 1, 80 + a * 30 + 28);
                ctx.lineTo(tx + 249, 80 + a * 30 + 28);
                ctx.stroke(); 2

                ctx.fillStyle = colors[a]; // 資料顏色方塊
                ctx.fillRect(tx + 5, 80 + a * 30 + 10, 70, 12);

                ctx.fillStyle = "black";
                //temp = sensorList[DragSensorList[a]];
                //temp = temp.substr(temp.indexOf(":") + 1);
                //ctx.fillText(temp, tx + 23, 80 + a * 30 + 22); //名稱
                if (a == 0) {
                    v = yaxis0[parseInt(sensor0[Math.floor((bx - 120) / 2)])];
                    //ctx.fillText(v.toFixed(1), tx + 83, 80 + a * 30 + 22); //數值 
                }
                else if (a == 1) {
                    v = yaxis0[parseInt(sensor1[Math.floor((bx - 120) / 2)])];
                   // ctx.fillText(v.toFixed(1), tx + 83, 80 + a * 30 + 22); //數值
                }
                 
                if (isNaN(v)) {
                    ctx.fillText("NAN", tx +  83, 80 + a * 30 + 22);//  NAN");
                }
                else {
                    ctx.fillText(v.toFixed(1), tx +  83, 80 + a * 30 + 22); //數值                    
                }
            }
            ctx.restore();
        }

        function draw() { // 主繪圖函數，整合一切 
            if (isDragline == -1)
                drawBG(); // 1. 先畫好背景

            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");

            var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            var imgData = ctxbg.getImageData(0, 0, 1560, 850);
            ctxfg.putImageData(imgData, 0, 0);

            drawYcurve(ctxfg);

            drawFG(ctxfg, fg);
        }

        function drawYcurve(ctx) { //Y軸刻度 
            ctx.fillStyle = "#d3ffff";
            ctx.fillRect(1, 1, 85, 848);

            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var a,v;
            var tempyInterval;
            tempyInterval = parseInt((scrollmax[0] - scrollmin[0]) / 12);
            //console.log("tempyInterval="+tempyInterval);
            for (a = 0; a <= 12; a++) {// 0~120度 
                //ctx.fillText(yaxis0[a * 60], 25, 740 - a * 60 + 4); // 數字 
                v=yaxis0[719 - scrollmax[0] + a * tempyInterval];
                ctx.fillText(v.toFixed(1), 25, 740 - a * 60 + 4); // 數字                
            }

            ctx.fillStyle = "#ffffff"; // scroll
            ctx.fillRect(86, 7, 15, 729);
            ctx.fillStyle = "#888888"; // scrollmin
            ctx.fillRect(87, scrollmin[0] + 8, 13, 8);
            ctx.fillStyle = "#888888"; // scrollmax
            ctx.fillRect(87, scrollmax[0] + 8, 13, 8);
        }

        function getparam() {
            // Step1 取得網址 
            console.log(location.href);
            var getUrlString = location.href;//取得網址，並存入變數
            //Step2 將網址 (字串轉成URL)
            var url = new URL(getUrlString);
            // Step3 使用URL.searchParams + get 函式  (括弧裡面帶入欲取得結果的KEY鍵值參數) 
            var id = url.searchParams.get('id');
            var a = url.searchParams.get('a');
            var b = url.searchParams.get('b');

            if (id == null)
                console.log("NULL");
            else
                console.log(id);
            console.log(url.searchParams.get('a'));
            console.log(url.searchParams.get('b'));


            var c = url.searchParams.get('c');
            var d = url.searchParams.get('d');
            if (c != null && d != null) {
                document.getElementById("test1").innerText = d;
                console.log(d);
            }
        }

        function CanvasFG_mouosedown(e) { 
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
           // document.getElementById("test1").innerText = ",now>>down";
            console.log(x + "," + y); 
            if (x > lineA - 3 && x < lineA + 3) { 
                isDragline = 1;
            }

            if (x >= 87 && x < 100) {// isScrollRange
                if (y > scrollmin[0] + 8 && y < scrollmin[0] + 16)
                    isScrollRange = 1;
                else if (y > scrollmax[0] + 8 && y < scrollmax[0] + 16)
                    isScrollRange = 2;

                console.log("y=" + y + ",isScrollRange=" + isScrollRange);
            }

        }
        function CanvasFG_mouosemove(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            if (isDragline == 1) {// isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
                if (x >= 100 && x <= 1200) { // 曲線範圍內才可以拖拉
                    lineA = x;
                    draw();
                }
            }

            if (x >= 87 && x < 100  ) {// isScrollRange
                if (isScrollRange == 1) {
                    if (y >= 8 && y < 728 && y < (scrollmax[0] - 12)) {
                        scrollmin[0] = y - 8;
                        draw();
                    }
                }
                else if (isScrollRange == 2) {
                    if (y >= 8 && y < 728 && y > (scrollmin[0] + 12)) {
                        scrollmax[0] = y - 8;
                        draw();
                    }
                }

            }
        }
        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            if (isDragline > -1) {
                isDragline = -1;
                draw();
            }

            if (isScrollRange > -1) {
                isScrollRange = -1;
            }
        }
      
        function GetsensorList() {
            var actionUrl;
            var f = document.getElementById("DDarea").value;
            var g = document.getElementById("DDclass").value;
            var h = document.getElementById("DDdevice").value;

            actionUrl = "wf.aspx?c=A02&f=" + f + "&g=" + g + "&h=" + h;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData(actionUrl); // 抓取json 
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料";
            console.log(details);

            var a;
            var selectbox = document.getElementById("ListBox1");
            for (a = selectbox.options.length - 1 ;a >= 0 ; a--) {
                selectbox.remove(a);
            }

            //for (var a = 0; a < 29; a++) {
            //    if ((a + t) >= sensorList.length) break; 
            //    temp = sensorList[a + t];
            //    temp = temp.substr(temp.indexOf(":") + 1); 
            //    ctx.fillText(temp, 10, a * 30 + 30); 
            //}

            var temp;
            for (a = 0; a < sensorList.length - 1; a++) {
                temp = sensorList[a];
                temp = temp.substr(temp.indexOf(":") + 1);
                selectbox.add(new Option(temp, temp), null);
                // selectbox.add(new Option("111", "111"), null) //add to position end
                // selectbox.add(new Option("222", "222"), selectbox.options[0]) //add to position 0         
            }

            // draw();
            // $('#test1').html(details); // 找到test1，並且把資訊顯示上去
        }

        function GetData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.ID + ":" + item.name;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        } 

        function listboxSelected() {
            // console.log(document.getElementById("ListBox1").value);
            // console.log(document.getElementById("ListBox1").options[3].text);
            // document.getElementById("myselect").options[3]//取第四個選項
            var a;
            a = document.getElementById("ListBox1").selectedIndex;
            console.log(a);
            if (a > -1) {
                isDragSensor = a;
                GenSensorGrid(0);
                GenSensorGrid(1);
                draw();
            }
            else
                isDragSensor = -1; 
        }

        function GenSensorGrid(n) {
            // DDyear1Start/DDmonth1Start/DDday1Start/DDhour1Start/DDminute1Start ~ 
            // DDyear1End/DDmonth1End/DDday1End/DDhour1End/DDminute1End   
            // DDyear2Start/DDmonth2Start/DDday2Start/DDhour2Start/DDminute2Start ~ 
            // DDyear2End/DDmonth2End/DDday2End/DDhour2End/DDminute2End   
            var a1, a2, a3, a4, a5, b1, b2, b3, b4, b5;
            if (n == 0) {
                a1 = document.getElementById("DDyear1Start").value;
                a2 = document.getElementById("DDmonth1Start").value;
                a3 = document.getElementById("DDday1Start").value;
                a4 = document.getElementById("DDhour1Start").value;
                a5 = document.getElementById("DDminute1Start").value;
                b1 = document.getElementById("DDyear1End").value;
                b2 = document.getElementById("DDmonth1End").value;
                b3 = document.getElementById("DDday1End").value;
                b4 = document.getElementById("DDhour1End").value;
                b5 = document.getElementById("DDminute1End").value;
            }
            else {
                a1 = document.getElementById("DDyear2Start").value;
                a2 = document.getElementById("DDmonth2Start").value;
                a3 = document.getElementById("DDday2Start").value;
                a4 = document.getElementById("DDhour2Start").value;
                a5 = document.getElementById("DDminute2Start").value;
                b1 = document.getElementById("DDyear2End").value;
                b2 = document.getElementById("DDmonth2End").value;
                b3 = document.getElementById("DDday2End").value;
                b4 = document.getElementById("DDhour2Start").value;
                b5 = document.getElementById("DDminute2End").value;
            }

            selectStartdate = a1 + a2 + a3 + a4 + a5;
            selectEnddate = b1 + b2 + b3 + b4 + b5;
            selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
            selectEnddateint = dateToint(b1, b2, b3, b4, b5);

            var a;
            var s = selectStartdateint;
            var e = selectEnddateint;
            var interval = (e - s) / 9;

            if (n == 0) {
                timeinterval = new Array();// 月份由0開始 
                timeinterval.push(dateformat(intTodate(s)));
                for (a = 1; a < 9; a++) {
                    timeinterval.push(dateformat(intTodate(s + a * interval)));
                }
                timeinterval.push(dateformat(intTodate(e)));
            }
            else {
                timeinterval.push(dateformat(intTodate(s)));
                for (a = 1; a < 9; a++) {
                    timeinterval.push(dateformat(intTodate(s + a * interval)));
                }
                timeinterval.push(dateformat(intTodate(e)));
            }
              
            var actionUrl; // 每一個sensor 獨立重新讀取
            console.log();
            var id = (String)(sensorList[isDragSensor]); // << 先讀取第一個 ，再來要包成迴圈         
            var pos = id.indexOf(":");
            id = id.substr(0, pos);
            console.log("ID:" + id + "," + sensorList[isDragSensor] + "," + isDragSensor);
            sensorName = (String)(sensorList[isDragSensor]);
            sensorName = sensorName.substr(pos + 1);


            if (id.length > 0)
                actionUrl = "wf.aspx?c=A01&d=" + selectStartdate + "&f=" + selectEnddate + "&g=" + id;
            else {
                console.log("no sensor select...");
                return;
            }
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            var start_date = new Date(); // 取得一開始的時間
            if (n == 0) {
                sensor0 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(0, sensor0);
            }
            else if (n == 1) {
                sensor1 = GetsensorData(actionUrl); // 抓取json
                calmax_min_avg(1, sensor1);
            } 

            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensor0.length + "筆資料";
            console.log(details);

            // draw();
        }

        function GetsensorData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(parseFloat(temp));
                        });
                    }
                }
            });
            return result;
        }

        function dateToint(y, M, d, h, m) {
            var nowdate = new Date(y, M - 1, d, h, m); // 月份由0開始
            console.log(y + "," + M + "," + d + "," + h + "," + m);
            console.log(nowdate);
            var ticks = ((nowdate.getTime() * 10000) + 621355968000000000);
            return ticks;//10000 are the ticks per millisecond.
        }

        function intTodate(n) {
            //var epochMicrotimeDiff = 621355968000000000; 
            var ticksToMicrotime = n - 621355968000000000;
            var tickDate = new Date(ticksToMicrotime / 10000);
            return tickDate;
        }

        function dateformat(d) { // 傳入date ，傳回2018/04/03 5:35 
            var M = d.getMonth() + 1;
            M = (M < 10) ? "0" + M : M;
            var D = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
            var H = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var m = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            var s = (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();
            var temp = d.getFullYear() + "/" + M + "/" + D + " " + H + ":" + m + ":" + s;
            console.log("dateformat" + temp);
            return temp;
        }

        function testtime() {
            var n = dateToint(2018, 4, 10, 6, 52);
            console.log("test:" + n);

            var d = intTodate(n);
            console.log("test:" + d);

            timeinterval = new Array();
            var sdate = new Date(2018, 3, 1, 10, 20); // 月份由0開始
            var edate = new Date(2018, 3, 1, 19, 20); // 月份由0開始
            var s = dateToint(2018, 4, 1, 10, 20);
            var e = dateToint(2018, 4, 1, 19, 20);
            var interval = (e - s) / 9;
            var a;
            console.log("1:" + intTodate(s));
            timeinterval.push(dateformat(intTodate(s)));
            for (a = 1; a < 9; a++) {
                console.log((a + 1) + ":" + intTodate(s + a * interval));
                timeinterval.push(dateformat(intTodate(s + a * interval)));
            }
            console.log("9:" + intTodate(e));
            timeinterval.push(dateformat(intTodate(e)));

            for (a = 0; a < timeinterval.length; a++)
                console.log("timeinterval:" + timeinterval[a]);
        }

        function calmax_min_avg(n, sen) {
            var max = sen[0], min = sen[0], avg = 0, c = 0;
            var a;
            for (a = 1; a < sen.length; a++) {
                if (sen[a] > max)
                    max = sen[a];
                else if (sen[a] < min)
                    min = sen[a];
                avg += parseFloat(sen[a]);
            }
            sensorMax[n] = max;
            sensormin[n] = min;
            sensorAvg[n] = avg / sen.length;
            //console.log("calmax_min_avg");
            //console.log(sensorMax[n]);
            //console.log(sensormin[n]);
            //console.log(sensorAvg[n]);

            if (n == 0)  
                cal_Yaxis(max, min, 0);
            else if (n == 1)
                cal_Yaxis(max, min, 1); 
            //console.log("n=" + n);
            //console.log("yaxis0=" + yaxis0.length); 
        }

        function cal_Yaxis(max, min, n) {
            var d, new_max, new_min, step, a;
            if (n == 0) {
                yaxis0 = [];

                // var yaxis0 = new Array(); // 720筆資料
                //yaxis0.push(val);
                //console.log("cal_Yaxis");

                d = max - min;
                //console.log("max=" + max);
                //console.log("min=" + min);
                //console.log("d=" + d);

                new_max = parseFloat(max) + parseFloat(d / 4);
                new_min = parseFloat(min) - parseFloat(d / 4);

                //console.log("new_max=" + new_max);
                //console.log("new_min=" + new_min);

                d = new_max - new_min;
                step = d / 720.0;
                //console.log("step=" + step);

                for (a = 0; a <= 720; a++) {
                    if (n == 0)
                        yaxis0.push(parseFloat(new_min + a * step));
                    else if (n == 1)
                        yaxis1.push(parseFloat(new_min + a * step));
                } 
                for (a = 0; a < sensor0.length; a++) {
                    sensor0[a] = parseFloat((sensor0[a] - new_min) / step);
                    // sensor1[a] = parseInt((sensor1[a] - new_min) / step);
                }
            }
            else {
                new_min = yaxis0[0];
                new_max = yaxis0[720];
                d = new_max - new_min;
                step = d / 720.0;
                for (a = 0; a < sensor0.length; a++) {
                    sensor1[a] = parseFloat((sensor1[a] - new_min) / step);
                }
            }
        }

        function MyDropDownyear1Function() {
            console.log(document.getElementById("DDyear1Start").value);
            console.log($("#DDyear1Start").val());
            document.getElementById("DDyear1End").value = document.getElementById("DDyear1Start").value;
        }
        function MyDropDownmonth1Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDmonth1Start").value);
            document.getElementById("DDmonth1End").value = document.getElementById("DDmonth1Start").value;
        }
        function MyDropDownday1Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDday1Start").value);
            document.getElementById("DDday1End").value = document.getElementById("DDday1Start").value;
        }
        function MyDropDownhour1Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDhour1Start").value);
            document.getElementById("DDhour1End").value = document.getElementById("DDhour1Start").value;
        }
        function MyDropDownminute1Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDminute1Start").value);
            document.getElementById("DDminute1End").value = document.getElementById("DDminute1Start").value;
        }
        function MyDropDownyear2Function() {
            console.log(document.getElementById("DDyear2Start").value);
            console.log($("#DDyear2Start").val());
            document.getElementById("DDyear2End").value = document.getElementById("DDyear2Start").value;
        }
        function MyDropDownmonth2Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDmonth2Start").value);
            document.getElementById("DDmonth2End").value = document.getElementById("DDmonth2Start").value;
        }
        function MyDropDownday2Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDday2Start").value);
            document.getElementById("DDday2End").value = document.getElementById("DDday2Start").value;
        }
        function MyDropDownhour2Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDhour2Start").value);
            document.getElementById("DDhour2End").value = document.getElementById("DDhour2Start").value;
        }
        function MyDropDownminute2Function() {
            //   console.log($("#DDmonth1").text)
            console.log(document.getElementById("DDminute2Start").value);
            document.getElementById("DDminute2End").value = document.getElementById("DDminute2Start").value;
        }
    </script>
</head>
<body style="background-color: #ddffff">
    <form id="form1" runat="server">
        <div id="divUP" style="width: 1890px; height: 95px; border: 1px solid #c3c3c3">
            <div style="font-size: 20px">
                廠區 
            <asp:DropDownList ID="DDarea" runat="server" Width="160px" Font-Size="Large">
                <asp:ListItem Value="1">一廠</asp:ListItem>
                <asp:ListItem Value="2">二廠</asp:ListItem>
                <asp:ListItem Value="3">三廠</asp:ListItem>
                <asp:ListItem Value="4">四廠</asp:ListItem>
                <asp:ListItem Value="5">五廠</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            課別 
            <asp:DropDownList ID="DDclass" runat="server" Width="120px" Font-Size="Large" AutoPostBack="True">
                <asp:ListItem Value="1">水務</asp:ListItem>
                <asp:ListItem Value="2">氣化</asp:ListItem>
                <asp:ListItem Value="3">空調</asp:ListItem>
                <asp:ListItem Value="4">電力</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp; &nbsp;&nbsp;
            設備 
            <asp:DropDownList ID="DDdevice" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">純水設備</asp:ListItem>
                <asp:ListItem Value="2">淨化設備</asp:ListItem>
                <asp:ListItem Value="3">冷卻設備</asp:ListItem>
                <asp:ListItem Value="4">過濾設備</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp; 
                <img id="btnGetsensorList" onclick="GetsensorList();" src="images/buttom.jpg" />
                <asp:Button ID="btnQuerySensor" runat="server"   BackColor="Yellow" Font-Size="Large" Text="查詢" Width="80px"  Visible="false" />
                &nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">回首頁</asp:HyperLink>

                <label id="test"></label>
                <label id="test1"></label>
                <label id="test2"></label>

                <br />

                第一段日期:
            <asp:DropDownList ID="DDyear1Start" runat="server"  onchange="javascript:MyDropDownyear1Function();" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem> 
                <asp:ListItem Value="2019">2019</asp:ListItem> 
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth1Start" runat="server" onchange="javascript:MyDropDownmonth1Function();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday1Start" runat="server" onchange="javascript:MyDropDownday1Function();" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour1Start" runat="server" onchange="javascript:MyDropDownhour1Function();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute1Start" runat="server" onchange="javascript:MyDropDownminute1Function();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp;～ 
            <asp:DropDownList ID="DDyear1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday1End" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分 &nbsp;

                <img id="btnQuerysensor" onclick="listboxSelected();" src="images/buttom.jpg" /> <br />
                 
                  第二段日期:
            <asp:DropDownList ID="DDyear2Start" runat="server"  onchange="javascript:MyDropDownyear2Function();" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth2Start" runat="server" onchange="javascript:MyDropDownmonth2Function();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday2Start" runat="server" onchange="javascript:MyDropDownday2Function();" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour2Start" runat="server" onchange="javascript:MyDropDownhour2Function();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute2Start" runat="server" onchange="javascript:MyDropDownminute2Function();" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp;～ 
            <asp:DropDownList ID="DDyear2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday2End" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp; 
            </div>
        </div>
        <div>
            <table style="width: 1890px; height: 850px; ">
                <tr>
                    <td style="width:300px;vertical-align:top">
                        <asp:ListBox ID="ListBox1" runat="server" Height="850px"  onclick="javascript:listboxSelected();"  Width="296px" Font-Size="Large" ></asp:ListBox>
                    </td>
                    <td style="width: 1560px; vertical-align: top">
                        <canvas style="background-color:lightgray" id="myCanvasFG" width="1560" height="850" onmousemove="CanvasFG_mouosemove(event)" onmousedown="CanvasFG_mouosedown(event)" onmouseup="CanvasFG_mouoseup(event)"></canvas>
                       
                    </td>
                </tr>
            </table>
        </div>
        <div style="visibility: hidden;">
            <canvas id="myCanvasBG" width="1560" height="850"></canvas>
        </div>
    </form>
</body>
</html>
