﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminSensor.aspx.vb" Inherits="SQLWeb.AdminSensor" %>

<%@ Register Src="~/ComfirmButton.ascx" TagPrefix="uc1" TagName="ComfirmButton" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        function openSelectSensorWindow() {
            window.open('selectSensorPosition.aspx', '_blank', 'width=1500,height=980,left=0,top=0');
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="text-align: center" colspan="2">
                        <a href="index.aspx">回首頁</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="08管理系統.aspx">回管理首頁</a>
                    </td>
                </tr>
                <tr> 
                    <td style="width: 25%; font-size: 30px">
                        功能列表 
                    </td>
                    <td style="width: 75%;" rowspan="2">
                        <div style="text-align: left">
                            <table style="width: 100%;border:1px dotted " >
                                <tr>
                                    <td style="text-align: right" class="auto-style2">
                                        <br />
                                        搜尋條件 </td>
                                    <td>
                                        <asp:DropDownList ID="DDSearchFactory" runat="server" Width="120px" AutoPostBack="True"></asp:DropDownList>
                                    &nbsp;
                                        <asp:DropDownList ID="DDSearchClass" runat="server" Width="120px" AutoPostBack="True"></asp:DropDownList>
                                    &nbsp;
                                        <asp:DropDownList ID="DDSearchDevice" runat="server" Width="120px" AutoPostBack="True"></asp:DropDownList>
                                    &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right" class="auto-style2">
                                        <br />
                                        感應器列表<br />
                                        <br />
                                        <br />
                                        <br />
                                        &nbsp;</td>
                                    <td>
                                        <asp:ListBox ID="ListBox1" runat="server" Height="140px" Width="548px" AutoPostBack="True"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">
                                        <asp:Label ID="txtID" runat="server" ForeColor="#CC6600"></asp:Label>
                                        <asp:Label ID="txtState" runat="server" Text="無" ForeColor="#3333CC" Width="64px"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:Label ID="txtMSG" runat="server" ForeColor="#C04000" Width="300px" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                               
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                                        <asp:Button ID="cmdNew" runat="server" Text="新增" BackColor="#FFFFC0" />&nbsp;
                                        <uc1:ComfirmButton ID="cmdSave" runat="server" CommandName="儲存" />
                                        <uc1:ComfirmButton ID="cmdDelete" runat="server" CommandName="刪除" />  
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">TAG: 
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="txtTag" runat="server" Width="420px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">名稱: 
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="txtName" runat="server" Width="360px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">位置: 
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:DropDownList ID="DDFactory" runat="server" Width="120px"></asp:DropDownList>
                                    &nbsp;
                                        <asp:DropDownList ID="DDClass" runat="server" Width="120px"></asp:DropDownList>
                                    &nbsp;
                                        <asp:DropDownList ID="DDDevice" runat="server" Width="120px"></asp:DropDownList>
                                    &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style3">Enable:</td>
                                    <td style="text-align: left;" class="auto-style4">
                                        <asp:CheckBox ID="chkDisable" runat="server" Text="啟用" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2">種類:</td>
                                    <td style="width: 85%; text-align: left;">

                                        <asp:RadioButtonList ID="RBType" runat="server" BackColor="#99FF33" RepeatDirection="Horizontal" AutoPostBack="True">
                                            <asp:ListItem Selected="True">Gateway</asp:ListItem>
                                            <asp:ListItem>OPC</asp:ListItem>
                                            <asp:ListItem>ModbusTCP</asp:ListItem>
                                            <asp:ListItem>Modbus232/485</asp:ListItem>
                                        </asp:RadioButtonList>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" class="auto-style2"></td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:Panel ID="Panel1" runat="server" Visible="true">
                                            Gateway:<asp:TextBox ID="txtGateway" runat="server" Width="180px"></asp:TextBox>&nbsp;&nbsp; 這裡填入gateway名稱，gateway的詳細設定需到左方 &quot;gateway管理&quot;功能<br /> &nbsp;&nbsp; UnitID:<asp:TextBox ID="txtUnitID" runat="server" Width="100"></asp:TextBox>
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;FFUID: <asp:TextBox ID="txtFFUID" runat="server" Width="100px"></asp:TextBox>
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 對應的FFUID:<asp:TextBox ID="txt對應的FFUID" runat="server" Width="100px"></asp:TextBox><br />
                                            
                                            感應器種類&nbsp;:&nbsp;<asp:DropDownList ID="DDSensorKind" runat="server" Width="120px">
                                                <asp:ListItem>壓力</asp:ListItem>
                                                <asp:ListItem>風量</asp:ListItem>
                                                <asp:ListItem>溫度</asp:ListItem>
                                                <asp:ListItem>濕度</asp:ListItem>
                                            </asp:DropDownList>(此欄位必須正確，後端程式才有辦法擷取到gateway正確內容)
                                        </asp:Panel>
                                        <asp:Panel ID="Panel2" runat="server" Visible="false"> 
                                            SlaveID:<asp:TextBox ID="txtSlaveID" runat="server" Width="100px"></asp:TextBox> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            Address:<asp:TextBox ID="txtAddress" runat="server" Width="80px">0</asp:TextBox>
                                            &nbsp;這個預設值是0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ComPort:
                                            <asp:TextBox ID="txtComPort" runat="server" Width="100px"></asp:TextBox>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel3" runat="server" Visible="false"> 

                                        </asp:Panel> 
                                        
                                    </td>
                                </tr>
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;"></td>
                                    <td style="text-align: left;" class="auto-style1">
                                       

                                    </td>
                                </tr> 
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;">群組:</td>
                                    <td style="text-align: left;" class="auto-style1">
                                        <asp:DropDownList ID="DDGroup" runat="server" Width="220px"></asp:DropDownList>
                                    </td>
                                </tr> 
                               <%-- <tr style="height: 22px;">
                                    <td style="text-align: right;">種類:</td>
                                    <td class="auto-style1">
                                        <asp:DropDownList ID="DDKind" runat="server" Width="120px"></asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;&nbsp; 座標:
                                        <asp:Label ID="txtX" runat="server" ForeColor="#CC6600"></asp:Label>
                                        &nbsp;,
                                        <asp:Label ID="txtY" runat="server" ForeColor="#CC6600"></asp:Label>
                                    &nbsp;
                                        <button id="btnSelectPosition" onclick="openSelectSensorWindow();" >
                                            選擇
                                        </button>
                                    </td>
                                </tr>--%>
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;">Fault軸:</td>
                                    <td style="text-align: left;" class="auto-style1">
                                        <asp:TextBox ID="txtFault軸" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        PreAlarm軸<asp:TextBox ID="txtPreAlarm軸" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        保養設定軸<asp:TextBox ID="txt保養設定軸" runat="server" Width="100px"></asp:TextBox> 
                                    </td>
                                </tr> 
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;">HH:</td>
                                    <td style="text-align: left;" class="auto-style1">
                                        <asp:TextBox ID="txtHH" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        HIGH<asp:TextBox ID="txtHIGH" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        LOW<asp:TextBox ID="txtLOW" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        LL<asp:TextBox ID="txtLL" runat="server" Width="100px"></asp:TextBox> 
                                    </td>
                                </tr>  
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;">Max:</td>
                                    <td style="text-align: left;" class="auto-style1">
                                        <asp:TextBox ID="txtMax" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        min<asp:TextBox ID="txtmin" runat="server" Width="100px"></asp:TextBox> 
                                         &nbsp; &nbsp; 這兩個是感應器的最大最小值，會直接影響圖表的呈現效果  
                                    </td>
                                </tr> 
                                <tr style="height: 22px;">
                                    <td style="width: 15%; text-align: right;">備註:</td>
                                    <td style="text-align: left;" class="auto-style1">
                                        <asp:TextBox ID="txtComment" runat="server" Width="300px"></asp:TextBox> 
                                    </td>
                                </tr> 

                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td style="width: 25%; font-size: 25px;vertical-align:top">
                        <asp:HyperLink ID="HLAdminUser" runat="server" NavigateUrl="AdminUser.aspx" >帳號管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLSensor" runat="server" NavigateUrl="AdminSensor.aspx" >感應器管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLFactory" runat="server" NavigateUrl="AdminFactory.aspx" >廠區管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLDevice" runat="server" NavigateUrl="AdminDevice.aspx" >設備管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLClass" runat="server" NavigateUrl="AdminClass.aspx" >課別管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLOPC" runat="server" NavigateUrl="AdminOPC.aspx" >OPC管理</asp:HyperLink> <br />
                        <asp:HyperLink ID="HLGateway" runat="server" NavigateUrl="AdminGateway.aspx" >Gateway管理</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">底部
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
