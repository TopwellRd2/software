﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="SQLWeb.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 20%;
            height: 57px;
        }

        .auto-style2 {
            width: 60%;
            height: 57px;
        }
    </style>
</head>

<body style="background: url(image/bg-gray.jpg); margin-right: auto; margin-left: auto; width: 1280px;">
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 35%; height: 80px"></td>
                                <td style="width: 40%"></td>
                                <td style="width: 35%"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <table style="width: 100%; font-weight: bold; color: #FFF; background-color: #124155;">
                                        <tr>
                                            <td style="height: 85px;">
                                                <label style="height: 50px; width: 450px; margin-bottom: 30px; margin-top: 5px;">
                                                    <img src="images/icon02.png" alt="" width="60" height="50" class="imgB" />
                                                    <asp:Label ID="Label1" runat="server" Text="帳號:" Font-Size="X-Large"></asp:Label>
                                                </label>
                                                <asp:TextBox ID="txtAccount" runat="server" Font-Size="X-Large"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 85px;">
                                                <label style="height: 50px; width: 450px; margin-bottom: 30px; margin-top: 5px;">
                                                    <img src="images/icon03.png" width="60" height="50" class="imgB" />
                                                    <asp:Label ID="Label2" runat="server" Text="密碼:" Font-Size="X-Large"></asp:Label>
                                                </label>
                                                <asp:TextBox ID="txtPassword" runat="server" Font-Size="X-Large" TextMode="Password"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 85px;">
                                                <asp:Label ID="txtMsg" runat="server" Font-Size="X-Large" ForeColor="#FF9999" Width="250"></asp:Label>

                                                <img src="images/icon04.png" width="40" height="28" class="imgC" />
                                                <asp:LinkButton ID="LinkButton1" runat="server" Font-Size="X-Large" ForeColor="White">登入</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>


                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="width: 35%; height: 80px"></td>
                                <td style="width: 30%"></td>
                                <td style="width: 35%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>


