﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminGroupAddmember.aspx.vb" Inherits="SQLWeb.AdminGroupAddmember" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 25px">
                                    <a href="Index.aspx">首頁</a> 
                                      >>
                                    <a href="06進階警報管理.aspx">進階警報管理</a>
                                    >>
                                    <a href="AdminGroup.aspx">群組推播設定管理</a>
                                    >>群組推播可接收人員設定
                                </td>  
                            </tr>
                           <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                          
                                            </td>
                                            <td style="width:1000px;height:220px;text-align:left;vertical-align:top;font-size:20px">
                                              
                                                <table>
                                                    <tr>
                                                        <td style="width:200px"></td>
                                                        <td style="width:800px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px">訊息內容:
                                                            <asp:Label ID="txtID" runat="server" ForeColor="#CC3300" Visible="False"></asp:Label></td>
                                                        <td style="width: 800px">
                                                            <asp:Label ID="txtMsgName" runat="server" ForeColor="#CC3300"></asp:Label></td>
                                                    </tr>
                     
                                                    <tr>
                                                        <td style="width: 200px">所有人員:<br />
                                                            (在右方選單點選加入)</td>
                                                        <td style="width: 800px">
                                                            <asp:ListBox ID="ListBox1" runat="server" Font-Size="X-Large" Height="300px" Width="460px" AutoPostBack="True"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="width: 200px">目前選擇的人員:</td>
                                                        <td style="width: 800px">
                                                            <asp:ListBox ID="ListBox2" runat="server" Font-Size="X-Large" Height="300px" Width="460px"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style1"></td>
                                                        <td class="auto-style2">
                                                            <asp:Button ID="btnClear" runat="server" Font-Size="16pt" Height="32px" Text="清除所有人" Width="120px" BackColor="#99FF66" ForeColor="Black" />
                                                        &nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btnSave" runat="server" Font-Size="16pt" Height="32px" Text="儲存目前設定" Width="145px" BackColor="#99FF66" ForeColor="Black" />
                                                        &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:100px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                            
                                            </td>
                                        </tr>  
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 36px;border:dotted">  
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>