﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="04維保趨勢通知.aspx.vb" Inherits="SQLWeb._02維保趨勢通知" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>奇立實業</title>
    <script src='http://code.jquery.com/jquery-1.8.2.js'></script>    
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>
    <script>

        var lineA = 150, lineB = 120, lineC = 100; // 3條特徵線
        var colors = new Array("blue", "orange", "green", "red", "purple", "brown", "yellow", "gray", "gold", "black");
        var isDragline = -1; // -1:沒有, 1:lineA, 2:lineB

        var timeinterval = new Array();
        var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
        var DragSensorx, DragSensory; // 被拖拉的感應器位置
        var DragSensor  =-1; // 被拉進來的感應器 
        var isDisplaySensor = new Array(); // 是否要顯示這個感應器
        var sensorMax = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensormin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensorAvg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensor0 = new Array(); // 600筆資料 

        var sensorList = new Array();// 左邊的感應器列表

        function drawBG() { // 在隱藏的BG 繪製底下的表格
            var canBG = document.getElementById("myCanvasBG");
            var ctx = canBG.getContext("2d");
            var a;

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(0, 0, 1520, 880);

            //Y軸刻度
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(90,20);
            ctx.lineTo(90,740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var tt =[0,5,19,21,23,25,28,80,90,100,110,120,130];
            for (a = 0; a <= 12; a++) {// 0~120度
                ctx.beginPath(); // 刻度
                ctx.moveTo(90, 740-a*60);
                ctx.lineTo(80, 740 - a * 60);
                ctx.stroke();

                if (a < 10)
                    //ctx.fillText(a * 10, 52, 740 - a * 60+4 ); // 數字
                    ctx.fillText(tt[a], 52, 740 - a * 60 + 4); // 數字
                else
                    ctx.fillText(a * 10, 46, 740 - a * 60+4 ); // 數字
            }
            ctx.save();
            ctx.strokeStyle = 'gray';// 0~120度的橫向虛線條
            ctx.setLineDash([5, 5]);
            for (a = 1; a <= 12; a++) {
                ctx.beginPath(); // 刻度
                ctx.moveTo(90.5, 740.5 - a * 60);
                ctx.lineTo(1250.5, 740.5 - a * 60);
                ctx.stroke();
            }
            ctx.restore();

            //X軸刻度
            ctx.strokeStyle = 'black';
            ctx.beginPath();
            ctx.moveTo(90, 740);
            ctx.lineTo(1250, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            for (a = 0; a <8; a++) {// 分8大等分
                ctx.beginPath(); // 刻度
                ctx.moveTo(240+a*142, 740);
                ctx.lineTo(240+a*142, 750);
                ctx.stroke();

                ctx.save();  //顯示日期刻度
                ctx.translate(235 + a * 142, 766);
                //ctx.rotate(Math.PI / 180 * 45);
                if (a<7)
                    ctx.fillText(a * 3 + 3, 0, 0);
                else
                    ctx.fillText(a * 3 + 3 + " hr", 0, 0);
                ctx.restore();
            }

            // 中間的曲線 
            drawSensorCurvline(ctx);// 中間sensor曲線 
        }

        function drawSensorCurvline(ctx) {
            var a;
            var linecolor = 0;

            console.log("drawSensorCurvline:"+DragSensor);
            if (DragSensor==-1) return;
            ctx.save();
            linecolor = 0;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - sensor0[0] * 6);// 先畫出線
            console.log("sensor0[0]:" + sensor0[0]);
            for (a = 0; a < 550; a++) {
                ctx.lineTo(100 + a * 2, 740 - sensor0[a] * 6);
            }
            ctx.stroke();
            ctx.restore();

            //drawDeadban(ctx, c);
        }

        function drawBGline(ctx, c, linecolor) { // 中間的曲線
            var a;
            ctx.save();
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - c[0] * 6);// 先畫出線
            for (a = 1; a < c.length ; a++) {
                ctx.lineTo(100 + a *19, 740 - c[a] * 6);
            }
            ctx.stroke();
            /*
            ctx.fillStyle = colors[linecolor];
            for (a = 0; a < c.length ; a++) {// 绘制一个填充圆
                ctx.beginPath();
                ctx.arc(100 + a * 41, 740 - c[a] * 6, 5, 0 * 0.0175, 360 * 0.0175, true);
                ctx.fill();
            }*/
            ctx.restore();

            drawDeadban(ctx, c);
        }

        function drawDeadban(ctx, c) {//繪製干擾的deadban框
            var a;
            ctx.save();
            ctx.strokeStyle = 'gray';
            ctx.beginPath();
            for (a = 0; a < c.length ; a++) {
                if (c[a] >= 110) {
                    //ctx.lineTo(100 + a * 20, 740 - c[a] * 6);
                    ctx.rect(100 + a * 19 - 7, 740 - c[a] * 6 - 10, 15, 60);
                }
            }
            ctx.stroke();
            ctx.restore();
        }

        function drawLeftList(ctx) { // 畫出左邊感應器列表
            ctx.fillStyle = "black";
            ctx.font = "20Px 新細明體"; 

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1, 1, 278, 879); //clear background
             
            var temp;
            for (var a = 0; a < sensorList.length; a++) {
                //ctx.fillStyle = "#ffcccc";
                //ctx.fillRect(10, a * 30 + 13, 266, 21); 
                ctx.fillStyle = "black";
                temp = sensorList[a];
                temp = temp.substr(temp.indexOf(":") + 1);
                //ctx.fillText(sensorList[a], 10, a * 30 + 30); 
                ctx.fillText(temp, 10, a * 30 + 30);
                //console.log(sensorList[a]);
            } 
        }
         
        function drawFG(ctx, fg) { // 在FG 繪製特徵線和處理滑鼠事件
            // 繪製3條特徵線，往右偏移280
            ctx.font = "16Px 新細明體";
            ctx.lineWidth = 3;
            ctx.strokeStyle = 'orange';
            ctx.fillStyle = 'orange';
            ctx.beginPath(); // 第1條，保養設定軸
            ctx.moveTo(280 + 95, lineA);
            ctx.lineTo(280 + 1250, lineA);
            ctx.stroke();
            ctx.fillText("保養設定軸", 280 + 1285, lineA+3);

            ctx.strokeStyle = 'red';
            ctx.fillStyle = 'red';
            ctx.beginPath(); // 第2條，PreAlarm軸
            ctx.moveTo(280 + 95, lineB);
            ctx.lineTo(280 + 1250, lineB);
            ctx.stroke();
            ctx.fillText("PreAlarm軸", 280 + 1285, lineB + 3);

            ctx.strokeStyle = 'purple';
            ctx.fillStyle = 'purple';
            ctx.beginPath(); // 第3條，Fault軸
            ctx.moveTo(280 + 95, lineC);
            ctx.lineTo(280 + 1250, lineC);
            ctx.stroke();
            ctx.fillText("Fault軸", 280 + 1285, lineC + 3);

            // 繪製警報點方框
            if (isDragline == 1) {  //  1:lineA, 2:lineB
                drawAlarmRec(ctx, lineA);
            }
            else if (isDragline == 2) {  //  1:lineA, 2:lineB
                drawAlarmRec(ctx, lineB);
            }
        }

        function drawAlarmRec(ctx, tx) // 繪製警報點方框,x要介於90~1250
        {
         
        }
          
        function drawclear() { // 清畫面
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function draw() { // 主繪圖函數，整合一切 
            if (isDragline == -1)
                drawBG(); // 1. 先畫好背景

            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");

            var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            var imgData = ctxbg.getImageData(0, 0, 1520, 880)
            ctxfg.putImageData(imgData, 280, 0);

            ctxfg.beginPath();// 劃出中間分隔線
            ctxfg.moveTo(280, 0);
            ctxfg.lineTo(280, 880);
            ctxfg.strokeStyle = 'black';
            ctxfg.stroke();
             
            drawFG(ctxfg, fg);
            drawLeftList(ctxfg); // 畫出左邊感應器列表

            if (isDragSensor > -1)// 左邊拖拉的感應器
            {
                ctxfg.fillStyle = "#ffcccc";
                ctxfg.fillRect(DragSensorx, DragSensory, 266, 21);
                ctxfg.fillStyle = "black";
                ctxfg.fillText(sensorList[isDragSensor], DragSensorx, DragSensory + 17);
            }
        }

        function GetsensorList() {
            var actionUrl;
            var f = document.getElementById("DDarea").value;
            var g = document.getElementById("DDclass").value;
            var h = document.getElementById("DDdevice").value;

            sensor0 = new Array();
            isDragSensor = -1;
            isDragline = -1;

            actionUrl = "wf.aspx?c=A02&f=" + f + "&g=" + g + "&h=" + h;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData(actionUrl); // 抓取json 
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料";
            var a;
            // console.log(details);

            draw();
            // $('#test1').html(details); // 找到test1，並且把資訊顯示上去
        }

        function GetData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.ID + ":" + item.name;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }


        function CanvasFG_mouosedown(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
           // document.getElementById("test1").innerText = ",now>>down";

            //280 + 95~280 + 1250
            // isDragline = -1; // -1:沒有, 1:lineA, 2:lineB , 3:lineC
            if (y > lineA - 3 && y < lineA + 3 && x>375) {
               // document.getElementById("test1").innerText = ",now>>down>>A";
                isDragline = 1;
            }
            else if (y > lineB - 3 && y < lineB + 3 && x > 375) {
               // document.getElementById("test1").innerText = ",now>>down>>B";
                isDragline = 2;
            }
            else if (y > lineC - 3 && y < lineC + 3 && x > 375) {
              //  document.getElementById("test1").innerText = ",now>>down>>C";
                isDragline = 3;
            }

            if (x > 10 && x < 276) {
                var n = parseInt((y - 13) / 30);
                var ty = n * 30 + 13;
                //console.log(ty+":"+y);
                if (y > ty && y < ty + 21)
                    // document.getElementById("test1").innerText = ",now>>down>>sensor" +n;
                    isDragSensor = n;
                else
                    isDragSensor = -1;
            }
        }

        function CanvasFG_mouosemove(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
           // document.getElementById("test").innerText = "Coordinates: (" + x + "," + y + ")";
           // document.getElementById("test1").innerText = ",now>>move";

            if (isDragline == 1) {// isDragline = -1; // -1:沒有, 1:lineA, 2:lineB , 3:lineC
                if (x > 380 && y < 740 && y > (lineB+2)) { // 曲線範圍內才可以拖拉
                    lineA = y;
                    draw();
                    console.log(y);
                }
            }
            else if (isDragline == 2) {// -1:沒有, 1:lineA, 2:lineB , 3:lineC
                if (x > 380 && y < (lineA - 2) && y > (lineC+2)) {// 曲線範圍內才可以拖拉
                    lineB = y;
                    draw();
                    console.log(y);
                }
            }
            else if (isDragline == 3) {// -1:沒有, 1:lineA, 2:lineB , 3:lineC
                if (x > 380 && y < (lineB-2)) {// 曲線範圍內才可以拖拉
                    lineC = y;
                    draw();
                    console.log(y);
                }
            }

            if (isDragSensor > -1) {
                DragSensorx = x; DragSensory = y;
                draw();
                // console.log(x + ";" + y);
            }
        }

        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
           // document.getElementById("test1").innerText = ",now>>up";

            if (isDragline > -1)
                isDragline = -1;

            if (isDragSensor > -1) { // 有選到sensor
                if (x < 280)
                    isDragSensor = -1;
                else if (x > 282) {
                    DragSensor = isDragSensor;
                    isDragSensor = -1; 
                    GenSensorGrid(DragSensor);

                    //document.getElementById("test1").innerText = ",isDragSensor";
                    //console.log(isDragSensor); 
                    //for ( a = 0; a < DragSensorList.length; a++)
                    //    console.log("DragSensorList"+a+":"+DragSensorList[a]); 
                    draw();
                }
            }
        }

        function GenSensorGrid(n) {
            var d=1;
            var radios = document.getElementsByName('myCheck');
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {                    
                    if (i == 0)
                        d = 1; // console.log("today");
                    else if (i == 1)
                        d = 2; //console.log("week");
                    else if (i == 2)
                        d = 3;//console.log("month");
                    // draw();
                    break;
                }
            }
              
            var actionUrl; // 每一個sensor 獨立重新讀取
            console.log("ID");
            var id = (String)(sensorList[n]); // << 先讀取第一個 ，再來要包成迴圈         
            var pos = id.indexOf(":");
            id = id.substr(0, pos);
            console.log(id);

            if (id.length > 0)
                actionUrl = "wf.aspx?c=A04&d=" + d + "&g=" + id;
            else {
                console.log("no sensor select...");
                return;
            }
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            var start_date = new Date(); // 取得一開始的時間
                sensor0 = GetsensorData(actionUrl); // 抓取json  
              
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensor0.length + "筆資料";
            console.log(details);

            // draw();
        }

        function GetsensorData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }

        function checkOption() { //myCheck1
            var radios = document.getElementsByName('myCheck');
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) { 
                   // alert(radios[i].value);// today,week,month
                    if (i==0) 
                        console.log("today");
                    else         if (i==1) 
                        console.log("week");
                    else         if (i==2) 
                        console.log("month");
                   // draw();
                    break;
                }
            }
            //console.log(radios[i].value);
        }

        window.onload = function (e) {
            draw();
        }
    </script>
    <style>
        #myCanvasFG {
            position: static;
            border: 1px solid black;
        }

        #myCanvasBG {
            position: static;
            border: 1px solid black;
        }
    </style>
</head>
<body style= "background-color:#ddffff">
    <form id="form1" runat="server">
    <div id="divUP" style="width:1800px;height:80px;border:1px solid #c3c3c3"> 
        <div style="font-size:20px">
            
            廠區 
            <asp:DropDownList ID="DDarea" runat="server" Width="160px" Font-Size="Large">
                <asp:ListItem Value="1">一廠</asp:ListItem>
                <asp:ListItem Value="2">二廠</asp:ListItem>
                <asp:ListItem Value="3">三廠</asp:ListItem>
                <asp:ListItem Value="4">四廠</asp:ListItem>
                <asp:ListItem Value="5">五廠</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            課別 
            <asp:DropDownList ID="DDclass" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">水務</asp:ListItem>
                <asp:ListItem Value="2">氣化</asp:ListItem>
                <asp:ListItem Value="3">空調</asp:ListItem>
                <asp:ListItem Value="4">電力</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp; &nbsp;&nbsp;
            設備 
            <asp:DropDownList ID="DDdevice" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">純水設備</asp:ListItem>
                <asp:ListItem Value="2">淨化設備</asp:ListItem>
                <asp:ListItem Value="3">冷卻設備</asp:ListItem>
                <asp:ListItem Value="4">過濾設備</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp; 
            
            &nbsp;&nbsp;
            <img id="btnGetsensorList" onclick="GetsensorList();" src="images/buttom.jpg" />
            &nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">回首頁</asp:HyperLink>
            <br />

            日期
            <input type="radio" name="myCheck" id ="today" onclick="checkOption( )" value="today"/>今日 &nbsp;&nbsp;&nbsp; 
            <input type="radio" name="myCheck" id ="week" onclick="checkOption( )" value="week"/>過去一週 &nbsp;&nbsp;&nbsp; 
            <input type="radio" name="myCheck" id ="month" onclick="checkOption( )" value="month"/>過去一月<br />
       
            <label id="test"></label>
            <label id="test1"></label> 
        </div>

        <div style="visibility:hidden;">
            <canvas id="myCanvasBG" width="1520" height="880"></canvas>
        </div>
    </div>
    <canvas id="myCanvasFG" width="1800" height="880" onmousemove="CanvasFG_mouosemove(event)" onmousedown="CanvasFG_mouosedown(event)" onmouseup="CanvasFG_mouoseup(event)"></canvas>

    </form>
</body>
</html>
