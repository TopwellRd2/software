﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="03設備不同時間點分析.aspx.vb" Inherits="SQLWeb._03設備不同時間點分析" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業
    </title>
    <script type="text/javascript" src="js/jquery-latest.min.js"></script>
    <script type="text/javascript" src='js/jquery.blockUI.js'></script>

    <script>
        var lineA = 680, lineB = 1080; // 兩條特徵線
        var colors = new Array("blue", "orange", "green", "red", "purple", "brown", "yellow", "gray", "gold", "black");
        var isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 

        var md = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        var selectStartdate, selectEnddate;// 選擇到的時間起點和終點
        var selectStartdateint, selectEnddateint;// 選擇到的時間起點和終點
        var timeinterval = new Array();
        var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
        var DragSensorx, DragSensory; // 被拖拉的感應器位置
        var DragSensorList = new Array(); // 被拉進來的感應器列表，最多8個
        var isDisplaySensor = new Array(); // 是否要顯示這個感應器
        var sensorMax = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensormin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensorAvg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 這個感應器的最大數值
        var sensor0 = new Array(); // 600筆資料
        var sensor1 = new Array();
        var sensor2 = new Array();
        var sensor3 = new Array();
        var sensor4 = new Array();
        var sensor5 = new Array();
        var sensor6 = new Array();
        var sensor7 = new Array();
        var sensor8 = new Array();
        var sensor9 = new Array();

        var sensorList = new Array();// 左邊的感應器列表

        function drawBG() { // 在隱藏的BG 繪製底下的表格
            var canBG = document.getElementById("myCanvasBG");
            var ctx = canBG.getContext("2d");
            var a;

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(0, 0, 1520, 880);

            //Y軸刻度
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(90, 20);
            ctx.lineTo(90, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var tt = [1, 2, 3, 4, 5, 20, 30, 40, 50, 60, 70, 80, 90];
            for (a = 0; a <= 12; a++) {// 0~120度 
                ctx.beginPath(); // 刻度
                ctx.moveTo(90, 740 - a * 60);
                ctx.lineTo(80, 740 - a * 60);
                ctx.stroke();

                if (a < 10)
                    ctx.fillText(tt[a], 52, 740 - a * 60 + 4); // 數字
                else
                    ctx.fillText(a * 10, 46, 740 - a * 60 + 4); // 數字
            }
            ctx.save();
            ctx.strokeStyle = 'gray';
            ctx.setLineDash([5, 5]);
            for (a = 1; a <= 12; a++) {// 0~120度的橫向虛線條
                ctx.beginPath(); // 刻度
                ctx.moveTo(90.5, 740.5 - a * 60);
                ctx.lineTo(1250.5, 740.5 - a * 60);
                ctx.stroke();
            }
            ctx.restore();

            //X軸刻度 
            ctx.strokeStyle = 'black';
            ctx.beginPath();
            ctx.moveTo(90, 740);
            ctx.lineTo(1250, 740);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "16Px 新細明體";
            var interval = (selectEnddateint - selectStartdateint) / 9;
            for (a = 0; a < 10; a++) {// 分十大等分 
                ctx.beginPath(); // 刻度
                ctx.moveTo(100 + a * 122, 740);
                ctx.lineTo(100 + a * 122, 750);
                ctx.stroke();

                ctx.save();  //顯示日期刻度
                ctx.translate(95 + a * 122, 760);
                ctx.rotate(Math.PI / 180 * 45);
                if (timeinterval.length == 10)
                    ctx.fillText(timeinterval[a], 0, 0);
                ctx.restore();

                //var s = dateToint(2018, 3, 1, 10, 20);
                //var e = dateToint(2018, 3, 1, 19, 20);
                //var interval = (e - s) / 9;
                //var a;
                //console.log("1:" + intTodate(s));
                //for (a = 1; a < 8; a++)
                //    console.log((a + 1) + ":" + intTodate(s + a * interval));
                //console.log("9:" + intTodate(e));
            }

            drawSensorCurvline(ctx);// 中間sensor曲線 

            drawrightGrid(ctx);   // 右邊的曲線分析資料
        }

        function drawrightGrid(ctx) { // 繪製右邊的曲線分析資料
            ctx.font = "20Px 新細明體";
            var temp;
            var max, min, avg;
            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1280, 30, 230, 830);
            for (a = 0; a < DragSensorList.length; a++) {
                ctx.fillStyle = colors[a];
                ctx.fillRect(1300, 50 + a * 70, 15, 15);

                ctx.fillStyle = "black";
                temp = sensorList[DragSensorList[a]];
                temp = temp.substr(temp.indexOf(":") + 1);
                ctx.fillText(temp, 1325, 50 + a * 70);//名稱

                if (a == 0) cal_right_max_min(sensor0, max, min, avg, a);
                else if (a == 1) cal_right_max_min(sensor1, max, min, avg, a);
                else if (a == 2) cal_right_max_min(sensor2, max, min, avg, a);
                else if (a == 3) cal_right_max_min(sensor3, max, min, avg, a);
                else if (a == 4) cal_right_max_min(sensor4, max, min, avg, a);
                else if (a == 5) cal_right_max_min(sensor5, max, min, avg, a);
                else if (a == 6) cal_right_max_min(sensor6, max, min, avg, a);
                else if (a == 7) cal_right_max_min(sensor7, max, min, avg, a);
                else if (a == 8) cal_right_max_min(sensor8, max, min, avg, a);
                else if (a == 9) cal_right_max_min(sensor9, max, min, avg, a);
                ctx.fillText(sensorMax[a] + "/" + sensormin[a] + "/" + sensorAvg[a].toFixed(1), 1325, 75 + a * 70);
            }
        }

        function cal_right_max_min(sen, max, min, avg, n) {
            var left, right;
            if (lineA > lineB)
            { left = lineB; right = lineA; }
            else
            { left = lineA; right = lineB; }
            left = Math.floor((left - 380) / 2);
            right = Math.floor((right - 380) / 2);

            max = parseInt(sen[left]);
            min = parseInt(sen[left]);
            var avg = 0, c = 0;
            var a;
            for (a = left + 1; a <= right; a++) {
                c += 1;
                if (parseInt(sen[a]) > max)
                    max = parseInt(sen[a]);
                else if (parseInt(sen[a]) < min)
                    min = parseInt(sen[a]);
                avg += parseInt(parseInt(sen[a]));
            }
            sensorMax[n] = max;
            sensormin[n] = min;
            if (c > 0)
                sensorAvg[n] = avg / c;
            else
                sensorAvg[n] = 0;
            console.log("cal_right_max_min");
            console.log(sen.length);
            console.log(avg);
            console.log(max);
            console.log(min);
            console.log(sensorAvg[n]);
        }

        function drawSensorCurvline(ctx) {
            var a;
            var linecolor = 0;

            // 第1條
            if (DragSensorList.length < 1) return;
            ctx.save();
            linecolor = 0;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - sensor0[0] * 6);// 先畫出線
            for (a = 0; a < 550; a++) {
                ctx.lineTo(100 + a * 2, 740 - sensor0[a] * 6);
            }
            ctx.stroke();
            ctx.restore();

            // 第2條
            if (DragSensorList.length < 2) return;
            ctx.save();
            linecolor = 1;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - sensor1[0] * 6);// 先畫出線
            for (a = 0; a < 550; a++) {
                ctx.lineTo(100 + a * 2, 740 - sensor1[a] * 6);
            }
            ctx.stroke();
            ctx.restore();

            // 第3條
            if (DragSensorList.length < 3) return;
            ctx.save();
            linecolor = 2;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - sensor2[0] * 6);// 先畫出線
            for (a = 0; a < 550; a++) {
                ctx.lineTo(100 + a * 2, 740 - sensor2[a] * 6);
            }
            ctx.stroke();
            ctx.restore();

            // 第4條
            if (DragSensorList.length < 3) return;
            ctx.save();
            linecolor = 3;
            ctx.strokeStyle = colors[linecolor];
            ctx.beginPath();
            ctx.moveTo(100, 740 - sensor3[0] * 6);// 先畫出線
            for (a = 0; a < 550; a++) {
                ctx.lineTo(100 + a * 2, 740 - sensor3[a] * 6);
            }
            ctx.stroke();
            ctx.restore();
            //ctx.fillStyle = colors[linecolor];
            //for (a = 0; a < 10; a++) {// 绘制一个填充圆                
            //    ctx.beginPath();
            //    ctx.arc(100 + a * 125, 740 - c[a] * 6, 5, 0 * 0.0175, 360 * 0.0175, true);
            //    ctx.fill();
            //}

            //console.log("color:"+linecolor);
        }

        function drawFG(ctx, fg) { // 在FG 繪製特徵線和處理滑鼠事件
            // 繪製兩條特徵線
            ctx.strokeStyle = 'red';
            ctx.lineWidth = 3;
            ctx.beginPath(); // 第一條
            ctx.moveTo(lineA, 0);
            ctx.lineTo(lineA, 740);
            ctx.stroke();
            ctx.beginPath(); // 第二條
            ctx.moveTo(lineB, 0);
            ctx.lineTo(lineB, 740);
            ctx.stroke();

            // 繪製交點資訊
            if (isDragline == 1) {  //  1:lineA, 2:lineB 
                drawfocus(ctx, lineA);
            }
            else if (isDragline == 2) {  //  1:lineA, 2:lineB 
                drawfocus(ctx, lineB);
            }
        }

        function drawfocus(ctx, tx) // 繪製交點資訊,x要介於380~1480間
        {
            var bx = tx;
            tx += 20;
            if (tx > 1280) tx -= 230;
            ctx.save();
            ctx.font = "bold 20Px 新細明體";
            ctx.fillStyle = "#a8aaaa";   // 'lightgray';
            ctx.fillRect(tx, 50, 200, 30);
            ctx.fillStyle = "white";
            var t = (bx - 380) * (selectEnddateint - selectStartdateint) / 1100;
            console.log("BX:" + bx);
            console.log("tx:" + tx);
            console.log("lineA:" + lineA);
            console.log("lineB:" + lineB);
            var d = intTodate(selectStartdateint + t);
            ctx.fillText(dateformat(d), tx + 15, 72);

            var n = DragSensorList.length; // 假設有N筆資料
            ctx.fillStyle = "#c8c8c8"; // 畫外框
            ctx.fillRect(tx, 80, 200, n * 30);
            ctx.fillStyle = 'white';
            ctx.fillRect(tx + 1, 81, 198, n * 30 - 2);

            ctx.strokeStyle = "#c8c8c8";
            ctx.lineWidth = 1;
            ctx.beginPath(); // 先畫名稱和數值的分隔線
            ctx.moveTo(tx + 140, 80);
            ctx.lineTo(tx + 140, 80 + n * 30 - 2);
            ctx.stroke();
            var temp;
            for (var a = 0; a < n; a++) {
                ctx.beginPath(); // 畫這筆資料的底線
                ctx.moveTo(tx + 1, 80 + a * 30 + 28);
                ctx.lineTo(tx + 199, 80 + a * 30 + 28);
                ctx.stroke();

                ctx.fillStyle = colors[a]; // 資料顏色方塊
                ctx.fillRect(tx + 5, 80 + a * 30 + 10, 12, 12);

                ctx.fillStyle = "black";
                temp = sensorList[DragSensorList[a]];
                temp = temp.substr(temp.indexOf(":") + 1);
                ctx.fillText(temp, tx + 23, 80 + a * 30 + 22); //名稱
                if (a == 0)
                    ctx.fillText(sensor0[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 1)
                    ctx.fillText(sensor1[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 2)
                    ctx.fillText(sensor2[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 3)
                    ctx.fillText(sensor3[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 4)
                    ctx.fillText(sensor4[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 5)
                    ctx.fillText(sensor5[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 6)
                    ctx.fillText(sensor6[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
                else if (a == 7)
                    ctx.fillText(sensor7[Math.floor((bx - 380) / 2)], tx + 143, 80 + a * 30 + 22); //數值
            }

            ctx.restore();
        }

        function drawLeftList(ctx) { // 畫出左邊感應器列表
            ctx.fillStyle = "black";
            ctx.font = "20Px 新細明體";
            //var imgData = ctxbg.getImageData(0, 0, 1520, 880)
            //ctxfg.putImageData(imgData, 280, 0);

            ctx.fillStyle = "#ddffff";
            ctx.fillRect(1, 1, 278, 879); //clear background

            //ctx.fillStyle = "black";
            // console.log("drawLeftList:" + sensorList.length);

            var temp;
            for (var a = 0; a < sensorList.length; a++) {
                //ctx.fillStyle = "#ffcccc";
                //ctx.fillRect(10, a * 30 + 13, 266, 21); 
                ctx.fillStyle = "black";
                temp = sensorList[a];
                temp = temp.substr(temp.indexOf(":") + 1);
                //ctx.fillText(sensorList[a], 10, a * 30 + 30); 
                ctx.fillText(temp, 10, a * 30 + 30);
                //console.log(sensorList[a]);
            }

            //for (var a = sensorList.length;a<29; a++) {
            //    ctx.fillStyle = "#ffdddd";
            //    ctx.fillRect(10, a * 30 + 13, 266, 21);
            //    //ctx.fillStyle = "black";
            //    //ctx.fillText(a, 10, a * 30 + 30);
            //}
        }

        function drawclear() { // 清畫面
            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");
            ctxfg.clearRect(0, 0, fg.width, fg.height);
        }

        function draw() { // 主繪圖函數，整合一切 
            if (isDragline == -1)
                drawBG(); // 1. 先畫好背景

            var fg = document.getElementById("myCanvasFG");
            var ctxfg = fg.getContext("2d");

            var ctxbg = myCanvasBG.getContext("2d"); // 先把背景畫上
            var imgData = ctxbg.getImageData(0, 0, 1520, 880);
            ctxfg.putImageData(imgData, 280, 0);

            ctxfg.beginPath();// 劃出中間分隔線
            ctxfg.moveTo(280, 0);
            ctxfg.lineTo(280, 880);
            ctxfg.strokeStyle = 'black';
            ctxfg.stroke();

            drawFG(ctxfg, fg);
            drawLeftList(ctxfg); // 畫出左邊感應器列表

            if (isDragSensor > -1)// 左邊拖拉的感應器
            {
                ctxfg.fillStyle = "#ffcccc";
                ctxfg.fillRect(DragSensorx, DragSensory, 266, 21);
                ctxfg.fillStyle = "black";
                ctxfg.fillText(sensorList[isDragSensor], DragSensorx, DragSensory + 17);
            }
            //document.getElementById("Button1").value = abc;
        }


        function getparam() {
            // Step1 取得網址 
            console.log(location.href);
            var getUrlString = location.href;//取得網址，並存入變數
            //Step2 將網址 (字串轉成URL)
            var url = new URL(getUrlString);
            // Step3 使用URL.searchParams + get 函式  (括弧裡面帶入欲取得結果的KEY鍵值參數) 
            var id = url.searchParams.get('id');
            var a = url.searchParams.get('a');
            var b = url.searchParams.get('b');

            if (id == null)
                console.log("NULL");
            else
                console.log(id);
            console.log(url.searchParams.get('a'));
            console.log(url.searchParams.get('b'));


            var c = url.searchParams.get('c');
            var d = url.searchParams.get('d');
            if (c != null && d != null) {
                document.getElementById("test1").innerText = d;
                console.log(d);
            }
        }

        function CanvasFG_mouosedown(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            document.getElementById("test1").innerText = ",now>>down";

            // isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
            if (x > lineA - 3 && x < lineA + 3) {
                document.getElementById("test1").innerText = ",now>>down>>A";
                isDragline = 1;
            }
            else if (x > lineB - 3 && x < lineB + 3) {
                document.getElementById("test1").innerText = ",now>>down>>B";
                isDragline = 2;
            }

            // 感應器位置 ctx.fillRect(10, a * 30 + 13, 266, 21);
            // var isDragSensor = -1; // -1:沒有，>0:sensorlist的索引值
            if (x > 10 && x < 276) {
                var n = parseInt((y - 13) / 30);
                var ty = n * 30 + 13;
                //console.log(ty+":"+y);
                if (y > ty && y < ty + 21)
                    // document.getElementById("test1").innerText = ",now>>down>>sensor" +n;
                    isDragSensor = n;
                else
                    isDragSensor = -1;
            }
        }
        function CanvasFG_mouosemove(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            document.getElementById("test").innerText = "Coordinates: (" + x + "," + y + ")";
            document.getElementById("test1").innerText = ",now>>move";

            if (isDragline == 1) {// isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
                if (x > 380 && x < 1480) { // 曲線範圍內才可以拖拉
                    lineA = x;
                    draw();
                }
            }
            else if (isDragline == 2) {// isDragline = -1; // -1:沒有, 1:lineA, 2:lineB 
                if (x > 380 && x < 1480) {// 曲線範圍內才可以拖拉
                    lineB = x;
                    draw();
                }
            }
            if (isDragSensor > -1) {
                DragSensorx = x; DragSensory = y;
                draw();
                // console.log(x + ";" + y);
            }
        }
        function CanvasFG_mouoseup(e) {
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            //document.getElementById("test").innerHTML = "Coordinates: (" + x + "," + y + ")";
            document.getElementById("test1").innerText = ",now>>up";

            if (isDragline > -1) {
                isDragline = -1;
                draw();
            }

            if (isDragSensor > -1) { // 有選到sensor
                if (x < 280)
                    isDragSensor = -1;
                else if (x > 282) {
                    var a;
                    if (DragSensorList.length >= 9) return; // 已經滿10個了
                    for (a = 0; a < DragSensorList.length; a++) { // 先看看是否已經拉過了
                        if (DragSensorList[a] == isDragSensor) break;
                    }
                    //console.log("a=" + a + ", DragSensorList.length=" + DragSensorList.length);
                    if (a >= DragSensorList.length || DragSensorList.length == 0) { // 沒拉過，加入串列
                        DragSensorList.push(isDragSensor);
                        // console.log("DragSensorList length:" + DragSensorList.length + ",isDragSensor=" + isDragSensor);
                        GenSensorGrid(DragSensorList.length - 1);
                    }

                    //document.getElementById("test1").innerText = ",isDragSensor";
                    //console.log(isDragSensor);
                    isDragSensor = -1;
                    //for ( a = 0; a < DragSensorList.length; a++)
                    //    console.log("DragSensorList"+a+":"+DragSensorList[a]); 
                    draw();
                }
            }
        }

        function GetsensorList() {
            var actionUrl;
            var f = document.getElementById("DDarea").value;
            var g = document.getElementById("DDclass").value;
            var h = document.getElementById("DDdevice").value;

            actionUrl = "wf.aspx?c=A02&f=" + f + "&g=" + g + "&h=" + h;
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);
            var start_date = new Date(); // 取得一開始的時間
            sensorList = GetData(actionUrl); // 抓取json 
            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensorList.length + "筆資料";
            var a;
            console.log(details);

            draw();
            // $('#test1').html(details); // 找到test1，並且把資訊顯示上去
        }

        function GetData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.ID + ":" + item.name;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }

        function GenAllSensorGrid() {
            var a1 = document.getElementById("DDyear1").value;
            var a2 = document.getElementById("DDmonth1").value;
            var a3 = document.getElementById("DDday1").value;
            var a4 = document.getElementById("DDhour1").value;
            var a5 = document.getElementById("DDminute1").value;
            var b1 = document.getElementById("DDyear2").value;
            var b2 = document.getElementById("DDmonth2").value;
            var b3 = document.getElementById("DDday2").value;
            var b4 = document.getElementById("DDhour2").value;
            var b5 = document.getElementById("DDminute2").value;
            selectStartdate = a1 + a2 + a3 + a4 + a5;
            selectEnddate = b1 + b2 + b3 + b4 + b5;
            selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
            selectEnddateint = dateToint(b1, b2, b3, b4, b5);
            console.log(selectStartdateint);
            console.log(selectEnddateint);

            var s = selectStartdateint;
            var e = selectEnddateint;
            var interval = (e - s) / 9;
            var a;

            timeinterval = new Array();// 月份由0開始
            console.log("1:" + intTodate(s));
            timeinterval.push(dateformat(intTodate(s)));
            for (a = 1; a < 9; a++) {
                console.log((a + 1) + ":" + intTodate(s + a * interval));
                timeinterval.push(dateformat(intTodate(s + a * interval)));
            }
            console.log("9:" + intTodate(e));
            timeinterval.push(dateformat(intTodate(e)));

            //for (a = 0; a < timeinterval.length; a++)
            //    console.log("timeinterval:" + timeinterval[a]);

            for (a = 0; a < 10; a++) {
                if (a > DragSensorList.length - 1) break;
                GenSensorGrid(a);
            }
            draw();
        }

        function GenSensorGrid(n) {
            //DDyear1  DDmonth1  DDday1  DDhour1  DDminute1  ~ DDyear2  DDmonth2  DDday2  DDhour2  DDminute2 
            var a1 = document.getElementById("DDyear1").value;
            var a2 = document.getElementById("DDmonth1").value;
            var a3 = document.getElementById("DDday1").value;
            var a4 = document.getElementById("DDhour1").value;
            var a5 = document.getElementById("DDminute1").value;
            var b1 = document.getElementById("DDyear2").value;
            var b2 = document.getElementById("DDmonth2").value;
            var b3 = document.getElementById("DDday2").value;
            var b4 = document.getElementById("DDhour2").value;
            var b5 = document.getElementById("DDminute2").value;
            selectStartdate = a1 + a2 + a3 + a4 + a5;
            selectEnddate = b1 + b2 + b3 + b4 + b5;
            selectStartdateint = dateToint(a1, a2, a3, a4, a5);//a1*+a2*+a3*86400+a4*3600+a5*60;
            selectEnddateint = dateToint(b1, b2, b3, b4, b5);

            var actionUrl; // 每一個sensor 獨立重新讀取
            console.log("ID");
            var id = (String)(sensorList[DragSensorList[n]]); // << 先讀取第一個 ，再來要包成迴圈         
            var pos = id.indexOf(":");
            id = id.substr(0, pos);
            console.log(id);

            if (id.length > 0)
                actionUrl = "wf.aspx?c=A01&d=" + selectStartdate + "&f=" + selectEnddate + "&g=" + id;
            else {
                console.log("no sensor select...");
                return;
            }
            console.log(actionUrl);

            $.blockUI({// 鎖定畫面
                message: '<h1>讀取中...請稍待</h1>'
            });

            setTimeout(function () {
                $.unblockUI();
            }, 1000);

            var start_date = new Date(); // 取得一開始的時間
            if (n == 0) {
                sensor0 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(0, sensor0);
            }
            else if (n == 1) {
                sensor1 = GetsensorData(actionUrl); // 抓取json
                calmax_min_avg(1, sensor1);
            }
            else if (n == 2) {
                sensor2 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(2, sensor2);
            }
            else if (n == 3) {
                sensor3 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(3, sensor3);
            }
            else if (n == 4) {
                sensor4 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(4, sensor4);
            }
            else if (n == 5) {
                sensor5 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(5, sensor5);
            }
            else if (n == 6) {
                sensor6 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(6, sensor6);
            }
            else if (n == 7) {
                sensor7 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(7, sensor7);
            }
            else if (n == 8) {
                sensor8 = GetsensorData(actionUrl); // 抓取json 
                calmax_min_avg(8, sensor8);
            }
            else if (n == 9) {
                sensor9 = GetsensorData(actionUrl); // 抓取json
                calmax_min_avg(9, sensor9);
            }

            var time_different;
            var end_date = new Date();
            var details = "花費" + (end_date - start_date) + " ms," + "共" + sensor0.length + "筆資料";
            console.log(details);

            // draw();
        }

        function GetsensorData(actionUrl) {
            var result = new Array();
            var temp;
            $.ajax(
            {
                url: actionUrl, type: 'post', cache: false, async: false, dataType: 'json',
                success: function (data) {
                    if (data) { //sensorList = new Array();
                        $.each(data, function (i, item) {
                            temp = item.value;// 大小寫必須一樣
                            result.push(temp);
                        });
                    }
                }
            });
            return result;
        }

        function dateToint(y, M, d, h, m) {
            var nowdate = new Date(y, M - 1, d, h, m); // 月份由0開始
            console.log(y + "," + M + "," + d + "," + h + "," + m);
            console.log(nowdate);
            var ticks = ((nowdate.getTime() * 10000) + 621355968000000000);
            return ticks;//10000 are the ticks per millisecond.
        }

        function intTodate(n) {
            //var epochMicrotimeDiff = 621355968000000000; 
            var ticksToMicrotime = n - 621355968000000000;
            var tickDate = new Date(ticksToMicrotime / 10000);
            return tickDate;
        }

        function dateformat(d) { // 傳入date ，傳回2018/04/03 5:35 
            var M = d.getMonth() + 1;
            M = (M < 10) ? "0" + M : M;
            var D = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
            var H = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var m = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            var s = (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();
            var temp = d.getFullYear() + "/" + M + "/" + D + " " + H + ":" + m + ":" + s;
            console.log("dateformat" + temp);
            return temp;
        }

        function testtime() {
            var n = dateToint(2018, 4, 10, 6, 52);
            console.log("test:" + n);

            var d = intTodate(n);
            console.log("test:" + d);

            timeinterval = new Array();
            var sdate = new Date(2018, 3, 1, 10, 20); // 月份由0開始
            var edate = new Date(2018, 3, 1, 19, 20); // 月份由0開始
            var s = dateToint(2018, 4, 1, 10, 20);
            var e = dateToint(2018, 4, 1, 19, 20);
            var interval = (e - s) / 9;
            var a;
            console.log("1:" + intTodate(s));
            timeinterval.push(dateformat(intTodate(s)));
            for (a = 1; a < 9; a++) {
                console.log((a + 1) + ":" + intTodate(s + a * interval));
                timeinterval.push(dateformat(intTodate(s + a * interval)));
            }
            console.log("9:" + intTodate(e));
            timeinterval.push(dateformat(intTodate(e)));

            for (a = 0; a < timeinterval.length; a++)
                console.log("timeinterval:" + timeinterval[a]);
        }

        function calmax_min_avg(n, sen) {
            var max = sen[0], min = sen[0], avg = 0, c = 0;
            var a;
            for (a = 1; a < sen.length; a++) {
                if (sen[a] > max)
                    max = sen[a];
                else if (sen[a] < min)
                    min = sen[a];
                avg += parseInt(sen[a]);
            }
            sensorMax[n] = max;
            sensormin[n] = min;
            sensorAvg[n] = avg / sen.length;
            console.log(sen.length);
            console.log(avg);
            console.log(max);
            console.log(min);
            console.log(sensorAvg[n]);
        }

    </script>
</head>
<body style="background-color: #ddffff">
    <form id="form1" runat="server">
        <div id="divUP" style="width: 1800px; height: 95px; border: 1px solid #c3c3c3">
            <div style="font-size: 20px">
                廠區 
            <asp:DropDownList ID="DDarea" runat="server" Width="160px" Font-Size="Large">
                <asp:ListItem Value="1">一廠</asp:ListItem>
                <asp:ListItem Value="2">二廠</asp:ListItem>
                <asp:ListItem Value="3">三廠</asp:ListItem>
                <asp:ListItem Value="4">四廠</asp:ListItem>
                <asp:ListItem Value="5">五廠</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
            課別 
            <asp:DropDownList ID="DDclass" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">水務</asp:ListItem>
                <asp:ListItem Value="2">氣化</asp:ListItem>
                <asp:ListItem Value="3">空調</asp:ListItem>
                <asp:ListItem Value="4">電力</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp; &nbsp;&nbsp;
            設備 
            <asp:DropDownList ID="DDdevice" runat="server" Width="120px" Font-Size="Large">
                <asp:ListItem Value="1">純水設備</asp:ListItem>
                <asp:ListItem Value="2">淨化設備</asp:ListItem>
                <asp:ListItem Value="3">冷卻設備</asp:ListItem>
                <asp:ListItem Value="4">過濾設備</asp:ListItem>
                <asp:ListItem Value="5">其他</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp; 

                <asp:Button ID="btnQuerySensor" runat="server" BackColor="Yellow" Font-Size="Large" Text="查詢" Width="80px" />
                &nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">回首頁</asp:HyperLink>

                <label id="test"></label>
                <label id="test1"></label>
                <label id="test2"></label>

                <br />

                第一段日期:
            <asp:DropDownList ID="DDyear1Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem> 
                <asp:ListItem Value="2019">2019</asp:ListItem> 
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth1Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday1Start" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour1Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute1Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp;～ 
            <asp:DropDownList ID="DDyear1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday1End" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute1End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp;
            <asp:Button ID="btnQueryTime" runat="server" BackColor="Yellow" Font-Size="Large" Text="查詢" Width="80px" /><br />

                  第二段日期:
            <asp:DropDownList ID="DDyear2Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth2Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday2Start" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour2Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute2Start" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp;～ 
            <asp:DropDownList ID="DDyear2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                年&nbsp; 
            <asp:DropDownList ID="DDmonth2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                月&nbsp; 
            <asp:DropDownList ID="DDday2End" runat="server" Width="70px" Font-Size="Large">
            </asp:DropDownList>
                日&nbsp; 
            <asp:DropDownList ID="DDhour2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                時&nbsp; 
            <asp:DropDownList ID="DDminute2End" runat="server" Width="70px" Font-Size="Large">
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem Value="2019"></asp:ListItem>
            </asp:DropDownList>
                分&nbsp; 
            </div> 
        </div> 
        <div>
            <table style="width:1800px;height:820px;border:1px solid">
                <tr>
                    <td style="width:300px;vertical-align:top">
                        <asp:ListBox ID="ListBox1" runat="server" Height="798px" Width="296px" Font-Size="Large" AutoPostBack="True"></asp:ListBox>
                    </td>
                    <td style="width:1500px;vertical-align:top">
                        <asp:Image ID="Image1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
