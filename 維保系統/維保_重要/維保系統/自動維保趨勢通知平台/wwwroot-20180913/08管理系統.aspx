﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="08管理系統.aspx.vb" Inherits="SQLWeb._08管理系統" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 30px">
                                    <a href="Index.aspx">首頁</a> 
                                      >> 管理系統
                                </td> 
                            </tr>
                            <tr>
                                <td > 
                                    <table> 
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="AdminUser.aspx" target="_blank"><img src="images/001.jpg" width="150" height="150"/>
                                                <br />
                                                帳號管理 </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="AdminSensor.aspx">
                                                    <img src="images/002.jpg" width="150" height="150"/>
                                                <br />
                                                感應器管理  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="AdminFactory.aspx"><img src="images/003.jpg" width="150" height="150"/>
                                                <br />
                                                廠區管理  </a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="AdminDevice.aspx"><img src="images/004.jpg" width="150" height="150"/>
                                                <br />
                                                設備管理  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="AdminClass.aspx"><img src="images/005.jpg" width="150" height="150"/>
                                                <br />
                                                課別管理  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                              <a href="AdminAddsensor.aspx"><img src="images/006.jpg" width="150" height="150"/>
                                                <br />
                                               新增感應器</a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                              <a href="AdminSensorGroup.aspx"><img src="images/010.jpg" width="150" height="150"/>
                                                <br />
                                               感應器群組管理</a> 
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <img src="images/010.jpg" width="150" height="150"/>
                                                <br />
                                               使用權限管理 
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <%--  <a href="AdminOPC.aspx"><img src="images/006.jpg" width="150" height="150"/>
                                                <br />
                                                OPC管理  </a>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                            </tr>
                            <tr>
                                <td style=" font-size: 36px;border:dotted">  
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
