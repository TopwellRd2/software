﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="01歷史和即時資料庫.aspx.vb" Inherits="SQLWeb._01歷史和即時資料庫" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>奇立實業</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1280px; position: absolute; left: 50%; margin-left: -640px;">
            <table style="width: 100%">
                <tr>
                    <td style="height: 142px">
                        <img src="images/title.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style=" font-size: 30px">
                                    <a href="Index.aspx">首頁</a> 
                                      >> 歷史和即時資料庫
                                </td> 
                            </tr>
                            <tr>
                                <td > 
                                    <table>
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="01歷史和即時資料庫.aspx" target="_blank"><img src="images/001.jpg" width="150" height="150"/>
                                                <br />
                                                歷史和即時資料庫 </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="02多曲線雙時間軌分析.aspx">
                                                    <img src="images/002.jpg" width="150" height="150"/>
                                                <br />
                                                多曲線雙時間軌分析  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="#"><img src="images/003.jpg" width="150" height="150"/>
                                                <br />
                                                數據資料統計分析  </a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="04維保趨勢通知.aspx"><img src="images/004.jpg" width="150" height="150"/>
                                                <br />
                                                維保趨勢通知  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="05數據監控紀錄.aspx"><img src="images/005.jpg" width="150" height="150"/>
                                                <br />
                                                數據監控紀錄  </a>
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="#"><img src="images/006.jpg" width="150" height="150"/>
                                                <br />
                                                進階警報管理  </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                                <a href="07上下限警報通知.aspx"><img src="images/007.jpg" width="150" height="150"/>
                                                <br />
                                                上下線警報通知 </a> 
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                               
                                            </td>
                                            <td style="width:400px;height:220px;text-align:center;vertical-align:central;font-size:20px">
                                               
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                            </tr> 
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>通訊地址: 台南市新營區五福路25號
                        <br />
                        電話: 886-2-29069201　傳真: 886-2-29068821
                        <br />
                        Email: pes@ms41.hinet.net
                        <br />
                        Copyright © 2013  奇立實業股份有限公司   All rights reserved.  -隱私權政策
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

