﻿
jQuery.support.cors = true;
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, which) {
            return typeof args[which] != "undefined" ? args[which] : match;
        });
    };
}
function setCSS(cssPath, cssAry) {
    $.each(cssAry, function(idx, cssFile) {
        $("head").append("<link rel='stylesheet' type='text/css' href='{0}{1}.css'>".format(cssPath, cssFile));
    });
}
function loadScripts(jsPath, jsAry, callback) {
    var deferred = $.Deferred();

    function loadScript(jsPath, jsAry, callback, i) {
        $.ajax({
            url: jsPath + jsAry[i] + ".js",
            dataType: "script",
            cache: true,
            success: function() {
                if (i + 1 < jsAry.length) {
                    loadScript(jsPath, jsAry, callback, i + 1);
                } else {
                    if (callback) {
                        callback();
                    }
                    deferred.resolve();
                }
            }
        });
    }
    loadScript(jsPath, jsAry, callback, 0);
    return deferred;
}
function initNotify(container, notifyPnl, pnlWidth, pnlPos, opacify, autoShow, aniShowDelay, autoHide, autohideDelay, pnlTemp) {
    notifyPnl.jqxNotification({
        width: pnlWidth || "auto", 
        position: pnlPos || "bottom-right",
        opacity: opacify || 1,
        autoOpen: autoShow || false,
        animationOpenDelay: aniShowDelay || 999,
        autoClose: autoHide || true,
        autoCloseDelay: autohideDelay || 5000,
        template: pnlTemp || "info",
        appendContainer: container
    });
}
function showMsg(notifyPnl, msgPnl, msg, noClose, msgIco, autohideDelay) {
    if (notify && message) {
        notifyPnl.jqxNotification("closeAll");
        if (noClose) {
            notifyPnl.jqxNotification({
                autoClose: !noClose
            });
        } else {
            notifyPnl.jqxNotification({
                autoClose: true
            });
        }
        if (msgIco) {
            notifyPnl.jqxNotification({
                template: msgIco
            });
        } else {
            notifyPnl.jqxNotification({
                template: "info"
            });
        }
        msgPnl.text(msg);
        if (autohideDelay) {
            notifyPnl.jqxNotification({
                autoCloseDelay: autohideDelay
            });
        }
        notifyPnl.jqxNotification("open");
    } else {
        alert(msg);
    }
}
function invokeAPI(requestUrl, data, timeout, async, method, details, type, contentType) {
    timeout = timeout || 2 * 60;
    details = details || true;
    method = method || "POST";
    async = async || false;
    type = type || "json";
    contentType = contentType || "application/json; charset=utf-8";
    return $.ajax({
        url: requestUrl,
        type: method,
        async: async,
        timeout: timeout * 1E3,
        crossDomain: details,
        cache: false,
        dataType: type,
        contentType: contentType,
        processData: false,
        data: JSON.stringify(data)
    });
}
var cssAry = ["jqx.base", "jqx.classic", "jqx.darkblue", "jqx.energyblue", "jqx.shinyblack", "jqx.web", "jqx.bootstrap", "jqx.orange", "jqx.summer", "jqx.fresh", "jqx.black", "jqx.metro", "jqx.office", "jqx.arctic", "jqx.metrodark", "jqx.highcontrast", "jqx.ui-redmond"];
var jsAry = ["jqxcore", "jqxdata", "jqxresponse", "jqxnotification", "jqxloader", "jqxbuttons", "jqxinput", "jqxpasswordinput", "jqxcheckbox", "jqxlistbox", "jqxcombobox", "jqxscrollbar", "jqxpanel", "jqxresponsivepanel", "jqxmenu", "jqxtabs", "jqxwindow", "jqxdragdrop"];
var win, doc, bod;
var cntr, list, cond, cont;
var loading, notify, message;
var lstSensor, cboSttYea, cboSttMth, cboStpYea, cboStpMth, btnQuery;
var fontFami, fontSize, comboFontSize;
var chart, senData, matrix;
var currTime = new Date();
var currYear = currTime.getFullYear();
var currMonth = currTime.getMonth() + 1;
var chartInit = false;
var elements =
    "        <div id='list'>" +
    "            <div id='lstSensor'></div>" +
    "        </div>" +
    "        <div id='cond'>" +
    "            <div id='cboStartYear'></div>" +
    "            <div id='divStartYear'>年</div>" +
    "            <div id='cboStartMonth'></div>" +
    "            <div id='divStartMonth'>月</div>" +
    "            <div id='divUntil'>~</div>" +
    "            <div id='cboStopYear'></div>" +
    "            <div id='divStopYear'>年</div>" +
    "            <div id='cboStopMonth'></div>" +
    "            <div id='divStopMonth'>月</div>" +
    "            <div id='btnQuery'>" +
    "                <img id='imgQuery' src='images/Query3.png' />" +
    "                <div id='divQuery'>查詢</div>" +
    "            </div>" +
    "        </div>" +
    "        <div id='cont'><div id='chart'></div></div>" +
    "        <div id='loading'></div>" +
    "        <div id='notify'><div id='message'></div></div>";
var yeaArr = ["2018", "2019", "2020"];
var mthArr = [];
for (lp = 1; lp <= 12; lp++) {
    mthArr.push(lp);
}
setCSS("jqwidgets/styles/", cssAry);
setCSS("css/", ["c3.min"]);
$(document).ready(function() {
    var dynaC3 = loadScripts("js/", ["d3.v3.min", "c3.min", "lodash.core", "lodash"], function() {});
    var dynaLoad = loadScripts("jqwidgets/", jsAry, function() {});
    $.when(dynaLoad).done(function() {
        $.jqx.theme = "fresh";
        win = $("window");
        doc = $("document");
        bod = $("body");
        cntr = $("#container");
        cntr.append(elements);
        list = $("#list");
        cond = $("#cond");
        cont = $("#cont");
        loading = $("#loading");
        notify = $("#notify");
        message = $("#message");
        matrix = $("#chart");
        initNotify("", notify);
        loading.jqxLoader({
            text: "<div id='loadSpan'>載入中</div>",
            width: 128,
            height: 66,
            imagePosition: "top",
            autoOpen: true
        });
        fontFami = bod.css("font-family");
        fontSize = bod.css("font-size");
        comboFontSize = $(".jqx-combobox-input").css("font-size");
        lstSensor = $("#lstSensor");
        lstSensor.jqxListBox({
            width: "84%",
            height: "96%",
            allowDrop: true,
            allowDrag: true,
            dragStart: function(item) {
            },
            renderer: function(index, label, value) {
                return label;
            }
        });
        lstSensor.on("dragStart", function(event) {
            console.log("Drag Start: " + event.args.label);
        });
        lstSensor.on("dragEnd", function(event) {
            console.log("Drag End: " + event.args.label);
            if (event.args.label) {
                var ev = event.args.originalEvent;
                var x = ev.pageX;
                var y = ev.pageY;
                if (event.args.originalEvent && event.args.originalEvent.originalEvent && event.args.originalEvent.originalEvent.touches) {
                    var touch = event.args.originalEvent.originalEvent.changedTouches[0];
                    x = touch.pageX;
                    y = touch.pageY;
                }
                var offset = matrix.offset();
                var width = matrix.width();
                var height = matrix.height();
                var right = parseInt(offset.left) + width;
                var bottom = parseInt(offset.top) + height;
                if (x >= parseInt(offset.left) && x <= right) {
                    if (y >= parseInt(offset.top) && y <= bottom) {
                        console.log(event.args.label);
                        draw(event.args.label);
                    }
                }
            }
        });
        function genChart(datAry) {
            chartInit = true;
            chart = c3.generate({
                padding: {
                    top: 0,
                    right: 128,
                    bottom: 0,
                    left: 64
                },
                data: {
                    empty: {
                        label: {
                            text: "無 Sensor 資料數據"
                        }
                    },
                    x: "date",
                    xFormat: "%Y%m%d%H%M%S",
                    columns: datAry
                },
                axis: {
                    x: {
                        type: "timeseries",
                        label: "",
                        tick: {
                            culling: false,
                            format: "%Y/%m/%d %H:%M:%S",
                            rotate: 45,
                            fit: true,
                            multiline: false
                        }
                    },
                    y: {
                        label: "",
                        show: true
                    }
                },
                zoom: {
                    enabled: true
                },
                legend: {
                    show: true,
                    position: "right"
                },
                tooltip: {
                    grouped: true
                },
                point: {
                    show: true
                },
                grid: {
                    y: {
                        show: true
                    }
                }
            });
        }
        function draw(senName) {
            var sdr = senData.filter(function(element, index, array) {
                return element.name === senName;
            });
            var datTim = [];
            var senDatRec = [];
            datTim.push("date");
            senDatRec.push(senName + "　");
            $.each(sdr, function(idx, elem) {
                datTim.push(elem.date);
                senDatRec.push(elem.data);
            });
            var datAry = [datTim, senDatRec];
            if (!chartInit) {
                genChart(datAry);
            } else {
                chart.load({
                    columns: datAry
                });
            }
        }
        // htpp://SQLWEB.aspx?f=1
        var getSensorUrl = "Sensors.json"; 
        invokeAPI(getSensorUrl, null, 60, true, "GET").done(getSensorOK).fail(onError);
        function getSensorOK(result, status, xhr) {
            loading.jqxLoader("close");
            if (xhr.readyState == 4 && status == "success") {
                if (!result) {
                    showMsg(notify, message, "( {0} ) {1}".format(xhr.status, xhr.statusText), true, "error");
                    return;
                }
            } else {
                showMsg(notify, message, "( {0} ) {1}".format(xhr.status, xhr.statusText), true);
                return;
            }
            if (result.length === 0) {
                showMsg(notify, message, "查詢不到 Sensor !");
            } else {
                addSensorToList(result);
            }
        }
        function addSensorToList(sensors) {
            $.each(sensors, function(idx, elem) {
                lstSensor.jqxListBox("addItem", {
                    label: elem.Name,
                    value: elem.ID,
                    disabled: (elem.Status === "0" ? true : false)
                });
            });
            showMsg(notify, message, "Sensor 清單列表完成 !");
        }
        function onError(xhr, status, error) {
            loading.jqxLoader("close");
            if (status == "error") {
                var msg = "( {0} ) {1}".format(xhr.status, xhr.statusText);
                showMsg(notify, message, msg, true, "error");
            } else {
                showMsg(notify, message, status, true, "error");
            }
        }
        cboSttYea = $("#cboStartYear");
        cboSttYea.jqxComboBox({
            source: yeaArr,
            width: "99px",
            height: "26px",
            itemHeight: "26px",
            autoComplete: true,
            searchMode: "containsignorecase",
            animationType: "fade",
            autoDropDownHeight: true,
            enableBrowserBoundsDetection: true,
            autoItemsHeight: true,
            selectionMode: "dropDownList"
        });
        var sttYeaInput = cboSttYea.find("input");
        sttYeaInput.css("font-family", fontFami);
        sttYeaInput.css("font-size", comboFontSize);
        sttYeaInput.css("text-align", "center");
        cboSttYea.val(currYear);
        cboSttMth = $("#cboStartMonth");
        cboSttMth.jqxComboBox({
            source: mthArr,
            width: "99px",
            height: "26px",
            itemHeight: "26px",
            autoComplete: true,
            searchMode: "containsignorecase",
            animationType: "fade",
            autoDropDownHeight: true,
            enableBrowserBoundsDetection: true,
            autoItemsHeight: true,
            selectionMode: "dropDownList"
        });
        var sttMthInput = cboSttMth.find("input");
        sttMthInput.css("font-family", fontFami);
        sttMthInput.css("font-size", comboFontSize);
        sttMthInput.css("text-align", "center");
        cboSttMth.val(currMonth);
        cboStpYea = $("#cboStopYear");
        cboStpYea.jqxComboBox({
            source: yeaArr,
            width: "99px",
            height: "26px",
            itemHeight: "26px",
            autoComplete: true,
            searchMode: "containsignorecase",
            animationType: "fade",
            autoDropDownHeight: true,
            enableBrowserBoundsDetection: true,
            autoItemsHeight: true,
            selectionMode: "dropDownList"
        });
        var StpYeaInput = cboStpYea.find("input");
        StpYeaInput.css("font-family", fontFami);
        StpYeaInput.css("font-size", comboFontSize);
        StpYeaInput.css("text-align", "center");
        cboStpYea.val(currYear);
        cboStpMth = $("#cboStopMonth");
        cboStpMth.jqxComboBox({
            source: mthArr,
            width: "99px",
            height: "26px",
            itemHeight: "26px",
            autoComplete: true,
            searchMode: "containsignorecase",
            animationType: "fade",
            autoDropDownHeight: true,
            enableBrowserBoundsDetection: true,
            autoItemsHeight: true,
            selectionMode: "dropDownList"
        });
        var StpMthInput = cboStpMth.find("input");
        StpMthInput.css("font-family", fontFami);
        StpMthInput.css("font-size", comboFontSize);
        StpMthInput.css("text-align", "center");
        cboStpMth.val(currMonth);
        btnQuery = $("#btnQuery");
        btnQuery.jqxButton({
            width: "109px",
            height: "23px"
        });
        btnQuery.click(function() {
            var getDataUrl;
            // getDataUrl = "htpp://SQLWEB.aspx?f=2&y1={0}&m1={1}&y2={2}&m2={3}";
            // getDataUrl = getDataUrl.format(cboSttYea.val(), cboSttMth.val(), cboStpYea.val(), cboStpMth.val());
            getDataUrl = "Datas.json";
            invokeAPI(getDataUrl, null, 60, true, "GET").done(getDataOK).fail(onError);
        });
        function getDataOK(result, status, xhr) {
            loading.jqxLoader("close");
            if (xhr.readyState == 4 && status == "success") {
                if (!result) {
                    showMsg(notify, message, "( {0} ) {1}".format(xhr.status, xhr.statusText), true, "error");
                    return;
                }
            } else {
                showMsg(notify, message, "( {0} ) {1}".format(xhr.status, xhr.statusText), true);
                return;
            }
            if (result.length === 0) {
                showMsg(notify, message, "查詢不到 Data !");
            } else {
                senData = result;
                chartSpace();
                showMsg(notify, message, "Sensor 數據查詢完成 !");
            }
        }
        function chartSpace() {
            chartInit = false;
            chart = c3.generate({
                padding: {
                    top: 0,
                    right: 100,
                    bottom: 0,
                    left: 100
                },
                data: {
                    empty: {
                        label: {
                            text: "拖曳 Sensor 至此 產生圖表"
                        }
                    },
                    columns: [
                        [null]
                    ]
                },
                axis: {
                    y: {
                        label: "",
                        show: false
                    }
                },
                grid: {
                    y: {
                        show: true
                    }
                }
            });
        }
    });
});
