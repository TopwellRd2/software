Thank you for downloading OPC Expert!  This document helps you get started quickly with OPC Expert.

========================================
Software Description
========================================
OPC Expert is a software application enabling automation personnel to easily view and manipulate OPC information. OPC Expert's powerful diagnostic capabilities, and intuitive user interface, make OPC Expert a unique tool.

OPC Expert is aimed at automation personnel with little to no OPC experience. It automatically diagnoses communication problems and provides actionable suggestions in clear and concise messages. It also ensures communication reliability by automatically changing communication tactics when new network barriers appear. With OPC Expert you have the world's most knowledgeable OPC expert at your side ready to provide helpful advice.

========================================
Included Files
========================================
The following 3 files are included in OpcExpert.zip:

1. ReadMe.txt
This document (which you are currently reading).

2. OPC Expert.exe
This is OPC Expert executable itself.  Unless there is a problem, you can ignore all other files.  Please note OPC Expert does not require an installation and does not change registry settings.

3. OPC Expert Compatability.exe
Run this executable in case you have a problem running OPC Expert.  This application will inform you the reason OPC Expert does not properly run on your computer.

========================================
OPC Expert Free versus OPC Expert Pro
========================================
OPC Expert Free is the free version of OPC Expert Pro.  OPC Expert Free runs for 4 hours at a time.  After this time period, OPC Expert Free will stop.  When you restart OPC Expert Free it will work for another 4-hour time period; you do not have to download OPC Expert Free again.

To upgrade from OPC Expert Free to OPC Expert Pro, simply purchase OPC Expert Pro.  OPCTI will provide you with an Activation key.  Both versions (OPC Expert Free and OPC Expert Pro) use the same files, so once you activate OPC Expert Free, it will immediately become OPC Expert Pro and will no longer have a time limit.  This enables you to use OPC Expert Pro in production since functions such as trending, health monitoring, and archiving must not stop.

========================================
Software Requirements
========================================
OPC Expert requires Microsoft .NET Framework 3.5.  Download this software directly from Microsoft for free at:
http://www.microsoft.com/en-us/download/details.aspx?id=25150

Note: You must have Microsoft .NET Framework 3.5 specifically.  You do not require previous versions of the Framework.  Later versions of Microsoft .NET Framework (such as 4.0 and 4.5) do not include Microsoft .NET Framework 3.5, so you must download from the website above.

========================================
Video tutorials
========================================
OPC Expert's website (see contact information below) provides many of additional resources for you to learn how to use OPC Expert and how to establish OPC communication. You can view various tutorials on how to use OPC Expert and how to troubleshoot and diagnose communication problems.  You can also download many valuable OPC whitepapers and troubleshooting guides at http://www.OPCTI.com.

OPC Training Institute also provides many hands-on OPC training courses.  OPCTI delivers these course in person and online.  OPCTI also customizes OPC training for various companies around the world.  Please contact us if this is of interest to you or your team.

========================================
Distributing OPC Expert
========================================
OPC Expert is an ideal companion application for any vendor who sells OPC applications.  Several vendors include OPC Expert with their software installation because OPC Expert helps users establish OPC connections.  To including OPC Expert with your software distribution, please contact us using the contact information below.

========================================
Contact Information
========================================
If you have comments to make or questions to ask, please contact OPC Training Institute at the following:
Web: http://www.OpcExpert.com
Email: opc.expert@opcti.com
Phone: +1-780-784-4444
Fax: +1-780-784-4445

