﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace topwell_detector
{
    class TCPIPHandler : IConnectHandler
    {
        public int Register { get; private set; }
        public bool IsConnected { get; private set; }
        public int ReadLength { get; private set; }

        private TcpClient m_myTcpClient;
        private NetworkStream m_myNetworkStream;
        private Thread m_thread;

        private FFUModel[] m_FFUModelAry; 

        public TCPIPHandler(FFUModel[] FFUModelAry, int register)
        {
            m_FFUModelAry = FFUModelAry;
            Register = register;
            IsConnected = false;
            ReadLength = 10;
        }

        public void close()
        {
            m_myTcpClient.Close();

            if (m_thread != null)
                m_thread.Abort();

            IsConnected = false;
        }

        public void connect(string ip)
        {
            try {
                m_myTcpClient = new TcpClient(ip, 502);

                if (m_myTcpClient.Connected) {
                    m_myNetworkStream = m_myTcpClient.GetStream();
                    IsConnected = true;

                    startThread();
                }
                else {
                    IsConnected = false;
                }
            }
            catch (Exception ex) {
                IsConnected = false;
            }
        }

        private void startThread()
        {
            m_thread = new Thread(DoReceive);
            m_thread.IsBackground = true;
            m_thread.Start();
        }

        public void DoReceive()
        {
            while (IsConnected)
            {
                for (int i = 0; i < Form1.FFU_COUNT; i++)
                {
                    if (requestaDeviceData(i + 1) == false)
                        requestaDeviceData(i + 1); //多做一次 避免沒更新
                    Thread.Sleep(40);
                }
                Thread.Sleep(500);
            } 
        }

        private bool requestaDeviceData(int id)
        {
            byte[] inputByte = new byte[12];
            byte[] outputByte = new byte[29];
            int startAddress = id * ReadLength;

            inputByte[0] = 0;    //Tcp Header
            inputByte[1] = 2;    //Tcp Header
            inputByte[2] = 0;    //Tcp Header
            inputByte[3] = 0;    //Tcp Header
            inputByte[4] = 0;    //Tcp Header
            inputByte[5] = 6;    //Tcp Header

            inputByte[6] = Convert.ToByte(Register);    //Tcp Header   

            inputByte[7] = 3;    //Function Code

            byte[] _adr = BitConverter.GetBytes(startAddress);
            inputByte[8] = _adr[1];    
            inputByte[9] = _adr[0];    

            byte[] _length = BitConverter.GetBytes(ReadLength);
            inputByte[10] = _length[1];    
            inputByte[11] = _length[0];   

            try
            {
                m_myNetworkStream.Write(inputByte, 0, inputByte.Length);
                m_myNetworkStream.Read(outputByte, 0, outputByte.Length);
            }
            catch(ThreadAbortException e)
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace.ToString());
                IsConnected = false;
                return false;
            }

            if (outputByte[0] != 0 || outputByte[1] != 2)
            {
                return false;
            }
            else
            {
                int[] data = new int[10];

                for (int i = 0; i < ReadLength; i++)
                {
                    data[i] = outputByte[9 + (2 * i)];
                    data[i] = (data[i] << 8) + outputByte[10 + (2 * i)];
                }

                m_FFUModelAry[id - 1].Status = data[0];
                m_FFUModelAry[id - 1].SettingSpeed = data[1];
                m_FFUModelAry[id - 1].NowSpeed = data[4];
                m_FFUModelAry[id - 1].Alarm = data[3];
            }
            return true;
        }

        public bool writeStatus(int id, uint val)
        {
            beforeWriteAction();
            bool ret = writeData(id, 0, (int)val);
            startThread();
            return ret;
        }

        public bool writeSettingSpeed(int id, uint val)
        {
            beforeWriteAction();
            bool ret = writeData(id, 1, (int)val);
            startThread();
            return ret;    
        }

        public void OtkWriteStatus(uint val)
        {
            beforeWriteAction();
            for (int id = 1; id < 64; id++)
            {
                writeData(id, 0, (int)val);
                Thread.Sleep(10);
            }
            MessageBox.Show("OK");
            startThread();
        }

        public void OtkWriteSettingSpeed(uint val)
        {
            beforeWriteAction();
            for (int id = 1; id < 64; id++)
            {
                writeData(id, 1, (int)val);
                Thread.Sleep(20);
            }
            MessageBox.Show("OK");
            startThread();
        }

        private void beforeWriteAction()
        {
            m_thread.Abort();

            m_myNetworkStream.ReadTimeout = 1000;
            try { m_myNetworkStream.Read(new byte[30], 0, 30); }
            catch (Exception ex) { }
        }

        private bool writeData(int id, int pos, int value)
        {
            byte[] inputByte = new byte[12];
            byte[] outputByte = new byte[12];
            int startAddress = id * ReadLength + pos;

            inputByte[0] = 0;    //Tcp Header
            inputByte[1] = 2;    //Tcp Header
            inputByte[2] = 0;    //Tcp Header
            inputByte[3] = 0;    //Tcp Header
            inputByte[4] = 0;    //Tcp Header
            inputByte[5] = 6;    //Tcp Header

            inputByte[6] = Convert.ToByte(Register);  

            inputByte[7] = 6;    //Function Code  6:寫入

            byte[] _adr = BitConverter.GetBytes(startAddress);
            inputByte[8] = _adr[1];    
            inputByte[9] = _adr[0];    

            byte[] _length = BitConverter.GetBytes(value);
            inputByte[10] = _length[1];   
            inputByte[11] = _length[0];    

            try
            {
                m_myNetworkStream.Write(inputByte, 0, inputByte.Length);
                Thread.Sleep(20);
                m_myNetworkStream.Read(outputByte, 0, outputByte.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                IsConnected = false;
                return false;
            }

            //確認寫入是否成功  (Slave會 Echo相同訊息  表示寫入成功)
            for (int i = 0; i < 12; i++)
            {
                if (inputByte[i] != outputByte[i])
                    return false;
            }
            return true;
        }

    }
}
