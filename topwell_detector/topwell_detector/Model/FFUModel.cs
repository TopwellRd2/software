﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_detector
{
    class FFUModel
    {
        public int Status { get; set; }
        public int SettingSpeed { get; set; }
        public int NowSpeed { get; set; }
        public int Alarm { get; set; }

        public int ErrorReadCount { get; set; }

        public FFUModel()
        {
            ErrorReadCount = 0;
        }
    }
}
