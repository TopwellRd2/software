﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_detector
{
    public partial class Form_Simple : Form
    {
        public Label[] IDLabelAry = new Label[Form1.FFU_COUNT];

        public Form_Simple()
        {
            
            InitializeComponent();

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == 3 && j == 15) continue;

                    Label label = new Label();
                    label.Font = new System.Drawing.Font("", 9);
                    label.Size = new System.Drawing.Size(50, 20);
                    label.Text = "ID : " + ((i * 16) + j + 1).ToString();
                    label.Location = new Point(30 + (i * 70), 40 + j * 25);

                    IDLabelAry[(i * 16) + j] = label;

                    panel1.Controls.Add(label);
                }
            }
        }

        private void Form_Simple_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true; //關閉視窗時取消
            this.Hide(); //隱藏式窗,下次再show出
        }
    }
}
