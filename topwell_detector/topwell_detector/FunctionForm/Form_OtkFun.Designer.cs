﻿namespace topwell_detector
{
    partial class Form_OtkFun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SpeedBtn = new System.Windows.Forms.Button();
            this.SpeedTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.StatusBtn = new System.Windows.Forms.Button();
            this.StatusTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(26, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "設定轉速 : ";
            // 
            // SpeedBtn
            // 
            this.SpeedBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SpeedBtn.Location = new System.Drawing.Point(294, 129);
            this.SpeedBtn.Name = "SpeedBtn";
            this.SpeedBtn.Size = new System.Drawing.Size(78, 46);
            this.SpeedBtn.TabIndex = 4;
            this.SpeedBtn.Text = "確定";
            this.SpeedBtn.UseVisualStyleBackColor = true;
            // 
            // SpeedTxt
            // 
            this.SpeedTxt.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SpeedTxt.Location = new System.Drawing.Point(145, 134);
            this.SpeedTxt.Name = "SpeedTxt";
            this.SpeedTxt.Size = new System.Drawing.Size(126, 31);
            this.SpeedTxt.TabIndex = 3;
            this.SpeedTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(27, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "設定開關 : ";
            // 
            // StatusBtn
            // 
            this.StatusBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.StatusBtn.Location = new System.Drawing.Point(294, 48);
            this.StatusBtn.Name = "StatusBtn";
            this.StatusBtn.Size = new System.Drawing.Size(78, 46);
            this.StatusBtn.TabIndex = 7;
            this.StatusBtn.Text = "確定";
            this.StatusBtn.UseVisualStyleBackColor = true;
            // 
            // StatusTxt
            // 
            this.StatusTxt.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.StatusTxt.Location = new System.Drawing.Point(145, 53);
            this.StatusTxt.Name = "StatusTxt";
            this.StatusTxt.Size = new System.Drawing.Size(126, 31);
            this.StatusTxt.TabIndex = 6;
            this.StatusTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "寫入 ID 1~64號";
            // 
            // Form_OtkFun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 189);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.StatusBtn);
            this.Controls.Add(this.StatusTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SpeedBtn);
            this.Controls.Add(this.SpeedTxt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_OtkFun";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "群控";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button SpeedBtn;
        public System.Windows.Forms.TextBox SpeedTxt;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button StatusBtn;
        public System.Windows.Forms.TextBox StatusTxt;
        private System.Windows.Forms.Label label3;
    }
}