﻿namespace topwell_detector
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.ConnectTypeBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.LockBtn = new System.Windows.Forms.ToolStripButton();
            this.ResearchSerialPortBtn = new System.Windows.Forms.ToolStripButton();
            this.SerialPortCombox = new System.Windows.Forms.ToolStripComboBox();
            this.SerialPortConnectBtn = new System.Windows.Forms.ToolStripButton();
            this.SerialPortToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.SerialConnectStateLable = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.CircuitBox = new System.Windows.Forms.ToolStripComboBox();
            this.TcpIpConnectBtn = new System.Windows.Forms.ToolStripButton();
            this.TCPIPToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.IpTxt = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.TCPConnectStateLable = new System.Windows.Forms.ToolStripLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SimpleFormBtn = new System.Windows.Forms.Button();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.OtkBtn = new System.Windows.Forms.Button();
            this.toolStrip1.SuspendLayout();
            this.SerialPortToolStrip.SuspendLayout();
            this.TCPIPToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(127, 36);
            this.toolStripLabel1.Text = "連接方式 : ";
            // 
            // ConnectTypeBox
            // 
            this.ConnectTypeBox.DropDownHeight = 80;
            this.ConnectTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConnectTypeBox.DropDownWidth = 110;
            this.ConnectTypeBox.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ConnectTypeBox.IntegralHeight = false;
            this.ConnectTypeBox.Name = "ConnectTypeBox";
            this.ConnectTypeBox.Size = new System.Drawing.Size(150, 39);
            this.ConnectTypeBox.SelectedIndexChanged += new System.EventHandler(this.ConnectTypeBox_SelectedIndexChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.ConnectTypeBox,
            this.toolStripSeparator1,
            this.LockBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1019, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // LockBtn
            // 
            this.LockBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LockBtn.Image = ((System.Drawing.Image)(resources.GetObject("LockBtn.Image")));
            this.LockBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LockBtn.Name = "LockBtn";
            this.LockBtn.Size = new System.Drawing.Size(36, 36);
            this.LockBtn.Text = "toolStripButton1";
            this.LockBtn.Click += new System.EventHandler(this.LockBtn_Click);
            // 
            // ResearchSerialPortBtn
            // 
            this.ResearchSerialPortBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ResearchSerialPortBtn.Image = ((System.Drawing.Image)(resources.GetObject("ResearchSerialPortBtn.Image")));
            this.ResearchSerialPortBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ResearchSerialPortBtn.Name = "ResearchSerialPortBtn";
            this.ResearchSerialPortBtn.Size = new System.Drawing.Size(36, 36);
            this.ResearchSerialPortBtn.Text = "搜尋 COM";
            this.ResearchSerialPortBtn.Click += new System.EventHandler(this.ResearchSerialPortBtn_Click);
            // 
            // SerialPortCombox
            // 
            this.SerialPortCombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SerialPortCombox.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SerialPortCombox.Name = "SerialPortCombox";
            this.SerialPortCombox.Size = new System.Drawing.Size(121, 39);
            // 
            // SerialPortConnectBtn
            // 
            this.SerialPortConnectBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SerialPortConnectBtn.Image = ((System.Drawing.Image)(resources.GetObject("SerialPortConnectBtn.Image")));
            this.SerialPortConnectBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SerialPortConnectBtn.Name = "SerialPortConnectBtn";
            this.SerialPortConnectBtn.Size = new System.Drawing.Size(36, 36);
            this.SerialPortConnectBtn.Text = "連線";
            this.SerialPortConnectBtn.Click += new System.EventHandler(this.SerialPortConnectBtn_Click);
            // 
            // SerialPortToolStrip
            // 
            this.SerialPortToolStrip.Font = new System.Drawing.Font("Microsoft JhengHei UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SerialPortToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.SerialPortToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResearchSerialPortBtn,
            this.SerialPortCombox,
            this.toolStripLabel4,
            this.SerialPortConnectBtn,
            this.SerialConnectStateLable});
            this.SerialPortToolStrip.Location = new System.Drawing.Point(0, 39);
            this.SerialPortToolStrip.Name = "SerialPortToolStrip";
            this.SerialPortToolStrip.Size = new System.Drawing.Size(1019, 39);
            this.SerialPortToolStrip.TabIndex = 0;
            this.SerialPortToolStrip.Text = "toolStrip2";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(65, 36);
            this.toolStripLabel4.Text = "           ";
            // 
            // SerialConnectStateLable
            // 
            this.SerialConnectStateLable.ForeColor = System.Drawing.Color.Red;
            this.SerialConnectStateLable.Name = "SerialConnectStateLable";
            this.SerialConnectStateLable.Size = new System.Drawing.Size(67, 36);
            this.SerialConnectStateLable.Text = "未連接";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(57, 36);
            this.toolStripLabel2.Text = "迴路 :";
            // 
            // CircuitBox
            // 
            this.CircuitBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CircuitBox.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.CircuitBox.Name = "CircuitBox";
            this.CircuitBox.Size = new System.Drawing.Size(121, 39);
            // 
            // TcpIpConnectBtn
            // 
            this.TcpIpConnectBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TcpIpConnectBtn.Image = ((System.Drawing.Image)(resources.GetObject("TcpIpConnectBtn.Image")));
            this.TcpIpConnectBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TcpIpConnectBtn.Name = "TcpIpConnectBtn";
            this.TcpIpConnectBtn.Size = new System.Drawing.Size(36, 36);
            this.TcpIpConnectBtn.Text = "連線";
            this.TcpIpConnectBtn.Click += new System.EventHandler(this.TcpIpConnectBtn_Click);
            // 
            // TCPIPToolStrip
            // 
            this.TCPIPToolStrip.Font = new System.Drawing.Font("Microsoft JhengHei UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TCPIPToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.TCPIPToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.IpTxt,
            this.toolStripLabel2,
            this.CircuitBox,
            this.toolStripLabel5,
            this.TcpIpConnectBtn,
            this.TCPConnectStateLable});
            this.TCPIPToolStrip.Location = new System.Drawing.Point(0, 78);
            this.TCPIPToolStrip.Name = "TCPIPToolStrip";
            this.TCPIPToolStrip.Size = new System.Drawing.Size(1019, 39);
            this.TCPIPToolStrip.TabIndex = 1;
            this.TCPIPToolStrip.Text = "toolStrip2";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(40, 36);
            this.toolStripLabel3.Text = "IP : ";
            // 
            // IpTxt
            // 
            this.IpTxt.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.IpTxt.Name = "IpTxt";
            this.IpTxt.Size = new System.Drawing.Size(170, 39);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(65, 36);
            this.toolStripLabel5.Text = "           ";
            // 
            // TCPConnectStateLable
            // 
            this.TCPConnectStateLable.ForeColor = System.Drawing.Color.Red;
            this.TCPConnectStateLable.Name = "TCPConnectStateLable";
            this.TCPConnectStateLable.Size = new System.Drawing.Size(67, 36);
            this.TCPConnectStateLable.Text = "未連接";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(7, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 1249);
            this.panel1.TabIndex = 2;
            // 
            // SimpleFormBtn
            // 
            this.SimpleFormBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SimpleFormBtn.Location = new System.Drawing.Point(480, 4);
            this.SimpleFormBtn.Name = "SimpleFormBtn";
            this.SimpleFormBtn.Size = new System.Drawing.Size(146, 36);
            this.SimpleFormBtn.TabIndex = 0;
            this.SimpleFormBtn.Text = "精簡版 Form";
            this.SimpleFormBtn.UseVisualStyleBackColor = true;
            this.SimpleFormBtn.Click += new System.EventHandler(this.SimpleFormBtn_Click);
            // 
            // updateTimer
            // 
            this.updateTimer.Interval = 400;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // OtkBtn
            // 
            this.OtkBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.OtkBtn.Location = new System.Drawing.Point(642, 3);
            this.OtkBtn.Name = "OtkBtn";
            this.OtkBtn.Size = new System.Drawing.Size(88, 38);
            this.OtkBtn.TabIndex = 3;
            this.OtkBtn.Text = "群控";
            this.OtkBtn.UseVisualStyleBackColor = true;
            this.OtkBtn.Click += new System.EventHandler(this.OtkBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1040, 1055);
            this.Controls.Add(this.OtkBtn);
            this.Controls.Add(this.SimpleFormBtn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TCPIPToolStrip);
            this.Controls.Add(this.SerialPortToolStrip);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FFU Status";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.SerialPortToolStrip.ResumeLayout(false);
            this.SerialPortToolStrip.PerformLayout();
            this.TCPIPToolStrip.ResumeLayout(false);
            this.TCPIPToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox ConnectTypeBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ResearchSerialPortBtn;
        private System.Windows.Forms.ToolStripComboBox SerialPortCombox;
        private System.Windows.Forms.ToolStripButton SerialPortConnectBtn;
        private System.Windows.Forms.ToolStrip SerialPortToolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox CircuitBox;
        private System.Windows.Forms.ToolStripButton TcpIpConnectBtn;
        private System.Windows.Forms.ToolStrip TCPIPToolStrip;
        private System.Windows.Forms.ToolStripLabel SerialConnectStateLable;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.ToolStripLabel TCPConnectStateLable;
        private System.Windows.Forms.ToolStripTextBox IpTxt;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.Button SimpleFormBtn;
        private System.Windows.Forms.ToolStripButton LockBtn;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.Button OtkBtn;

    }
}

