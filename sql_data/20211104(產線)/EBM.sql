USE [master]
GO
/****** Object:  Database [EBM]    Script Date: 2021/11/04 16:40:13 ******/
CREATE DATABASE [EBM]
ALTER DATABASE [EBM] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EBM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EBM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EBM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EBM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EBM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EBM] SET ARITHABORT OFF 
GO
ALTER DATABASE [EBM] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [EBM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EBM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EBM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EBM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EBM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EBM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EBM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EBM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EBM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EBM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EBM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EBM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EBM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EBM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EBM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EBM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EBM] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EBM] SET  MULTI_USER 
GO
ALTER DATABASE [EBM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EBM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EBM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EBM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EBM] SET DELAYED_DURABILITY = DISABLED 
GO
USE [EBM]
GO
/****** Object:  Table [dbo].[EBM]    Script Date: 2021/11/04 16:40:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EBM](
	[日期] [nvarchar](50) NULL,
	[時間] [nvarchar](50) NULL,
	[產品代碼] [nvarchar](50) NULL,
	[箱體編號] [nvarchar](50) NULL,
	[馬達編號] [nvarchar](50) NULL,
	[控制器編號] [nvarchar](50) NULL,
	[出廠電壓] [real] NULL,
	[出廠電流] [real] NULL,
	[出廠功率] [real] NULL,
	[出廠震動] [real] NULL,
	[出廠轉速] [real] NULL,
	[出廠頻率] [real] NULL,
	[最高電壓] [real] NULL,
	[最高電流] [real] NULL,
	[最高功率] [real] NULL,
	[最高震動] [real] NULL,
	[最高轉速] [real] NULL,
	[外觀檢測] [nvarchar](50) NULL,
	[運轉異音] [nvarchar](50) NULL,
	[使用者] [nchar](10) NULL,
	[功率因素] [real] NULL,
	[DCBUS] [real] NULL,
	[警報] [real] NULL,
	[備註] [nchar](10) NULL
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [EBM] SET  READ_WRITE 
GO
