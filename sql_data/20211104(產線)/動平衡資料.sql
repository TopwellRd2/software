USE [master]
GO
/****** Object:  Database [動平衡資料]    Script Date: 2021/11/22 10:54:59 ******/
CREATE DATABASE [動平衡資料]
ALTER DATABASE [動平衡資料] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [動平衡資料].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [動平衡資料] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [動平衡資料] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [動平衡資料] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [動平衡資料] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [動平衡資料] SET ARITHABORT OFF 
GO
ALTER DATABASE [動平衡資料] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [動平衡資料] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [動平衡資料] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [動平衡資料] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [動平衡資料] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [動平衡資料] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [動平衡資料] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [動平衡資料] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [動平衡資料] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [動平衡資料] SET  DISABLE_BROKER 
GO
ALTER DATABASE [動平衡資料] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [動平衡資料] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [動平衡資料] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [動平衡資料] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [動平衡資料] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [動平衡資料] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [動平衡資料] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [動平衡資料] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [動平衡資料] SET  MULTI_USER 
GO
ALTER DATABASE [動平衡資料] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [動平衡資料] SET DB_CHAINING OFF 
GO
ALTER DATABASE [動平衡資料] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [動平衡資料] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [動平衡資料] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [動平衡資料] SET QUERY_STORE = OFF
GO
USE [動平衡資料]
GO
/****** Object:  Table [dbo].[COMI]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMI](
	[CiNo] [int] NOT NULL,
	[CiName] [nvarchar](30) NULL,
	[CiMode] [int] NULL,
	[CiComport] [int] NULL,
	[CiBaudRate] [int] NULL,
	[CiDataBits] [int] NULL,
	[CiStopBits] [int] NULL,
	[CiTelNo] [nvarchar](20) NULL,
	[CiTelDelay] [int] NULL,
	[CiSendIP] [nvarchar](30) NULL,
	[CiSendPort] [int] NULL,
	[CiRecvPort] [int] NULL,
	[CiTimeOut] [int] NULL,
 CONSTRAINT [COMI_PX] PRIMARY KEY CLUSTERED 
(
	[CiNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Groups](
	[GroupNo] [int] NOT NULL,
	[GroupName] [nvarchar](50) NULL,
	[Note] [nvarchar](20) NULL,
 CONSTRAINT [Groups_PX] PRIMARY KEY CLUSTERED 
(
	[GroupNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryBak]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryBak](
	[HistoryNo] [int] NOT NULL,
	[RecvDateTime] [date] NULL,
	[CardNo] [nvarchar](10) NULL,
	[ReaderNo] [int] NULL,
	[LogDate] [nvarchar](8) NULL,
	[LogTime] [nvarchar](8) NULL,
	[State] [int] NULL,
 CONSTRAINT [HistoryBak_PX] PRIMARY KEY CLUSTERED 
(
	[HistoryNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryErr]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryErr](
	[HistoryNo] [int] NOT NULL,
	[ReaderNo] [int] NULL,
	[RecvDateTime] [date] NULL,
	[HistoryData] [nvarchar](255) NULL,
 CONSTRAINT [HistoryErr_PX] PRIMARY KEY CLUSTERED 
(
	[HistoryNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryNew]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryNew](
	[HistoryNo] [int] NOT NULL,
	[RecvDateTime] [date] NULL,
	[CardNo] [nvarchar](10) NULL,
	[ReaderNo] [int] NULL,
	[LogDate] [nvarchar](8) NULL,
	[LogTime] [nvarchar](6) NULL,
	[State] [int] NULL,
 CONSTRAINT [HistoryNew_PX] PRIMARY KEY CLUSTERED 
(
	[HistoryNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Model]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Model](
	[RmNo] [int] NULL,
	[ReaderModel] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permit]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permit](
	[GroupNo] [int] NOT NULL,
	[ReaderNo] [int] NOT NULL,
	[TimeZoneNo] [int] NULL,
 CONSTRAINT [Permit_PX] PRIMARY KEY CLUSTERED 
(
	[GroupNo] ASC,
	[ReaderNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermitDataLast]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermitDataLast](
	[ReaderNo] [int] NOT NULL,
	[CardNo] [nvarchar](10) NOT NULL,
	[MarkText] [nvarchar](200) NULL,
 CONSTRAINT [PermitDataLast_PX] PRIMARY KEY CLUSTERED 
(
	[ReaderNo] ASC,
	[CardNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermitDataNew]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermitDataNew](
	[ReaderNo] [int] NOT NULL,
	[CardNo] [nvarchar](10) NOT NULL,
	[MarkText] [nvarchar](200) NULL,
 CONSTRAINT [PermitDataNew_PX] PRIMARY KEY CLUSTERED 
(
	[ReaderNo] ASC,
	[CardNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RawData]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RawData](
	[案號] [varchar](50) NOT NULL,
	[Device_SN] [varchar](50) NULL,
	[日期] [date] NULL,
	[時間] [datetime] NULL,
	[轉速] [real] NULL,
	[左不平衡量] [float] NULL,
	[左不平衡角度] [varchar](15) NULL,
	[右不平衡量] [float] NULL,
	[右不平衡角度] [varchar](15) NULL,
	[左面結果] [varchar](10) NULL,
	[右面結果] [varchar](10) NULL,
	[使用者] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reader]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reader](
	[ReaderNo] [int] NOT NULL,
	[ReaderName] [nvarchar](50) NULL,
	[Note] [nvarchar](20) NULL,
	[CiNo] [int] NULL,
	[Act] [bit] NOT NULL,
	[RMNo] [int] NULL,
	[Msg] [ntext] NULL,
 CONSTRAINT [Reader_PX] PRIMARY KEY CLUSTERED 
(
	[ReaderNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[Run] [bit] NOT NULL,
	[RunTime] [nvarchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[System]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System](
	[MaxLines] [int] NULL,
	[AutoConvert] [bit] NOT NULL,
	[Retry] [int] NULL,
	[RetryDelay] [int] NULL,
	[RecviTime] [int] NULL,
	[RecvMode] [int] NULL,
	[ScheduleDate] [nvarchar](32) NULL,
	[RecvDate] [bit] NOT NULL,
	[CvtDir] [nvarchar](200) NULL,
	[CvtFileName1] [nvarchar](200) NULL,
	[CvtFileName2] [nvarchar](200) NULL,
	[CvtFileNameType] [int] NULL,
	[CvtSaveMode] [int] NULL,
	[CvtMode] [int] NULL,
	[CvtNormalIdx] [nvarchar](9) NULL,
	[Password] [nvarchar](20) NULL,
	[CvtCallEXE] [int] NULL,
	[ResetTime] [bit] NOT NULL,
	[AutoReData] [bit] NOT NULL,
	[SaveMessage] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2021/11/22 10:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[CardNo] [nvarchar](10) NOT NULL,
	[UserName] [nvarchar](20) NULL,
	[Note] [nvarchar](20) NULL,
	[GroupNo] [int] NULL,
	[FingerCount] [int] NULL,
 CONSTRAINT [Users_PX] PRIMARY KEY CLUSTERED 
(
	[CardNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Users]    Script Date: 2021/11/22 10:54:59 ******/
CREATE NONCLUSTERED INDEX [Users] ON [dbo].[Users]
(
	[CardNo] ASC,
	[GroupNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [動平衡資料] SET  READ_WRITE 
GO
