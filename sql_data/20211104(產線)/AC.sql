USE [master]
GO
/****** Object:  Database [AC]    Script Date: 2021/11/04 16:38:43 ******/
CREATE DATABASE [AC]
ALTER DATABASE [AC] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AC] SET ARITHABORT OFF 
GO
ALTER DATABASE [AC] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [AC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AC] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AC] SET  MULTI_USER 
GO
ALTER DATABASE [AC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AC] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [AC] SET DELAYED_DURABILITY = DISABLED 
GO
USE [AC]
GO
/****** Object:  Table [dbo].[AC]    Script Date: 2021/11/04 16:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AC](
	[日期] [nvarchar](30) NULL,
	[時間] [nvarchar](30) NULL,
	[產品代碼] [nvarchar](30) NULL,
	[箱體編號] [nvarchar](30) NULL,
	[馬達編號] [nvarchar](30) NULL,
	[控制器編號] [nvarchar](30) NULL,
	[一速電壓] [real] NULL,
	[一速電流] [real] NULL,
	[一速功率] [real] NULL,
	[一速保護電流] [real] NULL,
	[一速震動] [real] NULL,
	[二速電壓] [real] NULL,
	[二速電流] [real] NULL,
	[二速功率] [real] NULL,
	[二速保護電流] [real] NULL,
	[二速震動] [real] NULL,
	[三速電壓] [real] NULL,
	[三速電流] [real] NULL,
	[三速功率] [real] NULL,
	[三速保護電流] [real] NULL,
	[三速震動] [real] NULL,
	[四速電壓] [real] NULL,
	[四速電流] [real] NULL,
	[四速功率] [real] NULL,
	[四速保護電流] [real] NULL,
	[四速震動] [real] NULL,
	[外觀檢測] [nvarchar](50) NULL,
	[運轉異音] [nvarchar](50) NULL,
	[備註] [nchar](10) NULL
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [AC] SET  READ_WRITE 
GO
