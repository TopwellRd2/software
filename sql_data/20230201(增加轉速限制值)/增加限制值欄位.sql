ALTER TABLE [DC].[dbo].[DC] ADD
[最低電壓] REAL ,
[最低電流] REAL ,
[最低功率] REAL ,
[最低震動] REAL ,
[最低轉速] REAL ,
[最低轉速電壓容許最大值] REAL ,
[最低轉速電壓容許最小值] REAL ,
[最高轉速電壓容許最大值] REAL ,
[最高轉速電壓容許最小值] REAL ,
[出廠轉速電壓容許最大值] REAL ,
[出廠轉速電壓容許最小值] REAL ,
[最低轉速電流容許最大值] REAL ,
[最低轉速電流容許最小值] REAL ,
[最高轉速電流容許最大值] REAL ,
[最高轉速電流容許最小值] REAL ,
[出廠轉速電流容許最大值] REAL ,
[出廠轉速電流容許最小值] REAL ,
[最低轉速功率容許最大值] REAL ,
[最低轉速功率容許最小值] REAL ,
[最高轉速功率容許最大值] REAL ,
[最高轉速功率容許最小值] REAL ,
[出廠轉速功率容許最大值] REAL ,
[出廠轉速功率容許最小值] REAL ,
[最低功率因素] REAL ,
[最低頻率] REAL ,
[最高功率因素] REAL ,
[最高頻率] REAL ;