﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;

namespace aluminum_supplier_monitor.command
{
    public struct SettingSpeedparameter
    {
        public int id;
        public TextBox txt;
    }

    class WriteSettingSpeedCommand : ICommand 
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object _parameter)
        {
            if (_parameter is SettingSpeedparameter)
            {
                var parameter = (SettingSpeedparameter)_parameter;

                try
                {
                    int value = Convert.ToInt32(parameter.txt.Text);

                    int maximum = 1300;
                    int minimum = 600;

                    if (parameter.id == 1 || parameter.id == 2)
                        minimum = 700;
                    
                    if (value < minimum || value > maximum)
                    {
                        MessageBox.Show($"設定轉速不能小於{minimum} or 大於 {maximum}", "警告!!!" , MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else 
                    {
                        SingletonModbusClient.GetInstance().WriteSingleRegister((parameter.id * 10) + 1, value);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            } 
        }

        public event EventHandler CanExecuteChanged;
    }
}
