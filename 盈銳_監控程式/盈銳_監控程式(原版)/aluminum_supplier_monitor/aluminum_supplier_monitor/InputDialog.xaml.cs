﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

using aluminum_supplier_monitor.command;

namespace aluminum_supplier_monitor
{
    /// <summary>
    /// InputDialog.xaml 的互動邏輯
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog()
        {
            InitializeComponent();

            ButtonEnter.Command = new WriteSettingSpeedCommand();
        }

        public void setSettingSpeed(FFUModel _model)
        {
            var parameter = new SettingSpeedparameter();
            parameter.id = _model.Id;
            parameter.txt = SettingSpeedTxt;

            ButtonEnter.CommandParameter = parameter;
            
            SettingSpeedTxt.Text = _model.SettingSpeed.ToString().Split(' ')[0];
        }

        private void ButtonEnter_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void SettingSpeedTxt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }
    }
}
