﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media;
using EasyModbus;

namespace aluminum_supplier_monitor
{
    public class FFUModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Property

        public string Number { get; set; } = "";

        public int Id { get; set; } = 0;

        private bool _operation = false;
        public bool Operation
        {
            get { return _operation; }
            set { }
        }
        public string SettingSpeed { get; private set; }

        public string ActualSpeed { get; private set; } 

        public int AlarmCode { get; private set; } 

        public Brush AlarmCodeColor { get; private set; } 

        public string Current { get; private set; }

        public string Pressure { get; private set; }

        #endregion

        public FFUModel(string _number, int _id)
        {
            Number = _number;
            Id = _id;

            init();
        }

        public void init()
        {
            _operation = false;
            OnPropertyChanged(nameof(Operation));
            SettingSpeed = "0 RPM";
            ActualSpeed = "0 RPM";
            AlarmCode = 255;
            Current = "0 A";
            Pressure = "0 PA";
            AlarmCodeColor = new SolidColorBrush(Color.FromRgb(128, 128, 128));
        }

        public bool setData(int[] data)
        {
            if (data.Length != 10)
                return false;
         
            _operation = data[0] == 1 ? true : false;
            SettingSpeed = data[1].ToString() + " RPM";
            AlarmCode = data[3];
            ActualSpeed = data[4].ToString() + " RPM";
            Current = ((double)data[2] / (double)100).ToString() + " A";
            Pressure = data[8].ToString() + " PA";

            if (AlarmCode > 0)
                AlarmCodeColor = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            else
                AlarmCodeColor = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            OnPropertyChanged(nameof(Operation));

            return true;
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
