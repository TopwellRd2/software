﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using EasyModbus;

using aluminum_supplier_monitor.command;

namespace aluminum_supplier_monitor
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Property
        private const int FFU_TOTAL = 10;

        private DispatcherTimer timer = new DispatcherTimer();

        private ModbusClient modbusClient;

        private Dictionary<int, FFUModel> FFUModelMap { get; set; } = new Dictionary<int, FFUModel>();

        private FFUModel CurrentFFUModel = null;//被點選的model

        private InputDialog dialog = new InputDialog();
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            modbusClient = SingletonModbusClient.GetInstance();

            IpLabel.Content = Info.IP;

            #region Create FFUModel and add it to FFUMap
            FFUModelMap.Add(1 , new FFUModel("358", 1 ));
            FFUModelMap.Add(2 , new FFUModel("258", 2 ));
            FFUModelMap.Add(3 , new FFUModel("359", 3 ));
            FFUModelMap.Add(4 , new FFUModel("259", 4 ));
            FFUModelMap.Add(5 , new FFUModel("360", 5 ));
            FFUModelMap.Add(6 , new FFUModel("260", 6 ));
            FFUModelMap.Add(7 , new FFUModel("361", 7 ));
            FFUModelMap.Add(8 , new FFUModel("261", 8 ));
            FFUModelMap.Add(9 , new FFUModel("362", 9 ));
            FFUModelMap.Add(10, new FFUModel("262", 10));
            #endregion

            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var toggleBtn = (ToggleButton)this.FindName("Operation_" + j.ToString());

                toggleBtn.Command = new WriteOperationCommand();
                toggleBtn.CommandParameter = FFUModelMap[j];
            }
                
            DataBindSetting();

         
            ModbusConnect();

            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = TimeSpan.FromSeconds(5);
            timer.Start();
        }

        private async void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();

            //Update word , let user know the program whether run 
            //UpdateLabel.Content = "更新中...";
            //Task.Run(() => { MessageBox.Show(Thread.CurrentThread.ManagedThreadId.ToString()); Thread.Sleep(2000); });

            int[] datas = await Task.Run<int[]>(() => { return OnTimeAction(); });

            if (datas == null)
            {
                foreach (var ffuModel in FFUModelMap)
                    ffuModel.Value.init();
            }
            else
            {
                for (int i = 1; i <= FFU_TOTAL; ++i)
                {
                    int[] ffuDatas = new int[10];
                    Array.Copy(datas, (i - 1) * 10, ffuDatas, 0, 10);
                
                    FFUModelMap[i].setData(ffuDatas);
                }
            }
            
            //Update connect state if not iplabel turn red else green
            if (modbusClient.Connected == false)
                IpLabel.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            else
                IpLabel.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            timer.Start();
        }

        private int[] OnTimeAction()
            {
            if (modbusClient.Connected == false)
            {
                ModbusConnect();
                return null;
            }
            else
            {
                try
                {
                    int[] datas = modbusClient.ReadHoldingRegisters(10, 100);

                    return datas;
                }
                catch (Exception ex)
                {
                    modbusClient.Disconnect();
                    //MessageBox.Show(ex.ToString());
                    return null;
                }
            }
        }

        #region Modbus Connect

        private void ModbusConnect()
        {
            try
            {
                modbusClient.Connect();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }
        #endregion

        #region DataBindSetting
        /// <summary>
        /// Setting DataBind
        /// </summary>
        private void DataBindSetting()
        {
            string[] names = new string[] { "Number", "SettingSpeed", "ActualSpeed", "AlarmCode", "Current", "Pressure" };
            
            for (int i = 0; i < names.Length; ++i)
            {
                for (int j = 1; j <= FFU_TOTAL; ++j)
                {
                    var bind = new Binding()
                    {
                        Source = FFUModelMap[j],
                        Path = new PropertyPath(names[i])
                    };

                    var label = (Label)this.FindName(names[i] + "_" + j.ToString());
                    label.SetBinding(Label.ContentProperty, bind);
                    
                    label.Tag = j;

                    if (names[i] == "AlarmCode")
                    {
                        label.SetBinding(Label.ForegroundProperty, new Binding()
                                         {
                                            Source = FFUModelMap[j],
                                            Path = new PropertyPath("AlarmCodeColor")
                                         }
                                         );
                    }
                }
            }

            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var bind = new Binding()
                {
                    Source = FFUModelMap[j],
                    Path = new PropertyPath("Operation")
                };

                var label = (ToggleButton)this.FindName("Operation_" + j.ToString());
                label.SetBinding(ToggleButton.IsCheckedProperty, bind);
            }

            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var bind = new Binding()
                {
                    Source = FFUModelMap[j],
                    Path = new PropertyPath("AlarmCodeColor")
                };

                var light = (Ellipse)this.FindName("Light_" + j.ToString());
                light.SetBinding(Ellipse.FillProperty, bind);
            }
        }

        #endregion

        #region SettingLabel_MouseLeftButtonDown

        private void SettingLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //取出 按下的是哪一個FFUModel
            CurrentFFUModel = FFUModelMap[(int)(((Label)sender).Tag)];

            dialog.setSettingSpeed(CurrentFFUModel);
            dialog.ShowDialog();
        }
        #endregion

        #region Window_Closing

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            dialog.Closing -= dialog.Window_Closing;
            dialog.Close();
        }
        #endregion

        private void IpLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
