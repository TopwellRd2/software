﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using EasyModbus;

namespace aluminum_supplier_monitor
{
    class SingletonModbusClient
    {
        private static readonly object thisLock = new object();

        private static ModbusClient ModbusClient;

        private SingletonModbusClient() { }

        public static ModbusClient GetInstance()
        {
            if (null == ModbusClient)
            {
                // 避免多執行緒可能會產生兩個以上的實例，所以 lock
                lock (thisLock)
                {
                    // lock 後，再判斷一次目前有無實例
                    // 此次的判斷，是避免多執行緒，同時通過前一次的 null == instance 判斷
                    if (null == ModbusClient)
                    {
                        IniHandler iniHandler = new IniHandler(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName)+ "/Ip.ini");
                        if (iniHandler.isExistIni() == false)
                            MessageBox.Show("遺失Ip.ini");

                        string ip = iniHandler.ReadIniFile("Setting", "Ip", "default");
                        if (ip == "default") {
                            MessageBox.Show("Ip Error");
                        }                         
                        else
                        {
                            Info.IP = ip;
                        }
                            
                        ModbusClient = new ModbusClient(Info.IP, 502);
                        //ModbusClient.UnitIdentifier = 1;
                    }
                }
            }

            return ModbusClient;
        }
    }
}
