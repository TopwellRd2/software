﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace aluminum_supplier_monitor.command
{
    class WriteOperationCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is FFUModel  &&  SingletonModbusClient.GetInstance().Connected)
            {
                FFUModel FFUModel = (FFUModel)parameter;

                if (FFUModel.Operation == true)
                {
                    if (MessageBox.Show("確定要關閉FFU?", "警告", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        SingletonModbusClient.GetInstance().WriteSingleRegister(FFUModel.Id * 10, 0);
                }
                else
                {
                    if (MessageBox.Show("確定要開啟FFU?", "警告", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        SingletonModbusClient.GetInstance().WriteSingleRegister(FFUModel.Id * 10, 1);
                }
            }
        }
    }
}
