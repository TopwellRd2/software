﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using EasyModbus;

using aluminum_supplier_monitor.command;

namespace aluminum_supplier_monitor
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Property
        private const int FFU_TOTAL = 10; //單一頁籤 FFU總數

        private DispatcherTimer timer = new DispatcherTimer();

        private ModbusClient modbusClient;

        private Dictionary<int, FFUModel> ST_FFUModelMap { get; set; } = new Dictionary<int, FFUModel>();
        public Dictionary<int, FFUModel> CT_FFUModelMap { get; set; } = new Dictionary<int, FFUModel>();

        private FFUModel CurrentFFUModel = null;//被點選的model

        private InputDialog dialog = new InputDialog();
        private DataWindow dataWindow;
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            modbusClient = SingletonModbusClient.GetInstance();
            dataWindow = new DataWindow(this);

            IpLabel.Content = Info.IP;
            IpLabel1.Content = Info.IP;

            #region Create FFUModel and add it to FFUMap
            ST_FFUModelMap.Add(1 , new FFUModel("358", 1 ));
            ST_FFUModelMap.Add(2 , new FFUModel("258", 2 ));
            ST_FFUModelMap.Add(3 , new FFUModel("359", 3 ));
            ST_FFUModelMap.Add(4 , new FFUModel("259", 4 ));
            ST_FFUModelMap.Add(5 , new FFUModel("360", 5 ));
            ST_FFUModelMap.Add(6 , new FFUModel("260", 6 ));
            ST_FFUModelMap.Add(7 , new FFUModel("361", 7 ));
            ST_FFUModelMap.Add(8 , new FFUModel("261", 8 ));
            ST_FFUModelMap.Add(9 , new FFUModel("362", 9 ));
            ST_FFUModelMap.Add(10, new FFUModel("262", 10));
            CT_FFUModelMap.Add(1, new FFUModel("G05-19", 1));
            CT_FFUModelMap.Add(2, new FFUModel("G05-22", 2));
            CT_FFUModelMap.Add(3, new FFUModel("G05-23", 3));
            CT_FFUModelMap.Add(4, new FFUModel("G05-24", 4));
            CT_FFUModelMap.Add(5, new FFUModel("G05-25", 5));
            CT_FFUModelMap.Add(6, new FFUModel("G05-26", 6));
            CT_FFUModelMap.Add(7, new FFUModel("G05-27", 7));
            CT_FFUModelMap.Add(8, new FFUModel("G05-28", 8));
            CT_FFUModelMap.Add(9, new FFUModel("G05-29", 9));
            CT_FFUModelMap.Add(10, new FFUModel("G05-30", 10));
            #endregion

            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var toggleBtn = (ToggleButton)this.FindName("Operation_" + j.ToString());

                toggleBtn.Command = new WriteOperationCommand();
                toggleBtn.CommandParameter = ST_FFUModelMap[j];
            }
                
            DataBindSetting();

         
            ModbusConnect();

            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = TimeSpan.FromSeconds(5);
            timer.Start();
        }

        private async void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();

            //Update word , let user know the program whether run 
            //UpdateLabel.Content = "更新中...";
            //Task.Run(() => { MessageBox.Show(Thread.CurrentThread.ManagedThreadId.ToString()); Thread.Sleep(2000); });

            //第一頁籤
            modbusClient.UnitIdentifier = 1;
            int[] datas = await Task.Run<int[]>(() => { return OnTimeAction(); });

            if (datas == null)
            {
                foreach (var ffuModel in ST_FFUModelMap)
                    ffuModel.Value.init();
            }
            else
            {
                for (int i = 1; i <= FFU_TOTAL; ++i)
                {
                    int[] ffuDatas = new int[10];
                    Array.Copy(datas, (i - 1) * 10, ffuDatas, 0, 10);

                    ST_FFUModelMap[i].setData(ffuDatas);
                }
            }

            //第二頁籤
            modbusClient.UnitIdentifier = 2;
            int[] datas2 = await Task.Run<int[]>(() => { return OnTimeAction(); });

            if (datas2 == null)
            {
                foreach (var ffuModel in CT_FFUModelMap)
                    ffuModel.Value.init();
            }
            else
            {
                for (int i = 1; i <= FFU_TOTAL; ++i)
                {
                    int[] ffuDatas = new int[10];
                    Array.Copy(datas2, (i - 1) * 10, ffuDatas, 0, 10);

                    CT_FFUModelMap[i].setData(ffuDatas);
                }
            }

            //Update connect state if not iplabel turn red else green
            if (modbusClient.Connected == false)
            {
                IpLabel.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            }
            else
            {
                IpLabel.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            }

            if (modbusClient.Connected == false)
            {
                IpLabel1.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            }
            else
            {
                IpLabel1.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            }
            timer.Start();
        }

        private int[] OnTimeAction()
        {
            if (modbusClient.Connected == false)
            {
                ModbusConnect();
                return null;
            }
            else
            {
                try
                {
                    int[] datas = modbusClient.ReadHoldingRegisters(10, 100);

                    return datas;
                }
                catch (Exception ex)
                {
                    modbusClient.Disconnect();
                    //MessageBox.Show(ex.ToString());
                    return null;
                }
            }
        }

        #region Modbus Connect

        private void ModbusConnect()
        {
            try
            {
                modbusClient.Connect();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }
        #endregion

        #region DataBindSetting
        /// <summary>
        /// Setting DataBind
        /// </summary>
        private void DataBindSetting()
        {
            //第一頁籤內容
            string[] names = new string[] { "Number", "SettingSpeed", "ActualSpeed", "AlarmCode", "Current", "Pressure" };
            
            for (int i = 0; i < names.Length; ++i)
            {
                for (int j = 1; j <= FFU_TOTAL; ++j)
                {
                    var bind = new Binding()
                    {
                        Source = ST_FFUModelMap[j],
                        Path = new PropertyPath(names[i])
                    };

                    var label = (Label)this.FindName(names[i] + "_" + j.ToString());
                    label.SetBinding(Label.ContentProperty, bind);
                    
                    label.Tag = j;

                    if (names[i] == "AlarmCode")
                    {
                        label.SetBinding(Label.ForegroundProperty, new Binding()
                                         {
                                            Source = ST_FFUModelMap[j],
                                            Path = new PropertyPath("AlarmCodeColor")
                                         }
                                         );
                    }
                }
            }

            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var bind = new Binding()
                {
                    Source = ST_FFUModelMap[j],
                    Path = new PropertyPath("Operation")
                };

                var label = (ToggleButton)this.FindName("Operation_" + j.ToString());
                label.SetBinding(ToggleButton.IsCheckedProperty, bind);
            }

            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var bind = new Binding()
                {
                    Source = ST_FFUModelMap[j],
                    Path = new PropertyPath("AlarmCodeColor")
                };

                var light = (Ellipse)this.FindName("Light_" + j.ToString());
                light.SetBinding(Ellipse.FillProperty, bind);
            }

            //第二頁籤內容
            for (int j = 1; j <= FFU_TOTAL; ++j)
            {
                var bind = new Binding()
                {
                    Source = CT_FFUModelMap[j],
                    Path = new PropertyPath("AlarmCodeColor")
                };

                var light = (Button)this.FindName("button_" + j.ToString());
                light.SetBinding(Button.BackgroundProperty, bind);
            }
        }

        #endregion

        #region SettingLabel_MouseLeftButtonDown

        private void SettingLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //取出 按下的是哪一個FFUModel
            CurrentFFUModel = ST_FFUModelMap[(int)(((Label)sender).Tag)];

            dialog.setSettingSpeed(CurrentFFUModel);
            dialog.ShowDialog();
        }
        #endregion

        #region Window_Closing

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            dialog.Closing -= dialog.Window_Closing;
            dialog.Close();
            dataWindow.Closing -= dataWindow.Window_Closing;
            dataWindow.Close();
        }
        #endregion

        private void button_Click(object sender, RoutedEventArgs e)     //CTAX-14 第二頁籤按鈕
        {
            dataWindow.Title = ((Button)sender).Content.ToString();
            //dataWindow.Number.Content = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].Number;
            //dataWindow.Operation.IsChecked = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].Operation;
            //dataWindow.SettingSpeed.Content = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].SettingSpeed;
            //dataWindow.ActualSpeed.Content = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].ActualSpeed;
            //dataWindow.AlarmCode.Content = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].AlarmCode;
            //dataWindow.Current.Content = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].Current;
            //dataWindow.Pressure.Content = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].Pressure;
            //dataWindow.Light.Fill = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))].AlarmCodeColor;

            //DataBinding

            string[] names = new string[] { "Number", "SettingSpeed", "ActualSpeed", "AlarmCode", "Current", "Pressure" };

            for (int i = 0; i < names.Length; ++i)
            {
                var bind = new Binding()
                {
                    Source = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))],
                    Path = new PropertyPath(names[i])
                };

                var label = (Label)dataWindow.FindName(names[i]);
                label.SetBinding(Label.ContentProperty, bind);

                label.Tag = int.Parse(((Button)sender).Name.Replace("button_", ""));

                if (names[i] == "AlarmCode")
                {
                    label.SetBinding(Label.ForegroundProperty, new Binding()
                    {
                        Source = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))],
                        Path = new PropertyPath("AlarmCodeColor")
                    }
                                        );
                }
            }

            //Operation
            var bind1 = new Binding()
            {
                Source = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))],
                Path = new PropertyPath("Operation")
            };

            var label1 = (ToggleButton)dataWindow.FindName("Operation");
            label1.SetBinding(ToggleButton.IsCheckedProperty, bind1);

            //Light
            var bind2 = new Binding()
            {
                Source = CT_FFUModelMap[int.Parse(((Button)sender).Name.Replace("button_", ""))],
                Path = new PropertyPath("AlarmCodeColor")
            };

            var light = (Ellipse)dataWindow.FindName("Light");
            light.SetBinding(Ellipse.FillProperty, bind2);

            //設定啟停command
            var toggleBtn = (ToggleButton)dataWindow.FindName("Operation".ToString());
            toggleBtn.Command = new WriteOperationCommand();
            int nowClick = int.Parse(((Button)sender).Name.Replace("button_", ""));
            toggleBtn.CommandParameter = CT_FFUModelMap[nowClick];

            dataWindow.ShowDialog();
        }
    }
}
