﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace aluminum_supplier_monitor
{
    /// <summary>
    /// DataWindow.xaml 的互動邏輯
    /// </summary>
    public partial class DataWindow : Window
    {
        private MainWindow mainWindow;
        private InputDialog dialog = new InputDialog();
        private FFUModel CurrentFFUModel = null;//被點選的model
        public DataWindow(MainWindow window)
        {
            InitializeComponent();
            mainWindow = window;
        }

        public void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void SettingSpeed_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //取出 按下的是哪一個FFUModel
            CurrentFFUModel = mainWindow.CT_FFUModelMap[(int)(((Label)sender).Tag)];

            dialog.setSettingSpeed(CurrentFFUModel);
            dialog.ShowDialog();
        }
    }
}
