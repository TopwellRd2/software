﻿namespace pm5000_particle_counter
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SerialPortToolStrip = new System.Windows.Forms.ToolStrip();
            this.ResearchSerialPortBtn = new System.Windows.Forms.ToolStripButton();
            this.SerialPortCombox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.ConnectBtn = new System.Windows.Forms.Button();
            this.StartBtn = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.GetValueBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Particle0Dot3Label = new System.Windows.Forms.Label();
            this.Particle0Dot5Label = new System.Windows.Forms.Label();
            this.Particle1Dot0Label = new System.Windows.Forms.Label();
            this.Particle2Dot5Label = new System.Windows.Forms.Label();
            this.Particle5Dot0Label = new System.Windows.Forms.Label();
            this.Particle10Dot0Label = new System.Windows.Forms.Label();
            this.StopBtn = new System.Windows.Forms.Button();
            this.IsConnectedLabel = new System.Windows.Forms.Label();
            this.NumberBtn = new System.Windows.Forms.Button();
            this.AlarmLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SerialPortToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // SerialPortToolStrip
            // 
            this.SerialPortToolStrip.Font = new System.Drawing.Font("Microsoft JhengHei UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SerialPortToolStrip.ImageScalingSize = new System.Drawing.Size(25, 25);
            this.SerialPortToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResearchSerialPortBtn,
            this.SerialPortCombox,
            this.toolStripSeparator1,
            this.toolStripLabel4});
            this.SerialPortToolStrip.Location = new System.Drawing.Point(0, 0);
            this.SerialPortToolStrip.Name = "SerialPortToolStrip";
            this.SerialPortToolStrip.Size = new System.Drawing.Size(518, 33);
            this.SerialPortToolStrip.TabIndex = 1;
            this.SerialPortToolStrip.Text = "toolStrip2";
            // 
            // ResearchSerialPortBtn
            // 
            this.ResearchSerialPortBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ResearchSerialPortBtn.Image = ((System.Drawing.Image)(resources.GetObject("ResearchSerialPortBtn.Image")));
            this.ResearchSerialPortBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ResearchSerialPortBtn.Name = "ResearchSerialPortBtn";
            this.ResearchSerialPortBtn.Size = new System.Drawing.Size(29, 30);
            this.ResearchSerialPortBtn.Text = "搜尋 COM";
            this.ResearchSerialPortBtn.Click += new System.EventHandler(this.ResearchSerialPortBtn_Click);
            // 
            // SerialPortCombox
            // 
            this.SerialPortCombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SerialPortCombox.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SerialPortCombox.Name = "SerialPortCombox";
            this.SerialPortCombox.Size = new System.Drawing.Size(121, 33);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 33);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(65, 30);
            this.toolStripLabel4.Text = "           ";
            // 
            // ConnectBtn
            // 
            this.ConnectBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ConnectBtn.Location = new System.Drawing.Point(186, 0);
            this.ConnectBtn.Name = "ConnectBtn";
            this.ConnectBtn.Size = new System.Drawing.Size(79, 37);
            this.ConnectBtn.TabIndex = 2;
            this.ConnectBtn.Text = "連線";
            this.ConnectBtn.UseVisualStyleBackColor = true;
            this.ConnectBtn.Click += new System.EventHandler(this.ConnectBtn_Click);
            // 
            // StartBtn
            // 
            this.StartBtn.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.StartBtn.Location = new System.Drawing.Point(25, 52);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(142, 43);
            this.StartBtn.TabIndex = 3;
            this.StartBtn.Text = "啟動";
            this.StartBtn.UseVisualStyleBackColor = true;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // GetValueBtn
            // 
            this.GetValueBtn.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.GetValueBtn.Location = new System.Drawing.Point(25, 123);
            this.GetValueBtn.Name = "GetValueBtn";
            this.GetValueBtn.Size = new System.Drawing.Size(142, 48);
            this.GetValueBtn.TabIndex = 4;
            this.GetValueBtn.Text = "取值";
            this.GetValueBtn.UseVisualStyleBackColor = true;
            this.GetValueBtn.Click += new System.EventHandler(this.GetValueBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(21, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = ">0.3um 顆粒數量 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(21, 261);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = ">0.5um 顆粒數量 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(21, 300);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = ">1.0um 顆粒數量 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(21, 420);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = ">10um 顆粒數量 :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(21, 378);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = ">5.0um 顆粒數量 :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(21, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(162, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = ">2.5um 顆粒數量 :";
            // 
            // Particle0Dot3Label
            // 
            this.Particle0Dot3Label.AutoSize = true;
            this.Particle0Dot3Label.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Particle0Dot3Label.Location = new System.Drawing.Point(200, 221);
            this.Particle0Dot3Label.Name = "Particle0Dot3Label";
            this.Particle0Dot3Label.Size = new System.Drawing.Size(18, 20);
            this.Particle0Dot3Label.TabIndex = 11;
            this.Particle0Dot3Label.Text = "0";
            // 
            // Particle0Dot5Label
            // 
            this.Particle0Dot5Label.AutoSize = true;
            this.Particle0Dot5Label.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Particle0Dot5Label.Location = new System.Drawing.Point(200, 260);
            this.Particle0Dot5Label.Name = "Particle0Dot5Label";
            this.Particle0Dot5Label.Size = new System.Drawing.Size(18, 20);
            this.Particle0Dot5Label.TabIndex = 12;
            this.Particle0Dot5Label.Text = "0";
            // 
            // Particle1Dot0Label
            // 
            this.Particle1Dot0Label.AutoSize = true;
            this.Particle1Dot0Label.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Particle1Dot0Label.Location = new System.Drawing.Point(200, 301);
            this.Particle1Dot0Label.Name = "Particle1Dot0Label";
            this.Particle1Dot0Label.Size = new System.Drawing.Size(18, 20);
            this.Particle1Dot0Label.TabIndex = 13;
            this.Particle1Dot0Label.Text = "0";
            // 
            // Particle2Dot5Label
            // 
            this.Particle2Dot5Label.AutoSize = true;
            this.Particle2Dot5Label.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Particle2Dot5Label.Location = new System.Drawing.Point(200, 340);
            this.Particle2Dot5Label.Name = "Particle2Dot5Label";
            this.Particle2Dot5Label.Size = new System.Drawing.Size(18, 20);
            this.Particle2Dot5Label.TabIndex = 14;
            this.Particle2Dot5Label.Text = "0";
            // 
            // Particle5Dot0Label
            // 
            this.Particle5Dot0Label.AutoSize = true;
            this.Particle5Dot0Label.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Particle5Dot0Label.Location = new System.Drawing.Point(200, 379);
            this.Particle5Dot0Label.Name = "Particle5Dot0Label";
            this.Particle5Dot0Label.Size = new System.Drawing.Size(18, 20);
            this.Particle5Dot0Label.TabIndex = 15;
            this.Particle5Dot0Label.Text = "0";
            // 
            // Particle10Dot0Label
            // 
            this.Particle10Dot0Label.AutoSize = true;
            this.Particle10Dot0Label.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Particle10Dot0Label.Location = new System.Drawing.Point(199, 419);
            this.Particle10Dot0Label.Name = "Particle10Dot0Label";
            this.Particle10Dot0Label.Size = new System.Drawing.Size(18, 20);
            this.Particle10Dot0Label.TabIndex = 16;
            this.Particle10Dot0Label.Text = "0";
            // 
            // StopBtn
            // 
            this.StopBtn.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.StopBtn.Location = new System.Drawing.Point(186, 52);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(142, 43);
            this.StopBtn.TabIndex = 17;
            this.StopBtn.Text = "停止";
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // IsConnectedLabel
            // 
            this.IsConnectedLabel.AutoSize = true;
            this.IsConnectedLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.IsConnectedLabel.Location = new System.Drawing.Point(298, 9);
            this.IsConnectedLabel.Name = "IsConnectedLabel";
            this.IsConnectedLabel.Size = new System.Drawing.Size(69, 20);
            this.IsConnectedLabel.TabIndex = 18;
            this.IsConnectedLabel.Text = "未連接";
            // 
            // NumberBtn
            // 
            this.NumberBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.NumberBtn.Location = new System.Drawing.Point(349, 52);
            this.NumberBtn.Name = "NumberBtn";
            this.NumberBtn.Size = new System.Drawing.Size(132, 43);
            this.NumberBtn.TabIndex = 19;
            this.NumberBtn.Text = "傳感器編碼";
            this.NumberBtn.UseVisualStyleBackColor = true;
            this.NumberBtn.Click += new System.EventHandler(this.NumberBtn_Click);
            // 
            // AlarmLabel
            // 
            this.AlarmLabel.AutoSize = true;
            this.AlarmLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AlarmLabel.Location = new System.Drawing.Point(382, 165);
            this.AlarmLabel.Name = "AlarmLabel";
            this.AlarmLabel.Size = new System.Drawing.Size(29, 20);
            this.AlarmLabel.TabIndex = 20;
            this.AlarmLabel.Text = "無";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(317, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "警報 :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 464);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AlarmLabel);
            this.Controls.Add(this.NumberBtn);
            this.Controls.Add(this.IsConnectedLabel);
            this.Controls.Add(this.StopBtn);
            this.Controls.Add(this.Particle10Dot0Label);
            this.Controls.Add(this.Particle5Dot0Label);
            this.Controls.Add(this.Particle2Dot5Label);
            this.Controls.Add(this.Particle1Dot0Label);
            this.Controls.Add(this.Particle0Dot5Label);
            this.Controls.Add(this.Particle0Dot3Label);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GetValueBtn);
            this.Controls.Add(this.StartBtn);
            this.Controls.Add(this.ConnectBtn);
            this.Controls.Add(this.SerialPortToolStrip);
            this.Name = "Form1";
            this.Text = "PM5000";
            this.SerialPortToolStrip.ResumeLayout(false);
            this.SerialPortToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip SerialPortToolStrip;
        private System.Windows.Forms.ToolStripButton ResearchSerialPortBtn;
        private System.Windows.Forms.ToolStripComboBox SerialPortCombox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Button ConnectBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button GetValueBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Particle0Dot3Label;
        private System.Windows.Forms.Label Particle0Dot5Label;
        private System.Windows.Forms.Label Particle1Dot0Label;
        private System.Windows.Forms.Label Particle2Dot5Label;
        private System.Windows.Forms.Label Particle5Dot0Label;
        private System.Windows.Forms.Label Particle10Dot0Label;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.Label IsConnectedLabel;
        private System.Windows.Forms.Button NumberBtn;
        private System.Windows.Forms.Label AlarmLabel;
        private System.Windows.Forms.Label label7;
    }
}

