﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;
using System.IO.Ports;

namespace pm5000_particle_counter
{
    public partial class Form1 : Form
    {
        private SerialPort mSerialPort;
        private bool isStart;

        public Form1()
        {
            InitializeComponent();
            isStart = false;
        }

        private void ResearchSerialPortBtn_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            SerialPortCombox.Items.Clear();
            foreach (string port in ports)
                SerialPortCombox.Items.Add(port);
        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string port = SerialPortCombox.SelectedItem.ToString();
                mSerialPort = new SerialPort(port, 9600, Parity.None, 8, StopBits.One);

                mSerialPort.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            if (mSerialPort == null || mSerialPort.IsOpen == false)
                return;

            byte[] inputByte;

            inputByte = new byte[] { 17, 3, 12, 2, 30, 192 };
            mSerialPort.Write(inputByte, 0, inputByte.Length);
            bool readSuccess = isReadData(5, 8);

            if (readSuccess)
            {
                byte[] bufferReceiver = new byte[mSerialPort.BytesToRead];
                mSerialPort.Read(bufferReceiver, 0, mSerialPort.BytesToRead);

                if (bufferReceiver[3] == 2)
                {
                    MessageBox.Show("已啟動");
                    isStart = true;
                }
            }
            else
            {
                MessageBox.Show("No response");
            }
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            if (mSerialPort == null || mSerialPort.IsOpen == false)
                return;

            byte[] inputByte;

            inputByte = new byte[] { 17, 3, 12, 1, 30, 193 };
            mSerialPort.Write(inputByte, 0, inputByte.Length);
            bool readSuccess = isReadData(5, 8);

            if (readSuccess)
            {
                byte[] bufferReceiver = new byte[mSerialPort.BytesToRead];
                mSerialPort.Read(bufferReceiver, 0, mSerialPort.BytesToRead);

                if (bufferReceiver[3] == 1)
                {
                    MessageBox.Show("已關閉");
                    isStart = false;
                }
            }
            else
            {
                MessageBox.Show("No response");
            }

        }

        private void NumberBtn_Click(object sender, EventArgs e)
        {
            if (mSerialPort == null || mSerialPort.IsOpen == false)
                return;

            byte[] inputByte;

            inputByte = new byte[] { 17, 1, 31, 207 };

            mSerialPort.Write(inputByte, 0, inputByte.Length);

            bool readSuccess = isReadData(14, 10);

            if (readSuccess)
            {
                string numberStr = "";
                byte[] bufferReceiver = new byte[mSerialPort.BytesToRead];
                mSerialPort.Read(bufferReceiver, 0, mSerialPort.BytesToRead);

                numberStr += (bufferReceiver[3] * 256 + bufferReceiver[4]).ToString() + " ";
                numberStr += (bufferReceiver[5] * 256 + bufferReceiver[6]).ToString() + " ";
                numberStr += (bufferReceiver[7] * 256 + bufferReceiver[8]).ToString() + " ";
                numberStr += (bufferReceiver[9] * 256 + bufferReceiver[10]).ToString() + " ";
                numberStr += (bufferReceiver[11] * 256 + bufferReceiver[12]).ToString() + " ";

                MessageBox.Show(numberStr);
            }
            else
            {
                MessageBox.Show("No response");
            }
        }

        private void GetValueBtn_Click(object sender, EventArgs e)
        {
            if (mSerialPort == null || mSerialPort.IsOpen == false)
                return;

            byte[] inputByte;

            inputByte = new byte[] { 17, 2, 11, 7, 219 };

            mSerialPort.Write(inputByte, 0, inputByte.Length);

            bool readSuccess = isReadData(55, 20);

            if (readSuccess)
            {
                byte[] bufferReceiver = new byte[mSerialPort.BytesToRead];
                mSerialPort.Read(bufferReceiver, 0, mSerialPort.BytesToRead);

                

                Particle0Dot3Label.Text = (bufferReceiver[27] * 256 * 256 * 256 + bufferReceiver[28] * 256 * 256 + bufferReceiver[29] * 256 + bufferReceiver[30]).ToString() +" "+ bufferReceiver[27].ToString() +","+ bufferReceiver[28].ToString() + "," + bufferReceiver[29].ToString() + "," + bufferReceiver[30].ToString();
                Particle0Dot5Label.Text = (bufferReceiver[31] * 256 * 256 * 256 + bufferReceiver[32] * 256 * 256 + bufferReceiver[33] * 256 + bufferReceiver[34]).ToString() + " " + bufferReceiver[31].ToString() + "," + bufferReceiver[32].ToString() + "," + bufferReceiver[33].ToString() + "," + bufferReceiver[34].ToString();
                Particle1Dot0Label.Text = (bufferReceiver[35] * 256 * 256 * 256 + bufferReceiver[36] * 256 * 256 + bufferReceiver[37] * 256 + bufferReceiver[38]).ToString() + " " + bufferReceiver[35].ToString() + "," + bufferReceiver[36].ToString() + "," + bufferReceiver[37].ToString() + "," + bufferReceiver[38].ToString();
                Particle2Dot5Label.Text = (bufferReceiver[39] * 256 * 256 * 256 + bufferReceiver[40] * 256 * 256 + bufferReceiver[41] * 256 + bufferReceiver[42]).ToString() + " " + bufferReceiver[39].ToString() + "," + bufferReceiver[40].ToString() + "," + bufferReceiver[41].ToString() + "," + bufferReceiver[42].ToString();
                Particle5Dot0Label.Text = (bufferReceiver[43] * 256 * 256 * 256 + bufferReceiver[44] * 256 * 256 + bufferReceiver[45] * 256 + bufferReceiver[46]).ToString() + " " + bufferReceiver[43].ToString() + "," + bufferReceiver[44].ToString() + "," + bufferReceiver[45].ToString() + "," + bufferReceiver[46].ToString();
                Particle10Dot0Label.Text = (bufferReceiver[47] * 256 * 256 * 256 + bufferReceiver[48] * 256 * 256 + bufferReceiver[49] * 256 + bufferReceiver[50]).ToString() + " " + bufferReceiver[47].ToString() + "," + bufferReceiver[48].ToString() + "," + bufferReceiver[49].ToString() + "," + bufferReceiver[50].ToString();

                if ((bufferReceiver[51] & 1) == 1)
                    AlarmLabel.Text = "風散轉速高";
                else
                    AlarmLabel.Text = "無";

                if (((bufferReceiver[51]>>1) & 1) == 1)
                    AlarmLabel.Text = "風散轉速低";
                else
                    AlarmLabel.Text = "無";
            }
            else
            {
                MessageBox.Show("No response");
            }
        }

        /// <summary>
        /// 讀取緩衝 確認是否有讀到足夠的數量
        /// </summary>
        /// <param name="dataCount">要讀的數量 byte為單位</param>
        /// <param name="waitParam">等的最大次數 1次=20ms</param>
        /// <returns></returns>
        private bool isReadData(int dataCount, int waitParam)
        {
            int count = 0;
            bool readSuccess = true;
            while (mSerialPort.BytesToRead < dataCount)
            {
                count++;
                if (count > waitParam)
                {
                    MessageBox.Show("無回應", "錯誤");
                    readSuccess = false;
                    break;
                }

                Thread.Sleep(20);
            }

            return readSuccess;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (mSerialPort != null)
            {
                if (mSerialPort.IsOpen) {
                    IsConnectedLabel.Text = "已連接";
                    //GetValueBtn.PerformClick();
                }
                else
                    IsConnectedLabel.Text = "未連接";
            }
        }


    }
}
