﻿Imports System.IO.Ports
Imports System.Threading
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.ComponentModel

Public Class Form1

    Dim line As Integer = 0
    Dim conn = New SqlConnection                                                         '設"conn"為新的SQL連線
    Dim strinsert As String

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ' ''視窗關閉再次去認功能,"在單一執行緒上啟動第二個訊息迴圈不是有效的作業。請使用 Form.ShowDialog 代替,待處理-20180208"''
        '程式搬到FromClosing 並把Application.Run() -> e.Cancel = True即可
        If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ' ''視窗關閉再次去認功能,"在單一執行緒上啟動第二個訊息迴圈不是有效的作業。請使用 Form.ShowDialog 代替,待處理-20180208"''
        '程式搬到FromClosing 並把Application.Run() -> e.Cancel = True即可
        conn.Close()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim index As Integer = 2014
        Dim dl As String

        For portCount = 1 To 7                                                          '選取年份建置 (2015~2021)
            index = index + 1
            ComboBox1.Items.Add(index)
        Next

        For portCount = 1 To 12                                                         '選取月份建置
            If portCount < 10 Then
                ComboBox2.Items.Add("0" & portCount)
            Else
                ComboBox2.Items.Add(portCount)
            End If
        Next

        For portCount = 1 To 31                                                         '選取日期建置
            If portCount < 10 Then
                ComboBox3.Items.Add("0" & portCount)
            Else
                ComboBox3.Items.Add(portCount)
            End If
        Next
        ComboBox1.Text = DateTime.Today.Year()                                          '套入今天之年分

        dl = DateTime.Today.Month()
        If Len(dl) = 1 Then                                                             '套入今天之月份
            ComboBox2.Text = "0" & dl
        Else
            ComboBox2.Text = dl
        End If

        dl = DateTime.Today.Day()
        If Len(dl) = 1 Then                                                             '套入今天之日期
            ComboBox3.Text = "0" & dl
        Else
            ComboBox3.Text = dl
        End If
        Thread.Sleep(4000)                                                                                                                       '緩衝開機啟動SQL還沒Ready
        'conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=PES-ACER\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=SB1708-04-PC\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=DESKTOP-80IH088\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=DESKTOP-4USONI6\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=DESKTOP-FF5KD8T\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=DESKTOP-2IKPQV9\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=DESKTOP-SJ96271\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源
        'conn.ConnectionString = "Data Source=.\SQLEXPRESS,1433;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源(local SQL 含指定port)
        'Try
        ' conn.Open()                                                                                                                          '開啟SQL連線
        ' Catch ex As Exception
        ' MessageBox.Show(ex.Message, "連接錯誤")
        ' End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        conn.ConnectionString = "Data Source=.\SQLEXPRESS,1433;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源(local SQL 含指定port)
        Try
            conn.Open()                                                                                                                          '開啟SQL連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        strinsert = "SELECT 案號,Device_SN,時間 as 日期,轉速,左不平衡量,左不平衡角度,右不平衡量,右不平衡角度,左面結果,右面結果 "                         '需要匯出之欄位
        strinsert = strinsert & "FROM [動平衡資料].[dbo].[RawData] "                                                                              '資料來源
        strinsert = strinsert & "where 日期 = '" & DateTime.Today.Year() & "/" & DateTime.Today.Month() & "/" & DateTime.Today.Day() & "'"       '當天日期"
        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                                                                                 '定義cmnd為SqlCommand指令
        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)

        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        Dim FileName As String = DateTime.Today.Year()
        ''為了使輸出檔案名稱為yyyymmdd.csv
        Dim dl As String
        dl = DateTime.Today.Month()
        If Len(dl) = 1 Then                                                             '套入今天之月份
            FileName = FileName & "0" & dl
        Else
            FileName = FileName & dl
        End If
        dl = DateTime.Today.Day()
        If Len(dl) = 1 Then                                                             '套入今天之日期
            FileName = FileName & "0" & dl & ".csv"
        Else
            FileName = FileName & dl & ".csv"
        End If
        Dim FilePath As String = SavePath + FileName

        Try
            Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)                                                  '建立CSV檔
            '寫入欄位名稱
            If set1.Columns.Count > 0 Then
                sw.Write(set1.Columns.Item(0).ColumnName.ToString)
            End If
            For i As Integer = 1 To set1.Columns.Count - 1
                sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
            Next
            sw.Write(sw.NewLine)

            '寫入各欄位資料
            For i As Integer = 0 To set1.Rows.Count - 1
                For j As Integer = 0 To set1.Columns.Count - 1
                    If j = 0 Then
                        sw.Write(set1.Rows(i)(j))
                    Else
                        sw.Write("," + set1.Rows(i)(j).ToString)
                    End If
                Next
                sw.Write(sw.NewLine)
            Next
            MessageBox.Show("今日檔案已匯出完成")
            sw.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "檔案已開啟")
        End Try
        conn.close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Label8.Text = DateTime.Now
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        conn.ConnectionString = "Data Source=MOTOR-PC\SQLEXPRESS;Initial Catalog=動平衡資料;Persist Security Info=True;User ID=sa;Password=12"    '指定SQL Server MDF檔來源(local SQL 含指定port)
        Try
            conn.Open()                                                                                                                          '開啟SQL連線
        Catch ex As Exception
            MessageBox.Show(ex.Message, "連接錯誤")
        End Try
        Dim dataAdapter As New SqlDataAdapter
        Dim set1 As DataTable = New DataTable
        strinsert = "SELECT 案號,Device_SN,時間 as 日期,轉速,左不平衡量,左不平衡角度,右不平衡量,右不平衡角度,左面結果,右面結果 "                         '需要匯出之欄位
        strinsert = strinsert & "FROM [動平衡資料].[dbo].[RawData] "                                                                              '資料來源
        strinsert = strinsert & "where 日期 = '" & DateTime.Today.Year() & "/" & DateTime.Today.Month() & "/" & DateTime.Today.Day() & "'"       '當天日期"
        Dim cmnd As SqlCommand = New SqlCommand(strinsert, conn)                                                                                 '定義cmnd為SqlCommand指令
        dataAdapter.SelectCommand = cmnd
        dataAdapter.Fill(set1)

        'DataTable存成CSV檔的程式碼----------------------------------
        Dim SavePath As String = "C:\Temp\"
        Dim pday As String = Mid(Label11.Text, 1, 4) & Mid(Label11.Text, 6, 2) & Mid(Label11.Text, 9, 2) '篩選出日期yyyymmdd
        Dim day As String = Mid(Label8.Text, 1, 4) & Mid(Label8.Text, 6, 2) & Mid(Label8.Text, 9, 2)     '篩選出日期yyyymmdd
        Dim FileName As String = pday & "_" & day & ".csv"                                               '結合前後日期為當檔案名稱
        Dim FilePath As String = SavePath + FileName

        Try
            Dim sw As New System.IO.StreamWriter(FilePath, False, System.Text.Encoding.Default)                                                  '建立CSV檔
            '寫入欄位名稱
            If set1.Columns.Count > 0 Then
                sw.Write(set1.Columns.Item(0).ColumnName.ToString)
            End If
            For i As Integer = 1 To set1.Columns.Count - 1
                sw.Write("," + set1.Columns.Item(i).ColumnName.ToString)
            Next
            sw.Write(sw.NewLine)

            '寫入各欄位資料
            For i As Integer = 0 To set1.Rows.Count - 1
                For j As Integer = 0 To set1.Columns.Count - 1
                    If j = 0 Then
                        sw.Write(set1.Rows(i)(j))
                    Else
                        sw.Write("," + set1.Rows(i)(j).ToString)
                    End If
                Next
                sw.Write(sw.NewLine)
            Next
            MessageBox.Show("今日檔案已匯出完成")
            sw.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "檔案已開啟")
        End Try
        conn.close()
    End Sub

End Class
