﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoSendKey
{
    public partial class Form1 : Form
    {
        private System.Timers.Timer startButton_Timer;
        private System.Timers.Timer F1_Timer; 
        private System.Timers.Timer F2_Timer;
        private System.Timers.Timer F3_Timer;
        private System.Timers.Timer F4_Timer;
        private System.Timers.Timer F5_Timer;
        private System.Timers.Timer F6_Timer;
        private System.Timers.Timer F7_Timer;
        private System.Timers.Timer F8_Timer;
        private System.Timers.Timer F9_Timer;
        private System.Timers.Timer F10_Timer;
        private System.Timers.Timer F11_Timer;
        private System.Timers.Timer F12_Timer;
        private Boolean F1_Status = false;
        private Boolean F2_Status = false;
        private Boolean F3_Status = false;
        private Boolean F4_Status = false;
        private Boolean F5_Status = false;
        private Boolean F6_Status = false;
        private Boolean F7_Status = false;
        private Boolean F8_Status = false;
        private Boolean F9_Status = false;
        private Boolean F10_Status = false;
        private Boolean F11_Status = false;
        private Boolean F12_Status = false;
        public Form1()
        {
            InitializeComponent();

            StopButton.Enabled = false;

            startButton_Timer = new System.Timers.Timer();
            startButton_Timer.Interval = 1000;
            startButton_Timer.Elapsed += new System.Timers.ElapsedEventHandler(StartButton_Click);

            F1_Timer = new System.Timers.Timer();
            F1_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F1")); 

            F2_Timer = new System.Timers.Timer();
            F2_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F2")); 

            F3_Timer = new System.Timers.Timer();
            F3_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F3")); 

            F4_Timer = new System.Timers.Timer();
            F4_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F4")); 

            F5_Timer = new System.Timers.Timer();
            F5_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F5")); 

            F6_Timer = new System.Timers.Timer();
            F6_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F6"));

            F7_Timer = new System.Timers.Timer();
            F7_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F7"));

            F8_Timer = new System.Timers.Timer();
            F8_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F8"));

            F9_Timer = new System.Timers.Timer();
            F9_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F9"));

            F10_Timer = new System.Timers.Timer();
            F10_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F10"));

            F11_Timer = new System.Timers.Timer();
            F11_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F11"));

            F12_Timer = new System.Timers.Timer();
            F12_Timer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => SendKey(s, e, "F12"));

        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            startButton_Timer.Start();
            StopButton.Enabled = true;
            StartButton.Enabled = false;

            if (((CheckBox)this.Controls.Find("checkBox1", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox1", true)[0]).Text))
            {
                if (F1_Status == false) 
                {
                    F1_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox1", true)[0]).Text + "000");
                    F1_Timer.Start();
                    F1_Status = true;
                }
            }
            else
            {
                F1_Timer.Stop();
                F1_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox2", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox2", true)[0]).Text))
            {
                if (F2_Status == false)
                {
                    F2_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox2", true)[0]).Text + "000");
                    F2_Timer.Start();
                    F2_Status = true;
                }
            }
            else
            {
                F2_Timer.Stop();
                F2_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox3", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox3", true)[0]).Text))
            {
                if (F3_Status == false)
                {
                    F3_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox3", true)[0]).Text + "000");
                    F3_Timer.Start();
                    F3_Status = true;
                }
            }
            else
            {
                F3_Timer.Stop();
                F3_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox4", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox4", true)[0]).Text))
            {
                if (F4_Status == false)
                {
                    F4_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox4", true)[0]).Text + "000");
                    F4_Timer.Start();
                    F4_Status = true;
                }
            }
            else
            {
                F4_Timer.Stop();
                F4_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox5", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox5", true)[0]).Text))
            {
                if (F5_Status == false)
                {
                    F5_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox5", true)[0]).Text + "000");
                    F5_Timer.Start();
                    F5_Status = true;
                }
            }
            else
            {
                F5_Timer.Stop();
                F5_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox6", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox6", true)[0]).Text))
            {
                if (F6_Status == false)
                {
                    F6_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox6", true)[0]).Text + "000");
                    F6_Timer.Start();
                    F6_Status = true;
                }
            }
            else
            {
                F6_Timer.Stop();
                F6_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox7", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox7", true)[0]).Text))
            {
                if (F7_Status == false)
                {
                    F7_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox7", true)[0]).Text + "000");
                    F7_Timer.Start();
                    F7_Status = true;
                }
            }
            else
            {
                F7_Timer.Stop();
                F7_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox8", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox8", true)[0]).Text))
            {
                if (F8_Status == false)
                {
                    F8_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox8", true)[0]).Text + "000");
                    F8_Timer.Start();
                    F8_Status = true;
                }
            }
            else
            {
                F8_Timer.Stop();
                F8_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox9", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox9", true)[0]).Text))
            {
                if (F9_Status == false)
                {
                    F9_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox9", true)[0]).Text + "000");
                    F9_Timer.Start();
                    F9_Status = true;
                }
            }
            else
            {
                F9_Timer.Stop();
                F9_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox10", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox10", true)[0]).Text))
            {
                if (F10_Status == false)
                {
                    F10_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox10", true)[0]).Text + "000");
                    F10_Timer.Start();
                    F10_Status = true;
                }
            }
            else
            {
                F10_Timer.Stop();
                F10_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox11", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox11", true)[0]).Text))
            {
                if (F11_Status == false)
                {
                    F11_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox11", true)[0]).Text + "000");
                    F11_Timer.Start();
                    F11_Status = true;
                }
            }
            else
            {
                F11_Timer.Stop();
                F11_Status = false;
            }

            if (((CheckBox)this.Controls.Find("checkBox12", true)[0]).Checked == true && !string.IsNullOrEmpty(((TextBox)this.Controls.Find("textBox12", true)[0]).Text))
            {
                if (F12_Status == false)
                {
                    F12_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox12", true)[0]).Text + "000");
                    F12_Timer.Start();
                    F12_Status = true;
                }
            }
            else
            {
                F12_Timer.Stop();
                F12_Status = false;
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            StopButton.Enabled = false;
            StartButton.Enabled = true;

            startButton_Timer.Stop();
            F1_Timer.Stop();
            F1_Status = false;
            F2_Timer.Stop();
            F2_Status = false;
            F3_Timer.Stop();
            F3_Status = false;
            F4_Timer.Stop();
            F4_Status = false;
            F5_Timer.Stop();
            F5_Status = false;
            F6_Timer.Stop();
            F6_Status = false;
            F7_Timer.Stop();
            F7_Status = false;
            F8_Timer.Stop();
            F8_Status = false;
            F9_Timer.Stop();
            F9_Status = false;
            F10_Timer.Stop();
            F10_Status = false;
            F11_Timer.Stop();
            F11_Status = false;
            F12_Timer.Stop();
            F12_Status = false;
        }

        private void SendKey(object sender, EventArgs e, string key)
        {
            SendKeys.SendWait("{"+ key +"}");
        }

        private void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            if (StartButton.Enabled == false)
            {

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x1")
                {
                    F1_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox1", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x2")
                {
                    F2_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox2", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x3")
                {
                    F3_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox3", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x4")
                {
                    F4_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox4", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x5")
                {
                    F5_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox5", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x6")
                {
                    F6_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox6", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x7")
                {
                    F7_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox7", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x8")
                {
                    F8_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox8", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "x9")
                {
                    F9_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox9", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "10")
                {
                    F10_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox10", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "11")
                {
                    F11_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox11", true)[0]).Text + "000");
                }

                if (textbox.Name.Substring(textbox.Name.Length - 2, 2) == "12")
                {
                    F12_Timer.Interval = Convert.ToDouble(((TextBox)this.Controls.Find("textBox12", true)[0]).Text + "000");
                }
            }
        }
    }
}
