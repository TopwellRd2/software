﻿
namespace YSD_WriteAirFormula
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SerialPortToolStrip = new System.Windows.Forms.ToolStrip();
            this.ResearchSerialPortBtn = new System.Windows.Forms.ToolStripButton();
            this.SerialPortCombox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.SerialPortConnectBtn = new System.Windows.Forms.ToolStripButton();
            this.SerialConnectStateLable = new System.Windows.Forms.ToolStripLabel();
            this.airFormula_Label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.writeButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.splitter9 = new System.Windows.Forms.Splitter();
            this.splitter10 = new System.Windows.Forms.Splitter();
            this.splitter11 = new System.Windows.Forms.Splitter();
            this.splitter12 = new System.Windows.Forms.Splitter();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.splitter13 = new System.Windows.Forms.Splitter();
            this.splitter14 = new System.Windows.Forms.Splitter();
            this.splitter15 = new System.Windows.Forms.Splitter();
            this.splitter16 = new System.Windows.Forms.Splitter();
            this.splitter17 = new System.Windows.Forms.Splitter();
            this.splitter18 = new System.Windows.Forms.Splitter();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.splitter19 = new System.Windows.Forms.Splitter();
            this.splitter20 = new System.Windows.Forms.Splitter();
            this.splitter21 = new System.Windows.Forms.Splitter();
            this.splitter22 = new System.Windows.Forms.Splitter();
            this.splitter23 = new System.Windows.Forms.Splitter();
            this.splitter24 = new System.Windows.Forms.Splitter();
            this.readButton = new System.Windows.Forms.Button();
            this.SerialPortToolStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // SerialPortToolStrip
            // 
            this.SerialPortToolStrip.Font = new System.Drawing.Font("Microsoft JhengHei UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SerialPortToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.SerialPortToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResearchSerialPortBtn,
            this.SerialPortCombox,
            this.toolStripLabel4,
            this.SerialPortConnectBtn,
            this.SerialConnectStateLable});
            this.SerialPortToolStrip.Location = new System.Drawing.Point(0, 0);
            this.SerialPortToolStrip.Name = "SerialPortToolStrip";
            this.SerialPortToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.SerialPortToolStrip.Size = new System.Drawing.Size(1129, 39);
            this.SerialPortToolStrip.TabIndex = 1;
            this.SerialPortToolStrip.Text = "toolStrip2";
            // 
            // ResearchSerialPortBtn
            // 
            this.ResearchSerialPortBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ResearchSerialPortBtn.Image = ((System.Drawing.Image)(resources.GetObject("ResearchSerialPortBtn.Image")));
            this.ResearchSerialPortBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ResearchSerialPortBtn.Name = "ResearchSerialPortBtn";
            this.ResearchSerialPortBtn.Size = new System.Drawing.Size(36, 36);
            this.ResearchSerialPortBtn.Text = "搜尋 COM";
            this.ResearchSerialPortBtn.Click += new System.EventHandler(this.ResearchSerialPortBtn_Click);
            // 
            // SerialPortCombox
            // 
            this.SerialPortCombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SerialPortCombox.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SerialPortCombox.Name = "SerialPortCombox";
            this.SerialPortCombox.Size = new System.Drawing.Size(121, 39);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(65, 36);
            this.toolStripLabel4.Text = "           ";
            // 
            // SerialPortConnectBtn
            // 
            this.SerialPortConnectBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SerialPortConnectBtn.Image = ((System.Drawing.Image)(resources.GetObject("SerialPortConnectBtn.Image")));
            this.SerialPortConnectBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SerialPortConnectBtn.Name = "SerialPortConnectBtn";
            this.SerialPortConnectBtn.Size = new System.Drawing.Size(36, 36);
            this.SerialPortConnectBtn.Text = "連線";
            this.SerialPortConnectBtn.Click += new System.EventHandler(this.SerialPortConnectBtn_Click);
            // 
            // SerialConnectStateLable
            // 
            this.SerialConnectStateLable.ForeColor = System.Drawing.Color.Red;
            this.SerialConnectStateLable.Name = "SerialConnectStateLable";
            this.SerialConnectStateLable.Size = new System.Drawing.Size(67, 36);
            this.SerialConnectStateLable.Text = "未連接";
            // 
            // airFormula_Label
            // 
            this.airFormula_Label.AutoSize = true;
            this.airFormula_Label.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.airFormula_Label.Location = new System.Drawing.Point(17, 93);
            this.airFormula_Label.Name = "airFormula_Label";
            this.airFormula_Label.Size = new System.Drawing.Size(465, 31);
            this.airFormula_Label.TabIndex = 2;
            this.airFormula_Label.Text = "風量公式：Y (風量) = Ax³ + Bx² + Cx +D ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(95, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 38);
            this.label1.TabIndex = 3;
            this.label1.Text = "A = ";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox1.Location = new System.Drawing.Point(166, 172);
            this.textBox1.MaxLength = 6;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(111, 38);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.Leave += new System.EventHandler(this.TextBox1_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(283, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 38);
            this.label2.TabIndex = 5;
            this.label2.Text = "X 10";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(353, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 28);
            this.label3.TabIndex = 6;
            this.label3.Text = "-";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox2.Location = new System.Drawing.Point(373, 153);
            this.textBox2.MaxLength = 1;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(32, 34);
            this.textBox2.TabIndex = 7;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox2.Leave += new System.EventHandler(this.TextBox2_Leave);
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox6.Location = new System.Drawing.Point(373, 314);
            this.textBox6.MaxLength = 1;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(32, 34);
            this.textBox6.TabIndex = 11;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox6.Leave += new System.EventHandler(this.TextBox2_Leave);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(353, 314);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 28);
            this.label4.TabIndex = 11;
            this.label4.Text = "-";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(283, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 38);
            this.label5.TabIndex = 10;
            this.label5.Text = "X 10";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox5.Location = new System.Drawing.Point(166, 333);
            this.textBox5.MaxLength = 6;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(111, 38);
            this.textBox5.TabIndex = 10;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox5.Leave += new System.EventHandler(this.TextBox1_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(95, 333);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 38);
            this.label6.TabIndex = 8;
            this.label6.Text = "C = ";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox8.Location = new System.Drawing.Point(373, 397);
            this.textBox8.MaxLength = 1;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(32, 34);
            this.textBox8.TabIndex = 13;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox8.Leave += new System.EventHandler(this.TextBox2_Leave);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(353, 397);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 28);
            this.label7.TabIndex = 16;
            this.label7.Text = "-";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(283, 416);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 38);
            this.label8.TabIndex = 15;
            this.label8.Text = "X 10";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox7.Location = new System.Drawing.Point(166, 416);
            this.textBox7.MaxLength = 6;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(111, 38);
            this.textBox7.TabIndex = 12;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox7.Leave += new System.EventHandler(this.TextBox1_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(95, 416);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 38);
            this.label9.TabIndex = 13;
            this.label9.Text = "D = ";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox4.Location = new System.Drawing.Point(373, 233);
            this.textBox4.MaxLength = 1;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(32, 34);
            this.textBox4.TabIndex = 9;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox4.Leave += new System.EventHandler(this.TextBox2_Leave);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(353, 233);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 28);
            this.label10.TabIndex = 21;
            this.label10.Text = "-";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(283, 252);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 38);
            this.label11.TabIndex = 20;
            this.label11.Text = "X 10";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox3.Location = new System.Drawing.Point(166, 252);
            this.textBox3.MaxLength = 6;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(111, 38);
            this.textBox3.TabIndex = 8;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox3.Leave += new System.EventHandler(this.TextBox1_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(95, 252);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 38);
            this.label12.TabIndex = 18;
            this.label12.Text = "B = ";
            // 
            // writeButton
            // 
            this.writeButton.BackColor = System.Drawing.Color.Silver;
            this.writeButton.Enabled = false;
            this.writeButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.writeButton.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.writeButton.Location = new System.Drawing.Point(145, 496);
            this.writeButton.Name = "writeButton";
            this.writeButton.Size = new System.Drawing.Size(202, 58);
            this.writeButton.TabIndex = 23;
            this.writeButton.Text = "寫入";
            this.writeButton.UseVisualStyleBackColor = false;
            this.writeButton.Click += new System.EventHandler(this.writeButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.label74);
            this.groupBox1.Controls.Add(this.label66);
            this.groupBox1.Controls.Add(this.label73);
            this.groupBox1.Controls.Add(this.label58);
            this.groupBox1.Controls.Add(this.label65);
            this.groupBox1.Controls.Add(this.label57);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.splitter6);
            this.groupBox1.Controls.Add(this.splitter5);
            this.groupBox1.Controls.Add(this.splitter4);
            this.groupBox1.Controls.Add(this.splitter3);
            this.groupBox1.Controls.Add(this.splitter2);
            this.groupBox1.Controls.Add(this.splitter1);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox1.Location = new System.Drawing.Point(510, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(607, 122);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "A";
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label74.Location = new System.Drawing.Point(520, 82);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(68, 22);
            this.label74.TabIndex = 26;
            this.label74.Text = "label74";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label66.Location = new System.Drawing.Point(419, 82);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(68, 22);
            this.label66.TabIndex = 26;
            this.label66.Text = "label66";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label73.Location = new System.Drawing.Point(520, 39);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(68, 34);
            this.label73.TabIndex = 25;
            this.label73.Text = "label73";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label58.Location = new System.Drawing.Point(320, 82);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(68, 22);
            this.label58.TabIndex = 13;
            this.label58.Text = "label58";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label65.Location = new System.Drawing.Point(419, 39);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(68, 34);
            this.label65.TabIndex = 25;
            this.label65.Text = "label65";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label57.Location = new System.Drawing.Point(320, 39);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(68, 34);
            this.label57.TabIndex = 12;
            this.label57.Text = "label57";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label50.Location = new System.Drawing.Point(220, 82);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(68, 22);
            this.label50.TabIndex = 11;
            this.label50.Text = "label50";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(220, 39);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 34);
            this.label49.TabIndex = 10;
            this.label49.Text = "label49";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(120, 82);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(68, 22);
            this.label42.TabIndex = 9;
            this.label42.Text = "label42";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(120, 39);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(68, 34);
            this.label41.TabIndex = 8;
            this.label41.Text = "label41";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(19, 82);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 22);
            this.label34.TabIndex = 7;
            this.label34.Text = "label34";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(19, 39);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 34);
            this.label33.TabIndex = 6;
            this.label33.Text = "label33";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter6
            // 
            this.splitter6.BackColor = System.Drawing.SystemColors.Control;
            this.splitter6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter6.Location = new System.Drawing.Point(503, 27);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(100, 92);
            this.splitter6.TabIndex = 5;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.SystemColors.Control;
            this.splitter5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter5.Location = new System.Drawing.Point(403, 27);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(100, 92);
            this.splitter5.TabIndex = 4;
            this.splitter5.TabStop = false;
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.SystemColors.Control;
            this.splitter4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter4.Location = new System.Drawing.Point(303, 27);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(100, 92);
            this.splitter4.TabIndex = 3;
            this.splitter4.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.SystemColors.Control;
            this.splitter3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter3.Location = new System.Drawing.Point(203, 27);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(100, 92);
            this.splitter3.TabIndex = 2;
            this.splitter3.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.Control;
            this.splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter2.Location = new System.Drawing.Point(103, 27);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(100, 92);
            this.splitter2.TabIndex = 1;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.Control;
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Location = new System.Drawing.Point(3, 27);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(100, 92);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.label76);
            this.groupBox2.Controls.Add(this.label68);
            this.groupBox2.Controls.Add(this.label75);
            this.groupBox2.Controls.Add(this.label60);
            this.groupBox2.Controls.Add(this.label67);
            this.groupBox2.Controls.Add(this.label59);
            this.groupBox2.Controls.Add(this.label52);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.splitter7);
            this.groupBox2.Controls.Add(this.splitter8);
            this.groupBox2.Controls.Add(this.splitter9);
            this.groupBox2.Controls.Add(this.splitter10);
            this.groupBox2.Controls.Add(this.splitter11);
            this.groupBox2.Controls.Add(this.splitter12);
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.Location = new System.Drawing.Point(510, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(607, 122);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "B";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label76.Location = new System.Drawing.Point(520, 82);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(68, 22);
            this.label76.TabIndex = 26;
            this.label76.Text = "label76";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label68.Location = new System.Drawing.Point(419, 82);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(68, 22);
            this.label68.TabIndex = 26;
            this.label68.Text = "label68";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label75.Location = new System.Drawing.Point(520, 39);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(68, 34);
            this.label75.TabIndex = 25;
            this.label75.Text = "label75";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label60.Location = new System.Drawing.Point(320, 82);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(68, 22);
            this.label60.TabIndex = 13;
            this.label60.Text = "label60";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label67.Location = new System.Drawing.Point(419, 39);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(68, 34);
            this.label67.TabIndex = 25;
            this.label67.Text = "label67";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label59.Location = new System.Drawing.Point(320, 39);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(68, 34);
            this.label59.TabIndex = 12;
            this.label59.Text = "label59";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label52.Location = new System.Drawing.Point(220, 82);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(68, 22);
            this.label52.TabIndex = 11;
            this.label52.Text = "label52";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label51.Location = new System.Drawing.Point(220, 39);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(68, 34);
            this.label51.TabIndex = 10;
            this.label51.Text = "label51";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(120, 82);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 22);
            this.label44.TabIndex = 9;
            this.label44.Text = "label44";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(120, 39);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 34);
            this.label43.TabIndex = 8;
            this.label43.Text = "label43";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label36.Location = new System.Drawing.Point(19, 82);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 22);
            this.label36.TabIndex = 7;
            this.label36.Text = "label36";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(19, 39);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 34);
            this.label35.TabIndex = 6;
            this.label35.Text = "label35";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter7
            // 
            this.splitter7.BackColor = System.Drawing.SystemColors.Control;
            this.splitter7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter7.Location = new System.Drawing.Point(503, 27);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(100, 92);
            this.splitter7.TabIndex = 5;
            this.splitter7.TabStop = false;
            // 
            // splitter8
            // 
            this.splitter8.BackColor = System.Drawing.SystemColors.Control;
            this.splitter8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter8.Location = new System.Drawing.Point(403, 27);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(100, 92);
            this.splitter8.TabIndex = 4;
            this.splitter8.TabStop = false;
            // 
            // splitter9
            // 
            this.splitter9.BackColor = System.Drawing.SystemColors.Control;
            this.splitter9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter9.Location = new System.Drawing.Point(303, 27);
            this.splitter9.Name = "splitter9";
            this.splitter9.Size = new System.Drawing.Size(100, 92);
            this.splitter9.TabIndex = 3;
            this.splitter9.TabStop = false;
            // 
            // splitter10
            // 
            this.splitter10.BackColor = System.Drawing.SystemColors.Control;
            this.splitter10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter10.Location = new System.Drawing.Point(203, 27);
            this.splitter10.Name = "splitter10";
            this.splitter10.Size = new System.Drawing.Size(100, 92);
            this.splitter10.TabIndex = 2;
            this.splitter10.TabStop = false;
            // 
            // splitter11
            // 
            this.splitter11.BackColor = System.Drawing.SystemColors.Control;
            this.splitter11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter11.Location = new System.Drawing.Point(103, 27);
            this.splitter11.Name = "splitter11";
            this.splitter11.Size = new System.Drawing.Size(100, 92);
            this.splitter11.TabIndex = 1;
            this.splitter11.TabStop = false;
            // 
            // splitter12
            // 
            this.splitter12.BackColor = System.Drawing.SystemColors.Control;
            this.splitter12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter12.Location = new System.Drawing.Point(3, 27);
            this.splitter12.Name = "splitter12";
            this.splitter12.Size = new System.Drawing.Size(100, 92);
            this.splitter12.TabIndex = 0;
            this.splitter12.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.label78);
            this.groupBox3.Controls.Add(this.label70);
            this.groupBox3.Controls.Add(this.label77);
            this.groupBox3.Controls.Add(this.label62);
            this.groupBox3.Controls.Add(this.label69);
            this.groupBox3.Controls.Add(this.label61);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.splitter13);
            this.groupBox3.Controls.Add(this.splitter14);
            this.groupBox3.Controls.Add(this.splitter15);
            this.groupBox3.Controls.Add(this.splitter16);
            this.groupBox3.Controls.Add(this.splitter17);
            this.groupBox3.Controls.Add(this.splitter18);
            this.groupBox3.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox3.Location = new System.Drawing.Point(510, 317);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(607, 122);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "C";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label78.Location = new System.Drawing.Point(520, 82);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(68, 22);
            this.label78.TabIndex = 26;
            this.label78.Text = "label78";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label70.Location = new System.Drawing.Point(419, 82);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(68, 22);
            this.label70.TabIndex = 26;
            this.label70.Text = "label70";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label77.Location = new System.Drawing.Point(520, 39);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(68, 34);
            this.label77.TabIndex = 25;
            this.label77.Text = "label77";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label62.Location = new System.Drawing.Point(320, 82);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(68, 22);
            this.label62.TabIndex = 13;
            this.label62.Text = "label62";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label69.Location = new System.Drawing.Point(419, 39);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(68, 34);
            this.label69.TabIndex = 25;
            this.label69.Text = "label69";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label61.Location = new System.Drawing.Point(320, 39);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 34);
            this.label61.TabIndex = 12;
            this.label61.Text = "label61";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label54.Location = new System.Drawing.Point(220, 82);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(68, 22);
            this.label54.TabIndex = 11;
            this.label54.Text = "label54";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label53.Location = new System.Drawing.Point(220, 39);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(68, 34);
            this.label53.TabIndex = 10;
            this.label53.Text = "label53";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(120, 82);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(68, 22);
            this.label46.TabIndex = 9;
            this.label46.Text = "label46";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(120, 39);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(68, 34);
            this.label45.TabIndex = 8;
            this.label45.Text = "label45";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label38.Location = new System.Drawing.Point(19, 82);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(68, 22);
            this.label38.TabIndex = 7;
            this.label38.Text = "label38";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(19, 39);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(68, 34);
            this.label37.TabIndex = 6;
            this.label37.Text = "label37";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter13
            // 
            this.splitter13.BackColor = System.Drawing.SystemColors.Control;
            this.splitter13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter13.Location = new System.Drawing.Point(503, 27);
            this.splitter13.Name = "splitter13";
            this.splitter13.Size = new System.Drawing.Size(100, 92);
            this.splitter13.TabIndex = 5;
            this.splitter13.TabStop = false;
            // 
            // splitter14
            // 
            this.splitter14.BackColor = System.Drawing.SystemColors.Control;
            this.splitter14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter14.Location = new System.Drawing.Point(403, 27);
            this.splitter14.Name = "splitter14";
            this.splitter14.Size = new System.Drawing.Size(100, 92);
            this.splitter14.TabIndex = 4;
            this.splitter14.TabStop = false;
            // 
            // splitter15
            // 
            this.splitter15.BackColor = System.Drawing.SystemColors.Control;
            this.splitter15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter15.Location = new System.Drawing.Point(303, 27);
            this.splitter15.Name = "splitter15";
            this.splitter15.Size = new System.Drawing.Size(100, 92);
            this.splitter15.TabIndex = 3;
            this.splitter15.TabStop = false;
            // 
            // splitter16
            // 
            this.splitter16.BackColor = System.Drawing.SystemColors.Control;
            this.splitter16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter16.Location = new System.Drawing.Point(203, 27);
            this.splitter16.Name = "splitter16";
            this.splitter16.Size = new System.Drawing.Size(100, 92);
            this.splitter16.TabIndex = 2;
            this.splitter16.TabStop = false;
            // 
            // splitter17
            // 
            this.splitter17.BackColor = System.Drawing.SystemColors.Control;
            this.splitter17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter17.Location = new System.Drawing.Point(103, 27);
            this.splitter17.Name = "splitter17";
            this.splitter17.Size = new System.Drawing.Size(100, 92);
            this.splitter17.TabIndex = 1;
            this.splitter17.TabStop = false;
            // 
            // splitter18
            // 
            this.splitter18.BackColor = System.Drawing.SystemColors.Control;
            this.splitter18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter18.Location = new System.Drawing.Point(3, 27);
            this.splitter18.Name = "splitter18";
            this.splitter18.Size = new System.Drawing.Size(100, 92);
            this.splitter18.TabIndex = 0;
            this.splitter18.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.label80);
            this.groupBox4.Controls.Add(this.label72);
            this.groupBox4.Controls.Add(this.label79);
            this.groupBox4.Controls.Add(this.label64);
            this.groupBox4.Controls.Add(this.label71);
            this.groupBox4.Controls.Add(this.label63);
            this.groupBox4.Controls.Add(this.label56);
            this.groupBox4.Controls.Add(this.label55);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.splitter19);
            this.groupBox4.Controls.Add(this.splitter20);
            this.groupBox4.Controls.Add(this.splitter21);
            this.groupBox4.Controls.Add(this.splitter22);
            this.groupBox4.Controls.Add(this.splitter23);
            this.groupBox4.Controls.Add(this.splitter24);
            this.groupBox4.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox4.Location = new System.Drawing.Point(510, 445);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(607, 122);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "D";
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label80.Location = new System.Drawing.Point(520, 82);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(68, 22);
            this.label80.TabIndex = 26;
            this.label80.Text = "label80";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label72.Location = new System.Drawing.Point(419, 82);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(68, 22);
            this.label72.TabIndex = 26;
            this.label72.Text = "label72";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label79.Location = new System.Drawing.Point(520, 39);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(68, 34);
            this.label79.TabIndex = 25;
            this.label79.Text = "label79";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label64.Location = new System.Drawing.Point(320, 82);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(68, 22);
            this.label64.TabIndex = 13;
            this.label64.Text = "label64";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label71.Location = new System.Drawing.Point(419, 39);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(68, 34);
            this.label71.TabIndex = 25;
            this.label71.Text = "label71";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label63.Location = new System.Drawing.Point(320, 39);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(68, 34);
            this.label63.TabIndex = 12;
            this.label63.Text = "label63";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(220, 82);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(68, 22);
            this.label56.TabIndex = 11;
            this.label56.Text = "label56";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label55.Location = new System.Drawing.Point(220, 39);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(68, 34);
            this.label55.TabIndex = 10;
            this.label55.Text = "label55";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label48.Location = new System.Drawing.Point(120, 82);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(68, 22);
            this.label48.TabIndex = 9;
            this.label48.Text = "label48";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(120, 39);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(68, 34);
            this.label47.TabIndex = 8;
            this.label47.Text = "label47";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(19, 82);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(68, 22);
            this.label40.TabIndex = 7;
            this.label40.Text = "label40";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(19, 39);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(68, 34);
            this.label39.TabIndex = 6;
            this.label39.Text = "label39";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter19
            // 
            this.splitter19.BackColor = System.Drawing.SystemColors.Control;
            this.splitter19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter19.Location = new System.Drawing.Point(503, 27);
            this.splitter19.Name = "splitter19";
            this.splitter19.Size = new System.Drawing.Size(100, 92);
            this.splitter19.TabIndex = 5;
            this.splitter19.TabStop = false;
            // 
            // splitter20
            // 
            this.splitter20.BackColor = System.Drawing.SystemColors.Control;
            this.splitter20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter20.Location = new System.Drawing.Point(403, 27);
            this.splitter20.Name = "splitter20";
            this.splitter20.Size = new System.Drawing.Size(100, 92);
            this.splitter20.TabIndex = 4;
            this.splitter20.TabStop = false;
            // 
            // splitter21
            // 
            this.splitter21.BackColor = System.Drawing.SystemColors.Control;
            this.splitter21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter21.Location = new System.Drawing.Point(303, 27);
            this.splitter21.Name = "splitter21";
            this.splitter21.Size = new System.Drawing.Size(100, 92);
            this.splitter21.TabIndex = 3;
            this.splitter21.TabStop = false;
            // 
            // splitter22
            // 
            this.splitter22.BackColor = System.Drawing.SystemColors.Control;
            this.splitter22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter22.Location = new System.Drawing.Point(203, 27);
            this.splitter22.Name = "splitter22";
            this.splitter22.Size = new System.Drawing.Size(100, 92);
            this.splitter22.TabIndex = 2;
            this.splitter22.TabStop = false;
            // 
            // splitter23
            // 
            this.splitter23.BackColor = System.Drawing.SystemColors.Control;
            this.splitter23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter23.Location = new System.Drawing.Point(103, 27);
            this.splitter23.Name = "splitter23";
            this.splitter23.Size = new System.Drawing.Size(100, 92);
            this.splitter23.TabIndex = 1;
            this.splitter23.TabStop = false;
            // 
            // splitter24
            // 
            this.splitter24.BackColor = System.Drawing.SystemColors.Control;
            this.splitter24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter24.Location = new System.Drawing.Point(3, 27);
            this.splitter24.Name = "splitter24";
            this.splitter24.Size = new System.Drawing.Size(100, 92);
            this.splitter24.TabIndex = 0;
            this.splitter24.TabStop = false;
            // 
            // readButton
            // 
            this.readButton.BackColor = System.Drawing.Color.Silver;
            this.readButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.readButton.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.readButton.Location = new System.Drawing.Point(510, 4);
            this.readButton.Name = "readButton";
            this.readButton.Size = new System.Drawing.Size(169, 39);
            this.readButton.TabIndex = 28;
            this.readButton.Text = "讀取";
            this.readButton.UseVisualStyleBackColor = false;
            this.readButton.Click += new System.EventHandler(this.readButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1129, 581);
            this.Controls.Add(this.readButton);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.writeButton);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.airFormula_Label);
            this.Controls.Add(this.SerialPortToolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "YSD_WriteAirFormula";
            this.SerialPortToolStrip.ResumeLayout(false);
            this.SerialPortToolStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip SerialPortToolStrip;
        private System.Windows.Forms.ToolStripButton ResearchSerialPortBtn;
        private System.Windows.Forms.ToolStripComboBox SerialPortCombox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripButton SerialPortConnectBtn;
        private System.Windows.Forms.ToolStripLabel SerialConnectStateLable;
        private System.Windows.Forms.Label airFormula_Label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button writeButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Splitter splitter9;
        private System.Windows.Forms.Splitter splitter10;
        private System.Windows.Forms.Splitter splitter11;
        private System.Windows.Forms.Splitter splitter12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Splitter splitter13;
        private System.Windows.Forms.Splitter splitter14;
        private System.Windows.Forms.Splitter splitter15;
        private System.Windows.Forms.Splitter splitter16;
        private System.Windows.Forms.Splitter splitter17;
        private System.Windows.Forms.Splitter splitter18;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Splitter splitter19;
        private System.Windows.Forms.Splitter splitter20;
        private System.Windows.Forms.Splitter splitter21;
        private System.Windows.Forms.Splitter splitter22;
        private System.Windows.Forms.Splitter splitter23;
        private System.Windows.Forms.Splitter splitter24;
        private System.Windows.Forms.Button readButton;
    }
}

