﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace YSD_WriteAirFormula
{
    class SerialHandler
    {
        private SerialPort _serialPort;
        public bool IsConnected { get; private set; }

        private const byte WRITE_FUNCTION_CODE = 6;

        private const byte READ_FUNCTION_CODE = 3;

        private const int READ_COUNT = 48;

        private ushort _startAddress = 32;

        private Form1 form1;

        public SerialHandler(Form1 form)
        {
            form1 = form;
            IsConnected = false;
        }

        public void close()
        {
            if (_serialPort != null)
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }
            IsConnected = false;
        }

        public bool connect(string port)
        {
            try
            {
                _serialPort = new SerialPort(port, 9600, Parity.None, 8, StopBits.One);

                if (!_serialPort.IsOpen)
                {
                    _serialPort.Open();
                    _serialPort.RtsEnable = true;
                    _serialPort.DtrEnable = true;
                    IsConnected = true;

                }
                return true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool writeData(ushort pos, int val)
        {
            _serialPort.DiscardInBuffer();

            byte[] inputByte;
            inputByte = ReadHoldingRegistersMsg(Convert.ToByte(1), pos, WRITE_FUNCTION_CODE, val);

            if (IsConnected)
            {
                _serialPort.Write(inputByte, 0, inputByte.Length);
                Thread.Sleep(65);

                if (_serialPort.BytesToRead >= inputByte.Length)
                {
                    byte[] bufferReceiver = new byte[_serialPort.BytesToRead];
                    _serialPort.Read(bufferReceiver, 0, _serialPort.BytesToRead);

                    for (int i = 0; i < inputByte.Length; i++)
                    {
                        if (inputByte[i] != bufferReceiver[i])
                            return false;
                    }
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public void DoReceive()
        {
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] inputByte;
                    inputByte = ReadHoldingRegistersMsg(Convert.ToByte(1), _startAddress, READ_FUNCTION_CODE, READ_COUNT);
                    _serialPort.Write(inputByte, 0, inputByte.Length);

                    int count = 0;
                    bool readSuccess = true;

                    while (_serialPort.BytesToRead < 101)
                    {
                        Thread.Sleep(16);
                        count++;

                        if (count > 48)
                        {
                            readSuccess = false;
                            break;
                        }
                    }

                    if (readSuccess)
                    {
                        byte[] bufferReceiver = new byte[_serialPort.BytesToRead];
                        _serialPort.Read(bufferReceiver, 0, _serialPort.BytesToRead);

                        int[] data = new int[READ_COUNT];

                        if (bufferReceiver[0] == 1 && bufferReceiver[1] == 3)
                        {
                            for (int i = 0; i < data.Length; i++)
                            {
                                data[i] = (data[i] << 8) + bufferReceiver[3 + (2 * i)];
                                data[i] = (data[i] << 8) + bufferReceiver[4 + (2 * i)];
                                if (data[i] > 30000)       //此通訊位置最大僅能輸入至30000,且負號會從65535(-1)開始倒數回去,所以以此來判斷顯示負數
                                {
                                    data[i] = data[i] - 65536;
                                }
                                ((Label)(form1.Controls.Find("label" + (i + 33), true)[0])).Text = data[i].ToString();
                            }
                            MessageBox.Show("讀取成功!!", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("讀取失敗,請確認儀器是否脫落、COM PORT 是否選擇正確!! \n若依然有問題請聯絡設計人員!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("讀取失敗,請確認儀器是否脫落、COM PORT 是否選擇正確!! \n若依然有問題請聯絡設計人員!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("讀取失敗,請確認儀器是否脫落、COM PORT 是否選擇正確!! \n若依然有問題請聯絡設計人員!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private byte[] ReadHoldingRegistersMsg(byte slaveAddress, ushort startAddress, byte function, int numberOfPoints)
        {
            byte[] frame = new byte[8];
            frame[0] = slaveAddress;			    // Slave Address
            frame[1] = function;				    // Function             
            frame[2] = (byte)(startAddress >> 8);	// Starting Address High
            frame[3] = (byte)startAddress;		    // Starting Address Low            
            frame[4] = (byte)(numberOfPoints >> 8);	// Quantity of Registers High
            frame[5] = (byte)numberOfPoints;		// Quantity of Registers Low
            byte[] crc = this.CalculateCRC(frame);  // Calculate CRC.
            frame[frame.Length - 2] = crc[0];       // Error Check Low
            frame[frame.Length - 1] = crc[1];       // Error Check High
            return frame;
        }

        private byte[] CalculateCRC(byte[] data)
        {
            ushort CRCFull = 0xFFFF; // Set the 16-bit register (CRC register) = FFFFH.
            char CRCLSB;
            byte[] CRC = new byte[2];
            for (int i = 0; i < (data.Length) - 2; i++)
            {
                CRCFull = (ushort)(CRCFull ^ data[i]); // 

                for (int j = 0; j < 8; j++)
                {
                    CRCLSB = (char)(CRCFull & 0x0001);
                    CRCFull = (ushort)((CRCFull >> 1) & 0x7FFF);

                    if (CRCLSB == 1)
                        CRCFull = (ushort)(CRCFull ^ 0xA001);
                }
            }
            CRC[1] = (byte)((CRCFull >> 8) & 0xFF);
            CRC[0] = (byte)(CRCFull & 0xFF);
            return CRC;
        }

    }
}
