﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace YSD_WriteAirFormula
{
    public partial class Form1 : Form
    {
        private SerialHandler connectHandler;
        public Form1()
        {
            InitializeComponent();
            connectHandler = new SerialHandler(this);
        }

        private void SerialPortConnectBtn_Click(object sender, EventArgs e)
        {
            if (connectHandler.IsConnected)
            {
                connectHandler.close();
                SerialConnectStateLable.Text = "未連線";
                SerialConnectStateLable.ForeColor = Color.Red;
                SerialPortCombox.Enabled = true;
                writeButton.Enabled = false;
            }
            else
            {
                if (SerialPortCombox.SelectedItem != null)
                {
                    if (connectHandler != null)
                        connectHandler.close();

                    string port = SerialPortCombox.SelectedItem.ToString();
                    if (!connectHandler.connect(port))
                    {
                        return;
                    }
                    SerialConnectStateLable.Text = "已連線";
                    SerialConnectStateLable.ForeColor = Color.Green;
                    SerialPortCombox.Enabled = false;
                    writeButton.Enabled = true;
                    connectHandler.DoReceive();
                }
                else
                {
                    MessageBox.Show("沒選擇 COM PORT !!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
        }

        private void ResearchSerialPortBtn_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            SerialPortCombox.Items.Clear();
            foreach (string port in ports)
                SerialPortCombox.Items.Add(port);
            if(SerialPortCombox.Items.Count != 0)
            {
                SerialPortCombox.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("請連接RS485轉換器 !!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


        }

        private void TextBox1_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;
            if (!string.IsNullOrWhiteSpace(textbox.Text))
            {
                if (int.Parse(textbox.Text) > 30000 || int.Parse(textbox.Text) < -30000)
                {
                    MessageBox.Show("此數字必須介於 -30000 ~ 30000 !!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textbox.Text = "";
                    textbox.Focus();
                    return;
                }
            }
        }

        private void TextBox2_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;
            if (!string.IsNullOrWhiteSpace(textbox.Text))
            {
                if (int.Parse(textbox.Text) > 6 || int.Parse(textbox.Text) < 0)
                {
                    MessageBox.Show("此數字必須介於 0 ~ 6 !!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textbox.Text = "";
                    textbox.Focus();
                    return;
                }
            }
        }

        private void writeButton_Click(object sender, EventArgs e)
        {
            TextBox textbox;
            for (int x = 1; x <= 8; x++)
            {
                textbox = ((TextBox)this.Controls.Find("textBox" + x, true)[0]);
                if (string.IsNullOrWhiteSpace(textbox.Text))
                {
                    MessageBox.Show("欄位不得空白!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textbox.Focus();
                    return;
                }
            }

            bool result;
            int test=0;

            for (int x = 0; x <= 5; x++)
            {
                for (int y = 1; y <= 8; y++)
                {
                    test = Convert.ToInt32(((TextBox)this.Controls.Find("textBox" + y, true)[0]).Text);
                    result = connectHandler.writeData(Convert.ToUInt16(31 + y + ( 8 * x )), Convert.ToInt32(((TextBox)this.Controls.Find("textBox" + y, true)[0]).Text));
                    if (result == false)
                    {
                        MessageBox.Show("寫入失敗,請確認儀器是否脫落、COM PORT 是否選擇正確!! \n若依然有問題請聯絡設計人員!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            MessageBox.Show("寫入成功!!", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            connectHandler.DoReceive();

        }

        private void readButton_Click(object sender, EventArgs e)
        {
            if (connectHandler.IsConnected)
            {
                connectHandler.DoReceive();
            }
            else
            {
                MessageBox.Show("請連線後再讀取!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
