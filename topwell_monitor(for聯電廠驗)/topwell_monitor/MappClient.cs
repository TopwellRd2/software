﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace topwell_monitor
{
    class MappClient
    {
        private const int PORT = 7670;
        public string Ip { get; private set; }
        public bool Connected { get; private set; }

        private TcpClient m_tcpClient = new TcpClient();
        private NetworkStream m_networkStream;

        public MappClient()
        {
            Connected = false;
            IniHandler iniHandler = new IniHandler(Application.StartupPath + "/config.ini");

            if (iniHandler.isExistIni() == false){
                MessageBox.Show("缺少 config.ini 檔案");
                System.Environment.Exit(System.Environment.ExitCode);
            }
            else {
                IPAddress ipAddress;
                string ipStr = iniHandler.ReadIniFile("IP", "MAPP_SERVER", "default");

                if (IPAddress.TryParse(ipStr, out ipAddress))
                    Ip = ipAddress.ToString();
                else
                    MessageBox.Show("MAPP_SERVER IP 非有效IP位址");
            }     
            
        }

        public void connectServer()
        {
            try { 
                m_tcpClient.Close();
                m_tcpClient = new TcpClient();
                m_tcpClient.Connect(IPAddress.Parse(Ip), PORT);
                Connected = true;
                writeData("$已連線");  //test  之後要刪除
            }
            catch (Exception ex) {
                Connected = false;
            }
        }

        public bool writeData(string msg)
        {
            byte[] msgByte;

            if (Connected) {
                try {
                    m_networkStream = m_tcpClient.GetStream();
                    msgByte = System.Text.Encoding.BigEndianUnicode.GetBytes(msg);
                    m_networkStream.Write(msgByte, 0, msgByte.Length);
                    return true;
                }
                catch (Exception ex) {
                    Connected = false;
                    return false;
                }
            }
            else
                return false;
        }

    }
}
