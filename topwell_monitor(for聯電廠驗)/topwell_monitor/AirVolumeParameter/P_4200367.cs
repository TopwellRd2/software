﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    //品號 4200367 的風量參數
    class P_4200367 : ParameterBase
    {
        public P_4200367()
        {
 
        }

        public override FFUParemeter getParameter(int speed)
        {
            if (speed == 0)
                return createParameter(0, 0, 0, 0);
            if (speed < 1100)
                return createParameter(-0.00000506, 0.00061708, -0.09396336, 20.70644346);
            else if (speed > 1100 && speed < 1200)
                return createParameter(-0.00000506, 0.00061708, -0.09396336, 20.70644346);
            else if (speed > 1200 && speed < 1300)
                return createParameter(-0.00000333, 0.00045136, -0.07659806, 22.17575409);
            else if (speed > 1300 && speed < 1400)
                return createParameter(-0.00000263, 0.00048455, -0.08562352, 24.70249857);
            else if (speed > 1400 && speed < 1500)
                return createParameter(-0.00000131, 0.00021181, -0.06145674, 26.05394524);
            else if (speed > 1500 && speed < 1600)
                return createParameter(-0.00000164, 0.00043839, -0.08570108, 28.59628910);
            else if (speed > 1600 && speed < 1700)
                return createParameter(-0.00000122, 0.00041107, -0.09046392, 30.91019395);
            else if (speed > 1700 && speed < 1800)
                return createParameter(-0.00000129, 0.00057496, -0.12993795, 35.87758532);
            else if (speed > 1800)
                return createParameter(-0.00000098, 0.00051425, -0.13314889, 38.76360605);

            return createParameter(0, 0, 0, 0);
        }
    }
}
