﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    //品號 4200577 的風量參數
    class P_4200577 : ParameterBase
    {
        public P_4200577()
        {
 
        }

        public override FFUParemeter getParameter(int speed)
        {
            if (speed == 0)
                return createParameter(0, 0, 0, 0);
            if (speed < 900)
                return createParameter(-0.00002618, 0.00402085, -0.34837574, 33.19357330);
            else if (speed >= 900 && speed < 1000)
                return createParameter(-0.00001619, 0.00330139, -0.34532055, 38.13575385);
            else if (speed >= 1000 && speed < 1100)
                return createParameter(-0.00000885, 0.00232203, -0.31646432, 42.95260547);
            else if (speed >= 1100 && speed <1200)
                return createParameter(-0.00000586, 0.00191920, -0.31164844, 48.30866095);
            else if (speed >= 1200 && speed < 1250)
                return createParameter(-0.00000439, 0.00172850, -0.31387558, 53.37211745);
            else if (speed >= 1250)
                return createParameter(-0.00000706, 0.00350422, -0.66067804, 76.36805132);

            return createParameter(0, 0, 0, 0);
        }
    }
}
