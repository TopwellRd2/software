﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    //品號 4200480 的風量參數
    public class P_4200480 : ParameterBase
    {
        public P_4200480()
        {
        }

        public override FFUParemeter getParameter(int speed)
        {
            if (speed == 0)
                return createParameter(0, 0, 0, 0);
            if (speed < 700)
                return createParameter(-0.00005516, 0.00388647, -0.28423862, 23.04376006);
            else if (speed > 700 && speed < 800)
                return createParameter(-0.00000956, 0.00068116, -0.19965719, 26.94124626);
            else if (speed > 800 && speed < 900)
                return createParameter(-0.00000092, -0.00031108, -0.12876871, 30.44874617);
            else if (speed > 900 && speed < 1000)
                return createParameter(-0.00000346, 0.00040115, -0.14311572, 34.41865593);
            else if (speed > 1000 && speed < 1100)
                return createParameter(-0.00000624, 0.00147754, -0.22459773, 40.54569066);
            else if (speed > 1100 && speed < 1230)
                return createParameter(-0.00000353, 0.00103884, -0.20155340, 44.59948281);
            else if (speed > 1230 && speed < 1235)
                return createParameter(-0.00000569, 0.00287273, -0.58116116, 72.95484125);
            else if (speed > 1235 && speed < 1240)
                return createParameter(-0.00000741, 0.00367787, -0.69707223, 78.23644403);
            else if (speed > 1240 && speed < 1245)
                return createParameter(-0.00000617, 0.00309972, -0.61153966, 74.72044235);
            else if (speed > 1245 && speed < 1250)
                return createParameter(-0.00000674, 0.00339056, -0.65628157, 76.77302269);
            else if (speed > 1250)
                return createParameter(-0.00000689, 0.00353045, -0.68707130, 79.23332339);

            return createParameter(0, 0, 0, 0);
        }
    }
}
