﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    //品號 4200577 的風量參數
    class P_4200312 : ParameterBase
    {
        public P_4200312()
        {
 
        }

        public override FFUParemeter getParameter(int speed)
        {
            if (speed == 0)
                return createParameter(0, 0, 0, 0);
            if (speed < 1000)
                return createParameter(-0.00003667, 0.00585812, -0.45659551, 34.94623393);
            else if (speed >= 1000 && speed < 1200)
                return createParameter(-0.00001504, 0.00402415, -0.45916617, 45.91937710);
            else if (speed >= 1200)
                return createParameter(-0.00000631, 0.00240826, -0.37668331, 53.58851943);

            return createParameter(0, 0, 0, 0);
        }
    }
}
