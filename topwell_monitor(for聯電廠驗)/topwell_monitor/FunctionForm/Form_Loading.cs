﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public partial class Form_Loading : Form
    {
        public Form1 MainForm;

        public Form_Loading(Form1 form)
        {
            InitializeComponent();
            MainForm = form;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MainForm.m_monitor.firstGatewayAction();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            percentageLabel.Text = e.ProgressPercentage.ToString() + "%";
            if (e.ProgressPercentage > 50)
            {
                percentageLabel.BackColor = Color.FromArgb(27, 177, 27);
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
            MainForm.Show();
            MainForm.WindowState = FormWindowState.Normal;
        }
    }
}
