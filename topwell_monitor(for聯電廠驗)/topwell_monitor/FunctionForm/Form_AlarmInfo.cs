﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public partial class Form_AlarmInfo : Form
    {
        public Form_AlarmInfo()
        {
            InitializeComponent();
        }

        private void Form_AlarmInfo_Load(object sender, EventArgs e)
        {
            DataGridView1.Rows.Add(new object[] { "E1", "通訊錯誤(CPU)" });
            DataGridView1.Rows.Add(new object[] { "E2", "IPM過熱" });
            DataGridView1.Rows.Add(new object[] { "E3", "IPM溫度偵測錯誤" });
            DataGridView1.Rows.Add(new object[] { "E4", "IPM模組異常保護" });
            DataGridView1.Rows.Add(new object[] { "E9", "風扇啟動錯誤" });
        }

        private void Form_AlarmInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true; //關閉視窗時取消
            this.Hide(); //隱藏式窗,下次再show出
        }

    
    }
}
