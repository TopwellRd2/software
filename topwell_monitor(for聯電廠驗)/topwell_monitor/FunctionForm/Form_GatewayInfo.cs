﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public partial class Form_GatewayInfo : Form
    {
        public Form_GatewayInfo()
        {
            InitializeComponent();
        }

        private void Form_GatewayInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true; //關閉視窗時取消
            this.Hide(); //隱藏式窗,下次再show出
        }
    }
}
