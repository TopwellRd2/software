﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace topwell_monitor
{
    public partial class InputForm : Form
    {   
        public string ValueType { get; set; }

        public string BeforeValue { get; set; }

        public string RegionName { get; set; }


        Form1 Form1;

        public InputForm(Form1 form)
        {
            InitializeComponent();

            Form1 = form;
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {
            IniHandler iniHandler = new IniHandler(Application.StartupPath + "/config.ini");
           
            if (iniHandler.isExistIni() == false)
            {
                MessageBox.Show("缺少 config.ini 檔案");
                System.Environment.Exit(System.Environment.ExitCode);
            } else {
                string afterValue = valueTxt.Text;
                string regionName = RegionName;
                iniHandler.WriteIniFile("Area", regionName, afterValue);


                this.Close();
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
