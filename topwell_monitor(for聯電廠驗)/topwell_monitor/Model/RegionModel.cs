﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    public enum RegionStatus { Normal, Alarm, Stop, Disconnect }
    class RegionModel
    {
        public int NormalNum { get; private set; }
        public int StopNum { get; private set; }
        public int AlarmNum { get; private set; }
        public int BypassNum { get; private set; }
        public int DisconnectNum { get; private set; }

        public double TotalAirVolume { get; set; } 
        public double Area { get; set; }

        private List<Device> m_deviceList;
        public RegionStatus Status { get; private set; }

        public RegionModel(List<Device> deviceList, string area)
        {
            m_deviceList = deviceList;
            Status = RegionStatus.Disconnect;
            try
            {
                Area = Convert.ToDouble(area);
            }
            catch (Exception e)
            {
                Area = 0;
            }
        }

        public bool updateDevicesData(Dictionary<string, GatewayConnection> gatewayMap)
        {
            bool ret = true;

            foreach (var device in m_deviceList)
                ret = ret & device.updateModel(gatewayMap);

            NormalNum = 0;
            StopNum = 0;
            AlarmNum = 0;
            BypassNum = 0;
            DisconnectNum = 0;
            TotalAirVolume = 0;

            foreach (var device in m_deviceList)
            {
                TotalAirVolume += device.AirVolume;

                if (device.Status == DeviceStatusEnum.Normal)
                    NormalNum++;
                else if (device.Status == DeviceStatusEnum.FFUAlarm)
                    AlarmNum++;
                else if (device.Status == DeviceStatusEnum.Stop)
                    StopNum++;
                else if (device.Status == DeviceStatusEnum.Bypass)
                    BypassNum++;
                else if (device.Status == DeviceStatusEnum.Disconnect)
                    DisconnectNum++;
            }

            if (StopNum == 0 && AlarmNum == 0)
            {
                Status = RegionStatus.Normal;
            }

            if (StopNum > 0)
                Status = RegionStatus.Stop;
            if (DisconnectNum > 0)
                Status = RegionStatus.Disconnect;
            if (AlarmNum > 0)
                Status = RegionStatus.Alarm;

            return ret;
        }

        public int getProperRate()
        {
            if (NormalNum + AlarmNum != 0)
                return (int)((double)NormalNum / (NormalNum + AlarmNum)) * 100;
            else
                return 0;
        }

        //private void judgeStatus()
        //{
        //    foreach (var device in m_deviceList)
        //    {
        //        if (device.Status == DeviceStatusEnum.FFUAlarm || device.Status == DeviceStatusEnum.SMFAlarm)
        //        {
        //            Status = RegionStatus.Alarm;
        //            return;
        //        }
        //        else if (device.Status == DeviceStatusEnum.Stop)
        //        {
        //            Status = RegionStatus.Stop;
        //            return;
        //        }
        //        else if (device.Status == DeviceStatusEnum.Disconnect)
        //        {
        //            Status = RegionStatus.Disconnect;
        //            return;
        //        }
        //    }

        //    Status = RegionStatus.Normal;
        //}

        public List<Device> getDeviceList()
        {
            return m_deviceList;
        }
    }
}
