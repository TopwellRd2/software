﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace topwell_monitor
{
    static class XmlHandler
    {
        private static bool isRecord = false;
        private static readonly int[] HOURS = new int[] { 7, 23};
        private static readonly int MINUTE = 0;

        public static bool isTime2Export()
        {
            foreach (int hours in HOURS) {
                if (detectTime(hours))
                    return true;
            }
            return false;
        }

        private static bool detectTime(int hours)
        {
            DateTime now = DateTime.Now;

            if (isRecord) {
                if (now.Hour == hours && now.Minute == MINUTE + 1)
                    isRecord = false;
                return false;       
            }
            else {
                if (now.Hour == hours && now.Minute == MINUTE) {
                    isRecord = true;
                    return true;
                }   
            }
            return false;  
        }

        public static bool exportXml(string regionName, int properRate, string path)
        {
            XmlDocument xmlDoc;
            XmlElement edcElement;

            try {
                xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclare = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                xmlDoc.AppendChild(xmlDeclare);

                //建立根節點物件並加入 XmlDocument 中 (第0層)
                edcElement = xmlDoc.CreateElement("EDC");
                xmlDoc.AppendChild(edcElement);

                //開始寫檔
                writeAElementInEDC(xmlDoc, "glass_id", "STKFFU");
                writeAElementInEDC(xmlDoc, "group_id");
                writeAElementInEDC(xmlDoc, "lot_id");
                writeAElementInEDC(xmlDoc, "product_id", "SideFFU");
                writeAElementInEDC(xmlDoc, "pfcd");
                writeAElementInEDC(xmlDoc, "eqp_id", regionName);
                writeAElementInEDC(xmlDoc, "ec_code");
                writeAElementInEDC(xmlDoc, "route_no");
                writeAElementInEDC(xmlDoc, "route_version", "0000");
                writeAElementInEDC(xmlDoc, "owner", "PROD");
                writeAElementInEDC(xmlDoc, "recipe_id", "AMHS1000");
                writeAElementInEDC(xmlDoc, "operation", "1099");
                writeAElementInEDC(xmlDoc, "rtc_flag", "N");
                writeAElementInEDC(xmlDoc, "pnp");
                writeAElementInEDC(xmlDoc, "chamber", "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN");
                writeAElementInEDC(xmlDoc, "cassette_id", "AMHS");
                writeAElementInEDC(xmlDoc, "line_batch_id");
                writeAElementInEDC(xmlDoc, "cldate", DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString());
                writeAElementInEDC(xmlDoc, "cltime", DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString());
                writeAElementInEDC(xmlDoc, "mes_link_key");
                writeAElementInEDC(xmlDoc, "rework_count", "00");
                writeAElementInEDC(xmlDoc, "operator");
                writeAElementInEDC(xmlDoc, "reserve_field_1");
                writeAElementInEDC(xmlDoc, "reserve_field_2");
                writeAElementInEDC(xmlDoc, "datas");
                writeAElementInDatas(xmlDoc, "iary");
                writeAElementInIary(xmlDoc, "item_name", "FFU_RUN_Rate");
                writeAElementInIary(xmlDoc, "item_type", "X");
                writeAElementInIary(xmlDoc, "item_value", properRate.ToString());

                xmlDoc.Save(path + "/" + regionName + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xml");
            }
            catch (Exception ex) {
                return false;
            }
            return true;
        }

        private static void writeAElementInEDC(XmlDocument xmlDoc, string name, string innerTxt)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlElement element = xmlDoc.CreateElement(name); //建立一個節點 

            element.InnerText = innerTxt;
            root.AppendChild(element);
        }

        private static void writeAElementInEDC(XmlDocument xmlDoc, string name)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlElement element = xmlDoc.CreateElement(name); //建立一個節點 

            root.AppendChild(element);
        }

        private static void writeAElementInDatas(XmlDocument xmlDoc, string name)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlNode dataRoot = root.SelectSingleNode("datas"); //查詢<datas> 
            XmlElement element = xmlDoc.CreateElement(name); //建立一個節點 

            dataRoot.AppendChild(element);
            root.AppendChild(dataRoot);
        }

        private static void writeAElementInIary(XmlDocument xmlDoc, string name, string innerTxt)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlNode dataRoot = root.SelectSingleNode("datas"); //查詢<datas> 
            XmlNode iaryRoot = dataRoot.SelectSingleNode("iary"); //查詢<iary> 

            XmlElement element = xmlDoc.CreateElement(name); //建立一個節點 
            element.InnerText = innerTxt;

            iaryRoot.AppendChild(element);
            dataRoot.AppendChild(iaryRoot);
            root.AppendChild(dataRoot);
        }
    }
}
