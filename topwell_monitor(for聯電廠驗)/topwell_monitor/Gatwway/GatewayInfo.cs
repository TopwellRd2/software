﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public class GatewayInfo
    {
        public static readonly int REGISTER_TOTAL = 6;


        public ushort[] RegisterFFUCountAry { get; private set; }
        public ushort[] RegisterSMFCountAry { get; private set; }
        public ushort ReadLength { get; private set; }//讀取GATEWAY的資料數量 目前讀有6筆或10筆

        public GatewayInfo(ushort[] ffuCnt, ushort[] smfCnt, ReadLengthEnum readLength)
        {
            RegisterFFUCountAry = new ushort[REGISTER_TOTAL];
            RegisterSMFCountAry = new ushort[REGISTER_TOTAL];

            if (ffuCnt.Length != REGISTER_TOTAL || smfCnt.Length != REGISTER_TOTAL)
            {
                MessageBox.Show("Length Error");
                return;
            } 
            
            for (int i = 0; i < REGISTER_TOTAL; i++)
            {
                RegisterFFUCountAry[i] = ffuCnt[i];
                RegisterSMFCountAry[i] = smfCnt[i];
            }

            switch (readLength)
            {
            case ReadLengthEnum.Six:
                ReadLength = 6;
                break;
            case ReadLengthEnum.Ten :
                ReadLength = 10;
                break;
            }  
        }
    }
}
