﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;


namespace topwell_monitor
{
    public partial class Form1 : Form
    {
        public Monitor m_monitor;
        private LoginHandler m_loginHandler;
        private SizeHandler[] m_sizeHamdlerAry = new SizeHandler[4];
        InputForm InputForm;

        private Form_Login form_Login;
        private Form_AlarmInfo form_alarmInfo;
        public Form_GatewayInfo form_GatewayInfo;
        public Form_Alarm form_Alarm;
        public Form_Log form_Log;
        public Form_Loading form_loading;

        public Form1()
        {
            InitializeComponent();
            InitializeSizeHandler();
            InputForm = new InputForm(this);

            m_loginHandler = new LoginHandler();

            form_Login = new Form_Login(m_loginHandler, LoginBtn, LoginLabel); //登入
            form_alarmInfo = new Form_AlarmInfo();     //警報代碼資訊
            form_GatewayInfo = new Form_GatewayInfo(); //GATEWAY 資訊
            form_Alarm = new Form_Alarm();             //即時警報
            form_Log = new Form_Log();                 //歷史紀錄

            m_monitor = new Monitor(this);
            form_loading = new Form_Loading(this);        //進度畫面
            form_loading.Show();
            form_loading.backgroundWorker.RunWorkerAsync();

            RunLabel.BackColor = Info.NORMAL_COLOR;
            StopLabel.BackColor = Info.STOP_COLOR;
            AlarmLabel.BackColor = Info.ALARM_COLOR;
            DisconnectLabel.BackColor = Info.DISCONNECT_COLOR;

            #region  廠驗用
            TabPage2.Parent =  null;
            TabPage3.Parent = null;
            TabPage4.Parent = null;
            #endregion

            m_monitor.start();
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void InitializeSizeHandler()
        {
            m_sizeHamdlerAry[0] = new SizeHandler(new Panel[] {Panel1}, new PictureBox[] {PictureBox1}, new Button[] { OHGT01Btn, OHGT03Btn, STKT10Btn}, new PictureBox[] { maskPic01 }, 40);
            m_sizeHamdlerAry[1] = new SizeHandler(new Panel[] {Panel2}, new PictureBox[] {PictureBox2}, new Button[] { STKL21_L30Btn }, new PictureBox[] { }, 40);
            m_sizeHamdlerAry[2] = new SizeHandler(new Panel[] {Panel3}, new PictureBox[] {PictureBox3}, new Button[] { STKL25_L10Btn }, new PictureBox[] { }, 40);
            m_sizeHamdlerAry[3] = new SizeHandler(new Panel[] {Panel4}, new PictureBox[] {PictureBox4}, new Button[] { SORC01Btn}, new PictureBox[] {}, 40);

            foreach (SizeHandler sizeHandler in m_sizeHamdlerAry)
                sizeHandler.changeSize(20);
        }

        private void TrackBar1_Scroll(object sender, EventArgs e)
        {
            foreach (SizeHandler sizeHandler in m_sizeHamdlerAry)
                sizeHandler.changeSize(TrackBar1.Value);
        }

        private void LoginDetectTimer_Tick(object sender, EventArgs e)
        {
            m_loginHandler.detect();

            if (m_loginHandler.isLogin == false)
            {
                LoginBtn.Text = "登入";
                LoginLabel.Text = "未登入";
                LoginLabel.ForeColor = Color.Red;
            }
        }

        
        private void LoginBtn_Click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin)
            {
                m_loginHandler.isLogin = false;
                LoginBtn.Text = "登入";
                LoginLabel.Text = "未登入";
                LoginLabel.ForeColor = Color.Red;
            }
            else
                form_Login.ShowDialog();
        }

        private void AlarmBtn_Click(object sender, EventArgs e)
        {
            form_Alarm.BringToFront();
            form_Alarm.Show();
        }

        private void GatewayInfoBtn_Click(object sender, EventArgs e)
        {
            form_GatewayInfo.BringToFront();
            form_GatewayInfo.Show();
        }

        private void AlarmCodeInfoBtn_Click(object sender, EventArgs e)
        {
            form_alarmInfo.BringToFront();
            form_alarmInfo.Show();
        }

        private void LogBtn_Click(object sender, EventArgs e)
        {
            try
            {
                form_Log.BringToFront();
                form_Log.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("確定關閉監控系統?", "警告", MessageBoxButtons.YesNo) == DialogResult.No)
                e.Cancel = true;
        }

        public LoginHandler getLoginHandler()
        {
            return m_loginHandler;
        }

        private void InfoDataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex != 8)
            {
                return;
            }

            if (m_loginHandler.isLogin == false) {
                MessageBox.Show("沒有權限,請登入");
                return;
            }

            string value = InfoDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            InputForm.BeforeValue = value;
            InputForm.ValueType = InfoDataGrid.Columns[e.ColumnIndex].HeaderText;
            InputForm.RegionName = InfoDataGrid.Rows[e.RowIndex].Cells[0].Value.ToString();

            InputForm.valueTxt.Text = InfoDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            InputForm.typeLabel.Text = InfoDataGrid.Columns[e.ColumnIndex].HeaderText + " : ";

            InputForm.ShowDialog();
        }

    }
}
