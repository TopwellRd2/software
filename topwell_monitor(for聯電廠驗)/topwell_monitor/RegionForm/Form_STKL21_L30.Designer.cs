﻿namespace topwell_monitor
{
    partial class Form_STKL21_L30
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_STKL21_L30));
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Btn_1_12 = new System.Windows.Forms.Button();
            this.Btn_1_2 = new System.Windows.Forms.Button();
            this.Btn_1_9 = new System.Windows.Forms.Button();
            this.Btn_1_10 = new System.Windows.Forms.Button();
            this.Btn_1_11 = new System.Windows.Forms.Button();
            this.Btn_1_1 = new System.Windows.Forms.Button();
            this.Btn_1_5 = new System.Windows.Forms.Button();
            this.Btn_1_7 = new System.Windows.Forms.Button();
            this.Btn_1_4 = new System.Windows.Forms.Button();
            this.Btn_1_8 = new System.Windows.Forms.Button();
            this.Btn_1_3 = new System.Windows.Forms.Button();
            this.Btn_1_6 = new System.Windows.Forms.Button();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Btn_2_15 = new System.Windows.Forms.Button();
            this.Btn_2_12 = new System.Windows.Forms.Button();
            this.Btn_2_13 = new System.Windows.Forms.Button();
            this.Btn_2_14 = new System.Windows.Forms.Button();
            this.Btn_2_11 = new System.Windows.Forms.Button();
            this.Btn_2_10 = new System.Windows.Forms.Button();
            this.Btn_2_9 = new System.Windows.Forms.Button();
            this.Btn_2_8 = new System.Windows.Forms.Button();
            this.Btn_2_7 = new System.Windows.Forms.Button();
            this.Btn_2_2 = new System.Windows.Forms.Button();
            this.Btn_2_3 = new System.Windows.Forms.Button();
            this.Btn_2_6 = new System.Windows.Forms.Button();
            this.Btn_2_4 = new System.Windows.Forms.Button();
            this.Btn_2_5 = new System.Windows.Forms.Button();
            this.Btn_2_1 = new System.Windows.Forms.Button();
            this.Btn_2_27 = new System.Windows.Forms.Button();
            this.Btn_2_26 = new System.Windows.Forms.Button();
            this.Btn_2_25 = new System.Windows.Forms.Button();
            this.Btn_2_24 = new System.Windows.Forms.Button();
            this.Btn_2_23 = new System.Windows.Forms.Button();
            this.Btn_2_22 = new System.Windows.Forms.Button();
            this.Btn_2_21 = new System.Windows.Forms.Button();
            this.Btn_2_20 = new System.Windows.Forms.Button();
            this.Btn_2_19 = new System.Windows.Forms.Button();
            this.Btn_2_18 = new System.Windows.Forms.Button();
            this.Btn_2_17 = new System.Windows.Forms.Button();
            this.Btn_2_16 = new System.Windows.Forms.Button();
            this.PictureBox3 = new System.Windows.Forms.PictureBox();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.TrackBar1 = new System.Windows.Forms.TrackBar();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.TabPage2.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TabControl1.Location = new System.Drawing.Point(136, 2);
            this.TabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(1400, 874);
            this.TabControl1.TabIndex = 33;
            // 
            // TabPage1
            // 
            this.TabPage1.AutoScroll = true;
            this.TabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.TabPage1.Controls.Add(this.Panel1);
            this.TabPage1.Location = new System.Drawing.Point(4, 36);
            this.TabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.TabPage1.Size = new System.Drawing.Size(1392, 834);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "一般";
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.Btn_1_12);
            this.Panel1.Controls.Add(this.Btn_1_2);
            this.Panel1.Controls.Add(this.Btn_1_9);
            this.Panel1.Controls.Add(this.Btn_1_10);
            this.Panel1.Controls.Add(this.Btn_1_11);
            this.Panel1.Controls.Add(this.Btn_1_1);
            this.Panel1.Controls.Add(this.Btn_1_5);
            this.Panel1.Controls.Add(this.Btn_1_7);
            this.Panel1.Controls.Add(this.Btn_1_4);
            this.Panel1.Controls.Add(this.Btn_1_8);
            this.Panel1.Controls.Add(this.Btn_1_3);
            this.Panel1.Controls.Add(this.Btn_1_6);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Location = new System.Drawing.Point(49, 6);
            this.Panel1.Margin = new System.Windows.Forms.Padding(4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1292, 779);
            this.Panel1.TabIndex = 31;
            // 
            // Btn_1_12
            // 
            this.Btn_1_12.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_12.Location = new System.Drawing.Point(913, 722);
            this.Btn_1_12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_12.Name = "Btn_1_12";
            this.Btn_1_12.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_12.TabIndex = 33;
            this.Btn_1_12.Text = "12";
            this.Btn_1_12.UseVisualStyleBackColor = true;
            // 
            // Btn_1_2
            // 
            this.Btn_1_2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_2.Location = new System.Drawing.Point(1082, 16);
            this.Btn_1_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_2.Name = "Btn_1_2";
            this.Btn_1_2.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_2.TabIndex = 20;
            this.Btn_1_2.Text = "02";
            this.Btn_1_2.UseVisualStyleBackColor = true;
            // 
            // Btn_1_9
            // 
            this.Btn_1_9.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_9.Location = new System.Drawing.Point(1136, 720);
            this.Btn_1_9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_9.Name = "Btn_1_9";
            this.Btn_1_9.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_9.TabIndex = 27;
            this.Btn_1_9.Text = "09";
            this.Btn_1_9.UseVisualStyleBackColor = true;
            // 
            // Btn_1_10
            // 
            this.Btn_1_10.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_10.Location = new System.Drawing.Point(1064, 722);
            this.Btn_1_10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_10.Name = "Btn_1_10";
            this.Btn_1_10.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_10.TabIndex = 28;
            this.Btn_1_10.Text = "10";
            this.Btn_1_10.UseVisualStyleBackColor = true;
            // 
            // Btn_1_11
            // 
            this.Btn_1_11.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_11.Location = new System.Drawing.Point(996, 722);
            this.Btn_1_11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_11.Name = "Btn_1_11";
            this.Btn_1_11.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_11.TabIndex = 29;
            this.Btn_1_11.Text = "11";
            this.Btn_1_11.UseVisualStyleBackColor = true;
            // 
            // Btn_1_1
            // 
            this.Btn_1_1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_1.Location = new System.Drawing.Point(976, 11);
            this.Btn_1_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_1.Name = "Btn_1_1";
            this.Btn_1_1.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_1.TabIndex = 19;
            this.Btn_1_1.Text = "01";
            this.Btn_1_1.UseVisualStyleBackColor = true;
            // 
            // Btn_1_5
            // 
            this.Btn_1_5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_5.Location = new System.Drawing.Point(1162, 175);
            this.Btn_1_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_5.Name = "Btn_1_5";
            this.Btn_1_5.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_5.TabIndex = 23;
            this.Btn_1_5.Text = "05";
            this.Btn_1_5.UseVisualStyleBackColor = true;
            // 
            // Btn_1_7
            // 
            this.Btn_1_7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_7.Location = new System.Drawing.Point(1162, 299);
            this.Btn_1_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_7.Name = "Btn_1_7";
            this.Btn_1_7.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_7.TabIndex = 25;
            this.Btn_1_7.Text = "07";
            this.Btn_1_7.UseVisualStyleBackColor = true;
            // 
            // Btn_1_4
            // 
            this.Btn_1_4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_4.Location = new System.Drawing.Point(1162, 105);
            this.Btn_1_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_4.Name = "Btn_1_4";
            this.Btn_1_4.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_4.TabIndex = 22;
            this.Btn_1_4.Text = "04";
            this.Btn_1_4.UseVisualStyleBackColor = true;
            // 
            // Btn_1_8
            // 
            this.Btn_1_8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_8.Location = new System.Drawing.Point(1162, 361);
            this.Btn_1_8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_8.Name = "Btn_1_8";
            this.Btn_1_8.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_8.TabIndex = 26;
            this.Btn_1_8.Text = "08";
            this.Btn_1_8.UseVisualStyleBackColor = true;
            // 
            // Btn_1_3
            // 
            this.Btn_1_3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_3.Location = new System.Drawing.Point(1162, 46);
            this.Btn_1_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_3.Name = "Btn_1_3";
            this.Btn_1_3.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_3.TabIndex = 21;
            this.Btn_1_3.Text = "03";
            this.Btn_1_3.UseVisualStyleBackColor = true;
            // 
            // Btn_1_6
            // 
            this.Btn_1_6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_6.Location = new System.Drawing.Point(1162, 234);
            this.Btn_1_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_1_6.Name = "Btn_1_6";
            this.Btn_1_6.Size = new System.Drawing.Size(60, 38);
            this.Btn_1_6.TabIndex = 24;
            this.Btn_1_6.Text = "06";
            this.Btn_1_6.UseVisualStyleBackColor = true;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::topwell_monitor.Properties.Resources.STKL21_L30;
            this.PictureBox1.Location = new System.Drawing.Point(0, 0);
            this.PictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(1245, 776);
            this.PictureBox1.TabIndex = 30;
            this.PictureBox1.TabStop = false;
            // 
            // TabPage2
            // 
            this.TabPage2.AutoScroll = true;
            this.TabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.TabPage2.Controls.Add(this.Panel2);
            this.TabPage2.Location = new System.Drawing.Point(4, 36);
            this.TabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.TabPage2.Size = new System.Drawing.Size(1392, 834);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "軸流風扇";
            // 
            // Panel2
            // 
            this.Panel2.AutoScroll = true;
            this.Panel2.Controls.Add(this.Btn_2_15);
            this.Panel2.Controls.Add(this.Btn_2_12);
            this.Panel2.Controls.Add(this.Btn_2_13);
            this.Panel2.Controls.Add(this.Btn_2_14);
            this.Panel2.Controls.Add(this.Btn_2_11);
            this.Panel2.Controls.Add(this.Btn_2_10);
            this.Panel2.Controls.Add(this.Btn_2_9);
            this.Panel2.Controls.Add(this.Btn_2_8);
            this.Panel2.Controls.Add(this.Btn_2_7);
            this.Panel2.Controls.Add(this.Btn_2_2);
            this.Panel2.Controls.Add(this.Btn_2_3);
            this.Panel2.Controls.Add(this.Btn_2_6);
            this.Panel2.Controls.Add(this.Btn_2_4);
            this.Panel2.Controls.Add(this.Btn_2_5);
            this.Panel2.Controls.Add(this.Btn_2_1);
            this.Panel2.Controls.Add(this.Btn_2_27);
            this.Panel2.Controls.Add(this.Btn_2_26);
            this.Panel2.Controls.Add(this.Btn_2_25);
            this.Panel2.Controls.Add(this.Btn_2_24);
            this.Panel2.Controls.Add(this.Btn_2_23);
            this.Panel2.Controls.Add(this.Btn_2_22);
            this.Panel2.Controls.Add(this.Btn_2_21);
            this.Panel2.Controls.Add(this.Btn_2_20);
            this.Panel2.Controls.Add(this.Btn_2_19);
            this.Panel2.Controls.Add(this.Btn_2_18);
            this.Panel2.Controls.Add(this.Btn_2_17);
            this.Panel2.Controls.Add(this.Btn_2_16);
            this.Panel2.Controls.Add(this.PictureBox3);
            this.Panel2.Controls.Add(this.PictureBox2);
            this.Panel2.Location = new System.Drawing.Point(11, 9);
            this.Panel2.Margin = new System.Windows.Forms.Padding(4);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(1332, 746);
            this.Panel2.TabIndex = 0;
            // 
            // Btn_2_15
            // 
            this.Btn_2_15.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_15.Location = new System.Drawing.Point(188, 625);
            this.Btn_2_15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_15.Name = "Btn_2_15";
            this.Btn_2_15.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_15.TabIndex = 46;
            this.Btn_2_15.Text = "15";
            this.Btn_2_15.UseVisualStyleBackColor = true;
            // 
            // Btn_2_12
            // 
            this.Btn_2_12.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_12.Location = new System.Drawing.Point(188, 501);
            this.Btn_2_12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_12.Name = "Btn_2_12";
            this.Btn_2_12.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_12.TabIndex = 45;
            this.Btn_2_12.Text = "12";
            this.Btn_2_12.UseVisualStyleBackColor = true;
            // 
            // Btn_2_13
            // 
            this.Btn_2_13.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_13.Location = new System.Drawing.Point(188, 541);
            this.Btn_2_13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_13.Name = "Btn_2_13";
            this.Btn_2_13.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_13.TabIndex = 44;
            this.Btn_2_13.Text = "13";
            this.Btn_2_13.UseVisualStyleBackColor = true;
            // 
            // Btn_2_14
            // 
            this.Btn_2_14.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_14.Location = new System.Drawing.Point(187, 585);
            this.Btn_2_14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_14.Name = "Btn_2_14";
            this.Btn_2_14.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_14.TabIndex = 43;
            this.Btn_2_14.Text = "14";
            this.Btn_2_14.UseVisualStyleBackColor = true;
            // 
            // Btn_2_11
            // 
            this.Btn_2_11.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_11.Location = new System.Drawing.Point(188, 460);
            this.Btn_2_11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_11.Name = "Btn_2_11";
            this.Btn_2_11.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_11.TabIndex = 42;
            this.Btn_2_11.Text = "11";
            this.Btn_2_11.UseVisualStyleBackColor = true;
            // 
            // Btn_2_10
            // 
            this.Btn_2_10.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_10.Location = new System.Drawing.Point(189, 416);
            this.Btn_2_10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_10.Name = "Btn_2_10";
            this.Btn_2_10.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_10.TabIndex = 41;
            this.Btn_2_10.Text = "10";
            this.Btn_2_10.UseVisualStyleBackColor = true;
            // 
            // Btn_2_9
            // 
            this.Btn_2_9.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_9.Location = new System.Drawing.Point(189, 378);
            this.Btn_2_9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_9.Name = "Btn_2_9";
            this.Btn_2_9.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_9.TabIndex = 40;
            this.Btn_2_9.Text = "09";
            this.Btn_2_9.UseVisualStyleBackColor = true;
            // 
            // Btn_2_8
            // 
            this.Btn_2_8.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_8.Location = new System.Drawing.Point(189, 336);
            this.Btn_2_8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_8.Name = "Btn_2_8";
            this.Btn_2_8.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_8.TabIndex = 39;
            this.Btn_2_8.Text = "08";
            this.Btn_2_8.UseVisualStyleBackColor = true;
            // 
            // Btn_2_7
            // 
            this.Btn_2_7.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_7.Location = new System.Drawing.Point(189, 292);
            this.Btn_2_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_7.Name = "Btn_2_7";
            this.Btn_2_7.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_7.TabIndex = 38;
            this.Btn_2_7.Text = "07";
            this.Btn_2_7.UseVisualStyleBackColor = true;
            // 
            // Btn_2_2
            // 
            this.Btn_2_2.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_2.Location = new System.Drawing.Point(187, 78);
            this.Btn_2_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_2.Name = "Btn_2_2";
            this.Btn_2_2.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_2.TabIndex = 37;
            this.Btn_2_2.Text = "02";
            this.Btn_2_2.UseVisualStyleBackColor = true;
            // 
            // Btn_2_3
            // 
            this.Btn_2_3.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_3.Location = new System.Drawing.Point(188, 115);
            this.Btn_2_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_3.Name = "Btn_2_3";
            this.Btn_2_3.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_3.TabIndex = 36;
            this.Btn_2_3.Text = "03";
            this.Btn_2_3.UseVisualStyleBackColor = true;
            // 
            // Btn_2_6
            // 
            this.Btn_2_6.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_6.Location = new System.Drawing.Point(188, 250);
            this.Btn_2_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_6.Name = "Btn_2_6";
            this.Btn_2_6.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_6.TabIndex = 35;
            this.Btn_2_6.Text = "06";
            this.Btn_2_6.UseVisualStyleBackColor = true;
            // 
            // Btn_2_4
            // 
            this.Btn_2_4.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_4.Location = new System.Drawing.Point(188, 160);
            this.Btn_2_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_4.Name = "Btn_2_4";
            this.Btn_2_4.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_4.TabIndex = 34;
            this.Btn_2_4.Text = "04";
            this.Btn_2_4.UseVisualStyleBackColor = true;
            // 
            // Btn_2_5
            // 
            this.Btn_2_5.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_5.Location = new System.Drawing.Point(188, 204);
            this.Btn_2_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_5.Name = "Btn_2_5";
            this.Btn_2_5.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_5.TabIndex = 33;
            this.Btn_2_5.Text = "05";
            this.Btn_2_5.UseVisualStyleBackColor = true;
            // 
            // Btn_2_1
            // 
            this.Btn_2_1.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_1.Location = new System.Drawing.Point(188, 36);
            this.Btn_2_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_1.Name = "Btn_2_1";
            this.Btn_2_1.Size = new System.Drawing.Size(43, 31);
            this.Btn_2_1.TabIndex = 32;
            this.Btn_2_1.Text = "01";
            this.Btn_2_1.UseVisualStyleBackColor = true;
            // 
            // Btn_2_27
            // 
            this.Btn_2_27.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_27.Location = new System.Drawing.Point(1323, 334);
            this.Btn_2_27.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_27.Name = "Btn_2_27";
            this.Btn_2_27.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_27.TabIndex = 31;
            this.Btn_2_27.Text = "27";
            this.Btn_2_27.UseVisualStyleBackColor = true;
            // 
            // Btn_2_26
            // 
            this.Btn_2_26.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_26.Location = new System.Drawing.Point(1442, 337);
            this.Btn_2_26.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_26.Name = "Btn_2_26";
            this.Btn_2_26.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_26.TabIndex = 30;
            this.Btn_2_26.Text = "26";
            this.Btn_2_26.UseVisualStyleBackColor = true;
            // 
            // Btn_2_25
            // 
            this.Btn_2_25.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_25.Location = new System.Drawing.Point(1534, 337);
            this.Btn_2_25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_25.Name = "Btn_2_25";
            this.Btn_2_25.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_25.TabIndex = 29;
            this.Btn_2_25.Text = "25";
            this.Btn_2_25.UseVisualStyleBackColor = true;
            // 
            // Btn_2_24
            // 
            this.Btn_2_24.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_24.Location = new System.Drawing.Point(1322, 246);
            this.Btn_2_24.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_24.Name = "Btn_2_24";
            this.Btn_2_24.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_24.TabIndex = 28;
            this.Btn_2_24.Text = "24";
            this.Btn_2_24.UseVisualStyleBackColor = true;
            // 
            // Btn_2_23
            // 
            this.Btn_2_23.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_23.Location = new System.Drawing.Point(1442, 250);
            this.Btn_2_23.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_23.Name = "Btn_2_23";
            this.Btn_2_23.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_23.TabIndex = 27;
            this.Btn_2_23.Text = "23";
            this.Btn_2_23.UseVisualStyleBackColor = true;
            // 
            // Btn_2_22
            // 
            this.Btn_2_22.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_22.Location = new System.Drawing.Point(1535, 250);
            this.Btn_2_22.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_22.Name = "Btn_2_22";
            this.Btn_2_22.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_22.TabIndex = 26;
            this.Btn_2_22.Text = "22";
            this.Btn_2_22.UseVisualStyleBackColor = true;
            // 
            // Btn_2_21
            // 
            this.Btn_2_21.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_21.Location = new System.Drawing.Point(1322, 164);
            this.Btn_2_21.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_21.Name = "Btn_2_21";
            this.Btn_2_21.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_21.TabIndex = 25;
            this.Btn_2_21.Text = "21";
            this.Btn_2_21.UseVisualStyleBackColor = true;
            // 
            // Btn_2_20
            // 
            this.Btn_2_20.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_20.Location = new System.Drawing.Point(1442, 168);
            this.Btn_2_20.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_20.Name = "Btn_2_20";
            this.Btn_2_20.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_20.TabIndex = 24;
            this.Btn_2_20.Text = "20";
            this.Btn_2_20.UseVisualStyleBackColor = true;
            // 
            // Btn_2_19
            // 
            this.Btn_2_19.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_19.Location = new System.Drawing.Point(1536, 168);
            this.Btn_2_19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_19.Name = "Btn_2_19";
            this.Btn_2_19.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_19.TabIndex = 23;
            this.Btn_2_19.Text = "19";
            this.Btn_2_19.UseVisualStyleBackColor = true;
            // 
            // Btn_2_18
            // 
            this.Btn_2_18.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_18.Location = new System.Drawing.Point(1444, 82);
            this.Btn_2_18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_18.Name = "Btn_2_18";
            this.Btn_2_18.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_18.TabIndex = 22;
            this.Btn_2_18.Text = "18";
            this.Btn_2_18.UseVisualStyleBackColor = true;
            // 
            // Btn_2_17
            // 
            this.Btn_2_17.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_17.Location = new System.Drawing.Point(1549, 526);
            this.Btn_2_17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_17.Name = "Btn_2_17";
            this.Btn_2_17.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_17.TabIndex = 21;
            this.Btn_2_17.Text = "17";
            this.Btn_2_17.UseVisualStyleBackColor = true;
            // 
            // Btn_2_16
            // 
            this.Btn_2_16.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_2_16.Location = new System.Drawing.Point(1444, 433);
            this.Btn_2_16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_2_16.Name = "Btn_2_16";
            this.Btn_2_16.Size = new System.Drawing.Size(65, 56);
            this.Btn_2_16.TabIndex = 20;
            this.Btn_2_16.Text = "16";
            this.Btn_2_16.UseVisualStyleBackColor = true;
            // 
            // PictureBox3
            // 
            this.PictureBox3.Image = global::topwell_monitor.Properties.Resources.STKL21_L30_3;
            this.PictureBox3.Location = new System.Drawing.Point(421, 4);
            this.PictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox3.Name = "PictureBox3";
            this.PictureBox3.Size = new System.Drawing.Size(1300, 980);
            this.PictureBox3.TabIndex = 1;
            this.PictureBox3.TabStop = false;
            // 
            // PictureBox2
            // 
            this.PictureBox2.Image = global::topwell_monitor.Properties.Resources.STKL21_L30_2;
            this.PictureBox2.Location = new System.Drawing.Point(16, 4);
            this.PictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(383, 1090);
            this.PictureBox2.TabIndex = 0;
            this.PictureBox2.TabStop = false;
            // 
            // TrackBar1
            // 
            this.TrackBar1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TrackBar1.Location = new System.Drawing.Point(6, 156);
            this.TrackBar1.Margin = new System.Windows.Forms.Padding(4);
            this.TrackBar1.Maximum = 40;
            this.TrackBar1.Minimum = 1;
            this.TrackBar1.Name = "TrackBar1";
            this.TrackBar1.Size = new System.Drawing.Size(127, 56);
            this.TrackBar1.TabIndex = 34;
            this.TrackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.TrackBar1.Value = 30;
            // 
            // Form_STKL21_L30
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1547, 883);
            this.Controls.Add(this.TrackBar1);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_STKL21_L30";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "STKL21_L30";
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.TabPage2.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button Btn_1_12;
        internal System.Windows.Forms.Button Btn_1_2;
        internal System.Windows.Forms.Button Btn_1_9;
        internal System.Windows.Forms.Button Btn_1_10;
        internal System.Windows.Forms.Button Btn_1_11;
        internal System.Windows.Forms.Button Btn_1_1;
        internal System.Windows.Forms.Button Btn_1_5;
        internal System.Windows.Forms.Button Btn_1_7;
        internal System.Windows.Forms.Button Btn_1_4;
        internal System.Windows.Forms.Button Btn_1_8;
        internal System.Windows.Forms.Button Btn_1_3;
        internal System.Windows.Forms.Button Btn_1_6;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Button Btn_2_15;
        internal System.Windows.Forms.Button Btn_2_12;
        internal System.Windows.Forms.Button Btn_2_13;
        internal System.Windows.Forms.Button Btn_2_14;
        internal System.Windows.Forms.Button Btn_2_11;
        internal System.Windows.Forms.Button Btn_2_10;
        internal System.Windows.Forms.Button Btn_2_9;
        internal System.Windows.Forms.Button Btn_2_8;
        internal System.Windows.Forms.Button Btn_2_7;
        internal System.Windows.Forms.Button Btn_2_2;
        internal System.Windows.Forms.Button Btn_2_3;
        internal System.Windows.Forms.Button Btn_2_6;
        internal System.Windows.Forms.Button Btn_2_4;
        internal System.Windows.Forms.Button Btn_2_5;
        internal System.Windows.Forms.Button Btn_2_1;
        internal System.Windows.Forms.Button Btn_2_27;
        internal System.Windows.Forms.Button Btn_2_26;
        internal System.Windows.Forms.Button Btn_2_25;
        internal System.Windows.Forms.Button Btn_2_24;
        internal System.Windows.Forms.Button Btn_2_23;
        internal System.Windows.Forms.Button Btn_2_22;
        internal System.Windows.Forms.Button Btn_2_21;
        internal System.Windows.Forms.Button Btn_2_20;
        internal System.Windows.Forms.Button Btn_2_19;
        internal System.Windows.Forms.Button Btn_2_18;
        internal System.Windows.Forms.Button Btn_2_17;
        internal System.Windows.Forms.Button Btn_2_16;
        internal System.Windows.Forms.PictureBox PictureBox3;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.TrackBar TrackBar1;
    }
}