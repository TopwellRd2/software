﻿namespace topwell_monitor
{
    partial class Form_OHGT03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_OHGT03));
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Btn_1_13 = new System.Windows.Forms.Button();
            this.Btn_1_12 = new System.Windows.Forms.Button();
            this.Btn_1_11 = new System.Windows.Forms.Button();
            this.Btn_1_10 = new System.Windows.Forms.Button();
            this.Btn_1_2 = new System.Windows.Forms.Button();
            this.Btn_1_1 = new System.Windows.Forms.Button();
            this.Btn_1_8 = new System.Windows.Forms.Button();
            this.Btn_1_9 = new System.Windows.Forms.Button();
            this.Btn_1_3 = new System.Windows.Forms.Button();
            this.Btn_1_6 = new System.Windows.Forms.Button();
            this.Btn_1_7 = new System.Windows.Forms.Button();
            this.Btn_1_5 = new System.Windows.Forms.Button();
            this.Btn_1_4 = new System.Windows.Forms.Button();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TrackBar1 = new System.Windows.Forms.TrackBar();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.AutoScroll = true;
            this.Panel1.Controls.Add(this.Btn_1_13);
            this.Panel1.Controls.Add(this.Btn_1_12);
            this.Panel1.Controls.Add(this.Btn_1_11);
            this.Panel1.Controls.Add(this.Btn_1_10);
            this.Panel1.Controls.Add(this.Btn_1_2);
            this.Panel1.Controls.Add(this.Btn_1_1);
            this.Panel1.Controls.Add(this.Btn_1_8);
            this.Panel1.Controls.Add(this.Btn_1_9);
            this.Panel1.Controls.Add(this.Btn_1_3);
            this.Panel1.Controls.Add(this.Btn_1_6);
            this.Panel1.Controls.Add(this.Btn_1_7);
            this.Panel1.Controls.Add(this.Btn_1_5);
            this.Panel1.Controls.Add(this.Btn_1_4);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Location = new System.Drawing.Point(16, 26);
            this.Panel1.Margin = new System.Windows.Forms.Padding(4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1433, 464);
            this.Panel1.TabIndex = 2;
            // 
            // Btn_1_13
            // 
            this.Btn_1_13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_13.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_13.Location = new System.Drawing.Point(352, 20);
            this.Btn_1_13.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_13.Name = "Btn_1_13";
            this.Btn_1_13.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_13.TabIndex = 21;
            this.Btn_1_13.Text = "13";
            this.Btn_1_13.UseVisualStyleBackColor = true;
            // 
            // Btn_1_12
            // 
            this.Btn_1_12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_12.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_12.Location = new System.Drawing.Point(538, 19);
            this.Btn_1_12.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_12.Name = "Btn_1_12";
            this.Btn_1_12.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_12.TabIndex = 20;
            this.Btn_1_12.Text = "12";
            this.Btn_1_12.UseVisualStyleBackColor = true;
            // 
            // Btn_1_11
            // 
            this.Btn_1_11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_11.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_11.Location = new System.Drawing.Point(635, 19);
            this.Btn_1_11.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_11.Name = "Btn_1_11";
            this.Btn_1_11.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_11.TabIndex = 19;
            this.Btn_1_11.Text = "11";
            this.Btn_1_11.UseVisualStyleBackColor = true;
            // 
            // Btn_1_10
            // 
            this.Btn_1_10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_10.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_10.Location = new System.Drawing.Point(822, 19);
            this.Btn_1_10.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_10.Name = "Btn_1_10";
            this.Btn_1_10.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_10.TabIndex = 18;
            this.Btn_1_10.Text = "10";
            this.Btn_1_10.UseVisualStyleBackColor = true;
            // 
            // Btn_1_2
            // 
            this.Btn_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_2.Location = new System.Drawing.Point(1768, 19);
            this.Btn_1_2.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_2.Name = "Btn_1_2";
            this.Btn_1_2.Size = new System.Drawing.Size(87, 129);
            this.Btn_1_2.TabIndex = 10;
            this.Btn_1_2.Text = "02";
            this.Btn_1_2.UseVisualStyleBackColor = true;
            // 
            // Btn_1_1
            // 
            this.Btn_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_1.Location = new System.Drawing.Point(1955, 19);
            this.Btn_1_1.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_1.Name = "Btn_1_1";
            this.Btn_1_1.Size = new System.Drawing.Size(87, 129);
            this.Btn_1_1.TabIndex = 2;
            this.Btn_1_1.Text = "01";
            this.Btn_1_1.UseVisualStyleBackColor = true;
            // 
            // Btn_1_8
            // 
            this.Btn_1_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_8.Location = new System.Drawing.Point(1012, 19);
            this.Btn_1_8.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_8.Name = "Btn_1_8";
            this.Btn_1_8.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_8.TabIndex = 16;
            this.Btn_1_8.Text = "08";
            this.Btn_1_8.UseVisualStyleBackColor = true;
            // 
            // Btn_1_9
            // 
            this.Btn_1_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_9.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_9.Location = new System.Drawing.Point(917, 19);
            this.Btn_1_9.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_9.Name = "Btn_1_9";
            this.Btn_1_9.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_9.TabIndex = 17;
            this.Btn_1_9.Text = "09";
            this.Btn_1_9.UseVisualStyleBackColor = true;
            // 
            // Btn_1_3
            // 
            this.Btn_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_3.Location = new System.Drawing.Point(1673, 19);
            this.Btn_1_3.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_3.Name = "Btn_1_3";
            this.Btn_1_3.Size = new System.Drawing.Size(87, 129);
            this.Btn_1_3.TabIndex = 11;
            this.Btn_1_3.Text = "03";
            this.Btn_1_3.UseVisualStyleBackColor = true;
            // 
            // Btn_1_6
            // 
            this.Btn_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_6.Location = new System.Drawing.Point(1202, 19);
            this.Btn_1_6.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_6.Name = "Btn_1_6";
            this.Btn_1_6.Size = new System.Drawing.Size(87, 129);
            this.Btn_1_6.TabIndex = 14;
            this.Btn_1_6.Text = "06";
            this.Btn_1_6.UseVisualStyleBackColor = true;
            // 
            // Btn_1_7
            // 
            this.Btn_1_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_7.Location = new System.Drawing.Point(1107, 19);
            this.Btn_1_7.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_7.Name = "Btn_1_7";
            this.Btn_1_7.Size = new System.Drawing.Size(87, 128);
            this.Btn_1_7.TabIndex = 15;
            this.Btn_1_7.Text = "07";
            this.Btn_1_7.UseVisualStyleBackColor = true;
            // 
            // Btn_1_5
            // 
            this.Btn_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_5.Location = new System.Drawing.Point(1390, 19);
            this.Btn_1_5.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_5.Name = "Btn_1_5";
            this.Btn_1_5.Size = new System.Drawing.Size(87, 129);
            this.Btn_1_5.TabIndex = 13;
            this.Btn_1_5.Text = "05";
            this.Btn_1_5.UseVisualStyleBackColor = true;
            // 
            // Btn_1_4
            // 
            this.Btn_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_4.Location = new System.Drawing.Point(1486, 19);
            this.Btn_1_4.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_4.Name = "Btn_1_4";
            this.Btn_1_4.Size = new System.Drawing.Size(87, 129);
            this.Btn_1_4.TabIndex = 12;
            this.Btn_1_4.Text = "04";
            this.Btn_1_4.UseVisualStyleBackColor = true;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::topwell_monitor.Properties.Resources.OHGT03;
            this.PictureBox1.Location = new System.Drawing.Point(0, 0);
            this.PictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(2400, 384);
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            // 
            // TrackBar1
            // 
            this.TrackBar1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TrackBar1.Location = new System.Drawing.Point(16, 529);
            this.TrackBar1.Margin = new System.Windows.Forms.Padding(4);
            this.TrackBar1.Maximum = 40;
            this.TrackBar1.Minimum = 1;
            this.TrackBar1.Name = "TrackBar1";
            this.TrackBar1.Size = new System.Drawing.Size(333, 56);
            this.TrackBar1.TabIndex = 22;
            this.TrackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.TrackBar1.Value = 30;
            // 
            // Form_OHGT03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1604, 671);
            this.Controls.Add(this.TrackBar1);
            this.Controls.Add(this.Panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_OHGT03";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OHGT03";
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button Btn_1_13;
        internal System.Windows.Forms.Button Btn_1_12;
        internal System.Windows.Forms.Button Btn_1_11;
        internal System.Windows.Forms.Button Btn_1_10;
        internal System.Windows.Forms.Button Btn_1_2;
        internal System.Windows.Forms.Button Btn_1_1;
        internal System.Windows.Forms.Button Btn_1_8;
        internal System.Windows.Forms.Button Btn_1_9;
        internal System.Windows.Forms.Button Btn_1_3;
        internal System.Windows.Forms.Button Btn_1_6;
        internal System.Windows.Forms.Button Btn_1_7;
        internal System.Windows.Forms.Button Btn_1_5;
        internal System.Windows.Forms.Button Btn_1_4;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TrackBar TrackBar1;
    }
}