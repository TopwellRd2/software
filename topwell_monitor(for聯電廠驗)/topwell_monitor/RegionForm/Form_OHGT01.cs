﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public partial class Form_OHGT01 : Form
    {
        public Form_OHGT01()
        {
            InitializeComponent();

            #region  廠驗用
            Btn_1_5.Visible = false;
            Btn_1_6.Visible = false;
            Btn_1_7.Visible = false;
            Btn_1_8.Visible = false;
            Btn_1_9.Visible = false;
            #endregion
        }
    }
}
