﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db_handler
{
    public partial class SignInForm : Form
    {
        private Form1 form1;

        public SignInForm(Form1 form)
        {
            InitializeComponent();

            form1 = form;
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {
            if (accountTxt.Text == form1.ACCOUNT)
            {
                if (passwordTxt.Text == form1.PASSWORD)
                {
                    form1.IsSignIn = true;
                    form1.signinBtn.Text = "登出";
                }
                else
                {
                    form1.IsSignIn = false;
                    MessageBox.Show("密碼錯誤");
                }
            }
            else
            {
                form1.IsSignIn = false;
                MessageBox.Show("帳號錯誤");
            }

            this.Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
