﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace db_handler
{
    public partial class Form1 : Form
    {
        public readonly string ACCOUNT = "admin";

        public readonly string PASSWORD = "1234";

        public bool IsSignIn { get; set; }

        SqlConnection sqlConnection;

        InputForm InputForm;

        SignInForm SignInForm;

        public string Type { get; private set; }

        public string CaseNumber { get; private set; }

        public Form1()
        {
            InitializeComponent();

            InputForm = new InputForm(this);

            SignInForm = new SignInForm(this);

            IsSignIn = false;

            typeComboBox.SelectedIndex = 0;

            connectDB();
        }

        private void connectDB()
        {
            string connectionString = "Data Source=(local);Integrated Security=SSPI;";

            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void queryBtn_Click(object sender, EventArgs e)
        {
            queryDB();
        }

        public void queryDB()
        {
            Type = typeComboBox.SelectedItem.ToString();
            CaseNumber = caseNumberTxt.Text;

            string strCmd = "";

            if (Type == "AC")
            {
                strCmd = $"SELECT A.[日期], " +
                $"A.[時間]" +
                $",[產品代碼]" +
                $",A.[箱體編號]" +
                $",[馬達編號]" +
                $",[控制器編號]" +
                $",[一速電壓]" +
                $",[一速電流]" +
                $",[一速功率]" +
                $",[一速保護電流]" +
                $",[一速震動]" +
                $",[二速電壓]" +
                $",[二速電流]" +
                $",[二速功率]" +
                $",[二速保護電流]" +
                $",[二速震動]" +
                $",[三速電壓]" +
                $",[三速電流]" +
                $",[三速功率]" +
                $",[三速保護電流]" +
                $",[三速震動]" +
                $",[四速電壓]" +
                $",[四速電流]" +
                $",[四速功率] " +
                $",[四速保護電流] " +
                $",[四速震動]" +
                $",[外觀檢測]" +
                $",[運轉異音]" +
                $",[備註]  " +
                $"from (select 箱體編號,時間,日期 " +
                $"FROM [{Type}].[dbo].[{Type}] " +
                $"where [產品代碼] = '{CaseNumber}' " +
                $"and concat(replace(日期,'/',''),replace(時間,':','')) in (select max(concat(replace(日期,'/',''),replace(時間,':',''))) " +
                $"FROM [{Type}].[dbo].[{Type}] " +
                $"where [產品代碼] = '{CaseNumber}' " +
                $"group by 箱體編號)) A " +
                $"left join [{Type}].[dbo].[{Type}] B on  A.[箱體編號] = B.箱體編號 and A.時間 = B.時間 and A.日期 = B.日期";
            }
            else
            {
                strCmd = $"SELECT A.[日期], " +
                $"A.[時間]" +
                $",[產品代碼]" +
                $",A.[箱體編號]" +
                $",[馬達編號]" +
                $",[控制器編號]" +
                $",[出廠電壓]" +
                $",[出廠電流]" +
                $",[出廠功率]" +
                $",[出廠震動]" +
                $",[出廠轉速]" +
                $",[出廠頻率]" +
                $",[最高電壓]" +
                $",[最高電流]" +
                $",[最高功率]" +
                $",[最高震動]" +
                $",[最高轉速]" +
                $",[外觀檢測]" +
                $",[運轉異音]" +
                $",[使用者]  " +
                $",[備註]  " +
                $"from (select 箱體編號,時間,日期 " +
                $"FROM [{Type}].[dbo].[{Type}] " +
                $"where [產品代碼] = '{CaseNumber}' " +
                $"and concat(replace(日期,'/',''),replace(時間,':','')) in (select max(concat(replace(日期,'/',''),replace(時間,':',''))) " +
                $"FROM [{Type}].[dbo].[{Type}] " +
                $"where [產品代碼] = '{CaseNumber}' " +
                $"group by 箱體編號)) A " +
                $"left join [{Type}].[dbo].[{Type}] B on  A.[箱體編號] = B.箱體編號 and A.時間 = B.時間 and A.日期 = B.日期";
            }
            

            try
            {
                SqlCommand command = new SqlCommand(strCmd, sqlConnection);

                DataTable dataTable = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dataTable);
                dataGridView.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }

        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsSignIn == false)
                return;

            if (Type == "AC")
            {
                if (e.RowIndex == -1 || e.ColumnIndex < 6 || e.ColumnIndex == 28)
                    return;
            }
            else
            {
                if (e.RowIndex == -1 || e.ColumnIndex < 6 || e.ColumnIndex == 20)
                    return;
            }
            
            string boxNumber = dataGridView.Rows[e.RowIndex].Cells[3].Value.ToString();
            string value = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            InputForm.CaseNumber = CaseNumber;
            InputForm.BoxName = boxNumber;
            InputForm.Type = Type;
            InputForm.BeforeValue = value;
            InputForm.ValueType = dataGridView.Columns[e.ColumnIndex].HeaderText;

            InputForm.valueTxt.Text = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            InputForm.typeLabel.Text = dataGridView.Columns[e.ColumnIndex].HeaderText + " : ";
            
            InputForm.ShowDialog();
        }

        private void signinBtn_Click(object sender, EventArgs e)
        {
            if (IsSignIn == true)
            {
                IsSignIn = false;
                signinBtn.Text = "登入";
            }
            else
            {
                SignInForm.accountTxt.Text = "";
                SignInForm.passwordTxt.Text = "";

                SignInForm.ShowDialog();
            }
        }
    }
}
