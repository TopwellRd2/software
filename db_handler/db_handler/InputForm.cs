﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace db_handler
{
    public partial class InputForm : Form
    {
        SqlConnection sqlConnection;

        public string CaseNumber { get; set; }

        public string Type { get; set; }  //DC or AC or EBM

        public string ValueType { get; set; }

        public string BoxName { get; set; }

        public string BeforeValue { get; set; }


        Form1 Form1;

        public InputForm(Form1 form)
        {
            InitializeComponent();

            Form1 = form;
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {
            connectDB();

            string afterValue = valueTxt.Text;

            string strCmd = $"Update [{Type}].[dbo].[{Type}] " +
                            $"Set {ValueType} = '{valueTxt.Text}' " +
                            $"Where 產品代碼 = '{CaseNumber}' and 箱體編號 = '{BoxName}' and [時間] = (" +
                                $"Select MAX(時間)" +
                                $"From [{Type}].[dbo].[{Type}]" +
                                $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}' and [日期] = (" +
                                    $"Select MAX(日期)" +
                                    $"From [{Type}].[dbo].[{Type}]" +
                                    $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}'" +
                                    $")" +
                                    $")" +
                                    $"and " +
                                    $" [日期] = (" +
                                    $"Select MAX(日期)" +
                                    $"From [{Type}].[dbo].[{Type}]" +
                                    $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}'" +
                                    $")";

            try
            {
                SqlCommand command = new SqlCommand(strCmd, sqlConnection);
                MessageBox.Show(command.ExecuteNonQuery().ToString() + " 筆資料已被改變");

                strCmd = $"Select [備註]" +
                         $"From [{Type}].[dbo].[{Type}]" +
                         $"Where 產品代碼 = '{CaseNumber}' and 箱體編號 = '{BoxName}' and [時間] = (" +
                                $"Select MAX(時間)" +
                                $"From [{Type}].[dbo].[{Type}]" +
                                $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}' and [日期] = (" +
                                    $"Select MAX(日期)" +
                                    $"From [{Type}].[dbo].[{Type}]" +
                                    $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}'" +
                                    $")" +
                                    $")" +
                                    $"and " +
                                    $" [日期] = (" +
                                    $"Select MAX(日期)" +
                                    $"From [{Type}].[dbo].[{Type}]" +
                                    $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}'" +
                                    $")";

                command = new SqlCommand(strCmd, sqlConnection);

                string beforeRemarks = command.ExecuteScalar().ToString();

                string afterRemarks = beforeRemarks + "{ DayTime : " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") +
                                                      ",  Type : " +  ValueType  + 
                                                      ",  Value : " + BeforeValue.Trim() + "  -->  " +  afterValue.Trim() +  
                                                      " };  ";

                strCmd = $"Update [{Type}].[dbo].[{Type}] " +
                            $"Set 備註 = '{afterRemarks}' " +
                            $"Where 產品代碼 = '{CaseNumber}' and 箱體編號 = '{BoxName}' and [時間] = (" +
                                $"Select MAX(時間)" +
                                $"From [{Type}].[dbo].[{Type}]" +
                                $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}' and [日期] = (" +
                                    $"Select MAX(日期)" +
                                    $"From [{Type}].[dbo].[{Type}]" +
                                    $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}'" +
                                    $")" +
                                    $")" +
                                    $"and " +
                                    $" [日期] = (" +
                                    $"Select MAX(日期)" +
                                    $"From [{Type}].[dbo].[{Type}]" +
                                    $"Where 產品代碼 = '{CaseNumber}' and [箱體編號] = '{BoxName}'" +
                                    $")";

                command = new SqlCommand(strCmd, sqlConnection);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }

            closeDB();
            Form1.queryDB();
            this.Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void connectDB()
        {
            string connectionString = "Data Source=(local);Integrated Security=SSPI;";

            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void closeDB()
        {
            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();
        }

    }
}
