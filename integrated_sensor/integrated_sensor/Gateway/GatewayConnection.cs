﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.SqlClient;
using System.Data;

namespace integrated_sensor
{
    public class GatewayConnection
    {
        public Form1 MainForm;

        SqlConnection sqlConnection;
        public string Ip { get; private set; }
        public bool ConnectState { get; private set; }

        private int timeOutCount = 0;

        private TcpClient m_myTcpClient;

        private NetworkStream m_myNetworkStream;

        private Thread m_thread;

        private int m_tcpCommunicationTime = 0;

        private int sensorCount = 3; //幾個感測器 , id (1:SMF , 2:Particle , 3:Vibration)

        public ushort ReadLength = 10; // gateway 一組訊息為10筆

        private delegate void UiUpdatedelegate(Control c, string s);
        private delegate void updateChartDelegate(Chart c, Queue<double> dataQueue);

        public String Pressure = "0";
        public String Temperature = "0";
        public String Humidity = "0";
        public String Particle = "0";
        public String Vibration = "0";
        public DateTime RecordTime;

        private static int chartCount = 20;  //紀錄資料筆數
        private static int axisY = 10;   //Y軸上下限增加值
        private Dictionary<String, int> axisYMap = new Dictionary<String, int>(); //紀錄Y軸最大、最小值
        private Dictionary<String, Object> sensorDataMap = new Dictionary<String, Object>();

        private Queue<double> timeQueue = new Queue<double>(chartCount);
        private Queue<double> pressureQueue = new Queue<double>(chartCount);
        private Queue<double> temperatureQueue = new Queue<double>(chartCount);
        private Queue<double> humidityQueue = new Queue<double>(chartCount);
        private Queue<double> particleQueue = new Queue<double>(chartCount);
        private Queue<double> vibrationQueue = new Queue<double>(chartCount);

        public GatewayConnection(Form1 form)
        {
            MainForm = form;
            Ip = "192.168.3.19";
            ConnectState = false;
            axisYMap.Add("MAX", 0);
            axisYMap.Add("MIN", 0);
            sensorDataMap.Add("Pressure", 0);
            sensorDataMap.Add("Temperature", 0);
            sensorDataMap.Add("Humidity", 0);
            sensorDataMap.Add("Particle", 0);
            sensorDataMap.Add("Vibration", 0);
            sensorDataMap.Add("RecordTime", 0);
        }

        public void doAction()
        {
            if (ConnectState == false)
            {
                if (m_thread == null ||
                    m_thread.ThreadState == ThreadState.Stopped ||
                    m_thread.ThreadState == ThreadState.Unstarted)
                {
                    m_thread = new Thread(connect);
                    m_thread.IsBackground = true;
                    m_thread.Start();
                }
            }
            else
            {
                //每一小關閉TcpClient 
                if (++m_tcpCommunicationTime > 700)
                {
                    m_tcpCommunicationTime = 0;

                    try
                    {
                        if (m_myTcpClient != null)
                            m_myTcpClient.Close();
                        if (m_myNetworkStream != null)
                            m_myNetworkStream.Close();

                        m_myTcpClient = new TcpClient(Ip, 502);

                        if (m_myTcpClient.Connected)
                        {
                            m_myNetworkStream = m_myTcpClient.GetStream();
                            m_myNetworkStream.WriteTimeout = 5000;
                            m_myNetworkStream.ReadTimeout = 5000;  //設定Timeout 有可能讀不到 程式會卡住
                            ConnectState = true;
                        }
                        else
                        {
                            ConnectState = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        ConnectState = false;
                    }
                }
                else
                {
                    requestaDeviceData();
                    Thread.Sleep(50);
                }
            }
        }

        public void connect()
        {
            Ping ping = new Ping();
            PingReply reply = null;

            try
            {
                reply = ping.Send(Ip);
            }
            catch (Exception e)
            {
                ConnectState = false;
                MainForm.RealTimeGraphLabel.ForeColor = Color.Red;
                closeThread();
                return;
            }

            if (reply.Status == IPStatus.Success)
            {
            }
            else
            {
                ConnectState = false;
                MainForm.RealTimeGraphLabel.ForeColor = Color.Red;
                closeThread();
                return;
            }

            try
            {
                if (m_myTcpClient != null)
                    m_myTcpClient.Close();
                if (m_myNetworkStream != null)
                    m_myNetworkStream.Close();

                m_myTcpClient = new TcpClient(Ip, 502);

                if (m_myTcpClient.Connected)
                {
                    m_myNetworkStream = m_myTcpClient.GetStream();
                    m_myNetworkStream.WriteTimeout = 5000;
                    m_myNetworkStream.ReadTimeout = 5000;  //設定Timeout 有可能讀不到 程式會卡住
                    ConnectState = true;
                    MainForm.RealTimeGraphLabel.ForeColor = Color.Black;
                }
                else
                {
                    ConnectState = false;
                    MainForm.RealTimeGraphLabel.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                ConnectState = false;
                MainForm.RealTimeGraphLabel.ForeColor = Color.Red;
                closeThread();
                return;
            }
            closeThread();
            return;
        }

        private void closeThread()
        {
            if (m_thread != null)
            {
                m_thread.Abort();
                m_thread = null;
            }
        }

        private void uiUpdate(Control ctrl, string value)
        {
            if (ctrl.InvokeRequired)
            {
                UiUpdatedelegate callBack = new UiUpdatedelegate(uiUpdate);
                ctrl.Invoke(callBack, ctrl, value);
            }
            else
            {
                ctrl.Text = value;
            }
        }

        private bool requestaDeviceData()
        {
            byte[] inputByte = new byte[12];
            byte[] outputByte = new byte[9 + sensorCount * ReadLength * 2]; //9 + (READ_GATEWAY_DEVICE_COUNT * m_gatewayInfo.ReadLength * 2)
            int startAddress = 64 * ReadLength; //startId  * m_gatewayInfo.ReadLength

            inputByte[0] = 0;    //Tcp Header
            inputByte[1] = 1;    //Tcp Header
            inputByte[2] = 0;    //Tcp Header
            inputByte[3] = 0;    //Tcp Header
            inputByte[4] = 0;    //Tcp Header
            inputByte[5] = 6;    //Tcp Header

            inputByte[6] = Convert.ToByte(1);    //Tcp Header   ,固定取gateway第一區

            inputByte[7] = 3;    //Function Code

            byte[] _adr = BitConverter.GetBytes(startAddress);
            inputByte[8] = _adr[1];   //startAddress
            inputByte[9] = _adr[0];   //startAddress 

            byte[] _length = BitConverter.GetBytes(sensorCount * ReadLength); //READ_GATEWAY_DEVICE_COUNT * m_gatewayInfo.ReadLength ,取三個 (震動、傳感、Particle)
            inputByte[10] = _length[1];   //Data Length
            inputByte[11] = _length[0];   //Data Length

            try
            {
                m_myNetworkStream.Write(inputByte, 0, inputByte.Length);
                m_myNetworkStream.Read(outputByte, 0, outputByte.Length);
            }
            catch (TimeoutException e)
            {
                ++timeOutCount;
                MessageBox.Show("ip:" + Ip + "timeout 第" + timeOutCount.ToString() + "次   " + e.ToString());
                ConnectState = false;
                return false;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                ConnectState = false;
                return false;
            }

            if (outputByte[0] != 0 || outputByte[1] != 1)
            {
                return false;
            }
            else
            {
                RecordTime = Convert.ToDateTime(DateTime.Now);
                updateDataQueue(timeQueue, RecordTime.ToString("HHmmss"));
                for (int i = 0; i < sensorCount; i++)
                {
                    if (i == 0)    //傳感器
                    {
                        for (int j = 0; j < ReadLength; j++)
                        {
                            int data = outputByte[9 + (i * 2 * ReadLength) + (2 * j)];
                            data = (data << 8) + outputByte[10 + (i * 2 * ReadLength) + (2 * j)];
                            if (j == 2)  //風壓 adr:3
                            {
                                Pressure = (data / 10.0F).ToString();
                                updateDataQueue(pressureQueue, Pressure);
                            }
                            if (j == 6)  //溫度 adr:7
                            {
                                Temperature = (data / 10.0F).ToString();
                                updateDataQueue(temperatureQueue, Temperature);
                            }
                            if (j == 7)  //濕度 adr:8 
                            {
                                Humidity = (data / 10.0F).ToString();
                                Particle = (data / 10.0F).ToString();
                                Vibration = (data / 10.0F).ToString();
                                updateDataQueue(humidityQueue, Humidity);
                            }
                            if (j == 8)  //震動 adr:9 
                            {
                                Vibration = (data / 10.0F).ToString();
                                updateDataQueue(vibrationQueue, Vibration);
                            }
                        }
                    }
                    else if (i == 1)
                    {

                    }
                }
                connectDB();
                sensorDataMap["Pressure"] = Pressure;
                sensorDataMap["Temperature"] = Temperature;
                sensorDataMap["Humidity"] = Humidity;
                sensorDataMap["Particle"] = Particle;
                sensorDataMap["Vibration"] = Vibration;
                sensorDataMap["RecordTime"] = RecordTime;
                insertSensorData(sensorDataMap);
                closeDB();

            }
            uiUpdate(MainForm.PressureLabel, Pressure);
            uiUpdate(MainForm.TemperatureLabel, Temperature);
            uiUpdate(MainForm.HumidityLabel, Humidity);
            uiUpdate(MainForm.ParticleLabel, Particle);
            uiUpdate(MainForm.VibrationLabel, Vibration);
            ChartUpdate(MainForm.PressureChart, pressureQueue);
            ChartUpdate(MainForm.TT_RHChart, temperatureQueue);
            ChartUpdate(MainForm.ParticleChart, humidityQueue);
            ChartUpdate(MainForm.VibrationChart, vibrationQueue);
            return true;
        }

        public void updateDataQueue(Queue<double> dataQueue , String value)
        {
            if (dataQueue.Count > chartCount)
            {
                //先出列
                dataQueue.Dequeue();
                dataQueue.Enqueue(double.Parse(value));
            }
            else
            {
                dataQueue.Enqueue(double.Parse(value));
            }
        }
        public void ChartUpdate(Chart ctrl, Queue<double> dataQueue)
        {
            int maxValue = 0;
            int minValue = 0;
            String time;
            axisYMap["MAX"] = maxValue;
            axisYMap["MIN"] = minValue;

            if (ctrl.InvokeRequired)
            {
                updateChartDelegate callBack = new updateChartDelegate(ChartUpdate);
                    ctrl.Invoke(new MethodInvoker(delegate ()
                    {
                        ctrl.Series[0].Points.Clear();
                        if ("TT_RHChart" == ctrl.Name)
                        {
                            ctrl.Series[1].Points.Clear();
                        }
                        for (int i = 0; i < timeQueue.Count; i++)
                        {
                            time = timeQueue.ElementAt(i).ToString();
                            while (time.Length < 6){
                                time = "0" + time;
                            }
                            time = time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":" + time.Substring(4, 2);
                            ctrl.Series[0].Points.AddXY(time, (int)dataQueue.ElementAt(i));
                            if("TT_RHChart" == ctrl.Name)
                            {
                                ctrl.Series[1].Points.AddXY(time, (int)humidityQueue.ElementAt(i));
                            }

                            //判斷Y軸上下限
                            axisYMap = adjustAxisY(maxValue, minValue, dataQueue, i, ctrl);
                            maxValue = axisYMap["MAX"];
                            minValue = axisYMap["MIN"];
                        }
                        ctrl.ChartAreas[0].AxisY.Maximum = axisYMap["MAX"] + axisY;
                        ctrl.ChartAreas[0].AxisY.Minimum = axisYMap["MIN"] - axisY;
                    }));
            }
            else
            {
                //ctrl.Series[0].Points.Clear();
                //if ("TT_RHChart" == ctrl.Name)
                //{
                //    ctrl.Series[1].Points.Clear();
                //}
                
                for (int i = 0; i < timeQueue.Count; i++)
                {
                    time = timeQueue.ElementAt(i).ToString();
                    while (time.Length < 6){
                        time = "0" + time;
                    }
                    time = time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":" + time.Substring(4, 2);
                    ctrl.Series[0].Points.AddXY(time, (int)dataQueue.ElementAt(i));
                    if ("TT_RHChart" == ctrl.Name)
                    {
                        ctrl.Series[1].Points.AddXY(time, (int)humidityQueue.ElementAt(i));
                    }

                    //判斷Y軸上下限
                    axisYMap = adjustAxisY(maxValue, minValue, dataQueue, i, ctrl);
                    maxValue = axisYMap["MAX"];
                    minValue = axisYMap["MIN"];
                }
                ctrl.ChartAreas[0].AxisY.Maximum = axisYMap["MAX"] + axisY;
                ctrl.ChartAreas[0].AxisY.Minimum = axisYMap["MIN"] - axisY;
            }
        }

        public Dictionary<String, int> adjustAxisY(int maxValue, int minValue, Queue<double> dataQueue , int i, Chart ctrl )
        {
            if (i == 0)
            {
                maxValue = (int)dataQueue.ElementAt(i);
                minValue = (int)dataQueue.ElementAt(i);

                if ("TT_RHChart" == ctrl.Name)
                {
                    if (humidityQueue.ElementAt(i) > maxValue)
                    {
                        maxValue = (int)humidityQueue.ElementAt(i);
                    }
                    if (humidityQueue.ElementAt(i) < minValue)
                    {
                        minValue = (int)humidityQueue.ElementAt(i);
                    }
                }
            }
            else
            {
                if (dataQueue.ElementAt(i) > maxValue)
                {
                    maxValue = (int)dataQueue.ElementAt(i);
                }
                if (dataQueue.ElementAt(i) < minValue)
                {
                    minValue = (int)dataQueue.ElementAt(i);
                }

                if ("TT_RHChart" == ctrl.Name)
                {
                    if (humidityQueue.ElementAt(i) > maxValue)
                    {
                        maxValue = (int)humidityQueue.ElementAt(i);
                    }
                    if (humidityQueue.ElementAt(i) < minValue)
                    {
                        minValue = (int)humidityQueue.ElementAt(i);
                    }
                }
            }
            axisYMap["MAX"] =  maxValue;
            axisYMap["MIN"] =  minValue;
            return axisYMap;
        }

        private void connectDB()
        {
            string connectionString = "Data Source=(local);Integrated Security=SSPI;";

            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void closeDB()
        {
            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();
        }

        private void insertSensorData(Dictionary<String, Object> sensorDataMap)
        {
            String sql = $"INSERT INTO [SENSOR].[dbo].[sensor]" +
                         $"            ([pressure],[temperature],[humidity],[particle],[vibration],[recordTime]) " +
                         $"      VALUES "+
                         $"            ({sensorDataMap["Pressure"]}, {sensorDataMap["Temperature"]}, {sensorDataMap["Humidity"]}," +
                         $"             {sensorDataMap["Particle"]}, {sensorDataMap["Vibration"]}, '{sensorDataMap["RecordTime"]}')";

            SqlCommand command = new SqlCommand(sql, sqlConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
