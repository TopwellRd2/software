﻿namespace integrated_sensor
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.RealTimeTabPage = new System.Windows.Forms.TabPage();
            this.ParticleGroupBox = new System.Windows.Forms.GroupBox();
            this.ParticleChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.VibrationGroupBox = new System.Windows.Forms.GroupBox();
            this.VibrationChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.PressureGroupBox = new System.Windows.Forms.GroupBox();
            this.PressureChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RealTimeGraphLabel = new System.Windows.Forms.Label();
            this.TT_RHGroupBox = new System.Windows.Forms.GroupBox();
            this.TT_RHChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.SensorGroupBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.VibrationLabel = new System.Windows.Forms.Label();
            this.ParticleLabel = new System.Windows.Forms.Label();
            this.HumidityLabel = new System.Windows.Forms.Label();
            this.PressureLabel = new System.Windows.Forms.Label();
            this.TemperatureLabel = new System.Windows.Forms.Label();
            this.HistoricalTabPage = new System.Windows.Forms.TabPage();
            this.ParticleGroupBoxH = new System.Windows.Forms.GroupBox();
            this.ParticleChartH = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.VibrationGroupBoxH = new System.Windows.Forms.GroupBox();
            this.VibrationChartH = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.PressureGroupBoxH = new System.Windows.Forms.GroupBox();
            this.PressureChartH = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.HistoricalGraphLabel = new System.Windows.Forms.Label();
            this.TTRHGroupBoxH = new System.Windows.Forms.GroupBox();
            this.TT_RHChartH = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.QueryCriteriaGroupBox = new System.Windows.Forms.GroupBox();
            this.ExportButton = new System.Windows.Forms.Button();
            this.QueryButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.EndDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.StratDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tabControl1.SuspendLayout();
            this.RealTimeTabPage.SuspendLayout();
            this.ParticleGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParticleChart)).BeginInit();
            this.VibrationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VibrationChart)).BeginInit();
            this.PressureGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PressureChart)).BeginInit();
            this.TT_RHGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TT_RHChart)).BeginInit();
            this.SensorGroupBox.SuspendLayout();
            this.HistoricalTabPage.SuspendLayout();
            this.ParticleGroupBoxH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParticleChartH)).BeginInit();
            this.VibrationGroupBoxH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VibrationChartH)).BeginInit();
            this.PressureGroupBoxH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PressureChartH)).BeginInit();
            this.TTRHGroupBoxH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TT_RHChartH)).BeginInit();
            this.QueryCriteriaGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.AllowDrop = true;
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.RealTimeTabPage);
            this.tabControl1.Controls.Add(this.HistoricalTabPage);
            this.tabControl1.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(12, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1499, 906);
            this.tabControl1.TabIndex = 6;
            // 
            // RealTimeTabPage
            // 
            this.RealTimeTabPage.AutoScroll = true;
            this.RealTimeTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.RealTimeTabPage.Controls.Add(this.ParticleGroupBox);
            this.RealTimeTabPage.Controls.Add(this.VibrationGroupBox);
            this.RealTimeTabPage.Controls.Add(this.PressureGroupBox);
            this.RealTimeTabPage.Controls.Add(this.RealTimeGraphLabel);
            this.RealTimeTabPage.Controls.Add(this.TT_RHGroupBox);
            this.RealTimeTabPage.Controls.Add(this.SensorGroupBox);
            this.RealTimeTabPage.Location = new System.Drawing.Point(4, 33);
            this.RealTimeTabPage.Name = "RealTimeTabPage";
            this.RealTimeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.RealTimeTabPage.Size = new System.Drawing.Size(1491, 869);
            this.RealTimeTabPage.TabIndex = 0;
            this.RealTimeTabPage.Text = "Real-Time Data";
            // 
            // ParticleGroupBox
            // 
            this.ParticleGroupBox.Controls.Add(this.ParticleChart);
            this.ParticleGroupBox.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ParticleGroupBox.Location = new System.Drawing.Point(30, 616);
            this.ParticleGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.ParticleGroupBox.Name = "ParticleGroupBox";
            this.ParticleGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.ParticleGroupBox.Size = new System.Drawing.Size(881, 364);
            this.ParticleGroupBox.TabIndex = 9;
            this.ParticleGroupBox.TabStop = false;
            this.ParticleGroupBox.Text = "Particle ( 0.3μm ) ";
            // 
            // ParticleChart
            // 
            chartArea1.AxisX.MajorGrid.LineWidth = 0;
            chartArea1.AxisX.Title = "Time";
            chartArea1.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea1.AxisY.MajorGrid.LineWidth = 0;
            chartArea1.Name = "ChartArea1";
            this.ParticleChart.ChartAreas.Add(chartArea1);
            this.ParticleChart.Location = new System.Drawing.Point(7, 39);
            this.ParticleChart.Name = "ParticleChart";
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            series1.IsVisibleInLegend = false;
            series1.Name = "Series1";
            this.ParticleChart.Series.Add(series1);
            this.ParticleChart.Size = new System.Drawing.Size(867, 318);
            this.ParticleChart.TabIndex = 1;
            this.ParticleChart.Text = "Particle";
            // 
            // VibrationGroupBox
            // 
            this.VibrationGroupBox.Controls.Add(this.VibrationChart);
            this.VibrationGroupBox.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.VibrationGroupBox.Location = new System.Drawing.Point(958, 616);
            this.VibrationGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.VibrationGroupBox.Name = "VibrationGroupBox";
            this.VibrationGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.VibrationGroupBox.Size = new System.Drawing.Size(881, 364);
            this.VibrationGroupBox.TabIndex = 11;
            this.VibrationGroupBox.TabStop = false;
            this.VibrationGroupBox.Text = "Vibration";
            // 
            // VibrationChart
            // 
            chartArea2.AxisX.MajorGrid.LineWidth = 0;
            chartArea2.AxisX.Title = "Time";
            chartArea2.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea2.AxisY.MajorGrid.LineWidth = 0;
            chartArea2.Name = "ChartArea1";
            this.VibrationChart.ChartAreas.Add(chartArea2);
            this.VibrationChart.Location = new System.Drawing.Point(7, 39);
            this.VibrationChart.Name = "VibrationChart";
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Color = System.Drawing.Color.Black;
            series2.IsVisibleInLegend = false;
            series2.Name = "Series1";
            this.VibrationChart.Series.Add(series2);
            this.VibrationChart.Size = new System.Drawing.Size(867, 318);
            this.VibrationChart.TabIndex = 1;
            this.VibrationChart.Text = "Vibration";
            // 
            // PressureGroupBox
            // 
            this.PressureGroupBox.Controls.Add(this.PressureChart);
            this.PressureGroupBox.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.PressureGroupBox.Location = new System.Drawing.Point(958, 235);
            this.PressureGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.PressureGroupBox.Name = "PressureGroupBox";
            this.PressureGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.PressureGroupBox.Size = new System.Drawing.Size(881, 364);
            this.PressureGroupBox.TabIndex = 10;
            this.PressureGroupBox.TabStop = false;
            this.PressureGroupBox.Text = "Pressure";
            // 
            // PressureChart
            // 
            chartArea3.AxisX.MajorGrid.LineWidth = 0;
            chartArea3.AxisX.Title = "Time";
            chartArea3.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea3.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea3.AxisY.MajorGrid.LineWidth = 0;
            chartArea3.Name = "ChartArea1";
            this.PressureChart.ChartAreas.Add(chartArea3);
            this.PressureChart.Location = new System.Drawing.Point(7, 39);
            this.PressureChart.Name = "PressureChart";
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Color = System.Drawing.Color.Green;
            series3.IsVisibleInLegend = false;
            series3.Name = "Series1";
            this.PressureChart.Series.Add(series3);
            this.PressureChart.Size = new System.Drawing.Size(867, 318);
            this.PressureChart.TabIndex = 1;
            this.PressureChart.Text = "Pressure";
            // 
            // RealTimeGraphLabel
            // 
            this.RealTimeGraphLabel.AutoSize = true;
            this.RealTimeGraphLabel.BackColor = System.Drawing.Color.Transparent;
            this.RealTimeGraphLabel.Font = new System.Drawing.Font("新細明體", 30F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.RealTimeGraphLabel.ForeColor = System.Drawing.Color.Red;
            this.RealTimeGraphLabel.Location = new System.Drawing.Point(742, 173);
            this.RealTimeGraphLabel.Name = "RealTimeGraphLabel";
            this.RealTimeGraphLabel.Size = new System.Drawing.Size(434, 50);
            this.RealTimeGraphLabel.TabIndex = 8;
            this.RealTimeGraphLabel.Text = "Realtime Trend Chart";
            this.RealTimeGraphLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TT_RHGroupBox
            // 
            this.TT_RHGroupBox.Controls.Add(this.TT_RHChart);
            this.TT_RHGroupBox.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TT_RHGroupBox.Location = new System.Drawing.Point(30, 235);
            this.TT_RHGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.TT_RHGroupBox.Name = "TT_RHGroupBox";
            this.TT_RHGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.TT_RHGroupBox.Size = new System.Drawing.Size(881, 364);
            this.TT_RHGroupBox.TabIndex = 7;
            this.TT_RHGroupBox.TabStop = false;
            this.TT_RHGroupBox.Text = "Temperature  /  Humidity";
            // 
            // TT_RHChart
            // 
            chartArea4.AxisX.MajorGrid.LineWidth = 0;
            chartArea4.AxisX.Title = "Time";
            chartArea4.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea4.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea4.AxisY.MajorGrid.LineWidth = 0;
            chartArea4.Name = "ChartArea1";
            this.TT_RHChart.ChartAreas.Add(chartArea4);
            legend1.Alignment = System.Drawing.StringAlignment.Far;
            legend1.DockedToChartArea = "ChartArea1";
            legend1.Name = "Temperature";
            legend2.Alignment = System.Drawing.StringAlignment.Far;
            legend2.DockedToChartArea = "ChartArea1";
            legend2.Name = "Humidity";
            this.TT_RHChart.Legends.Add(legend1);
            this.TT_RHChart.Legends.Add(legend2);
            this.TT_RHChart.Location = new System.Drawing.Point(7, 39);
            this.TT_RHChart.Name = "TT_RHChart";
            series4.BorderWidth = 3;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Legend = "Temperature";
            series4.Name = "Temperature";
            series5.BorderWidth = 3;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Color = System.Drawing.Color.Maroon;
            series5.Legend = "Humidity";
            series5.Name = "Humidity";
            this.TT_RHChart.Series.Add(series4);
            this.TT_RHChart.Series.Add(series5);
            this.TT_RHChart.Size = new System.Drawing.Size(867, 318);
            this.TT_RHChart.TabIndex = 0;
            this.TT_RHChart.Text = "Temperature / Humidity";
            // 
            // SensorGroupBox
            // 
            this.SensorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SensorGroupBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.SensorGroupBox.Controls.Add(this.label11);
            this.SensorGroupBox.Controls.Add(this.label10);
            this.SensorGroupBox.Controls.Add(this.label9);
            this.SensorGroupBox.Controls.Add(this.label8);
            this.SensorGroupBox.Controls.Add(this.label7);
            this.SensorGroupBox.Controls.Add(this.label1);
            this.SensorGroupBox.Controls.Add(this.label2);
            this.SensorGroupBox.Controls.Add(this.label3);
            this.SensorGroupBox.Controls.Add(this.label4);
            this.SensorGroupBox.Controls.Add(this.label5);
            this.SensorGroupBox.Controls.Add(this.VibrationLabel);
            this.SensorGroupBox.Controls.Add(this.ParticleLabel);
            this.SensorGroupBox.Controls.Add(this.HumidityLabel);
            this.SensorGroupBox.Controls.Add(this.PressureLabel);
            this.SensorGroupBox.Controls.Add(this.TemperatureLabel);
            this.SensorGroupBox.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SensorGroupBox.Location = new System.Drawing.Point(30, 22);
            this.SensorGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.SensorGroupBox.Name = "SensorGroupBox";
            this.SensorGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.SensorGroupBox.Size = new System.Drawing.Size(7669, 147);
            this.SensorGroupBox.TabIndex = 6;
            this.SensorGroupBox.TabStop = false;
            this.SensorGroupBox.Text = "Sensor";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1453, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 27);
            this.label11.TabIndex = 14;
            this.label11.Text = "%";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(942, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 27);
            this.label10.TabIndex = 13;
            this.label10.Text = "μm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(942, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 27);
            this.label9.TabIndex = 12;
            this.label9.Text = "Pa";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(480, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 27);
            this.label8.TabIndex = 11;
            this.label8.Text = "N/㎝^3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(403, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 27);
            this.label7.TabIndex = 10;
            this.label7.Text = "°C";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(754, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 27);
            this.label1.TabIndex = 9;
            this.label1.Text = "Vibration :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 27);
            this.label2.TabIndex = 8;
            this.label2.Text = "Particle ( 0.3μm ) :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1260, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 27);
            this.label3.TabIndex = 7;
            this.label3.Text = "Humidity :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(754, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 27);
            this.label4.TabIndex = 6;
            this.label4.Text = "Pressure :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(181, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 27);
            this.label5.TabIndex = 5;
            this.label5.Text = "Temperature :";
            // 
            // VibrationLabel
            // 
            this.VibrationLabel.AutoSize = true;
            this.VibrationLabel.ForeColor = System.Drawing.Color.Blue;
            this.VibrationLabel.Location = new System.Drawing.Point(883, 97);
            this.VibrationLabel.Name = "VibrationLabel";
            this.VibrationLabel.Size = new System.Drawing.Size(109, 27);
            this.VibrationLabel.TabIndex = 4;
            this.VibrationLabel.Text = "Vibration";
            this.VibrationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ParticleLabel
            // 
            this.ParticleLabel.AutoSize = true;
            this.ParticleLabel.ForeColor = System.Drawing.Color.Blue;
            this.ParticleLabel.Location = new System.Drawing.Point(412, 97);
            this.ParticleLabel.Name = "ParticleLabel";
            this.ParticleLabel.Size = new System.Drawing.Size(204, 27);
            this.ParticleLabel.TabIndex = 3;
            this.ParticleLabel.Text = "Particle ( 0.3μm )";
            this.ParticleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HumidityLabel
            // 
            this.HumidityLabel.AutoSize = true;
            this.HumidityLabel.ForeColor = System.Drawing.Color.Blue;
            this.HumidityLabel.Location = new System.Drawing.Point(1390, 36);
            this.HumidityLabel.Name = "HumidityLabel";
            this.HumidityLabel.Size = new System.Drawing.Size(110, 27);
            this.HumidityLabel.TabIndex = 2;
            this.HumidityLabel.Text = "Humidity";
            this.HumidityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PressureLabel
            // 
            this.PressureLabel.AutoSize = true;
            this.PressureLabel.ForeColor = System.Drawing.Color.Blue;
            this.PressureLabel.Location = new System.Drawing.Point(871, 36);
            this.PressureLabel.Name = "PressureLabel";
            this.PressureLabel.Size = new System.Drawing.Size(97, 27);
            this.PressureLabel.TabIndex = 1;
            this.PressureLabel.Text = "Pressure";
            this.PressureLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TemperatureLabel
            // 
            this.TemperatureLabel.AutoSize = true;
            this.TemperatureLabel.ForeColor = System.Drawing.Color.Blue;
            this.TemperatureLabel.Location = new System.Drawing.Point(342, 36);
            this.TemperatureLabel.Name = "TemperatureLabel";
            this.TemperatureLabel.Size = new System.Drawing.Size(141, 27);
            this.TemperatureLabel.TabIndex = 0;
            this.TemperatureLabel.Text = "Temperature";
            this.TemperatureLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HistoricalTabPage
            // 
            this.HistoricalTabPage.AutoScroll = true;
            this.HistoricalTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.HistoricalTabPage.Controls.Add(this.ParticleGroupBoxH);
            this.HistoricalTabPage.Controls.Add(this.VibrationGroupBoxH);
            this.HistoricalTabPage.Controls.Add(this.PressureGroupBoxH);
            this.HistoricalTabPage.Controls.Add(this.HistoricalGraphLabel);
            this.HistoricalTabPage.Controls.Add(this.TTRHGroupBoxH);
            this.HistoricalTabPage.Controls.Add(this.QueryCriteriaGroupBox);
            this.HistoricalTabPage.ForeColor = System.Drawing.Color.Red;
            this.HistoricalTabPage.Location = new System.Drawing.Point(4, 33);
            this.HistoricalTabPage.Name = "HistoricalTabPage";
            this.HistoricalTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.HistoricalTabPage.Size = new System.Drawing.Size(1491, 869);
            this.HistoricalTabPage.TabIndex = 1;
            this.HistoricalTabPage.Text = "Historical Data";
            // 
            // ParticleGroupBoxH
            // 
            this.ParticleGroupBoxH.Controls.Add(this.ParticleChartH);
            this.ParticleGroupBoxH.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ParticleGroupBoxH.Location = new System.Drawing.Point(30, 616);
            this.ParticleGroupBoxH.Margin = new System.Windows.Forms.Padding(4);
            this.ParticleGroupBoxH.Name = "ParticleGroupBoxH";
            this.ParticleGroupBoxH.Padding = new System.Windows.Forms.Padding(4);
            this.ParticleGroupBoxH.Size = new System.Drawing.Size(881, 364);
            this.ParticleGroupBoxH.TabIndex = 15;
            this.ParticleGroupBoxH.TabStop = false;
            this.ParticleGroupBoxH.Text = "Particle ( 0.3μm ) ";
            // 
            // ParticleChartH
            // 
            chartArea5.AxisX.MajorGrid.LineWidth = 0;
            chartArea5.AxisX.Title = "Time";
            chartArea5.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea5.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea5.AxisY.MajorGrid.LineWidth = 0;
            chartArea5.Name = "ChartArea1";
            this.ParticleChartH.ChartAreas.Add(chartArea5);
            this.ParticleChartH.Location = new System.Drawing.Point(7, 39);
            this.ParticleChartH.Name = "ParticleChartH";
            series6.BorderWidth = 3;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            series6.IsVisibleInLegend = false;
            series6.Name = "Series1";
            this.ParticleChartH.Series.Add(series6);
            this.ParticleChartH.Size = new System.Drawing.Size(867, 318);
            this.ParticleChartH.TabIndex = 2;
            this.ParticleChartH.Text = "Particle";
            // 
            // VibrationGroupBoxH
            // 
            this.VibrationGroupBoxH.Controls.Add(this.VibrationChartH);
            this.VibrationGroupBoxH.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.VibrationGroupBoxH.Location = new System.Drawing.Point(958, 616);
            this.VibrationGroupBoxH.Margin = new System.Windows.Forms.Padding(4);
            this.VibrationGroupBoxH.Name = "VibrationGroupBoxH";
            this.VibrationGroupBoxH.Padding = new System.Windows.Forms.Padding(4);
            this.VibrationGroupBoxH.Size = new System.Drawing.Size(881, 364);
            this.VibrationGroupBoxH.TabIndex = 17;
            this.VibrationGroupBoxH.TabStop = false;
            this.VibrationGroupBoxH.Text = "Vibration";
            // 
            // VibrationChartH
            // 
            chartArea6.AxisX.MajorGrid.LineWidth = 0;
            chartArea6.AxisX.Title = "Time";
            chartArea6.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea6.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea6.AxisY.MajorGrid.LineWidth = 0;
            chartArea6.Name = "ChartArea1";
            this.VibrationChartH.ChartAreas.Add(chartArea6);
            this.VibrationChartH.Location = new System.Drawing.Point(7, 39);
            this.VibrationChartH.Name = "VibrationChartH";
            series7.BorderWidth = 3;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series7.Color = System.Drawing.Color.Black;
            series7.IsVisibleInLegend = false;
            series7.Name = "Series1";
            this.VibrationChartH.Series.Add(series7);
            this.VibrationChartH.Size = new System.Drawing.Size(867, 318);
            this.VibrationChartH.TabIndex = 2;
            this.VibrationChartH.Text = "Vibration";
            // 
            // PressureGroupBoxH
            // 
            this.PressureGroupBoxH.Controls.Add(this.PressureChartH);
            this.PressureGroupBoxH.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.PressureGroupBoxH.Location = new System.Drawing.Point(958, 235);
            this.PressureGroupBoxH.Margin = new System.Windows.Forms.Padding(4);
            this.PressureGroupBoxH.Name = "PressureGroupBoxH";
            this.PressureGroupBoxH.Padding = new System.Windows.Forms.Padding(4);
            this.PressureGroupBoxH.Size = new System.Drawing.Size(881, 364);
            this.PressureGroupBoxH.TabIndex = 16;
            this.PressureGroupBoxH.TabStop = false;
            this.PressureGroupBoxH.Text = "Pressure";
            // 
            // PressureChartH
            // 
            chartArea7.AxisX.MajorGrid.LineWidth = 0;
            chartArea7.AxisX.Title = "Time";
            chartArea7.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea7.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea7.AxisY.MajorGrid.LineWidth = 0;
            chartArea7.Name = "ChartArea1";
            this.PressureChartH.ChartAreas.Add(chartArea7);
            this.PressureChartH.Location = new System.Drawing.Point(7, 27);
            this.PressureChartH.Name = "PressureChartH";
            series8.BorderWidth = 3;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Color = System.Drawing.Color.Green;
            series8.IsVisibleInLegend = false;
            series8.Name = "Series1";
            this.PressureChartH.Series.Add(series8);
            this.PressureChartH.Size = new System.Drawing.Size(867, 330);
            this.PressureChartH.TabIndex = 2;
            this.PressureChartH.Text = "Pressure";
            // 
            // HistoricalGraphLabel
            // 
            this.HistoricalGraphLabel.AutoSize = true;
            this.HistoricalGraphLabel.BackColor = System.Drawing.Color.Transparent;
            this.HistoricalGraphLabel.Font = new System.Drawing.Font("新細明體", 30F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HistoricalGraphLabel.Location = new System.Drawing.Point(742, 173);
            this.HistoricalGraphLabel.Name = "HistoricalGraphLabel";
            this.HistoricalGraphLabel.Size = new System.Drawing.Size(449, 50);
            this.HistoricalGraphLabel.TabIndex = 14;
            this.HistoricalGraphLabel.Text = "Historical Trend Chart";
            this.HistoricalGraphLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TTRHGroupBoxH
            // 
            this.TTRHGroupBoxH.Controls.Add(this.TT_RHChartH);
            this.TTRHGroupBoxH.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TTRHGroupBoxH.Location = new System.Drawing.Point(30, 235);
            this.TTRHGroupBoxH.Margin = new System.Windows.Forms.Padding(4);
            this.TTRHGroupBoxH.Name = "TTRHGroupBoxH";
            this.TTRHGroupBoxH.Padding = new System.Windows.Forms.Padding(4);
            this.TTRHGroupBoxH.Size = new System.Drawing.Size(881, 364);
            this.TTRHGroupBoxH.TabIndex = 13;
            this.TTRHGroupBoxH.TabStop = false;
            this.TTRHGroupBoxH.Text = "Temperature  /  Humidity";
            // 
            // TT_RHChartH
            // 
            chartArea8.AxisX.MajorGrid.LineWidth = 0;
            chartArea8.AxisX.Title = "Time";
            chartArea8.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea8.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea8.AxisY.MajorGrid.LineWidth = 0;
            chartArea8.Name = "ChartArea1";
            this.TT_RHChartH.ChartAreas.Add(chartArea8);
            legend3.Alignment = System.Drawing.StringAlignment.Far;
            legend3.DockedToChartArea = "ChartArea1";
            legend3.Name = "Temperature";
            legend4.Alignment = System.Drawing.StringAlignment.Far;
            legend4.DockedToChartArea = "ChartArea1";
            legend4.Name = "Humidity";
            this.TT_RHChartH.Legends.Add(legend3);
            this.TT_RHChartH.Legends.Add(legend4);
            this.TT_RHChartH.Location = new System.Drawing.Point(7, 27);
            this.TT_RHChartH.Name = "TT_RHChartH";
            series9.BorderWidth = 3;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series9.Legend = "Temperature";
            series9.Name = "Temperature";
            series10.BorderWidth = 3;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series10.Color = System.Drawing.Color.Maroon;
            series10.Legend = "Humidity";
            series10.Name = "Humidity";
            this.TT_RHChartH.Series.Add(series9);
            this.TT_RHChartH.Series.Add(series10);
            this.TT_RHChartH.Size = new System.Drawing.Size(867, 330);
            this.TT_RHChartH.TabIndex = 1;
            this.TT_RHChartH.Text = "Temperature / Humidity";
            // 
            // QueryCriteriaGroupBox
            // 
            this.QueryCriteriaGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.QueryCriteriaGroupBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.QueryCriteriaGroupBox.Controls.Add(this.ExportButton);
            this.QueryCriteriaGroupBox.Controls.Add(this.QueryButton);
            this.QueryCriteriaGroupBox.Controls.Add(this.label6);
            this.QueryCriteriaGroupBox.Controls.Add(this.EndDateTimePicker);
            this.QueryCriteriaGroupBox.Controls.Add(this.StratDateTimePicker);
            this.QueryCriteriaGroupBox.Font = new System.Drawing.Font("新細明體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.QueryCriteriaGroupBox.Location = new System.Drawing.Point(30, 22);
            this.QueryCriteriaGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.QueryCriteriaGroupBox.Name = "QueryCriteriaGroupBox";
            this.QueryCriteriaGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.QueryCriteriaGroupBox.Size = new System.Drawing.Size(7669, 147);
            this.QueryCriteriaGroupBox.TabIndex = 12;
            this.QueryCriteriaGroupBox.TabStop = false;
            this.QueryCriteriaGroupBox.Text = "Query Criteria";
            // 
            // ExportButton
            // 
            this.ExportButton.BackColor = System.Drawing.SystemColors.Control;
            this.ExportButton.ForeColor = System.Drawing.Color.Black;
            this.ExportButton.Location = new System.Drawing.Point(1214, 55);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(100, 40);
            this.ExportButton.TabIndex = 4;
            this.ExportButton.Text = "Export";
            this.ExportButton.UseVisualStyleBackColor = false;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // QueryButton
            // 
            this.QueryButton.BackColor = System.Drawing.SystemColors.Control;
            this.QueryButton.ForeColor = System.Drawing.Color.Black;
            this.QueryButton.Location = new System.Drawing.Point(1096, 55);
            this.QueryButton.Name = "QueryButton";
            this.QueryButton.Size = new System.Drawing.Size(90, 40);
            this.QueryButton.TabIndex = 3;
            this.QueryButton.Text = "Query";
            this.QueryButton.UseVisualStyleBackColor = false;
            this.QueryButton.Click += new System.EventHandler(this.QueryButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(759, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 27);
            this.label6.TabIndex = 2;
            this.label6.Text = "~";
            // 
            // EndDateTimePicker
            // 
            this.EndDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.EndDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.EndDateTimePicker.Location = new System.Drawing.Point(794, 56);
            this.EndDateTimePicker.Name = "EndDateTimePicker";
            this.EndDateTimePicker.Size = new System.Drawing.Size(272, 39);
            this.EndDateTimePicker.TabIndex = 1;
            // 
            // StratDateTimePicker
            // 
            this.StratDateTimePicker.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.StratDateTimePicker.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.StratDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.StratDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.StratDateTimePicker.Location = new System.Drawing.Point(480, 56);
            this.StratDateTimePicker.Name = "StratDateTimePicker";
            this.StratDateTimePicker.Size = new System.Drawing.Size(272, 39);
            this.StratDateTimePicker.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1514, 931);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Integrated Sensor";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.RealTimeTabPage.ResumeLayout(false);
            this.RealTimeTabPage.PerformLayout();
            this.ParticleGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ParticleChart)).EndInit();
            this.VibrationGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VibrationChart)).EndInit();
            this.PressureGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PressureChart)).EndInit();
            this.TT_RHGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TT_RHChart)).EndInit();
            this.SensorGroupBox.ResumeLayout(false);
            this.SensorGroupBox.PerformLayout();
            this.HistoricalTabPage.ResumeLayout(false);
            this.HistoricalTabPage.PerformLayout();
            this.ParticleGroupBoxH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ParticleChartH)).EndInit();
            this.VibrationGroupBoxH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VibrationChartH)).EndInit();
            this.PressureGroupBoxH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PressureChartH)).EndInit();
            this.TTRHGroupBoxH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TT_RHChartH)).EndInit();
            this.QueryCriteriaGroupBox.ResumeLayout(false);
            this.QueryCriteriaGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage RealTimeTabPage;
        private System.Windows.Forms.GroupBox VibrationGroupBox;
        private System.Windows.Forms.GroupBox PressureGroupBox;
        private System.Windows.Forms.GroupBox TT_RHGroupBox;
        private System.Windows.Forms.GroupBox SensorGroupBox;
        private System.Windows.Forms.TabPage HistoricalTabPage;
        private System.Windows.Forms.GroupBox ParticleGroupBox;
        private System.Windows.Forms.GroupBox ParticleGroupBoxH;
        private System.Windows.Forms.GroupBox VibrationGroupBoxH;
        private System.Windows.Forms.GroupBox PressureGroupBoxH;
        private System.Windows.Forms.Label HistoricalGraphLabel;
        private System.Windows.Forms.GroupBox TTRHGroupBoxH;
        private System.Windows.Forms.GroupBox QueryCriteriaGroupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ExportButton;
        private System.Windows.Forms.Button QueryButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker EndDateTimePicker;
        private System.Windows.Forms.DateTimePicker StratDateTimePicker;
        public System.Windows.Forms.Label RealTimeGraphLabel;
        public System.Windows.Forms.Label VibrationLabel;
        public System.Windows.Forms.Label ParticleLabel;
        public System.Windows.Forms.Label HumidityLabel;
        public System.Windows.Forms.Label PressureLabel;
        public System.Windows.Forms.Label TemperatureLabel;
        public System.Windows.Forms.DataVisualization.Charting.Chart TT_RHChart;
        public System.Windows.Forms.DataVisualization.Charting.Chart ParticleChart;
        public System.Windows.Forms.DataVisualization.Charting.Chart VibrationChart;
        public System.Windows.Forms.DataVisualization.Charting.Chart PressureChart;
        public System.Windows.Forms.DataVisualization.Charting.Chart TT_RHChartH;
        public System.Windows.Forms.DataVisualization.Charting.Chart PressureChartH;
        public System.Windows.Forms.DataVisualization.Charting.Chart ParticleChartH;
        public System.Windows.Forms.DataVisualization.Charting.Chart VibrationChartH;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}

