﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Data.SqlClient;
using System.IO;

namespace integrated_sensor
{
    public partial class Form1 : Form
    {
        private System.Timers.Timer updateDataTimer;
        GatewayConnection gatewayConnection;
        SqlConnection sqlConnection;

        DataTable dataTable = new DataTable();
        public string FILE_PATH = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "/csv";

        private Dictionary<String, Double> hisAxisYMap = new Dictionary<String, Double>(); //紀錄Y軸最大、最小值
        private static int axisY = 10;   //Y軸上下限增加值

        public Form1()
        {
            InitializeComponent();
            gatewayConnection = new GatewayConnection(this);
            Form_Loading form_loading = new Form_Loading();
            form_loading.Show();
            gatewayConnection.connect(); //先對Gateway做連線
            gatewayConnection.doAction();
            //給預設值
            StratDateTimePicker.Value = new DateTime(this.StratDateTimePicker.Value.Year, this.StratDateTimePicker.Value.Month, this.StratDateTimePicker.Value.Day, 00, 00, 00);
            EndDateTimePicker.Value = new DateTime(this.EndDateTimePicker.Value.Year, this.EndDateTimePicker.Value.Month, this.EndDateTimePicker.Value.Day, 23, 59, 59);
            hisAxisYMap.Add("MAX", 0);
            hisAxisYMap.Add("MIN", 0);
            form_loading.Close();

            //提示未連線成功, 當前連線狀態可由標題 "Realtime Trend Chart" 字體顏色判斷,紅色為未連接,黑色為已連接
            if (gatewayConnection.ConnectState == false)
            {
                MessageBox.Show("Gateway連線失敗,請確認連線問題 !");
            }

            //TIMER
            //===========================================================================
            updateDataTimer = new System.Timers.Timer();
            updateDataTimer.Interval = 1000;
            updateDataTimer.Elapsed += new ElapsedEventHandler(updateData);
            //===========================================================================

            updateDataTimer.Start();
        }

        private void updateData(object sender, EventArgs e){
            updateDataTimer.Stop();
            gatewayConnection.doAction();
            updateDataTimer.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e){
            if (MessageBox.Show("確定關閉監控系統?", "警告", MessageBoxButtons.YesNo) == DialogResult.No)
                e.Cancel = true;
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            connectDB();
            dataTable = getSensorData();
            if (dataTable.Rows.Count == 0)
            {
                MessageBox.Show("沒有符合匯出條件的資料,請重新調整匯出條件 !");
                return;
            }
            SaveToCSV(dataTable, FILE_PATH);
            closeDB();
            MessageBox.Show("Export successfully !");
        }
        private void QueryButton_Click(object sender, EventArgs e)
        {
            connectDB();
            dataTable = getSensorData();
            if (dataTable.Rows.Count == 0){
                MessageBox.Show("沒有符合查詢條件的資料,請重新調整查詢條件 !");
                TT_RHChartH.Series[0].Points.Clear();
                TT_RHChartH.Series[1].Points.Clear();
                PressureChartH.Series[0].Points.Clear();
                ParticleChartH.Series[0].Points.Clear();
                VibrationChartH.Series[0].Points.Clear();
                return;
            }
            HistoricalChartUpdate(dataTable);
            closeDB();
        }

        private void connectDB()
        {
            string connectionString = "Data Source=(local);Integrated Security=SSPI;";

            sqlConnection = new SqlConnection(connectionString);

            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void closeDB()
        {
            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();
        }

        private DataTable getSensorData()
        {
            String sql = $"SELECT * FROM [SENSOR].[dbo].[sensor] " +
                         $"WHERE [recordTime] between convert(DATETIME, '{StratDateTimePicker.Value.ToString("yyyy/MM/dd HH:mm:ss")}') " +
                         $"                       and convert(DATETIME, '{EndDateTimePicker.Value.ToString("yyyy/MM/dd HH:mm:ss")}')" +
                         $"ORDER BY [recordTime] ";

            try
            {
                dataTable.Clear();
                SqlCommand command = new SqlCommand(sql, sqlConnection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dataTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return dataTable;
        }

        public void SaveToCSV(DataTable Table, string FilePath)
        {
            string data = "";
            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }
            String FileName = FilePath + "/" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".csv";
            StreamWriter wr = new StreamWriter(FileName, false, System.Text.Encoding.Default);
            foreach (DataColumn column in Table.Columns)
            {
                data += column.ColumnName + ",";
            }
            data += "\n";
            wr.Write(data);

            data = "";
            foreach (DataRow row in Table.Rows)
            {
                foreach (DataColumn column in Table.Columns)
                {
                    data += row[column].ToString().Trim() + ",";
                }
                data += "\n";
                wr.Write(data);
                data = "";
            }
            data += "\n";

            wr.Dispose();
            wr.Close();
        }

        public void HistoricalChartUpdate(DataTable Table)
        {
            String Pressure = "0";
            String Temperature = "0";
            String Humidity = "0";
            String Particle = "0";
            String Vibration = "0";
            String RecordTime = "0";
            Double pressureMaxValue = 0;
            Double pressureMinValue = 0;
            Double tt_rhMaxValue = 0;
            Double tt_rhMinValue = 0;
            Double particleMaxValue = 0;
            Double particleMinValue = 0;
            Double vibrationMaxValue = 0;
            Double vibrationMinValue = 0;
            bool ifFirst = true;
            TT_RHChartH.Series[0].Points.Clear();
            TT_RHChartH.Series[1].Points.Clear();
            PressureChartH.Series[0].Points.Clear();
            ParticleChartH.Series[0].Points.Clear();
            VibrationChartH.Series[0].Points.Clear();

            foreach (DataRow row in Table.Rows)
            {
                foreach (DataColumn column in Table.Columns)
                {
                    if ("pressure" == column.ColumnName){
                        Pressure = row[column].ToString().Trim();
                        if (ifFirst){
                            pressureMaxValue = Double.Parse(Pressure);
                            pressureMinValue = Double.Parse(Pressure);
                        } 
                        else {
                            hisAxisYMap = compareMaxMin(pressureMaxValue, pressureMinValue, Double.Parse(Pressure));
                            pressureMaxValue = hisAxisYMap["MAX"];
                            pressureMinValue = hisAxisYMap["MIN"];
                        }
                    }

                    if ("temperature" == column.ColumnName)
                    {
                        Temperature = row[column].ToString().Trim();
                        if (ifFirst)
                        {
                            tt_rhMaxValue = Double.Parse(Temperature);
                            tt_rhMinValue = Double.Parse(Temperature);
                        }
                        else
                        {
                            hisAxisYMap = compareMaxMin(tt_rhMaxValue, tt_rhMinValue, Double.Parse(Temperature));
                            tt_rhMaxValue = hisAxisYMap["MAX"];
                            tt_rhMinValue = hisAxisYMap["MIN"];
                        }
                    }

                    if ("humidity" == column.ColumnName)
                    {
                        Humidity = row[column].ToString().Trim();
                        hisAxisYMap = compareMaxMin(tt_rhMaxValue, tt_rhMinValue, Double.Parse(Humidity));
                        tt_rhMaxValue = hisAxisYMap["MAX"];
                        tt_rhMinValue = hisAxisYMap["MIN"];
                    }

                    if ("particle" == column.ColumnName)
                    {
                        Particle = row[column].ToString().Trim();
                        if (ifFirst)
                        {
                            particleMaxValue = Double.Parse(Particle);
                            particleMinValue = Double.Parse(Particle);
                        }
                        else
                        {
                            hisAxisYMap = compareMaxMin(particleMaxValue, particleMinValue, Double.Parse(Particle));
                            particleMaxValue = hisAxisYMap["MAX"];
                            particleMinValue = hisAxisYMap["MIN"];
                        }
                    }

                    if ("vibration" == column.ColumnName)
                    {
                        Vibration = row[column].ToString().Trim();
                        if (ifFirst)
                        {
                            vibrationMaxValue = Double.Parse(Vibration);
                            vibrationMinValue = Double.Parse(Vibration);
                        }
                        else
                        {
                            hisAxisYMap = compareMaxMin(vibrationMaxValue, vibrationMinValue, Double.Parse(Vibration));
                            vibrationMaxValue = hisAxisYMap["MAX"];
                            vibrationMinValue = hisAxisYMap["MIN"];
                        }
                    }

                    if ("recordTime" == column.ColumnName)
                       RecordTime = row[column].ToString().Trim();
                }

                ifFirst = false;
                TT_RHChartH.Series[0].Points.AddXY(RecordTime, Temperature);
                TT_RHChartH.Series[1].Points.AddXY(RecordTime, Humidity);
                PressureChartH.Series[0].Points.AddXY(RecordTime, Pressure);
                ParticleChartH.Series[0].Points.AddXY(RecordTime, Particle);
                VibrationChartH.Series[0].Points.AddXY(RecordTime, Vibration);

            }
            TT_RHChartH.ChartAreas[0].AxisY.Maximum = tt_rhMaxValue + axisY;
            TT_RHChartH.ChartAreas[0].AxisY.Minimum = tt_rhMinValue - axisY;
            PressureChartH.ChartAreas[0].AxisY.Maximum = pressureMaxValue + axisY;
            PressureChartH.ChartAreas[0].AxisY.Minimum = pressureMinValue - axisY;
            ParticleChartH.ChartAreas[0].AxisY.Maximum = particleMaxValue + axisY;
            ParticleChartH.ChartAreas[0].AxisY.Minimum = particleMinValue - axisY;
            VibrationChartH.ChartAreas[0].AxisY.Maximum = vibrationMaxValue + axisY;
            VibrationChartH.ChartAreas[0].AxisY.Minimum = vibrationMinValue - axisY;
        }

        public Dictionary<String, Double> compareMaxMin(Double maxValue, Double minValue, Double value)
        {
            
            if (value > maxValue)
            {
                maxValue = value;
            }
            if (value < minValue)
            {
                minValue = value;
            }

            hisAxisYMap["MAX"] = maxValue;
            hisAxisYMap["MIN"] = minValue;
            return hisAxisYMap;
        }
    }
}
