﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form_User_Edit : Form
    {
        private Form_User_Management form_User_Management;
        private Form1 mainForm;
        SqlConnection sqlConnection;

        public Form_User_Edit(Form_User_Management form, Form1 form1)
        {
            InitializeComponent();
            form_User_Management = form;
            mainForm = form1;
            string connectionString = mainForm.GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
        }

        private void Form_User_Edit_Shown(object sender, EventArgs e)
        {
            accountEditLabel.Text = form_User_Management.accountEditStr;
            passwordEditTextBox.Text = form_User_Management.passwordEditStr;
            adminEditCheckBox.Checked = form_User_Management.adminEditStr == "Y" ? true : false;
        }

        private void userEditCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void userEditButton_Click(object sender, EventArgs e)
        {
            String accountStr = accountEditLabel.Text.Replace(" ", "");
            String passwordStr = passwordEditTextBox.Text.Replace(" ", "");
            String administratorStr = adminEditCheckBox.Checked ? "Y" : "N";

            if (string.IsNullOrWhiteSpace(passwordStr))
            {
                MessageBox.Show("密碼不得空白", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SqlCommand command;

            //驗證修改的是否是最後一筆管理員權限的使用者帳號
            if (form_User_Management.adminEditStr == "Y" && !adminEditCheckBox.Checked) //當原設定有管理員權限,後來被取消權限時
            {
                String checkSql = "SELECT COUNT(1)" +
                                  "  FROM [ICE_STORAGE].[dbo].[USER] " +
                                  " WHERE administrator = 'Y'" +
                                  "   AND account !='"+ accountStr+ "'";

                command = new SqlCommand(checkSql, sqlConnection);
                int adminCount = 0;

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            adminCount = reader.GetInt32(0);
                        }
                    }
                }

                if (adminCount == 0)
                {
                    MessageBox.Show("此組為最後一組管理員權限的帳號,無法修改權限", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            //開始更新資料庫
            String updateUserSql = "UPDATE [ICE_STORAGE].[dbo].[USER] " +
                                   "SET password = '" + passwordStr + "', administrator = '" + administratorStr + "'" +
                                   "WHERE account = '" + accountStr + "'";

            String selectAllSql = "SELECT account,password,administrator FROM [ICE_STORAGE].[dbo].[USER]";

            try
            {
                command = new SqlCommand(updateUserSql, sqlConnection);
                if (command.ExecuteNonQuery().ToString() != "0")
                {
                    this.Close();
                    MessageBox.Show("修改成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SqlDataAdapter newAdapter = new SqlDataAdapter(selectAllSql, sqlConnection);
                    DataSet newDataset = new DataSet();
                    newAdapter.Fill(newDataset, "UPDATE");
                    form_User_Management.UserDataGridView.DataSource = newDataset.Tables["UPDATE"];
                }
                else
                {
                    MessageBox.Show("修改失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            sqlConnection.Close();

        }

        private void passwordEditTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                passwordEditTextBox.Text = passwordEditTextBox.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
