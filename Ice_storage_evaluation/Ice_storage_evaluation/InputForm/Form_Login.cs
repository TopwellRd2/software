﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form_Login : Form
    {
        private Form1 form1;

        SqlConnection sqlConnection;
        public bool isLogin { get; set; }
        public String account { get; set; }
        public String administrator { get; set; }

        public Form_Login(Form1 form)
        {
            InitializeComponent();
            form1 = form;

            string connectionString = form1.GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
        }

        private void loginOkButton_Click(object sender, EventArgs e)
        {
            String accountStr = accountTextBox.Text.Replace(" ", "");
            String passwordStr = passwordTextBox.Text.Replace(" ", "");

            if (string.IsNullOrWhiteSpace(accountStr) || string.IsNullOrWhiteSpace(passwordStr))
            {
                MessageBox.Show("帳號或密碼不得空白", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String loginSql = "SELECT REPLACE(account,' ','') account, administrator " +
                              "  FROM [ICE_STORAGE].[dbo].[USER] " +
                              " WHERE REPLACE(account,' ','') = '"+ accountStr + "'" +
                              "   AND REPLACE(password,' ','') = '" + passwordStr + "'";

            SqlCommand command = new SqlCommand(loginSql, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows){
                    while (reader.Read())
                    {
                        account = reader.GetString(0);
                        administrator = reader.GetString(1);
                    }
                }
                else{
                    MessageBox.Show("請確認帳號或密碼是否正確!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            isLogin = true;
            form1.function_GroupBox.Enabled = true;
            form1.login_Label.Visible = true;
            form1.login_Label.Text = account + " 登入中";
            form1.login_Button.Text = "登出";
            if("Y" == administrator){
                form1.userManagement_CheckBox.Visible = true;
            }
            this.Close();
            
        }

        private void Form_Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();
        }

        private void accountTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                accountTextBox.Text = accountTextBox.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void passwordTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                passwordTextBox.Text = passwordTextBox.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        
    }
}
