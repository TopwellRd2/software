﻿namespace Ice_storage_evaluation
{
    partial class Form_User_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userEditButton = new System.Windows.Forms.Button();
            this.adminEditCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.passwordEditTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.userEditCancelButton = new System.Windows.Forms.Button();
            this.accountEditLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // userEditButton
            // 
            this.userEditButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userEditButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.userEditButton.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.userEditButton.Location = new System.Drawing.Point(628, 72);
            this.userEditButton.Name = "userEditButton";
            this.userEditButton.Size = new System.Drawing.Size(98, 38);
            this.userEditButton.TabIndex = 14;
            this.userEditButton.Text = "修改";
            this.userEditButton.UseVisualStyleBackColor = false;
            this.userEditButton.Click += new System.EventHandler(this.userEditButton_Click);
            // 
            // adminEditCheckBox
            // 
            this.adminEditCheckBox.AutoSize = true;
            this.adminEditCheckBox.Location = new System.Drawing.Point(513, 87);
            this.adminEditCheckBox.Name = "adminEditCheckBox";
            this.adminEditCheckBox.Size = new System.Drawing.Size(18, 17);
            this.adminEditCheckBox.TabIndex = 13;
            this.adminEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(465, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "管理員權限";
            // 
            // passwordEditTextBox
            // 
            this.passwordEditTextBox.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.passwordEditTextBox.Location = new System.Drawing.Point(252, 82);
            this.passwordEditTextBox.Name = "passwordEditTextBox";
            this.passwordEditTextBox.Size = new System.Drawing.Size(179, 32);
            this.passwordEditTextBox.TabIndex = 11;
            this.passwordEditTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.passwordEditTextBox_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(247, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "密碼";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(48, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "使用者帳號";
            // 
            // userEditCancelButton
            // 
            this.userEditCancelButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userEditCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.userEditCancelButton.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.userEditCancelButton.Location = new System.Drawing.Point(758, 72);
            this.userEditCancelButton.Name = "userEditCancelButton";
            this.userEditCancelButton.Size = new System.Drawing.Size(98, 38);
            this.userEditCancelButton.TabIndex = 15;
            this.userEditCancelButton.Text = "取消";
            this.userEditCancelButton.UseVisualStyleBackColor = false;
            this.userEditCancelButton.Click += new System.EventHandler(this.userEditCancelButton_Click);
            // 
            // accountEditLabel
            // 
            this.accountEditLabel.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.accountEditLabel.Location = new System.Drawing.Point(31, 84);
            this.accountEditLabel.Name = "accountEditLabel";
            this.accountEditLabel.Size = new System.Drawing.Size(155, 30);
            this.accountEditLabel.TabIndex = 16;
            this.accountEditLabel.Text = "administrator";
            this.accountEditLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form_User_Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(883, 176);
            this.Controls.Add(this.accountEditLabel);
            this.Controls.Add(this.userEditCancelButton);
            this.Controls.Add(this.userEditButton);
            this.Controls.Add(this.adminEditCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.passwordEditTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_User_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "編輯";
            this.Shown += new System.EventHandler(this.Form_User_Edit_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button userEditButton;
        public System.Windows.Forms.CheckBox adminEditCheckBox;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox passwordEditTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button userEditCancelButton;
        private System.Windows.Forms.Label accountEditLabel;
    }
}