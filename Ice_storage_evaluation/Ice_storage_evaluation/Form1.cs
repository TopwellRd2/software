﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form1 : Form
    {
        public Form_Project_Design_Load form_Project_Design_Load;
        public Form_Mode_Running_Time form_Mode_Running_Time;
        public Form_Evaporator_Type form_Evaporator_Type;
        public Form_Horsepower_Data form_Horsepower_Data;
        public Form_Compressor_Type form_Compressor_Type;
        public Form_Condenser_Type form_Condenser_Type;
        public Form_Air_Conditioning_Load_Equipment form_Air_Conditioning_Load_Equipment;
        public Form_Ice_Storage_Capacity form_Ice_Storage_Capacity;
        public Form_Ice_Storage_Type_Method form_Ice_Storage_Type_Method;
        public Form_Plan_Unit_Price form_Plan_Unit_Price;
        public Form_Plan_Cost_Analysis form_Plan_Cost_Analysis;
        public Form_Basic_Electricity_Price_Management form_Basic_Electricity_Price_Management;
        public Form_Export_Report form_Export_Report;
        public Form_User_Management form_User_Management;
        public Form_Login form_Login;
        SqlConnection sqlConnection;

        public Form1()
        {
            InitializeComponent();
            string connectionString = GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
            //宣告畫面
            form_Project_Design_Load = new Form_Project_Design_Load(this);
            form_Mode_Running_Time = new Form_Mode_Running_Time(this);
            form_Evaporator_Type = new Form_Evaporator_Type(this);
            form_Horsepower_Data = new Form_Horsepower_Data(this);
            form_Compressor_Type = new Form_Compressor_Type(this);
            form_Condenser_Type = new Form_Condenser_Type(this);
            form_Air_Conditioning_Load_Equipment = new Form_Air_Conditioning_Load_Equipment(this);
            form_Ice_Storage_Capacity = new Form_Ice_Storage_Capacity(this);
            form_Ice_Storage_Type_Method = new Form_Ice_Storage_Type_Method(this);
            form_Plan_Unit_Price = new Form_Plan_Unit_Price(this);
            form_Plan_Cost_Analysis = new Form_Plan_Cost_Analysis(this);
            form_Basic_Electricity_Price_Management = new Form_Basic_Electricity_Price_Management(this);
            form_Export_Report = new Form_Export_Report(this);
            form_Login = new Form_Login(this);
            form_User_Management = new Form_User_Management(this);
            form_Login.isLogin = false;
        }

        public string GetConnectionString()
        {
            //return "Data Source=(local);"
            //       + "Integrated Security=SSPI;";
            //return "Data Source = 192.168.0.181;"
            //      + "User = sa; Password = 12;";
            return Properties.Settings.Default.ICE_STORAGEConnectionString;

        }

        private void projectDesignLoad_CheckedChanged(object sender, EventArgs e)
        {
            if (projectDesignLoad_CheckBox.Checked) {
                form_Project_Design_Load.ShowDialog();
                projectDesignLoad_CheckBox1.Checked = true;
            }
        }

        private void modeRunningTimeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (modeRunningTime_CheckBox.Checked){
                form_Mode_Running_Time.ShowDialog();
                modeRunningTime_CheckBox1.Checked = true;
            }
        }

        private void evaporatorType_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (evaporatorType_CheckBox.Checked)
            {
                form_Evaporator_Type.ShowDialog();
                evaporatorType_CheckBox1.Checked = true;
            }
        }

        private void horsepowerData_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (horsepowerData_CheckBox.Checked)
            {
                form_Horsepower_Data.ShowDialog();
                horsepowerData_CheckBox1.Checked = true;
            }
        }

        private void compressorType_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (compressorType_CheckBox.Checked)
            {
                form_Compressor_Type.ShowDialog();
                compressorType_CheckBox1.Checked = true;
            }
        }

        private void condenserType_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (condenserType_CheckBox.Checked)
            {
                form_Condenser_Type.ShowDialog();
                condenserType_CheckBox1.Checked = true;
            }
        }

        private void airConditioningLoadEquipment_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (airConditioningLoadEquipment_CheckBox.Checked)
            {
                form_Air_Conditioning_Load_Equipment.ShowDialog();
                airConditioningLoadEquipment_CheckBox1.Checked = true;
            }
        }

        private void ice_Storage_Capacity_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ice_Storage_Capacity_CheckBox.Checked)
            {
                form_Ice_Storage_Capacity.ShowDialog();
                ice_Storage_Capacity_CheckBox1.Checked = true;
            }
        }

        private void iceStorageTypeMethod_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (iceStorageTypeMethod_CheckBox.Checked)
            {
                form_Ice_Storage_Type_Method.ShowDialog();
                iceStorageTypeMethod_CheckBox1.Checked = true;
            }
        }

        private void planUnitPrice_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (planUnitPrice_CheckBox.Checked)
            {
                form_Plan_Unit_Price.ShowDialog();
                planUnitPrice_CheckBox1.Checked = true;
            }
        }

        private void planCostAnalysis_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (planCostAnalysis_CheckBox.Checked)
            {
                form_Plan_Cost_Analysis.ShowDialog();
                planCostAnalysis_CheckBox1.Checked = true;
            }
        }

        private void basicElectricityPriceManagement_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (basicElectricityPriceManagement_CheckBox.Checked)
            {
                form_Basic_Electricity_Price_Management.ShowDialog();
                basicElectricityPriceManagement_CheckBox1.Checked = true;
            }
        }

        private void exportReport_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (exportReport_CheckBox.Checked)
            {
                form_Export_Report.ShowDialog();
            }
        }

        private void userManagement_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (userManagement_CheckBox.Checked)
            {
                form_User_Management.accountNewTextBox.Text = "";
                form_User_Management.passwordNewTextBox.Text = "";
                form_User_Management.adminNewCheckBox.Checked = false;
                form_User_Management.ShowDialog();
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (!form_Login.isLogin)
            {
                form_Login.accountTextBox.Text = "";
                form_Login.passwordTextBox.Text = "";
                form_Login.ShowDialog();
            }
            else {
                form_Login.isLogin = false;
                function_GroupBox.Enabled = false;
                login_Label.Visible = false;
                userManagement_CheckBox.Visible = false;
                login_Button.Text = "登入";
                CheckBox checkbox;
                foreach (Control control in function_GroupBox.Controls) //把CheckBox清空
                {
                    if (control.Name.Contains("CheckBox")){
                        checkbox = (CheckBox)control;
                        checkbox.Checked = false;
                    }
                }
            }
            
        }
    }
}
