﻿namespace Ice_storage_evaluation
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.projectDesignLoad_CheckBox = new System.Windows.Forms.CheckBox();
            this.modeRunningTime_CheckBox = new System.Windows.Forms.CheckBox();
            this.evaporatorType_CheckBox = new System.Windows.Forms.CheckBox();
            this.horsepowerData_CheckBox = new System.Windows.Forms.CheckBox();
            this.compressorType_CheckBox = new System.Windows.Forms.CheckBox();
            this.condenserType_CheckBox = new System.Windows.Forms.CheckBox();
            this.airConditioningLoadEquipment_CheckBox = new System.Windows.Forms.CheckBox();
            this.ice_Storage_Capacity_CheckBox = new System.Windows.Forms.CheckBox();
            this.iceStorageTypeMethod_CheckBox = new System.Windows.Forms.CheckBox();
            this.planUnitPrice_CheckBox = new System.Windows.Forms.CheckBox();
            this.planCostAnalysis_CheckBox = new System.Windows.Forms.CheckBox();
            this.basicElectricityPriceManagement_CheckBox = new System.Windows.Forms.CheckBox();
            this.function_GroupBox = new System.Windows.Forms.GroupBox();
            this.basicElectricityPriceManagement_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.planCostAnalysis_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.planUnitPrice_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.iceStorageTypeMethod_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.ice_Storage_Capacity_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.airConditioningLoadEquipment_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.condenserType_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.compressorType_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.horsepowerData_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.evaporatorType_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.modeRunningTime_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.projectDesignLoad_CheckBox1 = new System.Windows.Forms.CheckBox();
            this.exportReport_CheckBox = new System.Windows.Forms.CheckBox();
            this.login_Button = new System.Windows.Forms.Button();
            this.login_Label = new System.Windows.Forms.Label();
            this.userManagement_CheckBox = new System.Windows.Forms.CheckBox();
            this.function_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1732, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(0, 938);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1732, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // projectDesignLoad_CheckBox
            // 
            this.projectDesignLoad_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.projectDesignLoad_CheckBox.BackColor = System.Drawing.SystemColors.Menu;
            this.projectDesignLoad_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.projectDesignLoad_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.projectDesignLoad_CheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.projectDesignLoad_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.projectDesignLoad_CheckBox.Location = new System.Drawing.Point(5, 6);
            this.projectDesignLoad_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.projectDesignLoad_CheckBox.Name = "projectDesignLoad_CheckBox";
            this.projectDesignLoad_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.projectDesignLoad_CheckBox.TabIndex = 14;
            this.projectDesignLoad_CheckBox.Text = "專案設計負荷";
            this.projectDesignLoad_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.projectDesignLoad_CheckBox.UseVisualStyleBackColor = false;
            this.projectDesignLoad_CheckBox.CheckedChanged += new System.EventHandler(this.projectDesignLoad_CheckedChanged);
            // 
            // modeRunningTime_CheckBox
            // 
            this.modeRunningTime_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.modeRunningTime_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.modeRunningTime_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modeRunningTime_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.modeRunningTime_CheckBox.Location = new System.Drawing.Point(135, 6);
            this.modeRunningTime_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.modeRunningTime_CheckBox.Name = "modeRunningTime_CheckBox";
            this.modeRunningTime_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.modeRunningTime_CheckBox.TabIndex = 15;
            this.modeRunningTime_CheckBox.Text = "模式運轉時間";
            this.modeRunningTime_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.modeRunningTime_CheckBox.UseVisualStyleBackColor = true;
            this.modeRunningTime_CheckBox.CheckedChanged += new System.EventHandler(this.modeRunningTimeCheckBox_CheckedChanged);
            // 
            // evaporatorType_CheckBox
            // 
            this.evaporatorType_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.evaporatorType_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.evaporatorType_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.evaporatorType_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.evaporatorType_CheckBox.Location = new System.Drawing.Point(265, 6);
            this.evaporatorType_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.evaporatorType_CheckBox.Name = "evaporatorType_CheckBox";
            this.evaporatorType_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.evaporatorType_CheckBox.TabIndex = 16;
            this.evaporatorType_CheckBox.Text = "蒸發器型式";
            this.evaporatorType_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.evaporatorType_CheckBox.UseVisualStyleBackColor = true;
            this.evaporatorType_CheckBox.CheckedChanged += new System.EventHandler(this.evaporatorType_CheckBox_CheckedChanged);
            // 
            // horsepowerData_CheckBox
            // 
            this.horsepowerData_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.horsepowerData_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.horsepowerData_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.horsepowerData_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.horsepowerData_CheckBox.Location = new System.Drawing.Point(395, 6);
            this.horsepowerData_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.horsepowerData_CheckBox.Name = "horsepowerData_CheckBox";
            this.horsepowerData_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.horsepowerData_CheckBox.TabIndex = 17;
            this.horsepowerData_CheckBox.Text = "馬力數據";
            this.horsepowerData_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.horsepowerData_CheckBox.UseVisualStyleBackColor = true;
            this.horsepowerData_CheckBox.CheckedChanged += new System.EventHandler(this.horsepowerData_CheckBox_CheckedChanged);
            // 
            // compressorType_CheckBox
            // 
            this.compressorType_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.compressorType_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.compressorType_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.compressorType_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.compressorType_CheckBox.Location = new System.Drawing.Point(525, 6);
            this.compressorType_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.compressorType_CheckBox.Name = "compressorType_CheckBox";
            this.compressorType_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.compressorType_CheckBox.TabIndex = 18;
            this.compressorType_CheckBox.Text = "壓縮機型式";
            this.compressorType_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.compressorType_CheckBox.UseVisualStyleBackColor = true;
            this.compressorType_CheckBox.CheckedChanged += new System.EventHandler(this.compressorType_CheckBox_CheckedChanged);
            // 
            // condenserType_CheckBox
            // 
            this.condenserType_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.condenserType_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.condenserType_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.condenserType_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.condenserType_CheckBox.Location = new System.Drawing.Point(655, 6);
            this.condenserType_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.condenserType_CheckBox.Name = "condenserType_CheckBox";
            this.condenserType_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.condenserType_CheckBox.TabIndex = 19;
            this.condenserType_CheckBox.Text = "冷凝器型式";
            this.condenserType_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.condenserType_CheckBox.UseVisualStyleBackColor = true;
            this.condenserType_CheckBox.CheckedChanged += new System.EventHandler(this.condenserType_CheckBox_CheckedChanged);
            // 
            // airConditioningLoadEquipment_CheckBox
            // 
            this.airConditioningLoadEquipment_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.airConditioningLoadEquipment_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.airConditioningLoadEquipment_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.airConditioningLoadEquipment_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.airConditioningLoadEquipment_CheckBox.Location = new System.Drawing.Point(785, 6);
            this.airConditioningLoadEquipment_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.airConditioningLoadEquipment_CheckBox.Name = "airConditioningLoadEquipment_CheckBox";
            this.airConditioningLoadEquipment_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.airConditioningLoadEquipment_CheckBox.TabIndex = 20;
            this.airConditioningLoadEquipment_CheckBox.Text = "空調負載設備";
            this.airConditioningLoadEquipment_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.airConditioningLoadEquipment_CheckBox.UseVisualStyleBackColor = true;
            this.airConditioningLoadEquipment_CheckBox.CheckedChanged += new System.EventHandler(this.airConditioningLoadEquipment_CheckBox_CheckedChanged);
            // 
            // ice_Storage_Capacity_CheckBox
            // 
            this.ice_Storage_Capacity_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.ice_Storage_Capacity_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.ice_Storage_Capacity_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ice_Storage_Capacity_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ice_Storage_Capacity_CheckBox.Location = new System.Drawing.Point(915, 6);
            this.ice_Storage_Capacity_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.ice_Storage_Capacity_CheckBox.Name = "ice_Storage_Capacity_CheckBox";
            this.ice_Storage_Capacity_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.ice_Storage_Capacity_CheckBox.TabIndex = 21;
            this.ice_Storage_Capacity_CheckBox.Text = "儲冰容量";
            this.ice_Storage_Capacity_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ice_Storage_Capacity_CheckBox.UseVisualStyleBackColor = true;
            this.ice_Storage_Capacity_CheckBox.CheckedChanged += new System.EventHandler(this.ice_Storage_Capacity_CheckBox_CheckedChanged);
            // 
            // iceStorageTypeMethod_CheckBox
            // 
            this.iceStorageTypeMethod_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.iceStorageTypeMethod_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.iceStorageTypeMethod_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iceStorageTypeMethod_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.iceStorageTypeMethod_CheckBox.Location = new System.Drawing.Point(1045, 6);
            this.iceStorageTypeMethod_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.iceStorageTypeMethod_CheckBox.Name = "iceStorageTypeMethod_CheckBox";
            this.iceStorageTypeMethod_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.iceStorageTypeMethod_CheckBox.TabIndex = 22;
            this.iceStorageTypeMethod_CheckBox.Text = "儲冰類型方式";
            this.iceStorageTypeMethod_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.iceStorageTypeMethod_CheckBox.UseVisualStyleBackColor = true;
            this.iceStorageTypeMethod_CheckBox.CheckedChanged += new System.EventHandler(this.iceStorageTypeMethod_CheckBox_CheckedChanged);
            // 
            // planUnitPrice_CheckBox
            // 
            this.planUnitPrice_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.planUnitPrice_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.planUnitPrice_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.planUnitPrice_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.planUnitPrice_CheckBox.Location = new System.Drawing.Point(1175, 6);
            this.planUnitPrice_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.planUnitPrice_CheckBox.Name = "planUnitPrice_CheckBox";
            this.planUnitPrice_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.planUnitPrice_CheckBox.TabIndex = 23;
            this.planUnitPrice_CheckBox.Text = "計劃單價";
            this.planUnitPrice_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.planUnitPrice_CheckBox.UseVisualStyleBackColor = true;
            this.planUnitPrice_CheckBox.CheckedChanged += new System.EventHandler(this.planUnitPrice_CheckBox_CheckedChanged);
            // 
            // planCostAnalysis_CheckBox
            // 
            this.planCostAnalysis_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.planCostAnalysis_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.planCostAnalysis_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.planCostAnalysis_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.planCostAnalysis_CheckBox.Location = new System.Drawing.Point(1305, 6);
            this.planCostAnalysis_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.planCostAnalysis_CheckBox.Name = "planCostAnalysis_CheckBox";
            this.planCostAnalysis_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.planCostAnalysis_CheckBox.TabIndex = 24;
            this.planCostAnalysis_CheckBox.Text = "計劃費用分析";
            this.planCostAnalysis_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.planCostAnalysis_CheckBox.UseVisualStyleBackColor = true;
            this.planCostAnalysis_CheckBox.CheckedChanged += new System.EventHandler(this.planCostAnalysis_CheckBox_CheckedChanged);
            // 
            // basicElectricityPriceManagement_CheckBox
            // 
            this.basicElectricityPriceManagement_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.basicElectricityPriceManagement_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.basicElectricityPriceManagement_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.basicElectricityPriceManagement_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.basicElectricityPriceManagement_CheckBox.Location = new System.Drawing.Point(1435, 6);
            this.basicElectricityPriceManagement_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.basicElectricityPriceManagement_CheckBox.Name = "basicElectricityPriceManagement_CheckBox";
            this.basicElectricityPriceManagement_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.basicElectricityPriceManagement_CheckBox.TabIndex = 25;
            this.basicElectricityPriceManagement_CheckBox.Text = "基本電價管理";
            this.basicElectricityPriceManagement_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.basicElectricityPriceManagement_CheckBox.UseVisualStyleBackColor = true;
            this.basicElectricityPriceManagement_CheckBox.CheckedChanged += new System.EventHandler(this.basicElectricityPriceManagement_CheckBox_CheckedChanged);
            // 
            // function_GroupBox
            // 
            this.function_GroupBox.BackColor = System.Drawing.SystemColors.Menu;
            this.function_GroupBox.Controls.Add(this.basicElectricityPriceManagement_CheckBox1);
            this.function_GroupBox.Controls.Add(this.planCostAnalysis_CheckBox1);
            this.function_GroupBox.Controls.Add(this.planUnitPrice_CheckBox1);
            this.function_GroupBox.Controls.Add(this.iceStorageTypeMethod_CheckBox1);
            this.function_GroupBox.Controls.Add(this.ice_Storage_Capacity_CheckBox1);
            this.function_GroupBox.Controls.Add(this.airConditioningLoadEquipment_CheckBox1);
            this.function_GroupBox.Controls.Add(this.condenserType_CheckBox1);
            this.function_GroupBox.Controls.Add(this.compressorType_CheckBox1);
            this.function_GroupBox.Controls.Add(this.horsepowerData_CheckBox1);
            this.function_GroupBox.Controls.Add(this.evaporatorType_CheckBox1);
            this.function_GroupBox.Controls.Add(this.modeRunningTime_CheckBox1);
            this.function_GroupBox.Controls.Add(this.projectDesignLoad_CheckBox1);
            this.function_GroupBox.Controls.Add(this.exportReport_CheckBox);
            this.function_GroupBox.Controls.Add(this.basicElectricityPriceManagement_CheckBox);
            this.function_GroupBox.Controls.Add(this.projectDesignLoad_CheckBox);
            this.function_GroupBox.Controls.Add(this.planCostAnalysis_CheckBox);
            this.function_GroupBox.Controls.Add(this.modeRunningTime_CheckBox);
            this.function_GroupBox.Controls.Add(this.planUnitPrice_CheckBox);
            this.function_GroupBox.Controls.Add(this.evaporatorType_CheckBox);
            this.function_GroupBox.Controls.Add(this.iceStorageTypeMethod_CheckBox);
            this.function_GroupBox.Controls.Add(this.horsepowerData_CheckBox);
            this.function_GroupBox.Controls.Add(this.ice_Storage_Capacity_CheckBox);
            this.function_GroupBox.Controls.Add(this.compressorType_CheckBox);
            this.function_GroupBox.Controls.Add(this.airConditioningLoadEquipment_CheckBox);
            this.function_GroupBox.Controls.Add(this.condenserType_CheckBox);
            this.function_GroupBox.Enabled = false;
            this.function_GroupBox.Location = new System.Drawing.Point(14, 32);
            this.function_GroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.function_GroupBox.Name = "function_GroupBox";
            this.function_GroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.function_GroupBox.Size = new System.Drawing.Size(1700, 110);
            this.function_GroupBox.TabIndex = 26;
            this.function_GroupBox.TabStop = false;
            // 
            // basicElectricityPriceManagement_CheckBox1
            // 
            this.basicElectricityPriceManagement_CheckBox1.AutoSize = true;
            this.basicElectricityPriceManagement_CheckBox1.Enabled = false;
            this.basicElectricityPriceManagement_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.basicElectricityPriceManagement_CheckBox1.Location = new System.Drawing.Point(1491, 78);
            this.basicElectricityPriceManagement_CheckBox1.Name = "basicElectricityPriceManagement_CheckBox1";
            this.basicElectricityPriceManagement_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.basicElectricityPriceManagement_CheckBox1.TabIndex = 32;
            this.basicElectricityPriceManagement_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // planCostAnalysis_CheckBox1
            // 
            this.planCostAnalysis_CheckBox1.AutoSize = true;
            this.planCostAnalysis_CheckBox1.Enabled = false;
            this.planCostAnalysis_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.planCostAnalysis_CheckBox1.Location = new System.Drawing.Point(1359, 78);
            this.planCostAnalysis_CheckBox1.Name = "planCostAnalysis_CheckBox1";
            this.planCostAnalysis_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.planCostAnalysis_CheckBox1.TabIndex = 32;
            this.planCostAnalysis_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // planUnitPrice_CheckBox1
            // 
            this.planUnitPrice_CheckBox1.AutoSize = true;
            this.planUnitPrice_CheckBox1.Enabled = false;
            this.planUnitPrice_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.planUnitPrice_CheckBox1.Location = new System.Drawing.Point(1230, 78);
            this.planUnitPrice_CheckBox1.Name = "planUnitPrice_CheckBox1";
            this.planUnitPrice_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.planUnitPrice_CheckBox1.TabIndex = 32;
            this.planUnitPrice_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // iceStorageTypeMethod_CheckBox1
            // 
            this.iceStorageTypeMethod_CheckBox1.AutoSize = true;
            this.iceStorageTypeMethod_CheckBox1.Enabled = false;
            this.iceStorageTypeMethod_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iceStorageTypeMethod_CheckBox1.Location = new System.Drawing.Point(1099, 78);
            this.iceStorageTypeMethod_CheckBox1.Name = "iceStorageTypeMethod_CheckBox1";
            this.iceStorageTypeMethod_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.iceStorageTypeMethod_CheckBox1.TabIndex = 32;
            this.iceStorageTypeMethod_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // ice_Storage_Capacity_CheckBox1
            // 
            this.ice_Storage_Capacity_CheckBox1.AutoSize = true;
            this.ice_Storage_Capacity_CheckBox1.Enabled = false;
            this.ice_Storage_Capacity_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ice_Storage_Capacity_CheckBox1.Location = new System.Drawing.Point(969, 78);
            this.ice_Storage_Capacity_CheckBox1.Name = "ice_Storage_Capacity_CheckBox1";
            this.ice_Storage_Capacity_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.ice_Storage_Capacity_CheckBox1.TabIndex = 32;
            this.ice_Storage_Capacity_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // airConditioningLoadEquipment_CheckBox1
            // 
            this.airConditioningLoadEquipment_CheckBox1.AutoSize = true;
            this.airConditioningLoadEquipment_CheckBox1.Enabled = false;
            this.airConditioningLoadEquipment_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.airConditioningLoadEquipment_CheckBox1.Location = new System.Drawing.Point(839, 78);
            this.airConditioningLoadEquipment_CheckBox1.Name = "airConditioningLoadEquipment_CheckBox1";
            this.airConditioningLoadEquipment_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.airConditioningLoadEquipment_CheckBox1.TabIndex = 32;
            this.airConditioningLoadEquipment_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // condenserType_CheckBox1
            // 
            this.condenserType_CheckBox1.AutoSize = true;
            this.condenserType_CheckBox1.Enabled = false;
            this.condenserType_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.condenserType_CheckBox1.Location = new System.Drawing.Point(710, 78);
            this.condenserType_CheckBox1.Name = "condenserType_CheckBox1";
            this.condenserType_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.condenserType_CheckBox1.TabIndex = 32;
            this.condenserType_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // compressorType_CheckBox1
            // 
            this.compressorType_CheckBox1.AutoSize = true;
            this.compressorType_CheckBox1.Enabled = false;
            this.compressorType_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.compressorType_CheckBox1.Location = new System.Drawing.Point(579, 78);
            this.compressorType_CheckBox1.Name = "compressorType_CheckBox1";
            this.compressorType_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.compressorType_CheckBox1.TabIndex = 32;
            this.compressorType_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // horsepowerData_CheckBox1
            // 
            this.horsepowerData_CheckBox1.AutoSize = true;
            this.horsepowerData_CheckBox1.Enabled = false;
            this.horsepowerData_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.horsepowerData_CheckBox1.Location = new System.Drawing.Point(449, 78);
            this.horsepowerData_CheckBox1.Name = "horsepowerData_CheckBox1";
            this.horsepowerData_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.horsepowerData_CheckBox1.TabIndex = 32;
            this.horsepowerData_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // evaporatorType_CheckBox1
            // 
            this.evaporatorType_CheckBox1.AutoSize = true;
            this.evaporatorType_CheckBox1.Enabled = false;
            this.evaporatorType_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.evaporatorType_CheckBox1.Location = new System.Drawing.Point(319, 78);
            this.evaporatorType_CheckBox1.Name = "evaporatorType_CheckBox1";
            this.evaporatorType_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.evaporatorType_CheckBox1.TabIndex = 32;
            this.evaporatorType_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // modeRunningTime_CheckBox1
            // 
            this.modeRunningTime_CheckBox1.AutoSize = true;
            this.modeRunningTime_CheckBox1.Enabled = false;
            this.modeRunningTime_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.modeRunningTime_CheckBox1.Location = new System.Drawing.Point(190, 78);
            this.modeRunningTime_CheckBox1.Name = "modeRunningTime_CheckBox1";
            this.modeRunningTime_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.modeRunningTime_CheckBox1.TabIndex = 31;
            this.modeRunningTime_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // projectDesignLoad_CheckBox1
            // 
            this.projectDesignLoad_CheckBox1.AutoSize = true;
            this.projectDesignLoad_CheckBox1.Enabled = false;
            this.projectDesignLoad_CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.projectDesignLoad_CheckBox1.Location = new System.Drawing.Point(57, 78);
            this.projectDesignLoad_CheckBox1.Name = "projectDesignLoad_CheckBox1";
            this.projectDesignLoad_CheckBox1.Size = new System.Drawing.Size(15, 14);
            this.projectDesignLoad_CheckBox1.TabIndex = 30;
            this.projectDesignLoad_CheckBox1.UseVisualStyleBackColor = true;
            // 
            // exportReport_CheckBox
            // 
            this.exportReport_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.exportReport_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.exportReport_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportReport_CheckBox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exportReport_CheckBox.Location = new System.Drawing.Point(1565, 6);
            this.exportReport_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.exportReport_CheckBox.Name = "exportReport_CheckBox";
            this.exportReport_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.exportReport_CheckBox.TabIndex = 29;
            this.exportReport_CheckBox.Text = "報告輸出";
            this.exportReport_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.exportReport_CheckBox.UseVisualStyleBackColor = true;
            this.exportReport_CheckBox.CheckedChanged += new System.EventHandler(this.exportReport_CheckBox_CheckedChanged);
            // 
            // login_Button
            // 
            this.login_Button.AutoSize = true;
            this.login_Button.BackColor = System.Drawing.SystemColors.ControlDark;
            this.login_Button.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.login_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.login_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login_Button.Location = new System.Drawing.Point(1603, 0);
            this.login_Button.Margin = new System.Windows.Forms.Padding(2);
            this.login_Button.Name = "login_Button";
            this.login_Button.Size = new System.Drawing.Size(122, 27);
            this.login_Button.TabIndex = 27;
            this.login_Button.Text = "登入";
            this.login_Button.UseVisualStyleBackColor = false;
            this.login_Button.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // login_Label
            // 
            this.login_Label.BackColor = System.Drawing.SystemColors.Window;
            this.login_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.login_Label.ForeColor = System.Drawing.Color.DarkGreen;
            this.login_Label.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.login_Label.Location = new System.Drawing.Point(1297, 3);
            this.login_Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.login_Label.Name = "login_Label";
            this.login_Label.Size = new System.Drawing.Size(302, 24);
            this.login_Label.TabIndex = 28;
            this.login_Label.Text = "administrator 登入中";
            this.login_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.login_Label.Visible = false;
            // 
            // userManagement_CheckBox
            // 
            this.userManagement_CheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.userManagement_CheckBox.BackColor = System.Drawing.Color.Wheat;
            this.userManagement_CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightGray;
            this.userManagement_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.userManagement_CheckBox.Image = ((System.Drawing.Image)(resources.GetObject("userManagement_CheckBox.Image")));
            this.userManagement_CheckBox.Location = new System.Drawing.Point(19, 146);
            this.userManagement_CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.userManagement_CheckBox.Name = "userManagement_CheckBox";
            this.userManagement_CheckBox.Size = new System.Drawing.Size(126, 100);
            this.userManagement_CheckBox.TabIndex = 26;
            this.userManagement_CheckBox.Text = "使用者管理";
            this.userManagement_CheckBox.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.userManagement_CheckBox.UseVisualStyleBackColor = false;
            this.userManagement_CheckBox.Visible = false;
            this.userManagement_CheckBox.CheckedChanged += new System.EventHandler(this.userManagement_CheckBox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1732, 960);
            this.Controls.Add(this.userManagement_CheckBox);
            this.Controls.Add(this.login_Label);
            this.Controls.Add(this.login_Button);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.function_GroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "儲冰空調效益評估";
            this.function_GroupBox.ResumeLayout(false);
            this.function_GroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.CheckBox projectDesignLoad_CheckBox;
        public System.Windows.Forms.GroupBox function_GroupBox;
        public System.Windows.Forms.Label login_Label;
        public System.Windows.Forms.Button login_Button;
        public System.Windows.Forms.CheckBox userManagement_CheckBox;
        public System.Windows.Forms.CheckBox modeRunningTime_CheckBox;
        public System.Windows.Forms.CheckBox evaporatorType_CheckBox;
        public System.Windows.Forms.CheckBox horsepowerData_CheckBox;
        public System.Windows.Forms.CheckBox compressorType_CheckBox;
        public System.Windows.Forms.CheckBox condenserType_CheckBox;
        public System.Windows.Forms.CheckBox airConditioningLoadEquipment_CheckBox;
        public System.Windows.Forms.CheckBox ice_Storage_Capacity_CheckBox;
        public System.Windows.Forms.CheckBox iceStorageTypeMethod_CheckBox;
        public System.Windows.Forms.CheckBox planUnitPrice_CheckBox;
        public System.Windows.Forms.CheckBox planCostAnalysis_CheckBox;
        public System.Windows.Forms.CheckBox basicElectricityPriceManagement_CheckBox;
        public System.Windows.Forms.CheckBox exportReport_CheckBox;
        private System.Windows.Forms.CheckBox projectDesignLoad_CheckBox1;
        private System.Windows.Forms.CheckBox basicElectricityPriceManagement_CheckBox1;
        private System.Windows.Forms.CheckBox planCostAnalysis_CheckBox1;
        private System.Windows.Forms.CheckBox planUnitPrice_CheckBox1;
        private System.Windows.Forms.CheckBox iceStorageTypeMethod_CheckBox1;
        private System.Windows.Forms.CheckBox ice_Storage_Capacity_CheckBox1;
        private System.Windows.Forms.CheckBox airConditioningLoadEquipment_CheckBox1;
        private System.Windows.Forms.CheckBox condenserType_CheckBox1;
        private System.Windows.Forms.CheckBox compressorType_CheckBox1;
        private System.Windows.Forms.CheckBox horsepowerData_CheckBox1;
        private System.Windows.Forms.CheckBox evaporatorType_CheckBox1;
        private System.Windows.Forms.CheckBox modeRunningTime_CheckBox1;
    }
}

