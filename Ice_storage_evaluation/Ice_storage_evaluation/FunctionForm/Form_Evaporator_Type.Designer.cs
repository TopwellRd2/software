﻿namespace Ice_storage_evaluation
{
    partial class Form_Evaporator_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.set_GroupBox = new System.Windows.Forms.GroupBox();
            this.set_TextBox = new System.Windows.Forms.TextBox();
            this.ok_Button = new System.Windows.Forms.Button();
            this.choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.set_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_RadioButton = new System.Windows.Forms.RadioButton();
            this.expand_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_GroupBox.SuspendLayout();
            this.choose_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // set_GroupBox
            // 
            this.set_GroupBox.Controls.Add(this.set_TextBox);
            this.set_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_GroupBox.Location = new System.Drawing.Point(328, 29);
            this.set_GroupBox.Name = "set_GroupBox";
            this.set_GroupBox.Size = new System.Drawing.Size(252, 133);
            this.set_GroupBox.TabIndex = 3;
            this.set_GroupBox.TabStop = false;
            this.set_GroupBox.Text = "自行設定";
            // 
            // set_TextBox
            // 
            this.set_TextBox.Enabled = false;
            this.set_TextBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_TextBox.Location = new System.Drawing.Point(36, 52);
            this.set_TextBox.Name = "set_TextBox";
            this.set_TextBox.Size = new System.Drawing.Size(182, 41);
            this.set_TextBox.TabIndex = 0;
            this.set_TextBox.Text = "0.75";
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ok_Button.Location = new System.Drawing.Point(328, 192);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(252, 86);
            this.ok_Button.TabIndex = 4;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // choose_GroupBox
            // 
            this.choose_GroupBox.Controls.Add(this.set_RadioButton);
            this.choose_GroupBox.Controls.Add(this.full_RadioButton);
            this.choose_GroupBox.Controls.Add(this.expand_RadioButton);
            this.choose_GroupBox.Location = new System.Drawing.Point(25, 29);
            this.choose_GroupBox.Name = "choose_GroupBox";
            this.choose_GroupBox.Size = new System.Drawing.Size(282, 249);
            this.choose_GroupBox.TabIndex = 5;
            this.choose_GroupBox.TabStop = false;
            // 
            // set_RadioButton
            // 
            this.set_RadioButton.AutoSize = true;
            this.set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_RadioButton.Location = new System.Drawing.Point(59, 179);
            this.set_RadioButton.Name = "set_RadioButton";
            this.set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.set_RadioButton.TabIndex = 8;
            this.set_RadioButton.Text = "自行設定";
            this.set_RadioButton.UseVisualStyleBackColor = true;
            this.set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_RadioButton
            // 
            this.full_RadioButton.AutoSize = true;
            this.full_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.full_RadioButton.Location = new System.Drawing.Point(59, 115);
            this.full_RadioButton.Name = "full_RadioButton";
            this.full_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.full_RadioButton.TabIndex = 7;
            this.full_RadioButton.Text = "滿液式";
            this.full_RadioButton.UseVisualStyleBackColor = true;
            this.full_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // expand_RadioButton
            // 
            this.expand_RadioButton.AutoSize = true;
            this.expand_RadioButton.Checked = true;
            this.expand_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.expand_RadioButton.Location = new System.Drawing.Point(59, 52);
            this.expand_RadioButton.Name = "expand_RadioButton";
            this.expand_RadioButton.Size = new System.Drawing.Size(160, 35);
            this.expand_RadioButton.TabIndex = 6;
            this.expand_RadioButton.TabStop = true;
            this.expand_RadioButton.Text = "直接膨脹式";
            this.expand_RadioButton.UseVisualStyleBackColor = true;
            this.expand_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // Form_Evaporator_Type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(609, 307);
            this.Controls.Add(this.choose_GroupBox);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.set_GroupBox);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Evaporator_Type";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "蒸發器型式";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Evaporator_Type_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Evaporator_Type_Closed);
            this.set_GroupBox.ResumeLayout(false);
            this.set_GroupBox.PerformLayout();
            this.choose_GroupBox.ResumeLayout(false);
            this.choose_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox set_GroupBox;
        private System.Windows.Forms.TextBox set_TextBox;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.GroupBox choose_GroupBox;
        private System.Windows.Forms.RadioButton set_RadioButton;
        private System.Windows.Forms.RadioButton full_RadioButton;
        private System.Windows.Forms.RadioButton expand_RadioButton;
    }
}