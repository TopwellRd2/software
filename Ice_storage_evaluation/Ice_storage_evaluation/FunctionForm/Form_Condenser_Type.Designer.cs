﻿namespace Ice_storage_evaluation
{
    partial class Form_Condenser_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.steaming_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_RadioButton = new System.Windows.Forms.RadioButton();
            this.water_RadioButton = new System.Windows.Forms.RadioButton();
            this.air_RadioButton = new System.Windows.Forms.RadioButton();
            this.ok_Button = new System.Windows.Forms.Button();
            this.set_GroupBox = new System.Windows.Forms.GroupBox();
            this.set_TextBox = new System.Windows.Forms.TextBox();
            this.choose_GroupBox.SuspendLayout();
            this.set_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // choose_GroupBox
            // 
            this.choose_GroupBox.Controls.Add(this.steaming_RadioButton);
            this.choose_GroupBox.Controls.Add(this.set_RadioButton);
            this.choose_GroupBox.Controls.Add(this.water_RadioButton);
            this.choose_GroupBox.Controls.Add(this.air_RadioButton);
            this.choose_GroupBox.Location = new System.Drawing.Point(25, 29);
            this.choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Name = "choose_GroupBox";
            this.choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Size = new System.Drawing.Size(282, 249);
            this.choose_GroupBox.TabIndex = 9;
            this.choose_GroupBox.TabStop = false;
            // 
            // steaming_RadioButton
            // 
            this.steaming_RadioButton.AutoSize = true;
            this.steaming_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.steaming_RadioButton.Location = new System.Drawing.Point(74, 136);
            this.steaming_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.steaming_RadioButton.Name = "steaming_RadioButton";
            this.steaming_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.steaming_RadioButton.TabIndex = 9;
            this.steaming_RadioButton.Text = "蒸發式";
            this.steaming_RadioButton.UseVisualStyleBackColor = true;
            this.steaming_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // set_RadioButton
            // 
            this.set_RadioButton.AutoSize = true;
            this.set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_RadioButton.Location = new System.Drawing.Point(74, 191);
            this.set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_RadioButton.Name = "set_RadioButton";
            this.set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.set_RadioButton.TabIndex = 8;
            this.set_RadioButton.Text = "自行設定";
            this.set_RadioButton.UseVisualStyleBackColor = true;
            this.set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // water_RadioButton
            // 
            this.water_RadioButton.AutoSize = true;
            this.water_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.water_RadioButton.Location = new System.Drawing.Point(74, 81);
            this.water_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.water_RadioButton.Name = "water_RadioButton";
            this.water_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.water_RadioButton.TabIndex = 7;
            this.water_RadioButton.Text = "水冷式";
            this.water_RadioButton.UseVisualStyleBackColor = true;
            this.water_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // air_RadioButton
            // 
            this.air_RadioButton.AutoSize = true;
            this.air_RadioButton.Checked = true;
            this.air_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_RadioButton.Location = new System.Drawing.Point(74, 26);
            this.air_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_RadioButton.Name = "air_RadioButton";
            this.air_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.air_RadioButton.TabIndex = 6;
            this.air_RadioButton.TabStop = true;
            this.air_RadioButton.Text = "氣冷式";
            this.air_RadioButton.UseVisualStyleBackColor = true;
            this.air_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.ok_Button.Location = new System.Drawing.Point(328, 192);
            this.ok_Button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(252, 86);
            this.ok_Button.TabIndex = 8;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // set_GroupBox
            // 
            this.set_GroupBox.Controls.Add(this.set_TextBox);
            this.set_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.set_GroupBox.Location = new System.Drawing.Point(328, 29);
            this.set_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_GroupBox.Name = "set_GroupBox";
            this.set_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_GroupBox.Size = new System.Drawing.Size(252, 133);
            this.set_GroupBox.TabIndex = 7;
            this.set_GroupBox.TabStop = false;
            this.set_GroupBox.Text = "自行設定";
            // 
            // set_TextBox
            // 
            this.set_TextBox.Enabled = false;
            this.set_TextBox.Location = new System.Drawing.Point(36, 52);
            this.set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_TextBox.Name = "set_TextBox";
            this.set_TextBox.Size = new System.Drawing.Size(182, 41);
            this.set_TextBox.TabIndex = 0;
            this.set_TextBox.Text = "1.069";
            // 
            // Form_Condenser_Type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(609, 307);
            this.Controls.Add(this.choose_GroupBox);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.set_GroupBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Condenser_Type";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "冷凝器型式";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Condenser_Type_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Condenser_Type_Closed);
            this.choose_GroupBox.ResumeLayout(false);
            this.choose_GroupBox.PerformLayout();
            this.set_GroupBox.ResumeLayout(false);
            this.set_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox choose_GroupBox;
        private System.Windows.Forms.RadioButton steaming_RadioButton;
        private System.Windows.Forms.RadioButton set_RadioButton;
        private System.Windows.Forms.RadioButton water_RadioButton;
        private System.Windows.Forms.RadioButton air_RadioButton;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.GroupBox set_GroupBox;
        private System.Windows.Forms.TextBox set_TextBox;
    }
}