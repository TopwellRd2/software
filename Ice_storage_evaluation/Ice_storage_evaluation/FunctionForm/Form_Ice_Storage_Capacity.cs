﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Ice_Storage_Capacity : Form
    {
        private Form1 form1;
        public Dictionary<string, string> ice_Storage_Capacity_Map { get; set; } = new Dictionary<string, string>();
        public Form_Ice_Storage_Capacity(Form1 form)
        {
            InitializeComponent();
            form1 = form;
        }

        private void Form_Ice_Storage_Capacity_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (set_RadioButton.Checked && string.IsNullOrWhiteSpace(set_TextBox.Text))
            {
                MessageBox.Show("設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                set_TextBox.Text = "";
                set_TextBox.Focus();
                e.Cancel = true;
            }
        }

        private void Form_Ice_Storage_Capacity_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.ice_Storage_Capacity_CheckBox.Checked)
            {
                form1.ice_Storage_Capacity_CheckBox.Checked = false;
            }
        }

        private void initialMap()
        {
            ice_Storage_Capacity_Map.Add("choose", set_TextBox.Text);
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            ice_Storage_Capacity_Map["choose"] = set_TextBox.Text;
            /*
            if (to8_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.2";
            }
            else if (to14_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.1775";
            }
            else if (to20_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.2";
            }
            else if (to30_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.18";
            }
            else if (to50_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.16";
            }
            else if (to100_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.13";
            }
            else if (over100_RadioButton.Checked)
            {
                ice_Storage_Capacity_Map["choose"] = "0.1";
            }
            else
            {
                ice_Storage_Capacity_Map["choose"] = set_TextBox.Text;
            }
            */
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Name == "set_RadioButton")
            {
                set_TextBox.Enabled = true;
                set_TextBox.Text = "";
            }
            else
            {
                set_TextBox.Enabled = false;
                if (radioButton.Name == "to8_RadioButton")
                {
                    set_TextBox.Text = "0.2";
                }
                else if (radioButton.Name == "to14_RadioButton")
                {
                    set_TextBox.Text = "0.1775";
                }
                else if (radioButton.Name == "to20_RadioButton")
                {
                    set_TextBox.Text = "0.2";
                }
                else if (radioButton.Name == "to30_RadioButton")
                {
                    set_TextBox.Text = "0.18";
                }
                else if (radioButton.Name == "to50_RadioButton")
                {
                    set_TextBox.Text = "0.16";
                }
                else if (radioButton.Name == "to100_RadioButton")
                {
                    set_TextBox.Text = "0.13";
                }
                else if (radioButton.Name == "over100_RadioButton")
                {
                    set_TextBox.Text = "0.1";
                }
            }
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
