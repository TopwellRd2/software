﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Ice_Storage_Type_Method : Form
    {
        private Form1 form1;
        public Dictionary<string, string> ice_Storage_Type_Map { get; set; } = new Dictionary<string, string>();

        public Form_Ice_Storage_Type_Method(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Ice_Storage_Type_Method_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.iceStorageTypeMethod_CheckBox.Checked)
            {
                form1.iceStorageTypeMethod_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            ice_Storage_Type_Map.Add("type", "");
            ice_Storage_Type_Map.Add("method", "");
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            if (frozen_RadioButton.Checked)  //全凍結式
            {
                ice_Storage_Type_Map["type"] = "1";
            }
            else if (hockey_RadioButton.Checked)  //冰球式
            {
                ice_Storage_Type_Map["type"] = "2";
            }
            else if (coil_RadioButton.Checked)  //冰盤管式
            {
                ice_Storage_Type_Map["type"] = "3";
            }

            if (full_RadioButton.Checked)  //全量儲冰
            {
                ice_Storage_Type_Map["method"] = "1";
            }
            else   //分量儲冰
            {
                ice_Storage_Type_Map["method"] = "2";
            }
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
