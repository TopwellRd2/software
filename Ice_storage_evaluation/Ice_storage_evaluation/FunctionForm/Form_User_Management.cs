﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form_User_Management : Form
    {
        private Form1 form1;
        private Form_User_Edit form_User_Edit;
        SqlConnection sqlConnection;
        public String accountEditStr { get; set; }
        public String passwordEditStr { get; set; }
        public String adminEditStr { get; set; }

        public Form_User_Management(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            form_User_Edit = new Form_User_Edit(this,form1);
            string connectionString = form1.GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
        }

        private void Form_User_Management_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (form1.userManagement_CheckBox.Checked)
            {
                form1.userManagement_CheckBox.Checked = false;
            }
        }


        private void Form_User_Management_Load(object sender, EventArgs e)
        {
            // TODO: 這行程式碼會將資料載入 'iCE_STORAGE_DataSet.USER' 資料表。您可以視需要進行移動或移除。
            this.uSERTableAdapter.Fill(this.iCE_STORAGE_DataSet.USER);

        }

        private void userNewButton_Click(object sender, EventArgs e)
        {
            String accountStr = accountNewTextBox.Text.Replace(" ", "");
            String passwordStr = passwordNewTextBox.Text.Replace(" ", "");
            String administratorStr = adminNewCheckBox.Checked ? "Y" : "N" ;

            if (string.IsNullOrWhiteSpace(accountStr) || string.IsNullOrWhiteSpace(passwordStr))
            {
                MessageBox.Show("帳號或密碼不得空白", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //驗證使用者帳號是否重複
            String loginSql = "SELECT COUNT(1)" +
                              "  FROM [ICE_STORAGE].[dbo].[USER] " +
                              " WHERE REPLACE(account,' ','') = '" + accountStr + "'" ;

            SqlCommand command = new SqlCommand(loginSql, sqlConnection);
            int accountCount = 0;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        accountCount = reader.GetInt32(0);
                    }
                }
            }

            if (accountCount > 0){
                accountNewTextBox.Text = "";
                MessageBox.Show("使用者帳號不得重複,請重新輸入!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String insertUserSql = "INSERT INTO [ICE_STORAGE].[dbo].[USER]" +
                              "  (account,password,administrator) " +
                              "  VALUES ('"+ accountStr + "' , '"+ passwordStr +"' , '"+ administratorStr +"' )";

            String selectAllSql = "SELECT account,password,administrator FROM [ICE_STORAGE].[dbo].[USER]";

            try
            {
                command = new SqlCommand(insertUserSql, sqlConnection);
                if (command.ExecuteNonQuery().ToString() != "0")
                {
                    MessageBox.Show("新增成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SqlDataAdapter newAdapter = new SqlDataAdapter(selectAllSql, sqlConnection); 
                    DataSet newDataset = new DataSet();
                    newAdapter.Fill(newDataset, "New");
                    UserDataGridView.DataSource = newDataset.Tables["New"];
                    accountNewTextBox.Text = "";
                    passwordNewTextBox.Text = "";
                    adminNewCheckBox.Checked = false;
                } 
                else
                {
                    MessageBox.Show("新增失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            sqlConnection.Close();

        }

        private void UserDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //按下修改鈕時
            if (e.ColumnIndex == 3) {
                accountEditStr = UserDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();
                passwordEditStr = UserDataGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
                adminEditStr = UserDataGridView.Rows[e.RowIndex].Cells[2].Value.ToString();
                form_User_Edit.ShowDialog();
            }

            //按下刪除鈕時
            if (e.ColumnIndex == 4)
            {
                accountEditStr = UserDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();
                adminEditStr = UserDataGridView.Rows[e.RowIndex].Cells[2].Value.ToString();

                if (MessageBox.Show("確定要刪除此使用者帳號 : " + accountEditStr + " ?", "刪除",
                     MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel){
                    return;
                }

                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                    MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                SqlCommand command;

                //驗證刪除的是否為最後一組管理員權限帳號
                if (adminEditStr == "Y")
                {
                    String checkSql = "SELECT COUNT(1)" +
                                      "  FROM [ICE_STORAGE].[dbo].[USER] " +
                                      " WHERE administrator = 'Y'" ;

                    command = new SqlCommand(checkSql, sqlConnection);
                    int adminCount = 0;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                adminCount = reader.GetInt32(0);
                            }
                        }
                    }

                    if (adminCount == 1)
                    {
                        MessageBox.Show("此使用者帳號為最後一組管理員權限,不得刪除!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                String deleteSql = "DELETE FROM [ICE_STORAGE].[dbo].[USER] WHERE account = '"+ accountEditStr + "'";
                String selectAllSql = "SELECT account,password,administrator FROM [ICE_STORAGE].[dbo].[USER]";

                try
                {
                    command = new SqlCommand(deleteSql, sqlConnection);
                    if (command.ExecuteNonQuery().ToString() != "0")
                    {
                        MessageBox.Show("刪除成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SqlDataAdapter newAdapter = new SqlDataAdapter(selectAllSql, sqlConnection);
                        DataSet newDataset = new DataSet();
                        newAdapter.Fill(newDataset, "DELETE");
                        UserDataGridView.DataSource = newDataset.Tables["DELETE"];
                    }
                    else
                    {
                        MessageBox.Show("刪除失敗,請確認網路連線!","警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                sqlConnection.Close();
            }

        }

        private void accountNewTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                accountNewTextBox.Text = accountNewTextBox.Text.Replace(" ","");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void passwordNewTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                passwordNewTextBox.Text = passwordNewTextBox.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

    }
}
