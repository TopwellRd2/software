﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Air_Conditioning_Load_Equipment : Form
    {
        private Form1 form1;
        public Dictionary<string, string> air_Conditioning_Load_Map { get; set; } = new Dictionary<string, string>();
        public Form_Air_Conditioning_Load_Equipment(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Air_Conditioning_Load_Equipment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (set_RadioButton.Checked && string.IsNullOrWhiteSpace(set_TextBox.Text))
            {
                MessageBox.Show("設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                set_TextBox.Text = "";
                set_TextBox.Focus();
                e.Cancel = true;
            }
        }

        private void Form_Air_Conditioning_Load_Equipment_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.airConditioningLoadEquipment_CheckBox.Checked)
            {
                form1.airConditioningLoadEquipment_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            air_Conditioning_Load_Map.Add("choose", set_TextBox.Text);
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            air_Conditioning_Load_Map["choose"] = set_TextBox.Text;
            /*
            if (blower_RadioButton.Checked)
            {
                if (to3_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.25";
                } 
                else if (to5_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.2";
                }
                else if (to8_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.162";
                }
                else if (to12_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.125";
                }
                else if (than12_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.11";
                }
            }
            else if (air_RadioButton.Checked)
            {
                if (to3_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.32";
                }
                else if (to5_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.256";
                }
                else if (to8_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.208";
                }
                else if (to12_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.16";
                }
                else if (than12_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.14";
                }
            }
            else if (mix_RadioButton.Checked)
            {
                if (to3_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.285";
                }
                else if (to5_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.228";
                }
                else if (to8_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.185";
                }
                else if (to12_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.142";
                }
                else if (than12_RadioButton.Checked)
                {
                    air_Conditioning_Load_Map["choose"] = "0.125";
                }

            }
            else
            {
                air_Conditioning_Load_Map["choose"] = set_TextBox.Text;
            }
            */
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Name == "set_RadioButton")
            {
                set_TextBox.Enabled = true;
                set_TextBox.Text = "";
            }
            else
            {
                if (radioButton.Parent.Name == "loadChoose_GroupBox")
                {
                    set_TextBox.Enabled = false;
                }

                if (blower_RadioButton.Checked )
                {
                    if (to3_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.25";
                    }
                    else if (to5_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.2";
                    }
                    else if (to8_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.162";
                    }
                    else if (to12_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.125";
                    }
                    else if (than12_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.11";
                    }
                }
                else if (air_RadioButton.Checked)
                {
                    if (to3_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.32";
                    }
                    else if (to5_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.256";
                    }
                    else if (to8_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.208";
                    }
                    else if (to12_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.16";
                    }
                    else if (than12_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.14";
                    }
                }
                else if (mix_RadioButton.Checked)
                {
                    if (to3_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.285";
                    }
                    else if (to5_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.228";
                    }
                    else if (to8_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.185";
                    }
                    else if (to12_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.142";
                    }
                    else if (than12_RadioButton.Checked)
                    {
                        set_TextBox.Text = "0.125";
                    }
                }
            }
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
