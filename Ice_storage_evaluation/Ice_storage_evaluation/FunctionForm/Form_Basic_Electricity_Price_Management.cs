﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Basic_Electricity_Price_Management : Form
    {
        private CheckBox basicElectricityPriceManagement_CheckBox;
        public Form_Basic_Electricity_Price_Management(CheckBox checkBox)
        {
            InitializeComponent();
            basicElectricityPriceManagement_CheckBox = checkBox;
        }

        private void Form_Basic_Electricity_Price_Management_Closed(object sender, FormClosedEventArgs e)
        {
            if (basicElectricityPriceManagement_CheckBox.Checked)
            {
                basicElectricityPriceManagement_CheckBox.Checked = false;
            }
        }
    }
}
