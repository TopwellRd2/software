﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Compressor_Type : Form
    {
        private Form1 form1;
        public Dictionary<string, string> compressor_Type_Map { get; set; } = new Dictionary<string, string>();
        public Form_Compressor_Type(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Compressor_Type_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (set_RadioButton.Checked && string.IsNullOrWhiteSpace(set_TextBox.Text))
            {
                MessageBox.Show("設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                set_TextBox.Text = "";
                set_TextBox.Focus();
                e.Cancel = true;
            }
        }

        private void Form_Compressor_Type_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.compressorType_CheckBox.Checked)
            {
                form1.compressorType_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            compressor_Type_Map.Add("choose", set_TextBox.Text);
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            compressor_Type_Map["choose"] = set_TextBox.Text;
            /*
            if (repeat_RadioButton.Checked)
            {
                compressor_Type_Map["choose"] = "0.9";
            }
            else if (screw_RadioButton.Checked)
            {
                compressor_Type_Map["choose"] = "0.8";
            } 
            else if (centrifugal_RadioButton.Checked)
            {
                compressor_Type_Map["choose"] = "0.7";
            }
            else
            {
                compressor_Type_Map["choose"] = set_TextBox.Text;
            }
            */
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Name == "set_RadioButton")
            {
                set_TextBox.Enabled = true;
                set_TextBox.Text = "";
            }
            else
            {
                set_TextBox.Enabled = false;
                if (radioButton.Name == "repeat_RadioButton")
                {
                    set_TextBox.Text = "0.9";
                }
                else if (radioButton.Name == "screw_RadioButton")
                {
                    set_TextBox.Text = "0.8";
                }
                else if (radioButton.Name == "centrifugal_RadioButton")
                {
                    set_TextBox.Text = "0.7";
                }
            }
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
