﻿namespace Ice_storage_evaluation
{
    partial class Form_Ice_Storage_Type_Method
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.coil_RadioButton = new System.Windows.Forms.RadioButton();
            this.hockey_RadioButton = new System.Windows.Forms.RadioButton();
            this.frozen_RadioButton = new System.Windows.Forms.RadioButton();
            this.ok_Button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.part_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_RadioButton = new System.Windows.Forms.RadioButton();
            this.choose_GroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // choose_GroupBox
            // 
            this.choose_GroupBox.Controls.Add(this.coil_RadioButton);
            this.choose_GroupBox.Controls.Add(this.hockey_RadioButton);
            this.choose_GroupBox.Controls.Add(this.frozen_RadioButton);
            this.choose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.choose_GroupBox.Location = new System.Drawing.Point(12, 13);
            this.choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Name = "choose_GroupBox";
            this.choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Size = new System.Drawing.Size(289, 263);
            this.choose_GroupBox.TabIndex = 11;
            this.choose_GroupBox.TabStop = false;
            this.choose_GroupBox.Text = "儲冰類型";
            // 
            // coil_RadioButton
            // 
            this.coil_RadioButton.AutoSize = true;
            this.coil_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.coil_RadioButton.Location = new System.Drawing.Point(73, 178);
            this.coil_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coil_RadioButton.Name = "coil_RadioButton";
            this.coil_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.coil_RadioButton.TabIndex = 9;
            this.coil_RadioButton.Text = "冰盤管式";
            this.coil_RadioButton.UseVisualStyleBackColor = true;
            // 
            // hockey_RadioButton
            // 
            this.hockey_RadioButton.AutoSize = true;
            this.hockey_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.hockey_RadioButton.Location = new System.Drawing.Point(73, 118);
            this.hockey_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.hockey_RadioButton.Name = "hockey_RadioButton";
            this.hockey_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.hockey_RadioButton.TabIndex = 7;
            this.hockey_RadioButton.Text = "冰球式";
            this.hockey_RadioButton.UseVisualStyleBackColor = true;
            // 
            // frozen_RadioButton
            // 
            this.frozen_RadioButton.AutoSize = true;
            this.frozen_RadioButton.Checked = true;
            this.frozen_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.frozen_RadioButton.Location = new System.Drawing.Point(73, 58);
            this.frozen_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.frozen_RadioButton.Name = "frozen_RadioButton";
            this.frozen_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.frozen_RadioButton.TabIndex = 6;
            this.frozen_RadioButton.TabStop = true;
            this.frozen_RadioButton.Text = "全凍結式";
            this.frozen_RadioButton.UseVisualStyleBackColor = true;
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.ok_Button.Location = new System.Drawing.Point(316, 212);
            this.ok_Button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(282, 64);
            this.ok_Button.TabIndex = 10;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.part_RadioButton);
            this.groupBox1.Controls.Add(this.full_RadioButton);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.groupBox1.Location = new System.Drawing.Point(316, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(282, 187);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "儲冰方式";
            // 
            // part_RadioButton
            // 
            this.part_RadioButton.AutoSize = true;
            this.part_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.part_RadioButton.Location = new System.Drawing.Point(76, 118);
            this.part_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.part_RadioButton.Name = "part_RadioButton";
            this.part_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.part_RadioButton.TabIndex = 7;
            this.part_RadioButton.Text = "分量儲冰";
            this.part_RadioButton.UseVisualStyleBackColor = true;
            // 
            // full_RadioButton
            // 
            this.full_RadioButton.AutoSize = true;
            this.full_RadioButton.Checked = true;
            this.full_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.full_RadioButton.Location = new System.Drawing.Point(76, 58);
            this.full_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.full_RadioButton.Name = "full_RadioButton";
            this.full_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.full_RadioButton.TabIndex = 6;
            this.full_RadioButton.TabStop = true;
            this.full_RadioButton.Text = "全量儲冰";
            this.full_RadioButton.UseVisualStyleBackColor = true;
            // 
            // Form_Ice_Storage_Type_Method
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(612, 289);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.choose_GroupBox);
            this.Controls.Add(this.ok_Button);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Ice_Storage_Type_Method";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "儲冰類型及方式";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Ice_Storage_Type_Method_Closed);
            this.choose_GroupBox.ResumeLayout(false);
            this.choose_GroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox choose_GroupBox;
        private System.Windows.Forms.RadioButton coil_RadioButton;
        private System.Windows.Forms.RadioButton hockey_RadioButton;
        private System.Windows.Forms.RadioButton frozen_RadioButton;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton part_RadioButton;
        private System.Windows.Forms.RadioButton full_RadioButton;
    }
}