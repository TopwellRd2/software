﻿namespace Ice_storage_evaluation
{
    partial class Form_Mode_Running_Time
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.full_Ice_Hour_TextBox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.full_Air_Hour_TextBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.part_Air_Hour_TextBox = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.part_Ice_Hour_TextBox = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.full_Rollback_Button = new System.Windows.Forms.Button();
            this.part_Rollback_Button = new System.Windows.Forms.Button();
            this.mode_Running_Time_Ok_Button = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.full_Air_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.full_Air_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.full_Air_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.full_Air_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.full_Air_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.full_Air_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.full_Air_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.full_Air_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.full_Air_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.full_Air_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.full_Air_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel12 = new System.Windows.Forms.Panel();
            this.full_Air_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel13 = new System.Windows.Forms.Panel();
            this.full_Air_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel14 = new System.Windows.Forms.Panel();
            this.full_Air_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.full_Air_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel16 = new System.Windows.Forms.Panel();
            this.full_Air_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel17 = new System.Windows.Forms.Panel();
            this.full_Air_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel18 = new System.Windows.Forms.Panel();
            this.full_Air_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel19 = new System.Windows.Forms.Panel();
            this.full_Air_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel20 = new System.Windows.Forms.Panel();
            this.full_Air_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel21 = new System.Windows.Forms.Panel();
            this.full_Air_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel22 = new System.Windows.Forms.Panel();
            this.full_Air_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel23 = new System.Windows.Forms.Panel();
            this.full_Air_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel24 = new System.Windows.Forms.Panel();
            this.full_Air_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel25 = new System.Windows.Forms.Panel();
            this.part_Air_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel26 = new System.Windows.Forms.Panel();
            this.part_Air_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel27 = new System.Windows.Forms.Panel();
            this.part_Air_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel28 = new System.Windows.Forms.Panel();
            this.part_Air_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel29 = new System.Windows.Forms.Panel();
            this.part_Air_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel30 = new System.Windows.Forms.Panel();
            this.part_Air_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel31 = new System.Windows.Forms.Panel();
            this.part_Air_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel32 = new System.Windows.Forms.Panel();
            this.part_Air_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel33 = new System.Windows.Forms.Panel();
            this.part_Air_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel34 = new System.Windows.Forms.Panel();
            this.part_Air_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel35 = new System.Windows.Forms.Panel();
            this.part_Air_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel36 = new System.Windows.Forms.Panel();
            this.part_Air_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel37 = new System.Windows.Forms.Panel();
            this.part_Air_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel38 = new System.Windows.Forms.Panel();
            this.part_Air_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel39 = new System.Windows.Forms.Panel();
            this.part_Air_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel40 = new System.Windows.Forms.Panel();
            this.part_Air_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel41 = new System.Windows.Forms.Panel();
            this.part_Air_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel42 = new System.Windows.Forms.Panel();
            this.part_Air_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel43 = new System.Windows.Forms.Panel();
            this.part_Air_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel44 = new System.Windows.Forms.Panel();
            this.part_Air_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel45 = new System.Windows.Forms.Panel();
            this.part_Air_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel46 = new System.Windows.Forms.Panel();
            this.part_Air_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel47 = new System.Windows.Forms.Panel();
            this.part_Air_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel48 = new System.Windows.Forms.Panel();
            this.part_Air_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel49 = new System.Windows.Forms.Panel();
            this.trough_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_010_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel50 = new System.Windows.Forms.Panel();
            this.trough_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_013_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel51 = new System.Windows.Forms.Panel();
            this.trough_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_020_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel52 = new System.Windows.Forms.Panel();
            this.trough_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_023_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel53 = new System.Windows.Forms.Panel();
            this.trough_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_030_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel54 = new System.Windows.Forms.Panel();
            this.trough_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_033_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel55 = new System.Windows.Forms.Panel();
            this.trough_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_040_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel56 = new System.Windows.Forms.Panel();
            this.trough_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_043_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel57 = new System.Windows.Forms.Panel();
            this.trough_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_050_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel58 = new System.Windows.Forms.Panel();
            this.trough_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_053_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel59 = new System.Windows.Forms.Panel();
            this.trough_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_060_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel60 = new System.Windows.Forms.Panel();
            this.trough_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_063_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel61 = new System.Windows.Forms.Panel();
            this.trough_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_070_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel62 = new System.Windows.Forms.Panel();
            this.trough_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_073_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel63 = new System.Windows.Forms.Panel();
            this.trough_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_080_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel64 = new System.Windows.Forms.Panel();
            this.trough_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_083_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel65 = new System.Windows.Forms.Panel();
            this.trough_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_090_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel66 = new System.Windows.Forms.Panel();
            this.trough_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_093_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel67 = new System.Windows.Forms.Panel();
            this.trough_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_100_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel68 = new System.Windows.Forms.Panel();
            this.trough_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_103_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel69 = new System.Windows.Forms.Panel();
            this.trough_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_110_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel70 = new System.Windows.Forms.Panel();
            this.trough_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_113_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel71 = new System.Windows.Forms.Panel();
            this.trough_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_120_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel72 = new System.Windows.Forms.Panel();
            this.trough_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_123_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Air_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel73 = new System.Windows.Forms.Panel();
            this.panel74 = new System.Windows.Forms.Panel();
            this.full_Air_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel75 = new System.Windows.Forms.Panel();
            this.full_Air_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel76 = new System.Windows.Forms.Panel();
            this.full_Air_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel77 = new System.Windows.Forms.Panel();
            this.full_Air_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel78 = new System.Windows.Forms.Panel();
            this.full_Air_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel79 = new System.Windows.Forms.Panel();
            this.full_Air_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel80 = new System.Windows.Forms.Panel();
            this.full_Air_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel81 = new System.Windows.Forms.Panel();
            this.full_Air_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel82 = new System.Windows.Forms.Panel();
            this.full_Air_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel83 = new System.Windows.Forms.Panel();
            this.full_Air_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel84 = new System.Windows.Forms.Panel();
            this.full_Air_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel85 = new System.Windows.Forms.Panel();
            this.full_Air_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel86 = new System.Windows.Forms.Panel();
            this.full_Air_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel87 = new System.Windows.Forms.Panel();
            this.full_Air_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel88 = new System.Windows.Forms.Panel();
            this.full_Air_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel89 = new System.Windows.Forms.Panel();
            this.full_Air_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel90 = new System.Windows.Forms.Panel();
            this.full_Air_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel91 = new System.Windows.Forms.Panel();
            this.full_Air_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel92 = new System.Windows.Forms.Panel();
            this.full_Air_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Air_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel93 = new System.Windows.Forms.Panel();
            this.panel94 = new System.Windows.Forms.Panel();
            this.full_Air_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel95 = new System.Windows.Forms.Panel();
            this.full_Air_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel96 = new System.Windows.Forms.Panel();
            this.full_Air_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.full_Ice_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel97 = new System.Windows.Forms.Panel();
            this.part_Air_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel98 = new System.Windows.Forms.Panel();
            this.part_Air_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel99 = new System.Windows.Forms.Panel();
            this.part_Air_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel100 = new System.Windows.Forms.Panel();
            this.part_Air_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel101 = new System.Windows.Forms.Panel();
            this.part_Air_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel102 = new System.Windows.Forms.Panel();
            this.part_Air_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel103 = new System.Windows.Forms.Panel();
            this.part_Air_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel104 = new System.Windows.Forms.Panel();
            this.part_Air_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel105 = new System.Windows.Forms.Panel();
            this.part_Air_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel106 = new System.Windows.Forms.Panel();
            this.part_Air_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel107 = new System.Windows.Forms.Panel();
            this.part_Air_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel108 = new System.Windows.Forms.Panel();
            this.part_Air_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel109 = new System.Windows.Forms.Panel();
            this.part_Air_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel110 = new System.Windows.Forms.Panel();
            this.part_Air_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel111 = new System.Windows.Forms.Panel();
            this.part_Air_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel112 = new System.Windows.Forms.Panel();
            this.part_Air_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel113 = new System.Windows.Forms.Panel();
            this.part_Air_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel114 = new System.Windows.Forms.Panel();
            this.part_Air_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel115 = new System.Windows.Forms.Panel();
            this.part_Air_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel116 = new System.Windows.Forms.Panel();
            this.part_Air_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel117 = new System.Windows.Forms.Panel();
            this.part_Air_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel118 = new System.Windows.Forms.Panel();
            this.part_Air_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel119 = new System.Windows.Forms.Panel();
            this.part_Air_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel120 = new System.Windows.Forms.Panel();
            this.part_Air_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.part_Ice_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel121 = new System.Windows.Forms.Panel();
            this.trough_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_130_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel122 = new System.Windows.Forms.Panel();
            this.trough_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_133_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel123 = new System.Windows.Forms.Panel();
            this.trough_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_140_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel124 = new System.Windows.Forms.Panel();
            this.trough_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_143_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel125 = new System.Windows.Forms.Panel();
            this.trough_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_150_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel126 = new System.Windows.Forms.Panel();
            this.trough_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_153_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel127 = new System.Windows.Forms.Panel();
            this.trough_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_160_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel128 = new System.Windows.Forms.Panel();
            this.trough_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_163_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel129 = new System.Windows.Forms.Panel();
            this.trough_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_170_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel130 = new System.Windows.Forms.Panel();
            this.trough_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_173_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel131 = new System.Windows.Forms.Panel();
            this.trough_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_180_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel132 = new System.Windows.Forms.Panel();
            this.trough_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_183_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel133 = new System.Windows.Forms.Panel();
            this.trough_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_190_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel134 = new System.Windows.Forms.Panel();
            this.trough_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_193_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel135 = new System.Windows.Forms.Panel();
            this.trough_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_200_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel136 = new System.Windows.Forms.Panel();
            this.trough_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_203_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel137 = new System.Windows.Forms.Panel();
            this.trough_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_210_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel138 = new System.Windows.Forms.Panel();
            this.trough_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_213_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel139 = new System.Windows.Forms.Panel();
            this.trough_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_220_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel140 = new System.Windows.Forms.Panel();
            this.trough_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_223_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel141 = new System.Windows.Forms.Panel();
            this.trough_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_230_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel142 = new System.Windows.Forms.Panel();
            this.trough_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_233_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel143 = new System.Windows.Forms.Panel();
            this.trough_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_240_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel144 = new System.Windows.Forms.Panel();
            this.trough_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.flat_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.crest_003_RadioButton = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel72.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel81.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel83.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel87.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panel91.SuspendLayout();
            this.panel92.SuspendLayout();
            this.panel93.SuspendLayout();
            this.panel94.SuspendLayout();
            this.panel95.SuspendLayout();
            this.panel96.SuspendLayout();
            this.panel97.SuspendLayout();
            this.panel98.SuspendLayout();
            this.panel99.SuspendLayout();
            this.panel100.SuspendLayout();
            this.panel101.SuspendLayout();
            this.panel102.SuspendLayout();
            this.panel103.SuspendLayout();
            this.panel104.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel108.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel110.SuspendLayout();
            this.panel111.SuspendLayout();
            this.panel112.SuspendLayout();
            this.panel113.SuspendLayout();
            this.panel114.SuspendLayout();
            this.panel115.SuspendLayout();
            this.panel116.SuspendLayout();
            this.panel117.SuspendLayout();
            this.panel118.SuspendLayout();
            this.panel119.SuspendLayout();
            this.panel120.SuspendLayout();
            this.panel121.SuspendLayout();
            this.panel122.SuspendLayout();
            this.panel123.SuspendLayout();
            this.panel124.SuspendLayout();
            this.panel125.SuspendLayout();
            this.panel126.SuspendLayout();
            this.panel127.SuspendLayout();
            this.panel128.SuspendLayout();
            this.panel129.SuspendLayout();
            this.panel130.SuspendLayout();
            this.panel131.SuspendLayout();
            this.panel132.SuspendLayout();
            this.panel133.SuspendLayout();
            this.panel134.SuspendLayout();
            this.panel135.SuspendLayout();
            this.panel136.SuspendLayout();
            this.panel137.SuspendLayout();
            this.panel138.SuspendLayout();
            this.panel139.SuspendLayout();
            this.panel140.SuspendLayout();
            this.panel141.SuspendLayout();
            this.panel142.SuspendLayout();
            this.panel143.SuspendLayout();
            this.panel144.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(15, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1350, 2);
            this.label1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(1399, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(2, 750);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(1604, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(2, 750);
            this.label3.TabIndex = 2;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(22, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "時";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(85, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "段";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(180, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 22);
            this.label6.TabIndex = 5;
            this.label6.Text = "全";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(237, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 22);
            this.label7.TabIndex = 6;
            this.label7.Text = "量";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(393, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 22);
            this.label8.TabIndex = 8;
            this.label8.Text = "量";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(336, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 22);
            this.label9.TabIndex = 7;
            this.label9.Text = "分";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(160, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 22);
            this.label10.TabIndex = 9;
            this.label10.Text = "儲冰";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(237, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 22);
            this.label11.TabIndex = 10;
            this.label11.Text = "空調";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(391, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 22);
            this.label12.TabIndex = 12;
            this.label12.Text = "空調";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(314, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 22);
            this.label13.TabIndex = 11;
            this.label13.Text = "儲冰";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(526, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 22);
            this.label14.TabIndex = 13;
            this.label14.Text = "電價時段";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(484, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 22);
            this.label15.TabIndex = 14;
            this.label15.Text = "波峰";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(561, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 22);
            this.label16.TabIndex = 15;
            this.label16.Text = "平";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(622, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 22);
            this.label17.TabIndex = 16;
            this.label17.Text = "波谷";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(1317, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 22);
            this.label18.TabIndex = 30;
            this.label18.Text = "波谷";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(1256, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 22);
            this.label19.TabIndex = 29;
            this.label19.Text = "平";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(1179, 68);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 22);
            this.label20.TabIndex = 28;
            this.label20.Text = "波峰";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(1221, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 22);
            this.label21.TabIndex = 27;
            this.label21.Text = "電價時段";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(1086, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 22);
            this.label22.TabIndex = 26;
            this.label22.Text = "空調";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(1009, 68);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 22);
            this.label23.TabIndex = 25;
            this.label23.Text = "儲冰";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(932, 68);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 22);
            this.label24.TabIndex = 24;
            this.label24.Text = "空調";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(855, 68);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 22);
            this.label25.TabIndex = 23;
            this.label25.Text = "儲冰";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(1088, 28);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(32, 22);
            this.label26.TabIndex = 22;
            this.label26.Text = "量";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(1031, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 22);
            this.label27.TabIndex = 21;
            this.label27.Text = "分";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(932, 28);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(32, 22);
            this.label28.TabIndex = 20;
            this.label28.Text = "量";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(875, 28);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(32, 22);
            this.label29.TabIndex = 19;
            this.label29.Text = "全";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(780, 43);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 22);
            this.label30.TabIndex = 18;
            this.label30.Text = "段";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(717, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 22);
            this.label31.TabIndex = 17;
            this.label31.Text = "時";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Cyan;
            this.label32.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(1459, 103);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(98, 22);
            this.label32.TabIndex = 31;
            this.label32.Text = "全量儲冰";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(1419, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(120, 22);
            this.label33.TabIndex = 32;
            this.label33.Text = "儲冰時段共";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // full_Ice_Hour_TextBox
            // 
            this.full_Ice_Hour_TextBox.Enabled = false;
            this.full_Ice_Hour_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.full_Ice_Hour_TextBox.Location = new System.Drawing.Point(1423, 173);
            this.full_Ice_Hour_TextBox.Name = "full_Ice_Hour_TextBox";
            this.full_Ice_Hour_TextBox.Size = new System.Drawing.Size(94, 30);
            this.full_Ice_Hour_TextBox.TabIndex = 33;
            this.full_Ice_Hour_TextBox.Text = "9";
            this.full_Ice_Hour_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(1535, 176);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 22);
            this.label34.TabIndex = 34;
            this.label34.Text = "小時";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(1535, 251);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 22);
            this.label35.TabIndex = 37;
            this.label35.Text = "小時";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // full_Air_Hour_TextBox
            // 
            this.full_Air_Hour_TextBox.Enabled = false;
            this.full_Air_Hour_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.full_Air_Hour_TextBox.Location = new System.Drawing.Point(1423, 248);
            this.full_Air_Hour_TextBox.Name = "full_Air_Hour_TextBox";
            this.full_Air_Hour_TextBox.Size = new System.Drawing.Size(94, 30);
            this.full_Air_Hour_TextBox.TabIndex = 36;
            this.full_Air_Hour_TextBox.Text = "11";
            this.full_Air_Hour_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label36.Location = new System.Drawing.Point(1419, 214);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(120, 22);
            this.label36.TabIndex = 35;
            this.label36.Text = "空調時段共";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(1535, 438);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 22);
            this.label37.TabIndex = 44;
            this.label37.Text = "小時";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // part_Air_Hour_TextBox
            // 
            this.part_Air_Hour_TextBox.Enabled = false;
            this.part_Air_Hour_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.part_Air_Hour_TextBox.Location = new System.Drawing.Point(1423, 435);
            this.part_Air_Hour_TextBox.Name = "part_Air_Hour_TextBox";
            this.part_Air_Hour_TextBox.Size = new System.Drawing.Size(94, 30);
            this.part_Air_Hour_TextBox.TabIndex = 43;
            this.part_Air_Hour_TextBox.Text = "11";
            this.part_Air_Hour_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label38.Location = new System.Drawing.Point(1419, 401);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(120, 22);
            this.label38.TabIndex = 42;
            this.label38.Text = "空調時段共";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(1535, 363);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 22);
            this.label39.TabIndex = 41;
            this.label39.Text = "小時";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // part_Ice_Hour_TextBox
            // 
            this.part_Ice_Hour_TextBox.Enabled = false;
            this.part_Ice_Hour_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.part_Ice_Hour_TextBox.Location = new System.Drawing.Point(1423, 360);
            this.part_Ice_Hour_TextBox.Name = "part_Ice_Hour_TextBox";
            this.part_Ice_Hour_TextBox.Size = new System.Drawing.Size(94, 30);
            this.part_Ice_Hour_TextBox.TabIndex = 40;
            this.part_Ice_Hour_TextBox.Text = "9";
            this.part_Ice_Hour_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(1419, 326);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(120, 22);
            this.label40.TabIndex = 39;
            this.label40.Text = "儲冰時段共";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label41.Font = new System.Drawing.Font("新細明體", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(1459, 290);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(98, 22);
            this.label41.TabIndex = 38;
            this.label41.Text = "分量儲冰";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label42.Location = new System.Drawing.Point(1423, 492);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(165, 2);
            this.label42.TabIndex = 45;
            // 
            // full_Rollback_Button
            // 
            this.full_Rollback_Button.Location = new System.Drawing.Point(1423, 522);
            this.full_Rollback_Button.Name = "full_Rollback_Button";
            this.full_Rollback_Button.Size = new System.Drawing.Size(165, 49);
            this.full_Rollback_Button.TabIndex = 46;
            this.full_Rollback_Button.Text = "全量時段回復";
            this.full_Rollback_Button.UseVisualStyleBackColor = true;
            this.full_Rollback_Button.Click += new System.EventHandler(this.full_Rollback_Button_Click);
            // 
            // part_Rollback_Button
            // 
            this.part_Rollback_Button.Location = new System.Drawing.Point(1423, 597);
            this.part_Rollback_Button.Name = "part_Rollback_Button";
            this.part_Rollback_Button.Size = new System.Drawing.Size(165, 49);
            this.part_Rollback_Button.TabIndex = 47;
            this.part_Rollback_Button.Text = "分量時段回復";
            this.part_Rollback_Button.UseVisualStyleBackColor = true;
            this.part_Rollback_Button.Click += new System.EventHandler(this.part_Rollback_Button_Click);
            // 
            // mode_Running_Time_Ok_Button
            // 
            this.mode_Running_Time_Ok_Button.Location = new System.Drawing.Point(1424, 678);
            this.mode_Running_Time_Ok_Button.Name = "mode_Running_Time_Ok_Button";
            this.mode_Running_Time_Ok_Button.Size = new System.Drawing.Size(165, 66);
            this.mode_Running_Time_Ok_Button.TabIndex = 48;
            this.mode_Running_Time_Ok_Button.Text = "確定";
            this.mode_Running_Time_Ok_Button.UseVisualStyleBackColor = true;
            this.mode_Running_Time_Ok_Button.Click += new System.EventHandler(this.mode_Running_Time_Ok_Button_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("新細明體", 11F);
            this.label43.Location = new System.Drawing.Point(16, 116);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(101, 19);
            this.label43.TabIndex = 49;
            this.label43.Text = "01:00~01:30";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("新細明體", 11F);
            this.label44.Location = new System.Drawing.Point(16, 143);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(101, 19);
            this.label44.TabIndex = 57;
            this.label44.Text = "01:30~02:00";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("新細明體", 11F);
            this.label45.Location = new System.Drawing.Point(16, 170);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(101, 19);
            this.label45.TabIndex = 65;
            this.label45.Text = "02:00~02:30";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("新細明體", 11F);
            this.label46.Location = new System.Drawing.Point(16, 251);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(101, 19);
            this.label46.TabIndex = 89;
            this.label46.Text = "03:30~04:00";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("新細明體", 11F);
            this.label47.Location = new System.Drawing.Point(16, 224);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(101, 19);
            this.label47.TabIndex = 81;
            this.label47.Text = "03:00~03:30";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("新細明體", 11F);
            this.label48.Location = new System.Drawing.Point(16, 197);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(101, 19);
            this.label48.TabIndex = 73;
            this.label48.Text = "02:30~03:00";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("新細明體", 11F);
            this.label49.Location = new System.Drawing.Point(16, 305);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(101, 19);
            this.label49.TabIndex = 105;
            this.label49.Text = "04:30~05:00";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("新細明體", 11F);
            this.label50.Location = new System.Drawing.Point(16, 278);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(101, 19);
            this.label50.TabIndex = 97;
            this.label50.Text = "04:00~04:30";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("新細明體", 11F);
            this.label51.Location = new System.Drawing.Point(16, 537);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(101, 19);
            this.label51.TabIndex = 169;
            this.label51.Text = "08:30~09:00";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("新細明體", 11F);
            this.label52.Location = new System.Drawing.Point(16, 510);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(101, 19);
            this.label52.TabIndex = 161;
            this.label52.Text = "08:00~08:30";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("新細明體", 11F);
            this.label53.Location = new System.Drawing.Point(16, 483);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(101, 19);
            this.label53.TabIndex = 153;
            this.label53.Text = "07:30~08:00";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("新細明體", 11F);
            this.label54.Location = new System.Drawing.Point(16, 456);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 19);
            this.label54.TabIndex = 145;
            this.label54.Text = "07:00~07:30";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("新細明體", 11F);
            this.label55.Location = new System.Drawing.Point(16, 429);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(101, 19);
            this.label55.TabIndex = 137;
            this.label55.Text = "06:30~07:00";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("新細明體", 11F);
            this.label56.Location = new System.Drawing.Point(16, 402);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(101, 19);
            this.label56.TabIndex = 129;
            this.label56.Text = "06:00~06:30";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("新細明體", 11F);
            this.label57.Location = new System.Drawing.Point(16, 375);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(101, 19);
            this.label57.TabIndex = 121;
            this.label57.Text = "05:30~06:00";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("新細明體", 11F);
            this.label58.Location = new System.Drawing.Point(16, 348);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(101, 19);
            this.label58.TabIndex = 113;
            this.label58.Text = "05:00~05:30";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("新細明體", 11F);
            this.label59.Location = new System.Drawing.Point(16, 769);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(101, 19);
            this.label59.TabIndex = 233;
            this.label59.Text = "12:30~13:00";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("新細明體", 11F);
            this.label60.Location = new System.Drawing.Point(16, 742);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(101, 19);
            this.label60.TabIndex = 225;
            this.label60.Text = "12:00~12:30";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("新細明體", 11F);
            this.label61.Location = new System.Drawing.Point(16, 715);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(101, 19);
            this.label61.TabIndex = 217;
            this.label61.Text = "11:30~12:00";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("新細明體", 11F);
            this.label62.Location = new System.Drawing.Point(16, 688);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(101, 19);
            this.label62.TabIndex = 209;
            this.label62.Text = "11:00~11:30";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("新細明體", 11F);
            this.label63.Location = new System.Drawing.Point(16, 661);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(101, 19);
            this.label63.TabIndex = 201;
            this.label63.Text = "10:30~11:00";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("新細明體", 11F);
            this.label64.Location = new System.Drawing.Point(16, 634);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(101, 19);
            this.label64.TabIndex = 193;
            this.label64.Text = "10:00~10:30";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("新細明體", 11F);
            this.label65.Location = new System.Drawing.Point(16, 607);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(101, 19);
            this.label65.TabIndex = 185;
            this.label65.Text = "09:30~10:00";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("新細明體", 11F);
            this.label66.Location = new System.Drawing.Point(16, 580);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(101, 19);
            this.label66.TabIndex = 177;
            this.label66.Text = "09:00~09:30";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("新細明體", 11F);
            this.label67.Location = new System.Drawing.Point(717, 769);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(101, 19);
            this.label67.TabIndex = 425;
            this.label67.Text = "00:30~01:00";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("新細明體", 11F);
            this.label68.Location = new System.Drawing.Point(717, 742);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(101, 19);
            this.label68.TabIndex = 417;
            this.label68.Text = "24:00~00:30";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("新細明體", 11F);
            this.label69.Location = new System.Drawing.Point(717, 715);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(101, 19);
            this.label69.TabIndex = 409;
            this.label69.Text = "23:30~24:00";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("新細明體", 11F);
            this.label70.Location = new System.Drawing.Point(717, 688);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(101, 19);
            this.label70.TabIndex = 401;
            this.label70.Text = "23:00~23:30";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("新細明體", 11F);
            this.label71.Location = new System.Drawing.Point(717, 661);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(101, 19);
            this.label71.TabIndex = 393;
            this.label71.Text = "22:30~23:00";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("新細明體", 11F);
            this.label72.Location = new System.Drawing.Point(717, 634);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(101, 19);
            this.label72.TabIndex = 385;
            this.label72.Text = "22:00~22:30";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("新細明體", 11F);
            this.label73.Location = new System.Drawing.Point(717, 607);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(101, 19);
            this.label73.TabIndex = 377;
            this.label73.Text = "21:30~22:00";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("新細明體", 11F);
            this.label74.Location = new System.Drawing.Point(717, 580);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(101, 19);
            this.label74.TabIndex = 369;
            this.label74.Text = "21:00~21:30";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("新細明體", 11F);
            this.label75.Location = new System.Drawing.Point(717, 537);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(101, 19);
            this.label75.TabIndex = 361;
            this.label75.Text = "20:30~21:00";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("新細明體", 11F);
            this.label76.Location = new System.Drawing.Point(717, 510);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(101, 19);
            this.label76.TabIndex = 353;
            this.label76.Text = "20:00~20:30";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("新細明體", 11F);
            this.label77.Location = new System.Drawing.Point(717, 483);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(101, 19);
            this.label77.TabIndex = 345;
            this.label77.Text = "19:30~20:00";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("新細明體", 11F);
            this.label78.Location = new System.Drawing.Point(717, 456);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(101, 19);
            this.label78.TabIndex = 337;
            this.label78.Text = "19:00~19:30";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("新細明體", 11F);
            this.label79.Location = new System.Drawing.Point(717, 429);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(101, 19);
            this.label79.TabIndex = 329;
            this.label79.Text = "18:30~19:00";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("新細明體", 11F);
            this.label80.Location = new System.Drawing.Point(717, 402);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(101, 19);
            this.label80.TabIndex = 321;
            this.label80.Text = "18:00~18:30";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("新細明體", 11F);
            this.label81.Location = new System.Drawing.Point(717, 375);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(101, 19);
            this.label81.TabIndex = 313;
            this.label81.Text = "17:30~18:00";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("新細明體", 11F);
            this.label82.Location = new System.Drawing.Point(717, 348);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(101, 19);
            this.label82.TabIndex = 305;
            this.label82.Text = "17:00~17:30";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("新細明體", 11F);
            this.label83.Location = new System.Drawing.Point(717, 305);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(101, 19);
            this.label83.TabIndex = 297;
            this.label83.Text = "16:30~17:00";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("新細明體", 11F);
            this.label84.Location = new System.Drawing.Point(717, 278);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(101, 19);
            this.label84.TabIndex = 289;
            this.label84.Text = "16:00~16:30";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("新細明體", 11F);
            this.label85.Location = new System.Drawing.Point(717, 251);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(101, 19);
            this.label85.TabIndex = 281;
            this.label85.Text = "15:30~16:00";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("新細明體", 11F);
            this.label86.Location = new System.Drawing.Point(717, 224);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(101, 19);
            this.label86.TabIndex = 273;
            this.label86.Text = "15:00~15:30";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("新細明體", 11F);
            this.label87.Location = new System.Drawing.Point(717, 197);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(101, 19);
            this.label87.TabIndex = 265;
            this.label87.Text = "14:30~15:00";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("新細明體", 11F);
            this.label88.Location = new System.Drawing.Point(717, 170);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(101, 19);
            this.label88.TabIndex = 257;
            this.label88.Text = "14:00~14:30";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("新細明體", 11F);
            this.label89.Location = new System.Drawing.Point(717, 143);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(101, 19);
            this.label89.TabIndex = 249;
            this.label89.Text = "13:30~14:00";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("新細明體", 11F);
            this.label90.Location = new System.Drawing.Point(717, 116);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(101, 19);
            this.label90.TabIndex = 241;
            this.label90.Text = "13:00~13:30";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.full_Air_010_RadioButton);
            this.panel1.Controls.Add(this.full_Ice_010_RadioButton);
            this.panel1.Location = new System.Drawing.Point(164, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(127, 25);
            this.panel1.TabIndex = 433;
            // 
            // full_Air_010_RadioButton
            // 
            this.full_Air_010_RadioButton.AutoCheck = false;
            this.full_Air_010_RadioButton.AutoSize = true;
            this.full_Air_010_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_010_RadioButton.Name = "full_Air_010_RadioButton";
            this.full_Air_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_010_RadioButton.TabIndex = 266;
            this.full_Air_010_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_010_RadioButton
            // 
            this.full_Ice_010_RadioButton.AutoCheck = false;
            this.full_Ice_010_RadioButton.AutoSize = true;
            this.full_Ice_010_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_010_RadioButton.Checked = true;
            this.full_Ice_010_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_010_RadioButton.Name = "full_Ice_010_RadioButton";
            this.full_Ice_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_010_RadioButton.TabIndex = 264;
            this.full_Ice_010_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.full_Air_013_RadioButton);
            this.panel2.Controls.Add(this.full_Ice_013_RadioButton);
            this.panel2.Location = new System.Drawing.Point(164, 143);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 25);
            this.panel2.TabIndex = 434;
            // 
            // full_Air_013_RadioButton
            // 
            this.full_Air_013_RadioButton.AutoCheck = false;
            this.full_Air_013_RadioButton.AutoSize = true;
            this.full_Air_013_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_013_RadioButton.Name = "full_Air_013_RadioButton";
            this.full_Air_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_013_RadioButton.TabIndex = 266;
            this.full_Air_013_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_013_RadioButton
            // 
            this.full_Ice_013_RadioButton.AutoCheck = false;
            this.full_Ice_013_RadioButton.AutoSize = true;
            this.full_Ice_013_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_013_RadioButton.Checked = true;
            this.full_Ice_013_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_013_RadioButton.Name = "full_Ice_013_RadioButton";
            this.full_Ice_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_013_RadioButton.TabIndex = 264;
            this.full_Ice_013_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.full_Air_020_RadioButton);
            this.panel3.Controls.Add(this.full_Ice_020_RadioButton);
            this.panel3.Location = new System.Drawing.Point(164, 170);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(127, 25);
            this.panel3.TabIndex = 435;
            // 
            // full_Air_020_RadioButton
            // 
            this.full_Air_020_RadioButton.AutoCheck = false;
            this.full_Air_020_RadioButton.AutoSize = true;
            this.full_Air_020_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_020_RadioButton.Name = "full_Air_020_RadioButton";
            this.full_Air_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_020_RadioButton.TabIndex = 266;
            this.full_Air_020_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_020_RadioButton
            // 
            this.full_Ice_020_RadioButton.AutoCheck = false;
            this.full_Ice_020_RadioButton.AutoSize = true;
            this.full_Ice_020_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_020_RadioButton.Checked = true;
            this.full_Ice_020_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_020_RadioButton.Name = "full_Ice_020_RadioButton";
            this.full_Ice_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_020_RadioButton.TabIndex = 264;
            this.full_Ice_020_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.full_Air_023_RadioButton);
            this.panel4.Controls.Add(this.full_Ice_023_RadioButton);
            this.panel4.Location = new System.Drawing.Point(164, 197);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(127, 25);
            this.panel4.TabIndex = 436;
            // 
            // full_Air_023_RadioButton
            // 
            this.full_Air_023_RadioButton.AutoCheck = false;
            this.full_Air_023_RadioButton.AutoSize = true;
            this.full_Air_023_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_023_RadioButton.Name = "full_Air_023_RadioButton";
            this.full_Air_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_023_RadioButton.TabIndex = 266;
            this.full_Air_023_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_023_RadioButton
            // 
            this.full_Ice_023_RadioButton.AutoCheck = false;
            this.full_Ice_023_RadioButton.AutoSize = true;
            this.full_Ice_023_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_023_RadioButton.Checked = true;
            this.full_Ice_023_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_023_RadioButton.Name = "full_Ice_023_RadioButton";
            this.full_Ice_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_023_RadioButton.TabIndex = 264;
            this.full_Ice_023_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.full_Air_030_RadioButton);
            this.panel5.Controls.Add(this.full_Ice_030_RadioButton);
            this.panel5.Location = new System.Drawing.Point(164, 224);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(127, 25);
            this.panel5.TabIndex = 437;
            // 
            // full_Air_030_RadioButton
            // 
            this.full_Air_030_RadioButton.AutoCheck = false;
            this.full_Air_030_RadioButton.AutoSize = true;
            this.full_Air_030_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_030_RadioButton.Name = "full_Air_030_RadioButton";
            this.full_Air_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_030_RadioButton.TabIndex = 266;
            this.full_Air_030_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_030_RadioButton
            // 
            this.full_Ice_030_RadioButton.AutoCheck = false;
            this.full_Ice_030_RadioButton.AutoSize = true;
            this.full_Ice_030_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_030_RadioButton.Checked = true;
            this.full_Ice_030_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_030_RadioButton.Name = "full_Ice_030_RadioButton";
            this.full_Ice_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_030_RadioButton.TabIndex = 264;
            this.full_Ice_030_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.full_Air_033_RadioButton);
            this.panel6.Controls.Add(this.full_Ice_033_RadioButton);
            this.panel6.Location = new System.Drawing.Point(164, 251);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(127, 25);
            this.panel6.TabIndex = 438;
            // 
            // full_Air_033_RadioButton
            // 
            this.full_Air_033_RadioButton.AutoCheck = false;
            this.full_Air_033_RadioButton.AutoSize = true;
            this.full_Air_033_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_033_RadioButton.Name = "full_Air_033_RadioButton";
            this.full_Air_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_033_RadioButton.TabIndex = 266;
            this.full_Air_033_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_033_RadioButton
            // 
            this.full_Ice_033_RadioButton.AutoCheck = false;
            this.full_Ice_033_RadioButton.AutoSize = true;
            this.full_Ice_033_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_033_RadioButton.Checked = true;
            this.full_Ice_033_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_033_RadioButton.Name = "full_Ice_033_RadioButton";
            this.full_Ice_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_033_RadioButton.TabIndex = 264;
            this.full_Ice_033_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.full_Air_040_RadioButton);
            this.panel7.Controls.Add(this.full_Ice_040_RadioButton);
            this.panel7.Location = new System.Drawing.Point(164, 278);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(127, 25);
            this.panel7.TabIndex = 439;
            // 
            // full_Air_040_RadioButton
            // 
            this.full_Air_040_RadioButton.AutoCheck = false;
            this.full_Air_040_RadioButton.AutoSize = true;
            this.full_Air_040_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_040_RadioButton.Name = "full_Air_040_RadioButton";
            this.full_Air_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_040_RadioButton.TabIndex = 266;
            this.full_Air_040_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_040_RadioButton
            // 
            this.full_Ice_040_RadioButton.AutoCheck = false;
            this.full_Ice_040_RadioButton.AutoSize = true;
            this.full_Ice_040_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_040_RadioButton.Checked = true;
            this.full_Ice_040_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_040_RadioButton.Name = "full_Ice_040_RadioButton";
            this.full_Ice_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_040_RadioButton.TabIndex = 264;
            this.full_Ice_040_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.full_Air_043_RadioButton);
            this.panel8.Controls.Add(this.full_Ice_043_RadioButton);
            this.panel8.Location = new System.Drawing.Point(164, 305);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(127, 25);
            this.panel8.TabIndex = 440;
            // 
            // full_Air_043_RadioButton
            // 
            this.full_Air_043_RadioButton.AutoCheck = false;
            this.full_Air_043_RadioButton.AutoSize = true;
            this.full_Air_043_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_043_RadioButton.Name = "full_Air_043_RadioButton";
            this.full_Air_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_043_RadioButton.TabIndex = 266;
            this.full_Air_043_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_043_RadioButton
            // 
            this.full_Ice_043_RadioButton.AutoCheck = false;
            this.full_Ice_043_RadioButton.AutoSize = true;
            this.full_Ice_043_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_043_RadioButton.Checked = true;
            this.full_Ice_043_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_043_RadioButton.Name = "full_Ice_043_RadioButton";
            this.full_Ice_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_043_RadioButton.TabIndex = 264;
            this.full_Ice_043_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.full_Air_050_RadioButton);
            this.panel9.Controls.Add(this.full_Ice_050_RadioButton);
            this.panel9.Location = new System.Drawing.Point(164, 348);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(127, 25);
            this.panel9.TabIndex = 441;
            // 
            // full_Air_050_RadioButton
            // 
            this.full_Air_050_RadioButton.AutoCheck = false;
            this.full_Air_050_RadioButton.AutoSize = true;
            this.full_Air_050_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_050_RadioButton.Name = "full_Air_050_RadioButton";
            this.full_Air_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_050_RadioButton.TabIndex = 266;
            this.full_Air_050_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_050_RadioButton
            // 
            this.full_Ice_050_RadioButton.AutoCheck = false;
            this.full_Ice_050_RadioButton.AutoSize = true;
            this.full_Ice_050_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_050_RadioButton.Checked = true;
            this.full_Ice_050_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_050_RadioButton.Name = "full_Ice_050_RadioButton";
            this.full_Ice_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_050_RadioButton.TabIndex = 264;
            this.full_Ice_050_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.full_Air_053_RadioButton);
            this.panel10.Controls.Add(this.full_Ice_053_RadioButton);
            this.panel10.Location = new System.Drawing.Point(164, 375);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(127, 25);
            this.panel10.TabIndex = 442;
            // 
            // full_Air_053_RadioButton
            // 
            this.full_Air_053_RadioButton.AutoCheck = false;
            this.full_Air_053_RadioButton.AutoSize = true;
            this.full_Air_053_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_053_RadioButton.Name = "full_Air_053_RadioButton";
            this.full_Air_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_053_RadioButton.TabIndex = 266;
            this.full_Air_053_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_053_RadioButton
            // 
            this.full_Ice_053_RadioButton.AutoCheck = false;
            this.full_Ice_053_RadioButton.AutoSize = true;
            this.full_Ice_053_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_053_RadioButton.Checked = true;
            this.full_Ice_053_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_053_RadioButton.Name = "full_Ice_053_RadioButton";
            this.full_Ice_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_053_RadioButton.TabIndex = 264;
            this.full_Ice_053_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.full_Air_060_RadioButton);
            this.panel11.Controls.Add(this.full_Ice_060_RadioButton);
            this.panel11.Location = new System.Drawing.Point(164, 402);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(127, 25);
            this.panel11.TabIndex = 443;
            // 
            // full_Air_060_RadioButton
            // 
            this.full_Air_060_RadioButton.AutoCheck = false;
            this.full_Air_060_RadioButton.AutoSize = true;
            this.full_Air_060_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_060_RadioButton.Name = "full_Air_060_RadioButton";
            this.full_Air_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_060_RadioButton.TabIndex = 266;
            this.full_Air_060_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_060_RadioButton
            // 
            this.full_Ice_060_RadioButton.AutoCheck = false;
            this.full_Ice_060_RadioButton.AutoSize = true;
            this.full_Ice_060_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_060_RadioButton.Checked = true;
            this.full_Ice_060_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_060_RadioButton.Name = "full_Ice_060_RadioButton";
            this.full_Ice_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_060_RadioButton.TabIndex = 264;
            this.full_Ice_060_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.full_Air_063_RadioButton);
            this.panel12.Controls.Add(this.full_Ice_063_RadioButton);
            this.panel12.Location = new System.Drawing.Point(164, 429);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(127, 25);
            this.panel12.TabIndex = 444;
            // 
            // full_Air_063_RadioButton
            // 
            this.full_Air_063_RadioButton.AutoCheck = false;
            this.full_Air_063_RadioButton.AutoSize = true;
            this.full_Air_063_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_063_RadioButton.Name = "full_Air_063_RadioButton";
            this.full_Air_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_063_RadioButton.TabIndex = 266;
            this.full_Air_063_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_063_RadioButton
            // 
            this.full_Ice_063_RadioButton.AutoCheck = false;
            this.full_Ice_063_RadioButton.AutoSize = true;
            this.full_Ice_063_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_063_RadioButton.Checked = true;
            this.full_Ice_063_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_063_RadioButton.Name = "full_Ice_063_RadioButton";
            this.full_Ice_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_063_RadioButton.TabIndex = 264;
            this.full_Ice_063_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.full_Air_070_RadioButton);
            this.panel13.Controls.Add(this.full_Ice_070_RadioButton);
            this.panel13.Location = new System.Drawing.Point(164, 456);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(127, 25);
            this.panel13.TabIndex = 445;
            // 
            // full_Air_070_RadioButton
            // 
            this.full_Air_070_RadioButton.AutoCheck = false;
            this.full_Air_070_RadioButton.AutoSize = true;
            this.full_Air_070_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_070_RadioButton.Name = "full_Air_070_RadioButton";
            this.full_Air_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_070_RadioButton.TabIndex = 266;
            this.full_Air_070_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_070_RadioButton
            // 
            this.full_Ice_070_RadioButton.AutoCheck = false;
            this.full_Ice_070_RadioButton.AutoSize = true;
            this.full_Ice_070_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_070_RadioButton.Checked = true;
            this.full_Ice_070_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_070_RadioButton.Name = "full_Ice_070_RadioButton";
            this.full_Ice_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_070_RadioButton.TabIndex = 264;
            this.full_Ice_070_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.full_Air_073_RadioButton);
            this.panel14.Controls.Add(this.full_Ice_073_RadioButton);
            this.panel14.Location = new System.Drawing.Point(164, 483);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(127, 25);
            this.panel14.TabIndex = 446;
            // 
            // full_Air_073_RadioButton
            // 
            this.full_Air_073_RadioButton.AutoCheck = false;
            this.full_Air_073_RadioButton.AutoSize = true;
            this.full_Air_073_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_073_RadioButton.Name = "full_Air_073_RadioButton";
            this.full_Air_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_073_RadioButton.TabIndex = 266;
            this.full_Air_073_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_073_RadioButton
            // 
            this.full_Ice_073_RadioButton.AutoCheck = false;
            this.full_Ice_073_RadioButton.AutoSize = true;
            this.full_Ice_073_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_073_RadioButton.Checked = true;
            this.full_Ice_073_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_073_RadioButton.Name = "full_Ice_073_RadioButton";
            this.full_Ice_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_073_RadioButton.TabIndex = 264;
            this.full_Ice_073_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.full_Air_080_RadioButton);
            this.panel15.Controls.Add(this.full_Ice_080_RadioButton);
            this.panel15.Location = new System.Drawing.Point(164, 510);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(127, 25);
            this.panel15.TabIndex = 447;
            // 
            // full_Air_080_RadioButton
            // 
            this.full_Air_080_RadioButton.AutoCheck = false;
            this.full_Air_080_RadioButton.AutoSize = true;
            this.full_Air_080_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_080_RadioButton.Checked = true;
            this.full_Air_080_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_080_RadioButton.Name = "full_Air_080_RadioButton";
            this.full_Air_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_080_RadioButton.TabIndex = 266;
            this.full_Air_080_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_080_RadioButton
            // 
            this.full_Ice_080_RadioButton.AutoCheck = false;
            this.full_Ice_080_RadioButton.AutoSize = true;
            this.full_Ice_080_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_080_RadioButton.Name = "full_Ice_080_RadioButton";
            this.full_Ice_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_080_RadioButton.TabIndex = 264;
            this.full_Ice_080_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.full_Air_083_RadioButton);
            this.panel16.Controls.Add(this.full_Ice_083_RadioButton);
            this.panel16.Location = new System.Drawing.Point(164, 537);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(127, 25);
            this.panel16.TabIndex = 448;
            // 
            // full_Air_083_RadioButton
            // 
            this.full_Air_083_RadioButton.AutoCheck = false;
            this.full_Air_083_RadioButton.AutoSize = true;
            this.full_Air_083_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_083_RadioButton.Checked = true;
            this.full_Air_083_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_083_RadioButton.Name = "full_Air_083_RadioButton";
            this.full_Air_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_083_RadioButton.TabIndex = 266;
            this.full_Air_083_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_083_RadioButton
            // 
            this.full_Ice_083_RadioButton.AutoCheck = false;
            this.full_Ice_083_RadioButton.AutoSize = true;
            this.full_Ice_083_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_083_RadioButton.Name = "full_Ice_083_RadioButton";
            this.full_Ice_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_083_RadioButton.TabIndex = 264;
            this.full_Ice_083_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.full_Air_090_RadioButton);
            this.panel17.Controls.Add(this.full_Ice_090_RadioButton);
            this.panel17.Location = new System.Drawing.Point(164, 580);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(127, 25);
            this.panel17.TabIndex = 449;
            // 
            // full_Air_090_RadioButton
            // 
            this.full_Air_090_RadioButton.AutoCheck = false;
            this.full_Air_090_RadioButton.AutoSize = true;
            this.full_Air_090_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_090_RadioButton.Checked = true;
            this.full_Air_090_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_090_RadioButton.Name = "full_Air_090_RadioButton";
            this.full_Air_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_090_RadioButton.TabIndex = 266;
            this.full_Air_090_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_090_RadioButton
            // 
            this.full_Ice_090_RadioButton.AutoCheck = false;
            this.full_Ice_090_RadioButton.AutoSize = true;
            this.full_Ice_090_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_090_RadioButton.Name = "full_Ice_090_RadioButton";
            this.full_Ice_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_090_RadioButton.TabIndex = 264;
            this.full_Ice_090_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.full_Air_093_RadioButton);
            this.panel18.Controls.Add(this.full_Ice_093_RadioButton);
            this.panel18.Location = new System.Drawing.Point(164, 607);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(127, 25);
            this.panel18.TabIndex = 450;
            // 
            // full_Air_093_RadioButton
            // 
            this.full_Air_093_RadioButton.AutoCheck = false;
            this.full_Air_093_RadioButton.AutoSize = true;
            this.full_Air_093_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_093_RadioButton.Checked = true;
            this.full_Air_093_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_093_RadioButton.Name = "full_Air_093_RadioButton";
            this.full_Air_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_093_RadioButton.TabIndex = 266;
            this.full_Air_093_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_093_RadioButton
            // 
            this.full_Ice_093_RadioButton.AutoCheck = false;
            this.full_Ice_093_RadioButton.AutoSize = true;
            this.full_Ice_093_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_093_RadioButton.Name = "full_Ice_093_RadioButton";
            this.full_Ice_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_093_RadioButton.TabIndex = 264;
            this.full_Ice_093_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.full_Air_100_RadioButton);
            this.panel19.Controls.Add(this.full_Ice_100_RadioButton);
            this.panel19.Location = new System.Drawing.Point(164, 634);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(127, 25);
            this.panel19.TabIndex = 451;
            // 
            // full_Air_100_RadioButton
            // 
            this.full_Air_100_RadioButton.AutoCheck = false;
            this.full_Air_100_RadioButton.AutoSize = true;
            this.full_Air_100_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_100_RadioButton.Checked = true;
            this.full_Air_100_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_100_RadioButton.Name = "full_Air_100_RadioButton";
            this.full_Air_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_100_RadioButton.TabIndex = 266;
            this.full_Air_100_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_100_RadioButton
            // 
            this.full_Ice_100_RadioButton.AutoCheck = false;
            this.full_Ice_100_RadioButton.AutoSize = true;
            this.full_Ice_100_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_100_RadioButton.Name = "full_Ice_100_RadioButton";
            this.full_Ice_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_100_RadioButton.TabIndex = 264;
            this.full_Ice_100_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.full_Air_103_RadioButton);
            this.panel20.Controls.Add(this.full_Ice_103_RadioButton);
            this.panel20.Location = new System.Drawing.Point(164, 661);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(127, 25);
            this.panel20.TabIndex = 452;
            // 
            // full_Air_103_RadioButton
            // 
            this.full_Air_103_RadioButton.AutoCheck = false;
            this.full_Air_103_RadioButton.AutoSize = true;
            this.full_Air_103_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_103_RadioButton.Checked = true;
            this.full_Air_103_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_103_RadioButton.Name = "full_Air_103_RadioButton";
            this.full_Air_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_103_RadioButton.TabIndex = 266;
            this.full_Air_103_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_103_RadioButton
            // 
            this.full_Ice_103_RadioButton.AutoCheck = false;
            this.full_Ice_103_RadioButton.AutoSize = true;
            this.full_Ice_103_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_103_RadioButton.Name = "full_Ice_103_RadioButton";
            this.full_Ice_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_103_RadioButton.TabIndex = 264;
            this.full_Ice_103_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.full_Air_110_RadioButton);
            this.panel21.Controls.Add(this.full_Ice_110_RadioButton);
            this.panel21.Location = new System.Drawing.Point(164, 688);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(127, 25);
            this.panel21.TabIndex = 453;
            // 
            // full_Air_110_RadioButton
            // 
            this.full_Air_110_RadioButton.AutoCheck = false;
            this.full_Air_110_RadioButton.AutoSize = true;
            this.full_Air_110_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_110_RadioButton.Checked = true;
            this.full_Air_110_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_110_RadioButton.Name = "full_Air_110_RadioButton";
            this.full_Air_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_110_RadioButton.TabIndex = 266;
            this.full_Air_110_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_110_RadioButton
            // 
            this.full_Ice_110_RadioButton.AutoCheck = false;
            this.full_Ice_110_RadioButton.AutoSize = true;
            this.full_Ice_110_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_110_RadioButton.Name = "full_Ice_110_RadioButton";
            this.full_Ice_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_110_RadioButton.TabIndex = 264;
            this.full_Ice_110_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.full_Air_113_RadioButton);
            this.panel22.Controls.Add(this.full_Ice_113_RadioButton);
            this.panel22.Location = new System.Drawing.Point(164, 715);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(127, 25);
            this.panel22.TabIndex = 454;
            // 
            // full_Air_113_RadioButton
            // 
            this.full_Air_113_RadioButton.AutoCheck = false;
            this.full_Air_113_RadioButton.AutoSize = true;
            this.full_Air_113_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_113_RadioButton.Checked = true;
            this.full_Air_113_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_113_RadioButton.Name = "full_Air_113_RadioButton";
            this.full_Air_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_113_RadioButton.TabIndex = 266;
            this.full_Air_113_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_113_RadioButton
            // 
            this.full_Ice_113_RadioButton.AutoCheck = false;
            this.full_Ice_113_RadioButton.AutoSize = true;
            this.full_Ice_113_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_113_RadioButton.Name = "full_Ice_113_RadioButton";
            this.full_Ice_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_113_RadioButton.TabIndex = 264;
            this.full_Ice_113_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.full_Air_120_RadioButton);
            this.panel23.Controls.Add(this.full_Ice_120_RadioButton);
            this.panel23.Location = new System.Drawing.Point(164, 742);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(127, 25);
            this.panel23.TabIndex = 455;
            // 
            // full_Air_120_RadioButton
            // 
            this.full_Air_120_RadioButton.AutoCheck = false;
            this.full_Air_120_RadioButton.AutoSize = true;
            this.full_Air_120_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_120_RadioButton.Checked = true;
            this.full_Air_120_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_120_RadioButton.Name = "full_Air_120_RadioButton";
            this.full_Air_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_120_RadioButton.TabIndex = 266;
            this.full_Air_120_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_120_RadioButton
            // 
            this.full_Ice_120_RadioButton.AutoCheck = false;
            this.full_Ice_120_RadioButton.AutoSize = true;
            this.full_Ice_120_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_120_RadioButton.Name = "full_Ice_120_RadioButton";
            this.full_Ice_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_120_RadioButton.TabIndex = 264;
            this.full_Ice_120_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.full_Air_123_RadioButton);
            this.panel24.Controls.Add(this.full_Ice_123_RadioButton);
            this.panel24.Location = new System.Drawing.Point(164, 769);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(127, 25);
            this.panel24.TabIndex = 456;
            // 
            // full_Air_123_RadioButton
            // 
            this.full_Air_123_RadioButton.AutoCheck = false;
            this.full_Air_123_RadioButton.AutoSize = true;
            this.full_Air_123_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_123_RadioButton.Checked = true;
            this.full_Air_123_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_123_RadioButton.Name = "full_Air_123_RadioButton";
            this.full_Air_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_123_RadioButton.TabIndex = 266;
            this.full_Air_123_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_123_RadioButton
            // 
            this.full_Ice_123_RadioButton.AutoCheck = false;
            this.full_Ice_123_RadioButton.AutoSize = true;
            this.full_Ice_123_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_123_RadioButton.Name = "full_Ice_123_RadioButton";
            this.full_Ice_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_123_RadioButton.TabIndex = 264;
            this.full_Ice_123_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.part_Air_010_RadioButton);
            this.panel25.Controls.Add(this.part_Ice_010_RadioButton);
            this.panel25.Location = new System.Drawing.Point(318, 116);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(127, 25);
            this.panel25.TabIndex = 457;
            // 
            // part_Air_010_RadioButton
            // 
            this.part_Air_010_RadioButton.AutoCheck = false;
            this.part_Air_010_RadioButton.AutoSize = true;
            this.part_Air_010_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_010_RadioButton.Name = "part_Air_010_RadioButton";
            this.part_Air_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_010_RadioButton.TabIndex = 266;
            this.part_Air_010_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_010_RadioButton
            // 
            this.part_Ice_010_RadioButton.AutoCheck = false;
            this.part_Ice_010_RadioButton.AutoSize = true;
            this.part_Ice_010_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_010_RadioButton.Checked = true;
            this.part_Ice_010_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_010_RadioButton.Name = "part_Ice_010_RadioButton";
            this.part_Ice_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_010_RadioButton.TabIndex = 264;
            this.part_Ice_010_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.part_Air_013_RadioButton);
            this.panel26.Controls.Add(this.part_Ice_013_RadioButton);
            this.panel26.Location = new System.Drawing.Point(318, 143);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(127, 25);
            this.panel26.TabIndex = 458;
            // 
            // part_Air_013_RadioButton
            // 
            this.part_Air_013_RadioButton.AutoCheck = false;
            this.part_Air_013_RadioButton.AutoSize = true;
            this.part_Air_013_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_013_RadioButton.Name = "part_Air_013_RadioButton";
            this.part_Air_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_013_RadioButton.TabIndex = 266;
            this.part_Air_013_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_013_RadioButton
            // 
            this.part_Ice_013_RadioButton.AutoCheck = false;
            this.part_Ice_013_RadioButton.AutoSize = true;
            this.part_Ice_013_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_013_RadioButton.Checked = true;
            this.part_Ice_013_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_013_RadioButton.Name = "part_Ice_013_RadioButton";
            this.part_Ice_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_013_RadioButton.TabIndex = 264;
            this.part_Ice_013_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.part_Air_020_RadioButton);
            this.panel27.Controls.Add(this.part_Ice_020_RadioButton);
            this.panel27.Location = new System.Drawing.Point(318, 170);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(127, 25);
            this.panel27.TabIndex = 459;
            // 
            // part_Air_020_RadioButton
            // 
            this.part_Air_020_RadioButton.AutoCheck = false;
            this.part_Air_020_RadioButton.AutoSize = true;
            this.part_Air_020_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_020_RadioButton.Name = "part_Air_020_RadioButton";
            this.part_Air_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_020_RadioButton.TabIndex = 266;
            this.part_Air_020_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_020_RadioButton
            // 
            this.part_Ice_020_RadioButton.AutoCheck = false;
            this.part_Ice_020_RadioButton.AutoSize = true;
            this.part_Ice_020_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_020_RadioButton.Checked = true;
            this.part_Ice_020_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_020_RadioButton.Name = "part_Ice_020_RadioButton";
            this.part_Ice_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_020_RadioButton.TabIndex = 264;
            this.part_Ice_020_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.part_Air_023_RadioButton);
            this.panel28.Controls.Add(this.part_Ice_023_RadioButton);
            this.panel28.Location = new System.Drawing.Point(318, 197);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(127, 25);
            this.panel28.TabIndex = 460;
            // 
            // part_Air_023_RadioButton
            // 
            this.part_Air_023_RadioButton.AutoCheck = false;
            this.part_Air_023_RadioButton.AutoSize = true;
            this.part_Air_023_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_023_RadioButton.Name = "part_Air_023_RadioButton";
            this.part_Air_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_023_RadioButton.TabIndex = 266;
            this.part_Air_023_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_023_RadioButton
            // 
            this.part_Ice_023_RadioButton.AutoCheck = false;
            this.part_Ice_023_RadioButton.AutoSize = true;
            this.part_Ice_023_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_023_RadioButton.Checked = true;
            this.part_Ice_023_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_023_RadioButton.Name = "part_Ice_023_RadioButton";
            this.part_Ice_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_023_RadioButton.TabIndex = 264;
            this.part_Ice_023_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.part_Air_030_RadioButton);
            this.panel29.Controls.Add(this.part_Ice_030_RadioButton);
            this.panel29.Location = new System.Drawing.Point(318, 224);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(127, 25);
            this.panel29.TabIndex = 461;
            // 
            // part_Air_030_RadioButton
            // 
            this.part_Air_030_RadioButton.AutoCheck = false;
            this.part_Air_030_RadioButton.AutoSize = true;
            this.part_Air_030_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_030_RadioButton.Name = "part_Air_030_RadioButton";
            this.part_Air_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_030_RadioButton.TabIndex = 266;
            this.part_Air_030_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_030_RadioButton
            // 
            this.part_Ice_030_RadioButton.AutoCheck = false;
            this.part_Ice_030_RadioButton.AutoSize = true;
            this.part_Ice_030_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_030_RadioButton.Checked = true;
            this.part_Ice_030_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_030_RadioButton.Name = "part_Ice_030_RadioButton";
            this.part_Ice_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_030_RadioButton.TabIndex = 264;
            this.part_Ice_030_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.part_Air_033_RadioButton);
            this.panel30.Controls.Add(this.part_Ice_033_RadioButton);
            this.panel30.Location = new System.Drawing.Point(318, 251);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(127, 25);
            this.panel30.TabIndex = 462;
            // 
            // part_Air_033_RadioButton
            // 
            this.part_Air_033_RadioButton.AutoCheck = false;
            this.part_Air_033_RadioButton.AutoSize = true;
            this.part_Air_033_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_033_RadioButton.Name = "part_Air_033_RadioButton";
            this.part_Air_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_033_RadioButton.TabIndex = 266;
            this.part_Air_033_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_033_RadioButton
            // 
            this.part_Ice_033_RadioButton.AutoCheck = false;
            this.part_Ice_033_RadioButton.AutoSize = true;
            this.part_Ice_033_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_033_RadioButton.Checked = true;
            this.part_Ice_033_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_033_RadioButton.Name = "part_Ice_033_RadioButton";
            this.part_Ice_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_033_RadioButton.TabIndex = 264;
            this.part_Ice_033_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.part_Air_040_RadioButton);
            this.panel31.Controls.Add(this.part_Ice_040_RadioButton);
            this.panel31.Location = new System.Drawing.Point(318, 278);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(127, 25);
            this.panel31.TabIndex = 463;
            // 
            // part_Air_040_RadioButton
            // 
            this.part_Air_040_RadioButton.AutoCheck = false;
            this.part_Air_040_RadioButton.AutoSize = true;
            this.part_Air_040_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_040_RadioButton.Name = "part_Air_040_RadioButton";
            this.part_Air_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_040_RadioButton.TabIndex = 266;
            this.part_Air_040_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_040_RadioButton
            // 
            this.part_Ice_040_RadioButton.AutoCheck = false;
            this.part_Ice_040_RadioButton.AutoSize = true;
            this.part_Ice_040_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_040_RadioButton.Checked = true;
            this.part_Ice_040_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_040_RadioButton.Name = "part_Ice_040_RadioButton";
            this.part_Ice_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_040_RadioButton.TabIndex = 264;
            this.part_Ice_040_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.part_Air_043_RadioButton);
            this.panel32.Controls.Add(this.part_Ice_043_RadioButton);
            this.panel32.Location = new System.Drawing.Point(318, 305);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(127, 25);
            this.panel32.TabIndex = 464;
            // 
            // part_Air_043_RadioButton
            // 
            this.part_Air_043_RadioButton.AutoCheck = false;
            this.part_Air_043_RadioButton.AutoSize = true;
            this.part_Air_043_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_043_RadioButton.Name = "part_Air_043_RadioButton";
            this.part_Air_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_043_RadioButton.TabIndex = 266;
            this.part_Air_043_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_043_RadioButton
            // 
            this.part_Ice_043_RadioButton.AutoCheck = false;
            this.part_Ice_043_RadioButton.AutoSize = true;
            this.part_Ice_043_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_043_RadioButton.Checked = true;
            this.part_Ice_043_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_043_RadioButton.Name = "part_Ice_043_RadioButton";
            this.part_Ice_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_043_RadioButton.TabIndex = 264;
            this.part_Ice_043_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.part_Air_050_RadioButton);
            this.panel33.Controls.Add(this.part_Ice_050_RadioButton);
            this.panel33.Location = new System.Drawing.Point(318, 348);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(127, 25);
            this.panel33.TabIndex = 465;
            // 
            // part_Air_050_RadioButton
            // 
            this.part_Air_050_RadioButton.AutoCheck = false;
            this.part_Air_050_RadioButton.AutoSize = true;
            this.part_Air_050_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_050_RadioButton.Name = "part_Air_050_RadioButton";
            this.part_Air_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_050_RadioButton.TabIndex = 266;
            this.part_Air_050_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_050_RadioButton
            // 
            this.part_Ice_050_RadioButton.AutoCheck = false;
            this.part_Ice_050_RadioButton.AutoSize = true;
            this.part_Ice_050_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_050_RadioButton.Checked = true;
            this.part_Ice_050_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_050_RadioButton.Name = "part_Ice_050_RadioButton";
            this.part_Ice_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_050_RadioButton.TabIndex = 264;
            this.part_Ice_050_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.part_Air_053_RadioButton);
            this.panel34.Controls.Add(this.part_Ice_053_RadioButton);
            this.panel34.Location = new System.Drawing.Point(318, 375);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(127, 25);
            this.panel34.TabIndex = 466;
            // 
            // part_Air_053_RadioButton
            // 
            this.part_Air_053_RadioButton.AutoCheck = false;
            this.part_Air_053_RadioButton.AutoSize = true;
            this.part_Air_053_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_053_RadioButton.Name = "part_Air_053_RadioButton";
            this.part_Air_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_053_RadioButton.TabIndex = 266;
            this.part_Air_053_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_053_RadioButton
            // 
            this.part_Ice_053_RadioButton.AutoCheck = false;
            this.part_Ice_053_RadioButton.AutoSize = true;
            this.part_Ice_053_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_053_RadioButton.Checked = true;
            this.part_Ice_053_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_053_RadioButton.Name = "part_Ice_053_RadioButton";
            this.part_Ice_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_053_RadioButton.TabIndex = 264;
            this.part_Ice_053_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.part_Air_060_RadioButton);
            this.panel35.Controls.Add(this.part_Ice_060_RadioButton);
            this.panel35.Location = new System.Drawing.Point(318, 402);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(127, 25);
            this.panel35.TabIndex = 467;
            // 
            // part_Air_060_RadioButton
            // 
            this.part_Air_060_RadioButton.AutoCheck = false;
            this.part_Air_060_RadioButton.AutoSize = true;
            this.part_Air_060_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_060_RadioButton.Name = "part_Air_060_RadioButton";
            this.part_Air_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_060_RadioButton.TabIndex = 266;
            this.part_Air_060_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_060_RadioButton
            // 
            this.part_Ice_060_RadioButton.AutoCheck = false;
            this.part_Ice_060_RadioButton.AutoSize = true;
            this.part_Ice_060_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_060_RadioButton.Checked = true;
            this.part_Ice_060_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_060_RadioButton.Name = "part_Ice_060_RadioButton";
            this.part_Ice_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_060_RadioButton.TabIndex = 264;
            this.part_Ice_060_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.part_Air_063_RadioButton);
            this.panel36.Controls.Add(this.part_Ice_063_RadioButton);
            this.panel36.Location = new System.Drawing.Point(318, 429);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(127, 25);
            this.panel36.TabIndex = 468;
            // 
            // part_Air_063_RadioButton
            // 
            this.part_Air_063_RadioButton.AutoCheck = false;
            this.part_Air_063_RadioButton.AutoSize = true;
            this.part_Air_063_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_063_RadioButton.Name = "part_Air_063_RadioButton";
            this.part_Air_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_063_RadioButton.TabIndex = 266;
            this.part_Air_063_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_063_RadioButton
            // 
            this.part_Ice_063_RadioButton.AutoCheck = false;
            this.part_Ice_063_RadioButton.AutoSize = true;
            this.part_Ice_063_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_063_RadioButton.Checked = true;
            this.part_Ice_063_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_063_RadioButton.Name = "part_Ice_063_RadioButton";
            this.part_Ice_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_063_RadioButton.TabIndex = 264;
            this.part_Ice_063_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.part_Air_070_RadioButton);
            this.panel37.Controls.Add(this.part_Ice_070_RadioButton);
            this.panel37.Location = new System.Drawing.Point(318, 456);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(127, 25);
            this.panel37.TabIndex = 469;
            // 
            // part_Air_070_RadioButton
            // 
            this.part_Air_070_RadioButton.AutoCheck = false;
            this.part_Air_070_RadioButton.AutoSize = true;
            this.part_Air_070_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_070_RadioButton.Name = "part_Air_070_RadioButton";
            this.part_Air_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_070_RadioButton.TabIndex = 266;
            this.part_Air_070_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_070_RadioButton
            // 
            this.part_Ice_070_RadioButton.AutoCheck = false;
            this.part_Ice_070_RadioButton.AutoSize = true;
            this.part_Ice_070_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_070_RadioButton.Checked = true;
            this.part_Ice_070_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_070_RadioButton.Name = "part_Ice_070_RadioButton";
            this.part_Ice_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_070_RadioButton.TabIndex = 264;
            this.part_Ice_070_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.part_Air_073_RadioButton);
            this.panel38.Controls.Add(this.part_Ice_073_RadioButton);
            this.panel38.Location = new System.Drawing.Point(318, 483);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(127, 25);
            this.panel38.TabIndex = 470;
            // 
            // part_Air_073_RadioButton
            // 
            this.part_Air_073_RadioButton.AutoCheck = false;
            this.part_Air_073_RadioButton.AutoSize = true;
            this.part_Air_073_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_073_RadioButton.Name = "part_Air_073_RadioButton";
            this.part_Air_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_073_RadioButton.TabIndex = 266;
            this.part_Air_073_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_073_RadioButton
            // 
            this.part_Ice_073_RadioButton.AutoCheck = false;
            this.part_Ice_073_RadioButton.AutoSize = true;
            this.part_Ice_073_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_073_RadioButton.Checked = true;
            this.part_Ice_073_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_073_RadioButton.Name = "part_Ice_073_RadioButton";
            this.part_Ice_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_073_RadioButton.TabIndex = 264;
            this.part_Ice_073_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.part_Air_080_RadioButton);
            this.panel39.Controls.Add(this.part_Ice_080_RadioButton);
            this.panel39.Location = new System.Drawing.Point(318, 510);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(127, 25);
            this.panel39.TabIndex = 471;
            // 
            // part_Air_080_RadioButton
            // 
            this.part_Air_080_RadioButton.AutoCheck = false;
            this.part_Air_080_RadioButton.AutoSize = true;
            this.part_Air_080_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_080_RadioButton.Checked = true;
            this.part_Air_080_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_080_RadioButton.Name = "part_Air_080_RadioButton";
            this.part_Air_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_080_RadioButton.TabIndex = 266;
            this.part_Air_080_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_080_RadioButton
            // 
            this.part_Ice_080_RadioButton.AutoCheck = false;
            this.part_Ice_080_RadioButton.AutoSize = true;
            this.part_Ice_080_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_080_RadioButton.Name = "part_Ice_080_RadioButton";
            this.part_Ice_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_080_RadioButton.TabIndex = 264;
            this.part_Ice_080_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.part_Air_083_RadioButton);
            this.panel40.Controls.Add(this.part_Ice_083_RadioButton);
            this.panel40.Location = new System.Drawing.Point(318, 537);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(127, 25);
            this.panel40.TabIndex = 472;
            // 
            // part_Air_083_RadioButton
            // 
            this.part_Air_083_RadioButton.AutoCheck = false;
            this.part_Air_083_RadioButton.AutoSize = true;
            this.part_Air_083_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_083_RadioButton.Checked = true;
            this.part_Air_083_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_083_RadioButton.Name = "part_Air_083_RadioButton";
            this.part_Air_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_083_RadioButton.TabIndex = 266;
            this.part_Air_083_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_083_RadioButton
            // 
            this.part_Ice_083_RadioButton.AutoCheck = false;
            this.part_Ice_083_RadioButton.AutoSize = true;
            this.part_Ice_083_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_083_RadioButton.Name = "part_Ice_083_RadioButton";
            this.part_Ice_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_083_RadioButton.TabIndex = 264;
            this.part_Ice_083_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.part_Air_090_RadioButton);
            this.panel41.Controls.Add(this.part_Ice_090_RadioButton);
            this.panel41.Location = new System.Drawing.Point(318, 580);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(127, 25);
            this.panel41.TabIndex = 473;
            // 
            // part_Air_090_RadioButton
            // 
            this.part_Air_090_RadioButton.AutoCheck = false;
            this.part_Air_090_RadioButton.AutoSize = true;
            this.part_Air_090_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_090_RadioButton.Checked = true;
            this.part_Air_090_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_090_RadioButton.Name = "part_Air_090_RadioButton";
            this.part_Air_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_090_RadioButton.TabIndex = 266;
            this.part_Air_090_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_090_RadioButton
            // 
            this.part_Ice_090_RadioButton.AutoCheck = false;
            this.part_Ice_090_RadioButton.AutoSize = true;
            this.part_Ice_090_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_090_RadioButton.Name = "part_Ice_090_RadioButton";
            this.part_Ice_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_090_RadioButton.TabIndex = 264;
            this.part_Ice_090_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.part_Air_093_RadioButton);
            this.panel42.Controls.Add(this.part_Ice_093_RadioButton);
            this.panel42.Location = new System.Drawing.Point(318, 607);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(127, 25);
            this.panel42.TabIndex = 474;
            // 
            // part_Air_093_RadioButton
            // 
            this.part_Air_093_RadioButton.AutoCheck = false;
            this.part_Air_093_RadioButton.AutoSize = true;
            this.part_Air_093_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_093_RadioButton.Checked = true;
            this.part_Air_093_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_093_RadioButton.Name = "part_Air_093_RadioButton";
            this.part_Air_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_093_RadioButton.TabIndex = 266;
            this.part_Air_093_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_093_RadioButton
            // 
            this.part_Ice_093_RadioButton.AutoCheck = false;
            this.part_Ice_093_RadioButton.AutoSize = true;
            this.part_Ice_093_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_093_RadioButton.Name = "part_Ice_093_RadioButton";
            this.part_Ice_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_093_RadioButton.TabIndex = 264;
            this.part_Ice_093_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.part_Air_100_RadioButton);
            this.panel43.Controls.Add(this.part_Ice_100_RadioButton);
            this.panel43.Location = new System.Drawing.Point(318, 634);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(127, 25);
            this.panel43.TabIndex = 475;
            // 
            // part_Air_100_RadioButton
            // 
            this.part_Air_100_RadioButton.AutoCheck = false;
            this.part_Air_100_RadioButton.AutoSize = true;
            this.part_Air_100_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_100_RadioButton.Checked = true;
            this.part_Air_100_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_100_RadioButton.Name = "part_Air_100_RadioButton";
            this.part_Air_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_100_RadioButton.TabIndex = 266;
            this.part_Air_100_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_100_RadioButton
            // 
            this.part_Ice_100_RadioButton.AutoCheck = false;
            this.part_Ice_100_RadioButton.AutoSize = true;
            this.part_Ice_100_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_100_RadioButton.Name = "part_Ice_100_RadioButton";
            this.part_Ice_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_100_RadioButton.TabIndex = 264;
            this.part_Ice_100_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.part_Air_103_RadioButton);
            this.panel44.Controls.Add(this.part_Ice_103_RadioButton);
            this.panel44.Location = new System.Drawing.Point(318, 661);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(127, 25);
            this.panel44.TabIndex = 476;
            // 
            // part_Air_103_RadioButton
            // 
            this.part_Air_103_RadioButton.AutoCheck = false;
            this.part_Air_103_RadioButton.AutoSize = true;
            this.part_Air_103_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_103_RadioButton.Checked = true;
            this.part_Air_103_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_103_RadioButton.Name = "part_Air_103_RadioButton";
            this.part_Air_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_103_RadioButton.TabIndex = 266;
            this.part_Air_103_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_103_RadioButton
            // 
            this.part_Ice_103_RadioButton.AutoCheck = false;
            this.part_Ice_103_RadioButton.AutoSize = true;
            this.part_Ice_103_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_103_RadioButton.Name = "part_Ice_103_RadioButton";
            this.part_Ice_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_103_RadioButton.TabIndex = 264;
            this.part_Ice_103_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.part_Air_110_RadioButton);
            this.panel45.Controls.Add(this.part_Ice_110_RadioButton);
            this.panel45.Location = new System.Drawing.Point(318, 688);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(127, 25);
            this.panel45.TabIndex = 477;
            // 
            // part_Air_110_RadioButton
            // 
            this.part_Air_110_RadioButton.AutoCheck = false;
            this.part_Air_110_RadioButton.AutoSize = true;
            this.part_Air_110_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_110_RadioButton.Checked = true;
            this.part_Air_110_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_110_RadioButton.Name = "part_Air_110_RadioButton";
            this.part_Air_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_110_RadioButton.TabIndex = 266;
            this.part_Air_110_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_110_RadioButton
            // 
            this.part_Ice_110_RadioButton.AutoCheck = false;
            this.part_Ice_110_RadioButton.AutoSize = true;
            this.part_Ice_110_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_110_RadioButton.Name = "part_Ice_110_RadioButton";
            this.part_Ice_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_110_RadioButton.TabIndex = 264;
            this.part_Ice_110_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.part_Air_113_RadioButton);
            this.panel46.Controls.Add(this.part_Ice_113_RadioButton);
            this.panel46.Location = new System.Drawing.Point(318, 715);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(127, 25);
            this.panel46.TabIndex = 478;
            // 
            // part_Air_113_RadioButton
            // 
            this.part_Air_113_RadioButton.AutoCheck = false;
            this.part_Air_113_RadioButton.AutoSize = true;
            this.part_Air_113_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_113_RadioButton.Checked = true;
            this.part_Air_113_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_113_RadioButton.Name = "part_Air_113_RadioButton";
            this.part_Air_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_113_RadioButton.TabIndex = 266;
            this.part_Air_113_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_113_RadioButton
            // 
            this.part_Ice_113_RadioButton.AutoCheck = false;
            this.part_Ice_113_RadioButton.AutoSize = true;
            this.part_Ice_113_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_113_RadioButton.Name = "part_Ice_113_RadioButton";
            this.part_Ice_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_113_RadioButton.TabIndex = 264;
            this.part_Ice_113_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.part_Air_120_RadioButton);
            this.panel47.Controls.Add(this.part_Ice_120_RadioButton);
            this.panel47.Location = new System.Drawing.Point(318, 742);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(127, 25);
            this.panel47.TabIndex = 479;
            // 
            // part_Air_120_RadioButton
            // 
            this.part_Air_120_RadioButton.AutoCheck = false;
            this.part_Air_120_RadioButton.AutoSize = true;
            this.part_Air_120_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_120_RadioButton.Checked = true;
            this.part_Air_120_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_120_RadioButton.Name = "part_Air_120_RadioButton";
            this.part_Air_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_120_RadioButton.TabIndex = 266;
            this.part_Air_120_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_120_RadioButton
            // 
            this.part_Ice_120_RadioButton.AutoCheck = false;
            this.part_Ice_120_RadioButton.AutoSize = true;
            this.part_Ice_120_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_120_RadioButton.Name = "part_Ice_120_RadioButton";
            this.part_Ice_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_120_RadioButton.TabIndex = 264;
            this.part_Ice_120_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.part_Air_123_RadioButton);
            this.panel48.Controls.Add(this.part_Ice_123_RadioButton);
            this.panel48.Location = new System.Drawing.Point(318, 769);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(127, 25);
            this.panel48.TabIndex = 480;
            // 
            // part_Air_123_RadioButton
            // 
            this.part_Air_123_RadioButton.AutoCheck = false;
            this.part_Air_123_RadioButton.AutoSize = true;
            this.part_Air_123_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_123_RadioButton.Checked = true;
            this.part_Air_123_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_123_RadioButton.Name = "part_Air_123_RadioButton";
            this.part_Air_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_123_RadioButton.TabIndex = 266;
            this.part_Air_123_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_123_RadioButton
            // 
            this.part_Ice_123_RadioButton.AutoCheck = false;
            this.part_Ice_123_RadioButton.AutoSize = true;
            this.part_Ice_123_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_123_RadioButton.Name = "part_Ice_123_RadioButton";
            this.part_Ice_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_123_RadioButton.TabIndex = 264;
            this.part_Ice_123_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.trough_010_RadioButton);
            this.panel49.Controls.Add(this.flat_010_RadioButton);
            this.panel49.Controls.Add(this.crest_010_RadioButton);
            this.panel49.Location = new System.Drawing.Point(488, 116);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(188, 25);
            this.panel49.TabIndex = 481;
            // 
            // trough_010_RadioButton
            // 
            this.trough_010_RadioButton.AutoCheck = false;
            this.trough_010_RadioButton.AutoSize = true;
            this.trough_010_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_010_RadioButton.Checked = true;
            this.trough_010_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_010_RadioButton.Name = "trough_010_RadioButton";
            this.trough_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_010_RadioButton.TabIndex = 482;
            this.trough_010_RadioButton.UseVisualStyleBackColor = false;
            this.trough_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_010_RadioButton
            // 
            this.flat_010_RadioButton.AutoCheck = false;
            this.flat_010_RadioButton.AutoSize = true;
            this.flat_010_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_010_RadioButton.Name = "flat_010_RadioButton";
            this.flat_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_010_RadioButton.TabIndex = 266;
            this.flat_010_RadioButton.UseVisualStyleBackColor = false;
            this.flat_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_010_RadioButton
            // 
            this.crest_010_RadioButton.AutoCheck = false;
            this.crest_010_RadioButton.AutoSize = true;
            this.crest_010_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_010_RadioButton.Name = "crest_010_RadioButton";
            this.crest_010_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_010_RadioButton.TabIndex = 264;
            this.crest_010_RadioButton.UseVisualStyleBackColor = false;
            this.crest_010_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.trough_013_RadioButton);
            this.panel50.Controls.Add(this.flat_013_RadioButton);
            this.panel50.Controls.Add(this.crest_013_RadioButton);
            this.panel50.Location = new System.Drawing.Point(488, 143);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(188, 25);
            this.panel50.TabIndex = 482;
            // 
            // trough_013_RadioButton
            // 
            this.trough_013_RadioButton.AutoCheck = false;
            this.trough_013_RadioButton.AutoSize = true;
            this.trough_013_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_013_RadioButton.Checked = true;
            this.trough_013_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_013_RadioButton.Name = "trough_013_RadioButton";
            this.trough_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_013_RadioButton.TabIndex = 482;
            this.trough_013_RadioButton.UseVisualStyleBackColor = false;
            this.trough_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_013_RadioButton
            // 
            this.flat_013_RadioButton.AutoCheck = false;
            this.flat_013_RadioButton.AutoSize = true;
            this.flat_013_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_013_RadioButton.Name = "flat_013_RadioButton";
            this.flat_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_013_RadioButton.TabIndex = 266;
            this.flat_013_RadioButton.UseVisualStyleBackColor = false;
            this.flat_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_013_RadioButton
            // 
            this.crest_013_RadioButton.AutoCheck = false;
            this.crest_013_RadioButton.AutoSize = true;
            this.crest_013_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_013_RadioButton.Name = "crest_013_RadioButton";
            this.crest_013_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_013_RadioButton.TabIndex = 264;
            this.crest_013_RadioButton.UseVisualStyleBackColor = false;
            this.crest_013_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.trough_020_RadioButton);
            this.panel51.Controls.Add(this.flat_020_RadioButton);
            this.panel51.Controls.Add(this.crest_020_RadioButton);
            this.panel51.Location = new System.Drawing.Point(488, 170);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(188, 25);
            this.panel51.TabIndex = 483;
            // 
            // trough_020_RadioButton
            // 
            this.trough_020_RadioButton.AutoCheck = false;
            this.trough_020_RadioButton.AutoSize = true;
            this.trough_020_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_020_RadioButton.Checked = true;
            this.trough_020_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_020_RadioButton.Name = "trough_020_RadioButton";
            this.trough_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_020_RadioButton.TabIndex = 482;
            this.trough_020_RadioButton.UseVisualStyleBackColor = false;
            this.trough_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_020_RadioButton
            // 
            this.flat_020_RadioButton.AutoCheck = false;
            this.flat_020_RadioButton.AutoSize = true;
            this.flat_020_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_020_RadioButton.Name = "flat_020_RadioButton";
            this.flat_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_020_RadioButton.TabIndex = 266;
            this.flat_020_RadioButton.UseVisualStyleBackColor = false;
            this.flat_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_020_RadioButton
            // 
            this.crest_020_RadioButton.AutoCheck = false;
            this.crest_020_RadioButton.AutoSize = true;
            this.crest_020_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_020_RadioButton.Name = "crest_020_RadioButton";
            this.crest_020_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_020_RadioButton.TabIndex = 264;
            this.crest_020_RadioButton.UseVisualStyleBackColor = false;
            this.crest_020_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel52
            // 
            this.panel52.Controls.Add(this.trough_023_RadioButton);
            this.panel52.Controls.Add(this.flat_023_RadioButton);
            this.panel52.Controls.Add(this.crest_023_RadioButton);
            this.panel52.Location = new System.Drawing.Point(488, 197);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(188, 25);
            this.panel52.TabIndex = 484;
            // 
            // trough_023_RadioButton
            // 
            this.trough_023_RadioButton.AutoCheck = false;
            this.trough_023_RadioButton.AutoSize = true;
            this.trough_023_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_023_RadioButton.Checked = true;
            this.trough_023_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_023_RadioButton.Name = "trough_023_RadioButton";
            this.trough_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_023_RadioButton.TabIndex = 482;
            this.trough_023_RadioButton.UseVisualStyleBackColor = false;
            this.trough_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_023_RadioButton
            // 
            this.flat_023_RadioButton.AutoCheck = false;
            this.flat_023_RadioButton.AutoSize = true;
            this.flat_023_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_023_RadioButton.Name = "flat_023_RadioButton";
            this.flat_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_023_RadioButton.TabIndex = 266;
            this.flat_023_RadioButton.UseVisualStyleBackColor = false;
            this.flat_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_023_RadioButton
            // 
            this.crest_023_RadioButton.AutoCheck = false;
            this.crest_023_RadioButton.AutoSize = true;
            this.crest_023_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_023_RadioButton.Name = "crest_023_RadioButton";
            this.crest_023_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_023_RadioButton.TabIndex = 264;
            this.crest_023_RadioButton.UseVisualStyleBackColor = false;
            this.crest_023_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.trough_030_RadioButton);
            this.panel53.Controls.Add(this.flat_030_RadioButton);
            this.panel53.Controls.Add(this.crest_030_RadioButton);
            this.panel53.Location = new System.Drawing.Point(488, 224);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(188, 25);
            this.panel53.TabIndex = 485;
            // 
            // trough_030_RadioButton
            // 
            this.trough_030_RadioButton.AutoCheck = false;
            this.trough_030_RadioButton.AutoSize = true;
            this.trough_030_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_030_RadioButton.Checked = true;
            this.trough_030_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_030_RadioButton.Name = "trough_030_RadioButton";
            this.trough_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_030_RadioButton.TabIndex = 482;
            this.trough_030_RadioButton.UseVisualStyleBackColor = false;
            this.trough_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_030_RadioButton
            // 
            this.flat_030_RadioButton.AutoCheck = false;
            this.flat_030_RadioButton.AutoSize = true;
            this.flat_030_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_030_RadioButton.Name = "flat_030_RadioButton";
            this.flat_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_030_RadioButton.TabIndex = 266;
            this.flat_030_RadioButton.UseVisualStyleBackColor = false;
            this.flat_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_030_RadioButton
            // 
            this.crest_030_RadioButton.AutoCheck = false;
            this.crest_030_RadioButton.AutoSize = true;
            this.crest_030_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_030_RadioButton.Name = "crest_030_RadioButton";
            this.crest_030_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_030_RadioButton.TabIndex = 264;
            this.crest_030_RadioButton.UseVisualStyleBackColor = false;
            this.crest_030_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.trough_033_RadioButton);
            this.panel54.Controls.Add(this.flat_033_RadioButton);
            this.panel54.Controls.Add(this.crest_033_RadioButton);
            this.panel54.Location = new System.Drawing.Point(488, 251);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(188, 25);
            this.panel54.TabIndex = 486;
            // 
            // trough_033_RadioButton
            // 
            this.trough_033_RadioButton.AutoCheck = false;
            this.trough_033_RadioButton.AutoSize = true;
            this.trough_033_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_033_RadioButton.Checked = true;
            this.trough_033_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_033_RadioButton.Name = "trough_033_RadioButton";
            this.trough_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_033_RadioButton.TabIndex = 482;
            this.trough_033_RadioButton.UseVisualStyleBackColor = false;
            this.trough_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_033_RadioButton
            // 
            this.flat_033_RadioButton.AutoCheck = false;
            this.flat_033_RadioButton.AutoSize = true;
            this.flat_033_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_033_RadioButton.Name = "flat_033_RadioButton";
            this.flat_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_033_RadioButton.TabIndex = 266;
            this.flat_033_RadioButton.UseVisualStyleBackColor = false;
            this.flat_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_033_RadioButton
            // 
            this.crest_033_RadioButton.AutoCheck = false;
            this.crest_033_RadioButton.AutoSize = true;
            this.crest_033_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_033_RadioButton.Name = "crest_033_RadioButton";
            this.crest_033_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_033_RadioButton.TabIndex = 264;
            this.crest_033_RadioButton.UseVisualStyleBackColor = false;
            this.crest_033_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.trough_040_RadioButton);
            this.panel55.Controls.Add(this.flat_040_RadioButton);
            this.panel55.Controls.Add(this.crest_040_RadioButton);
            this.panel55.Location = new System.Drawing.Point(488, 278);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(188, 25);
            this.panel55.TabIndex = 487;
            // 
            // trough_040_RadioButton
            // 
            this.trough_040_RadioButton.AutoCheck = false;
            this.trough_040_RadioButton.AutoSize = true;
            this.trough_040_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_040_RadioButton.Checked = true;
            this.trough_040_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_040_RadioButton.Name = "trough_040_RadioButton";
            this.trough_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_040_RadioButton.TabIndex = 482;
            this.trough_040_RadioButton.UseVisualStyleBackColor = false;
            this.trough_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_040_RadioButton
            // 
            this.flat_040_RadioButton.AutoCheck = false;
            this.flat_040_RadioButton.AutoSize = true;
            this.flat_040_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_040_RadioButton.Name = "flat_040_RadioButton";
            this.flat_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_040_RadioButton.TabIndex = 266;
            this.flat_040_RadioButton.UseVisualStyleBackColor = false;
            this.flat_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_040_RadioButton
            // 
            this.crest_040_RadioButton.AutoCheck = false;
            this.crest_040_RadioButton.AutoSize = true;
            this.crest_040_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_040_RadioButton.Name = "crest_040_RadioButton";
            this.crest_040_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_040_RadioButton.TabIndex = 264;
            this.crest_040_RadioButton.UseVisualStyleBackColor = false;
            this.crest_040_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.trough_043_RadioButton);
            this.panel56.Controls.Add(this.flat_043_RadioButton);
            this.panel56.Controls.Add(this.crest_043_RadioButton);
            this.panel56.Location = new System.Drawing.Point(488, 305);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(188, 25);
            this.panel56.TabIndex = 488;
            // 
            // trough_043_RadioButton
            // 
            this.trough_043_RadioButton.AutoCheck = false;
            this.trough_043_RadioButton.AutoSize = true;
            this.trough_043_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_043_RadioButton.Checked = true;
            this.trough_043_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_043_RadioButton.Name = "trough_043_RadioButton";
            this.trough_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_043_RadioButton.TabIndex = 482;
            this.trough_043_RadioButton.UseVisualStyleBackColor = false;
            this.trough_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_043_RadioButton
            // 
            this.flat_043_RadioButton.AutoCheck = false;
            this.flat_043_RadioButton.AutoSize = true;
            this.flat_043_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_043_RadioButton.Name = "flat_043_RadioButton";
            this.flat_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_043_RadioButton.TabIndex = 266;
            this.flat_043_RadioButton.UseVisualStyleBackColor = false;
            this.flat_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_043_RadioButton
            // 
            this.crest_043_RadioButton.AutoCheck = false;
            this.crest_043_RadioButton.AutoSize = true;
            this.crest_043_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_043_RadioButton.Name = "crest_043_RadioButton";
            this.crest_043_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_043_RadioButton.TabIndex = 264;
            this.crest_043_RadioButton.UseVisualStyleBackColor = false;
            this.crest_043_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.trough_050_RadioButton);
            this.panel57.Controls.Add(this.flat_050_RadioButton);
            this.panel57.Controls.Add(this.crest_050_RadioButton);
            this.panel57.Location = new System.Drawing.Point(488, 348);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(188, 25);
            this.panel57.TabIndex = 489;
            // 
            // trough_050_RadioButton
            // 
            this.trough_050_RadioButton.AutoCheck = false;
            this.trough_050_RadioButton.AutoSize = true;
            this.trough_050_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_050_RadioButton.Checked = true;
            this.trough_050_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_050_RadioButton.Name = "trough_050_RadioButton";
            this.trough_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_050_RadioButton.TabIndex = 482;
            this.trough_050_RadioButton.UseVisualStyleBackColor = false;
            this.trough_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_050_RadioButton
            // 
            this.flat_050_RadioButton.AutoCheck = false;
            this.flat_050_RadioButton.AutoSize = true;
            this.flat_050_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_050_RadioButton.Name = "flat_050_RadioButton";
            this.flat_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_050_RadioButton.TabIndex = 266;
            this.flat_050_RadioButton.UseVisualStyleBackColor = false;
            this.flat_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_050_RadioButton
            // 
            this.crest_050_RadioButton.AutoCheck = false;
            this.crest_050_RadioButton.AutoSize = true;
            this.crest_050_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_050_RadioButton.Name = "crest_050_RadioButton";
            this.crest_050_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_050_RadioButton.TabIndex = 264;
            this.crest_050_RadioButton.UseVisualStyleBackColor = false;
            this.crest_050_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.trough_053_RadioButton);
            this.panel58.Controls.Add(this.flat_053_RadioButton);
            this.panel58.Controls.Add(this.crest_053_RadioButton);
            this.panel58.Location = new System.Drawing.Point(488, 375);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(188, 25);
            this.panel58.TabIndex = 490;
            // 
            // trough_053_RadioButton
            // 
            this.trough_053_RadioButton.AutoCheck = false;
            this.trough_053_RadioButton.AutoSize = true;
            this.trough_053_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_053_RadioButton.Checked = true;
            this.trough_053_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_053_RadioButton.Name = "trough_053_RadioButton";
            this.trough_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_053_RadioButton.TabIndex = 482;
            this.trough_053_RadioButton.UseVisualStyleBackColor = false;
            this.trough_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_053_RadioButton
            // 
            this.flat_053_RadioButton.AutoCheck = false;
            this.flat_053_RadioButton.AutoSize = true;
            this.flat_053_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_053_RadioButton.Name = "flat_053_RadioButton";
            this.flat_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_053_RadioButton.TabIndex = 266;
            this.flat_053_RadioButton.UseVisualStyleBackColor = false;
            this.flat_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_053_RadioButton
            // 
            this.crest_053_RadioButton.AutoCheck = false;
            this.crest_053_RadioButton.AutoSize = true;
            this.crest_053_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_053_RadioButton.Name = "crest_053_RadioButton";
            this.crest_053_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_053_RadioButton.TabIndex = 264;
            this.crest_053_RadioButton.UseVisualStyleBackColor = false;
            this.crest_053_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.trough_060_RadioButton);
            this.panel59.Controls.Add(this.flat_060_RadioButton);
            this.panel59.Controls.Add(this.crest_060_RadioButton);
            this.panel59.Location = new System.Drawing.Point(488, 402);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(188, 25);
            this.panel59.TabIndex = 491;
            // 
            // trough_060_RadioButton
            // 
            this.trough_060_RadioButton.AutoCheck = false;
            this.trough_060_RadioButton.AutoSize = true;
            this.trough_060_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_060_RadioButton.Checked = true;
            this.trough_060_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_060_RadioButton.Name = "trough_060_RadioButton";
            this.trough_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_060_RadioButton.TabIndex = 482;
            this.trough_060_RadioButton.UseVisualStyleBackColor = false;
            this.trough_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_060_RadioButton
            // 
            this.flat_060_RadioButton.AutoCheck = false;
            this.flat_060_RadioButton.AutoSize = true;
            this.flat_060_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_060_RadioButton.Name = "flat_060_RadioButton";
            this.flat_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_060_RadioButton.TabIndex = 266;
            this.flat_060_RadioButton.UseVisualStyleBackColor = false;
            this.flat_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_060_RadioButton
            // 
            this.crest_060_RadioButton.AutoCheck = false;
            this.crest_060_RadioButton.AutoSize = true;
            this.crest_060_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_060_RadioButton.Name = "crest_060_RadioButton";
            this.crest_060_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_060_RadioButton.TabIndex = 264;
            this.crest_060_RadioButton.UseVisualStyleBackColor = false;
            this.crest_060_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel60
            // 
            this.panel60.Controls.Add(this.trough_063_RadioButton);
            this.panel60.Controls.Add(this.flat_063_RadioButton);
            this.panel60.Controls.Add(this.crest_063_RadioButton);
            this.panel60.Location = new System.Drawing.Point(488, 429);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(188, 25);
            this.panel60.TabIndex = 492;
            // 
            // trough_063_RadioButton
            // 
            this.trough_063_RadioButton.AutoCheck = false;
            this.trough_063_RadioButton.AutoSize = true;
            this.trough_063_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_063_RadioButton.Checked = true;
            this.trough_063_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_063_RadioButton.Name = "trough_063_RadioButton";
            this.trough_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_063_RadioButton.TabIndex = 482;
            this.trough_063_RadioButton.UseVisualStyleBackColor = false;
            this.trough_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_063_RadioButton
            // 
            this.flat_063_RadioButton.AutoCheck = false;
            this.flat_063_RadioButton.AutoSize = true;
            this.flat_063_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_063_RadioButton.Name = "flat_063_RadioButton";
            this.flat_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_063_RadioButton.TabIndex = 266;
            this.flat_063_RadioButton.UseVisualStyleBackColor = false;
            this.flat_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_063_RadioButton
            // 
            this.crest_063_RadioButton.AutoCheck = false;
            this.crest_063_RadioButton.AutoSize = true;
            this.crest_063_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_063_RadioButton.Name = "crest_063_RadioButton";
            this.crest_063_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_063_RadioButton.TabIndex = 264;
            this.crest_063_RadioButton.UseVisualStyleBackColor = false;
            this.crest_063_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.trough_070_RadioButton);
            this.panel61.Controls.Add(this.flat_070_RadioButton);
            this.panel61.Controls.Add(this.crest_070_RadioButton);
            this.panel61.Location = new System.Drawing.Point(488, 456);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(188, 25);
            this.panel61.TabIndex = 493;
            // 
            // trough_070_RadioButton
            // 
            this.trough_070_RadioButton.AutoCheck = false;
            this.trough_070_RadioButton.AutoSize = true;
            this.trough_070_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_070_RadioButton.Checked = true;
            this.trough_070_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_070_RadioButton.Name = "trough_070_RadioButton";
            this.trough_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_070_RadioButton.TabIndex = 482;
            this.trough_070_RadioButton.UseVisualStyleBackColor = false;
            this.trough_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_070_RadioButton
            // 
            this.flat_070_RadioButton.AutoCheck = false;
            this.flat_070_RadioButton.AutoSize = true;
            this.flat_070_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_070_RadioButton.Name = "flat_070_RadioButton";
            this.flat_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_070_RadioButton.TabIndex = 266;
            this.flat_070_RadioButton.UseVisualStyleBackColor = false;
            this.flat_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_070_RadioButton
            // 
            this.crest_070_RadioButton.AutoCheck = false;
            this.crest_070_RadioButton.AutoSize = true;
            this.crest_070_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_070_RadioButton.Name = "crest_070_RadioButton";
            this.crest_070_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_070_RadioButton.TabIndex = 264;
            this.crest_070_RadioButton.UseVisualStyleBackColor = false;
            this.crest_070_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.trough_073_RadioButton);
            this.panel62.Controls.Add(this.flat_073_RadioButton);
            this.panel62.Controls.Add(this.crest_073_RadioButton);
            this.panel62.Location = new System.Drawing.Point(488, 483);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(188, 25);
            this.panel62.TabIndex = 494;
            // 
            // trough_073_RadioButton
            // 
            this.trough_073_RadioButton.AutoCheck = false;
            this.trough_073_RadioButton.AutoSize = true;
            this.trough_073_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_073_RadioButton.Name = "trough_073_RadioButton";
            this.trough_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_073_RadioButton.TabIndex = 482;
            this.trough_073_RadioButton.UseVisualStyleBackColor = false;
            this.trough_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_073_RadioButton
            // 
            this.flat_073_RadioButton.AutoCheck = false;
            this.flat_073_RadioButton.AutoSize = true;
            this.flat_073_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_073_RadioButton.Checked = true;
            this.flat_073_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_073_RadioButton.Name = "flat_073_RadioButton";
            this.flat_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_073_RadioButton.TabIndex = 266;
            this.flat_073_RadioButton.UseVisualStyleBackColor = false;
            this.flat_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_073_RadioButton
            // 
            this.crest_073_RadioButton.AutoCheck = false;
            this.crest_073_RadioButton.AutoSize = true;
            this.crest_073_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_073_RadioButton.Name = "crest_073_RadioButton";
            this.crest_073_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_073_RadioButton.TabIndex = 264;
            this.crest_073_RadioButton.UseVisualStyleBackColor = false;
            this.crest_073_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.trough_080_RadioButton);
            this.panel63.Controls.Add(this.flat_080_RadioButton);
            this.panel63.Controls.Add(this.crest_080_RadioButton);
            this.panel63.Location = new System.Drawing.Point(488, 510);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(188, 25);
            this.panel63.TabIndex = 495;
            // 
            // trough_080_RadioButton
            // 
            this.trough_080_RadioButton.AutoCheck = false;
            this.trough_080_RadioButton.AutoSize = true;
            this.trough_080_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_080_RadioButton.Name = "trough_080_RadioButton";
            this.trough_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_080_RadioButton.TabIndex = 482;
            this.trough_080_RadioButton.UseVisualStyleBackColor = false;
            this.trough_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_080_RadioButton
            // 
            this.flat_080_RadioButton.AutoCheck = false;
            this.flat_080_RadioButton.AutoSize = true;
            this.flat_080_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_080_RadioButton.Checked = true;
            this.flat_080_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_080_RadioButton.Name = "flat_080_RadioButton";
            this.flat_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_080_RadioButton.TabIndex = 266;
            this.flat_080_RadioButton.UseVisualStyleBackColor = false;
            this.flat_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_080_RadioButton
            // 
            this.crest_080_RadioButton.AutoCheck = false;
            this.crest_080_RadioButton.AutoSize = true;
            this.crest_080_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_080_RadioButton.Name = "crest_080_RadioButton";
            this.crest_080_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_080_RadioButton.TabIndex = 264;
            this.crest_080_RadioButton.UseVisualStyleBackColor = false;
            this.crest_080_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.trough_083_RadioButton);
            this.panel64.Controls.Add(this.flat_083_RadioButton);
            this.panel64.Controls.Add(this.crest_083_RadioButton);
            this.panel64.Location = new System.Drawing.Point(488, 537);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(188, 25);
            this.panel64.TabIndex = 496;
            // 
            // trough_083_RadioButton
            // 
            this.trough_083_RadioButton.AutoCheck = false;
            this.trough_083_RadioButton.AutoSize = true;
            this.trough_083_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_083_RadioButton.Name = "trough_083_RadioButton";
            this.trough_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_083_RadioButton.TabIndex = 482;
            this.trough_083_RadioButton.UseVisualStyleBackColor = false;
            this.trough_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_083_RadioButton
            // 
            this.flat_083_RadioButton.AutoCheck = false;
            this.flat_083_RadioButton.AutoSize = true;
            this.flat_083_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_083_RadioButton.Checked = true;
            this.flat_083_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_083_RadioButton.Name = "flat_083_RadioButton";
            this.flat_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_083_RadioButton.TabIndex = 266;
            this.flat_083_RadioButton.UseVisualStyleBackColor = false;
            this.flat_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_083_RadioButton
            // 
            this.crest_083_RadioButton.AutoCheck = false;
            this.crest_083_RadioButton.AutoSize = true;
            this.crest_083_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_083_RadioButton.Name = "crest_083_RadioButton";
            this.crest_083_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_083_RadioButton.TabIndex = 264;
            this.crest_083_RadioButton.UseVisualStyleBackColor = false;
            this.crest_083_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.trough_090_RadioButton);
            this.panel65.Controls.Add(this.flat_090_RadioButton);
            this.panel65.Controls.Add(this.crest_090_RadioButton);
            this.panel65.Location = new System.Drawing.Point(488, 580);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(188, 25);
            this.panel65.TabIndex = 497;
            // 
            // trough_090_RadioButton
            // 
            this.trough_090_RadioButton.AutoCheck = false;
            this.trough_090_RadioButton.AutoSize = true;
            this.trough_090_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_090_RadioButton.Name = "trough_090_RadioButton";
            this.trough_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_090_RadioButton.TabIndex = 482;
            this.trough_090_RadioButton.UseVisualStyleBackColor = false;
            this.trough_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_090_RadioButton
            // 
            this.flat_090_RadioButton.AutoCheck = false;
            this.flat_090_RadioButton.AutoSize = true;
            this.flat_090_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_090_RadioButton.Checked = true;
            this.flat_090_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_090_RadioButton.Name = "flat_090_RadioButton";
            this.flat_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_090_RadioButton.TabIndex = 266;
            this.flat_090_RadioButton.UseVisualStyleBackColor = false;
            this.flat_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_090_RadioButton
            // 
            this.crest_090_RadioButton.AutoCheck = false;
            this.crest_090_RadioButton.AutoSize = true;
            this.crest_090_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_090_RadioButton.Name = "crest_090_RadioButton";
            this.crest_090_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_090_RadioButton.TabIndex = 264;
            this.crest_090_RadioButton.UseVisualStyleBackColor = false;
            this.crest_090_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.trough_093_RadioButton);
            this.panel66.Controls.Add(this.flat_093_RadioButton);
            this.panel66.Controls.Add(this.crest_093_RadioButton);
            this.panel66.Location = new System.Drawing.Point(488, 607);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(188, 25);
            this.panel66.TabIndex = 498;
            // 
            // trough_093_RadioButton
            // 
            this.trough_093_RadioButton.AutoCheck = false;
            this.trough_093_RadioButton.AutoSize = true;
            this.trough_093_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_093_RadioButton.Name = "trough_093_RadioButton";
            this.trough_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_093_RadioButton.TabIndex = 482;
            this.trough_093_RadioButton.UseVisualStyleBackColor = false;
            this.trough_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_093_RadioButton
            // 
            this.flat_093_RadioButton.AutoCheck = false;
            this.flat_093_RadioButton.AutoSize = true;
            this.flat_093_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_093_RadioButton.Checked = true;
            this.flat_093_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_093_RadioButton.Name = "flat_093_RadioButton";
            this.flat_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_093_RadioButton.TabIndex = 266;
            this.flat_093_RadioButton.UseVisualStyleBackColor = false;
            this.flat_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_093_RadioButton
            // 
            this.crest_093_RadioButton.AutoCheck = false;
            this.crest_093_RadioButton.AutoSize = true;
            this.crest_093_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_093_RadioButton.Name = "crest_093_RadioButton";
            this.crest_093_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_093_RadioButton.TabIndex = 264;
            this.crest_093_RadioButton.UseVisualStyleBackColor = false;
            this.crest_093_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.trough_100_RadioButton);
            this.panel67.Controls.Add(this.flat_100_RadioButton);
            this.panel67.Controls.Add(this.crest_100_RadioButton);
            this.panel67.Location = new System.Drawing.Point(488, 634);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(188, 25);
            this.panel67.TabIndex = 499;
            // 
            // trough_100_RadioButton
            // 
            this.trough_100_RadioButton.AutoCheck = false;
            this.trough_100_RadioButton.AutoSize = true;
            this.trough_100_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_100_RadioButton.Name = "trough_100_RadioButton";
            this.trough_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_100_RadioButton.TabIndex = 482;
            this.trough_100_RadioButton.UseVisualStyleBackColor = false;
            this.trough_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_100_RadioButton
            // 
            this.flat_100_RadioButton.AutoCheck = false;
            this.flat_100_RadioButton.AutoSize = true;
            this.flat_100_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_100_RadioButton.Name = "flat_100_RadioButton";
            this.flat_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_100_RadioButton.TabIndex = 266;
            this.flat_100_RadioButton.UseVisualStyleBackColor = false;
            this.flat_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_100_RadioButton
            // 
            this.crest_100_RadioButton.AutoCheck = false;
            this.crest_100_RadioButton.AutoSize = true;
            this.crest_100_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_100_RadioButton.Checked = true;
            this.crest_100_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_100_RadioButton.Name = "crest_100_RadioButton";
            this.crest_100_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_100_RadioButton.TabIndex = 264;
            this.crest_100_RadioButton.UseVisualStyleBackColor = false;
            this.crest_100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel68
            // 
            this.panel68.Controls.Add(this.trough_103_RadioButton);
            this.panel68.Controls.Add(this.flat_103_RadioButton);
            this.panel68.Controls.Add(this.crest_103_RadioButton);
            this.panel68.Location = new System.Drawing.Point(488, 661);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(188, 25);
            this.panel68.TabIndex = 500;
            // 
            // trough_103_RadioButton
            // 
            this.trough_103_RadioButton.AutoCheck = false;
            this.trough_103_RadioButton.AutoSize = true;
            this.trough_103_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_103_RadioButton.Name = "trough_103_RadioButton";
            this.trough_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_103_RadioButton.TabIndex = 482;
            this.trough_103_RadioButton.UseVisualStyleBackColor = false;
            this.trough_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_103_RadioButton
            // 
            this.flat_103_RadioButton.AutoCheck = false;
            this.flat_103_RadioButton.AutoSize = true;
            this.flat_103_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_103_RadioButton.Name = "flat_103_RadioButton";
            this.flat_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_103_RadioButton.TabIndex = 266;
            this.flat_103_RadioButton.UseVisualStyleBackColor = false;
            this.flat_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_103_RadioButton
            // 
            this.crest_103_RadioButton.AutoCheck = false;
            this.crest_103_RadioButton.AutoSize = true;
            this.crest_103_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_103_RadioButton.Checked = true;
            this.crest_103_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_103_RadioButton.Name = "crest_103_RadioButton";
            this.crest_103_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_103_RadioButton.TabIndex = 264;
            this.crest_103_RadioButton.UseVisualStyleBackColor = false;
            this.crest_103_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.trough_110_RadioButton);
            this.panel69.Controls.Add(this.flat_110_RadioButton);
            this.panel69.Controls.Add(this.crest_110_RadioButton);
            this.panel69.Location = new System.Drawing.Point(488, 688);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(188, 25);
            this.panel69.TabIndex = 501;
            // 
            // trough_110_RadioButton
            // 
            this.trough_110_RadioButton.AutoCheck = false;
            this.trough_110_RadioButton.AutoSize = true;
            this.trough_110_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_110_RadioButton.Name = "trough_110_RadioButton";
            this.trough_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_110_RadioButton.TabIndex = 482;
            this.trough_110_RadioButton.UseVisualStyleBackColor = false;
            this.trough_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_110_RadioButton
            // 
            this.flat_110_RadioButton.AutoCheck = false;
            this.flat_110_RadioButton.AutoSize = true;
            this.flat_110_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_110_RadioButton.Name = "flat_110_RadioButton";
            this.flat_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_110_RadioButton.TabIndex = 266;
            this.flat_110_RadioButton.UseVisualStyleBackColor = false;
            this.flat_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_110_RadioButton
            // 
            this.crest_110_RadioButton.AutoCheck = false;
            this.crest_110_RadioButton.AutoSize = true;
            this.crest_110_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_110_RadioButton.Checked = true;
            this.crest_110_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_110_RadioButton.Name = "crest_110_RadioButton";
            this.crest_110_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_110_RadioButton.TabIndex = 264;
            this.crest_110_RadioButton.UseVisualStyleBackColor = false;
            this.crest_110_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel70
            // 
            this.panel70.Controls.Add(this.trough_113_RadioButton);
            this.panel70.Controls.Add(this.flat_113_RadioButton);
            this.panel70.Controls.Add(this.crest_113_RadioButton);
            this.panel70.Location = new System.Drawing.Point(488, 715);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(188, 25);
            this.panel70.TabIndex = 502;
            // 
            // trough_113_RadioButton
            // 
            this.trough_113_RadioButton.AutoCheck = false;
            this.trough_113_RadioButton.AutoSize = true;
            this.trough_113_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_113_RadioButton.Name = "trough_113_RadioButton";
            this.trough_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_113_RadioButton.TabIndex = 482;
            this.trough_113_RadioButton.UseVisualStyleBackColor = false;
            this.trough_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_113_RadioButton
            // 
            this.flat_113_RadioButton.AutoCheck = false;
            this.flat_113_RadioButton.AutoSize = true;
            this.flat_113_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_113_RadioButton.Name = "flat_113_RadioButton";
            this.flat_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_113_RadioButton.TabIndex = 266;
            this.flat_113_RadioButton.UseVisualStyleBackColor = false;
            this.flat_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_113_RadioButton
            // 
            this.crest_113_RadioButton.AutoCheck = false;
            this.crest_113_RadioButton.AutoSize = true;
            this.crest_113_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_113_RadioButton.Checked = true;
            this.crest_113_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_113_RadioButton.Name = "crest_113_RadioButton";
            this.crest_113_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_113_RadioButton.TabIndex = 264;
            this.crest_113_RadioButton.UseVisualStyleBackColor = false;
            this.crest_113_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel71
            // 
            this.panel71.Controls.Add(this.trough_120_RadioButton);
            this.panel71.Controls.Add(this.flat_120_RadioButton);
            this.panel71.Controls.Add(this.crest_120_RadioButton);
            this.panel71.Location = new System.Drawing.Point(488, 742);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(188, 25);
            this.panel71.TabIndex = 503;
            // 
            // trough_120_RadioButton
            // 
            this.trough_120_RadioButton.AutoCheck = false;
            this.trough_120_RadioButton.AutoSize = true;
            this.trough_120_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_120_RadioButton.Name = "trough_120_RadioButton";
            this.trough_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_120_RadioButton.TabIndex = 482;
            this.trough_120_RadioButton.UseVisualStyleBackColor = false;
            this.trough_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_120_RadioButton
            // 
            this.flat_120_RadioButton.AutoCheck = false;
            this.flat_120_RadioButton.AutoSize = true;
            this.flat_120_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_120_RadioButton.Checked = true;
            this.flat_120_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_120_RadioButton.Name = "flat_120_RadioButton";
            this.flat_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_120_RadioButton.TabIndex = 266;
            this.flat_120_RadioButton.UseVisualStyleBackColor = false;
            this.flat_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_120_RadioButton
            // 
            this.crest_120_RadioButton.AutoCheck = false;
            this.crest_120_RadioButton.AutoSize = true;
            this.crest_120_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_120_RadioButton.Name = "crest_120_RadioButton";
            this.crest_120_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_120_RadioButton.TabIndex = 264;
            this.crest_120_RadioButton.UseVisualStyleBackColor = false;
            this.crest_120_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel72
            // 
            this.panel72.Controls.Add(this.trough_123_RadioButton);
            this.panel72.Controls.Add(this.flat_123_RadioButton);
            this.panel72.Controls.Add(this.crest_123_RadioButton);
            this.panel72.Location = new System.Drawing.Point(488, 769);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(188, 25);
            this.panel72.TabIndex = 504;
            // 
            // trough_123_RadioButton
            // 
            this.trough_123_RadioButton.AutoCheck = false;
            this.trough_123_RadioButton.AutoSize = true;
            this.trough_123_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_123_RadioButton.Name = "trough_123_RadioButton";
            this.trough_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_123_RadioButton.TabIndex = 482;
            this.trough_123_RadioButton.UseVisualStyleBackColor = false;
            this.trough_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_123_RadioButton
            // 
            this.flat_123_RadioButton.AutoCheck = false;
            this.flat_123_RadioButton.AutoSize = true;
            this.flat_123_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_123_RadioButton.Checked = true;
            this.flat_123_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_123_RadioButton.Name = "flat_123_RadioButton";
            this.flat_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_123_RadioButton.TabIndex = 266;
            this.flat_123_RadioButton.UseVisualStyleBackColor = false;
            this.flat_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_123_RadioButton
            // 
            this.crest_123_RadioButton.AutoCheck = false;
            this.crest_123_RadioButton.AutoSize = true;
            this.crest_123_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_123_RadioButton.Name = "crest_123_RadioButton";
            this.crest_123_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_123_RadioButton.TabIndex = 264;
            this.crest_123_RadioButton.UseVisualStyleBackColor = false;
            this.crest_123_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_130_RadioButton
            // 
            this.full_Ice_130_RadioButton.AutoCheck = false;
            this.full_Ice_130_RadioButton.AutoSize = true;
            this.full_Ice_130_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_130_RadioButton.Name = "full_Ice_130_RadioButton";
            this.full_Ice_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_130_RadioButton.TabIndex = 264;
            this.full_Ice_130_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Air_130_RadioButton
            // 
            this.full_Air_130_RadioButton.AutoCheck = false;
            this.full_Air_130_RadioButton.AutoSize = true;
            this.full_Air_130_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_130_RadioButton.Checked = true;
            this.full_Air_130_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_130_RadioButton.Name = "full_Air_130_RadioButton";
            this.full_Air_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_130_RadioButton.TabIndex = 266;
            this.full_Air_130_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel73
            // 
            this.panel73.Controls.Add(this.full_Air_130_RadioButton);
            this.panel73.Controls.Add(this.full_Ice_130_RadioButton);
            this.panel73.Location = new System.Drawing.Point(859, 116);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(127, 25);
            this.panel73.TabIndex = 505;
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.full_Air_133_RadioButton);
            this.panel74.Controls.Add(this.full_Ice_133_RadioButton);
            this.panel74.Location = new System.Drawing.Point(859, 143);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(127, 25);
            this.panel74.TabIndex = 506;
            // 
            // full_Air_133_RadioButton
            // 
            this.full_Air_133_RadioButton.AutoCheck = false;
            this.full_Air_133_RadioButton.AutoSize = true;
            this.full_Air_133_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_133_RadioButton.Checked = true;
            this.full_Air_133_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_133_RadioButton.Name = "full_Air_133_RadioButton";
            this.full_Air_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_133_RadioButton.TabIndex = 266;
            this.full_Air_133_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_133_RadioButton
            // 
            this.full_Ice_133_RadioButton.AutoCheck = false;
            this.full_Ice_133_RadioButton.AutoSize = true;
            this.full_Ice_133_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_133_RadioButton.Name = "full_Ice_133_RadioButton";
            this.full_Ice_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_133_RadioButton.TabIndex = 264;
            this.full_Ice_133_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel75
            // 
            this.panel75.Controls.Add(this.full_Air_140_RadioButton);
            this.panel75.Controls.Add(this.full_Ice_140_RadioButton);
            this.panel75.Location = new System.Drawing.Point(859, 170);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(127, 25);
            this.panel75.TabIndex = 507;
            // 
            // full_Air_140_RadioButton
            // 
            this.full_Air_140_RadioButton.AutoCheck = false;
            this.full_Air_140_RadioButton.AutoSize = true;
            this.full_Air_140_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_140_RadioButton.Checked = true;
            this.full_Air_140_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_140_RadioButton.Name = "full_Air_140_RadioButton";
            this.full_Air_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_140_RadioButton.TabIndex = 266;
            this.full_Air_140_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_140_RadioButton
            // 
            this.full_Ice_140_RadioButton.AutoCheck = false;
            this.full_Ice_140_RadioButton.AutoSize = true;
            this.full_Ice_140_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_140_RadioButton.Name = "full_Ice_140_RadioButton";
            this.full_Ice_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_140_RadioButton.TabIndex = 264;
            this.full_Ice_140_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel76
            // 
            this.panel76.Controls.Add(this.full_Air_143_RadioButton);
            this.panel76.Controls.Add(this.full_Ice_143_RadioButton);
            this.panel76.Location = new System.Drawing.Point(859, 197);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(127, 25);
            this.panel76.TabIndex = 508;
            // 
            // full_Air_143_RadioButton
            // 
            this.full_Air_143_RadioButton.AutoCheck = false;
            this.full_Air_143_RadioButton.AutoSize = true;
            this.full_Air_143_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_143_RadioButton.Checked = true;
            this.full_Air_143_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_143_RadioButton.Name = "full_Air_143_RadioButton";
            this.full_Air_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_143_RadioButton.TabIndex = 266;
            this.full_Air_143_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_143_RadioButton
            // 
            this.full_Ice_143_RadioButton.AutoCheck = false;
            this.full_Ice_143_RadioButton.AutoSize = true;
            this.full_Ice_143_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_143_RadioButton.Name = "full_Ice_143_RadioButton";
            this.full_Ice_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_143_RadioButton.TabIndex = 264;
            this.full_Ice_143_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel77
            // 
            this.panel77.Controls.Add(this.full_Air_150_RadioButton);
            this.panel77.Controls.Add(this.full_Ice_150_RadioButton);
            this.panel77.Location = new System.Drawing.Point(859, 224);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(127, 25);
            this.panel77.TabIndex = 509;
            // 
            // full_Air_150_RadioButton
            // 
            this.full_Air_150_RadioButton.AutoCheck = false;
            this.full_Air_150_RadioButton.AutoSize = true;
            this.full_Air_150_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_150_RadioButton.Checked = true;
            this.full_Air_150_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_150_RadioButton.Name = "full_Air_150_RadioButton";
            this.full_Air_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_150_RadioButton.TabIndex = 266;
            this.full_Air_150_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_150_RadioButton
            // 
            this.full_Ice_150_RadioButton.AutoCheck = false;
            this.full_Ice_150_RadioButton.AutoSize = true;
            this.full_Ice_150_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_150_RadioButton.Name = "full_Ice_150_RadioButton";
            this.full_Ice_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_150_RadioButton.TabIndex = 264;
            this.full_Ice_150_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.full_Air_153_RadioButton);
            this.panel78.Controls.Add(this.full_Ice_153_RadioButton);
            this.panel78.Location = new System.Drawing.Point(859, 251);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(127, 25);
            this.panel78.TabIndex = 510;
            // 
            // full_Air_153_RadioButton
            // 
            this.full_Air_153_RadioButton.AutoCheck = false;
            this.full_Air_153_RadioButton.AutoSize = true;
            this.full_Air_153_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_153_RadioButton.Checked = true;
            this.full_Air_153_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_153_RadioButton.Name = "full_Air_153_RadioButton";
            this.full_Air_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_153_RadioButton.TabIndex = 266;
            this.full_Air_153_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_153_RadioButton
            // 
            this.full_Ice_153_RadioButton.AutoCheck = false;
            this.full_Ice_153_RadioButton.AutoSize = true;
            this.full_Ice_153_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_153_RadioButton.Name = "full_Ice_153_RadioButton";
            this.full_Ice_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_153_RadioButton.TabIndex = 264;
            this.full_Ice_153_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel79
            // 
            this.panel79.Controls.Add(this.full_Air_160_RadioButton);
            this.panel79.Controls.Add(this.full_Ice_160_RadioButton);
            this.panel79.Location = new System.Drawing.Point(859, 278);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(127, 25);
            this.panel79.TabIndex = 511;
            // 
            // full_Air_160_RadioButton
            // 
            this.full_Air_160_RadioButton.AutoCheck = false;
            this.full_Air_160_RadioButton.AutoSize = true;
            this.full_Air_160_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_160_RadioButton.Checked = true;
            this.full_Air_160_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_160_RadioButton.Name = "full_Air_160_RadioButton";
            this.full_Air_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_160_RadioButton.TabIndex = 266;
            this.full_Air_160_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_160_RadioButton
            // 
            this.full_Ice_160_RadioButton.AutoCheck = false;
            this.full_Ice_160_RadioButton.AutoSize = true;
            this.full_Ice_160_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_160_RadioButton.Name = "full_Ice_160_RadioButton";
            this.full_Ice_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_160_RadioButton.TabIndex = 264;
            this.full_Ice_160_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel80
            // 
            this.panel80.Controls.Add(this.full_Air_163_RadioButton);
            this.panel80.Controls.Add(this.full_Ice_163_RadioButton);
            this.panel80.Location = new System.Drawing.Point(859, 305);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(127, 25);
            this.panel80.TabIndex = 512;
            // 
            // full_Air_163_RadioButton
            // 
            this.full_Air_163_RadioButton.AutoCheck = false;
            this.full_Air_163_RadioButton.AutoSize = true;
            this.full_Air_163_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_163_RadioButton.Checked = true;
            this.full_Air_163_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_163_RadioButton.Name = "full_Air_163_RadioButton";
            this.full_Air_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_163_RadioButton.TabIndex = 266;
            this.full_Air_163_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_163_RadioButton
            // 
            this.full_Ice_163_RadioButton.AutoCheck = false;
            this.full_Ice_163_RadioButton.AutoSize = true;
            this.full_Ice_163_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_163_RadioButton.Name = "full_Ice_163_RadioButton";
            this.full_Ice_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_163_RadioButton.TabIndex = 264;
            this.full_Ice_163_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel81
            // 
            this.panel81.Controls.Add(this.full_Air_170_RadioButton);
            this.panel81.Controls.Add(this.full_Ice_170_RadioButton);
            this.panel81.Location = new System.Drawing.Point(859, 348);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(127, 25);
            this.panel81.TabIndex = 513;
            // 
            // full_Air_170_RadioButton
            // 
            this.full_Air_170_RadioButton.AutoCheck = false;
            this.full_Air_170_RadioButton.AutoSize = true;
            this.full_Air_170_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_170_RadioButton.Checked = true;
            this.full_Air_170_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_170_RadioButton.Name = "full_Air_170_RadioButton";
            this.full_Air_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_170_RadioButton.TabIndex = 266;
            this.full_Air_170_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_170_RadioButton
            // 
            this.full_Ice_170_RadioButton.AutoCheck = false;
            this.full_Ice_170_RadioButton.AutoSize = true;
            this.full_Ice_170_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_170_RadioButton.Name = "full_Ice_170_RadioButton";
            this.full_Ice_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_170_RadioButton.TabIndex = 264;
            this.full_Ice_170_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel82
            // 
            this.panel82.Controls.Add(this.full_Air_173_RadioButton);
            this.panel82.Controls.Add(this.full_Ice_173_RadioButton);
            this.panel82.Location = new System.Drawing.Point(859, 375);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(127, 25);
            this.panel82.TabIndex = 514;
            // 
            // full_Air_173_RadioButton
            // 
            this.full_Air_173_RadioButton.AutoCheck = false;
            this.full_Air_173_RadioButton.AutoSize = true;
            this.full_Air_173_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_173_RadioButton.Checked = true;
            this.full_Air_173_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_173_RadioButton.Name = "full_Air_173_RadioButton";
            this.full_Air_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_173_RadioButton.TabIndex = 266;
            this.full_Air_173_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_173_RadioButton
            // 
            this.full_Ice_173_RadioButton.AutoCheck = false;
            this.full_Ice_173_RadioButton.AutoSize = true;
            this.full_Ice_173_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_173_RadioButton.Name = "full_Ice_173_RadioButton";
            this.full_Ice_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_173_RadioButton.TabIndex = 264;
            this.full_Ice_173_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel83
            // 
            this.panel83.Controls.Add(this.full_Air_180_RadioButton);
            this.panel83.Controls.Add(this.full_Ice_180_RadioButton);
            this.panel83.Location = new System.Drawing.Point(859, 402);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(127, 25);
            this.panel83.TabIndex = 515;
            // 
            // full_Air_180_RadioButton
            // 
            this.full_Air_180_RadioButton.AutoCheck = false;
            this.full_Air_180_RadioButton.AutoSize = true;
            this.full_Air_180_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_180_RadioButton.Checked = true;
            this.full_Air_180_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_180_RadioButton.Name = "full_Air_180_RadioButton";
            this.full_Air_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_180_RadioButton.TabIndex = 266;
            this.full_Air_180_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_180_RadioButton
            // 
            this.full_Ice_180_RadioButton.AutoCheck = false;
            this.full_Ice_180_RadioButton.AutoSize = true;
            this.full_Ice_180_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_180_RadioButton.Name = "full_Ice_180_RadioButton";
            this.full_Ice_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_180_RadioButton.TabIndex = 264;
            this.full_Ice_180_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel84
            // 
            this.panel84.Controls.Add(this.full_Air_183_RadioButton);
            this.panel84.Controls.Add(this.full_Ice_183_RadioButton);
            this.panel84.Location = new System.Drawing.Point(859, 429);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(127, 25);
            this.panel84.TabIndex = 516;
            // 
            // full_Air_183_RadioButton
            // 
            this.full_Air_183_RadioButton.AutoCheck = false;
            this.full_Air_183_RadioButton.AutoSize = true;
            this.full_Air_183_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.full_Air_183_RadioButton.Checked = true;
            this.full_Air_183_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_183_RadioButton.Name = "full_Air_183_RadioButton";
            this.full_Air_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_183_RadioButton.TabIndex = 266;
            this.full_Air_183_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_183_RadioButton
            // 
            this.full_Ice_183_RadioButton.AutoCheck = false;
            this.full_Ice_183_RadioButton.AutoSize = true;
            this.full_Ice_183_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_183_RadioButton.Name = "full_Ice_183_RadioButton";
            this.full_Ice_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_183_RadioButton.TabIndex = 264;
            this.full_Ice_183_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel85
            // 
            this.panel85.Controls.Add(this.full_Air_190_RadioButton);
            this.panel85.Controls.Add(this.full_Ice_190_RadioButton);
            this.panel85.Location = new System.Drawing.Point(859, 456);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(127, 25);
            this.panel85.TabIndex = 517;
            // 
            // full_Air_190_RadioButton
            // 
            this.full_Air_190_RadioButton.AutoCheck = false;
            this.full_Air_190_RadioButton.AutoSize = true;
            this.full_Air_190_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_190_RadioButton.Name = "full_Air_190_RadioButton";
            this.full_Air_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_190_RadioButton.TabIndex = 266;
            this.full_Air_190_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_190_RadioButton
            // 
            this.full_Ice_190_RadioButton.AutoCheck = false;
            this.full_Ice_190_RadioButton.AutoSize = true;
            this.full_Ice_190_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_190_RadioButton.Name = "full_Ice_190_RadioButton";
            this.full_Ice_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_190_RadioButton.TabIndex = 264;
            this.full_Ice_190_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel86
            // 
            this.panel86.Controls.Add(this.full_Air_193_RadioButton);
            this.panel86.Controls.Add(this.full_Ice_193_RadioButton);
            this.panel86.Location = new System.Drawing.Point(859, 483);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(127, 25);
            this.panel86.TabIndex = 518;
            // 
            // full_Air_193_RadioButton
            // 
            this.full_Air_193_RadioButton.AutoCheck = false;
            this.full_Air_193_RadioButton.AutoSize = true;
            this.full_Air_193_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_193_RadioButton.Name = "full_Air_193_RadioButton";
            this.full_Air_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_193_RadioButton.TabIndex = 266;
            this.full_Air_193_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_193_RadioButton
            // 
            this.full_Ice_193_RadioButton.AutoCheck = false;
            this.full_Ice_193_RadioButton.AutoSize = true;
            this.full_Ice_193_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_193_RadioButton.Name = "full_Ice_193_RadioButton";
            this.full_Ice_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_193_RadioButton.TabIndex = 264;
            this.full_Ice_193_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel87
            // 
            this.panel87.Controls.Add(this.full_Air_200_RadioButton);
            this.panel87.Controls.Add(this.full_Ice_200_RadioButton);
            this.panel87.Location = new System.Drawing.Point(859, 510);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(127, 25);
            this.panel87.TabIndex = 519;
            // 
            // full_Air_200_RadioButton
            // 
            this.full_Air_200_RadioButton.AutoCheck = false;
            this.full_Air_200_RadioButton.AutoSize = true;
            this.full_Air_200_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_200_RadioButton.Name = "full_Air_200_RadioButton";
            this.full_Air_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_200_RadioButton.TabIndex = 266;
            this.full_Air_200_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_200_RadioButton
            // 
            this.full_Ice_200_RadioButton.AutoCheck = false;
            this.full_Ice_200_RadioButton.AutoSize = true;
            this.full_Ice_200_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_200_RadioButton.Name = "full_Ice_200_RadioButton";
            this.full_Ice_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_200_RadioButton.TabIndex = 264;
            this.full_Ice_200_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel88
            // 
            this.panel88.Controls.Add(this.full_Air_203_RadioButton);
            this.panel88.Controls.Add(this.full_Ice_203_RadioButton);
            this.panel88.Location = new System.Drawing.Point(859, 537);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(127, 25);
            this.panel88.TabIndex = 520;
            // 
            // full_Air_203_RadioButton
            // 
            this.full_Air_203_RadioButton.AutoCheck = false;
            this.full_Air_203_RadioButton.AutoSize = true;
            this.full_Air_203_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_203_RadioButton.Name = "full_Air_203_RadioButton";
            this.full_Air_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_203_RadioButton.TabIndex = 266;
            this.full_Air_203_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_203_RadioButton
            // 
            this.full_Ice_203_RadioButton.AutoCheck = false;
            this.full_Ice_203_RadioButton.AutoSize = true;
            this.full_Ice_203_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_203_RadioButton.Name = "full_Ice_203_RadioButton";
            this.full_Ice_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_203_RadioButton.TabIndex = 264;
            this.full_Ice_203_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel89
            // 
            this.panel89.Controls.Add(this.full_Air_210_RadioButton);
            this.panel89.Controls.Add(this.full_Ice_210_RadioButton);
            this.panel89.Location = new System.Drawing.Point(859, 580);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(127, 25);
            this.panel89.TabIndex = 521;
            // 
            // full_Air_210_RadioButton
            // 
            this.full_Air_210_RadioButton.AutoCheck = false;
            this.full_Air_210_RadioButton.AutoSize = true;
            this.full_Air_210_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_210_RadioButton.Name = "full_Air_210_RadioButton";
            this.full_Air_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_210_RadioButton.TabIndex = 266;
            this.full_Air_210_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_210_RadioButton
            // 
            this.full_Ice_210_RadioButton.AutoCheck = false;
            this.full_Ice_210_RadioButton.AutoSize = true;
            this.full_Ice_210_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_210_RadioButton.Name = "full_Ice_210_RadioButton";
            this.full_Ice_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_210_RadioButton.TabIndex = 264;
            this.full_Ice_210_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel90
            // 
            this.panel90.Controls.Add(this.full_Air_213_RadioButton);
            this.panel90.Controls.Add(this.full_Ice_213_RadioButton);
            this.panel90.Location = new System.Drawing.Point(859, 607);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(127, 25);
            this.panel90.TabIndex = 522;
            // 
            // full_Air_213_RadioButton
            // 
            this.full_Air_213_RadioButton.AutoCheck = false;
            this.full_Air_213_RadioButton.AutoSize = true;
            this.full_Air_213_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_213_RadioButton.Name = "full_Air_213_RadioButton";
            this.full_Air_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_213_RadioButton.TabIndex = 266;
            this.full_Air_213_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_213_RadioButton
            // 
            this.full_Ice_213_RadioButton.AutoCheck = false;
            this.full_Ice_213_RadioButton.AutoSize = true;
            this.full_Ice_213_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_213_RadioButton.Name = "full_Ice_213_RadioButton";
            this.full_Ice_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_213_RadioButton.TabIndex = 264;
            this.full_Ice_213_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel91
            // 
            this.panel91.Controls.Add(this.full_Air_220_RadioButton);
            this.panel91.Controls.Add(this.full_Ice_220_RadioButton);
            this.panel91.Location = new System.Drawing.Point(859, 634);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(127, 25);
            this.panel91.TabIndex = 523;
            // 
            // full_Air_220_RadioButton
            // 
            this.full_Air_220_RadioButton.AutoCheck = false;
            this.full_Air_220_RadioButton.AutoSize = true;
            this.full_Air_220_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_220_RadioButton.Name = "full_Air_220_RadioButton";
            this.full_Air_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_220_RadioButton.TabIndex = 266;
            this.full_Air_220_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_220_RadioButton
            // 
            this.full_Ice_220_RadioButton.AutoCheck = false;
            this.full_Ice_220_RadioButton.AutoSize = true;
            this.full_Ice_220_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_220_RadioButton.Name = "full_Ice_220_RadioButton";
            this.full_Ice_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_220_RadioButton.TabIndex = 264;
            this.full_Ice_220_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel92
            // 
            this.panel92.Controls.Add(this.full_Air_223_RadioButton);
            this.panel92.Controls.Add(this.full_Ice_223_RadioButton);
            this.panel92.Location = new System.Drawing.Point(859, 661);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(127, 25);
            this.panel92.TabIndex = 524;
            // 
            // full_Air_223_RadioButton
            // 
            this.full_Air_223_RadioButton.AutoCheck = false;
            this.full_Air_223_RadioButton.AutoSize = true;
            this.full_Air_223_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_223_RadioButton.Name = "full_Air_223_RadioButton";
            this.full_Air_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_223_RadioButton.TabIndex = 266;
            this.full_Air_223_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_223_RadioButton
            // 
            this.full_Ice_223_RadioButton.AutoCheck = false;
            this.full_Ice_223_RadioButton.AutoSize = true;
            this.full_Ice_223_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_223_RadioButton.Name = "full_Ice_223_RadioButton";
            this.full_Ice_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_223_RadioButton.TabIndex = 264;
            this.full_Ice_223_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_230_RadioButton
            // 
            this.full_Ice_230_RadioButton.AutoCheck = false;
            this.full_Ice_230_RadioButton.AutoSize = true;
            this.full_Ice_230_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_230_RadioButton.Checked = true;
            this.full_Ice_230_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_230_RadioButton.Name = "full_Ice_230_RadioButton";
            this.full_Ice_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_230_RadioButton.TabIndex = 264;
            this.full_Ice_230_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Air_230_RadioButton
            // 
            this.full_Air_230_RadioButton.AutoCheck = false;
            this.full_Air_230_RadioButton.AutoSize = true;
            this.full_Air_230_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_230_RadioButton.Name = "full_Air_230_RadioButton";
            this.full_Air_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_230_RadioButton.TabIndex = 266;
            this.full_Air_230_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel93
            // 
            this.panel93.Controls.Add(this.full_Air_230_RadioButton);
            this.panel93.Controls.Add(this.full_Ice_230_RadioButton);
            this.panel93.Location = new System.Drawing.Point(859, 688);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(127, 25);
            this.panel93.TabIndex = 525;
            // 
            // panel94
            // 
            this.panel94.Controls.Add(this.full_Air_233_RadioButton);
            this.panel94.Controls.Add(this.full_Ice_233_RadioButton);
            this.panel94.Location = new System.Drawing.Point(859, 715);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(127, 25);
            this.panel94.TabIndex = 526;
            // 
            // full_Air_233_RadioButton
            // 
            this.full_Air_233_RadioButton.AutoCheck = false;
            this.full_Air_233_RadioButton.AutoSize = true;
            this.full_Air_233_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_233_RadioButton.Name = "full_Air_233_RadioButton";
            this.full_Air_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_233_RadioButton.TabIndex = 266;
            this.full_Air_233_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_233_RadioButton
            // 
            this.full_Ice_233_RadioButton.AutoCheck = false;
            this.full_Ice_233_RadioButton.AutoSize = true;
            this.full_Ice_233_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_233_RadioButton.Checked = true;
            this.full_Ice_233_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_233_RadioButton.Name = "full_Ice_233_RadioButton";
            this.full_Ice_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_233_RadioButton.TabIndex = 264;
            this.full_Ice_233_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel95
            // 
            this.panel95.Controls.Add(this.full_Air_240_RadioButton);
            this.panel95.Controls.Add(this.full_Ice_240_RadioButton);
            this.panel95.Location = new System.Drawing.Point(859, 742);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(127, 25);
            this.panel95.TabIndex = 527;
            // 
            // full_Air_240_RadioButton
            // 
            this.full_Air_240_RadioButton.AutoCheck = false;
            this.full_Air_240_RadioButton.AutoSize = true;
            this.full_Air_240_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_240_RadioButton.Name = "full_Air_240_RadioButton";
            this.full_Air_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_240_RadioButton.TabIndex = 266;
            this.full_Air_240_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_240_RadioButton
            // 
            this.full_Ice_240_RadioButton.AutoCheck = false;
            this.full_Ice_240_RadioButton.AutoSize = true;
            this.full_Ice_240_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_240_RadioButton.Checked = true;
            this.full_Ice_240_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_240_RadioButton.Name = "full_Ice_240_RadioButton";
            this.full_Ice_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_240_RadioButton.TabIndex = 264;
            this.full_Ice_240_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel96
            // 
            this.panel96.Controls.Add(this.full_Air_003_RadioButton);
            this.panel96.Controls.Add(this.full_Ice_003_RadioButton);
            this.panel96.Location = new System.Drawing.Point(859, 769);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(127, 25);
            this.panel96.TabIndex = 528;
            // 
            // full_Air_003_RadioButton
            // 
            this.full_Air_003_RadioButton.AutoCheck = false;
            this.full_Air_003_RadioButton.AutoSize = true;
            this.full_Air_003_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.full_Air_003_RadioButton.Name = "full_Air_003_RadioButton";
            this.full_Air_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Air_003_RadioButton.TabIndex = 266;
            this.full_Air_003_RadioButton.UseVisualStyleBackColor = false;
            this.full_Air_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // full_Ice_003_RadioButton
            // 
            this.full_Ice_003_RadioButton.AutoCheck = false;
            this.full_Ice_003_RadioButton.AutoSize = true;
            this.full_Ice_003_RadioButton.BackColor = System.Drawing.Color.Blue;
            this.full_Ice_003_RadioButton.Checked = true;
            this.full_Ice_003_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.full_Ice_003_RadioButton.Name = "full_Ice_003_RadioButton";
            this.full_Ice_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.full_Ice_003_RadioButton.TabIndex = 264;
            this.full_Ice_003_RadioButton.UseVisualStyleBackColor = false;
            this.full_Ice_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel97
            // 
            this.panel97.Controls.Add(this.part_Air_130_RadioButton);
            this.panel97.Controls.Add(this.part_Ice_130_RadioButton);
            this.panel97.Location = new System.Drawing.Point(1013, 116);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(127, 25);
            this.panel97.TabIndex = 529;
            // 
            // part_Air_130_RadioButton
            // 
            this.part_Air_130_RadioButton.AutoCheck = false;
            this.part_Air_130_RadioButton.AutoSize = true;
            this.part_Air_130_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_130_RadioButton.Checked = true;
            this.part_Air_130_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_130_RadioButton.Name = "part_Air_130_RadioButton";
            this.part_Air_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_130_RadioButton.TabIndex = 266;
            this.part_Air_130_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_130_RadioButton
            // 
            this.part_Ice_130_RadioButton.AutoCheck = false;
            this.part_Ice_130_RadioButton.AutoSize = true;
            this.part_Ice_130_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_130_RadioButton.Name = "part_Ice_130_RadioButton";
            this.part_Ice_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_130_RadioButton.TabIndex = 264;
            this.part_Ice_130_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel98
            // 
            this.panel98.Controls.Add(this.part_Air_133_RadioButton);
            this.panel98.Controls.Add(this.part_Ice_133_RadioButton);
            this.panel98.Location = new System.Drawing.Point(1013, 143);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(127, 25);
            this.panel98.TabIndex = 530;
            // 
            // part_Air_133_RadioButton
            // 
            this.part_Air_133_RadioButton.AutoCheck = false;
            this.part_Air_133_RadioButton.AutoSize = true;
            this.part_Air_133_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_133_RadioButton.Checked = true;
            this.part_Air_133_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_133_RadioButton.Name = "part_Air_133_RadioButton";
            this.part_Air_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_133_RadioButton.TabIndex = 266;
            this.part_Air_133_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_133_RadioButton
            // 
            this.part_Ice_133_RadioButton.AutoCheck = false;
            this.part_Ice_133_RadioButton.AutoSize = true;
            this.part_Ice_133_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_133_RadioButton.Name = "part_Ice_133_RadioButton";
            this.part_Ice_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_133_RadioButton.TabIndex = 264;
            this.part_Ice_133_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel99
            // 
            this.panel99.Controls.Add(this.part_Air_140_RadioButton);
            this.panel99.Controls.Add(this.part_Ice_140_RadioButton);
            this.panel99.Location = new System.Drawing.Point(1013, 170);
            this.panel99.Name = "panel99";
            this.panel99.Size = new System.Drawing.Size(127, 25);
            this.panel99.TabIndex = 531;
            // 
            // part_Air_140_RadioButton
            // 
            this.part_Air_140_RadioButton.AutoCheck = false;
            this.part_Air_140_RadioButton.AutoSize = true;
            this.part_Air_140_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_140_RadioButton.Checked = true;
            this.part_Air_140_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_140_RadioButton.Name = "part_Air_140_RadioButton";
            this.part_Air_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_140_RadioButton.TabIndex = 266;
            this.part_Air_140_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_140_RadioButton
            // 
            this.part_Ice_140_RadioButton.AutoCheck = false;
            this.part_Ice_140_RadioButton.AutoSize = true;
            this.part_Ice_140_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_140_RadioButton.Name = "part_Ice_140_RadioButton";
            this.part_Ice_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_140_RadioButton.TabIndex = 264;
            this.part_Ice_140_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel100
            // 
            this.panel100.Controls.Add(this.part_Air_143_RadioButton);
            this.panel100.Controls.Add(this.part_Ice_143_RadioButton);
            this.panel100.Location = new System.Drawing.Point(1013, 197);
            this.panel100.Name = "panel100";
            this.panel100.Size = new System.Drawing.Size(127, 25);
            this.panel100.TabIndex = 532;
            // 
            // part_Air_143_RadioButton
            // 
            this.part_Air_143_RadioButton.AutoCheck = false;
            this.part_Air_143_RadioButton.AutoSize = true;
            this.part_Air_143_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_143_RadioButton.Checked = true;
            this.part_Air_143_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_143_RadioButton.Name = "part_Air_143_RadioButton";
            this.part_Air_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_143_RadioButton.TabIndex = 266;
            this.part_Air_143_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_143_RadioButton
            // 
            this.part_Ice_143_RadioButton.AutoCheck = false;
            this.part_Ice_143_RadioButton.AutoSize = true;
            this.part_Ice_143_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_143_RadioButton.Name = "part_Ice_143_RadioButton";
            this.part_Ice_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_143_RadioButton.TabIndex = 264;
            this.part_Ice_143_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel101
            // 
            this.panel101.Controls.Add(this.part_Air_150_RadioButton);
            this.panel101.Controls.Add(this.part_Ice_150_RadioButton);
            this.panel101.Location = new System.Drawing.Point(1013, 224);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(127, 25);
            this.panel101.TabIndex = 533;
            // 
            // part_Air_150_RadioButton
            // 
            this.part_Air_150_RadioButton.AutoCheck = false;
            this.part_Air_150_RadioButton.AutoSize = true;
            this.part_Air_150_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_150_RadioButton.Checked = true;
            this.part_Air_150_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_150_RadioButton.Name = "part_Air_150_RadioButton";
            this.part_Air_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_150_RadioButton.TabIndex = 266;
            this.part_Air_150_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_150_RadioButton
            // 
            this.part_Ice_150_RadioButton.AutoCheck = false;
            this.part_Ice_150_RadioButton.AutoSize = true;
            this.part_Ice_150_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_150_RadioButton.Name = "part_Ice_150_RadioButton";
            this.part_Ice_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_150_RadioButton.TabIndex = 264;
            this.part_Ice_150_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel102
            // 
            this.panel102.Controls.Add(this.part_Air_153_RadioButton);
            this.panel102.Controls.Add(this.part_Ice_153_RadioButton);
            this.panel102.Location = new System.Drawing.Point(1013, 251);
            this.panel102.Name = "panel102";
            this.panel102.Size = new System.Drawing.Size(127, 25);
            this.panel102.TabIndex = 534;
            // 
            // part_Air_153_RadioButton
            // 
            this.part_Air_153_RadioButton.AutoCheck = false;
            this.part_Air_153_RadioButton.AutoSize = true;
            this.part_Air_153_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_153_RadioButton.Checked = true;
            this.part_Air_153_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_153_RadioButton.Name = "part_Air_153_RadioButton";
            this.part_Air_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_153_RadioButton.TabIndex = 266;
            this.part_Air_153_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_153_RadioButton
            // 
            this.part_Ice_153_RadioButton.AutoCheck = false;
            this.part_Ice_153_RadioButton.AutoSize = true;
            this.part_Ice_153_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_153_RadioButton.Name = "part_Ice_153_RadioButton";
            this.part_Ice_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_153_RadioButton.TabIndex = 264;
            this.part_Ice_153_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel103
            // 
            this.panel103.Controls.Add(this.part_Air_160_RadioButton);
            this.panel103.Controls.Add(this.part_Ice_160_RadioButton);
            this.panel103.Location = new System.Drawing.Point(1013, 278);
            this.panel103.Name = "panel103";
            this.panel103.Size = new System.Drawing.Size(127, 25);
            this.panel103.TabIndex = 535;
            // 
            // part_Air_160_RadioButton
            // 
            this.part_Air_160_RadioButton.AutoCheck = false;
            this.part_Air_160_RadioButton.AutoSize = true;
            this.part_Air_160_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_160_RadioButton.Checked = true;
            this.part_Air_160_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_160_RadioButton.Name = "part_Air_160_RadioButton";
            this.part_Air_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_160_RadioButton.TabIndex = 266;
            this.part_Air_160_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_160_RadioButton
            // 
            this.part_Ice_160_RadioButton.AutoCheck = false;
            this.part_Ice_160_RadioButton.AutoSize = true;
            this.part_Ice_160_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_160_RadioButton.Name = "part_Ice_160_RadioButton";
            this.part_Ice_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_160_RadioButton.TabIndex = 264;
            this.part_Ice_160_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel104
            // 
            this.panel104.Controls.Add(this.part_Air_163_RadioButton);
            this.panel104.Controls.Add(this.part_Ice_163_RadioButton);
            this.panel104.Location = new System.Drawing.Point(1013, 305);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(127, 25);
            this.panel104.TabIndex = 536;
            // 
            // part_Air_163_RadioButton
            // 
            this.part_Air_163_RadioButton.AutoCheck = false;
            this.part_Air_163_RadioButton.AutoSize = true;
            this.part_Air_163_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_163_RadioButton.Checked = true;
            this.part_Air_163_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_163_RadioButton.Name = "part_Air_163_RadioButton";
            this.part_Air_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_163_RadioButton.TabIndex = 266;
            this.part_Air_163_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_163_RadioButton
            // 
            this.part_Ice_163_RadioButton.AutoCheck = false;
            this.part_Ice_163_RadioButton.AutoSize = true;
            this.part_Ice_163_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_163_RadioButton.Name = "part_Ice_163_RadioButton";
            this.part_Ice_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_163_RadioButton.TabIndex = 264;
            this.part_Ice_163_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel105
            // 
            this.panel105.Controls.Add(this.part_Air_170_RadioButton);
            this.panel105.Controls.Add(this.part_Ice_170_RadioButton);
            this.panel105.Location = new System.Drawing.Point(1013, 348);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(127, 25);
            this.panel105.TabIndex = 537;
            // 
            // part_Air_170_RadioButton
            // 
            this.part_Air_170_RadioButton.AutoCheck = false;
            this.part_Air_170_RadioButton.AutoSize = true;
            this.part_Air_170_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_170_RadioButton.Checked = true;
            this.part_Air_170_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_170_RadioButton.Name = "part_Air_170_RadioButton";
            this.part_Air_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_170_RadioButton.TabIndex = 266;
            this.part_Air_170_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_170_RadioButton
            // 
            this.part_Ice_170_RadioButton.AutoCheck = false;
            this.part_Ice_170_RadioButton.AutoSize = true;
            this.part_Ice_170_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_170_RadioButton.Name = "part_Ice_170_RadioButton";
            this.part_Ice_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_170_RadioButton.TabIndex = 264;
            this.part_Ice_170_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel106
            // 
            this.panel106.Controls.Add(this.part_Air_173_RadioButton);
            this.panel106.Controls.Add(this.part_Ice_173_RadioButton);
            this.panel106.Location = new System.Drawing.Point(1013, 375);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(127, 25);
            this.panel106.TabIndex = 538;
            // 
            // part_Air_173_RadioButton
            // 
            this.part_Air_173_RadioButton.AutoCheck = false;
            this.part_Air_173_RadioButton.AutoSize = true;
            this.part_Air_173_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_173_RadioButton.Checked = true;
            this.part_Air_173_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_173_RadioButton.Name = "part_Air_173_RadioButton";
            this.part_Air_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_173_RadioButton.TabIndex = 266;
            this.part_Air_173_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_173_RadioButton
            // 
            this.part_Ice_173_RadioButton.AutoCheck = false;
            this.part_Ice_173_RadioButton.AutoSize = true;
            this.part_Ice_173_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_173_RadioButton.Name = "part_Ice_173_RadioButton";
            this.part_Ice_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_173_RadioButton.TabIndex = 264;
            this.part_Ice_173_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel107
            // 
            this.panel107.Controls.Add(this.part_Air_180_RadioButton);
            this.panel107.Controls.Add(this.part_Ice_180_RadioButton);
            this.panel107.Location = new System.Drawing.Point(1013, 402);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(127, 25);
            this.panel107.TabIndex = 539;
            // 
            // part_Air_180_RadioButton
            // 
            this.part_Air_180_RadioButton.AutoCheck = false;
            this.part_Air_180_RadioButton.AutoSize = true;
            this.part_Air_180_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_180_RadioButton.Checked = true;
            this.part_Air_180_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_180_RadioButton.Name = "part_Air_180_RadioButton";
            this.part_Air_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_180_RadioButton.TabIndex = 266;
            this.part_Air_180_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_180_RadioButton
            // 
            this.part_Ice_180_RadioButton.AutoCheck = false;
            this.part_Ice_180_RadioButton.AutoSize = true;
            this.part_Ice_180_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_180_RadioButton.Name = "part_Ice_180_RadioButton";
            this.part_Ice_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_180_RadioButton.TabIndex = 264;
            this.part_Ice_180_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel108
            // 
            this.panel108.Controls.Add(this.part_Air_183_RadioButton);
            this.panel108.Controls.Add(this.part_Ice_183_RadioButton);
            this.panel108.Location = new System.Drawing.Point(1013, 429);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(127, 25);
            this.panel108.TabIndex = 540;
            // 
            // part_Air_183_RadioButton
            // 
            this.part_Air_183_RadioButton.AutoCheck = false;
            this.part_Air_183_RadioButton.AutoSize = true;
            this.part_Air_183_RadioButton.BackColor = System.Drawing.Color.Teal;
            this.part_Air_183_RadioButton.Checked = true;
            this.part_Air_183_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_183_RadioButton.Name = "part_Air_183_RadioButton";
            this.part_Air_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_183_RadioButton.TabIndex = 266;
            this.part_Air_183_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_183_RadioButton
            // 
            this.part_Ice_183_RadioButton.AutoCheck = false;
            this.part_Ice_183_RadioButton.AutoSize = true;
            this.part_Ice_183_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_183_RadioButton.Name = "part_Ice_183_RadioButton";
            this.part_Ice_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_183_RadioButton.TabIndex = 264;
            this.part_Ice_183_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel109
            // 
            this.panel109.Controls.Add(this.part_Air_190_RadioButton);
            this.panel109.Controls.Add(this.part_Ice_190_RadioButton);
            this.panel109.Location = new System.Drawing.Point(1013, 456);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(127, 25);
            this.panel109.TabIndex = 541;
            // 
            // part_Air_190_RadioButton
            // 
            this.part_Air_190_RadioButton.AutoCheck = false;
            this.part_Air_190_RadioButton.AutoSize = true;
            this.part_Air_190_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_190_RadioButton.Name = "part_Air_190_RadioButton";
            this.part_Air_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_190_RadioButton.TabIndex = 266;
            this.part_Air_190_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_190_RadioButton
            // 
            this.part_Ice_190_RadioButton.AutoCheck = false;
            this.part_Ice_190_RadioButton.AutoSize = true;
            this.part_Ice_190_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_190_RadioButton.Name = "part_Ice_190_RadioButton";
            this.part_Ice_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_190_RadioButton.TabIndex = 264;
            this.part_Ice_190_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel110
            // 
            this.panel110.Controls.Add(this.part_Air_193_RadioButton);
            this.panel110.Controls.Add(this.part_Ice_193_RadioButton);
            this.panel110.Location = new System.Drawing.Point(1013, 483);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(127, 25);
            this.panel110.TabIndex = 542;
            // 
            // part_Air_193_RadioButton
            // 
            this.part_Air_193_RadioButton.AutoCheck = false;
            this.part_Air_193_RadioButton.AutoSize = true;
            this.part_Air_193_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_193_RadioButton.Name = "part_Air_193_RadioButton";
            this.part_Air_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_193_RadioButton.TabIndex = 266;
            this.part_Air_193_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_193_RadioButton
            // 
            this.part_Ice_193_RadioButton.AutoCheck = false;
            this.part_Ice_193_RadioButton.AutoSize = true;
            this.part_Ice_193_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_193_RadioButton.Name = "part_Ice_193_RadioButton";
            this.part_Ice_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_193_RadioButton.TabIndex = 264;
            this.part_Ice_193_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel111
            // 
            this.panel111.Controls.Add(this.part_Air_200_RadioButton);
            this.panel111.Controls.Add(this.part_Ice_200_RadioButton);
            this.panel111.Location = new System.Drawing.Point(1013, 510);
            this.panel111.Name = "panel111";
            this.panel111.Size = new System.Drawing.Size(127, 25);
            this.panel111.TabIndex = 543;
            // 
            // part_Air_200_RadioButton
            // 
            this.part_Air_200_RadioButton.AutoCheck = false;
            this.part_Air_200_RadioButton.AutoSize = true;
            this.part_Air_200_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_200_RadioButton.Name = "part_Air_200_RadioButton";
            this.part_Air_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_200_RadioButton.TabIndex = 266;
            this.part_Air_200_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_200_RadioButton
            // 
            this.part_Ice_200_RadioButton.AutoCheck = false;
            this.part_Ice_200_RadioButton.AutoSize = true;
            this.part_Ice_200_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_200_RadioButton.Name = "part_Ice_200_RadioButton";
            this.part_Ice_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_200_RadioButton.TabIndex = 264;
            this.part_Ice_200_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel112
            // 
            this.panel112.Controls.Add(this.part_Air_203_RadioButton);
            this.panel112.Controls.Add(this.part_Ice_203_RadioButton);
            this.panel112.Location = new System.Drawing.Point(1013, 537);
            this.panel112.Name = "panel112";
            this.panel112.Size = new System.Drawing.Size(127, 25);
            this.panel112.TabIndex = 544;
            // 
            // part_Air_203_RadioButton
            // 
            this.part_Air_203_RadioButton.AutoCheck = false;
            this.part_Air_203_RadioButton.AutoSize = true;
            this.part_Air_203_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_203_RadioButton.Name = "part_Air_203_RadioButton";
            this.part_Air_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_203_RadioButton.TabIndex = 266;
            this.part_Air_203_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_203_RadioButton
            // 
            this.part_Ice_203_RadioButton.AutoCheck = false;
            this.part_Ice_203_RadioButton.AutoSize = true;
            this.part_Ice_203_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_203_RadioButton.Name = "part_Ice_203_RadioButton";
            this.part_Ice_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_203_RadioButton.TabIndex = 264;
            this.part_Ice_203_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel113
            // 
            this.panel113.Controls.Add(this.part_Air_210_RadioButton);
            this.panel113.Controls.Add(this.part_Ice_210_RadioButton);
            this.panel113.Location = new System.Drawing.Point(1013, 580);
            this.panel113.Name = "panel113";
            this.panel113.Size = new System.Drawing.Size(127, 25);
            this.panel113.TabIndex = 545;
            // 
            // part_Air_210_RadioButton
            // 
            this.part_Air_210_RadioButton.AutoCheck = false;
            this.part_Air_210_RadioButton.AutoSize = true;
            this.part_Air_210_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_210_RadioButton.Name = "part_Air_210_RadioButton";
            this.part_Air_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_210_RadioButton.TabIndex = 266;
            this.part_Air_210_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_210_RadioButton
            // 
            this.part_Ice_210_RadioButton.AutoCheck = false;
            this.part_Ice_210_RadioButton.AutoSize = true;
            this.part_Ice_210_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_210_RadioButton.Name = "part_Ice_210_RadioButton";
            this.part_Ice_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_210_RadioButton.TabIndex = 264;
            this.part_Ice_210_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel114
            // 
            this.panel114.Controls.Add(this.part_Air_213_RadioButton);
            this.panel114.Controls.Add(this.part_Ice_213_RadioButton);
            this.panel114.Location = new System.Drawing.Point(1013, 607);
            this.panel114.Name = "panel114";
            this.panel114.Size = new System.Drawing.Size(127, 25);
            this.panel114.TabIndex = 546;
            // 
            // part_Air_213_RadioButton
            // 
            this.part_Air_213_RadioButton.AutoCheck = false;
            this.part_Air_213_RadioButton.AutoSize = true;
            this.part_Air_213_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_213_RadioButton.Name = "part_Air_213_RadioButton";
            this.part_Air_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_213_RadioButton.TabIndex = 266;
            this.part_Air_213_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_213_RadioButton
            // 
            this.part_Ice_213_RadioButton.AutoCheck = false;
            this.part_Ice_213_RadioButton.AutoSize = true;
            this.part_Ice_213_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_213_RadioButton.Name = "part_Ice_213_RadioButton";
            this.part_Ice_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_213_RadioButton.TabIndex = 264;
            this.part_Ice_213_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel115
            // 
            this.panel115.Controls.Add(this.part_Air_220_RadioButton);
            this.panel115.Controls.Add(this.part_Ice_220_RadioButton);
            this.panel115.Location = new System.Drawing.Point(1013, 634);
            this.panel115.Name = "panel115";
            this.panel115.Size = new System.Drawing.Size(127, 25);
            this.panel115.TabIndex = 547;
            // 
            // part_Air_220_RadioButton
            // 
            this.part_Air_220_RadioButton.AutoCheck = false;
            this.part_Air_220_RadioButton.AutoSize = true;
            this.part_Air_220_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_220_RadioButton.Name = "part_Air_220_RadioButton";
            this.part_Air_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_220_RadioButton.TabIndex = 266;
            this.part_Air_220_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_220_RadioButton
            // 
            this.part_Ice_220_RadioButton.AutoCheck = false;
            this.part_Ice_220_RadioButton.AutoSize = true;
            this.part_Ice_220_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_220_RadioButton.Name = "part_Ice_220_RadioButton";
            this.part_Ice_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_220_RadioButton.TabIndex = 264;
            this.part_Ice_220_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel116
            // 
            this.panel116.Controls.Add(this.part_Air_223_RadioButton);
            this.panel116.Controls.Add(this.part_Ice_223_RadioButton);
            this.panel116.Location = new System.Drawing.Point(1013, 661);
            this.panel116.Name = "panel116";
            this.panel116.Size = new System.Drawing.Size(127, 25);
            this.panel116.TabIndex = 548;
            // 
            // part_Air_223_RadioButton
            // 
            this.part_Air_223_RadioButton.AutoCheck = false;
            this.part_Air_223_RadioButton.AutoSize = true;
            this.part_Air_223_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_223_RadioButton.Name = "part_Air_223_RadioButton";
            this.part_Air_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_223_RadioButton.TabIndex = 266;
            this.part_Air_223_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_223_RadioButton
            // 
            this.part_Ice_223_RadioButton.AutoCheck = false;
            this.part_Ice_223_RadioButton.AutoSize = true;
            this.part_Ice_223_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_223_RadioButton.Name = "part_Ice_223_RadioButton";
            this.part_Ice_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_223_RadioButton.TabIndex = 264;
            this.part_Ice_223_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel117
            // 
            this.panel117.Controls.Add(this.part_Air_230_RadioButton);
            this.panel117.Controls.Add(this.part_Ice_230_RadioButton);
            this.panel117.Location = new System.Drawing.Point(1013, 688);
            this.panel117.Name = "panel117";
            this.panel117.Size = new System.Drawing.Size(127, 25);
            this.panel117.TabIndex = 549;
            // 
            // part_Air_230_RadioButton
            // 
            this.part_Air_230_RadioButton.AutoCheck = false;
            this.part_Air_230_RadioButton.AutoSize = true;
            this.part_Air_230_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_230_RadioButton.Name = "part_Air_230_RadioButton";
            this.part_Air_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_230_RadioButton.TabIndex = 266;
            this.part_Air_230_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_230_RadioButton
            // 
            this.part_Ice_230_RadioButton.AutoCheck = false;
            this.part_Ice_230_RadioButton.AutoSize = true;
            this.part_Ice_230_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_230_RadioButton.Checked = true;
            this.part_Ice_230_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_230_RadioButton.Name = "part_Ice_230_RadioButton";
            this.part_Ice_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_230_RadioButton.TabIndex = 264;
            this.part_Ice_230_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel118
            // 
            this.panel118.Controls.Add(this.part_Air_233_RadioButton);
            this.panel118.Controls.Add(this.part_Ice_233_RadioButton);
            this.panel118.Location = new System.Drawing.Point(1013, 715);
            this.panel118.Name = "panel118";
            this.panel118.Size = new System.Drawing.Size(127, 25);
            this.panel118.TabIndex = 550;
            // 
            // part_Air_233_RadioButton
            // 
            this.part_Air_233_RadioButton.AutoCheck = false;
            this.part_Air_233_RadioButton.AutoSize = true;
            this.part_Air_233_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_233_RadioButton.Name = "part_Air_233_RadioButton";
            this.part_Air_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_233_RadioButton.TabIndex = 266;
            this.part_Air_233_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_233_RadioButton
            // 
            this.part_Ice_233_RadioButton.AutoCheck = false;
            this.part_Ice_233_RadioButton.AutoSize = true;
            this.part_Ice_233_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_233_RadioButton.Checked = true;
            this.part_Ice_233_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_233_RadioButton.Name = "part_Ice_233_RadioButton";
            this.part_Ice_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_233_RadioButton.TabIndex = 264;
            this.part_Ice_233_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel119
            // 
            this.panel119.Controls.Add(this.part_Air_240_RadioButton);
            this.panel119.Controls.Add(this.part_Ice_240_RadioButton);
            this.panel119.Location = new System.Drawing.Point(1013, 742);
            this.panel119.Name = "panel119";
            this.panel119.Size = new System.Drawing.Size(127, 25);
            this.panel119.TabIndex = 551;
            // 
            // part_Air_240_RadioButton
            // 
            this.part_Air_240_RadioButton.AutoCheck = false;
            this.part_Air_240_RadioButton.AutoSize = true;
            this.part_Air_240_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_240_RadioButton.Name = "part_Air_240_RadioButton";
            this.part_Air_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_240_RadioButton.TabIndex = 266;
            this.part_Air_240_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_240_RadioButton
            // 
            this.part_Ice_240_RadioButton.AutoCheck = false;
            this.part_Ice_240_RadioButton.AutoSize = true;
            this.part_Ice_240_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_240_RadioButton.Checked = true;
            this.part_Ice_240_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_240_RadioButton.Name = "part_Ice_240_RadioButton";
            this.part_Ice_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_240_RadioButton.TabIndex = 264;
            this.part_Ice_240_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel120
            // 
            this.panel120.Controls.Add(this.part_Air_003_RadioButton);
            this.panel120.Controls.Add(this.part_Ice_003_RadioButton);
            this.panel120.Location = new System.Drawing.Point(1013, 769);
            this.panel120.Name = "panel120";
            this.panel120.Size = new System.Drawing.Size(127, 25);
            this.panel120.TabIndex = 552;
            // 
            // part_Air_003_RadioButton
            // 
            this.part_Air_003_RadioButton.AutoCheck = false;
            this.part_Air_003_RadioButton.AutoSize = true;
            this.part_Air_003_RadioButton.Location = new System.Drawing.Point(88, 3);
            this.part_Air_003_RadioButton.Name = "part_Air_003_RadioButton";
            this.part_Air_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Air_003_RadioButton.TabIndex = 266;
            this.part_Air_003_RadioButton.UseVisualStyleBackColor = false;
            this.part_Air_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // part_Ice_003_RadioButton
            // 
            this.part_Ice_003_RadioButton.AutoCheck = false;
            this.part_Ice_003_RadioButton.AutoSize = true;
            this.part_Ice_003_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.part_Ice_003_RadioButton.Checked = true;
            this.part_Ice_003_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.part_Ice_003_RadioButton.Name = "part_Ice_003_RadioButton";
            this.part_Ice_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.part_Ice_003_RadioButton.TabIndex = 264;
            this.part_Ice_003_RadioButton.UseVisualStyleBackColor = false;
            this.part_Ice_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel121
            // 
            this.panel121.Controls.Add(this.trough_130_RadioButton);
            this.panel121.Controls.Add(this.flat_130_RadioButton);
            this.panel121.Controls.Add(this.crest_130_RadioButton);
            this.panel121.Location = new System.Drawing.Point(1183, 116);
            this.panel121.Name = "panel121";
            this.panel121.Size = new System.Drawing.Size(188, 25);
            this.panel121.TabIndex = 553;
            // 
            // trough_130_RadioButton
            // 
            this.trough_130_RadioButton.AutoCheck = false;
            this.trough_130_RadioButton.AutoSize = true;
            this.trough_130_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_130_RadioButton.Name = "trough_130_RadioButton";
            this.trough_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_130_RadioButton.TabIndex = 482;
            this.trough_130_RadioButton.UseVisualStyleBackColor = false;
            this.trough_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_130_RadioButton
            // 
            this.flat_130_RadioButton.AutoCheck = false;
            this.flat_130_RadioButton.AutoSize = true;
            this.flat_130_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_130_RadioButton.Name = "flat_130_RadioButton";
            this.flat_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_130_RadioButton.TabIndex = 266;
            this.flat_130_RadioButton.UseVisualStyleBackColor = false;
            this.flat_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_130_RadioButton
            // 
            this.crest_130_RadioButton.AutoCheck = false;
            this.crest_130_RadioButton.AutoSize = true;
            this.crest_130_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_130_RadioButton.Checked = true;
            this.crest_130_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_130_RadioButton.Name = "crest_130_RadioButton";
            this.crest_130_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_130_RadioButton.TabIndex = 264;
            this.crest_130_RadioButton.UseVisualStyleBackColor = false;
            this.crest_130_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel122
            // 
            this.panel122.Controls.Add(this.trough_133_RadioButton);
            this.panel122.Controls.Add(this.flat_133_RadioButton);
            this.panel122.Controls.Add(this.crest_133_RadioButton);
            this.panel122.Location = new System.Drawing.Point(1183, 143);
            this.panel122.Name = "panel122";
            this.panel122.Size = new System.Drawing.Size(188, 25);
            this.panel122.TabIndex = 554;
            // 
            // trough_133_RadioButton
            // 
            this.trough_133_RadioButton.AutoCheck = false;
            this.trough_133_RadioButton.AutoSize = true;
            this.trough_133_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_133_RadioButton.Name = "trough_133_RadioButton";
            this.trough_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_133_RadioButton.TabIndex = 482;
            this.trough_133_RadioButton.UseVisualStyleBackColor = false;
            this.trough_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_133_RadioButton
            // 
            this.flat_133_RadioButton.AutoCheck = false;
            this.flat_133_RadioButton.AutoSize = true;
            this.flat_133_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_133_RadioButton.Name = "flat_133_RadioButton";
            this.flat_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_133_RadioButton.TabIndex = 266;
            this.flat_133_RadioButton.UseVisualStyleBackColor = false;
            this.flat_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_133_RadioButton
            // 
            this.crest_133_RadioButton.AutoCheck = false;
            this.crest_133_RadioButton.AutoSize = true;
            this.crest_133_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_133_RadioButton.Checked = true;
            this.crest_133_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_133_RadioButton.Name = "crest_133_RadioButton";
            this.crest_133_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_133_RadioButton.TabIndex = 264;
            this.crest_133_RadioButton.UseVisualStyleBackColor = false;
            this.crest_133_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel123
            // 
            this.panel123.Controls.Add(this.trough_140_RadioButton);
            this.panel123.Controls.Add(this.flat_140_RadioButton);
            this.panel123.Controls.Add(this.crest_140_RadioButton);
            this.panel123.Location = new System.Drawing.Point(1183, 170);
            this.panel123.Name = "panel123";
            this.panel123.Size = new System.Drawing.Size(188, 25);
            this.panel123.TabIndex = 555;
            // 
            // trough_140_RadioButton
            // 
            this.trough_140_RadioButton.AutoCheck = false;
            this.trough_140_RadioButton.AutoSize = true;
            this.trough_140_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_140_RadioButton.Name = "trough_140_RadioButton";
            this.trough_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_140_RadioButton.TabIndex = 482;
            this.trough_140_RadioButton.UseVisualStyleBackColor = false;
            this.trough_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_140_RadioButton
            // 
            this.flat_140_RadioButton.AutoCheck = false;
            this.flat_140_RadioButton.AutoSize = true;
            this.flat_140_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_140_RadioButton.Name = "flat_140_RadioButton";
            this.flat_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_140_RadioButton.TabIndex = 266;
            this.flat_140_RadioButton.UseVisualStyleBackColor = false;
            this.flat_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_140_RadioButton
            // 
            this.crest_140_RadioButton.AutoCheck = false;
            this.crest_140_RadioButton.AutoSize = true;
            this.crest_140_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_140_RadioButton.Checked = true;
            this.crest_140_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_140_RadioButton.Name = "crest_140_RadioButton";
            this.crest_140_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_140_RadioButton.TabIndex = 264;
            this.crest_140_RadioButton.UseVisualStyleBackColor = false;
            this.crest_140_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel124
            // 
            this.panel124.Controls.Add(this.trough_143_RadioButton);
            this.panel124.Controls.Add(this.flat_143_RadioButton);
            this.panel124.Controls.Add(this.crest_143_RadioButton);
            this.panel124.Location = new System.Drawing.Point(1183, 197);
            this.panel124.Name = "panel124";
            this.panel124.Size = new System.Drawing.Size(188, 25);
            this.panel124.TabIndex = 556;
            // 
            // trough_143_RadioButton
            // 
            this.trough_143_RadioButton.AutoCheck = false;
            this.trough_143_RadioButton.AutoSize = true;
            this.trough_143_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_143_RadioButton.Name = "trough_143_RadioButton";
            this.trough_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_143_RadioButton.TabIndex = 482;
            this.trough_143_RadioButton.UseVisualStyleBackColor = false;
            this.trough_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_143_RadioButton
            // 
            this.flat_143_RadioButton.AutoCheck = false;
            this.flat_143_RadioButton.AutoSize = true;
            this.flat_143_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_143_RadioButton.Name = "flat_143_RadioButton";
            this.flat_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_143_RadioButton.TabIndex = 266;
            this.flat_143_RadioButton.UseVisualStyleBackColor = false;
            this.flat_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_143_RadioButton
            // 
            this.crest_143_RadioButton.AutoCheck = false;
            this.crest_143_RadioButton.AutoSize = true;
            this.crest_143_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_143_RadioButton.Checked = true;
            this.crest_143_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_143_RadioButton.Name = "crest_143_RadioButton";
            this.crest_143_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_143_RadioButton.TabIndex = 264;
            this.crest_143_RadioButton.UseVisualStyleBackColor = false;
            this.crest_143_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel125
            // 
            this.panel125.Controls.Add(this.trough_150_RadioButton);
            this.panel125.Controls.Add(this.flat_150_RadioButton);
            this.panel125.Controls.Add(this.crest_150_RadioButton);
            this.panel125.Location = new System.Drawing.Point(1183, 224);
            this.panel125.Name = "panel125";
            this.panel125.Size = new System.Drawing.Size(188, 25);
            this.panel125.TabIndex = 557;
            // 
            // trough_150_RadioButton
            // 
            this.trough_150_RadioButton.AutoCheck = false;
            this.trough_150_RadioButton.AutoSize = true;
            this.trough_150_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_150_RadioButton.Name = "trough_150_RadioButton";
            this.trough_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_150_RadioButton.TabIndex = 482;
            this.trough_150_RadioButton.UseVisualStyleBackColor = false;
            this.trough_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_150_RadioButton
            // 
            this.flat_150_RadioButton.AutoCheck = false;
            this.flat_150_RadioButton.AutoSize = true;
            this.flat_150_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_150_RadioButton.Name = "flat_150_RadioButton";
            this.flat_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_150_RadioButton.TabIndex = 266;
            this.flat_150_RadioButton.UseVisualStyleBackColor = false;
            this.flat_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_150_RadioButton
            // 
            this.crest_150_RadioButton.AutoCheck = false;
            this.crest_150_RadioButton.AutoSize = true;
            this.crest_150_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_150_RadioButton.Checked = true;
            this.crest_150_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_150_RadioButton.Name = "crest_150_RadioButton";
            this.crest_150_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_150_RadioButton.TabIndex = 264;
            this.crest_150_RadioButton.UseVisualStyleBackColor = false;
            this.crest_150_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel126
            // 
            this.panel126.Controls.Add(this.trough_153_RadioButton);
            this.panel126.Controls.Add(this.flat_153_RadioButton);
            this.panel126.Controls.Add(this.crest_153_RadioButton);
            this.panel126.Location = new System.Drawing.Point(1183, 251);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(188, 25);
            this.panel126.TabIndex = 558;
            // 
            // trough_153_RadioButton
            // 
            this.trough_153_RadioButton.AutoCheck = false;
            this.trough_153_RadioButton.AutoSize = true;
            this.trough_153_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_153_RadioButton.Name = "trough_153_RadioButton";
            this.trough_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_153_RadioButton.TabIndex = 482;
            this.trough_153_RadioButton.UseVisualStyleBackColor = false;
            this.trough_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_153_RadioButton
            // 
            this.flat_153_RadioButton.AutoCheck = false;
            this.flat_153_RadioButton.AutoSize = true;
            this.flat_153_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_153_RadioButton.Name = "flat_153_RadioButton";
            this.flat_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_153_RadioButton.TabIndex = 266;
            this.flat_153_RadioButton.UseVisualStyleBackColor = false;
            this.flat_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_153_RadioButton
            // 
            this.crest_153_RadioButton.AutoCheck = false;
            this.crest_153_RadioButton.AutoSize = true;
            this.crest_153_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_153_RadioButton.Checked = true;
            this.crest_153_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_153_RadioButton.Name = "crest_153_RadioButton";
            this.crest_153_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_153_RadioButton.TabIndex = 264;
            this.crest_153_RadioButton.UseVisualStyleBackColor = false;
            this.crest_153_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel127
            // 
            this.panel127.Controls.Add(this.trough_160_RadioButton);
            this.panel127.Controls.Add(this.flat_160_RadioButton);
            this.panel127.Controls.Add(this.crest_160_RadioButton);
            this.panel127.Location = new System.Drawing.Point(1183, 278);
            this.panel127.Name = "panel127";
            this.panel127.Size = new System.Drawing.Size(188, 25);
            this.panel127.TabIndex = 559;
            // 
            // trough_160_RadioButton
            // 
            this.trough_160_RadioButton.AutoCheck = false;
            this.trough_160_RadioButton.AutoSize = true;
            this.trough_160_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_160_RadioButton.Name = "trough_160_RadioButton";
            this.trough_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_160_RadioButton.TabIndex = 482;
            this.trough_160_RadioButton.UseVisualStyleBackColor = false;
            this.trough_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_160_RadioButton
            // 
            this.flat_160_RadioButton.AutoCheck = false;
            this.flat_160_RadioButton.AutoSize = true;
            this.flat_160_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_160_RadioButton.Name = "flat_160_RadioButton";
            this.flat_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_160_RadioButton.TabIndex = 266;
            this.flat_160_RadioButton.UseVisualStyleBackColor = false;
            this.flat_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_160_RadioButton
            // 
            this.crest_160_RadioButton.AutoCheck = false;
            this.crest_160_RadioButton.AutoSize = true;
            this.crest_160_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_160_RadioButton.Checked = true;
            this.crest_160_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_160_RadioButton.Name = "crest_160_RadioButton";
            this.crest_160_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_160_RadioButton.TabIndex = 264;
            this.crest_160_RadioButton.UseVisualStyleBackColor = false;
            this.crest_160_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel128
            // 
            this.panel128.Controls.Add(this.trough_163_RadioButton);
            this.panel128.Controls.Add(this.flat_163_RadioButton);
            this.panel128.Controls.Add(this.crest_163_RadioButton);
            this.panel128.Location = new System.Drawing.Point(1183, 305);
            this.panel128.Name = "panel128";
            this.panel128.Size = new System.Drawing.Size(188, 25);
            this.panel128.TabIndex = 560;
            // 
            // trough_163_RadioButton
            // 
            this.trough_163_RadioButton.AutoCheck = false;
            this.trough_163_RadioButton.AutoSize = true;
            this.trough_163_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_163_RadioButton.Name = "trough_163_RadioButton";
            this.trough_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_163_RadioButton.TabIndex = 482;
            this.trough_163_RadioButton.UseVisualStyleBackColor = false;
            this.trough_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_163_RadioButton
            // 
            this.flat_163_RadioButton.AutoCheck = false;
            this.flat_163_RadioButton.AutoSize = true;
            this.flat_163_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_163_RadioButton.Name = "flat_163_RadioButton";
            this.flat_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_163_RadioButton.TabIndex = 266;
            this.flat_163_RadioButton.UseVisualStyleBackColor = false;
            this.flat_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_163_RadioButton
            // 
            this.crest_163_RadioButton.AutoCheck = false;
            this.crest_163_RadioButton.AutoSize = true;
            this.crest_163_RadioButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crest_163_RadioButton.Checked = true;
            this.crest_163_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_163_RadioButton.Name = "crest_163_RadioButton";
            this.crest_163_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_163_RadioButton.TabIndex = 264;
            this.crest_163_RadioButton.UseVisualStyleBackColor = false;
            this.crest_163_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel129
            // 
            this.panel129.Controls.Add(this.trough_170_RadioButton);
            this.panel129.Controls.Add(this.flat_170_RadioButton);
            this.panel129.Controls.Add(this.crest_170_RadioButton);
            this.panel129.Location = new System.Drawing.Point(1183, 348);
            this.panel129.Name = "panel129";
            this.panel129.Size = new System.Drawing.Size(188, 25);
            this.panel129.TabIndex = 561;
            // 
            // trough_170_RadioButton
            // 
            this.trough_170_RadioButton.AutoCheck = false;
            this.trough_170_RadioButton.AutoSize = true;
            this.trough_170_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_170_RadioButton.Name = "trough_170_RadioButton";
            this.trough_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_170_RadioButton.TabIndex = 482;
            this.trough_170_RadioButton.UseVisualStyleBackColor = false;
            this.trough_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_170_RadioButton
            // 
            this.flat_170_RadioButton.AutoCheck = false;
            this.flat_170_RadioButton.AutoSize = true;
            this.flat_170_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_170_RadioButton.Checked = true;
            this.flat_170_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_170_RadioButton.Name = "flat_170_RadioButton";
            this.flat_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_170_RadioButton.TabIndex = 266;
            this.flat_170_RadioButton.UseVisualStyleBackColor = false;
            this.flat_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_170_RadioButton
            // 
            this.crest_170_RadioButton.AutoCheck = false;
            this.crest_170_RadioButton.AutoSize = true;
            this.crest_170_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_170_RadioButton.Name = "crest_170_RadioButton";
            this.crest_170_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_170_RadioButton.TabIndex = 264;
            this.crest_170_RadioButton.UseVisualStyleBackColor = false;
            this.crest_170_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel130
            // 
            this.panel130.Controls.Add(this.trough_173_RadioButton);
            this.panel130.Controls.Add(this.flat_173_RadioButton);
            this.panel130.Controls.Add(this.crest_173_RadioButton);
            this.panel130.Location = new System.Drawing.Point(1183, 375);
            this.panel130.Name = "panel130";
            this.panel130.Size = new System.Drawing.Size(188, 25);
            this.panel130.TabIndex = 562;
            // 
            // trough_173_RadioButton
            // 
            this.trough_173_RadioButton.AutoCheck = false;
            this.trough_173_RadioButton.AutoSize = true;
            this.trough_173_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_173_RadioButton.Name = "trough_173_RadioButton";
            this.trough_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_173_RadioButton.TabIndex = 482;
            this.trough_173_RadioButton.UseVisualStyleBackColor = false;
            this.trough_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_173_RadioButton
            // 
            this.flat_173_RadioButton.AutoCheck = false;
            this.flat_173_RadioButton.AutoSize = true;
            this.flat_173_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_173_RadioButton.Checked = true;
            this.flat_173_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_173_RadioButton.Name = "flat_173_RadioButton";
            this.flat_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_173_RadioButton.TabIndex = 266;
            this.flat_173_RadioButton.UseVisualStyleBackColor = false;
            this.flat_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_173_RadioButton
            // 
            this.crest_173_RadioButton.AutoCheck = false;
            this.crest_173_RadioButton.AutoSize = true;
            this.crest_173_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_173_RadioButton.Name = "crest_173_RadioButton";
            this.crest_173_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_173_RadioButton.TabIndex = 264;
            this.crest_173_RadioButton.UseVisualStyleBackColor = false;
            this.crest_173_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel131
            // 
            this.panel131.Controls.Add(this.trough_180_RadioButton);
            this.panel131.Controls.Add(this.flat_180_RadioButton);
            this.panel131.Controls.Add(this.crest_180_RadioButton);
            this.panel131.Location = new System.Drawing.Point(1183, 402);
            this.panel131.Name = "panel131";
            this.panel131.Size = new System.Drawing.Size(188, 25);
            this.panel131.TabIndex = 563;
            // 
            // trough_180_RadioButton
            // 
            this.trough_180_RadioButton.AutoCheck = false;
            this.trough_180_RadioButton.AutoSize = true;
            this.trough_180_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_180_RadioButton.Name = "trough_180_RadioButton";
            this.trough_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_180_RadioButton.TabIndex = 482;
            this.trough_180_RadioButton.UseVisualStyleBackColor = false;
            this.trough_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_180_RadioButton
            // 
            this.flat_180_RadioButton.AutoCheck = false;
            this.flat_180_RadioButton.AutoSize = true;
            this.flat_180_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_180_RadioButton.Checked = true;
            this.flat_180_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_180_RadioButton.Name = "flat_180_RadioButton";
            this.flat_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_180_RadioButton.TabIndex = 266;
            this.flat_180_RadioButton.UseVisualStyleBackColor = false;
            this.flat_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_180_RadioButton
            // 
            this.crest_180_RadioButton.AutoCheck = false;
            this.crest_180_RadioButton.AutoSize = true;
            this.crest_180_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_180_RadioButton.Name = "crest_180_RadioButton";
            this.crest_180_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_180_RadioButton.TabIndex = 264;
            this.crest_180_RadioButton.UseVisualStyleBackColor = false;
            this.crest_180_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel132
            // 
            this.panel132.Controls.Add(this.trough_183_RadioButton);
            this.panel132.Controls.Add(this.flat_183_RadioButton);
            this.panel132.Controls.Add(this.crest_183_RadioButton);
            this.panel132.Location = new System.Drawing.Point(1183, 429);
            this.panel132.Name = "panel132";
            this.panel132.Size = new System.Drawing.Size(188, 25);
            this.panel132.TabIndex = 564;
            // 
            // trough_183_RadioButton
            // 
            this.trough_183_RadioButton.AutoCheck = false;
            this.trough_183_RadioButton.AutoSize = true;
            this.trough_183_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_183_RadioButton.Name = "trough_183_RadioButton";
            this.trough_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_183_RadioButton.TabIndex = 482;
            this.trough_183_RadioButton.UseVisualStyleBackColor = false;
            this.trough_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_183_RadioButton
            // 
            this.flat_183_RadioButton.AutoCheck = false;
            this.flat_183_RadioButton.AutoSize = true;
            this.flat_183_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_183_RadioButton.Checked = true;
            this.flat_183_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_183_RadioButton.Name = "flat_183_RadioButton";
            this.flat_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_183_RadioButton.TabIndex = 266;
            this.flat_183_RadioButton.UseVisualStyleBackColor = false;
            this.flat_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_183_RadioButton
            // 
            this.crest_183_RadioButton.AutoCheck = false;
            this.crest_183_RadioButton.AutoSize = true;
            this.crest_183_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_183_RadioButton.Name = "crest_183_RadioButton";
            this.crest_183_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_183_RadioButton.TabIndex = 264;
            this.crest_183_RadioButton.UseVisualStyleBackColor = false;
            this.crest_183_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel133
            // 
            this.panel133.Controls.Add(this.trough_190_RadioButton);
            this.panel133.Controls.Add(this.flat_190_RadioButton);
            this.panel133.Controls.Add(this.crest_190_RadioButton);
            this.panel133.Location = new System.Drawing.Point(1183, 456);
            this.panel133.Name = "panel133";
            this.panel133.Size = new System.Drawing.Size(188, 25);
            this.panel133.TabIndex = 565;
            // 
            // trough_190_RadioButton
            // 
            this.trough_190_RadioButton.AutoCheck = false;
            this.trough_190_RadioButton.AutoSize = true;
            this.trough_190_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_190_RadioButton.Name = "trough_190_RadioButton";
            this.trough_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_190_RadioButton.TabIndex = 482;
            this.trough_190_RadioButton.UseVisualStyleBackColor = false;
            this.trough_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_190_RadioButton
            // 
            this.flat_190_RadioButton.AutoCheck = false;
            this.flat_190_RadioButton.AutoSize = true;
            this.flat_190_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_190_RadioButton.Checked = true;
            this.flat_190_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_190_RadioButton.Name = "flat_190_RadioButton";
            this.flat_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_190_RadioButton.TabIndex = 266;
            this.flat_190_RadioButton.UseVisualStyleBackColor = false;
            this.flat_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_190_RadioButton
            // 
            this.crest_190_RadioButton.AutoCheck = false;
            this.crest_190_RadioButton.AutoSize = true;
            this.crest_190_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_190_RadioButton.Name = "crest_190_RadioButton";
            this.crest_190_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_190_RadioButton.TabIndex = 264;
            this.crest_190_RadioButton.UseVisualStyleBackColor = false;
            this.crest_190_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel134
            // 
            this.panel134.Controls.Add(this.trough_193_RadioButton);
            this.panel134.Controls.Add(this.flat_193_RadioButton);
            this.panel134.Controls.Add(this.crest_193_RadioButton);
            this.panel134.Location = new System.Drawing.Point(1183, 483);
            this.panel134.Name = "panel134";
            this.panel134.Size = new System.Drawing.Size(188, 25);
            this.panel134.TabIndex = 566;
            // 
            // trough_193_RadioButton
            // 
            this.trough_193_RadioButton.AutoCheck = false;
            this.trough_193_RadioButton.AutoSize = true;
            this.trough_193_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_193_RadioButton.Name = "trough_193_RadioButton";
            this.trough_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_193_RadioButton.TabIndex = 482;
            this.trough_193_RadioButton.UseVisualStyleBackColor = false;
            this.trough_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_193_RadioButton
            // 
            this.flat_193_RadioButton.AutoCheck = false;
            this.flat_193_RadioButton.AutoSize = true;
            this.flat_193_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_193_RadioButton.Checked = true;
            this.flat_193_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_193_RadioButton.Name = "flat_193_RadioButton";
            this.flat_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_193_RadioButton.TabIndex = 266;
            this.flat_193_RadioButton.UseVisualStyleBackColor = false;
            this.flat_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_193_RadioButton
            // 
            this.crest_193_RadioButton.AutoCheck = false;
            this.crest_193_RadioButton.AutoSize = true;
            this.crest_193_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_193_RadioButton.Name = "crest_193_RadioButton";
            this.crest_193_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_193_RadioButton.TabIndex = 264;
            this.crest_193_RadioButton.UseVisualStyleBackColor = false;
            this.crest_193_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel135
            // 
            this.panel135.Controls.Add(this.trough_200_RadioButton);
            this.panel135.Controls.Add(this.flat_200_RadioButton);
            this.panel135.Controls.Add(this.crest_200_RadioButton);
            this.panel135.Location = new System.Drawing.Point(1183, 510);
            this.panel135.Name = "panel135";
            this.panel135.Size = new System.Drawing.Size(188, 25);
            this.panel135.TabIndex = 567;
            // 
            // trough_200_RadioButton
            // 
            this.trough_200_RadioButton.AutoCheck = false;
            this.trough_200_RadioButton.AutoSize = true;
            this.trough_200_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_200_RadioButton.Name = "trough_200_RadioButton";
            this.trough_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_200_RadioButton.TabIndex = 482;
            this.trough_200_RadioButton.UseVisualStyleBackColor = false;
            this.trough_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_200_RadioButton
            // 
            this.flat_200_RadioButton.AutoCheck = false;
            this.flat_200_RadioButton.AutoSize = true;
            this.flat_200_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_200_RadioButton.Checked = true;
            this.flat_200_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_200_RadioButton.Name = "flat_200_RadioButton";
            this.flat_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_200_RadioButton.TabIndex = 266;
            this.flat_200_RadioButton.UseVisualStyleBackColor = false;
            this.flat_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_200_RadioButton
            // 
            this.crest_200_RadioButton.AutoCheck = false;
            this.crest_200_RadioButton.AutoSize = true;
            this.crest_200_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_200_RadioButton.Name = "crest_200_RadioButton";
            this.crest_200_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_200_RadioButton.TabIndex = 264;
            this.crest_200_RadioButton.UseVisualStyleBackColor = false;
            this.crest_200_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel136
            // 
            this.panel136.Controls.Add(this.trough_203_RadioButton);
            this.panel136.Controls.Add(this.flat_203_RadioButton);
            this.panel136.Controls.Add(this.crest_203_RadioButton);
            this.panel136.Location = new System.Drawing.Point(1183, 537);
            this.panel136.Name = "panel136";
            this.panel136.Size = new System.Drawing.Size(188, 25);
            this.panel136.TabIndex = 568;
            // 
            // trough_203_RadioButton
            // 
            this.trough_203_RadioButton.AutoCheck = false;
            this.trough_203_RadioButton.AutoSize = true;
            this.trough_203_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_203_RadioButton.Name = "trough_203_RadioButton";
            this.trough_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_203_RadioButton.TabIndex = 482;
            this.trough_203_RadioButton.UseVisualStyleBackColor = false;
            this.trough_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_203_RadioButton
            // 
            this.flat_203_RadioButton.AutoCheck = false;
            this.flat_203_RadioButton.AutoSize = true;
            this.flat_203_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_203_RadioButton.Checked = true;
            this.flat_203_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_203_RadioButton.Name = "flat_203_RadioButton";
            this.flat_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_203_RadioButton.TabIndex = 266;
            this.flat_203_RadioButton.UseVisualStyleBackColor = false;
            this.flat_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_203_RadioButton
            // 
            this.crest_203_RadioButton.AutoCheck = false;
            this.crest_203_RadioButton.AutoSize = true;
            this.crest_203_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_203_RadioButton.Name = "crest_203_RadioButton";
            this.crest_203_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_203_RadioButton.TabIndex = 264;
            this.crest_203_RadioButton.UseVisualStyleBackColor = false;
            this.crest_203_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel137
            // 
            this.panel137.Controls.Add(this.trough_210_RadioButton);
            this.panel137.Controls.Add(this.flat_210_RadioButton);
            this.panel137.Controls.Add(this.crest_210_RadioButton);
            this.panel137.Location = new System.Drawing.Point(1183, 580);
            this.panel137.Name = "panel137";
            this.panel137.Size = new System.Drawing.Size(188, 25);
            this.panel137.TabIndex = 569;
            // 
            // trough_210_RadioButton
            // 
            this.trough_210_RadioButton.AutoCheck = false;
            this.trough_210_RadioButton.AutoSize = true;
            this.trough_210_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_210_RadioButton.Name = "trough_210_RadioButton";
            this.trough_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_210_RadioButton.TabIndex = 482;
            this.trough_210_RadioButton.UseVisualStyleBackColor = false;
            this.trough_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_210_RadioButton
            // 
            this.flat_210_RadioButton.AutoCheck = false;
            this.flat_210_RadioButton.AutoSize = true;
            this.flat_210_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_210_RadioButton.Checked = true;
            this.flat_210_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_210_RadioButton.Name = "flat_210_RadioButton";
            this.flat_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_210_RadioButton.TabIndex = 266;
            this.flat_210_RadioButton.UseVisualStyleBackColor = false;
            this.flat_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_210_RadioButton
            // 
            this.crest_210_RadioButton.AutoCheck = false;
            this.crest_210_RadioButton.AutoSize = true;
            this.crest_210_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_210_RadioButton.Name = "crest_210_RadioButton";
            this.crest_210_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_210_RadioButton.TabIndex = 264;
            this.crest_210_RadioButton.UseVisualStyleBackColor = false;
            this.crest_210_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel138
            // 
            this.panel138.Controls.Add(this.trough_213_RadioButton);
            this.panel138.Controls.Add(this.flat_213_RadioButton);
            this.panel138.Controls.Add(this.crest_213_RadioButton);
            this.panel138.Location = new System.Drawing.Point(1183, 607);
            this.panel138.Name = "panel138";
            this.panel138.Size = new System.Drawing.Size(188, 25);
            this.panel138.TabIndex = 570;
            // 
            // trough_213_RadioButton
            // 
            this.trough_213_RadioButton.AutoCheck = false;
            this.trough_213_RadioButton.AutoSize = true;
            this.trough_213_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_213_RadioButton.Name = "trough_213_RadioButton";
            this.trough_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_213_RadioButton.TabIndex = 482;
            this.trough_213_RadioButton.UseVisualStyleBackColor = false;
            this.trough_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_213_RadioButton
            // 
            this.flat_213_RadioButton.AutoCheck = false;
            this.flat_213_RadioButton.AutoSize = true;
            this.flat_213_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_213_RadioButton.Checked = true;
            this.flat_213_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_213_RadioButton.Name = "flat_213_RadioButton";
            this.flat_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_213_RadioButton.TabIndex = 266;
            this.flat_213_RadioButton.UseVisualStyleBackColor = false;
            this.flat_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_213_RadioButton
            // 
            this.crest_213_RadioButton.AutoCheck = false;
            this.crest_213_RadioButton.AutoSize = true;
            this.crest_213_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_213_RadioButton.Name = "crest_213_RadioButton";
            this.crest_213_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_213_RadioButton.TabIndex = 264;
            this.crest_213_RadioButton.UseVisualStyleBackColor = false;
            this.crest_213_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel139
            // 
            this.panel139.Controls.Add(this.trough_220_RadioButton);
            this.panel139.Controls.Add(this.flat_220_RadioButton);
            this.panel139.Controls.Add(this.crest_220_RadioButton);
            this.panel139.Location = new System.Drawing.Point(1183, 634);
            this.panel139.Name = "panel139";
            this.panel139.Size = new System.Drawing.Size(188, 25);
            this.panel139.TabIndex = 571;
            // 
            // trough_220_RadioButton
            // 
            this.trough_220_RadioButton.AutoCheck = false;
            this.trough_220_RadioButton.AutoSize = true;
            this.trough_220_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_220_RadioButton.Name = "trough_220_RadioButton";
            this.trough_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_220_RadioButton.TabIndex = 482;
            this.trough_220_RadioButton.UseVisualStyleBackColor = false;
            this.trough_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_220_RadioButton
            // 
            this.flat_220_RadioButton.AutoCheck = false;
            this.flat_220_RadioButton.AutoSize = true;
            this.flat_220_RadioButton.BackColor = System.Drawing.Color.SpringGreen;
            this.flat_220_RadioButton.Checked = true;
            this.flat_220_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_220_RadioButton.Name = "flat_220_RadioButton";
            this.flat_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_220_RadioButton.TabIndex = 266;
            this.flat_220_RadioButton.UseVisualStyleBackColor = false;
            this.flat_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_220_RadioButton
            // 
            this.crest_220_RadioButton.AutoCheck = false;
            this.crest_220_RadioButton.AutoSize = true;
            this.crest_220_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_220_RadioButton.Name = "crest_220_RadioButton";
            this.crest_220_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_220_RadioButton.TabIndex = 264;
            this.crest_220_RadioButton.UseVisualStyleBackColor = false;
            this.crest_220_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel140
            // 
            this.panel140.Controls.Add(this.trough_223_RadioButton);
            this.panel140.Controls.Add(this.flat_223_RadioButton);
            this.panel140.Controls.Add(this.crest_223_RadioButton);
            this.panel140.Location = new System.Drawing.Point(1183, 661);
            this.panel140.Name = "panel140";
            this.panel140.Size = new System.Drawing.Size(188, 25);
            this.panel140.TabIndex = 562;
            // 
            // trough_223_RadioButton
            // 
            this.trough_223_RadioButton.AutoCheck = false;
            this.trough_223_RadioButton.AutoSize = true;
            this.trough_223_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_223_RadioButton.Checked = true;
            this.trough_223_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_223_RadioButton.Name = "trough_223_RadioButton";
            this.trough_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_223_RadioButton.TabIndex = 482;
            this.trough_223_RadioButton.UseVisualStyleBackColor = false;
            this.trough_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_223_RadioButton
            // 
            this.flat_223_RadioButton.AutoCheck = false;
            this.flat_223_RadioButton.AutoSize = true;
            this.flat_223_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_223_RadioButton.Name = "flat_223_RadioButton";
            this.flat_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_223_RadioButton.TabIndex = 266;
            this.flat_223_RadioButton.UseVisualStyleBackColor = false;
            this.flat_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_223_RadioButton
            // 
            this.crest_223_RadioButton.AutoCheck = false;
            this.crest_223_RadioButton.AutoSize = true;
            this.crest_223_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_223_RadioButton.Name = "crest_223_RadioButton";
            this.crest_223_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_223_RadioButton.TabIndex = 264;
            this.crest_223_RadioButton.UseVisualStyleBackColor = false;
            this.crest_223_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel141
            // 
            this.panel141.Controls.Add(this.trough_230_RadioButton);
            this.panel141.Controls.Add(this.flat_230_RadioButton);
            this.panel141.Controls.Add(this.crest_230_RadioButton);
            this.panel141.Location = new System.Drawing.Point(1183, 688);
            this.panel141.Name = "panel141";
            this.panel141.Size = new System.Drawing.Size(188, 25);
            this.panel141.TabIndex = 563;
            // 
            // trough_230_RadioButton
            // 
            this.trough_230_RadioButton.AutoCheck = false;
            this.trough_230_RadioButton.AutoSize = true;
            this.trough_230_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_230_RadioButton.Checked = true;
            this.trough_230_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_230_RadioButton.Name = "trough_230_RadioButton";
            this.trough_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_230_RadioButton.TabIndex = 482;
            this.trough_230_RadioButton.UseVisualStyleBackColor = false;
            this.trough_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_230_RadioButton
            // 
            this.flat_230_RadioButton.AutoCheck = false;
            this.flat_230_RadioButton.AutoSize = true;
            this.flat_230_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_230_RadioButton.Name = "flat_230_RadioButton";
            this.flat_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_230_RadioButton.TabIndex = 266;
            this.flat_230_RadioButton.UseVisualStyleBackColor = false;
            this.flat_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_230_RadioButton
            // 
            this.crest_230_RadioButton.AutoCheck = false;
            this.crest_230_RadioButton.AutoSize = true;
            this.crest_230_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_230_RadioButton.Name = "crest_230_RadioButton";
            this.crest_230_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_230_RadioButton.TabIndex = 264;
            this.crest_230_RadioButton.UseVisualStyleBackColor = false;
            this.crest_230_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel142
            // 
            this.panel142.Controls.Add(this.trough_233_RadioButton);
            this.panel142.Controls.Add(this.flat_233_RadioButton);
            this.panel142.Controls.Add(this.crest_233_RadioButton);
            this.panel142.Location = new System.Drawing.Point(1183, 715);
            this.panel142.Name = "panel142";
            this.panel142.Size = new System.Drawing.Size(188, 25);
            this.panel142.TabIndex = 572;
            // 
            // trough_233_RadioButton
            // 
            this.trough_233_RadioButton.AutoCheck = false;
            this.trough_233_RadioButton.AutoSize = true;
            this.trough_233_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_233_RadioButton.Checked = true;
            this.trough_233_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_233_RadioButton.Name = "trough_233_RadioButton";
            this.trough_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_233_RadioButton.TabIndex = 482;
            this.trough_233_RadioButton.UseVisualStyleBackColor = false;
            this.trough_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_233_RadioButton
            // 
            this.flat_233_RadioButton.AutoCheck = false;
            this.flat_233_RadioButton.AutoSize = true;
            this.flat_233_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_233_RadioButton.Name = "flat_233_RadioButton";
            this.flat_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_233_RadioButton.TabIndex = 266;
            this.flat_233_RadioButton.UseVisualStyleBackColor = false;
            this.flat_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_233_RadioButton
            // 
            this.crest_233_RadioButton.AutoCheck = false;
            this.crest_233_RadioButton.AutoSize = true;
            this.crest_233_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_233_RadioButton.Name = "crest_233_RadioButton";
            this.crest_233_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_233_RadioButton.TabIndex = 264;
            this.crest_233_RadioButton.UseVisualStyleBackColor = false;
            this.crest_233_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel143
            // 
            this.panel143.Controls.Add(this.trough_240_RadioButton);
            this.panel143.Controls.Add(this.flat_240_RadioButton);
            this.panel143.Controls.Add(this.crest_240_RadioButton);
            this.panel143.Location = new System.Drawing.Point(1183, 742);
            this.panel143.Name = "panel143";
            this.panel143.Size = new System.Drawing.Size(188, 25);
            this.panel143.TabIndex = 573;
            // 
            // trough_240_RadioButton
            // 
            this.trough_240_RadioButton.AutoCheck = false;
            this.trough_240_RadioButton.AutoSize = true;
            this.trough_240_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_240_RadioButton.Checked = true;
            this.trough_240_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_240_RadioButton.Name = "trough_240_RadioButton";
            this.trough_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_240_RadioButton.TabIndex = 482;
            this.trough_240_RadioButton.UseVisualStyleBackColor = false;
            this.trough_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_240_RadioButton
            // 
            this.flat_240_RadioButton.AutoCheck = false;
            this.flat_240_RadioButton.AutoSize = true;
            this.flat_240_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_240_RadioButton.Name = "flat_240_RadioButton";
            this.flat_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_240_RadioButton.TabIndex = 266;
            this.flat_240_RadioButton.UseVisualStyleBackColor = false;
            this.flat_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_240_RadioButton
            // 
            this.crest_240_RadioButton.AutoCheck = false;
            this.crest_240_RadioButton.AutoSize = true;
            this.crest_240_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_240_RadioButton.Name = "crest_240_RadioButton";
            this.crest_240_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_240_RadioButton.TabIndex = 264;
            this.crest_240_RadioButton.UseVisualStyleBackColor = false;
            this.crest_240_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // panel144
            // 
            this.panel144.Controls.Add(this.trough_003_RadioButton);
            this.panel144.Controls.Add(this.flat_003_RadioButton);
            this.panel144.Controls.Add(this.crest_003_RadioButton);
            this.panel144.Location = new System.Drawing.Point(1183, 769);
            this.panel144.Name = "panel144";
            this.panel144.Size = new System.Drawing.Size(188, 25);
            this.panel144.TabIndex = 574;
            // 
            // trough_003_RadioButton
            // 
            this.trough_003_RadioButton.AutoCheck = false;
            this.trough_003_RadioButton.AutoSize = true;
            this.trough_003_RadioButton.BackColor = System.Drawing.Color.Green;
            this.trough_003_RadioButton.Checked = true;
            this.trough_003_RadioButton.Location = new System.Drawing.Point(151, 3);
            this.trough_003_RadioButton.Name = "trough_003_RadioButton";
            this.trough_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.trough_003_RadioButton.TabIndex = 482;
            this.trough_003_RadioButton.UseVisualStyleBackColor = false;
            this.trough_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // flat_003_RadioButton
            // 
            this.flat_003_RadioButton.AutoCheck = false;
            this.flat_003_RadioButton.AutoSize = true;
            this.flat_003_RadioButton.Location = new System.Drawing.Point(80, 3);
            this.flat_003_RadioButton.Name = "flat_003_RadioButton";
            this.flat_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.flat_003_RadioButton.TabIndex = 266;
            this.flat_003_RadioButton.UseVisualStyleBackColor = false;
            this.flat_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // crest_003_RadioButton
            // 
            this.crest_003_RadioButton.AutoCheck = false;
            this.crest_003_RadioButton.AutoSize = true;
            this.crest_003_RadioButton.Location = new System.Drawing.Point(13, 3);
            this.crest_003_RadioButton.Name = "crest_003_RadioButton";
            this.crest_003_RadioButton.Size = new System.Drawing.Size(17, 16);
            this.crest_003_RadioButton.TabIndex = 264;
            this.crest_003_RadioButton.UseVisualStyleBackColor = false;
            this.crest_003_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // Form_Mode_Running_Time
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1617, 811);
            this.Controls.Add(this.panel144);
            this.Controls.Add(this.panel143);
            this.Controls.Add(this.panel142);
            this.Controls.Add(this.panel141);
            this.Controls.Add(this.panel140);
            this.Controls.Add(this.panel139);
            this.Controls.Add(this.panel138);
            this.Controls.Add(this.panel137);
            this.Controls.Add(this.panel136);
            this.Controls.Add(this.panel135);
            this.Controls.Add(this.panel134);
            this.Controls.Add(this.panel133);
            this.Controls.Add(this.panel132);
            this.Controls.Add(this.panel131);
            this.Controls.Add(this.panel130);
            this.Controls.Add(this.panel129);
            this.Controls.Add(this.panel128);
            this.Controls.Add(this.panel127);
            this.Controls.Add(this.panel126);
            this.Controls.Add(this.panel125);
            this.Controls.Add(this.panel124);
            this.Controls.Add(this.panel123);
            this.Controls.Add(this.panel122);
            this.Controls.Add(this.panel121);
            this.Controls.Add(this.panel120);
            this.Controls.Add(this.panel119);
            this.Controls.Add(this.panel118);
            this.Controls.Add(this.panel117);
            this.Controls.Add(this.panel116);
            this.Controls.Add(this.panel115);
            this.Controls.Add(this.panel114);
            this.Controls.Add(this.panel113);
            this.Controls.Add(this.panel112);
            this.Controls.Add(this.panel111);
            this.Controls.Add(this.panel110);
            this.Controls.Add(this.panel109);
            this.Controls.Add(this.panel108);
            this.Controls.Add(this.panel107);
            this.Controls.Add(this.panel106);
            this.Controls.Add(this.panel105);
            this.Controls.Add(this.panel104);
            this.Controls.Add(this.panel103);
            this.Controls.Add(this.panel102);
            this.Controls.Add(this.panel101);
            this.Controls.Add(this.panel100);
            this.Controls.Add(this.panel99);
            this.Controls.Add(this.panel98);
            this.Controls.Add(this.panel97);
            this.Controls.Add(this.panel96);
            this.Controls.Add(this.panel95);
            this.Controls.Add(this.panel94);
            this.Controls.Add(this.panel93);
            this.Controls.Add(this.panel92);
            this.Controls.Add(this.panel91);
            this.Controls.Add(this.panel90);
            this.Controls.Add(this.panel89);
            this.Controls.Add(this.panel88);
            this.Controls.Add(this.panel87);
            this.Controls.Add(this.panel86);
            this.Controls.Add(this.panel85);
            this.Controls.Add(this.panel84);
            this.Controls.Add(this.panel83);
            this.Controls.Add(this.panel82);
            this.Controls.Add(this.panel81);
            this.Controls.Add(this.panel80);
            this.Controls.Add(this.panel79);
            this.Controls.Add(this.panel78);
            this.Controls.Add(this.panel77);
            this.Controls.Add(this.panel76);
            this.Controls.Add(this.panel75);
            this.Controls.Add(this.panel74);
            this.Controls.Add(this.panel73);
            this.Controls.Add(this.panel72);
            this.Controls.Add(this.panel71);
            this.Controls.Add(this.panel70);
            this.Controls.Add(this.panel69);
            this.Controls.Add(this.panel68);
            this.Controls.Add(this.panel67);
            this.Controls.Add(this.panel66);
            this.Controls.Add(this.panel65);
            this.Controls.Add(this.panel64);
            this.Controls.Add(this.panel63);
            this.Controls.Add(this.panel62);
            this.Controls.Add(this.panel61);
            this.Controls.Add(this.panel60);
            this.Controls.Add(this.panel59);
            this.Controls.Add(this.panel58);
            this.Controls.Add(this.panel57);
            this.Controls.Add(this.panel56);
            this.Controls.Add(this.panel55);
            this.Controls.Add(this.panel54);
            this.Controls.Add(this.panel53);
            this.Controls.Add(this.panel52);
            this.Controls.Add(this.panel51);
            this.Controls.Add(this.panel50);
            this.Controls.Add(this.panel49);
            this.Controls.Add(this.panel48);
            this.Controls.Add(this.panel47);
            this.Controls.Add(this.panel46);
            this.Controls.Add(this.panel45);
            this.Controls.Add(this.panel44);
            this.Controls.Add(this.panel43);
            this.Controls.Add(this.panel42);
            this.Controls.Add(this.panel41);
            this.Controls.Add(this.panel40);
            this.Controls.Add(this.panel39);
            this.Controls.Add(this.panel38);
            this.Controls.Add(this.panel37);
            this.Controls.Add(this.panel36);
            this.Controls.Add(this.panel35);
            this.Controls.Add(this.panel34);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel32);
            this.Controls.Add(this.panel31);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel29);
            this.Controls.Add(this.panel28);
            this.Controls.Add(this.panel27);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.mode_Running_Time_Ok_Button);
            this.Controls.Add(this.part_Rollback_Button);
            this.Controls.Add(this.full_Rollback_Button);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.part_Air_Hour_TextBox);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.part_Ice_Hour_TextBox);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.full_Air_Hour_TextBox);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.full_Ice_Hour_TextBox);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Mode_Running_Time";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "模式運轉時間";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Mode_Running_Time_Closed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel49.ResumeLayout(false);
            this.panel49.PerformLayout();
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.panel53.ResumeLayout(false);
            this.panel53.PerformLayout();
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel56.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel58.ResumeLayout(false);
            this.panel58.PerformLayout();
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel60.ResumeLayout(false);
            this.panel60.PerformLayout();
            this.panel61.ResumeLayout(false);
            this.panel61.PerformLayout();
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel64.PerformLayout();
            this.panel65.ResumeLayout(false);
            this.panel65.PerformLayout();
            this.panel66.ResumeLayout(false);
            this.panel66.PerformLayout();
            this.panel67.ResumeLayout(false);
            this.panel67.PerformLayout();
            this.panel68.ResumeLayout(false);
            this.panel68.PerformLayout();
            this.panel69.ResumeLayout(false);
            this.panel69.PerformLayout();
            this.panel70.ResumeLayout(false);
            this.panel70.PerformLayout();
            this.panel71.ResumeLayout(false);
            this.panel71.PerformLayout();
            this.panel72.ResumeLayout(false);
            this.panel72.PerformLayout();
            this.panel73.ResumeLayout(false);
            this.panel73.PerformLayout();
            this.panel74.ResumeLayout(false);
            this.panel74.PerformLayout();
            this.panel75.ResumeLayout(false);
            this.panel75.PerformLayout();
            this.panel76.ResumeLayout(false);
            this.panel76.PerformLayout();
            this.panel77.ResumeLayout(false);
            this.panel77.PerformLayout();
            this.panel78.ResumeLayout(false);
            this.panel78.PerformLayout();
            this.panel79.ResumeLayout(false);
            this.panel79.PerformLayout();
            this.panel80.ResumeLayout(false);
            this.panel80.PerformLayout();
            this.panel81.ResumeLayout(false);
            this.panel81.PerformLayout();
            this.panel82.ResumeLayout(false);
            this.panel82.PerformLayout();
            this.panel83.ResumeLayout(false);
            this.panel83.PerformLayout();
            this.panel84.ResumeLayout(false);
            this.panel84.PerformLayout();
            this.panel85.ResumeLayout(false);
            this.panel85.PerformLayout();
            this.panel86.ResumeLayout(false);
            this.panel86.PerformLayout();
            this.panel87.ResumeLayout(false);
            this.panel87.PerformLayout();
            this.panel88.ResumeLayout(false);
            this.panel88.PerformLayout();
            this.panel89.ResumeLayout(false);
            this.panel89.PerformLayout();
            this.panel90.ResumeLayout(false);
            this.panel90.PerformLayout();
            this.panel91.ResumeLayout(false);
            this.panel91.PerformLayout();
            this.panel92.ResumeLayout(false);
            this.panel92.PerformLayout();
            this.panel93.ResumeLayout(false);
            this.panel93.PerformLayout();
            this.panel94.ResumeLayout(false);
            this.panel94.PerformLayout();
            this.panel95.ResumeLayout(false);
            this.panel95.PerformLayout();
            this.panel96.ResumeLayout(false);
            this.panel96.PerformLayout();
            this.panel97.ResumeLayout(false);
            this.panel97.PerformLayout();
            this.panel98.ResumeLayout(false);
            this.panel98.PerformLayout();
            this.panel99.ResumeLayout(false);
            this.panel99.PerformLayout();
            this.panel100.ResumeLayout(false);
            this.panel100.PerformLayout();
            this.panel101.ResumeLayout(false);
            this.panel101.PerformLayout();
            this.panel102.ResumeLayout(false);
            this.panel102.PerformLayout();
            this.panel103.ResumeLayout(false);
            this.panel103.PerformLayout();
            this.panel104.ResumeLayout(false);
            this.panel104.PerformLayout();
            this.panel105.ResumeLayout(false);
            this.panel105.PerformLayout();
            this.panel106.ResumeLayout(false);
            this.panel106.PerformLayout();
            this.panel107.ResumeLayout(false);
            this.panel107.PerformLayout();
            this.panel108.ResumeLayout(false);
            this.panel108.PerformLayout();
            this.panel109.ResumeLayout(false);
            this.panel109.PerformLayout();
            this.panel110.ResumeLayout(false);
            this.panel110.PerformLayout();
            this.panel111.ResumeLayout(false);
            this.panel111.PerformLayout();
            this.panel112.ResumeLayout(false);
            this.panel112.PerformLayout();
            this.panel113.ResumeLayout(false);
            this.panel113.PerformLayout();
            this.panel114.ResumeLayout(false);
            this.panel114.PerformLayout();
            this.panel115.ResumeLayout(false);
            this.panel115.PerformLayout();
            this.panel116.ResumeLayout(false);
            this.panel116.PerformLayout();
            this.panel117.ResumeLayout(false);
            this.panel117.PerformLayout();
            this.panel118.ResumeLayout(false);
            this.panel118.PerformLayout();
            this.panel119.ResumeLayout(false);
            this.panel119.PerformLayout();
            this.panel120.ResumeLayout(false);
            this.panel120.PerformLayout();
            this.panel121.ResumeLayout(false);
            this.panel121.PerformLayout();
            this.panel122.ResumeLayout(false);
            this.panel122.PerformLayout();
            this.panel123.ResumeLayout(false);
            this.panel123.PerformLayout();
            this.panel124.ResumeLayout(false);
            this.panel124.PerformLayout();
            this.panel125.ResumeLayout(false);
            this.panel125.PerformLayout();
            this.panel126.ResumeLayout(false);
            this.panel126.PerformLayout();
            this.panel127.ResumeLayout(false);
            this.panel127.PerformLayout();
            this.panel128.ResumeLayout(false);
            this.panel128.PerformLayout();
            this.panel129.ResumeLayout(false);
            this.panel129.PerformLayout();
            this.panel130.ResumeLayout(false);
            this.panel130.PerformLayout();
            this.panel131.ResumeLayout(false);
            this.panel131.PerformLayout();
            this.panel132.ResumeLayout(false);
            this.panel132.PerformLayout();
            this.panel133.ResumeLayout(false);
            this.panel133.PerformLayout();
            this.panel134.ResumeLayout(false);
            this.panel134.PerformLayout();
            this.panel135.ResumeLayout(false);
            this.panel135.PerformLayout();
            this.panel136.ResumeLayout(false);
            this.panel136.PerformLayout();
            this.panel137.ResumeLayout(false);
            this.panel137.PerformLayout();
            this.panel138.ResumeLayout(false);
            this.panel138.PerformLayout();
            this.panel139.ResumeLayout(false);
            this.panel139.PerformLayout();
            this.panel140.ResumeLayout(false);
            this.panel140.PerformLayout();
            this.panel141.ResumeLayout(false);
            this.panel141.PerformLayout();
            this.panel142.ResumeLayout(false);
            this.panel142.PerformLayout();
            this.panel143.ResumeLayout(false);
            this.panel143.PerformLayout();
            this.panel144.ResumeLayout(false);
            this.panel144.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox full_Air_Hour_TextBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox part_Air_Hour_TextBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox part_Ice_Hour_TextBox;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button full_Rollback_Button;
        private System.Windows.Forms.Button part_Rollback_Button;
        private System.Windows.Forms.Button mode_Running_Time_Ok_Button;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton full_Air_010_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_010_RadioButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton full_Air_013_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_013_RadioButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton full_Air_020_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_020_RadioButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton full_Air_023_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_023_RadioButton;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton full_Air_030_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_030_RadioButton;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton full_Air_033_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_033_RadioButton;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton full_Air_040_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_040_RadioButton;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton full_Air_043_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_043_RadioButton;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton full_Air_050_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_050_RadioButton;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RadioButton full_Air_053_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_053_RadioButton;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RadioButton full_Air_060_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_060_RadioButton;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.RadioButton full_Air_063_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_063_RadioButton;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.RadioButton full_Air_070_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_070_RadioButton;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.RadioButton full_Air_073_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_073_RadioButton;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.RadioButton full_Air_080_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_080_RadioButton;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.RadioButton full_Air_083_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_083_RadioButton;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RadioButton full_Air_090_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_090_RadioButton;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.RadioButton full_Air_093_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_093_RadioButton;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.RadioButton full_Air_100_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_100_RadioButton;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.RadioButton full_Air_103_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_103_RadioButton;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.RadioButton full_Air_110_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_110_RadioButton;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.RadioButton full_Air_113_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_113_RadioButton;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.RadioButton full_Air_120_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_120_RadioButton;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.RadioButton full_Air_123_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_123_RadioButton;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.RadioButton part_Air_010_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_010_RadioButton;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.RadioButton part_Air_013_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_013_RadioButton;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.RadioButton part_Air_020_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_020_RadioButton;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.RadioButton part_Air_023_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_023_RadioButton;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.RadioButton part_Air_030_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_030_RadioButton;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.RadioButton part_Air_033_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_033_RadioButton;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.RadioButton part_Air_040_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_040_RadioButton;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.RadioButton part_Air_043_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_043_RadioButton;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.RadioButton part_Air_050_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_050_RadioButton;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.RadioButton part_Air_053_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_053_RadioButton;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.RadioButton part_Air_060_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_060_RadioButton;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.RadioButton part_Air_063_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_063_RadioButton;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.RadioButton part_Air_070_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_070_RadioButton;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.RadioButton part_Air_073_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_073_RadioButton;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.RadioButton part_Air_080_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_080_RadioButton;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.RadioButton part_Air_083_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_083_RadioButton;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.RadioButton part_Air_090_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_090_RadioButton;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.RadioButton part_Air_093_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_093_RadioButton;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.RadioButton part_Air_100_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_100_RadioButton;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.RadioButton part_Air_103_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_103_RadioButton;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.RadioButton part_Air_110_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_110_RadioButton;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.RadioButton part_Air_113_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_113_RadioButton;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.RadioButton part_Air_120_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_120_RadioButton;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.RadioButton part_Air_123_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_123_RadioButton;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.RadioButton trough_010_RadioButton;
        private System.Windows.Forms.RadioButton flat_010_RadioButton;
        private System.Windows.Forms.RadioButton crest_010_RadioButton;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.RadioButton trough_013_RadioButton;
        private System.Windows.Forms.RadioButton flat_013_RadioButton;
        private System.Windows.Forms.RadioButton crest_013_RadioButton;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.RadioButton trough_020_RadioButton;
        private System.Windows.Forms.RadioButton flat_020_RadioButton;
        private System.Windows.Forms.RadioButton crest_020_RadioButton;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.RadioButton trough_023_RadioButton;
        private System.Windows.Forms.RadioButton flat_023_RadioButton;
        private System.Windows.Forms.RadioButton crest_023_RadioButton;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.RadioButton trough_030_RadioButton;
        private System.Windows.Forms.RadioButton flat_030_RadioButton;
        private System.Windows.Forms.RadioButton crest_030_RadioButton;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.RadioButton trough_033_RadioButton;
        private System.Windows.Forms.RadioButton flat_033_RadioButton;
        private System.Windows.Forms.RadioButton crest_033_RadioButton;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.RadioButton trough_040_RadioButton;
        private System.Windows.Forms.RadioButton flat_040_RadioButton;
        private System.Windows.Forms.RadioButton crest_040_RadioButton;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.RadioButton trough_043_RadioButton;
        private System.Windows.Forms.RadioButton flat_043_RadioButton;
        private System.Windows.Forms.RadioButton crest_043_RadioButton;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.RadioButton trough_050_RadioButton;
        private System.Windows.Forms.RadioButton flat_050_RadioButton;
        private System.Windows.Forms.RadioButton crest_050_RadioButton;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.RadioButton trough_053_RadioButton;
        private System.Windows.Forms.RadioButton flat_053_RadioButton;
        private System.Windows.Forms.RadioButton crest_053_RadioButton;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.RadioButton trough_060_RadioButton;
        private System.Windows.Forms.RadioButton flat_060_RadioButton;
        private System.Windows.Forms.RadioButton crest_060_RadioButton;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.RadioButton trough_063_RadioButton;
        private System.Windows.Forms.RadioButton flat_063_RadioButton;
        private System.Windows.Forms.RadioButton crest_063_RadioButton;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.RadioButton trough_070_RadioButton;
        private System.Windows.Forms.RadioButton flat_070_RadioButton;
        private System.Windows.Forms.RadioButton crest_070_RadioButton;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.RadioButton trough_073_RadioButton;
        private System.Windows.Forms.RadioButton flat_073_RadioButton;
        private System.Windows.Forms.RadioButton crest_073_RadioButton;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.RadioButton trough_080_RadioButton;
        private System.Windows.Forms.RadioButton flat_080_RadioButton;
        private System.Windows.Forms.RadioButton crest_080_RadioButton;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.RadioButton trough_083_RadioButton;
        private System.Windows.Forms.RadioButton flat_083_RadioButton;
        private System.Windows.Forms.RadioButton crest_083_RadioButton;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.RadioButton trough_090_RadioButton;
        private System.Windows.Forms.RadioButton flat_090_RadioButton;
        private System.Windows.Forms.RadioButton crest_090_RadioButton;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.RadioButton trough_093_RadioButton;
        private System.Windows.Forms.RadioButton flat_093_RadioButton;
        private System.Windows.Forms.RadioButton crest_093_RadioButton;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.RadioButton trough_100_RadioButton;
        private System.Windows.Forms.RadioButton flat_100_RadioButton;
        private System.Windows.Forms.RadioButton crest_100_RadioButton;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.RadioButton trough_103_RadioButton;
        private System.Windows.Forms.RadioButton flat_103_RadioButton;
        private System.Windows.Forms.RadioButton crest_103_RadioButton;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.RadioButton trough_110_RadioButton;
        private System.Windows.Forms.RadioButton flat_110_RadioButton;
        private System.Windows.Forms.RadioButton crest_110_RadioButton;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.RadioButton trough_113_RadioButton;
        private System.Windows.Forms.RadioButton flat_113_RadioButton;
        private System.Windows.Forms.RadioButton crest_113_RadioButton;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.RadioButton trough_120_RadioButton;
        private System.Windows.Forms.RadioButton flat_120_RadioButton;
        private System.Windows.Forms.RadioButton crest_120_RadioButton;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.RadioButton trough_123_RadioButton;
        private System.Windows.Forms.RadioButton flat_123_RadioButton;
        private System.Windows.Forms.RadioButton crest_123_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_130_RadioButton;
        private System.Windows.Forms.RadioButton full_Air_130_RadioButton;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.RadioButton full_Air_133_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_133_RadioButton;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.RadioButton full_Air_140_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_140_RadioButton;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.RadioButton full_Air_143_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_143_RadioButton;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.RadioButton full_Air_150_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_150_RadioButton;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.RadioButton full_Air_153_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_153_RadioButton;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.RadioButton full_Air_160_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_160_RadioButton;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.RadioButton full_Air_163_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_163_RadioButton;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.RadioButton full_Air_170_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_170_RadioButton;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.RadioButton full_Air_173_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_173_RadioButton;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.RadioButton full_Air_180_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_180_RadioButton;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.RadioButton full_Air_183_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_183_RadioButton;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.RadioButton full_Air_190_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_190_RadioButton;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.RadioButton full_Air_193_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_193_RadioButton;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.RadioButton full_Air_200_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_200_RadioButton;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.RadioButton full_Air_203_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_203_RadioButton;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.RadioButton full_Air_210_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_210_RadioButton;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.RadioButton full_Air_213_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_213_RadioButton;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.RadioButton full_Air_220_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_220_RadioButton;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.RadioButton full_Air_223_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_223_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_230_RadioButton;
        private System.Windows.Forms.RadioButton full_Air_230_RadioButton;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.RadioButton full_Air_233_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_233_RadioButton;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.RadioButton full_Air_240_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_240_RadioButton;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.RadioButton full_Air_003_RadioButton;
        private System.Windows.Forms.RadioButton full_Ice_003_RadioButton;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.RadioButton part_Air_130_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_130_RadioButton;
        private System.Windows.Forms.Panel panel98;
        private System.Windows.Forms.RadioButton part_Air_133_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_133_RadioButton;
        private System.Windows.Forms.Panel panel99;
        private System.Windows.Forms.RadioButton part_Air_140_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_140_RadioButton;
        private System.Windows.Forms.Panel panel100;
        private System.Windows.Forms.RadioButton part_Air_143_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_143_RadioButton;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.RadioButton part_Air_150_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_150_RadioButton;
        private System.Windows.Forms.Panel panel102;
        private System.Windows.Forms.RadioButton part_Air_153_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_153_RadioButton;
        private System.Windows.Forms.Panel panel103;
        private System.Windows.Forms.RadioButton part_Air_160_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_160_RadioButton;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.RadioButton part_Air_163_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_163_RadioButton;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.RadioButton part_Air_170_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_170_RadioButton;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.RadioButton part_Air_173_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_173_RadioButton;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.RadioButton part_Air_180_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_180_RadioButton;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.RadioButton part_Air_183_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_183_RadioButton;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.RadioButton part_Air_190_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_190_RadioButton;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.RadioButton part_Air_193_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_193_RadioButton;
        private System.Windows.Forms.Panel panel111;
        private System.Windows.Forms.RadioButton part_Air_200_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_200_RadioButton;
        private System.Windows.Forms.Panel panel112;
        private System.Windows.Forms.RadioButton part_Air_203_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_203_RadioButton;
        private System.Windows.Forms.Panel panel113;
        private System.Windows.Forms.RadioButton part_Air_210_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_210_RadioButton;
        private System.Windows.Forms.Panel panel114;
        private System.Windows.Forms.RadioButton part_Air_213_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_213_RadioButton;
        private System.Windows.Forms.Panel panel115;
        private System.Windows.Forms.RadioButton part_Air_220_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_220_RadioButton;
        private System.Windows.Forms.Panel panel116;
        private System.Windows.Forms.RadioButton part_Air_223_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_223_RadioButton;
        private System.Windows.Forms.Panel panel117;
        private System.Windows.Forms.RadioButton part_Air_230_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_230_RadioButton;
        private System.Windows.Forms.Panel panel118;
        private System.Windows.Forms.RadioButton part_Air_233_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_233_RadioButton;
        private System.Windows.Forms.Panel panel119;
        private System.Windows.Forms.RadioButton part_Air_240_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_240_RadioButton;
        private System.Windows.Forms.Panel panel120;
        private System.Windows.Forms.RadioButton part_Air_003_RadioButton;
        private System.Windows.Forms.RadioButton part_Ice_003_RadioButton;
        private System.Windows.Forms.Panel panel121;
        private System.Windows.Forms.RadioButton trough_130_RadioButton;
        private System.Windows.Forms.RadioButton flat_130_RadioButton;
        private System.Windows.Forms.RadioButton crest_130_RadioButton;
        private System.Windows.Forms.Panel panel122;
        private System.Windows.Forms.RadioButton trough_133_RadioButton;
        private System.Windows.Forms.RadioButton flat_133_RadioButton;
        private System.Windows.Forms.RadioButton crest_133_RadioButton;
        private System.Windows.Forms.Panel panel123;
        private System.Windows.Forms.RadioButton trough_140_RadioButton;
        private System.Windows.Forms.RadioButton flat_140_RadioButton;
        private System.Windows.Forms.RadioButton crest_140_RadioButton;
        private System.Windows.Forms.Panel panel124;
        private System.Windows.Forms.RadioButton trough_143_RadioButton;
        private System.Windows.Forms.RadioButton flat_143_RadioButton;
        private System.Windows.Forms.RadioButton crest_143_RadioButton;
        private System.Windows.Forms.Panel panel125;
        private System.Windows.Forms.RadioButton trough_150_RadioButton;
        private System.Windows.Forms.RadioButton flat_150_RadioButton;
        private System.Windows.Forms.RadioButton crest_150_RadioButton;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.RadioButton trough_153_RadioButton;
        private System.Windows.Forms.RadioButton flat_153_RadioButton;
        private System.Windows.Forms.RadioButton crest_153_RadioButton;
        private System.Windows.Forms.Panel panel127;
        private System.Windows.Forms.RadioButton trough_160_RadioButton;
        private System.Windows.Forms.RadioButton flat_160_RadioButton;
        private System.Windows.Forms.RadioButton crest_160_RadioButton;
        private System.Windows.Forms.Panel panel128;
        private System.Windows.Forms.RadioButton trough_163_RadioButton;
        private System.Windows.Forms.RadioButton flat_163_RadioButton;
        private System.Windows.Forms.RadioButton crest_163_RadioButton;
        private System.Windows.Forms.Panel panel129;
        private System.Windows.Forms.RadioButton trough_170_RadioButton;
        private System.Windows.Forms.RadioButton flat_170_RadioButton;
        private System.Windows.Forms.RadioButton crest_170_RadioButton;
        private System.Windows.Forms.Panel panel130;
        private System.Windows.Forms.RadioButton trough_173_RadioButton;
        private System.Windows.Forms.RadioButton flat_173_RadioButton;
        private System.Windows.Forms.RadioButton crest_173_RadioButton;
        private System.Windows.Forms.Panel panel131;
        private System.Windows.Forms.RadioButton trough_180_RadioButton;
        private System.Windows.Forms.RadioButton flat_180_RadioButton;
        private System.Windows.Forms.RadioButton crest_180_RadioButton;
        private System.Windows.Forms.Panel panel132;
        private System.Windows.Forms.RadioButton trough_183_RadioButton;
        private System.Windows.Forms.RadioButton flat_183_RadioButton;
        private System.Windows.Forms.RadioButton crest_183_RadioButton;
        private System.Windows.Forms.Panel panel133;
        private System.Windows.Forms.RadioButton trough_190_RadioButton;
        private System.Windows.Forms.RadioButton flat_190_RadioButton;
        private System.Windows.Forms.RadioButton crest_190_RadioButton;
        private System.Windows.Forms.Panel panel134;
        private System.Windows.Forms.RadioButton trough_193_RadioButton;
        private System.Windows.Forms.RadioButton flat_193_RadioButton;
        private System.Windows.Forms.RadioButton crest_193_RadioButton;
        private System.Windows.Forms.Panel panel135;
        private System.Windows.Forms.RadioButton trough_200_RadioButton;
        private System.Windows.Forms.RadioButton flat_200_RadioButton;
        private System.Windows.Forms.RadioButton crest_200_RadioButton;
        private System.Windows.Forms.Panel panel136;
        private System.Windows.Forms.RadioButton trough_203_RadioButton;
        private System.Windows.Forms.RadioButton flat_203_RadioButton;
        private System.Windows.Forms.RadioButton crest_203_RadioButton;
        private System.Windows.Forms.Panel panel137;
        private System.Windows.Forms.RadioButton trough_210_RadioButton;
        private System.Windows.Forms.RadioButton flat_210_RadioButton;
        private System.Windows.Forms.RadioButton crest_210_RadioButton;
        private System.Windows.Forms.Panel panel138;
        private System.Windows.Forms.RadioButton trough_213_RadioButton;
        private System.Windows.Forms.RadioButton flat_213_RadioButton;
        private System.Windows.Forms.RadioButton crest_213_RadioButton;
        private System.Windows.Forms.Panel panel139;
        private System.Windows.Forms.RadioButton trough_220_RadioButton;
        private System.Windows.Forms.RadioButton flat_220_RadioButton;
        private System.Windows.Forms.RadioButton crest_220_RadioButton;
        private System.Windows.Forms.Panel panel140;
        private System.Windows.Forms.RadioButton trough_223_RadioButton;
        private System.Windows.Forms.RadioButton flat_223_RadioButton;
        private System.Windows.Forms.RadioButton crest_223_RadioButton;
        private System.Windows.Forms.Panel panel141;
        private System.Windows.Forms.RadioButton trough_230_RadioButton;
        private System.Windows.Forms.RadioButton flat_230_RadioButton;
        private System.Windows.Forms.RadioButton crest_230_RadioButton;
        private System.Windows.Forms.Panel panel142;
        private System.Windows.Forms.RadioButton trough_233_RadioButton;
        private System.Windows.Forms.RadioButton flat_233_RadioButton;
        private System.Windows.Forms.RadioButton crest_233_RadioButton;
        private System.Windows.Forms.Panel panel143;
        private System.Windows.Forms.RadioButton trough_240_RadioButton;
        private System.Windows.Forms.RadioButton flat_240_RadioButton;
        private System.Windows.Forms.RadioButton crest_240_RadioButton;
        private System.Windows.Forms.Panel panel144;
        private System.Windows.Forms.RadioButton trough_003_RadioButton;
        private System.Windows.Forms.RadioButton flat_003_RadioButton;
        private System.Windows.Forms.RadioButton crest_003_RadioButton;
        public System.Windows.Forms.TextBox full_Ice_Hour_TextBox;
    }
}