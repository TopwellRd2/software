﻿namespace Ice_storage_evaluation
{
    partial class Form_Plan_Cost_Analysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.traditional_TextBox1 = new System.Windows.Forms.TextBox();
            this.full_TextBox1 = new System.Windows.Forms.TextBox();
            this.part_TextBox1 = new System.Windows.Forms.TextBox();
            this.part_TextBox2 = new System.Windows.Forms.TextBox();
            this.full_TextBox2 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox2 = new System.Windows.Forms.TextBox();
            this.part_TextBox3 = new System.Windows.Forms.TextBox();
            this.full_TextBox3 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox3 = new System.Windows.Forms.TextBox();
            this.part_TextBox4 = new System.Windows.Forms.TextBox();
            this.full_TextBox4 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox4 = new System.Windows.Forms.TextBox();
            this.part_TextBox5 = new System.Windows.Forms.TextBox();
            this.full_TextBox5 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox5 = new System.Windows.Forms.TextBox();
            this.part_TextBox6 = new System.Windows.Forms.TextBox();
            this.full_TextBox6 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox6 = new System.Windows.Forms.TextBox();
            this.part_TextBox7 = new System.Windows.Forms.TextBox();
            this.full_TextBox7 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox7 = new System.Windows.Forms.TextBox();
            this.part_TextBox8 = new System.Windows.Forms.TextBox();
            this.full_TextBox8 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox8 = new System.Windows.Forms.TextBox();
            this.part_TextBox9 = new System.Windows.Forms.TextBox();
            this.full_TextBox9 = new System.Windows.Forms.TextBox();
            this.traditional_TextBox9 = new System.Windows.Forms.TextBox();
            this.three_part_TextBox = new System.Windows.Forms.TextBox();
            this.three_full_TextBox = new System.Windows.Forms.TextBox();
            this.two_part_TextBox = new System.Windows.Forms.TextBox();
            this.two_full_TextBox = new System.Windows.Forms.TextBox();
            this.auto_Button = new System.Windows.Forms.Button();
            this.ok_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label1.Location = new System.Drawing.Point(227, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "傳統空調";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label2.Location = new System.Drawing.Point(457, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "全量空調";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label3.Location = new System.Drawing.Point(687, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "分量空調";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label4.Location = new System.Drawing.Point(45, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "機械設備";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label5.Location = new System.Drawing.Point(45, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "儲冰設備";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label6.Location = new System.Drawing.Point(45, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "配管工程";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label7.Location = new System.Drawing.Point(45, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 29);
            this.label7.TabIndex = 6;
            this.label7.Text = "配電工程";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label8.Location = new System.Drawing.Point(45, 237);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 29);
            this.label8.TabIndex = 7;
            this.label8.Text = "自動控制";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label9.Location = new System.Drawing.Point(45, 277);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 29);
            this.label9.TabIndex = 8;
            this.label9.Text = "定位安裝";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label10.Location = new System.Drawing.Point(28, 317);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(142, 29);
            this.label10.TabIndex = 9;
            this.label10.Text = "利潤管理費";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label11.Location = new System.Drawing.Point(45, 357);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 29);
            this.label11.TabIndex = 10;
            this.label11.Text = "總價";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label12.Location = new System.Drawing.Point(45, 397);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 29);
            this.label12.TabIndex = 11;
            this.label12.Text = "設計費";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label14.Location = new System.Drawing.Point(12, 460);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(245, 29);
            this.label14.TabIndex = 13;
            this.label14.Text = "投資費用與電價差";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label13.Location = new System.Drawing.Point(12, 489);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(245, 29);
            this.label13.TabIndex = 14;
            this.label13.Text = "回收平衡性(年)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label15.Location = new System.Drawing.Point(296, 489);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 29);
            this.label15.TabIndex = 15;
            this.label15.Text = "二段式";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label16.Location = new System.Drawing.Point(296, 528);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(125, 29);
            this.label16.TabIndex = 16;
            this.label16.Text = "三段式";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // traditional_TextBox1
            // 
            this.traditional_TextBox1.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox1.Location = new System.Drawing.Point(194, 77);
            this.traditional_TextBox1.Name = "traditional_TextBox1";
            this.traditional_TextBox1.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox1.TabIndex = 17;
            this.traditional_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox1
            // 
            this.full_TextBox1.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox1.Location = new System.Drawing.Point(427, 77);
            this.full_TextBox1.Name = "full_TextBox1";
            this.full_TextBox1.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox1.TabIndex = 18;
            this.full_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox1
            // 
            this.part_TextBox1.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox1.Location = new System.Drawing.Point(663, 77);
            this.part_TextBox1.Name = "part_TextBox1";
            this.part_TextBox1.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox1.TabIndex = 19;
            this.part_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox2
            // 
            this.part_TextBox2.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox2.Location = new System.Drawing.Point(663, 117);
            this.part_TextBox2.Name = "part_TextBox2";
            this.part_TextBox2.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox2.TabIndex = 22;
            this.part_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox2
            // 
            this.full_TextBox2.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox2.Location = new System.Drawing.Point(427, 117);
            this.full_TextBox2.Name = "full_TextBox2";
            this.full_TextBox2.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox2.TabIndex = 21;
            this.full_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox2
            // 
            this.traditional_TextBox2.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox2.Location = new System.Drawing.Point(194, 117);
            this.traditional_TextBox2.Name = "traditional_TextBox2";
            this.traditional_TextBox2.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox2.TabIndex = 20;
            this.traditional_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox3
            // 
            this.part_TextBox3.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox3.Location = new System.Drawing.Point(663, 157);
            this.part_TextBox3.Name = "part_TextBox3";
            this.part_TextBox3.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox3.TabIndex = 25;
            this.part_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox3
            // 
            this.full_TextBox3.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox3.Location = new System.Drawing.Point(427, 157);
            this.full_TextBox3.Name = "full_TextBox3";
            this.full_TextBox3.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox3.TabIndex = 24;
            this.full_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox3
            // 
            this.traditional_TextBox3.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox3.Location = new System.Drawing.Point(194, 157);
            this.traditional_TextBox3.Name = "traditional_TextBox3";
            this.traditional_TextBox3.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox3.TabIndex = 23;
            this.traditional_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox4
            // 
            this.part_TextBox4.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox4.Location = new System.Drawing.Point(663, 197);
            this.part_TextBox4.Name = "part_TextBox4";
            this.part_TextBox4.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox4.TabIndex = 28;
            this.part_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox4
            // 
            this.full_TextBox4.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox4.Location = new System.Drawing.Point(427, 197);
            this.full_TextBox4.Name = "full_TextBox4";
            this.full_TextBox4.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox4.TabIndex = 27;
            this.full_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox4
            // 
            this.traditional_TextBox4.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox4.Location = new System.Drawing.Point(194, 197);
            this.traditional_TextBox4.Name = "traditional_TextBox4";
            this.traditional_TextBox4.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox4.TabIndex = 26;
            this.traditional_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox5
            // 
            this.part_TextBox5.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox5.Location = new System.Drawing.Point(663, 237);
            this.part_TextBox5.Name = "part_TextBox5";
            this.part_TextBox5.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox5.TabIndex = 31;
            this.part_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox5
            // 
            this.full_TextBox5.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox5.Location = new System.Drawing.Point(427, 237);
            this.full_TextBox5.Name = "full_TextBox5";
            this.full_TextBox5.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox5.TabIndex = 30;
            this.full_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox5
            // 
            this.traditional_TextBox5.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox5.Location = new System.Drawing.Point(194, 237);
            this.traditional_TextBox5.Name = "traditional_TextBox5";
            this.traditional_TextBox5.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox5.TabIndex = 29;
            this.traditional_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox6
            // 
            this.part_TextBox6.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox6.Location = new System.Drawing.Point(663, 277);
            this.part_TextBox6.Name = "part_TextBox6";
            this.part_TextBox6.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox6.TabIndex = 34;
            this.part_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox6
            // 
            this.full_TextBox6.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox6.Location = new System.Drawing.Point(427, 277);
            this.full_TextBox6.Name = "full_TextBox6";
            this.full_TextBox6.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox6.TabIndex = 33;
            this.full_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox6
            // 
            this.traditional_TextBox6.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox6.Location = new System.Drawing.Point(194, 277);
            this.traditional_TextBox6.Name = "traditional_TextBox6";
            this.traditional_TextBox6.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox6.TabIndex = 32;
            this.traditional_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox7
            // 
            this.part_TextBox7.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox7.Location = new System.Drawing.Point(663, 317);
            this.part_TextBox7.Name = "part_TextBox7";
            this.part_TextBox7.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox7.TabIndex = 37;
            this.part_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox7
            // 
            this.full_TextBox7.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox7.Location = new System.Drawing.Point(427, 317);
            this.full_TextBox7.Name = "full_TextBox7";
            this.full_TextBox7.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox7.TabIndex = 36;
            this.full_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox7
            // 
            this.traditional_TextBox7.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox7.Location = new System.Drawing.Point(194, 317);
            this.traditional_TextBox7.Name = "traditional_TextBox7";
            this.traditional_TextBox7.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox7.TabIndex = 35;
            this.traditional_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox8
            // 
            this.part_TextBox8.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox8.Location = new System.Drawing.Point(663, 357);
            this.part_TextBox8.Name = "part_TextBox8";
            this.part_TextBox8.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox8.TabIndex = 40;
            this.part_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox8
            // 
            this.full_TextBox8.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox8.Location = new System.Drawing.Point(427, 357);
            this.full_TextBox8.Name = "full_TextBox8";
            this.full_TextBox8.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox8.TabIndex = 39;
            this.full_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox8
            // 
            this.traditional_TextBox8.BackColor = System.Drawing.SystemColors.Info;
            this.traditional_TextBox8.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox8.Location = new System.Drawing.Point(194, 357);
            this.traditional_TextBox8.Name = "traditional_TextBox8";
            this.traditional_TextBox8.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox8.TabIndex = 38;
            this.traditional_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // part_TextBox9
            // 
            this.part_TextBox9.Font = new System.Drawing.Font("新細明體", 11F);
            this.part_TextBox9.Location = new System.Drawing.Point(663, 397);
            this.part_TextBox9.Name = "part_TextBox9";
            this.part_TextBox9.Size = new System.Drawing.Size(185, 29);
            this.part_TextBox9.TabIndex = 43;
            this.part_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // full_TextBox9
            // 
            this.full_TextBox9.Font = new System.Drawing.Font("新細明體", 11F);
            this.full_TextBox9.Location = new System.Drawing.Point(427, 397);
            this.full_TextBox9.Name = "full_TextBox9";
            this.full_TextBox9.Size = new System.Drawing.Size(185, 29);
            this.full_TextBox9.TabIndex = 42;
            this.full_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // traditional_TextBox9
            // 
            this.traditional_TextBox9.Font = new System.Drawing.Font("新細明體", 11F);
            this.traditional_TextBox9.Location = new System.Drawing.Point(194, 397);
            this.traditional_TextBox9.Name = "traditional_TextBox9";
            this.traditional_TextBox9.Size = new System.Drawing.Size(185, 29);
            this.traditional_TextBox9.TabIndex = 41;
            this.traditional_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_part_TextBox
            // 
            this.three_part_TextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.three_part_TextBox.Font = new System.Drawing.Font("新細明體", 11F);
            this.three_part_TextBox.Location = new System.Drawing.Point(663, 528);
            this.three_part_TextBox.Name = "three_part_TextBox";
            this.three_part_TextBox.Size = new System.Drawing.Size(185, 29);
            this.three_part_TextBox.TabIndex = 47;
            this.three_part_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_full_TextBox
            // 
            this.three_full_TextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.three_full_TextBox.Font = new System.Drawing.Font("新細明體", 11F);
            this.three_full_TextBox.Location = new System.Drawing.Point(427, 528);
            this.three_full_TextBox.Name = "three_full_TextBox";
            this.three_full_TextBox.Size = new System.Drawing.Size(185, 29);
            this.three_full_TextBox.TabIndex = 46;
            this.three_full_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_part_TextBox
            // 
            this.two_part_TextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.two_part_TextBox.Font = new System.Drawing.Font("新細明體", 11F);
            this.two_part_TextBox.Location = new System.Drawing.Point(663, 488);
            this.two_part_TextBox.Name = "two_part_TextBox";
            this.two_part_TextBox.Size = new System.Drawing.Size(185, 29);
            this.two_part_TextBox.TabIndex = 45;
            this.two_part_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_full_TextBox
            // 
            this.two_full_TextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.two_full_TextBox.Font = new System.Drawing.Font("新細明體", 11F);
            this.two_full_TextBox.Location = new System.Drawing.Point(427, 488);
            this.two_full_TextBox.Name = "two_full_TextBox";
            this.two_full_TextBox.Size = new System.Drawing.Size(185, 29);
            this.two_full_TextBox.TabIndex = 44;
            this.two_full_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // auto_Button
            // 
            this.auto_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.auto_Button.Location = new System.Drawing.Point(282, 582);
            this.auto_Button.Name = "auto_Button";
            this.auto_Button.Size = new System.Drawing.Size(280, 64);
            this.auto_Button.TabIndex = 48;
            this.auto_Button.Text = "自動計算";
            this.auto_Button.UseVisualStyleBackColor = true;
            this.auto_Button.Click += new System.EventHandler(this.auto_Button_Click);
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ok_Button.Location = new System.Drawing.Point(568, 582);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(280, 64);
            this.ok_Button.TabIndex = 49;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // Form_Plan_Cost_Analysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(860, 663);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.auto_Button);
            this.Controls.Add(this.three_part_TextBox);
            this.Controls.Add(this.three_full_TextBox);
            this.Controls.Add(this.two_part_TextBox);
            this.Controls.Add(this.two_full_TextBox);
            this.Controls.Add(this.part_TextBox9);
            this.Controls.Add(this.full_TextBox9);
            this.Controls.Add(this.traditional_TextBox9);
            this.Controls.Add(this.part_TextBox8);
            this.Controls.Add(this.full_TextBox8);
            this.Controls.Add(this.traditional_TextBox8);
            this.Controls.Add(this.part_TextBox7);
            this.Controls.Add(this.full_TextBox7);
            this.Controls.Add(this.traditional_TextBox7);
            this.Controls.Add(this.part_TextBox6);
            this.Controls.Add(this.full_TextBox6);
            this.Controls.Add(this.traditional_TextBox6);
            this.Controls.Add(this.part_TextBox5);
            this.Controls.Add(this.full_TextBox5);
            this.Controls.Add(this.traditional_TextBox5);
            this.Controls.Add(this.part_TextBox4);
            this.Controls.Add(this.full_TextBox4);
            this.Controls.Add(this.traditional_TextBox4);
            this.Controls.Add(this.part_TextBox3);
            this.Controls.Add(this.full_TextBox3);
            this.Controls.Add(this.traditional_TextBox3);
            this.Controls.Add(this.part_TextBox2);
            this.Controls.Add(this.full_TextBox2);
            this.Controls.Add(this.traditional_TextBox2);
            this.Controls.Add(this.part_TextBox1);
            this.Controls.Add(this.full_TextBox1);
            this.Controls.Add(this.traditional_TextBox1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Plan_Cost_Analysis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "計劃費用分析";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Plan_Cost_Analysis_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Plan_Cost_Analysis_Closed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox traditional_TextBox1;
        private System.Windows.Forms.TextBox full_TextBox1;
        private System.Windows.Forms.TextBox part_TextBox1;
        private System.Windows.Forms.TextBox part_TextBox2;
        private System.Windows.Forms.TextBox full_TextBox2;
        private System.Windows.Forms.TextBox traditional_TextBox2;
        private System.Windows.Forms.TextBox part_TextBox3;
        private System.Windows.Forms.TextBox full_TextBox3;
        private System.Windows.Forms.TextBox traditional_TextBox3;
        private System.Windows.Forms.TextBox part_TextBox4;
        private System.Windows.Forms.TextBox full_TextBox4;
        private System.Windows.Forms.TextBox traditional_TextBox4;
        private System.Windows.Forms.TextBox part_TextBox5;
        private System.Windows.Forms.TextBox full_TextBox5;
        private System.Windows.Forms.TextBox traditional_TextBox5;
        private System.Windows.Forms.TextBox part_TextBox6;
        private System.Windows.Forms.TextBox full_TextBox6;
        private System.Windows.Forms.TextBox traditional_TextBox6;
        private System.Windows.Forms.TextBox part_TextBox7;
        private System.Windows.Forms.TextBox full_TextBox7;
        private System.Windows.Forms.TextBox traditional_TextBox7;
        private System.Windows.Forms.TextBox part_TextBox8;
        private System.Windows.Forms.TextBox full_TextBox8;
        private System.Windows.Forms.TextBox traditional_TextBox8;
        private System.Windows.Forms.TextBox part_TextBox9;
        private System.Windows.Forms.TextBox full_TextBox9;
        private System.Windows.Forms.TextBox traditional_TextBox9;
        private System.Windows.Forms.TextBox three_part_TextBox;
        private System.Windows.Forms.TextBox three_full_TextBox;
        private System.Windows.Forms.TextBox two_part_TextBox;
        private System.Windows.Forms.TextBox two_full_TextBox;
        private System.Windows.Forms.Button auto_Button;
        private System.Windows.Forms.Button ok_Button;
    }
}