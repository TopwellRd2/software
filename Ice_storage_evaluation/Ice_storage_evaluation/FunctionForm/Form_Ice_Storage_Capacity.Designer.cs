﻿namespace Ice_storage_evaluation
{
    partial class Form_Ice_Storage_Capacity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.to30_RadioButton = new System.Windows.Forms.RadioButton();
            this.over100_RadioButton = new System.Windows.Forms.RadioButton();
            this.to50_RadioButton = new System.Windows.Forms.RadioButton();
            this.to100_RadioButton = new System.Windows.Forms.RadioButton();
            this.to20_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_RadioButton = new System.Windows.Forms.RadioButton();
            this.to14_RadioButton = new System.Windows.Forms.RadioButton();
            this.to8_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_GroupBox = new System.Windows.Forms.GroupBox();
            this.set_TextBox = new System.Windows.Forms.TextBox();
            this.ok_Button = new System.Windows.Forms.Button();
            this.choose_GroupBox.SuspendLayout();
            this.set_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // choose_GroupBox
            // 
            this.choose_GroupBox.Controls.Add(this.to30_RadioButton);
            this.choose_GroupBox.Controls.Add(this.over100_RadioButton);
            this.choose_GroupBox.Controls.Add(this.to50_RadioButton);
            this.choose_GroupBox.Controls.Add(this.to100_RadioButton);
            this.choose_GroupBox.Controls.Add(this.to20_RadioButton);
            this.choose_GroupBox.Controls.Add(this.set_RadioButton);
            this.choose_GroupBox.Controls.Add(this.to14_RadioButton);
            this.choose_GroupBox.Controls.Add(this.to8_RadioButton);
            this.choose_GroupBox.Location = new System.Drawing.Point(34, 23);
            this.choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Name = "choose_GroupBox";
            this.choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Size = new System.Drawing.Size(647, 249);
            this.choose_GroupBox.TabIndex = 11;
            this.choose_GroupBox.TabStop = false;
            // 
            // to30_RadioButton
            // 
            this.to30_RadioButton.AutoSize = true;
            this.to30_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to30_RadioButton.Location = new System.Drawing.Point(48, 193);
            this.to30_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to30_RadioButton.Name = "to30_RadioButton";
            this.to30_RadioButton.Size = new System.Drawing.Size(259, 35);
            this.to30_RadioButton.TabIndex = 13;
            this.to30_RadioButton.Text = "2000 ~ 3000 RT-HR";
            this.to30_RadioButton.UseVisualStyleBackColor = true;
            this.to30_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // over100_RadioButton
            // 
            this.over100_RadioButton.AutoSize = true;
            this.over100_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.over100_RadioButton.Location = new System.Drawing.Point(349, 138);
            this.over100_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.over100_RadioButton.Name = "over100_RadioButton";
            this.over100_RadioButton.Size = new System.Drawing.Size(243, 35);
            this.over100_RadioButton.TabIndex = 12;
            this.over100_RadioButton.Text = "10000 RT-HR 以上";
            this.over100_RadioButton.UseVisualStyleBackColor = true;
            this.over100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to50_RadioButton
            // 
            this.to50_RadioButton.AutoSize = true;
            this.to50_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to50_RadioButton.Location = new System.Drawing.Point(349, 28);
            this.to50_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to50_RadioButton.Name = "to50_RadioButton";
            this.to50_RadioButton.Size = new System.Drawing.Size(259, 35);
            this.to50_RadioButton.TabIndex = 11;
            this.to50_RadioButton.Text = "3000 ~ 5000 RT-HR";
            this.to50_RadioButton.UseVisualStyleBackColor = true;
            this.to50_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to100_RadioButton
            // 
            this.to100_RadioButton.AutoSize = true;
            this.to100_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to100_RadioButton.Location = new System.Drawing.Point(349, 83);
            this.to100_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to100_RadioButton.Name = "to100_RadioButton";
            this.to100_RadioButton.Size = new System.Drawing.Size(273, 35);
            this.to100_RadioButton.TabIndex = 10;
            this.to100_RadioButton.Text = "5000 ~ 10000 RT-HR";
            this.to100_RadioButton.UseVisualStyleBackColor = true;
            this.to100_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to20_RadioButton
            // 
            this.to20_RadioButton.AutoSize = true;
            this.to20_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to20_RadioButton.Location = new System.Drawing.Point(48, 138);
            this.to20_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to20_RadioButton.Name = "to20_RadioButton";
            this.to20_RadioButton.Size = new System.Drawing.Size(259, 35);
            this.to20_RadioButton.TabIndex = 9;
            this.to20_RadioButton.Text = "1400 ~ 2000 RT-HR";
            this.to20_RadioButton.UseVisualStyleBackColor = true;
            this.to20_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // set_RadioButton
            // 
            this.set_RadioButton.AutoSize = true;
            this.set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_RadioButton.Location = new System.Drawing.Point(349, 193);
            this.set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_RadioButton.Name = "set_RadioButton";
            this.set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.set_RadioButton.TabIndex = 8;
            this.set_RadioButton.Text = "自行設定";
            this.set_RadioButton.UseVisualStyleBackColor = true;
            this.set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to14_RadioButton
            // 
            this.to14_RadioButton.AutoSize = true;
            this.to14_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to14_RadioButton.Location = new System.Drawing.Point(48, 83);
            this.to14_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to14_RadioButton.Name = "to14_RadioButton";
            this.to14_RadioButton.Size = new System.Drawing.Size(245, 35);
            this.to14_RadioButton.TabIndex = 7;
            this.to14_RadioButton.Text = "800 ~ 1400 RT-HR";
            this.to14_RadioButton.UseVisualStyleBackColor = true;
            this.to14_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to8_RadioButton
            // 
            this.to8_RadioButton.AutoSize = true;
            this.to8_RadioButton.Checked = true;
            this.to8_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to8_RadioButton.Location = new System.Drawing.Point(48, 28);
            this.to8_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to8_RadioButton.Name = "to8_RadioButton";
            this.to8_RadioButton.Size = new System.Drawing.Size(201, 35);
            this.to8_RadioButton.TabIndex = 6;
            this.to8_RadioButton.TabStop = true;
            this.to8_RadioButton.Text = ".. < 800 RT-HR";
            this.to8_RadioButton.UseVisualStyleBackColor = true;
            this.to8_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // set_GroupBox
            // 
            this.set_GroupBox.Controls.Add(this.set_TextBox);
            this.set_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.set_GroupBox.Location = new System.Drawing.Point(34, 280);
            this.set_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_GroupBox.Name = "set_GroupBox";
            this.set_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_GroupBox.Size = new System.Drawing.Size(327, 106);
            this.set_GroupBox.TabIndex = 10;
            this.set_GroupBox.TabStop = false;
            this.set_GroupBox.Text = "自行設定";
            // 
            // set_TextBox
            // 
            this.set_TextBox.Enabled = false;
            this.set_TextBox.Location = new System.Drawing.Point(48, 45);
            this.set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_TextBox.Name = "set_TextBox";
            this.set_TextBox.Size = new System.Drawing.Size(239, 41);
            this.set_TextBox.TabIndex = 0;
            this.set_TextBox.Text = "0.2";
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.ok_Button.Location = new System.Drawing.Point(407, 300);
            this.ok_Button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(274, 86);
            this.ok_Button.TabIndex = 12;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // Form_Ice_Storage_Capacity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(721, 413);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.choose_GroupBox);
            this.Controls.Add(this.set_GroupBox);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Ice_Storage_Capacity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "儲冰容量設計";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Ice_Storage_Capacity_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Ice_Storage_Capacity_Closed);
            this.choose_GroupBox.ResumeLayout(false);
            this.choose_GroupBox.PerformLayout();
            this.set_GroupBox.ResumeLayout(false);
            this.set_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox choose_GroupBox;
        private System.Windows.Forms.RadioButton to20_RadioButton;
        private System.Windows.Forms.RadioButton set_RadioButton;
        private System.Windows.Forms.RadioButton to14_RadioButton;
        private System.Windows.Forms.RadioButton to8_RadioButton;
        private System.Windows.Forms.GroupBox set_GroupBox;
        private System.Windows.Forms.TextBox set_TextBox;
        private System.Windows.Forms.RadioButton to30_RadioButton;
        private System.Windows.Forms.RadioButton over100_RadioButton;
        private System.Windows.Forms.RadioButton to50_RadioButton;
        private System.Windows.Forms.RadioButton to100_RadioButton;
        private System.Windows.Forms.Button ok_Button;
    }
}