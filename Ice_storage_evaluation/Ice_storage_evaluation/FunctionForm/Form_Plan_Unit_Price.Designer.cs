﻿namespace Ice_storage_evaluation
{
    partial class Form_Plan_Unit_Price
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.air_Choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.air_Set_TextBox = new System.Windows.Forms.TextBox();
            this.air_40_RadioButton = new System.Windows.Forms.RadioButton();
            this.air_Set_RadioButton = new System.Windows.Forms.RadioButton();
            this.air_35_RadioButton = new System.Windows.Forms.RadioButton();
            this.air_30_RadioButton = new System.Windows.Forms.RadioButton();
            this.high_Choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.out_Set_TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.high_Set_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.high_Set_RadioButton = new System.Windows.Forms.RadioButton();
            this.high_N_RadioButton = new System.Windows.Forms.RadioButton();
            this.high_Y_RadioButton = new System.Windows.Forms.RadioButton();
            this.ice_Choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.ice_Set_TextBox = new System.Windows.Forms.TextBox();
            this.ice_Set_RadioButton = new System.Windows.Forms.RadioButton();
            this.ice_20_RadioButton = new System.Windows.Forms.RadioButton();
            this.ice_18_RadioButton = new System.Windows.Forms.RadioButton();
            this.auto_Choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.auto_Set_TextBox = new System.Windows.Forms.TextBox();
            this.auto_Set_RadioButton = new System.Windows.Forms.RadioButton();
            this.auto_N_RadioButton = new System.Windows.Forms.RadioButton();
            this.auto_Y_RadioButton = new System.Windows.Forms.RadioButton();
            this.design_Choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.design_Set_TextBox = new System.Windows.Forms.TextBox();
            this.design_Set_RadioButton = new System.Windows.Forms.RadioButton();
            this.disign_N_RadioButton = new System.Windows.Forms.RadioButton();
            this.disign_Y_RadioButton = new System.Windows.Forms.RadioButton();
            this.ok_Button = new System.Windows.Forms.Button();
            this.air_Choose_GroupBox.SuspendLayout();
            this.high_Choose_GroupBox.SuspendLayout();
            this.ice_Choose_GroupBox.SuspendLayout();
            this.auto_Choose_GroupBox.SuspendLayout();
            this.design_Choose_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label1.Location = new System.Drawing.Point(403, 601);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 40);
            this.label1.TabIndex = 5;
            this.label1.Text = "目前幣制:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label2.Location = new System.Drawing.Point(529, 601);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 40);
            this.label2.TabIndex = 6;
            this.label2.Text = "NTD";
            // 
            // air_Choose_GroupBox
            // 
            this.air_Choose_GroupBox.Controls.Add(this.air_Set_TextBox);
            this.air_Choose_GroupBox.Controls.Add(this.air_40_RadioButton);
            this.air_Choose_GroupBox.Controls.Add(this.air_Set_RadioButton);
            this.air_Choose_GroupBox.Controls.Add(this.air_35_RadioButton);
            this.air_Choose_GroupBox.Controls.Add(this.air_30_RadioButton);
            this.air_Choose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_Choose_GroupBox.Location = new System.Drawing.Point(12, 13);
            this.air_Choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_Choose_GroupBox.Name = "air_Choose_GroupBox";
            this.air_Choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_Choose_GroupBox.Size = new System.Drawing.Size(536, 229);
            this.air_Choose_GroupBox.TabIndex = 10;
            this.air_Choose_GroupBox.TabStop = false;
            this.air_Choose_GroupBox.Text = "傳統空調計劃單價";
            // 
            // air_Set_TextBox
            // 
            this.air_Set_TextBox.Enabled = false;
            this.air_Set_TextBox.Location = new System.Drawing.Point(296, 101);
            this.air_Set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_Set_TextBox.Name = "air_Set_TextBox";
            this.air_Set_TextBox.Size = new System.Drawing.Size(211, 41);
            this.air_Set_TextBox.TabIndex = 10;
            this.air_Set_TextBox.Text = "30000";
            // 
            // air_40_RadioButton
            // 
            this.air_40_RadioButton.AutoSize = true;
            this.air_40_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_40_RadioButton.Location = new System.Drawing.Point(68, 168);
            this.air_40_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_40_RadioButton.Name = "air_40_RadioButton";
            this.air_40_RadioButton.Size = new System.Drawing.Size(105, 35);
            this.air_40_RadioButton.TabIndex = 9;
            this.air_40_RadioButton.Text = "40000";
            this.air_40_RadioButton.UseVisualStyleBackColor = true;
            this.air_40_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // air_Set_RadioButton
            // 
            this.air_Set_RadioButton.AutoSize = true;
            this.air_Set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_Set_RadioButton.Location = new System.Drawing.Point(268, 58);
            this.air_Set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_Set_RadioButton.Name = "air_Set_RadioButton";
            this.air_Set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.air_Set_RadioButton.TabIndex = 8;
            this.air_Set_RadioButton.Text = "自行設定";
            this.air_Set_RadioButton.UseVisualStyleBackColor = true;
            this.air_Set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // air_35_RadioButton
            // 
            this.air_35_RadioButton.AutoSize = true;
            this.air_35_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_35_RadioButton.Location = new System.Drawing.Point(68, 113);
            this.air_35_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_35_RadioButton.Name = "air_35_RadioButton";
            this.air_35_RadioButton.Size = new System.Drawing.Size(105, 35);
            this.air_35_RadioButton.TabIndex = 7;
            this.air_35_RadioButton.Text = "35000";
            this.air_35_RadioButton.UseVisualStyleBackColor = true;
            this.air_35_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // air_30_RadioButton
            // 
            this.air_30_RadioButton.AutoSize = true;
            this.air_30_RadioButton.Checked = true;
            this.air_30_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_30_RadioButton.Location = new System.Drawing.Point(68, 58);
            this.air_30_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_30_RadioButton.Name = "air_30_RadioButton";
            this.air_30_RadioButton.Size = new System.Drawing.Size(105, 35);
            this.air_30_RadioButton.TabIndex = 6;
            this.air_30_RadioButton.TabStop = true;
            this.air_30_RadioButton.Text = "30000";
            this.air_30_RadioButton.UseVisualStyleBackColor = true;
            this.air_30_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // high_Choose_GroupBox
            // 
            this.high_Choose_GroupBox.Controls.Add(this.out_Set_TextBox);
            this.high_Choose_GroupBox.Controls.Add(this.label4);
            this.high_Choose_GroupBox.Controls.Add(this.high_Set_TextBox);
            this.high_Choose_GroupBox.Controls.Add(this.label3);
            this.high_Choose_GroupBox.Controls.Add(this.high_Set_RadioButton);
            this.high_Choose_GroupBox.Controls.Add(this.high_N_RadioButton);
            this.high_Choose_GroupBox.Controls.Add(this.high_Y_RadioButton);
            this.high_Choose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.high_Choose_GroupBox.Location = new System.Drawing.Point(567, 13);
            this.high_Choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.high_Choose_GroupBox.Name = "high_Choose_GroupBox";
            this.high_Choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.high_Choose_GroupBox.Size = new System.Drawing.Size(536, 400);
            this.high_Choose_GroupBox.TabIndex = 11;
            this.high_Choose_GroupBox.TabStop = false;
            this.high_Choose_GroupBox.Text = "高壓裝置費用";
            // 
            // out_Set_TextBox
            // 
            this.out_Set_TextBox.Enabled = false;
            this.out_Set_TextBox.Location = new System.Drawing.Point(205, 319);
            this.out_Set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.out_Set_TextBox.Name = "out_Set_TextBox";
            this.out_Set_TextBox.Size = new System.Drawing.Size(211, 41);
            this.out_Set_TextBox.TabIndex = 13;
            this.out_Set_TextBox.Text = "1600";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label4.Location = new System.Drawing.Point(100, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 40);
            this.label4.TabIndex = 12;
            this.label4.Text = "外線補助費用";
            // 
            // high_Set_TextBox
            // 
            this.high_Set_TextBox.Enabled = false;
            this.high_Set_TextBox.Location = new System.Drawing.Point(205, 214);
            this.high_Set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.high_Set_TextBox.Name = "high_Set_TextBox";
            this.high_Set_TextBox.Size = new System.Drawing.Size(211, 41);
            this.high_Set_TextBox.TabIndex = 11;
            this.high_Set_TextBox.Text = "1200";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.label3.Location = new System.Drawing.Point(100, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 40);
            this.label3.TabIndex = 9;
            this.label3.Text = "高壓裝置費用";
            // 
            // high_Set_RadioButton
            // 
            this.high_Set_RadioButton.AutoSize = true;
            this.high_Set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.high_Set_RadioButton.Location = new System.Drawing.Point(106, 113);
            this.high_Set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.high_Set_RadioButton.Name = "high_Set_RadioButton";
            this.high_Set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.high_Set_RadioButton.TabIndex = 8;
            this.high_Set_RadioButton.Text = "自行設定";
            this.high_Set_RadioButton.UseVisualStyleBackColor = true;
            this.high_Set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // high_N_RadioButton
            // 
            this.high_N_RadioButton.AutoSize = true;
            this.high_N_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.high_N_RadioButton.Location = new System.Drawing.Point(306, 58);
            this.high_N_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.high_N_RadioButton.Name = "high_N_RadioButton";
            this.high_N_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.high_N_RadioButton.TabIndex = 7;
            this.high_N_RadioButton.Text = "不需要";
            this.high_N_RadioButton.UseVisualStyleBackColor = true;
            this.high_N_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // high_Y_RadioButton
            // 
            this.high_Y_RadioButton.AutoSize = true;
            this.high_Y_RadioButton.Checked = true;
            this.high_Y_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.high_Y_RadioButton.Location = new System.Drawing.Point(106, 58);
            this.high_Y_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.high_Y_RadioButton.Name = "high_Y_RadioButton";
            this.high_Y_RadioButton.Size = new System.Drawing.Size(85, 35);
            this.high_Y_RadioButton.TabIndex = 6;
            this.high_Y_RadioButton.TabStop = true;
            this.high_Y_RadioButton.Text = "需要";
            this.high_Y_RadioButton.UseVisualStyleBackColor = true;
            this.high_Y_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // ice_Choose_GroupBox
            // 
            this.ice_Choose_GroupBox.Controls.Add(this.ice_Set_TextBox);
            this.ice_Choose_GroupBox.Controls.Add(this.ice_Set_RadioButton);
            this.ice_Choose_GroupBox.Controls.Add(this.ice_20_RadioButton);
            this.ice_Choose_GroupBox.Controls.Add(this.ice_18_RadioButton);
            this.ice_Choose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ice_Choose_GroupBox.Location = new System.Drawing.Point(12, 250);
            this.ice_Choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ice_Choose_GroupBox.Name = "ice_Choose_GroupBox";
            this.ice_Choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ice_Choose_GroupBox.Size = new System.Drawing.Size(536, 163);
            this.ice_Choose_GroupBox.TabIndex = 12;
            this.ice_Choose_GroupBox.TabStop = false;
            this.ice_Choose_GroupBox.Text = "儲冰設備計劃單價";
            // 
            // ice_Set_TextBox
            // 
            this.ice_Set_TextBox.Enabled = false;
            this.ice_Set_TextBox.Location = new System.Drawing.Point(296, 96);
            this.ice_Set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ice_Set_TextBox.Name = "ice_Set_TextBox";
            this.ice_Set_TextBox.Size = new System.Drawing.Size(211, 41);
            this.ice_Set_TextBox.TabIndex = 10;
            this.ice_Set_TextBox.Text = "1800";
            // 
            // ice_Set_RadioButton
            // 
            this.ice_Set_RadioButton.AutoSize = true;
            this.ice_Set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ice_Set_RadioButton.Location = new System.Drawing.Point(268, 53);
            this.ice_Set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ice_Set_RadioButton.Name = "ice_Set_RadioButton";
            this.ice_Set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.ice_Set_RadioButton.TabIndex = 8;
            this.ice_Set_RadioButton.Text = "自行設定";
            this.ice_Set_RadioButton.UseVisualStyleBackColor = true;
            this.ice_Set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // ice_20_RadioButton
            // 
            this.ice_20_RadioButton.AutoSize = true;
            this.ice_20_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ice_20_RadioButton.Location = new System.Drawing.Point(68, 108);
            this.ice_20_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ice_20_RadioButton.Name = "ice_20_RadioButton";
            this.ice_20_RadioButton.Size = new System.Drawing.Size(91, 35);
            this.ice_20_RadioButton.TabIndex = 7;
            this.ice_20_RadioButton.Text = "2000";
            this.ice_20_RadioButton.UseVisualStyleBackColor = true;
            this.ice_20_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // ice_18_RadioButton
            // 
            this.ice_18_RadioButton.AutoSize = true;
            this.ice_18_RadioButton.Checked = true;
            this.ice_18_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ice_18_RadioButton.Location = new System.Drawing.Point(68, 53);
            this.ice_18_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ice_18_RadioButton.Name = "ice_18_RadioButton";
            this.ice_18_RadioButton.Size = new System.Drawing.Size(91, 35);
            this.ice_18_RadioButton.TabIndex = 6;
            this.ice_18_RadioButton.TabStop = true;
            this.ice_18_RadioButton.Text = "1800";
            this.ice_18_RadioButton.UseVisualStyleBackColor = true;
            this.ice_18_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // auto_Choose_GroupBox
            // 
            this.auto_Choose_GroupBox.Controls.Add(this.auto_Set_TextBox);
            this.auto_Choose_GroupBox.Controls.Add(this.auto_Set_RadioButton);
            this.auto_Choose_GroupBox.Controls.Add(this.auto_N_RadioButton);
            this.auto_Choose_GroupBox.Controls.Add(this.auto_Y_RadioButton);
            this.auto_Choose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.auto_Choose_GroupBox.Location = new System.Drawing.Point(12, 421);
            this.auto_Choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.auto_Choose_GroupBox.Name = "auto_Choose_GroupBox";
            this.auto_Choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.auto_Choose_GroupBox.Size = new System.Drawing.Size(536, 163);
            this.auto_Choose_GroupBox.TabIndex = 13;
            this.auto_Choose_GroupBox.TabStop = false;
            this.auto_Choose_GroupBox.Text = "自動控制設計單價";
            // 
            // auto_Set_TextBox
            // 
            this.auto_Set_TextBox.Enabled = false;
            this.auto_Set_TextBox.Location = new System.Drawing.Point(296, 96);
            this.auto_Set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.auto_Set_TextBox.Name = "auto_Set_TextBox";
            this.auto_Set_TextBox.Size = new System.Drawing.Size(211, 41);
            this.auto_Set_TextBox.TabIndex = 10;
            this.auto_Set_TextBox.Text = "400000";
            // 
            // auto_Set_RadioButton
            // 
            this.auto_Set_RadioButton.AutoSize = true;
            this.auto_Set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.auto_Set_RadioButton.Location = new System.Drawing.Point(268, 53);
            this.auto_Set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.auto_Set_RadioButton.Name = "auto_Set_RadioButton";
            this.auto_Set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.auto_Set_RadioButton.TabIndex = 8;
            this.auto_Set_RadioButton.Text = "自行設定";
            this.auto_Set_RadioButton.UseVisualStyleBackColor = true;
            this.auto_Set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // auto_N_RadioButton
            // 
            this.auto_N_RadioButton.AutoSize = true;
            this.auto_N_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.auto_N_RadioButton.Location = new System.Drawing.Point(68, 108);
            this.auto_N_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.auto_N_RadioButton.Name = "auto_N_RadioButton";
            this.auto_N_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.auto_N_RadioButton.TabIndex = 7;
            this.auto_N_RadioButton.Text = "不需要";
            this.auto_N_RadioButton.UseVisualStyleBackColor = true;
            this.auto_N_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // auto_Y_RadioButton
            // 
            this.auto_Y_RadioButton.AutoSize = true;
            this.auto_Y_RadioButton.Checked = true;
            this.auto_Y_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.auto_Y_RadioButton.Location = new System.Drawing.Point(68, 53);
            this.auto_Y_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.auto_Y_RadioButton.Name = "auto_Y_RadioButton";
            this.auto_Y_RadioButton.Size = new System.Drawing.Size(85, 35);
            this.auto_Y_RadioButton.TabIndex = 6;
            this.auto_Y_RadioButton.TabStop = true;
            this.auto_Y_RadioButton.Text = "需要";
            this.auto_Y_RadioButton.UseVisualStyleBackColor = true;
            this.auto_Y_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // design_Choose_GroupBox
            // 
            this.design_Choose_GroupBox.Controls.Add(this.design_Set_TextBox);
            this.design_Choose_GroupBox.Controls.Add(this.design_Set_RadioButton);
            this.design_Choose_GroupBox.Controls.Add(this.disign_N_RadioButton);
            this.design_Choose_GroupBox.Controls.Add(this.disign_Y_RadioButton);
            this.design_Choose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.design_Choose_GroupBox.Location = new System.Drawing.Point(567, 421);
            this.design_Choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.design_Choose_GroupBox.Name = "design_Choose_GroupBox";
            this.design_Choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.design_Choose_GroupBox.Size = new System.Drawing.Size(536, 163);
            this.design_Choose_GroupBox.TabIndex = 14;
            this.design_Choose_GroupBox.TabStop = false;
            this.design_Choose_GroupBox.Text = "設計費用";
            // 
            // design_Set_TextBox
            // 
            this.design_Set_TextBox.Enabled = false;
            this.design_Set_TextBox.Location = new System.Drawing.Point(296, 96);
            this.design_Set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.design_Set_TextBox.Name = "design_Set_TextBox";
            this.design_Set_TextBox.Size = new System.Drawing.Size(211, 41);
            this.design_Set_TextBox.TabIndex = 10;
            this.design_Set_TextBox.Text = "0";
            // 
            // design_Set_RadioButton
            // 
            this.design_Set_RadioButton.AutoSize = true;
            this.design_Set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.design_Set_RadioButton.Location = new System.Drawing.Point(268, 53);
            this.design_Set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.design_Set_RadioButton.Name = "design_Set_RadioButton";
            this.design_Set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.design_Set_RadioButton.TabIndex = 8;
            this.design_Set_RadioButton.Text = "自行設定";
            this.design_Set_RadioButton.UseVisualStyleBackColor = true;
            this.design_Set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // disign_N_RadioButton
            // 
            this.disign_N_RadioButton.AutoSize = true;
            this.disign_N_RadioButton.Checked = true;
            this.disign_N_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.disign_N_RadioButton.Location = new System.Drawing.Point(68, 108);
            this.disign_N_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.disign_N_RadioButton.Name = "disign_N_RadioButton";
            this.disign_N_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.disign_N_RadioButton.TabIndex = 7;
            this.disign_N_RadioButton.TabStop = true;
            this.disign_N_RadioButton.Text = "不需要";
            this.disign_N_RadioButton.UseVisualStyleBackColor = true;
            this.disign_N_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // disign_Y_RadioButton
            // 
            this.disign_Y_RadioButton.AutoSize = true;
            this.disign_Y_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.disign_Y_RadioButton.Location = new System.Drawing.Point(68, 53);
            this.disign_Y_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.disign_Y_RadioButton.Name = "disign_Y_RadioButton";
            this.disign_Y_RadioButton.Size = new System.Drawing.Size(85, 35);
            this.disign_Y_RadioButton.TabIndex = 6;
            this.disign_Y_RadioButton.Text = "需要";
            this.disign_Y_RadioButton.UseVisualStyleBackColor = true;
            this.disign_Y_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.ok_Button.Location = new System.Drawing.Point(851, 595);
            this.ok_Button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(252, 43);
            this.ok_Button.TabIndex = 15;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // Form_Plan_Unit_Price
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1115, 650);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.design_Choose_GroupBox);
            this.Controls.Add(this.auto_Choose_GroupBox);
            this.Controls.Add(this.ice_Choose_GroupBox);
            this.Controls.Add(this.high_Choose_GroupBox);
            this.Controls.Add(this.air_Choose_GroupBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Plan_Unit_Price";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "計劃單價";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Plan_Unit_Price_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Plan_Unit_Price_Closed);
            this.air_Choose_GroupBox.ResumeLayout(false);
            this.air_Choose_GroupBox.PerformLayout();
            this.high_Choose_GroupBox.ResumeLayout(false);
            this.high_Choose_GroupBox.PerformLayout();
            this.ice_Choose_GroupBox.ResumeLayout(false);
            this.ice_Choose_GroupBox.PerformLayout();
            this.auto_Choose_GroupBox.ResumeLayout(false);
            this.auto_Choose_GroupBox.PerformLayout();
            this.design_Choose_GroupBox.ResumeLayout(false);
            this.design_Choose_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox air_Choose_GroupBox;
        private System.Windows.Forms.RadioButton air_40_RadioButton;
        private System.Windows.Forms.RadioButton air_Set_RadioButton;
        private System.Windows.Forms.RadioButton air_35_RadioButton;
        private System.Windows.Forms.RadioButton air_30_RadioButton;
        private System.Windows.Forms.GroupBox high_Choose_GroupBox;
        private System.Windows.Forms.RadioButton high_Set_RadioButton;
        private System.Windows.Forms.RadioButton high_N_RadioButton;
        private System.Windows.Forms.RadioButton high_Y_RadioButton;
        private System.Windows.Forms.TextBox air_Set_TextBox;
        private System.Windows.Forms.TextBox out_Set_TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox high_Set_TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox ice_Choose_GroupBox;
        private System.Windows.Forms.TextBox ice_Set_TextBox;
        private System.Windows.Forms.RadioButton ice_Set_RadioButton;
        private System.Windows.Forms.RadioButton ice_20_RadioButton;
        private System.Windows.Forms.RadioButton ice_18_RadioButton;
        private System.Windows.Forms.GroupBox auto_Choose_GroupBox;
        private System.Windows.Forms.TextBox auto_Set_TextBox;
        private System.Windows.Forms.RadioButton auto_Set_RadioButton;
        private System.Windows.Forms.RadioButton auto_N_RadioButton;
        private System.Windows.Forms.RadioButton auto_Y_RadioButton;
        private System.Windows.Forms.GroupBox design_Choose_GroupBox;
        private System.Windows.Forms.TextBox design_Set_TextBox;
        private System.Windows.Forms.RadioButton design_Set_RadioButton;
        private System.Windows.Forms.RadioButton disign_N_RadioButton;
        private System.Windows.Forms.RadioButton disign_Y_RadioButton;
        private System.Windows.Forms.Button ok_Button;
    }
}