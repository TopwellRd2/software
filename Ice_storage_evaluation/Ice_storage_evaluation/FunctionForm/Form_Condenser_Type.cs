﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Condenser_Type : Form
    {
        private Form1 form1;
        public Dictionary<string, string> condenser_Type_Map { get; set; } = new Dictionary<string, string>();
        public Form_Condenser_Type(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Condenser_Type_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (set_RadioButton.Checked && string.IsNullOrWhiteSpace(set_TextBox.Text))
            {
                MessageBox.Show("設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                set_TextBox.Text = "";
                set_TextBox.Focus();
                e.Cancel = true;
            }
        }

        private void Form_Condenser_Type_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.condenserType_CheckBox.Checked)
            {
                form1.condenserType_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            condenser_Type_Map.Add("choose", set_TextBox.Text);
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            condenser_Type_Map["choose"] = set_TextBox.Text;
            /*
            if (air_RadioButton.Checked)
            {
                condenser_Type_Map["choose"] = "1.069";
            }
            else if (water_RadioButton.Checked)
            {
                condenser_Type_Map["choose"] = "1";
            }
            else if (steaming_RadioButton.Checked)
            {
                condenser_Type_Map["choose"] = "0.89";
            }
            else
            {
                condenser_Type_Map["choose"] = set_TextBox.Text;
            }
            */
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Name == "set_RadioButton")
            {
                set_TextBox.Enabled = true;
                set_TextBox.Text = "";
            }
            else
            {
                set_TextBox.Enabled = false;
                if (radioButton.Name == "air_RadioButton")
                {
                    set_TextBox.Text = "1.069";
                }
                else if (radioButton.Name == "water_RadioButton")
                {
                    set_TextBox.Text = "1";
                }
                else if (radioButton.Name == "steaming_RadioButton")
                {
                    set_TextBox.Text = "0.89";
                }
            }
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
