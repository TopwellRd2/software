﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form_Project_Design_Load : Form
    {
        private Form1 form1;
        SqlConnection sqlConnection;
        public Dictionary<string, string> project_Design_Map { get; set; } = new Dictionary<string, string>();

        public Form_Project_Design_Load(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
            projectName_TextBox.Focus();
            string connectionString = form1.GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
        }

        private void initialMap()
        {
            TextBox textbox;
            foreach (Control control in fullDayLoadDesignSamplingGroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    project_Design_Map.Add(textbox.Name.Replace("_TextBox",""),textbox.Text);
                }
            }

            foreach (Control control in monthlyLoadgroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    project_Design_Map.Add(textbox.Name.Replace("_TextBox", ""), textbox.Text);
                }
            }
            project_Design_Map.Add("projectName", "");
            project_Design_Map.Add("peak_Load","1350");

        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            TextBox textbox;
            foreach (Control control in fullDayLoadDesignSamplingGroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    project_Design_Map[textbox.Name.Replace("_TextBox", "")] = textbox.Text;
                }
            }

            foreach (Control control in monthlyLoadgroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    project_Design_Map[textbox.Name.Replace("_TextBox", "")] = textbox.Text;
                }
            }
            project_Design_Map["projectName"] = projectName_TextBox.Text;
            project_Design_Map["peak_Load"] = peak_Load_TextBox.Text;

        }

        private void Form_Project_Design_Load_FormClosing(object sender, FormClosingEventArgs e) //關閉前檢查
        {
           
            if (string.IsNullOrWhiteSpace(projectName_TextBox.Text))
            {
                MessageBox.Show("專案名稱不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                projectName_TextBox.Text = "";
                projectName_TextBox.Focus();
                e.Cancel = true;
            }
        }

        private void Form_Project_Design_Load_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.projectDesignLoad_CheckBox.Checked)
            {
                form1.projectDesignLoad_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void setDayHrParm()  //10Hr、11Hr、14Hr、15Hr 一樣的參數
        {
            TextBox textbox;
            foreach (Control control in fullDayLoadDesignSamplingGroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    textbox.Text = "0";
                }
            }
            double peakLoad = double.Parse(peak_Load_TextBox.Text);

            day_073_TextBox.Text = (0.475 * peakLoad).ToString();
            day_080_TextBox.Text = (0.475 * peakLoad).ToString();
            day_083_TextBox.Text = (0.45 * peakLoad).ToString();
            day_090_TextBox.Text = (0.45 * peakLoad).ToString();
            day_093_TextBox.Text = (0.425 * peakLoad).ToString();
            day_100_TextBox.Text = (0.425 * peakLoad).ToString();
            day_103_TextBox.Text = (0.4 * peakLoad).ToString();
            day_110_TextBox.Text = (0.4 * peakLoad).ToString();
            day_113_TextBox.Text = (0.425 * peakLoad).ToString();
            day_120_TextBox.Text = (0.45 * peakLoad).ToString();
            day_123_TextBox.Text = (0.475 * peakLoad).ToString();
            day_130_TextBox.Text = (0.475 * peakLoad).ToString();
            day_133_TextBox.Text = (0.5 * peakLoad).ToString();
            day_140_TextBox.Text = (0.5 * peakLoad).ToString();
            day_143_TextBox.Text = (0.5 * peakLoad).ToString();
            day_150_TextBox.Text = (0.5 * peakLoad).ToString();
            day_153_TextBox.Text = (0.5 * peakLoad).ToString();
            day_160_TextBox.Text = (0.475 * peakLoad).ToString();
            day_163_TextBox.Text = (0.45 * peakLoad).ToString();
            day_170_TextBox.Text = (0.425 * peakLoad).ToString();
        }

        private void hr9_Button_Click(object sender, EventArgs e)
        {
            TextBox textbox;
            foreach (Control control in fullDayLoadDesignSamplingGroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    textbox.Text = "0";
                }
            }
            double peakLoad = double.Parse(peak_Load_TextBox.Text);

            day_080_TextBox.Text = (0.475 * peakLoad).ToString();
            day_083_TextBox.Text = (0.475 * peakLoad).ToString();
            day_090_TextBox.Text = (0.5 * peakLoad).ToString();
            day_093_TextBox.Text = (0.5 * peakLoad).ToString();
            day_100_TextBox.Text = (0.475 * peakLoad).ToString();
            day_103_TextBox.Text = (0.45 * peakLoad).ToString();
            day_110_TextBox.Text = (0.425 * peakLoad).ToString();
            day_113_TextBox.Text = (0.4 * peakLoad).ToString();
            day_120_TextBox.Text = (0.425 * peakLoad).ToString();
            day_123_TextBox.Text = (0.425 * peakLoad).ToString();
            day_130_TextBox.Text = (0.45 * peakLoad).ToString();
            day_133_TextBox.Text = (0.475 * peakLoad).ToString();
            day_140_TextBox.Text = (0.5 * peakLoad).ToString();
            day_143_TextBox.Text = (0.5 * peakLoad).ToString();
            day_150_TextBox.Text = (0.5 * peakLoad).ToString();
            day_153_TextBox.Text = (0.5 * peakLoad).ToString();
            day_160_TextBox.Text = (0.475 * peakLoad).ToString();
            day_163_TextBox.Text = (0.45 * peakLoad).ToString();
        }

        private void hr10_Button_Click(object sender, EventArgs e)
        {
            setDayHrParm();
        }

        private void hr11_Button_Click(object sender, EventArgs e)
        {
            setDayHrParm();
            double peakLoad = double.Parse(peak_Load_TextBox.Text);
            day_173_TextBox.Text = (0.4 * peakLoad).ToString();
            day_180_TextBox.Text = (0.4 * peakLoad).ToString();
        }

        private void hr14_Button_Click(object sender, EventArgs e)
        {
            setDayHrParm();
            double peakLoad = double.Parse(peak_Load_TextBox.Text);
            day_173_TextBox.Text = (0.4 * peakLoad).ToString();
            day_180_TextBox.Text = (0.4 * peakLoad).ToString();
            day_183_TextBox.Text = (0.375 * peakLoad).ToString();
            day_190_TextBox.Text = (0.35 * peakLoad).ToString();
            day_193_TextBox.Text = (0.325 * peakLoad).ToString();
            day_200_TextBox.Text = (0.3 * peakLoad).ToString();
            day_203_TextBox.Text = (0.275 * peakLoad).ToString();
            day_210_TextBox.Text = (0.25 * peakLoad).ToString();
        }

        private void hr15_Button_Click(object sender, EventArgs e)
        {
            setDayHrParm();
            double peakLoad = double.Parse(peak_Load_TextBox.Text);
            day_173_TextBox.Text = (0.4 * peakLoad).ToString();
            day_180_TextBox.Text = (0.4 * peakLoad).ToString();
            day_183_TextBox.Text = (0.375 * peakLoad).ToString();
            day_190_TextBox.Text = (0.35 * peakLoad).ToString();
            day_193_TextBox.Text = (0.325 * peakLoad).ToString();
            day_200_TextBox.Text = (0.3 * peakLoad).ToString();
            day_203_TextBox.Text = (0.275 * peakLoad).ToString();
            day_210_TextBox.Text = (0.25 * peakLoad).ToString();
            day_213_TextBox.Text = (0.25 * peakLoad).ToString();
            day_220_TextBox.Text = (0.25 * peakLoad).ToString();
        }

        private void office_Button_Click(object sender, EventArgs e)
        {
            month1_Scale_TextBox.Text = "10";
            month2_Scale_TextBox.Text = "10";
            month3_Scale_TextBox.Text = "20";
            month4_Scale_TextBox.Text = "30";
            month5_Scale_TextBox.Text = "60";
            month6_Scale_TextBox.Text = "90";
            month7_Scale_TextBox.Text = "100";
            month8_Scale_TextBox.Text = "100";
            month9_Scale_TextBox.Text = "100";
            month10_Scale_TextBox.Text = "70";
            month11_Scale_TextBox.Text = "30";
            month12_Scale_TextBox.Text = "20";
        }

        private void factory_Button_Click(object sender, EventArgs e)
        {
            month1_Scale_TextBox.Text = "0";
            month2_Scale_TextBox.Text = "0";
            month3_Scale_TextBox.Text = "20";
            month4_Scale_TextBox.Text = "50";
            month5_Scale_TextBox.Text = "75";
            month6_Scale_TextBox.Text = "90";
            month7_Scale_TextBox.Text = "100";
            month8_Scale_TextBox.Text = "100";
            month9_Scale_TextBox.Text = "100";
            month10_Scale_TextBox.Text = "70";
            month11_Scale_TextBox.Text = "30";
            month12_Scale_TextBox.Text = "20";
        }

        private void day20_Button_Click(object sender, EventArgs e)
        {
            month1_Workday_TextBox.Text = "26";
            month2_Workday_TextBox.Text = "16";
            month3_Workday_TextBox.Text = "26";
            month4_Workday_TextBox.Text = "25";
            month5_Workday_TextBox.Text = "26";
            month6_Workday_TextBox.Text = "25";
            month7_Workday_TextBox.Text = "26";
            month8_Workday_TextBox.Text = "26";
            month9_Workday_TextBox.Text = "25";
            month10_Workday_TextBox.Text = "26"; 
            month11_Workday_TextBox.Text = "25";
            month12_Workday_TextBox.Text = "26";
        }

        private void day31_Button_Click(object sender, EventArgs e)
        {
            month1_Workday_TextBox.Text = "31";
            month2_Workday_TextBox.Text = "20";
            month3_Workday_TextBox.Text = "31";
            month4_Workday_TextBox.Text = "30";
            month5_Workday_TextBox.Text = "31";
            month6_Workday_TextBox.Text = "30";
            month7_Workday_TextBox.Text = "31";
            month8_Workday_TextBox.Text = "31";
            month9_Workday_TextBox.Text = "30";
            month10_Workday_TextBox.Text = "31";
            month11_Workday_TextBox.Text = "30";
            month12_Workday_TextBox.Text = "31";
        }

        private void dayToDef_Button_Click(object sender, EventArgs e)
        {
            TextBox textbox;
            foreach (Control control in fullDayLoadDesignSamplingGroupBox.Controls)
            {
                if (control.Name.Contains("TextBox"))
                {
                    textbox = (TextBox)control;
                    textbox.Text = "0";
                }
            }
        }

        private void scaleToDef_Button_Click(object sender, EventArgs e)
        {
            TextBox textbox;
            foreach (Control control in monthlyLoadgroupBox.Controls)
            {
                if (control.Name.Contains("Scale"))
                {
                    textbox = (TextBox)control;
                    textbox.Text = "0";
                }
            }
        }

        private void workdayToDef_Button_Click(object sender, EventArgs e)
        {
            TextBox textbox;
            foreach (Control control in monthlyLoadgroupBox.Controls)
            {
                if (control.Name.Contains("Workday"))
                {
                    textbox = (TextBox)control;
                    textbox.Text = "0";
                }
            }
        }

        private void project_Design_Ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void peak_Load_TextBox_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(peak_Load_TextBox.Text))
            {
                MessageBox.Show("尖峰負荷不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                peak_Load_TextBox.Text = "";
                peak_Load_TextBox.Focus();
            }
        }

        private void projectName_TextBox_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(projectName_TextBox.Text))
            {
                MessageBox.Show("專案名稱不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                projectName_TextBox.Text = "";
                projectName_TextBox.Focus();
                return;
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //驗證專案名稱是否重複
            String checkProjectSql = "SELECT COUNT(1)" +
                                     "  FROM [ICE_STORAGE].[dbo].[FILE] " +
                                     " WHERE REPLACE(project_name,' ','') = '" + projectName_TextBox.Text.Replace(" ","") + "'";

            SqlCommand command = new SqlCommand(checkProjectSql, sqlConnection);
            int projectCount = 0;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        projectCount = reader.GetInt32(0);
                    }
                }
            }

            if (projectCount > 0)
            {
                MessageBox.Show("專案名稱不得重複,請重新輸入!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                projectName_TextBox.Text = "";
                projectName_TextBox.Focus();
                return;
            }
            sqlConnection.Close();
        }
    }
}
