﻿namespace Ice_storage_evaluation
{
    partial class Form_Air_Conditioning_Load_Equipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadChoose_GroupBox = new System.Windows.Forms.GroupBox();
            this.set_TextBox = new System.Windows.Forms.TextBox();
            this.mix_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_RadioButton = new System.Windows.Forms.RadioButton();
            this.air_RadioButton = new System.Windows.Forms.RadioButton();
            this.blower_RadioButton = new System.Windows.Forms.RadioButton();
            this.tonsChoose_GroupBox = new System.Windows.Forms.GroupBox();
            this.than12_RadioButton = new System.Windows.Forms.RadioButton();
            this.to8_RadioButton = new System.Windows.Forms.RadioButton();
            this.to12_RadioButton = new System.Windows.Forms.RadioButton();
            this.to5_RadioButton = new System.Windows.Forms.RadioButton();
            this.to3_RadioButton = new System.Windows.Forms.RadioButton();
            this.ok_Button = new System.Windows.Forms.Button();
            this.loadChoose_GroupBox.SuspendLayout();
            this.tonsChoose_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadChoose_GroupBox
            // 
            this.loadChoose_GroupBox.Controls.Add(this.set_TextBox);
            this.loadChoose_GroupBox.Controls.Add(this.mix_RadioButton);
            this.loadChoose_GroupBox.Controls.Add(this.set_RadioButton);
            this.loadChoose_GroupBox.Controls.Add(this.air_RadioButton);
            this.loadChoose_GroupBox.Controls.Add(this.blower_RadioButton);
            this.loadChoose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.loadChoose_GroupBox.Location = new System.Drawing.Point(33, 25);
            this.loadChoose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.loadChoose_GroupBox.Name = "loadChoose_GroupBox";
            this.loadChoose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.loadChoose_GroupBox.Size = new System.Drawing.Size(353, 384);
            this.loadChoose_GroupBox.TabIndex = 10;
            this.loadChoose_GroupBox.TabStop = false;
            this.loadChoose_GroupBox.Text = "負載型式";
            // 
            // set_TextBox
            // 
            this.set_TextBox.Enabled = false;
            this.set_TextBox.Location = new System.Drawing.Point(101, 260);
            this.set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_TextBox.Name = "set_TextBox";
            this.set_TextBox.Size = new System.Drawing.Size(182, 41);
            this.set_TextBox.TabIndex = 10;
            this.set_TextBox.Text = "0.25";
            // 
            // mix_RadioButton
            // 
            this.mix_RadioButton.AutoSize = true;
            this.mix_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mix_RadioButton.Location = new System.Drawing.Point(74, 152);
            this.mix_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.mix_RadioButton.Name = "mix_RadioButton";
            this.mix_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.mix_RadioButton.TabIndex = 9;
            this.mix_RadioButton.Text = "混合配置";
            this.mix_RadioButton.UseVisualStyleBackColor = true;
            this.mix_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // set_RadioButton
            // 
            this.set_RadioButton.AutoSize = true;
            this.set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_RadioButton.Location = new System.Drawing.Point(74, 207);
            this.set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_RadioButton.Name = "set_RadioButton";
            this.set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.set_RadioButton.TabIndex = 8;
            this.set_RadioButton.Text = "自行設定";
            this.set_RadioButton.UseVisualStyleBackColor = true;
            this.set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // air_RadioButton
            // 
            this.air_RadioButton.AutoSize = true;
            this.air_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.air_RadioButton.Location = new System.Drawing.Point(74, 97);
            this.air_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.air_RadioButton.Name = "air_RadioButton";
            this.air_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.air_RadioButton.TabIndex = 7;
            this.air_RadioButton.Text = "空調箱";
            this.air_RadioButton.UseVisualStyleBackColor = true;
            this.air_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // blower_RadioButton
            // 
            this.blower_RadioButton.AutoSize = true;
            this.blower_RadioButton.Checked = true;
            this.blower_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.blower_RadioButton.Location = new System.Drawing.Point(74, 42);
            this.blower_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.blower_RadioButton.Name = "blower_RadioButton";
            this.blower_RadioButton.Size = new System.Drawing.Size(160, 35);
            this.blower_RadioButton.TabIndex = 6;
            this.blower_RadioButton.TabStop = true;
            this.blower_RadioButton.Text = "小型送風機";
            this.blower_RadioButton.UseVisualStyleBackColor = true;
            this.blower_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // tonsChoose_GroupBox
            // 
            this.tonsChoose_GroupBox.Controls.Add(this.than12_RadioButton);
            this.tonsChoose_GroupBox.Controls.Add(this.to8_RadioButton);
            this.tonsChoose_GroupBox.Controls.Add(this.to12_RadioButton);
            this.tonsChoose_GroupBox.Controls.Add(this.to5_RadioButton);
            this.tonsChoose_GroupBox.Controls.Add(this.to3_RadioButton);
            this.tonsChoose_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tonsChoose_GroupBox.Location = new System.Drawing.Point(420, 25);
            this.tonsChoose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tonsChoose_GroupBox.Name = "tonsChoose_GroupBox";
            this.tonsChoose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tonsChoose_GroupBox.Size = new System.Drawing.Size(353, 315);
            this.tonsChoose_GroupBox.TabIndex = 11;
            this.tonsChoose_GroupBox.TabStop = false;
            this.tonsChoose_GroupBox.Text = "噸數選擇";
            // 
            // than12_RadioButton
            // 
            this.than12_RadioButton.AutoSize = true;
            this.than12_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.than12_RadioButton.Location = new System.Drawing.Point(88, 262);
            this.than12_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.than12_RadioButton.Name = "than12_RadioButton";
            this.than12_RadioButton.Size = new System.Drawing.Size(151, 35);
            this.than12_RadioButton.TabIndex = 10;
            this.than12_RadioButton.Text = "> 1200 RT";
            this.than12_RadioButton.UseVisualStyleBackColor = true;
            this.than12_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to8_RadioButton
            // 
            this.to8_RadioButton.AutoSize = true;
            this.to8_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to8_RadioButton.Location = new System.Drawing.Point(88, 152);
            this.to8_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to8_RadioButton.Name = "to8_RadioButton";
            this.to8_RadioButton.Size = new System.Drawing.Size(185, 35);
            this.to8_RadioButton.TabIndex = 9;
            this.to8_RadioButton.Text = "500 ~ 800 RT";
            this.to8_RadioButton.UseVisualStyleBackColor = true;
            this.to8_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to12_RadioButton
            // 
            this.to12_RadioButton.AutoSize = true;
            this.to12_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to12_RadioButton.Location = new System.Drawing.Point(88, 207);
            this.to12_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to12_RadioButton.Name = "to12_RadioButton";
            this.to12_RadioButton.Size = new System.Drawing.Size(199, 35);
            this.to12_RadioButton.TabIndex = 8;
            this.to12_RadioButton.Text = "800 ~ 1200 RT";
            this.to12_RadioButton.UseVisualStyleBackColor = true;
            this.to12_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to5_RadioButton
            // 
            this.to5_RadioButton.AutoSize = true;
            this.to5_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to5_RadioButton.Location = new System.Drawing.Point(88, 97);
            this.to5_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to5_RadioButton.Name = "to5_RadioButton";
            this.to5_RadioButton.Size = new System.Drawing.Size(185, 35);
            this.to5_RadioButton.TabIndex = 7;
            this.to5_RadioButton.Text = "300 ~ 500 RT";
            this.to5_RadioButton.UseVisualStyleBackColor = true;
            this.to5_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // to3_RadioButton
            // 
            this.to3_RadioButton.AutoSize = true;
            this.to3_RadioButton.Checked = true;
            this.to3_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.to3_RadioButton.Location = new System.Drawing.Point(88, 42);
            this.to3_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.to3_RadioButton.Name = "to3_RadioButton";
            this.to3_RadioButton.Size = new System.Drawing.Size(137, 35);
            this.to3_RadioButton.TabIndex = 6;
            this.to3_RadioButton.TabStop = true;
            this.to3_RadioButton.Text = "< 300 RT";
            this.to3_RadioButton.UseVisualStyleBackColor = true;
            this.to3_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.ok_Button.Location = new System.Drawing.Point(420, 348);
            this.ok_Button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(353, 61);
            this.ok_Button.TabIndex = 12;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // Form_Air_Conditioning_Load_Equipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(807, 422);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.tonsChoose_GroupBox);
            this.Controls.Add(this.loadChoose_GroupBox);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Air_Conditioning_Load_Equipment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "空調負載設備";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Air_Conditioning_Load_Equipment_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Air_Conditioning_Load_Equipment_Closed);
            this.loadChoose_GroupBox.ResumeLayout(false);
            this.loadChoose_GroupBox.PerformLayout();
            this.tonsChoose_GroupBox.ResumeLayout(false);
            this.tonsChoose_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox loadChoose_GroupBox;
        private System.Windows.Forms.RadioButton mix_RadioButton;
        private System.Windows.Forms.RadioButton set_RadioButton;
        private System.Windows.Forms.RadioButton air_RadioButton;
        private System.Windows.Forms.RadioButton blower_RadioButton;
        private System.Windows.Forms.TextBox set_TextBox;
        private System.Windows.Forms.GroupBox tonsChoose_GroupBox;
        private System.Windows.Forms.RadioButton to8_RadioButton;
        private System.Windows.Forms.RadioButton to12_RadioButton;
        private System.Windows.Forms.RadioButton to5_RadioButton;
        private System.Windows.Forms.RadioButton to3_RadioButton;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.RadioButton than12_RadioButton;
    }
}