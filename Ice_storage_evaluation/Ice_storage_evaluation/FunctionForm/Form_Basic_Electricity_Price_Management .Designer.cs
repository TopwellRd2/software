﻿namespace Ice_storage_evaluation
{
    partial class Form_Basic_Electricity_Price_Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.e_Price_Name_TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.currency_System_TextBox = new System.Windows.Forms.TextBox();
            this.exchange_Rate_TW_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.two_Price_GroupBox = new System.Windows.Forms.GroupBox();
            this.two_Non_Summer_Basic_Price_TextBox = new System.Windows.Forms.TextBox();
            this.two_Summer_Basic_Price_TextBox = new System.Windows.Forms.TextBox();
            this.two_Non_Summer_Normal_TextBox = new System.Windows.Forms.TextBox();
            this.two_Summer_Normal_TextBox = new System.Windows.Forms.TextBox();
            this.two_Non_Summer_Peak_TextBox = new System.Windows.Forms.TextBox();
            this.two_Summer_Peak_TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.three_Price_GroupBox = new System.Windows.Forms.GroupBox();
            this.three_Non_Summer_Basic_Price_TextBox = new System.Windows.Forms.TextBox();
            this.three_Summer_Basic_Price_TextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.three_Non_Summer_Trough_TextBox = new System.Windows.Forms.TextBox();
            this.three_Summer_Trough_TextBox = new System.Windows.Forms.TextBox();
            this.three_Non_Summer_Normal_TextBox = new System.Windows.Forms.TextBox();
            this.three_Summer_Normal_TextBox = new System.Windows.Forms.TextBox();
            this.three_Non_Summer_Peak_TextBox = new System.Windows.Forms.TextBox();
            this.three_Summer_Peak_TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.e_Price_Name_comboBox = new System.Windows.Forms.ComboBox();
            this.eLECTRICITYPRICEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iCE_STORAGE_DataSet = new Ice_storage_evaluation.ICE_STORAGE_DataSet();
            this.e_Price_Choose_Button = new System.Windows.Forms.Button();
            this.e_Price_New_Button = new System.Windows.Forms.Button();
            this.e_Price_Edit_Button = new System.Windows.Forms.Button();
            this.e_Price_Delete_Button = new System.Windows.Forms.Button();
            this.e_Price_Ok_Button = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.M1_CheckBox = new System.Windows.Forms.CheckBox();
            this.label88 = new System.Windows.Forms.Label();
            this.M2_CheckBox = new System.Windows.Forms.CheckBox();
            this.label87 = new System.Windows.Forms.Label();
            this.M3_CheckBox = new System.Windows.Forms.CheckBox();
            this.label89 = new System.Windows.Forms.Label();
            this.M4_CheckBox = new System.Windows.Forms.CheckBox();
            this.label90 = new System.Windows.Forms.Label();
            this.M5_CheckBox = new System.Windows.Forms.CheckBox();
            this.label91 = new System.Windows.Forms.Label();
            this.M6_CheckBox = new System.Windows.Forms.CheckBox();
            this.label92 = new System.Windows.Forms.Label();
            this.M7_CheckBox = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.M8_CheckBox = new System.Windows.Forms.CheckBox();
            this.label94 = new System.Windows.Forms.Label();
            this.M9_CheckBox = new System.Windows.Forms.CheckBox();
            this.label95 = new System.Windows.Forms.Label();
            this.M10_CheckBox = new System.Windows.Forms.CheckBox();
            this.label96 = new System.Windows.Forms.Label();
            this.M11_CheckBox = new System.Windows.Forms.CheckBox();
            this.label97 = new System.Windows.Forms.Label();
            this.M12_CheckBox = new System.Windows.Forms.CheckBox();
            this.label98 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_1 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_1 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_1 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_2 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_2 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_2 = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_3 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_3 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_3 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_4 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_4 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_4 = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_5 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_5 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_5 = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_10 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_10 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_10 = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_9 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_9 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_9 = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_8 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_8 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_8 = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_7 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_7 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_7 = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_6 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_6 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_6 = new System.Windows.Forms.RadioButton();
            this.panel12 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_12 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_12 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_12 = new System.Windows.Forms.RadioButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_11 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_11 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_11 = new System.Windows.Forms.RadioButton();
            this.panel24 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_24 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_24 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_24 = new System.Windows.Forms.RadioButton();
            this.panel23 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_23 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_23 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_23 = new System.Windows.Forms.RadioButton();
            this.panel22 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_22 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_22 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_22 = new System.Windows.Forms.RadioButton();
            this.panel21 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_21 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_21 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_21 = new System.Windows.Forms.RadioButton();
            this.panel20 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_20 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_20 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_20 = new System.Windows.Forms.RadioButton();
            this.panel19 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_19 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_19 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_19 = new System.Windows.Forms.RadioButton();
            this.panel18 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_18 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_18 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_18 = new System.Windows.Forms.RadioButton();
            this.panel17 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_17 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_17 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_17 = new System.Windows.Forms.RadioButton();
            this.panel16 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_16 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_16 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_16 = new System.Windows.Forms.RadioButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_15 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_15 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_15 = new System.Windows.Forms.RadioButton();
            this.panel14 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_14 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_14 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_14 = new System.Windows.Forms.RadioButton();
            this.panel13 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_13 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_13 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_13 = new System.Windows.Forms.RadioButton();
            this.panel36 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_36 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_36 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_36 = new System.Windows.Forms.RadioButton();
            this.panel35 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_35 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_35 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_35 = new System.Windows.Forms.RadioButton();
            this.panel34 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_34 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_34 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_34 = new System.Windows.Forms.RadioButton();
            this.panel33 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_33 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_33 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_33 = new System.Windows.Forms.RadioButton();
            this.panel32 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_32 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_32 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_32 = new System.Windows.Forms.RadioButton();
            this.panel31 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_31 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_31 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_31 = new System.Windows.Forms.RadioButton();
            this.panel30 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_30 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_30 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_30 = new System.Windows.Forms.RadioButton();
            this.panel29 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_29 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_29 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_29 = new System.Windows.Forms.RadioButton();
            this.panel28 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_28 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_28 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_28 = new System.Windows.Forms.RadioButton();
            this.panel27 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_27 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_27 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_27 = new System.Windows.Forms.RadioButton();
            this.panel26 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_26 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_26 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_26 = new System.Windows.Forms.RadioButton();
            this.panel25 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_25 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_25 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_25 = new System.Windows.Forms.RadioButton();
            this.panel48 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_48 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_48 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_48 = new System.Windows.Forms.RadioButton();
            this.panel47 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_47 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_47 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_47 = new System.Windows.Forms.RadioButton();
            this.panel46 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_46 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_46 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_46 = new System.Windows.Forms.RadioButton();
            this.panel45 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_45 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_45 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_45 = new System.Windows.Forms.RadioButton();
            this.panel44 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_44 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_44 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_44 = new System.Windows.Forms.RadioButton();
            this.panel43 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_43 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_43 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_43 = new System.Windows.Forms.RadioButton();
            this.panel42 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_42 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_42 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_42 = new System.Windows.Forms.RadioButton();
            this.panel41 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_41 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_41 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_41 = new System.Windows.Forms.RadioButton();
            this.panel40 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_40 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_40 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_40 = new System.Windows.Forms.RadioButton();
            this.panel39 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_39 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_39 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_39 = new System.Windows.Forms.RadioButton();
            this.panel38 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_38 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_38 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_38 = new System.Windows.Forms.RadioButton();
            this.panel37 = new System.Windows.Forms.Panel();
            this.trough_RadioButton_37 = new System.Windows.Forms.RadioButton();
            this.flat_RadioButton_37 = new System.Windows.Forms.RadioButton();
            this.crest_RadioButton_37 = new System.Windows.Forms.RadioButton();
            this.eLECTRICITY_PRICETableAdapter = new Ice_storage_evaluation.ICE_STORAGE_DataSetTableAdapters.ELECTRICITY_PRICETableAdapter();
            this.two_Price_GroupBox.SuspendLayout();
            this.three_Price_GroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eLECTRICITYPRICEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iCE_STORAGE_DataSet)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel37.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(27, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "電價表名稱";
            // 
            // e_Price_Name_TextBox
            // 
            this.e_Price_Name_TextBox.Font = new System.Drawing.Font("新細明體", 12F);
            this.e_Price_Name_TextBox.Location = new System.Drawing.Point(177, 18);
            this.e_Price_Name_TextBox.Name = "e_Price_Name_TextBox";
            this.e_Price_Name_TextBox.Size = new System.Drawing.Size(726, 31);
            this.e_Price_Name_TextBox.TabIndex = 1;
            this.e_Price_Name_TextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.e_Price_Name_TextBox_KeyUp);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(924, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 31);
            this.label2.TabIndex = 2;
            this.label2.Text = "幣制";
            // 
            // currency_System_TextBox
            // 
            this.currency_System_TextBox.Font = new System.Drawing.Font("新細明體", 12F);
            this.currency_System_TextBox.Location = new System.Drawing.Point(1003, 19);
            this.currency_System_TextBox.Name = "currency_System_TextBox";
            this.currency_System_TextBox.Size = new System.Drawing.Size(179, 31);
            this.currency_System_TextBox.TabIndex = 3;
            this.currency_System_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // exchange_Rate_TW_TextBox
            // 
            this.exchange_Rate_TW_TextBox.Font = new System.Drawing.Font("新細明體", 12F);
            this.exchange_Rate_TW_TextBox.Location = new System.Drawing.Point(1362, 19);
            this.exchange_Rate_TW_TextBox.Name = "exchange_Rate_TW_TextBox";
            this.exchange_Rate_TW_TextBox.Size = new System.Drawing.Size(221, 31);
            this.exchange_Rate_TW_TextBox.TabIndex = 5;
            this.exchange_Rate_TW_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(1209, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "對台幣匯率";
            // 
            // two_Price_GroupBox
            // 
            this.two_Price_GroupBox.Controls.Add(this.two_Non_Summer_Basic_Price_TextBox);
            this.two_Price_GroupBox.Controls.Add(this.two_Summer_Basic_Price_TextBox);
            this.two_Price_GroupBox.Controls.Add(this.two_Non_Summer_Normal_TextBox);
            this.two_Price_GroupBox.Controls.Add(this.two_Summer_Normal_TextBox);
            this.two_Price_GroupBox.Controls.Add(this.two_Non_Summer_Peak_TextBox);
            this.two_Price_GroupBox.Controls.Add(this.two_Summer_Peak_TextBox);
            this.two_Price_GroupBox.Controls.Add(this.label8);
            this.two_Price_GroupBox.Controls.Add(this.label7);
            this.two_Price_GroupBox.Controls.Add(this.label6);
            this.two_Price_GroupBox.Controls.Add(this.label5);
            this.two_Price_GroupBox.Controls.Add(this.label4);
            this.two_Price_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Price_GroupBox.Location = new System.Drawing.Point(32, 71);
            this.two_Price_GroupBox.Name = "two_Price_GroupBox";
            this.two_Price_GroupBox.Size = new System.Drawing.Size(672, 143);
            this.two_Price_GroupBox.TabIndex = 6;
            this.two_Price_GroupBox.TabStop = false;
            this.two_Price_GroupBox.Text = "二段式電價";
            // 
            // two_Non_Summer_Basic_Price_TextBox
            // 
            this.two_Non_Summer_Basic_Price_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Non_Summer_Basic_Price_TextBox.Location = new System.Drawing.Point(507, 95);
            this.two_Non_Summer_Basic_Price_TextBox.Name = "two_Non_Summer_Basic_Price_TextBox";
            this.two_Non_Summer_Basic_Price_TextBox.Size = new System.Drawing.Size(145, 30);
            this.two_Non_Summer_Basic_Price_TextBox.TabIndex = 18;
            this.two_Non_Summer_Basic_Price_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_Summer_Basic_Price_TextBox
            // 
            this.two_Summer_Basic_Price_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Summer_Basic_Price_TextBox.Location = new System.Drawing.Point(507, 57);
            this.two_Summer_Basic_Price_TextBox.Name = "two_Summer_Basic_Price_TextBox";
            this.two_Summer_Basic_Price_TextBox.Size = new System.Drawing.Size(145, 30);
            this.two_Summer_Basic_Price_TextBox.TabIndex = 15;
            this.two_Summer_Basic_Price_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_Non_Summer_Normal_TextBox
            // 
            this.two_Non_Summer_Normal_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Non_Summer_Normal_TextBox.Location = new System.Drawing.Point(327, 95);
            this.two_Non_Summer_Normal_TextBox.Name = "two_Non_Summer_Normal_TextBox";
            this.two_Non_Summer_Normal_TextBox.Size = new System.Drawing.Size(145, 30);
            this.two_Non_Summer_Normal_TextBox.TabIndex = 17;
            this.two_Non_Summer_Normal_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_Summer_Normal_TextBox
            // 
            this.two_Summer_Normal_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Summer_Normal_TextBox.Location = new System.Drawing.Point(327, 57);
            this.two_Summer_Normal_TextBox.Name = "two_Summer_Normal_TextBox";
            this.two_Summer_Normal_TextBox.Size = new System.Drawing.Size(145, 30);
            this.two_Summer_Normal_TextBox.TabIndex = 14;
            this.two_Summer_Normal_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_Non_Summer_Peak_TextBox
            // 
            this.two_Non_Summer_Peak_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Non_Summer_Peak_TextBox.Location = new System.Drawing.Point(145, 95);
            this.two_Non_Summer_Peak_TextBox.Name = "two_Non_Summer_Peak_TextBox";
            this.two_Non_Summer_Peak_TextBox.Size = new System.Drawing.Size(145, 30);
            this.two_Non_Summer_Peak_TextBox.TabIndex = 16;
            this.two_Non_Summer_Peak_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // two_Summer_Peak_TextBox
            // 
            this.two_Summer_Peak_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.two_Summer_Peak_TextBox.Location = new System.Drawing.Point(145, 57);
            this.two_Summer_Peak_TextBox.Name = "two_Summer_Peak_TextBox";
            this.two_Summer_Peak_TextBox.Size = new System.Drawing.Size(145, 30);
            this.two_Summer_Peak_TextBox.TabIndex = 13;
            this.two_Summer_Peak_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(534, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 25);
            this.label8.TabIndex = 12;
            this.label8.Text = "基本電費";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(354, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 25);
            this.label7.TabIndex = 11;
            this.label7.Text = "一般時段";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(174, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "尖峰時段";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(35, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "非夏月";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(55, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "夏月";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // three_Price_GroupBox
            // 
            this.three_Price_GroupBox.Controls.Add(this.three_Non_Summer_Basic_Price_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.three_Summer_Basic_Price_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.label14);
            this.three_Price_GroupBox.Controls.Add(this.three_Non_Summer_Trough_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.three_Summer_Trough_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.three_Non_Summer_Normal_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.three_Summer_Normal_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.three_Non_Summer_Peak_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.three_Summer_Peak_TextBox);
            this.three_Price_GroupBox.Controls.Add(this.label9);
            this.three_Price_GroupBox.Controls.Add(this.label10);
            this.three_Price_GroupBox.Controls.Add(this.label11);
            this.three_Price_GroupBox.Controls.Add(this.label12);
            this.three_Price_GroupBox.Controls.Add(this.label13);
            this.three_Price_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Price_GroupBox.Location = new System.Drawing.Point(731, 71);
            this.three_Price_GroupBox.Name = "three_Price_GroupBox";
            this.three_Price_GroupBox.Size = new System.Drawing.Size(852, 143);
            this.three_Price_GroupBox.TabIndex = 7;
            this.three_Price_GroupBox.TabStop = false;
            this.three_Price_GroupBox.Text = "三段式電價";
            // 
            // three_Non_Summer_Basic_Price_TextBox
            // 
            this.three_Non_Summer_Basic_Price_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Non_Summer_Basic_Price_TextBox.Location = new System.Drawing.Point(700, 95);
            this.three_Non_Summer_Basic_Price_TextBox.Name = "three_Non_Summer_Basic_Price_TextBox";
            this.three_Non_Summer_Basic_Price_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Non_Summer_Basic_Price_TextBox.TabIndex = 32;
            this.three_Non_Summer_Basic_Price_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_Summer_Basic_Price_TextBox
            // 
            this.three_Summer_Basic_Price_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Summer_Basic_Price_TextBox.Location = new System.Drawing.Point(700, 57);
            this.three_Summer_Basic_Price_TextBox.Name = "three_Summer_Basic_Price_TextBox";
            this.three_Summer_Basic_Price_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Summer_Basic_Price_TextBox.TabIndex = 31;
            this.three_Summer_Basic_Price_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(727, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 25);
            this.label14.TabIndex = 30;
            this.label14.Text = "基本電費";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // three_Non_Summer_Trough_TextBox
            // 
            this.three_Non_Summer_Trough_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Non_Summer_Trough_TextBox.Location = new System.Drawing.Point(520, 95);
            this.three_Non_Summer_Trough_TextBox.Name = "three_Non_Summer_Trough_TextBox";
            this.three_Non_Summer_Trough_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Non_Summer_Trough_TextBox.TabIndex = 29;
            this.three_Non_Summer_Trough_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_Summer_Trough_TextBox
            // 
            this.three_Summer_Trough_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Summer_Trough_TextBox.Location = new System.Drawing.Point(520, 57);
            this.three_Summer_Trough_TextBox.Name = "three_Summer_Trough_TextBox";
            this.three_Summer_Trough_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Summer_Trough_TextBox.TabIndex = 26;
            this.three_Summer_Trough_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_Non_Summer_Normal_TextBox
            // 
            this.three_Non_Summer_Normal_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Non_Summer_Normal_TextBox.Location = new System.Drawing.Point(340, 95);
            this.three_Non_Summer_Normal_TextBox.Name = "three_Non_Summer_Normal_TextBox";
            this.three_Non_Summer_Normal_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Non_Summer_Normal_TextBox.TabIndex = 28;
            this.three_Non_Summer_Normal_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_Summer_Normal_TextBox
            // 
            this.three_Summer_Normal_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Summer_Normal_TextBox.Location = new System.Drawing.Point(340, 57);
            this.three_Summer_Normal_TextBox.Name = "three_Summer_Normal_TextBox";
            this.three_Summer_Normal_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Summer_Normal_TextBox.TabIndex = 25;
            this.three_Summer_Normal_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_Non_Summer_Peak_TextBox
            // 
            this.three_Non_Summer_Peak_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Non_Summer_Peak_TextBox.Location = new System.Drawing.Point(158, 95);
            this.three_Non_Summer_Peak_TextBox.Name = "three_Non_Summer_Peak_TextBox";
            this.three_Non_Summer_Peak_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Non_Summer_Peak_TextBox.TabIndex = 27;
            this.three_Non_Summer_Peak_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // three_Summer_Peak_TextBox
            // 
            this.three_Summer_Peak_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.three_Summer_Peak_TextBox.Location = new System.Drawing.Point(158, 57);
            this.three_Summer_Peak_TextBox.Name = "three_Summer_Peak_TextBox";
            this.three_Summer_Peak_TextBox.Size = new System.Drawing.Size(145, 30);
            this.three_Summer_Peak_TextBox.TabIndex = 24;
            this.three_Summer_Peak_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(547, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 25);
            this.label9.TabIndex = 23;
            this.label9.Text = "波谷時段";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(367, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 25);
            this.label10.TabIndex = 22;
            this.label10.Text = "一般時段";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(187, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 25);
            this.label11.TabIndex = 21;
            this.label11.Text = "尖峰時段";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(48, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 25);
            this.label12.TabIndex = 20;
            this.label12.Text = "非夏月";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(68, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 25);
            this.label13.TabIndex = 19;
            this.label13.Text = "夏月";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(67, 245);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 28);
            this.label15.TabIndex = 11;
            this.label15.Text = "時段";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(200, 217);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 28);
            this.label16.TabIndex = 12;
            this.label16.Text = "電價時段";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(182, 245);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 51);
            this.label17.TabIndex = 13;
            this.label17.Text = "波峰";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.UseWaitCursor = true;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(230, 245);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 51);
            this.label18.TabIndex = 14;
            this.label18.Text = "平";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label18.UseWaitCursor = true;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(280, 245);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 51);
            this.label19.TabIndex = 15;
            this.label19.Text = "波谷";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(599, 245);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 51);
            this.label20.TabIndex = 20;
            this.label20.Text = "波谷";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label20.UseWaitCursor = true;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(549, 245);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 51);
            this.label21.TabIndex = 19;
            this.label21.Text = "平";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.UseWaitCursor = true;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(501, 245);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 51);
            this.label22.TabIndex = 18;
            this.label22.Text = "波峰";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.UseWaitCursor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(518, 217);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 28);
            this.label23.TabIndex = 17;
            this.label23.Text = "電價時段";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(386, 245);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 28);
            this.label24.TabIndex = 16;
            this.label24.Text = "時段";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(918, 245);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 51);
            this.label25.TabIndex = 25;
            this.label25.Text = "波谷";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.UseWaitCursor = true;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(868, 245);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 51);
            this.label26.TabIndex = 24;
            this.label26.Text = "平";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label26.UseWaitCursor = true;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(820, 245);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 51);
            this.label27.TabIndex = 23;
            this.label27.Text = "波峰";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label27.UseWaitCursor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(836, 217);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 28);
            this.label28.TabIndex = 22;
            this.label28.Text = "電價時段";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(705, 245);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 28);
            this.label29.TabIndex = 21;
            this.label29.Text = "時段";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(1237, 245);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 51);
            this.label30.TabIndex = 30;
            this.label30.Text = "波谷";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label30.UseWaitCursor = true;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(1187, 245);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(33, 51);
            this.label31.TabIndex = 29;
            this.label31.Text = "平";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label31.UseWaitCursor = true;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(1139, 245);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(33, 51);
            this.label32.TabIndex = 28;
            this.label32.Text = "波峰";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label32.UseWaitCursor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(1154, 217);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 28);
            this.label33.TabIndex = 27;
            this.label33.Text = "電價時段";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(1024, 245);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(56, 28);
            this.label34.TabIndex = 26;
            this.label34.Text = "時段";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(1396, 255);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(154, 28);
            this.label35.TabIndex = 31;
            this.label35.Text = "高峰/夏月用電";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Location = new System.Drawing.Point(32, 296);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(1550, 2);
            this.label36.TabIndex = 32;
            // 
            // label37
            // 
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Location = new System.Drawing.Point(1335, 217);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(2, 480);
            this.label37.TabIndex = 33;
            // 
            // label38
            // 
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.Location = new System.Drawing.Point(33, 695);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(1550, 2);
            this.label38.TabIndex = 34;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.e_Price_Name_comboBox);
            this.groupBox3.Location = new System.Drawing.Point(382, 703);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(689, 85);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            // 
            // e_Price_Name_comboBox
            // 
            this.e_Price_Name_comboBox.DataSource = this.eLECTRICITYPRICEBindingSource;
            this.e_Price_Name_comboBox.DisplayMember = "nameStr";
            this.e_Price_Name_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.e_Price_Name_comboBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.e_Price_Name_comboBox.FormattingEnabled = true;
            this.e_Price_Name_comboBox.Location = new System.Drawing.Point(39, 28);
            this.e_Price_Name_comboBox.Name = "e_Price_Name_comboBox";
            this.e_Price_Name_comboBox.Size = new System.Drawing.Size(613, 28);
            this.e_Price_Name_comboBox.TabIndex = 4;
            this.e_Price_Name_comboBox.ValueMember = "e_price_seq";
            this.e_Price_Name_comboBox.SelectionChangeCommitted += new System.EventHandler(this.e_Price_Name_comboBox_SelectionChangeCommitted);
            // 
            // eLECTRICITYPRICEBindingSource
            // 
            this.eLECTRICITYPRICEBindingSource.DataMember = "ELECTRICITY_PRICE";
            this.eLECTRICITYPRICEBindingSource.DataSource = this.iCE_STORAGE_DataSet;
            // 
            // iCE_STORAGE_DataSet
            // 
            this.iCE_STORAGE_DataSet.DataSetName = "ICE_STORAGE_DataSet";
            this.iCE_STORAGE_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // e_Price_Choose_Button
            // 
            this.e_Price_Choose_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.e_Price_Choose_Button.Location = new System.Drawing.Point(1105, 731);
            this.e_Price_Choose_Button.Name = "e_Price_Choose_Button";
            this.e_Price_Choose_Button.Size = new System.Drawing.Size(157, 40);
            this.e_Price_Choose_Button.TabIndex = 3;
            this.e_Price_Choose_Button.Text = "選擇";
            this.e_Price_Choose_Button.UseVisualStyleBackColor = true;
            this.e_Price_Choose_Button.Visible = false;
            this.e_Price_Choose_Button.Click += new System.EventHandler(this.e_Price_Choose_Button_Click);
            // 
            // e_Price_New_Button
            // 
            this.e_Price_New_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.e_Price_New_Button.Location = new System.Drawing.Point(484, 794);
            this.e_Price_New_Button.Name = "e_Price_New_Button";
            this.e_Price_New_Button.Size = new System.Drawing.Size(157, 40);
            this.e_Price_New_Button.TabIndex = 37;
            this.e_Price_New_Button.Text = "新增";
            this.e_Price_New_Button.UseVisualStyleBackColor = true;
            this.e_Price_New_Button.Click += new System.EventHandler(this.e_Price_New_Button_Click);
            // 
            // e_Price_Edit_Button
            // 
            this.e_Price_Edit_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.e_Price_Edit_Button.Location = new System.Drawing.Point(647, 794);
            this.e_Price_Edit_Button.Name = "e_Price_Edit_Button";
            this.e_Price_Edit_Button.Size = new System.Drawing.Size(157, 40);
            this.e_Price_Edit_Button.TabIndex = 38;
            this.e_Price_Edit_Button.Text = "修改";
            this.e_Price_Edit_Button.UseVisualStyleBackColor = true;
            this.e_Price_Edit_Button.Click += new System.EventHandler(this.e_Price_Edit_Button_Click);
            // 
            // e_Price_Delete_Button
            // 
            this.e_Price_Delete_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.e_Price_Delete_Button.Location = new System.Drawing.Point(810, 794);
            this.e_Price_Delete_Button.Name = "e_Price_Delete_Button";
            this.e_Price_Delete_Button.Size = new System.Drawing.Size(157, 40);
            this.e_Price_Delete_Button.TabIndex = 40;
            this.e_Price_Delete_Button.Text = "刪除";
            this.e_Price_Delete_Button.UseVisualStyleBackColor = true;
            this.e_Price_Delete_Button.Click += new System.EventHandler(this.e_Price_Delete_Button_Click);
            // 
            // e_Price_Ok_Button
            // 
            this.e_Price_Ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.e_Price_Ok_Button.Location = new System.Drawing.Point(1406, 724);
            this.e_Price_Ok_Button.Name = "e_Price_Ok_Button";
            this.e_Price_Ok_Button.Size = new System.Drawing.Size(177, 120);
            this.e_Price_Ok_Button.TabIndex = 41;
            this.e_Price_Ok_Button.Text = "確定";
            this.e_Price_Ok_Button.UseVisualStyleBackColor = true;
            this.e_Price_Ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(37, 317);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(133, 25);
            this.label39.TabIndex = 43;
            this.label39.Text = "01:00~01:30";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(37, 347);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(133, 25);
            this.label40.TabIndex = 44;
            this.label40.Text = "01:30~02:00";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(37, 377);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(133, 25);
            this.label41.TabIndex = 45;
            this.label41.Text = "02:00~02:30";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(37, 407);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(133, 25);
            this.label42.TabIndex = 46;
            this.label42.Text = "02:30~03:00";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(37, 437);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(133, 25);
            this.label43.TabIndex = 47;
            this.label43.Text = "03:00~03:30";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(37, 467);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(133, 25);
            this.label44.TabIndex = 48;
            this.label44.Text = "03:30~04:00";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(37, 497);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(133, 25);
            this.label45.TabIndex = 49;
            this.label45.Text = "04:00~04:30";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(37, 527);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(133, 25);
            this.label46.TabIndex = 50;
            this.label46.Text = "04:30~05:00";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(37, 557);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(133, 25);
            this.label47.TabIndex = 51;
            this.label47.Text = "05:00~05:30";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label48.Location = new System.Drawing.Point(37, 587);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(133, 25);
            this.label48.TabIndex = 52;
            this.label48.Text = "05:30~06:00";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(37, 617);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(133, 25);
            this.label49.TabIndex = 53;
            this.label49.Text = "06:00~06:30";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label50.Location = new System.Drawing.Point(37, 647);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(133, 25);
            this.label50.TabIndex = 54;
            this.label50.Text = "06:30~07:00";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label51.Location = new System.Drawing.Point(356, 647);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(133, 25);
            this.label51.TabIndex = 102;
            this.label51.Text = "12:30~13:00";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label52.Location = new System.Drawing.Point(356, 617);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(133, 25);
            this.label52.TabIndex = 101;
            this.label52.Text = "12:00~12:30";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label53.Location = new System.Drawing.Point(356, 587);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(133, 25);
            this.label53.TabIndex = 100;
            this.label53.Text = "11:30~12:00";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label54.Location = new System.Drawing.Point(356, 557);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(133, 25);
            this.label54.TabIndex = 99;
            this.label54.Text = "11:00~11:30";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label55.Location = new System.Drawing.Point(356, 527);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(133, 25);
            this.label55.TabIndex = 98;
            this.label55.Text = "10:30~11:00";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(356, 497);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(133, 25);
            this.label56.TabIndex = 97;
            this.label56.Text = "10:00~10:30";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label57.Location = new System.Drawing.Point(356, 467);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(133, 25);
            this.label57.TabIndex = 96;
            this.label57.Text = "09:30~10:00";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label58.Location = new System.Drawing.Point(356, 437);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(133, 25);
            this.label58.TabIndex = 95;
            this.label58.Text = "09:00~09:30";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label59.Location = new System.Drawing.Point(356, 407);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(133, 25);
            this.label59.TabIndex = 94;
            this.label59.Text = "08:30~09:00";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label60.Location = new System.Drawing.Point(356, 377);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(133, 25);
            this.label60.TabIndex = 93;
            this.label60.Text = "08:00~08:30";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label61.Location = new System.Drawing.Point(356, 347);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(133, 25);
            this.label61.TabIndex = 92;
            this.label61.Text = "07:30~08:00";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label62.Location = new System.Drawing.Point(356, 317);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(133, 25);
            this.label62.TabIndex = 91;
            this.label62.Text = "07:00~07:30";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label63.Location = new System.Drawing.Point(675, 647);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(133, 25);
            this.label63.TabIndex = 150;
            this.label63.Text = "18:30~19:00";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label64.Location = new System.Drawing.Point(675, 617);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(133, 25);
            this.label64.TabIndex = 149;
            this.label64.Text = "18:00~18:30";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label65.Location = new System.Drawing.Point(675, 587);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(133, 25);
            this.label65.TabIndex = 148;
            this.label65.Text = "17:30~18:00";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label66.Location = new System.Drawing.Point(675, 557);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(133, 25);
            this.label66.TabIndex = 147;
            this.label66.Text = "17:00~17:30";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label67.Location = new System.Drawing.Point(675, 527);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(133, 25);
            this.label67.TabIndex = 146;
            this.label67.Text = "16:30~17:00";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label68.Location = new System.Drawing.Point(675, 497);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(133, 25);
            this.label68.TabIndex = 145;
            this.label68.Text = "16:00~16:30";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label69.Location = new System.Drawing.Point(675, 467);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(133, 25);
            this.label69.TabIndex = 144;
            this.label69.Text = "15:30~16:00";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label70.Location = new System.Drawing.Point(675, 437);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(133, 25);
            this.label70.TabIndex = 143;
            this.label70.Text = "15:00~15:30";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label71.Location = new System.Drawing.Point(675, 407);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(133, 25);
            this.label71.TabIndex = 142;
            this.label71.Text = "14:30~15:00";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label72.Location = new System.Drawing.Point(675, 377);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(133, 25);
            this.label72.TabIndex = 141;
            this.label72.Text = "14:00~14:30";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label73.Location = new System.Drawing.Point(675, 347);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(133, 25);
            this.label73.TabIndex = 140;
            this.label73.Text = "13:30~14:00";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label74.Location = new System.Drawing.Point(675, 317);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(133, 25);
            this.label74.TabIndex = 139;
            this.label74.Text = "13:00~13:30";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label75.Location = new System.Drawing.Point(995, 647);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(133, 25);
            this.label75.TabIndex = 198;
            this.label75.Text = "24:30~01:00";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label76.Location = new System.Drawing.Point(995, 617);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(133, 25);
            this.label76.TabIndex = 197;
            this.label76.Text = "24:00~24:30";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label77.Location = new System.Drawing.Point(995, 587);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(133, 25);
            this.label77.TabIndex = 196;
            this.label77.Text = "23:30~24:00";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label78.Location = new System.Drawing.Point(995, 557);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(133, 25);
            this.label78.TabIndex = 195;
            this.label78.Text = "23:00~23:30";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label79.Location = new System.Drawing.Point(995, 527);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(133, 25);
            this.label79.TabIndex = 194;
            this.label79.Text = "22:30~23:00";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label80.Location = new System.Drawing.Point(995, 497);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(133, 25);
            this.label80.TabIndex = 193;
            this.label80.Text = "22:00~22:30";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label81.Location = new System.Drawing.Point(995, 467);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(133, 25);
            this.label81.TabIndex = 192;
            this.label81.Text = "21:30~22:00";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label82.Location = new System.Drawing.Point(995, 437);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(133, 25);
            this.label82.TabIndex = 191;
            this.label82.Text = "21:00~21:30";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label83.Location = new System.Drawing.Point(995, 407);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(133, 25);
            this.label83.TabIndex = 190;
            this.label83.Text = "20:30~21:00";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label84.Location = new System.Drawing.Point(995, 377);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(133, 25);
            this.label84.TabIndex = 189;
            this.label84.Text = "20:00~20:30";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label85.Location = new System.Drawing.Point(995, 347);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(133, 25);
            this.label85.TabIndex = 188;
            this.label85.Text = "19:30~20:00";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label86.Location = new System.Drawing.Point(995, 317);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(133, 25);
            this.label86.TabIndex = 187;
            this.label86.Text = "19:00~19:30";
            // 
            // M1_CheckBox
            // 
            this.M1_CheckBox.AutoSize = true;
            this.M1_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M1_CheckBox.Location = new System.Drawing.Point(1493, 322);
            this.M1_CheckBox.Name = "M1_CheckBox";
            this.M1_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M1_CheckBox.TabIndex = 240;
            this.M1_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label88.Location = new System.Drawing.Point(1426, 317);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(44, 25);
            this.label88.TabIndex = 239;
            this.label88.Text = "1月";
            // 
            // M2_CheckBox
            // 
            this.M2_CheckBox.AutoSize = true;
            this.M2_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M2_CheckBox.Location = new System.Drawing.Point(1493, 352);
            this.M2_CheckBox.Name = "M2_CheckBox";
            this.M2_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M2_CheckBox.TabIndex = 242;
            this.M2_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label87.Location = new System.Drawing.Point(1426, 347);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(44, 25);
            this.label87.TabIndex = 241;
            this.label87.Text = "2月";
            // 
            // M3_CheckBox
            // 
            this.M3_CheckBox.AutoSize = true;
            this.M3_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M3_CheckBox.Location = new System.Drawing.Point(1493, 382);
            this.M3_CheckBox.Name = "M3_CheckBox";
            this.M3_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M3_CheckBox.TabIndex = 244;
            this.M3_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label89.Location = new System.Drawing.Point(1426, 377);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(44, 25);
            this.label89.TabIndex = 243;
            this.label89.Text = "3月";
            // 
            // M4_CheckBox
            // 
            this.M4_CheckBox.AutoSize = true;
            this.M4_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M4_CheckBox.Location = new System.Drawing.Point(1493, 412);
            this.M4_CheckBox.Name = "M4_CheckBox";
            this.M4_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M4_CheckBox.TabIndex = 246;
            this.M4_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label90.Location = new System.Drawing.Point(1426, 407);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(44, 25);
            this.label90.TabIndex = 245;
            this.label90.Text = "4月";
            // 
            // M5_CheckBox
            // 
            this.M5_CheckBox.AutoSize = true;
            this.M5_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M5_CheckBox.Location = new System.Drawing.Point(1493, 442);
            this.M5_CheckBox.Name = "M5_CheckBox";
            this.M5_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M5_CheckBox.TabIndex = 248;
            this.M5_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label91.Location = new System.Drawing.Point(1426, 437);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(44, 25);
            this.label91.TabIndex = 247;
            this.label91.Text = "5月";
            // 
            // M6_CheckBox
            // 
            this.M6_CheckBox.AutoSize = true;
            this.M6_CheckBox.BackColor = System.Drawing.Color.DeepPink;
            this.M6_CheckBox.Checked = true;
            this.M6_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.M6_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M6_CheckBox.Location = new System.Drawing.Point(1493, 472);
            this.M6_CheckBox.Name = "M6_CheckBox";
            this.M6_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M6_CheckBox.TabIndex = 250;
            this.M6_CheckBox.UseVisualStyleBackColor = false;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label92.Location = new System.Drawing.Point(1426, 467);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(44, 25);
            this.label92.TabIndex = 249;
            this.label92.Text = "6月";
            // 
            // M7_CheckBox
            // 
            this.M7_CheckBox.AutoSize = true;
            this.M7_CheckBox.BackColor = System.Drawing.Color.DeepPink;
            this.M7_CheckBox.Checked = true;
            this.M7_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.M7_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M7_CheckBox.Location = new System.Drawing.Point(1493, 502);
            this.M7_CheckBox.Name = "M7_CheckBox";
            this.M7_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M7_CheckBox.TabIndex = 252;
            this.M7_CheckBox.UseVisualStyleBackColor = false;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label93.Location = new System.Drawing.Point(1426, 497);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(44, 25);
            this.label93.TabIndex = 251;
            this.label93.Text = "7月";
            // 
            // M8_CheckBox
            // 
            this.M8_CheckBox.AutoSize = true;
            this.M8_CheckBox.BackColor = System.Drawing.Color.DeepPink;
            this.M8_CheckBox.Checked = true;
            this.M8_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.M8_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M8_CheckBox.Location = new System.Drawing.Point(1493, 532);
            this.M8_CheckBox.Name = "M8_CheckBox";
            this.M8_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M8_CheckBox.TabIndex = 254;
            this.M8_CheckBox.UseVisualStyleBackColor = false;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label94.Location = new System.Drawing.Point(1426, 527);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(44, 25);
            this.label94.TabIndex = 253;
            this.label94.Text = "8月";
            // 
            // M9_CheckBox
            // 
            this.M9_CheckBox.AutoSize = true;
            this.M9_CheckBox.BackColor = System.Drawing.Color.DeepPink;
            this.M9_CheckBox.Checked = true;
            this.M9_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.M9_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M9_CheckBox.Location = new System.Drawing.Point(1493, 562);
            this.M9_CheckBox.Name = "M9_CheckBox";
            this.M9_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M9_CheckBox.TabIndex = 256;
            this.M9_CheckBox.UseVisualStyleBackColor = false;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label95.Location = new System.Drawing.Point(1426, 557);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(44, 25);
            this.label95.TabIndex = 255;
            this.label95.Text = "9月";
            // 
            // M10_CheckBox
            // 
            this.M10_CheckBox.AutoSize = true;
            this.M10_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M10_CheckBox.Location = new System.Drawing.Point(1493, 592);
            this.M10_CheckBox.Name = "M10_CheckBox";
            this.M10_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M10_CheckBox.TabIndex = 258;
            this.M10_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label96.Location = new System.Drawing.Point(1414, 587);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(56, 25);
            this.label96.TabIndex = 257;
            this.label96.Text = "10月";
            // 
            // M11_CheckBox
            // 
            this.M11_CheckBox.AutoSize = true;
            this.M11_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M11_CheckBox.Location = new System.Drawing.Point(1493, 622);
            this.M11_CheckBox.Name = "M11_CheckBox";
            this.M11_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M11_CheckBox.TabIndex = 260;
            this.M11_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label97.Location = new System.Drawing.Point(1414, 617);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(56, 25);
            this.label97.TabIndex = 259;
            this.label97.Text = "11月";
            // 
            // M12_CheckBox
            // 
            this.M12_CheckBox.AutoSize = true;
            this.M12_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.M12_CheckBox.Location = new System.Drawing.Point(1493, 652);
            this.M12_CheckBox.Name = "M12_CheckBox";
            this.M12_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.M12_CheckBox.TabIndex = 262;
            this.M12_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label98.Location = new System.Drawing.Point(1414, 647);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(56, 25);
            this.label98.TabIndex = 261;
            this.label98.Text = "12月";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.trough_RadioButton_1);
            this.panel1.Controls.Add(this.flat_RadioButton_1);
            this.panel1.Controls.Add(this.crest_RadioButton_1);
            this.panel1.Location = new System.Drawing.Point(186, 317);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(127, 25);
            this.panel1.TabIndex = 263;
            // 
            // trough_RadioButton_1
            // 
            this.trough_RadioButton_1.AutoSize = true;
            this.trough_RadioButton_1.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_1.Checked = true;
            this.trough_RadioButton_1.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_1.Name = "trough_RadioButton_1";
            this.trough_RadioButton_1.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_1.TabIndex = 266;
            this.trough_RadioButton_1.TabStop = true;
            this.trough_RadioButton_1.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_1
            // 
            this.flat_RadioButton_1.AutoSize = true;
            this.flat_RadioButton_1.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_1.Name = "flat_RadioButton_1";
            this.flat_RadioButton_1.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_1.TabIndex = 265;
            this.flat_RadioButton_1.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_1
            // 
            this.crest_RadioButton_1.AutoSize = true;
            this.crest_RadioButton_1.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_1.Name = "crest_RadioButton_1";
            this.crest_RadioButton_1.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_1.TabIndex = 264;
            this.crest_RadioButton_1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.trough_RadioButton_2);
            this.panel2.Controls.Add(this.flat_RadioButton_2);
            this.panel2.Controls.Add(this.crest_RadioButton_2);
            this.panel2.Location = new System.Drawing.Point(186, 347);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 25);
            this.panel2.TabIndex = 264;
            // 
            // trough_RadioButton_2
            // 
            this.trough_RadioButton_2.AutoSize = true;
            this.trough_RadioButton_2.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_2.Checked = true;
            this.trough_RadioButton_2.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_2.Name = "trough_RadioButton_2";
            this.trough_RadioButton_2.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_2.TabIndex = 266;
            this.trough_RadioButton_2.TabStop = true;
            this.trough_RadioButton_2.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_2
            // 
            this.flat_RadioButton_2.AutoSize = true;
            this.flat_RadioButton_2.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_2.Name = "flat_RadioButton_2";
            this.flat_RadioButton_2.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_2.TabIndex = 265;
            this.flat_RadioButton_2.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_2
            // 
            this.crest_RadioButton_2.AutoSize = true;
            this.crest_RadioButton_2.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_2.Name = "crest_RadioButton_2";
            this.crest_RadioButton_2.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_2.TabIndex = 264;
            this.crest_RadioButton_2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.trough_RadioButton_3);
            this.panel3.Controls.Add(this.flat_RadioButton_3);
            this.panel3.Controls.Add(this.crest_RadioButton_3);
            this.panel3.Location = new System.Drawing.Point(186, 377);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(127, 25);
            this.panel3.TabIndex = 265;
            // 
            // trough_RadioButton_3
            // 
            this.trough_RadioButton_3.AutoSize = true;
            this.trough_RadioButton_3.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_3.Checked = true;
            this.trough_RadioButton_3.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_3.Name = "trough_RadioButton_3";
            this.trough_RadioButton_3.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_3.TabIndex = 266;
            this.trough_RadioButton_3.TabStop = true;
            this.trough_RadioButton_3.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_3
            // 
            this.flat_RadioButton_3.AutoSize = true;
            this.flat_RadioButton_3.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_3.Name = "flat_RadioButton_3";
            this.flat_RadioButton_3.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_3.TabIndex = 265;
            this.flat_RadioButton_3.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_3
            // 
            this.crest_RadioButton_3.AutoSize = true;
            this.crest_RadioButton_3.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_3.Name = "crest_RadioButton_3";
            this.crest_RadioButton_3.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_3.TabIndex = 264;
            this.crest_RadioButton_3.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.trough_RadioButton_4);
            this.panel4.Controls.Add(this.flat_RadioButton_4);
            this.panel4.Controls.Add(this.crest_RadioButton_4);
            this.panel4.Location = new System.Drawing.Point(186, 407);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(127, 25);
            this.panel4.TabIndex = 266;
            // 
            // trough_RadioButton_4
            // 
            this.trough_RadioButton_4.AutoSize = true;
            this.trough_RadioButton_4.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_4.Checked = true;
            this.trough_RadioButton_4.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_4.Name = "trough_RadioButton_4";
            this.trough_RadioButton_4.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_4.TabIndex = 266;
            this.trough_RadioButton_4.TabStop = true;
            this.trough_RadioButton_4.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_4
            // 
            this.flat_RadioButton_4.AutoSize = true;
            this.flat_RadioButton_4.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_4.Name = "flat_RadioButton_4";
            this.flat_RadioButton_4.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_4.TabIndex = 265;
            this.flat_RadioButton_4.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_4
            // 
            this.crest_RadioButton_4.AutoSize = true;
            this.crest_RadioButton_4.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_4.Name = "crest_RadioButton_4";
            this.crest_RadioButton_4.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_4.TabIndex = 264;
            this.crest_RadioButton_4.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.trough_RadioButton_5);
            this.panel5.Controls.Add(this.flat_RadioButton_5);
            this.panel5.Controls.Add(this.crest_RadioButton_5);
            this.panel5.Location = new System.Drawing.Point(186, 437);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(127, 25);
            this.panel5.TabIndex = 267;
            // 
            // trough_RadioButton_5
            // 
            this.trough_RadioButton_5.AutoSize = true;
            this.trough_RadioButton_5.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_5.Checked = true;
            this.trough_RadioButton_5.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_5.Name = "trough_RadioButton_5";
            this.trough_RadioButton_5.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_5.TabIndex = 266;
            this.trough_RadioButton_5.TabStop = true;
            this.trough_RadioButton_5.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_5
            // 
            this.flat_RadioButton_5.AutoSize = true;
            this.flat_RadioButton_5.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_5.Name = "flat_RadioButton_5";
            this.flat_RadioButton_5.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_5.TabIndex = 265;
            this.flat_RadioButton_5.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_5
            // 
            this.crest_RadioButton_5.AutoSize = true;
            this.crest_RadioButton_5.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_5.Name = "crest_RadioButton_5";
            this.crest_RadioButton_5.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_5.TabIndex = 264;
            this.crest_RadioButton_5.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.trough_RadioButton_10);
            this.panel10.Controls.Add(this.flat_RadioButton_10);
            this.panel10.Controls.Add(this.crest_RadioButton_10);
            this.panel10.Location = new System.Drawing.Point(186, 587);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(127, 25);
            this.panel10.TabIndex = 272;
            // 
            // trough_RadioButton_10
            // 
            this.trough_RadioButton_10.AutoSize = true;
            this.trough_RadioButton_10.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_10.Checked = true;
            this.trough_RadioButton_10.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_10.Name = "trough_RadioButton_10";
            this.trough_RadioButton_10.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_10.TabIndex = 266;
            this.trough_RadioButton_10.TabStop = true;
            this.trough_RadioButton_10.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_10
            // 
            this.flat_RadioButton_10.AutoSize = true;
            this.flat_RadioButton_10.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_10.Name = "flat_RadioButton_10";
            this.flat_RadioButton_10.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_10.TabIndex = 265;
            this.flat_RadioButton_10.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_10
            // 
            this.crest_RadioButton_10.AutoSize = true;
            this.crest_RadioButton_10.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_10.Name = "crest_RadioButton_10";
            this.crest_RadioButton_10.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_10.TabIndex = 264;
            this.crest_RadioButton_10.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.trough_RadioButton_9);
            this.panel9.Controls.Add(this.flat_RadioButton_9);
            this.panel9.Controls.Add(this.crest_RadioButton_9);
            this.panel9.Location = new System.Drawing.Point(186, 557);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(127, 25);
            this.panel9.TabIndex = 271;
            // 
            // trough_RadioButton_9
            // 
            this.trough_RadioButton_9.AutoSize = true;
            this.trough_RadioButton_9.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_9.Checked = true;
            this.trough_RadioButton_9.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_9.Name = "trough_RadioButton_9";
            this.trough_RadioButton_9.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_9.TabIndex = 266;
            this.trough_RadioButton_9.TabStop = true;
            this.trough_RadioButton_9.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_9
            // 
            this.flat_RadioButton_9.AutoSize = true;
            this.flat_RadioButton_9.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_9.Name = "flat_RadioButton_9";
            this.flat_RadioButton_9.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_9.TabIndex = 265;
            this.flat_RadioButton_9.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_9
            // 
            this.crest_RadioButton_9.AutoSize = true;
            this.crest_RadioButton_9.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_9.Name = "crest_RadioButton_9";
            this.crest_RadioButton_9.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_9.TabIndex = 264;
            this.crest_RadioButton_9.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.trough_RadioButton_8);
            this.panel8.Controls.Add(this.flat_RadioButton_8);
            this.panel8.Controls.Add(this.crest_RadioButton_8);
            this.panel8.Location = new System.Drawing.Point(186, 527);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(127, 25);
            this.panel8.TabIndex = 270;
            // 
            // trough_RadioButton_8
            // 
            this.trough_RadioButton_8.AutoSize = true;
            this.trough_RadioButton_8.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_8.Checked = true;
            this.trough_RadioButton_8.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_8.Name = "trough_RadioButton_8";
            this.trough_RadioButton_8.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_8.TabIndex = 266;
            this.trough_RadioButton_8.TabStop = true;
            this.trough_RadioButton_8.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_8
            // 
            this.flat_RadioButton_8.AutoSize = true;
            this.flat_RadioButton_8.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_8.Name = "flat_RadioButton_8";
            this.flat_RadioButton_8.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_8.TabIndex = 265;
            this.flat_RadioButton_8.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_8
            // 
            this.crest_RadioButton_8.AutoSize = true;
            this.crest_RadioButton_8.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_8.Name = "crest_RadioButton_8";
            this.crest_RadioButton_8.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_8.TabIndex = 264;
            this.crest_RadioButton_8.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.trough_RadioButton_7);
            this.panel7.Controls.Add(this.flat_RadioButton_7);
            this.panel7.Controls.Add(this.crest_RadioButton_7);
            this.panel7.Location = new System.Drawing.Point(186, 497);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(127, 25);
            this.panel7.TabIndex = 269;
            // 
            // trough_RadioButton_7
            // 
            this.trough_RadioButton_7.AutoSize = true;
            this.trough_RadioButton_7.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_7.Checked = true;
            this.trough_RadioButton_7.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_7.Name = "trough_RadioButton_7";
            this.trough_RadioButton_7.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_7.TabIndex = 266;
            this.trough_RadioButton_7.TabStop = true;
            this.trough_RadioButton_7.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_7
            // 
            this.flat_RadioButton_7.AutoSize = true;
            this.flat_RadioButton_7.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_7.Name = "flat_RadioButton_7";
            this.flat_RadioButton_7.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_7.TabIndex = 265;
            this.flat_RadioButton_7.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_7
            // 
            this.crest_RadioButton_7.AutoSize = true;
            this.crest_RadioButton_7.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_7.Name = "crest_RadioButton_7";
            this.crest_RadioButton_7.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_7.TabIndex = 264;
            this.crest_RadioButton_7.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.trough_RadioButton_6);
            this.panel6.Controls.Add(this.flat_RadioButton_6);
            this.panel6.Controls.Add(this.crest_RadioButton_6);
            this.panel6.Location = new System.Drawing.Point(186, 467);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(127, 25);
            this.panel6.TabIndex = 268;
            // 
            // trough_RadioButton_6
            // 
            this.trough_RadioButton_6.AutoSize = true;
            this.trough_RadioButton_6.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_6.Checked = true;
            this.trough_RadioButton_6.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_6.Name = "trough_RadioButton_6";
            this.trough_RadioButton_6.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_6.TabIndex = 266;
            this.trough_RadioButton_6.TabStop = true;
            this.trough_RadioButton_6.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_6
            // 
            this.flat_RadioButton_6.AutoSize = true;
            this.flat_RadioButton_6.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_6.Name = "flat_RadioButton_6";
            this.flat_RadioButton_6.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_6.TabIndex = 265;
            this.flat_RadioButton_6.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_6
            // 
            this.crest_RadioButton_6.AutoSize = true;
            this.crest_RadioButton_6.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_6.Name = "crest_RadioButton_6";
            this.crest_RadioButton_6.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_6.TabIndex = 264;
            this.crest_RadioButton_6.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.trough_RadioButton_12);
            this.panel12.Controls.Add(this.flat_RadioButton_12);
            this.panel12.Controls.Add(this.crest_RadioButton_12);
            this.panel12.Location = new System.Drawing.Point(186, 647);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(127, 25);
            this.panel12.TabIndex = 274;
            // 
            // trough_RadioButton_12
            // 
            this.trough_RadioButton_12.AutoSize = true;
            this.trough_RadioButton_12.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_12.Checked = true;
            this.trough_RadioButton_12.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_12.Name = "trough_RadioButton_12";
            this.trough_RadioButton_12.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_12.TabIndex = 266;
            this.trough_RadioButton_12.TabStop = true;
            this.trough_RadioButton_12.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_12
            // 
            this.flat_RadioButton_12.AutoSize = true;
            this.flat_RadioButton_12.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_12.Name = "flat_RadioButton_12";
            this.flat_RadioButton_12.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_12.TabIndex = 265;
            this.flat_RadioButton_12.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_12
            // 
            this.crest_RadioButton_12.AutoSize = true;
            this.crest_RadioButton_12.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_12.Name = "crest_RadioButton_12";
            this.crest_RadioButton_12.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_12.TabIndex = 264;
            this.crest_RadioButton_12.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.trough_RadioButton_11);
            this.panel11.Controls.Add(this.flat_RadioButton_11);
            this.panel11.Controls.Add(this.crest_RadioButton_11);
            this.panel11.Location = new System.Drawing.Point(186, 617);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(127, 25);
            this.panel11.TabIndex = 273;
            // 
            // trough_RadioButton_11
            // 
            this.trough_RadioButton_11.AutoSize = true;
            this.trough_RadioButton_11.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_11.Checked = true;
            this.trough_RadioButton_11.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_11.Name = "trough_RadioButton_11";
            this.trough_RadioButton_11.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_11.TabIndex = 266;
            this.trough_RadioButton_11.TabStop = true;
            this.trough_RadioButton_11.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_11
            // 
            this.flat_RadioButton_11.AutoSize = true;
            this.flat_RadioButton_11.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_11.Name = "flat_RadioButton_11";
            this.flat_RadioButton_11.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_11.TabIndex = 265;
            this.flat_RadioButton_11.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_11
            // 
            this.crest_RadioButton_11.AutoSize = true;
            this.crest_RadioButton_11.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_11.Name = "crest_RadioButton_11";
            this.crest_RadioButton_11.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_11.TabIndex = 264;
            this.crest_RadioButton_11.UseVisualStyleBackColor = true;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.trough_RadioButton_24);
            this.panel24.Controls.Add(this.flat_RadioButton_24);
            this.panel24.Controls.Add(this.crest_RadioButton_24);
            this.panel24.Location = new System.Drawing.Point(505, 647);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(127, 25);
            this.panel24.TabIndex = 286;
            // 
            // trough_RadioButton_24
            // 
            this.trough_RadioButton_24.AutoSize = true;
            this.trough_RadioButton_24.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_24.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_24.Name = "trough_RadioButton_24";
            this.trough_RadioButton_24.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_24.TabIndex = 266;
            this.trough_RadioButton_24.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_24
            // 
            this.flat_RadioButton_24.AutoSize = true;
            this.flat_RadioButton_24.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_24.Checked = true;
            this.flat_RadioButton_24.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_24.Name = "flat_RadioButton_24";
            this.flat_RadioButton_24.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_24.TabIndex = 265;
            this.flat_RadioButton_24.TabStop = true;
            this.flat_RadioButton_24.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_24
            // 
            this.crest_RadioButton_24.AutoSize = true;
            this.crest_RadioButton_24.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_24.Name = "crest_RadioButton_24";
            this.crest_RadioButton_24.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_24.TabIndex = 264;
            this.crest_RadioButton_24.UseVisualStyleBackColor = true;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.trough_RadioButton_23);
            this.panel23.Controls.Add(this.flat_RadioButton_23);
            this.panel23.Controls.Add(this.crest_RadioButton_23);
            this.panel23.Location = new System.Drawing.Point(505, 617);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(127, 25);
            this.panel23.TabIndex = 285;
            // 
            // trough_RadioButton_23
            // 
            this.trough_RadioButton_23.AutoSize = true;
            this.trough_RadioButton_23.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_23.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_23.Name = "trough_RadioButton_23";
            this.trough_RadioButton_23.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_23.TabIndex = 266;
            this.trough_RadioButton_23.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_23
            // 
            this.flat_RadioButton_23.AutoSize = true;
            this.flat_RadioButton_23.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_23.Checked = true;
            this.flat_RadioButton_23.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_23.Name = "flat_RadioButton_23";
            this.flat_RadioButton_23.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_23.TabIndex = 265;
            this.flat_RadioButton_23.TabStop = true;
            this.flat_RadioButton_23.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_23
            // 
            this.crest_RadioButton_23.AutoSize = true;
            this.crest_RadioButton_23.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_23.Name = "crest_RadioButton_23";
            this.crest_RadioButton_23.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_23.TabIndex = 264;
            this.crest_RadioButton_23.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.trough_RadioButton_22);
            this.panel22.Controls.Add(this.flat_RadioButton_22);
            this.panel22.Controls.Add(this.crest_RadioButton_22);
            this.panel22.Location = new System.Drawing.Point(505, 587);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(127, 25);
            this.panel22.TabIndex = 284;
            // 
            // trough_RadioButton_22
            // 
            this.trough_RadioButton_22.AutoSize = true;
            this.trough_RadioButton_22.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_22.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_22.Name = "trough_RadioButton_22";
            this.trough_RadioButton_22.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_22.TabIndex = 266;
            this.trough_RadioButton_22.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_22
            // 
            this.flat_RadioButton_22.AutoSize = true;
            this.flat_RadioButton_22.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_22.Name = "flat_RadioButton_22";
            this.flat_RadioButton_22.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_22.TabIndex = 265;
            this.flat_RadioButton_22.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_22
            // 
            this.crest_RadioButton_22.AutoSize = true;
            this.crest_RadioButton_22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_22.Checked = true;
            this.crest_RadioButton_22.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_22.Name = "crest_RadioButton_22";
            this.crest_RadioButton_22.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_22.TabIndex = 264;
            this.crest_RadioButton_22.TabStop = true;
            this.crest_RadioButton_22.UseVisualStyleBackColor = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.trough_RadioButton_21);
            this.panel21.Controls.Add(this.flat_RadioButton_21);
            this.panel21.Controls.Add(this.crest_RadioButton_21);
            this.panel21.Location = new System.Drawing.Point(505, 557);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(127, 25);
            this.panel21.TabIndex = 283;
            // 
            // trough_RadioButton_21
            // 
            this.trough_RadioButton_21.AutoSize = true;
            this.trough_RadioButton_21.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_21.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_21.Name = "trough_RadioButton_21";
            this.trough_RadioButton_21.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_21.TabIndex = 266;
            this.trough_RadioButton_21.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_21
            // 
            this.flat_RadioButton_21.AutoSize = true;
            this.flat_RadioButton_21.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_21.Name = "flat_RadioButton_21";
            this.flat_RadioButton_21.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_21.TabIndex = 265;
            this.flat_RadioButton_21.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_21
            // 
            this.crest_RadioButton_21.AutoSize = true;
            this.crest_RadioButton_21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_21.Checked = true;
            this.crest_RadioButton_21.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_21.Name = "crest_RadioButton_21";
            this.crest_RadioButton_21.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_21.TabIndex = 264;
            this.crest_RadioButton_21.TabStop = true;
            this.crest_RadioButton_21.UseVisualStyleBackColor = false;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.trough_RadioButton_20);
            this.panel20.Controls.Add(this.flat_RadioButton_20);
            this.panel20.Controls.Add(this.crest_RadioButton_20);
            this.panel20.Location = new System.Drawing.Point(505, 527);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(127, 25);
            this.panel20.TabIndex = 282;
            // 
            // trough_RadioButton_20
            // 
            this.trough_RadioButton_20.AutoSize = true;
            this.trough_RadioButton_20.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_20.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_20.Name = "trough_RadioButton_20";
            this.trough_RadioButton_20.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_20.TabIndex = 266;
            this.trough_RadioButton_20.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_20
            // 
            this.flat_RadioButton_20.AutoSize = true;
            this.flat_RadioButton_20.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_20.Name = "flat_RadioButton_20";
            this.flat_RadioButton_20.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_20.TabIndex = 265;
            this.flat_RadioButton_20.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_20
            // 
            this.crest_RadioButton_20.AutoSize = true;
            this.crest_RadioButton_20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_20.Checked = true;
            this.crest_RadioButton_20.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_20.Name = "crest_RadioButton_20";
            this.crest_RadioButton_20.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_20.TabIndex = 264;
            this.crest_RadioButton_20.TabStop = true;
            this.crest_RadioButton_20.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.trough_RadioButton_19);
            this.panel19.Controls.Add(this.flat_RadioButton_19);
            this.panel19.Controls.Add(this.crest_RadioButton_19);
            this.panel19.Location = new System.Drawing.Point(505, 497);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(127, 25);
            this.panel19.TabIndex = 281;
            // 
            // trough_RadioButton_19
            // 
            this.trough_RadioButton_19.AutoSize = true;
            this.trough_RadioButton_19.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_19.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_19.Name = "trough_RadioButton_19";
            this.trough_RadioButton_19.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_19.TabIndex = 266;
            this.trough_RadioButton_19.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_19
            // 
            this.flat_RadioButton_19.AutoSize = true;
            this.flat_RadioButton_19.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_19.Name = "flat_RadioButton_19";
            this.flat_RadioButton_19.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_19.TabIndex = 265;
            this.flat_RadioButton_19.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_19
            // 
            this.crest_RadioButton_19.AutoSize = true;
            this.crest_RadioButton_19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_19.Checked = true;
            this.crest_RadioButton_19.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_19.Name = "crest_RadioButton_19";
            this.crest_RadioButton_19.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_19.TabIndex = 264;
            this.crest_RadioButton_19.TabStop = true;
            this.crest_RadioButton_19.UseVisualStyleBackColor = false;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.trough_RadioButton_18);
            this.panel18.Controls.Add(this.flat_RadioButton_18);
            this.panel18.Controls.Add(this.crest_RadioButton_18);
            this.panel18.Location = new System.Drawing.Point(505, 467);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(127, 25);
            this.panel18.TabIndex = 280;
            // 
            // trough_RadioButton_18
            // 
            this.trough_RadioButton_18.AutoSize = true;
            this.trough_RadioButton_18.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_18.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_18.Name = "trough_RadioButton_18";
            this.trough_RadioButton_18.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_18.TabIndex = 266;
            this.trough_RadioButton_18.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_18
            // 
            this.flat_RadioButton_18.AutoSize = true;
            this.flat_RadioButton_18.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_18.Checked = true;
            this.flat_RadioButton_18.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_18.Name = "flat_RadioButton_18";
            this.flat_RadioButton_18.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_18.TabIndex = 265;
            this.flat_RadioButton_18.TabStop = true;
            this.flat_RadioButton_18.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_18
            // 
            this.crest_RadioButton_18.AutoSize = true;
            this.crest_RadioButton_18.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_18.Name = "crest_RadioButton_18";
            this.crest_RadioButton_18.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_18.TabIndex = 264;
            this.crest_RadioButton_18.UseVisualStyleBackColor = true;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.trough_RadioButton_17);
            this.panel17.Controls.Add(this.flat_RadioButton_17);
            this.panel17.Controls.Add(this.crest_RadioButton_17);
            this.panel17.Location = new System.Drawing.Point(505, 437);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(127, 25);
            this.panel17.TabIndex = 279;
            // 
            // trough_RadioButton_17
            // 
            this.trough_RadioButton_17.AutoSize = true;
            this.trough_RadioButton_17.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_17.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_17.Name = "trough_RadioButton_17";
            this.trough_RadioButton_17.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_17.TabIndex = 266;
            this.trough_RadioButton_17.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_17
            // 
            this.flat_RadioButton_17.AutoSize = true;
            this.flat_RadioButton_17.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_17.Checked = true;
            this.flat_RadioButton_17.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_17.Name = "flat_RadioButton_17";
            this.flat_RadioButton_17.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_17.TabIndex = 265;
            this.flat_RadioButton_17.TabStop = true;
            this.flat_RadioButton_17.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_17
            // 
            this.crest_RadioButton_17.AutoSize = true;
            this.crest_RadioButton_17.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_17.Name = "crest_RadioButton_17";
            this.crest_RadioButton_17.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_17.TabIndex = 264;
            this.crest_RadioButton_17.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.trough_RadioButton_16);
            this.panel16.Controls.Add(this.flat_RadioButton_16);
            this.panel16.Controls.Add(this.crest_RadioButton_16);
            this.panel16.Location = new System.Drawing.Point(505, 407);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(127, 25);
            this.panel16.TabIndex = 278;
            // 
            // trough_RadioButton_16
            // 
            this.trough_RadioButton_16.AutoSize = true;
            this.trough_RadioButton_16.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_16.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_16.Name = "trough_RadioButton_16";
            this.trough_RadioButton_16.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_16.TabIndex = 266;
            this.trough_RadioButton_16.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_16
            // 
            this.flat_RadioButton_16.AutoSize = true;
            this.flat_RadioButton_16.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_16.Checked = true;
            this.flat_RadioButton_16.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_16.Name = "flat_RadioButton_16";
            this.flat_RadioButton_16.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_16.TabIndex = 265;
            this.flat_RadioButton_16.TabStop = true;
            this.flat_RadioButton_16.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_16
            // 
            this.crest_RadioButton_16.AutoSize = true;
            this.crest_RadioButton_16.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_16.Name = "crest_RadioButton_16";
            this.crest_RadioButton_16.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_16.TabIndex = 264;
            this.crest_RadioButton_16.UseVisualStyleBackColor = true;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.trough_RadioButton_15);
            this.panel15.Controls.Add(this.flat_RadioButton_15);
            this.panel15.Controls.Add(this.crest_RadioButton_15);
            this.panel15.Location = new System.Drawing.Point(505, 377);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(127, 25);
            this.panel15.TabIndex = 277;
            // 
            // trough_RadioButton_15
            // 
            this.trough_RadioButton_15.AutoSize = true;
            this.trough_RadioButton_15.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_15.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_15.Name = "trough_RadioButton_15";
            this.trough_RadioButton_15.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_15.TabIndex = 266;
            this.trough_RadioButton_15.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_15
            // 
            this.flat_RadioButton_15.AutoSize = true;
            this.flat_RadioButton_15.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_15.Checked = true;
            this.flat_RadioButton_15.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_15.Name = "flat_RadioButton_15";
            this.flat_RadioButton_15.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_15.TabIndex = 265;
            this.flat_RadioButton_15.TabStop = true;
            this.flat_RadioButton_15.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_15
            // 
            this.crest_RadioButton_15.AutoSize = true;
            this.crest_RadioButton_15.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_15.Name = "crest_RadioButton_15";
            this.crest_RadioButton_15.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_15.TabIndex = 264;
            this.crest_RadioButton_15.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.trough_RadioButton_14);
            this.panel14.Controls.Add(this.flat_RadioButton_14);
            this.panel14.Controls.Add(this.crest_RadioButton_14);
            this.panel14.Location = new System.Drawing.Point(505, 347);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(127, 25);
            this.panel14.TabIndex = 276;
            // 
            // trough_RadioButton_14
            // 
            this.trough_RadioButton_14.AutoSize = true;
            this.trough_RadioButton_14.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_14.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_14.Name = "trough_RadioButton_14";
            this.trough_RadioButton_14.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_14.TabIndex = 266;
            this.trough_RadioButton_14.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_14
            // 
            this.flat_RadioButton_14.AutoSize = true;
            this.flat_RadioButton_14.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_14.Checked = true;
            this.flat_RadioButton_14.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_14.Name = "flat_RadioButton_14";
            this.flat_RadioButton_14.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_14.TabIndex = 265;
            this.flat_RadioButton_14.TabStop = true;
            this.flat_RadioButton_14.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_14
            // 
            this.crest_RadioButton_14.AutoSize = true;
            this.crest_RadioButton_14.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_14.Name = "crest_RadioButton_14";
            this.crest_RadioButton_14.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_14.TabIndex = 264;
            this.crest_RadioButton_14.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.trough_RadioButton_13);
            this.panel13.Controls.Add(this.flat_RadioButton_13);
            this.panel13.Controls.Add(this.crest_RadioButton_13);
            this.panel13.Location = new System.Drawing.Point(505, 317);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(127, 25);
            this.panel13.TabIndex = 275;
            // 
            // trough_RadioButton_13
            // 
            this.trough_RadioButton_13.AutoSize = true;
            this.trough_RadioButton_13.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_13.Checked = true;
            this.trough_RadioButton_13.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_13.Name = "trough_RadioButton_13";
            this.trough_RadioButton_13.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_13.TabIndex = 266;
            this.trough_RadioButton_13.TabStop = true;
            this.trough_RadioButton_13.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_13
            // 
            this.flat_RadioButton_13.AutoSize = true;
            this.flat_RadioButton_13.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_13.Name = "flat_RadioButton_13";
            this.flat_RadioButton_13.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_13.TabIndex = 265;
            this.flat_RadioButton_13.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_13
            // 
            this.crest_RadioButton_13.AutoSize = true;
            this.crest_RadioButton_13.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_13.Name = "crest_RadioButton_13";
            this.crest_RadioButton_13.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_13.TabIndex = 264;
            this.crest_RadioButton_13.UseVisualStyleBackColor = true;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.trough_RadioButton_36);
            this.panel36.Controls.Add(this.flat_RadioButton_36);
            this.panel36.Controls.Add(this.crest_RadioButton_36);
            this.panel36.Location = new System.Drawing.Point(824, 647);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(127, 25);
            this.panel36.TabIndex = 298;
            // 
            // trough_RadioButton_36
            // 
            this.trough_RadioButton_36.AutoSize = true;
            this.trough_RadioButton_36.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_36.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_36.Name = "trough_RadioButton_36";
            this.trough_RadioButton_36.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_36.TabIndex = 266;
            this.trough_RadioButton_36.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_36
            // 
            this.flat_RadioButton_36.AutoSize = true;
            this.flat_RadioButton_36.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_36.Checked = true;
            this.flat_RadioButton_36.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_36.Name = "flat_RadioButton_36";
            this.flat_RadioButton_36.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_36.TabIndex = 265;
            this.flat_RadioButton_36.TabStop = true;
            this.flat_RadioButton_36.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_36
            // 
            this.crest_RadioButton_36.AutoSize = true;
            this.crest_RadioButton_36.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_36.Name = "crest_RadioButton_36";
            this.crest_RadioButton_36.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_36.TabIndex = 264;
            this.crest_RadioButton_36.UseVisualStyleBackColor = true;
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.trough_RadioButton_35);
            this.panel35.Controls.Add(this.flat_RadioButton_35);
            this.panel35.Controls.Add(this.crest_RadioButton_35);
            this.panel35.Location = new System.Drawing.Point(824, 617);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(127, 25);
            this.panel35.TabIndex = 297;
            // 
            // trough_RadioButton_35
            // 
            this.trough_RadioButton_35.AutoSize = true;
            this.trough_RadioButton_35.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_35.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_35.Name = "trough_RadioButton_35";
            this.trough_RadioButton_35.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_35.TabIndex = 266;
            this.trough_RadioButton_35.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_35
            // 
            this.flat_RadioButton_35.AutoSize = true;
            this.flat_RadioButton_35.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_35.Checked = true;
            this.flat_RadioButton_35.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_35.Name = "flat_RadioButton_35";
            this.flat_RadioButton_35.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_35.TabIndex = 265;
            this.flat_RadioButton_35.TabStop = true;
            this.flat_RadioButton_35.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_35
            // 
            this.crest_RadioButton_35.AutoSize = true;
            this.crest_RadioButton_35.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_35.Name = "crest_RadioButton_35";
            this.crest_RadioButton_35.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_35.TabIndex = 264;
            this.crest_RadioButton_35.UseVisualStyleBackColor = true;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.trough_RadioButton_34);
            this.panel34.Controls.Add(this.flat_RadioButton_34);
            this.panel34.Controls.Add(this.crest_RadioButton_34);
            this.panel34.Location = new System.Drawing.Point(824, 587);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(127, 25);
            this.panel34.TabIndex = 296;
            // 
            // trough_RadioButton_34
            // 
            this.trough_RadioButton_34.AutoSize = true;
            this.trough_RadioButton_34.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_34.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_34.Name = "trough_RadioButton_34";
            this.trough_RadioButton_34.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_34.TabIndex = 266;
            this.trough_RadioButton_34.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_34
            // 
            this.flat_RadioButton_34.AutoSize = true;
            this.flat_RadioButton_34.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_34.Checked = true;
            this.flat_RadioButton_34.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_34.Name = "flat_RadioButton_34";
            this.flat_RadioButton_34.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_34.TabIndex = 265;
            this.flat_RadioButton_34.TabStop = true;
            this.flat_RadioButton_34.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_34
            // 
            this.crest_RadioButton_34.AutoSize = true;
            this.crest_RadioButton_34.BackColor = System.Drawing.SystemColors.Control;
            this.crest_RadioButton_34.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_34.Name = "crest_RadioButton_34";
            this.crest_RadioButton_34.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_34.TabIndex = 264;
            this.crest_RadioButton_34.UseVisualStyleBackColor = false;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.trough_RadioButton_33);
            this.panel33.Controls.Add(this.flat_RadioButton_33);
            this.panel33.Controls.Add(this.crest_RadioButton_33);
            this.panel33.Location = new System.Drawing.Point(824, 557);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(127, 25);
            this.panel33.TabIndex = 295;
            // 
            // trough_RadioButton_33
            // 
            this.trough_RadioButton_33.AutoSize = true;
            this.trough_RadioButton_33.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_33.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_33.Name = "trough_RadioButton_33";
            this.trough_RadioButton_33.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_33.TabIndex = 266;
            this.trough_RadioButton_33.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_33
            // 
            this.flat_RadioButton_33.AutoSize = true;
            this.flat_RadioButton_33.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_33.Checked = true;
            this.flat_RadioButton_33.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_33.Name = "flat_RadioButton_33";
            this.flat_RadioButton_33.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_33.TabIndex = 265;
            this.flat_RadioButton_33.TabStop = true;
            this.flat_RadioButton_33.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_33
            // 
            this.crest_RadioButton_33.AutoSize = true;
            this.crest_RadioButton_33.BackColor = System.Drawing.SystemColors.Control;
            this.crest_RadioButton_33.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_33.Name = "crest_RadioButton_33";
            this.crest_RadioButton_33.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_33.TabIndex = 264;
            this.crest_RadioButton_33.UseVisualStyleBackColor = false;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.trough_RadioButton_32);
            this.panel32.Controls.Add(this.flat_RadioButton_32);
            this.panel32.Controls.Add(this.crest_RadioButton_32);
            this.panel32.Location = new System.Drawing.Point(824, 527);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(127, 25);
            this.panel32.TabIndex = 294;
            // 
            // trough_RadioButton_32
            // 
            this.trough_RadioButton_32.AutoSize = true;
            this.trough_RadioButton_32.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_32.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_32.Name = "trough_RadioButton_32";
            this.trough_RadioButton_32.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_32.TabIndex = 266;
            this.trough_RadioButton_32.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_32
            // 
            this.flat_RadioButton_32.AutoSize = true;
            this.flat_RadioButton_32.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_32.Name = "flat_RadioButton_32";
            this.flat_RadioButton_32.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_32.TabIndex = 265;
            this.flat_RadioButton_32.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_32
            // 
            this.crest_RadioButton_32.AutoSize = true;
            this.crest_RadioButton_32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_32.Checked = true;
            this.crest_RadioButton_32.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_32.Name = "crest_RadioButton_32";
            this.crest_RadioButton_32.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_32.TabIndex = 264;
            this.crest_RadioButton_32.TabStop = true;
            this.crest_RadioButton_32.UseVisualStyleBackColor = false;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.trough_RadioButton_31);
            this.panel31.Controls.Add(this.flat_RadioButton_31);
            this.panel31.Controls.Add(this.crest_RadioButton_31);
            this.panel31.Location = new System.Drawing.Point(824, 497);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(127, 25);
            this.panel31.TabIndex = 293;
            // 
            // trough_RadioButton_31
            // 
            this.trough_RadioButton_31.AutoSize = true;
            this.trough_RadioButton_31.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_31.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_31.Name = "trough_RadioButton_31";
            this.trough_RadioButton_31.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_31.TabIndex = 266;
            this.trough_RadioButton_31.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_31
            // 
            this.flat_RadioButton_31.AutoSize = true;
            this.flat_RadioButton_31.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_31.Name = "flat_RadioButton_31";
            this.flat_RadioButton_31.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_31.TabIndex = 265;
            this.flat_RadioButton_31.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_31
            // 
            this.crest_RadioButton_31.AutoSize = true;
            this.crest_RadioButton_31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_31.Checked = true;
            this.crest_RadioButton_31.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_31.Name = "crest_RadioButton_31";
            this.crest_RadioButton_31.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_31.TabIndex = 264;
            this.crest_RadioButton_31.TabStop = true;
            this.crest_RadioButton_31.UseVisualStyleBackColor = false;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.trough_RadioButton_30);
            this.panel30.Controls.Add(this.flat_RadioButton_30);
            this.panel30.Controls.Add(this.crest_RadioButton_30);
            this.panel30.Location = new System.Drawing.Point(824, 467);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(127, 25);
            this.panel30.TabIndex = 292;
            // 
            // trough_RadioButton_30
            // 
            this.trough_RadioButton_30.AutoSize = true;
            this.trough_RadioButton_30.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_30.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_30.Name = "trough_RadioButton_30";
            this.trough_RadioButton_30.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_30.TabIndex = 266;
            this.trough_RadioButton_30.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_30
            // 
            this.flat_RadioButton_30.AutoSize = true;
            this.flat_RadioButton_30.BackColor = System.Drawing.SystemColors.Control;
            this.flat_RadioButton_30.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_30.Name = "flat_RadioButton_30";
            this.flat_RadioButton_30.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_30.TabIndex = 265;
            this.flat_RadioButton_30.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_30
            // 
            this.crest_RadioButton_30.AutoSize = true;
            this.crest_RadioButton_30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_30.Checked = true;
            this.crest_RadioButton_30.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_30.Name = "crest_RadioButton_30";
            this.crest_RadioButton_30.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_30.TabIndex = 264;
            this.crest_RadioButton_30.TabStop = true;
            this.crest_RadioButton_30.UseVisualStyleBackColor = false;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.trough_RadioButton_29);
            this.panel29.Controls.Add(this.flat_RadioButton_29);
            this.panel29.Controls.Add(this.crest_RadioButton_29);
            this.panel29.Location = new System.Drawing.Point(824, 437);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(127, 25);
            this.panel29.TabIndex = 291;
            // 
            // trough_RadioButton_29
            // 
            this.trough_RadioButton_29.AutoSize = true;
            this.trough_RadioButton_29.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_29.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_29.Name = "trough_RadioButton_29";
            this.trough_RadioButton_29.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_29.TabIndex = 266;
            this.trough_RadioButton_29.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_29
            // 
            this.flat_RadioButton_29.AutoSize = true;
            this.flat_RadioButton_29.BackColor = System.Drawing.SystemColors.Control;
            this.flat_RadioButton_29.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_29.Name = "flat_RadioButton_29";
            this.flat_RadioButton_29.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_29.TabIndex = 265;
            this.flat_RadioButton_29.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_29
            // 
            this.crest_RadioButton_29.AutoSize = true;
            this.crest_RadioButton_29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_29.Checked = true;
            this.crest_RadioButton_29.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_29.Name = "crest_RadioButton_29";
            this.crest_RadioButton_29.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_29.TabIndex = 264;
            this.crest_RadioButton_29.TabStop = true;
            this.crest_RadioButton_29.UseVisualStyleBackColor = false;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.trough_RadioButton_28);
            this.panel28.Controls.Add(this.flat_RadioButton_28);
            this.panel28.Controls.Add(this.crest_RadioButton_28);
            this.panel28.Location = new System.Drawing.Point(824, 407);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(127, 25);
            this.panel28.TabIndex = 290;
            // 
            // trough_RadioButton_28
            // 
            this.trough_RadioButton_28.AutoSize = true;
            this.trough_RadioButton_28.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_28.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_28.Name = "trough_RadioButton_28";
            this.trough_RadioButton_28.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_28.TabIndex = 266;
            this.trough_RadioButton_28.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_28
            // 
            this.flat_RadioButton_28.AutoSize = true;
            this.flat_RadioButton_28.BackColor = System.Drawing.SystemColors.Control;
            this.flat_RadioButton_28.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_28.Name = "flat_RadioButton_28";
            this.flat_RadioButton_28.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_28.TabIndex = 265;
            this.flat_RadioButton_28.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_28
            // 
            this.crest_RadioButton_28.AutoSize = true;
            this.crest_RadioButton_28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_28.Checked = true;
            this.crest_RadioButton_28.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_28.Name = "crest_RadioButton_28";
            this.crest_RadioButton_28.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_28.TabIndex = 264;
            this.crest_RadioButton_28.TabStop = true;
            this.crest_RadioButton_28.UseVisualStyleBackColor = false;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.trough_RadioButton_27);
            this.panel27.Controls.Add(this.flat_RadioButton_27);
            this.panel27.Controls.Add(this.crest_RadioButton_27);
            this.panel27.Location = new System.Drawing.Point(824, 377);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(127, 25);
            this.panel27.TabIndex = 289;
            // 
            // trough_RadioButton_27
            // 
            this.trough_RadioButton_27.AutoSize = true;
            this.trough_RadioButton_27.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_27.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_27.Name = "trough_RadioButton_27";
            this.trough_RadioButton_27.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_27.TabIndex = 266;
            this.trough_RadioButton_27.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_27
            // 
            this.flat_RadioButton_27.AutoSize = true;
            this.flat_RadioButton_27.BackColor = System.Drawing.SystemColors.Control;
            this.flat_RadioButton_27.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_27.Name = "flat_RadioButton_27";
            this.flat_RadioButton_27.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_27.TabIndex = 265;
            this.flat_RadioButton_27.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_27
            // 
            this.crest_RadioButton_27.AutoSize = true;
            this.crest_RadioButton_27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_27.Checked = true;
            this.crest_RadioButton_27.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_27.Name = "crest_RadioButton_27";
            this.crest_RadioButton_27.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_27.TabIndex = 264;
            this.crest_RadioButton_27.TabStop = true;
            this.crest_RadioButton_27.UseVisualStyleBackColor = false;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.trough_RadioButton_26);
            this.panel26.Controls.Add(this.flat_RadioButton_26);
            this.panel26.Controls.Add(this.crest_RadioButton_26);
            this.panel26.Location = new System.Drawing.Point(824, 347);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(127, 25);
            this.panel26.TabIndex = 288;
            // 
            // trough_RadioButton_26
            // 
            this.trough_RadioButton_26.AutoSize = true;
            this.trough_RadioButton_26.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_26.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_26.Name = "trough_RadioButton_26";
            this.trough_RadioButton_26.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_26.TabIndex = 266;
            this.trough_RadioButton_26.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_26
            // 
            this.flat_RadioButton_26.AutoSize = true;
            this.flat_RadioButton_26.BackColor = System.Drawing.SystemColors.Control;
            this.flat_RadioButton_26.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_26.Name = "flat_RadioButton_26";
            this.flat_RadioButton_26.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_26.TabIndex = 265;
            this.flat_RadioButton_26.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_26
            // 
            this.crest_RadioButton_26.AutoSize = true;
            this.crest_RadioButton_26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_26.Checked = true;
            this.crest_RadioButton_26.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_26.Name = "crest_RadioButton_26";
            this.crest_RadioButton_26.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_26.TabIndex = 264;
            this.crest_RadioButton_26.TabStop = true;
            this.crest_RadioButton_26.UseVisualStyleBackColor = false;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.trough_RadioButton_25);
            this.panel25.Controls.Add(this.flat_RadioButton_25);
            this.panel25.Controls.Add(this.crest_RadioButton_25);
            this.panel25.Location = new System.Drawing.Point(824, 317);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(127, 25);
            this.panel25.TabIndex = 287;
            // 
            // trough_RadioButton_25
            // 
            this.trough_RadioButton_25.AutoSize = true;
            this.trough_RadioButton_25.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_25.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_25.Name = "trough_RadioButton_25";
            this.trough_RadioButton_25.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_25.TabIndex = 266;
            this.trough_RadioButton_25.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_25
            // 
            this.flat_RadioButton_25.AutoSize = true;
            this.flat_RadioButton_25.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_25.Name = "flat_RadioButton_25";
            this.flat_RadioButton_25.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_25.TabIndex = 265;
            this.flat_RadioButton_25.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_25
            // 
            this.crest_RadioButton_25.AutoSize = true;
            this.crest_RadioButton_25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.crest_RadioButton_25.Checked = true;
            this.crest_RadioButton_25.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_25.Name = "crest_RadioButton_25";
            this.crest_RadioButton_25.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_25.TabIndex = 264;
            this.crest_RadioButton_25.TabStop = true;
            this.crest_RadioButton_25.UseVisualStyleBackColor = false;
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.trough_RadioButton_48);
            this.panel48.Controls.Add(this.flat_RadioButton_48);
            this.panel48.Controls.Add(this.crest_RadioButton_48);
            this.panel48.Location = new System.Drawing.Point(1143, 647);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(127, 25);
            this.panel48.TabIndex = 310;
            // 
            // trough_RadioButton_48
            // 
            this.trough_RadioButton_48.AutoSize = true;
            this.trough_RadioButton_48.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_48.Checked = true;
            this.trough_RadioButton_48.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_48.Name = "trough_RadioButton_48";
            this.trough_RadioButton_48.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_48.TabIndex = 266;
            this.trough_RadioButton_48.TabStop = true;
            this.trough_RadioButton_48.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_48
            // 
            this.flat_RadioButton_48.AutoSize = true;
            this.flat_RadioButton_48.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_48.Name = "flat_RadioButton_48";
            this.flat_RadioButton_48.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_48.TabIndex = 265;
            this.flat_RadioButton_48.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_48
            // 
            this.crest_RadioButton_48.AutoSize = true;
            this.crest_RadioButton_48.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_48.Name = "crest_RadioButton_48";
            this.crest_RadioButton_48.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_48.TabIndex = 264;
            this.crest_RadioButton_48.UseVisualStyleBackColor = true;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.trough_RadioButton_47);
            this.panel47.Controls.Add(this.flat_RadioButton_47);
            this.panel47.Controls.Add(this.crest_RadioButton_47);
            this.panel47.Location = new System.Drawing.Point(1143, 617);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(127, 25);
            this.panel47.TabIndex = 309;
            // 
            // trough_RadioButton_47
            // 
            this.trough_RadioButton_47.AutoSize = true;
            this.trough_RadioButton_47.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_47.Checked = true;
            this.trough_RadioButton_47.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_47.Name = "trough_RadioButton_47";
            this.trough_RadioButton_47.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_47.TabIndex = 266;
            this.trough_RadioButton_47.TabStop = true;
            this.trough_RadioButton_47.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_47
            // 
            this.flat_RadioButton_47.AutoSize = true;
            this.flat_RadioButton_47.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_47.Name = "flat_RadioButton_47";
            this.flat_RadioButton_47.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_47.TabIndex = 265;
            this.flat_RadioButton_47.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_47
            // 
            this.crest_RadioButton_47.AutoSize = true;
            this.crest_RadioButton_47.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_47.Name = "crest_RadioButton_47";
            this.crest_RadioButton_47.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_47.TabIndex = 264;
            this.crest_RadioButton_47.UseVisualStyleBackColor = true;
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.trough_RadioButton_46);
            this.panel46.Controls.Add(this.flat_RadioButton_46);
            this.panel46.Controls.Add(this.crest_RadioButton_46);
            this.panel46.Location = new System.Drawing.Point(1143, 587);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(127, 25);
            this.panel46.TabIndex = 308;
            // 
            // trough_RadioButton_46
            // 
            this.trough_RadioButton_46.AutoSize = true;
            this.trough_RadioButton_46.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_46.Checked = true;
            this.trough_RadioButton_46.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_46.Name = "trough_RadioButton_46";
            this.trough_RadioButton_46.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_46.TabIndex = 266;
            this.trough_RadioButton_46.TabStop = true;
            this.trough_RadioButton_46.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_46
            // 
            this.flat_RadioButton_46.AutoSize = true;
            this.flat_RadioButton_46.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_46.Name = "flat_RadioButton_46";
            this.flat_RadioButton_46.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_46.TabIndex = 265;
            this.flat_RadioButton_46.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_46
            // 
            this.crest_RadioButton_46.AutoSize = true;
            this.crest_RadioButton_46.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_46.Name = "crest_RadioButton_46";
            this.crest_RadioButton_46.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_46.TabIndex = 264;
            this.crest_RadioButton_46.UseVisualStyleBackColor = true;
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.trough_RadioButton_45);
            this.panel45.Controls.Add(this.flat_RadioButton_45);
            this.panel45.Controls.Add(this.crest_RadioButton_45);
            this.panel45.Location = new System.Drawing.Point(1143, 557);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(127, 25);
            this.panel45.TabIndex = 307;
            // 
            // trough_RadioButton_45
            // 
            this.trough_RadioButton_45.AutoSize = true;
            this.trough_RadioButton_45.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_45.Checked = true;
            this.trough_RadioButton_45.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_45.Name = "trough_RadioButton_45";
            this.trough_RadioButton_45.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_45.TabIndex = 266;
            this.trough_RadioButton_45.TabStop = true;
            this.trough_RadioButton_45.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_45
            // 
            this.flat_RadioButton_45.AutoSize = true;
            this.flat_RadioButton_45.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_45.Name = "flat_RadioButton_45";
            this.flat_RadioButton_45.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_45.TabIndex = 265;
            this.flat_RadioButton_45.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_45
            // 
            this.crest_RadioButton_45.AutoSize = true;
            this.crest_RadioButton_45.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_45.Name = "crest_RadioButton_45";
            this.crest_RadioButton_45.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_45.TabIndex = 264;
            this.crest_RadioButton_45.UseVisualStyleBackColor = true;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.trough_RadioButton_44);
            this.panel44.Controls.Add(this.flat_RadioButton_44);
            this.panel44.Controls.Add(this.crest_RadioButton_44);
            this.panel44.Location = new System.Drawing.Point(1143, 527);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(127, 25);
            this.panel44.TabIndex = 306;
            // 
            // trough_RadioButton_44
            // 
            this.trough_RadioButton_44.AutoSize = true;
            this.trough_RadioButton_44.BackColor = System.Drawing.Color.Green;
            this.trough_RadioButton_44.Checked = true;
            this.trough_RadioButton_44.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_44.Name = "trough_RadioButton_44";
            this.trough_RadioButton_44.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_44.TabIndex = 266;
            this.trough_RadioButton_44.TabStop = true;
            this.trough_RadioButton_44.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_44
            // 
            this.flat_RadioButton_44.AutoSize = true;
            this.flat_RadioButton_44.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_44.Name = "flat_RadioButton_44";
            this.flat_RadioButton_44.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_44.TabIndex = 265;
            this.flat_RadioButton_44.UseVisualStyleBackColor = true;
            // 
            // crest_RadioButton_44
            // 
            this.crest_RadioButton_44.AutoSize = true;
            this.crest_RadioButton_44.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_44.Name = "crest_RadioButton_44";
            this.crest_RadioButton_44.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_44.TabIndex = 264;
            this.crest_RadioButton_44.UseVisualStyleBackColor = true;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.trough_RadioButton_43);
            this.panel43.Controls.Add(this.flat_RadioButton_43);
            this.panel43.Controls.Add(this.crest_RadioButton_43);
            this.panel43.Location = new System.Drawing.Point(1143, 497);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(127, 25);
            this.panel43.TabIndex = 305;
            // 
            // trough_RadioButton_43
            // 
            this.trough_RadioButton_43.AutoSize = true;
            this.trough_RadioButton_43.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_43.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_43.Name = "trough_RadioButton_43";
            this.trough_RadioButton_43.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_43.TabIndex = 266;
            this.trough_RadioButton_43.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_43
            // 
            this.flat_RadioButton_43.AutoSize = true;
            this.flat_RadioButton_43.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_43.Checked = true;
            this.flat_RadioButton_43.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_43.Name = "flat_RadioButton_43";
            this.flat_RadioButton_43.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_43.TabIndex = 265;
            this.flat_RadioButton_43.TabStop = true;
            this.flat_RadioButton_43.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_43
            // 
            this.crest_RadioButton_43.AutoSize = true;
            this.crest_RadioButton_43.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_43.Name = "crest_RadioButton_43";
            this.crest_RadioButton_43.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_43.TabIndex = 264;
            this.crest_RadioButton_43.UseVisualStyleBackColor = true;
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.trough_RadioButton_42);
            this.panel42.Controls.Add(this.flat_RadioButton_42);
            this.panel42.Controls.Add(this.crest_RadioButton_42);
            this.panel42.Location = new System.Drawing.Point(1143, 467);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(127, 25);
            this.panel42.TabIndex = 304;
            // 
            // trough_RadioButton_42
            // 
            this.trough_RadioButton_42.AutoSize = true;
            this.trough_RadioButton_42.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_42.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_42.Name = "trough_RadioButton_42";
            this.trough_RadioButton_42.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_42.TabIndex = 266;
            this.trough_RadioButton_42.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_42
            // 
            this.flat_RadioButton_42.AutoSize = true;
            this.flat_RadioButton_42.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_42.Checked = true;
            this.flat_RadioButton_42.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_42.Name = "flat_RadioButton_42";
            this.flat_RadioButton_42.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_42.TabIndex = 265;
            this.flat_RadioButton_42.TabStop = true;
            this.flat_RadioButton_42.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_42
            // 
            this.crest_RadioButton_42.AutoSize = true;
            this.crest_RadioButton_42.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_42.Name = "crest_RadioButton_42";
            this.crest_RadioButton_42.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_42.TabIndex = 264;
            this.crest_RadioButton_42.UseVisualStyleBackColor = true;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.trough_RadioButton_41);
            this.panel41.Controls.Add(this.flat_RadioButton_41);
            this.panel41.Controls.Add(this.crest_RadioButton_41);
            this.panel41.Location = new System.Drawing.Point(1143, 437);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(127, 25);
            this.panel41.TabIndex = 303;
            // 
            // trough_RadioButton_41
            // 
            this.trough_RadioButton_41.AutoSize = true;
            this.trough_RadioButton_41.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_41.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_41.Name = "trough_RadioButton_41";
            this.trough_RadioButton_41.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_41.TabIndex = 266;
            this.trough_RadioButton_41.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_41
            // 
            this.flat_RadioButton_41.AutoSize = true;
            this.flat_RadioButton_41.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_41.Checked = true;
            this.flat_RadioButton_41.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_41.Name = "flat_RadioButton_41";
            this.flat_RadioButton_41.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_41.TabIndex = 265;
            this.flat_RadioButton_41.TabStop = true;
            this.flat_RadioButton_41.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_41
            // 
            this.crest_RadioButton_41.AutoSize = true;
            this.crest_RadioButton_41.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_41.Name = "crest_RadioButton_41";
            this.crest_RadioButton_41.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_41.TabIndex = 264;
            this.crest_RadioButton_41.UseVisualStyleBackColor = true;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.trough_RadioButton_40);
            this.panel40.Controls.Add(this.flat_RadioButton_40);
            this.panel40.Controls.Add(this.crest_RadioButton_40);
            this.panel40.Location = new System.Drawing.Point(1143, 407);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(127, 25);
            this.panel40.TabIndex = 302;
            // 
            // trough_RadioButton_40
            // 
            this.trough_RadioButton_40.AutoSize = true;
            this.trough_RadioButton_40.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_40.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_40.Name = "trough_RadioButton_40";
            this.trough_RadioButton_40.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_40.TabIndex = 266;
            this.trough_RadioButton_40.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_40
            // 
            this.flat_RadioButton_40.AutoSize = true;
            this.flat_RadioButton_40.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_40.Checked = true;
            this.flat_RadioButton_40.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_40.Name = "flat_RadioButton_40";
            this.flat_RadioButton_40.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_40.TabIndex = 265;
            this.flat_RadioButton_40.TabStop = true;
            this.flat_RadioButton_40.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_40
            // 
            this.crest_RadioButton_40.AutoSize = true;
            this.crest_RadioButton_40.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_40.Name = "crest_RadioButton_40";
            this.crest_RadioButton_40.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_40.TabIndex = 264;
            this.crest_RadioButton_40.UseVisualStyleBackColor = true;
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.trough_RadioButton_39);
            this.panel39.Controls.Add(this.flat_RadioButton_39);
            this.panel39.Controls.Add(this.crest_RadioButton_39);
            this.panel39.Location = new System.Drawing.Point(1143, 377);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(127, 25);
            this.panel39.TabIndex = 301;
            // 
            // trough_RadioButton_39
            // 
            this.trough_RadioButton_39.AutoSize = true;
            this.trough_RadioButton_39.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_39.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_39.Name = "trough_RadioButton_39";
            this.trough_RadioButton_39.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_39.TabIndex = 266;
            this.trough_RadioButton_39.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_39
            // 
            this.flat_RadioButton_39.AutoSize = true;
            this.flat_RadioButton_39.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_39.Checked = true;
            this.flat_RadioButton_39.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_39.Name = "flat_RadioButton_39";
            this.flat_RadioButton_39.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_39.TabIndex = 265;
            this.flat_RadioButton_39.TabStop = true;
            this.flat_RadioButton_39.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_39
            // 
            this.crest_RadioButton_39.AutoSize = true;
            this.crest_RadioButton_39.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_39.Name = "crest_RadioButton_39";
            this.crest_RadioButton_39.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_39.TabIndex = 264;
            this.crest_RadioButton_39.UseVisualStyleBackColor = true;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.trough_RadioButton_38);
            this.panel38.Controls.Add(this.flat_RadioButton_38);
            this.panel38.Controls.Add(this.crest_RadioButton_38);
            this.panel38.Location = new System.Drawing.Point(1143, 347);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(127, 25);
            this.panel38.TabIndex = 300;
            // 
            // trough_RadioButton_38
            // 
            this.trough_RadioButton_38.AutoSize = true;
            this.trough_RadioButton_38.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_38.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_38.Name = "trough_RadioButton_38";
            this.trough_RadioButton_38.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_38.TabIndex = 266;
            this.trough_RadioButton_38.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_38
            // 
            this.flat_RadioButton_38.AutoSize = true;
            this.flat_RadioButton_38.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_38.Checked = true;
            this.flat_RadioButton_38.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_38.Name = "flat_RadioButton_38";
            this.flat_RadioButton_38.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_38.TabIndex = 265;
            this.flat_RadioButton_38.TabStop = true;
            this.flat_RadioButton_38.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_38
            // 
            this.crest_RadioButton_38.AutoSize = true;
            this.crest_RadioButton_38.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_38.Name = "crest_RadioButton_38";
            this.crest_RadioButton_38.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_38.TabIndex = 264;
            this.crest_RadioButton_38.UseVisualStyleBackColor = true;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.trough_RadioButton_37);
            this.panel37.Controls.Add(this.flat_RadioButton_37);
            this.panel37.Controls.Add(this.crest_RadioButton_37);
            this.panel37.Location = new System.Drawing.Point(1143, 317);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(127, 25);
            this.panel37.TabIndex = 299;
            // 
            // trough_RadioButton_37
            // 
            this.trough_RadioButton_37.AutoSize = true;
            this.trough_RadioButton_37.BackColor = System.Drawing.SystemColors.Control;
            this.trough_RadioButton_37.Location = new System.Drawing.Point(102, 3);
            this.trough_RadioButton_37.Name = "trough_RadioButton_37";
            this.trough_RadioButton_37.Size = new System.Drawing.Size(17, 16);
            this.trough_RadioButton_37.TabIndex = 266;
            this.trough_RadioButton_37.UseVisualStyleBackColor = false;
            // 
            // flat_RadioButton_37
            // 
            this.flat_RadioButton_37.AutoSize = true;
            this.flat_RadioButton_37.BackColor = System.Drawing.Color.DarkKhaki;
            this.flat_RadioButton_37.Checked = true;
            this.flat_RadioButton_37.Location = new System.Drawing.Point(52, 3);
            this.flat_RadioButton_37.Name = "flat_RadioButton_37";
            this.flat_RadioButton_37.Size = new System.Drawing.Size(17, 16);
            this.flat_RadioButton_37.TabIndex = 265;
            this.flat_RadioButton_37.TabStop = true;
            this.flat_RadioButton_37.UseVisualStyleBackColor = false;
            // 
            // crest_RadioButton_37
            // 
            this.crest_RadioButton_37.AutoSize = true;
            this.crest_RadioButton_37.Location = new System.Drawing.Point(3, 3);
            this.crest_RadioButton_37.Name = "crest_RadioButton_37";
            this.crest_RadioButton_37.Size = new System.Drawing.Size(17, 16);
            this.crest_RadioButton_37.TabIndex = 264;
            this.crest_RadioButton_37.UseVisualStyleBackColor = true;
            // 
            // eLECTRICITY_PRICETableAdapter
            // 
            this.eLECTRICITY_PRICETableAdapter.ClearBeforeFill = true;
            // 
            // Form_Basic_Electricity_Price_Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1617, 856);
            this.Controls.Add(this.panel37);
            this.Controls.Add(this.e_Price_Choose_Button);
            this.Controls.Add(this.panel38);
            this.Controls.Add(this.panel39);
            this.Controls.Add(this.panel40);
            this.Controls.Add(this.panel41);
            this.Controls.Add(this.panel42);
            this.Controls.Add(this.panel43);
            this.Controls.Add(this.panel44);
            this.Controls.Add(this.panel45);
            this.Controls.Add(this.panel46);
            this.Controls.Add(this.panel47);
            this.Controls.Add(this.panel48);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel27);
            this.Controls.Add(this.panel28);
            this.Controls.Add(this.panel29);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel31);
            this.Controls.Add(this.panel32);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel34);
            this.Controls.Add(this.panel35);
            this.Controls.Add(this.panel36);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.M12_CheckBox);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.M11_CheckBox);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.M10_CheckBox);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.M9_CheckBox);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.M8_CheckBox);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.M7_CheckBox);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.M6_CheckBox);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.M5_CheckBox);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.M4_CheckBox);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.M3_CheckBox);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.M2_CheckBox);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.M1_CheckBox);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.e_Price_Ok_Button);
            this.Controls.Add(this.e_Price_Delete_Button);
            this.Controls.Add(this.e_Price_Edit_Button);
            this.Controls.Add(this.e_Price_New_Button);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.three_Price_GroupBox);
            this.Controls.Add(this.two_Price_GroupBox);
            this.Controls.Add(this.exchange_Rate_TW_TextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.currency_System_TextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.e_Price_Name_TextBox);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Basic_Electricity_Price_Management";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "基本電價管理    ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Basic_Electricity_Price_Management_Closed);
            this.Load += new System.EventHandler(this.Form_Basic_Electricity_Price_Management_Load);
            this.two_Price_GroupBox.ResumeLayout(false);
            this.two_Price_GroupBox.PerformLayout();
            this.three_Price_GroupBox.ResumeLayout(false);
            this.three_Price_GroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.eLECTRICITYPRICEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iCE_STORAGE_DataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox e_Price_Name_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox currency_System_TextBox;
        private System.Windows.Forms.TextBox exchange_Rate_TW_TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox two_Price_GroupBox;
        private System.Windows.Forms.GroupBox three_Price_GroupBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox two_Non_Summer_Basic_Price_TextBox;
        private System.Windows.Forms.TextBox two_Summer_Basic_Price_TextBox;
        private System.Windows.Forms.TextBox two_Non_Summer_Normal_TextBox;
        private System.Windows.Forms.TextBox two_Summer_Normal_TextBox;
        private System.Windows.Forms.TextBox two_Non_Summer_Peak_TextBox;
        private System.Windows.Forms.TextBox two_Summer_Peak_TextBox;
        private System.Windows.Forms.TextBox three_Non_Summer_Basic_Price_TextBox;
        private System.Windows.Forms.TextBox three_Summer_Basic_Price_TextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox three_Non_Summer_Trough_TextBox;
        private System.Windows.Forms.TextBox three_Summer_Trough_TextBox;
        private System.Windows.Forms.TextBox three_Non_Summer_Normal_TextBox;
        private System.Windows.Forms.TextBox three_Summer_Normal_TextBox;
        private System.Windows.Forms.TextBox three_Non_Summer_Peak_TextBox;
        private System.Windows.Forms.TextBox three_Summer_Peak_TextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button e_Price_Choose_Button;
        private System.Windows.Forms.Button e_Price_New_Button;
        private System.Windows.Forms.Button e_Price_Edit_Button;
        private System.Windows.Forms.Button e_Price_Delete_Button;
        private System.Windows.Forms.Button e_Price_Ok_Button;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.CheckBox M1_CheckBox;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox M2_CheckBox;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.CheckBox M3_CheckBox;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.CheckBox M4_CheckBox;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.CheckBox M5_CheckBox;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.CheckBox M6_CheckBox;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.CheckBox M7_CheckBox;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.CheckBox M8_CheckBox;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.CheckBox M9_CheckBox;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.CheckBox M10_CheckBox;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.CheckBox M11_CheckBox;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.CheckBox M12_CheckBox;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton trough_RadioButton_1;
        private System.Windows.Forms.RadioButton flat_RadioButton_1;
        private System.Windows.Forms.RadioButton crest_RadioButton_1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton trough_RadioButton_2;
        private System.Windows.Forms.RadioButton flat_RadioButton_2;
        private System.Windows.Forms.RadioButton crest_RadioButton_2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton trough_RadioButton_3;
        private System.Windows.Forms.RadioButton flat_RadioButton_3;
        private System.Windows.Forms.RadioButton crest_RadioButton_3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton trough_RadioButton_4;
        private System.Windows.Forms.RadioButton flat_RadioButton_4;
        private System.Windows.Forms.RadioButton crest_RadioButton_4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton trough_RadioButton_5;
        private System.Windows.Forms.RadioButton flat_RadioButton_5;
        private System.Windows.Forms.RadioButton crest_RadioButton_5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RadioButton trough_RadioButton_10;
        private System.Windows.Forms.RadioButton flat_RadioButton_10;
        private System.Windows.Forms.RadioButton crest_RadioButton_10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton trough_RadioButton_9;
        private System.Windows.Forms.RadioButton flat_RadioButton_9;
        private System.Windows.Forms.RadioButton crest_RadioButton_9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton trough_RadioButton_8;
        private System.Windows.Forms.RadioButton flat_RadioButton_8;
        private System.Windows.Forms.RadioButton crest_RadioButton_8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton trough_RadioButton_7;
        private System.Windows.Forms.RadioButton flat_RadioButton_7;
        private System.Windows.Forms.RadioButton crest_RadioButton_7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton trough_RadioButton_6;
        private System.Windows.Forms.RadioButton flat_RadioButton_6;
        private System.Windows.Forms.RadioButton crest_RadioButton_6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.RadioButton trough_RadioButton_12;
        private System.Windows.Forms.RadioButton flat_RadioButton_12;
        private System.Windows.Forms.RadioButton crest_RadioButton_12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RadioButton trough_RadioButton_11;
        private System.Windows.Forms.RadioButton flat_RadioButton_11;
        private System.Windows.Forms.RadioButton crest_RadioButton_11;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.RadioButton trough_RadioButton_24;
        private System.Windows.Forms.RadioButton flat_RadioButton_24;
        private System.Windows.Forms.RadioButton crest_RadioButton_24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.RadioButton trough_RadioButton_23;
        private System.Windows.Forms.RadioButton flat_RadioButton_23;
        private System.Windows.Forms.RadioButton crest_RadioButton_23;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.RadioButton trough_RadioButton_22;
        private System.Windows.Forms.RadioButton flat_RadioButton_22;
        private System.Windows.Forms.RadioButton crest_RadioButton_22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.RadioButton trough_RadioButton_21;
        private System.Windows.Forms.RadioButton flat_RadioButton_21;
        private System.Windows.Forms.RadioButton crest_RadioButton_21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.RadioButton trough_RadioButton_20;
        private System.Windows.Forms.RadioButton flat_RadioButton_20;
        private System.Windows.Forms.RadioButton crest_RadioButton_20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.RadioButton trough_RadioButton_19;
        private System.Windows.Forms.RadioButton flat_RadioButton_19;
        private System.Windows.Forms.RadioButton crest_RadioButton_19;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.RadioButton trough_RadioButton_18;
        private System.Windows.Forms.RadioButton flat_RadioButton_18;
        private System.Windows.Forms.RadioButton crest_RadioButton_18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RadioButton trough_RadioButton_17;
        private System.Windows.Forms.RadioButton flat_RadioButton_17;
        private System.Windows.Forms.RadioButton crest_RadioButton_17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.RadioButton trough_RadioButton_16;
        private System.Windows.Forms.RadioButton flat_RadioButton_16;
        private System.Windows.Forms.RadioButton crest_RadioButton_16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.RadioButton trough_RadioButton_15;
        private System.Windows.Forms.RadioButton flat_RadioButton_15;
        private System.Windows.Forms.RadioButton crest_RadioButton_15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.RadioButton trough_RadioButton_14;
        private System.Windows.Forms.RadioButton flat_RadioButton_14;
        private System.Windows.Forms.RadioButton crest_RadioButton_14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.RadioButton trough_RadioButton_13;
        private System.Windows.Forms.RadioButton flat_RadioButton_13;
        private System.Windows.Forms.RadioButton crest_RadioButton_13;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.RadioButton trough_RadioButton_36;
        private System.Windows.Forms.RadioButton flat_RadioButton_36;
        private System.Windows.Forms.RadioButton crest_RadioButton_36;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.RadioButton trough_RadioButton_35;
        private System.Windows.Forms.RadioButton flat_RadioButton_35;
        private System.Windows.Forms.RadioButton crest_RadioButton_35;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.RadioButton trough_RadioButton_34;
        private System.Windows.Forms.RadioButton flat_RadioButton_34;
        private System.Windows.Forms.RadioButton crest_RadioButton_34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.RadioButton trough_RadioButton_33;
        private System.Windows.Forms.RadioButton flat_RadioButton_33;
        private System.Windows.Forms.RadioButton crest_RadioButton_33;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.RadioButton trough_RadioButton_32;
        private System.Windows.Forms.RadioButton flat_RadioButton_32;
        private System.Windows.Forms.RadioButton crest_RadioButton_32;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.RadioButton trough_RadioButton_31;
        private System.Windows.Forms.RadioButton flat_RadioButton_31;
        private System.Windows.Forms.RadioButton crest_RadioButton_31;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.RadioButton trough_RadioButton_30;
        private System.Windows.Forms.RadioButton flat_RadioButton_30;
        private System.Windows.Forms.RadioButton crest_RadioButton_30;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.RadioButton trough_RadioButton_29;
        private System.Windows.Forms.RadioButton flat_RadioButton_29;
        private System.Windows.Forms.RadioButton crest_RadioButton_29;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.RadioButton trough_RadioButton_28;
        private System.Windows.Forms.RadioButton flat_RadioButton_28;
        private System.Windows.Forms.RadioButton crest_RadioButton_28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.RadioButton trough_RadioButton_27;
        private System.Windows.Forms.RadioButton flat_RadioButton_27;
        private System.Windows.Forms.RadioButton crest_RadioButton_27;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.RadioButton trough_RadioButton_26;
        private System.Windows.Forms.RadioButton flat_RadioButton_26;
        private System.Windows.Forms.RadioButton crest_RadioButton_26;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.RadioButton trough_RadioButton_25;
        private System.Windows.Forms.RadioButton flat_RadioButton_25;
        private System.Windows.Forms.RadioButton crest_RadioButton_25;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.RadioButton trough_RadioButton_48;
        private System.Windows.Forms.RadioButton flat_RadioButton_48;
        private System.Windows.Forms.RadioButton crest_RadioButton_48;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.RadioButton trough_RadioButton_47;
        private System.Windows.Forms.RadioButton flat_RadioButton_47;
        private System.Windows.Forms.RadioButton crest_RadioButton_47;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.RadioButton trough_RadioButton_46;
        private System.Windows.Forms.RadioButton flat_RadioButton_46;
        private System.Windows.Forms.RadioButton crest_RadioButton_46;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.RadioButton trough_RadioButton_45;
        private System.Windows.Forms.RadioButton flat_RadioButton_45;
        private System.Windows.Forms.RadioButton crest_RadioButton_45;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.RadioButton trough_RadioButton_44;
        private System.Windows.Forms.RadioButton flat_RadioButton_44;
        private System.Windows.Forms.RadioButton crest_RadioButton_44;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.RadioButton trough_RadioButton_43;
        private System.Windows.Forms.RadioButton flat_RadioButton_43;
        private System.Windows.Forms.RadioButton crest_RadioButton_43;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.RadioButton trough_RadioButton_42;
        private System.Windows.Forms.RadioButton flat_RadioButton_42;
        private System.Windows.Forms.RadioButton crest_RadioButton_42;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.RadioButton trough_RadioButton_41;
        private System.Windows.Forms.RadioButton flat_RadioButton_41;
        private System.Windows.Forms.RadioButton crest_RadioButton_41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.RadioButton trough_RadioButton_40;
        private System.Windows.Forms.RadioButton flat_RadioButton_40;
        private System.Windows.Forms.RadioButton crest_RadioButton_40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.RadioButton trough_RadioButton_39;
        private System.Windows.Forms.RadioButton flat_RadioButton_39;
        private System.Windows.Forms.RadioButton crest_RadioButton_39;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.RadioButton trough_RadioButton_38;
        private System.Windows.Forms.RadioButton flat_RadioButton_38;
        private System.Windows.Forms.RadioButton crest_RadioButton_38;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.RadioButton trough_RadioButton_37;
        private System.Windows.Forms.RadioButton flat_RadioButton_37;
        private System.Windows.Forms.RadioButton crest_RadioButton_37;
        private ICE_STORAGE_DataSet iCE_STORAGE_DataSet;
        private System.Windows.Forms.BindingSource eLECTRICITYPRICEBindingSource;
        private ICE_STORAGE_DataSetTableAdapters.ELECTRICITY_PRICETableAdapter eLECTRICITY_PRICETableAdapter;
        private System.Windows.Forms.ComboBox e_Price_Name_comboBox;
    }
}