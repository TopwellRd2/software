﻿namespace Ice_storage_evaluation
{
    partial class Form_Horsepower_Data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.full_Horsepower_Set_TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.full_horsepower_Label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.part_Horsepower_Set_TextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.part_horsepower_Label = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.horsepower_Data_Ok_Button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.full_Horsepower_Set_TextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.full_horsepower_Label);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(712, 176);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "全量儲冰";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(514, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 31);
            this.label5.TabIndex = 5;
            this.label5.Text = "Hp";
            // 
            // full_Horsepower_Set_TextBox
            // 
            this.full_Horsepower_Set_TextBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold);
            this.full_Horsepower_Set_TextBox.Location = new System.Drawing.Point(357, 109);
            this.full_Horsepower_Set_TextBox.Name = "full_Horsepower_Set_TextBox";
            this.full_Horsepower_Set_TextBox.Size = new System.Drawing.Size(151, 41);
            this.full_Horsepower_Set_TextBox.TabIndex = 4;
            this.full_Horsepower_Set_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(514, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 31);
            this.label4.TabIndex = 3;
            this.label4.Text = "Hp";
            // 
            // full_horsepower_Label
            // 
            this.full_horsepower_Label.AutoSize = true;
            this.full_horsepower_Label.Font = new System.Drawing.Font("微軟正黑體", 22.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.full_horsepower_Label.Location = new System.Drawing.Point(382, 39);
            this.full_horsepower_Label.Name = "full_horsepower_Label";
            this.full_horsepower_Label.Size = new System.Drawing.Size(108, 48);
            this.full_horsepower_Label.TabIndex = 2;
            this.full_horsepower_Label.Text = "2426";
            this.full_horsepower_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(118, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "設定馬力數";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(118, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "適合馬力數";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.part_Horsepower_Set_TextBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.part_horsepower_Label);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.Location = new System.Drawing.Point(12, 204);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(712, 176);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "分量儲冰";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(514, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 31);
            this.label6.TabIndex = 11;
            this.label6.Text = "Hp";
            // 
            // part_Horsepower_Set_TextBox
            // 
            this.part_Horsepower_Set_TextBox.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold);
            this.part_Horsepower_Set_TextBox.Location = new System.Drawing.Point(357, 113);
            this.part_Horsepower_Set_TextBox.Name = "part_Horsepower_Set_TextBox";
            this.part_Horsepower_Set_TextBox.Size = new System.Drawing.Size(151, 41);
            this.part_Horsepower_Set_TextBox.TabIndex = 10;
            this.part_Horsepower_Set_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(514, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 31);
            this.label7.TabIndex = 9;
            this.label7.Text = "Hp";
            // 
            // part_horsepower_Label
            // 
            this.part_horsepower_Label.AutoSize = true;
            this.part_horsepower_Label.Font = new System.Drawing.Font("微軟正黑體", 22.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.part_horsepower_Label.Location = new System.Drawing.Point(382, 43);
            this.part_horsepower_Label.Name = "part_horsepower_Label";
            this.part_horsepower_Label.Size = new System.Drawing.Size(86, 48);
            this.part_horsepower_Label.TabIndex = 8;
            this.part_horsepower_Label.Text = "865";
            this.part_horsepower_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(118, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 31);
            this.label9.TabIndex = 7;
            this.label9.Text = "設定馬力數";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(118, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 31);
            this.label10.TabIndex = 6;
            this.label10.Text = "適合馬力數";
            // 
            // horsepower_Data_Ok_Button
            // 
            this.horsepower_Data_Ok_Button.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.horsepower_Data_Ok_Button.Location = new System.Drawing.Point(531, 398);
            this.horsepower_Data_Ok_Button.Name = "horsepower_Data_Ok_Button";
            this.horsepower_Data_Ok_Button.Size = new System.Drawing.Size(193, 50);
            this.horsepower_Data_Ok_Button.TabIndex = 2;
            this.horsepower_Data_Ok_Button.Text = "確定";
            this.horsepower_Data_Ok_Button.UseVisualStyleBackColor = true;
            this.horsepower_Data_Ok_Button.Click += new System.EventHandler(this.horsepower_Data_Ok_Button_Click);
            // 
            // Form_Horsepower_Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(736, 460);
            this.Controls.Add(this.horsepower_Data_Ok_Button);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Horsepower_Data";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "馬力數據";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Horsepower_Data_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Horsepower_Data_Closed);
            this.Load += new System.EventHandler(this.Form_Horsepower_Data_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button horsepower_Data_Ok_Button;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox full_Horsepower_Set_TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label full_horsepower_Label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox part_Horsepower_Set_TextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label part_horsepower_Label;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}