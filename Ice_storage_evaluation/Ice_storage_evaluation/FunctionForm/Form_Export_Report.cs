﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Limilabs.FTP.Client;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form_Export_Report : Form
    {
        private Form1 form1;
        SqlConnection sqlConnection;

        public Form_Export_Report(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            string connectionString = form1.GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
        }

        private void Form_Export_Report_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (form1.exportReport_CheckBox.Checked)
            {
                form1.exportReport_CheckBox.Checked = false;
            }
        }

        private void save_Button_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }
            String selectedPath = folderBrowserDialog.SelectedPath;
            Ftp ftpClient = new Ftp();
            try
            {
                ftpClient.Connect(Properties.Settings.Default.ICE_STORAGE_FTP_ADDR);
                ftpClient.Login(Properties.Settings.Default.ICE_STORAGE_FTP_USER, Properties.Settings.Default.ICE_STORAGE_FTP_PASSWORD);
            }
            catch(FtpResponseException FTP_Ex)
            {
                MessageBox.Show("FTP連線異常,請確認連線是否正常!" + FTP_Ex, "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (ftpClient.Connected)
            {
                //下載報表範本
                ftpClient.Download("/Ice-Storage-FTP/ReportSample.xls", selectedPath + "/" + file_Name_TextBox.Text + ".xls");

                //開啟excel、修改報表內容
                Excel.Application excelApp = new Excel.Application();  //應用程式
                Excel.Workbook excelFormat = excelApp.Workbooks.Open(selectedPath + "/" + file_Name_TextBox.Text + ".xls");   //開啟此路徑檔案
                writeWorksheet1(excelFormat);  // 經濟效益評估報告             worksheet 1
                writeWorksheet2(excelFormat);  // 傳統空調負荷分佈             worksheet 2
                writeWorksheet3(excelFormat);  // 全量儲冰空調負荷分佈         worksheet 3
                writeWorksheet4(excelFormat);  // 分量儲冰空調負荷分佈         worksheet 4 
                writeWorksheet5(excelFormat);  // 傳統空調電力負載分佈         worksheet 5 
                writeWorksheet6(excelFormat);  // 全量儲冰空調電力負載分佈     worksheet 6
                writeWorksheet7(excelFormat);  // 分量儲冰空調電力負載分佈     worksheet 7 
                writeWorksheet8(excelFormat);  // 傳統空調電費估算             worksheet 8
                writeWorksheet9(excelFormat);  // 全量儲冰空調電費估算         worksheet 9
                writeWorksheet10(excelFormat); // 分量儲冰空調電費估算         worksheet 10 
                writeWorksheet11(excelFormat); // 電價結構分析                 worksheet 11 
                writeWorksheet12(excelFormat); // 計劃工程費用分析             worksheet 12 
                writeWorksheet13(excelFormat); // 電價結構與投資費用之平衡性   worksheet 13
                writeWorksheet14(excelFormat); // 電力負荷結構分析             worksheet 14 
                writeWorksheet15(excelFormat); // 電價表                       worksheet 15 
                excelFormat.Save();

                //關閉並釋放
                excelFormat.Close();
                excelApp.Quit();

                //將報表上傳回 FTP server備存
                if (!ftpClient.FolderExists("/Ice-Storage-FTP/" + DateTime.Now.ToString("yyyyMMdd")))
                {
                    ftpClient.CreateFolder("/Ice-Storage-FTP/" + DateTime.Now.ToString("yyyyMMdd"));
                }
                ftpClient.Upload("/Ice-Storage-FTP/" + DateTime.Now.ToString("yyyyMMdd") + "/" + form1.form_Project_Design_Load.project_Design_Map["projectName"] + ".xls"
                                  , selectedPath + "/" + file_Name_TextBox.Text + ".xls");

                //將檔案資訊儲存至SQL,以便日後可查詢出檔案
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                String insertProjectSql = "INSERT INTO [ICE_STORAGE].[dbo].[FILE] " +
                                          "(project_name,file_name,upload_time) " +
                                          " VALUES ('" + form1.form_Project_Design_Load.project_Design_Map["projectName"] +
                                          "','" + file_Name_TextBox.Text +
                                          "','" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "')";

                try
                {
                    SqlCommand command = new SqlCommand(insertProjectSql, sqlConnection);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("新增專案名稱失敗,請確認網路連線是否正常!\n"+ ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                MessageBox.Show("匯出完成!!\n已匯出至 " + selectedPath + "/" + file_Name_TextBox.Text + ".xls", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            sqlConnection.Close();
            ftpClient.Close();
        }

        #region 經濟效益評估報告 worksheet 1 
        private void writeWorksheet1 (Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[1];
            worksheet.Cells[10, 6] = "半  凝  結  式";
        }
        #endregion

        #region 傳統空調負荷分佈 worksheet 2 
        private void writeWorksheet2(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[2];
        }
        #endregion

        #region 全量儲冰空調負荷分佈 worksheet 3 
        private void writeWorksheet3(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[3];
        }
        #endregion

        #region 分量儲冰空調負荷分佈 worksheet 4 
        private void writeWorksheet4(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[4];
        }
        #endregion

        #region 傳統空調電力負載分佈 worksheet 5 
        private void writeWorksheet5(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[5];
        }
        #endregion

        #region 全量儲冰空調電力負載分佈 worksheet 6 
        private void writeWorksheet6(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[6];
        }
        #endregion

        #region 分量儲冰空調電力負載分佈 worksheet 7 
        private void writeWorksheet7(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[7];
        }
        #endregion

        #region 傳統空調電費估算 worksheet 8 
        private void writeWorksheet8(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[8];
        }
        #endregion

        #region 全量儲冰空調電費估算 worksheet 9 
        private void writeWorksheet9(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[9];
        }
        #endregion

        #region 分量儲冰空調電費估算 worksheet 10 
        private void writeWorksheet10(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[10];
        }
        #endregion

        #region 電價結構分析 worksheet 11 
        private void writeWorksheet11(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[11];
        }
        #endregion

        #region 計劃工程費用分析 worksheet 12 
        private void writeWorksheet12(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[12];
        }
        #endregion

        #region 電價結構與投資費用之平衡性 worksheet 13 
        private void writeWorksheet13(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[13];
        }
        #endregion

        #region 電力負荷結構分析 worksheet 14 
        private void writeWorksheet14(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[14];
        }
        #endregion

        #region 電價表 worksheet 15 
        private void writeWorksheet15(Excel.Workbook excelFormat)
        {
            Excel.Worksheet worksheet = (Excel.Worksheet)excelFormat.Worksheets[15];
        }
        #endregion
    }
}
