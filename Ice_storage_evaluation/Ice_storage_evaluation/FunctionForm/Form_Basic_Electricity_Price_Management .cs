﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ice_storage_evaluation
{
    public partial class Form_Basic_Electricity_Price_Management : Form
    {
        private Form1 form1;
        SqlConnection sqlConnection;
        private int seq;
        public Dictionary<string, string> e_Price_Map { get; set; } = new Dictionary<string, string>();

        public Form_Basic_Electricity_Price_Management(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            string connectionString = form1.GetConnectionString();
            sqlConnection = new SqlConnection(connectionString);
            initialMap();
        }

        private void Form_Basic_Electricity_Price_Management_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.basicElectricityPriceManagement_CheckBox.Checked)
            {
                form1.basicElectricityPriceManagement_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void Form_Basic_Electricity_Price_Management_Load(object sender, EventArgs e)
        {
            // TODO: 這行程式碼會將資料載入 'iCE_STORAGE_DataSet.ELECTRICITY_PRICE' 資料表。您可以視需要進行移動或移除。
            this.eLECTRICITY_PRICETableAdapter.Fill(this.iCE_STORAGE_DataSet.ELECTRICITY_PRICE);
            e_Price_Name_comboBox.SelectedValue = e_Price_Map["e_price_seq"];
        }

        private void e_Price_Name_TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                e_Price_Name_TextBox.Text = e_Price_Name_TextBox.Text.Replace(" ", "");
                MessageBox.Show("不得輸入空白!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void e_Price_Choose_Button_Click(object sender, EventArgs e) //(先隱藏不使用,改用直接選完後顯示)依據ComboBox的值,從資料庫將值取出至Controls、Map
        {
            seq = Convert.ToInt32(this.e_Price_Name_comboBox.SelectedValue);

            if (seq == 0)
            {
                MessageBox.Show("請選擇要顯示哪筆資料!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            seq = Convert.ToInt32(this.e_Price_Name_comboBox.SelectedValue);

            String chooseSql = "SELECT * " +
                              "  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                              " WHERE e_price_seq = " + seq + "";

            SqlCommand command = new SqlCommand(chooseSql, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        String value = "";
                        for (int i = 0; i < reader.FieldCount; i++){
                            if (!reader.IsDBNull(i)) //欄位值不為空
                            {
                                if (i == 0) //欄位e_price_seq資料類型為int
                                {
                                    value = reader.GetInt32(i).ToString();
                                }
                                else
                                {
                                    value = reader.GetString(i);
                                }
                            }else{
                                value = "";
                            }

                            //將值塞進Map
                            e_Price_Map[reader.GetName(i)] = value;

                            //將值塞進control(TextBox)
                            foreach (Control control in this.Controls)
                            {
                                if (control.Name.Contains("Price_GroupBox"))
                                {
                                    foreach (Control GroupBoxControl in control.Controls)
                                    {
                                        if (GroupBoxControl.Name.ToLower().Contains(reader.GetName(i).ToLower()))
                                        {
                                            GroupBoxControl.Text = value;
                                        }
                                    }
                                }
                                else if (control.Name.ToLower().Contains(reader.GetName(i).ToLower()))
                                {
                                    control.Text = value;
                                }
                            }
                        }
                    }
                }
            }

            //塞值進電價時段 RadioButton
            String[] priceTimeArray = e_Price_Map["e_price_time"].Split(',');
            RadioButton crest_RadioButton;
            RadioButton flat_RadioButton;
            RadioButton trough_RadioButton;

            for (int i = 1 ; i <49; i++){     // 48個電價時段
                if (priceTimeArray[i-1] == "C")
                {
                    crest_RadioButton = (RadioButton)this.Controls.Find("panel" + i, true)[0].Controls.Find("crest_RadioButton_" + i, true)[0];
                    crest_RadioButton.Checked = true;
                }else if (priceTimeArray[i-1] == "F")
                {
                    flat_RadioButton = (RadioButton)this.Controls.Find("panel" + i, true)[0].Controls.Find("flat_RadioButton_" + i, true)[0];
                    flat_RadioButton.Checked = true;
                }else if (priceTimeArray[i-1] == "T")
                {
                    trough_RadioButton = (RadioButton)this.Controls.Find("panel" + i, true)[0].Controls.Find("trough_RadioButton_" + i, true)[0];
                    trough_RadioButton.Checked = true;
                }

            }

            // 塞值進用電月份 CheckBox
            String[] monthArray = e_Price_Map["peak_e_month"].Split(',');
            CheckBox month_CheckBox;
            for(int i = 1; i <13; i++){
                if (monthArray[i-1] == "Y")
                {
                    month_CheckBox = (CheckBox)this.Controls.Find("M" + i + "_CheckBox", true)[0];
                    month_CheckBox.Checked = true;
                }else{
                    month_CheckBox = (CheckBox)this.Controls.Find("M" + i + "_CheckBox", true)[0];
                    month_CheckBox.Checked = false;
                }
            }
            sqlConnection.Close();
        }

        private void initialMap(){
            e_Price_Map.Add("e_price_seq", "0");
            e_Price_Map.Add("e_price_name", "");
            e_Price_Map.Add("currency_system", "");
            e_Price_Map.Add("exchange_rate_TW", "");
            e_Price_Map.Add("two_summer_peak", "");
            e_Price_Map.Add("two_summer_normal", "");
            e_Price_Map.Add("two_summer_basic_price", "");
            e_Price_Map.Add("two_non_summer_peak", "");
            e_Price_Map.Add("two_non_summer_normal", "");
            e_Price_Map.Add("two_non_summer_basic_price", "");
            e_Price_Map.Add("three_summer_peak", "");
            e_Price_Map.Add("three_summer_normal", "");
            e_Price_Map.Add("three_summer_trough", "");
            e_Price_Map.Add("three_summer_basic_price", "");
            e_Price_Map.Add("three_non_summer_peak", "");
            e_Price_Map.Add("three_non_summer_normal", "");
            e_Price_Map.Add("three_non_summer_trough", "");
            e_Price_Map.Add("three_non_summer_basic_price", "");
            e_Price_Map.Add("e_price_time", "");
            e_Price_Map.Add("peak_e_month", "");
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            e_Price_Map["e_price_seq"] = this.e_Price_Name_comboBox.SelectedValue.ToString();
            e_Price_Map["e_price_name"] = this.e_Price_Name_TextBox.Text;
            e_Price_Map["currency_system"] = this.currency_System_TextBox.Text;
            e_Price_Map["exchange_rate_TW"] = this.exchange_Rate_TW_TextBox.Text;
            e_Price_Map["two_summer_peak"] = this.two_Summer_Peak_TextBox.Text;
            e_Price_Map["two_summer_normal"] = this.two_Summer_Normal_TextBox.Text;
            e_Price_Map["two_summer_basic_price"] = this.two_Summer_Basic_Price_TextBox.Text;
            e_Price_Map["two_non_summer_peak"] = this.two_Non_Summer_Peak_TextBox.Text;
            e_Price_Map["two_non_summer_normal"] = this.two_Non_Summer_Normal_TextBox.Text;
            e_Price_Map["two_non_summer_basic_price"] = this.two_Non_Summer_Basic_Price_TextBox.Text;
            e_Price_Map["three_summer_peak"] = this.three_Summer_Peak_TextBox.Text;
            e_Price_Map["three_summer_normal"] = this.three_Summer_Normal_TextBox.Text;
            e_Price_Map["three_summer_trough"] = this.three_Summer_Trough_TextBox.Text;
            e_Price_Map["three_summer_basic_price"] = this.three_Summer_Basic_Price_TextBox.Text;
            e_Price_Map["three_non_summer_peak"] = this.three_Non_Summer_Peak_TextBox.Text;
            e_Price_Map["three_non_summer_normal"] = this.three_Non_Summer_Normal_TextBox.Text;
            e_Price_Map["three_non_summer_trough"] = this.three_Non_Summer_Trough_TextBox.Text;
            e_Price_Map["three_non_summer_basic_price"] = this.three_Non_Summer_Basic_Price_TextBox.Text;

            String ePriceTime = "";
            for(int i = 1; i < 49; i++)
            {
                foreach(RadioButton radioButton in  this.Controls.Find("panel"+ i, true)[0].Controls)
                {
                    if (radioButton.Checked){
                        if (radioButton.Name.Contains("crest")){
                            ePriceTime = ePriceTime + "C,";
                        } else if (radioButton.Name.Contains("flat")) {
                            ePriceTime = ePriceTime + "F,";
                        } else if (radioButton.Name.Contains("trough")){
                            ePriceTime = ePriceTime + "T,";
                        }
                    }
                }
            }
            e_Price_Map["e_price_time"] = ePriceTime.TrimEnd(',');

            String peakMonth = "";
            CheckBox month_CheckBox;
            for (int i = 1; i < 13; i++)
            {
                month_CheckBox = (CheckBox)this.Controls.Find("M" + i + "_CheckBox", true)[0];
                if (month_CheckBox.Checked){
                    peakMonth = peakMonth + "Y,";
                }else{
                    peakMonth = peakMonth + "N,";
                }
            }
            e_Price_Map["peak_e_month"] = peakMonth.TrimEnd(',');
        }

        private void e_Price_New_Button_Click(object sender, EventArgs e)
        {
            if (e_Price_Name_TextBox.Text == ""){
                MessageBox.Show("電價表名稱不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            updateMap();

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //驗證電價表名稱是否重複
            String loginSql = "SELECT COUNT(1)" +
                              "  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                              " WHERE REPLACE(e_price_name,' ','') = '" + e_Price_Name_TextBox.Text.Replace(" ","") + "'";

            SqlCommand command = new SqlCommand(loginSql, sqlConnection);
            int nameCount = 0;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        nameCount = reader.GetInt32(0);
                    }
                }
            }

            if (nameCount > 0)
            {
                e_Price_Name_TextBox.Text = "";
                MessageBox.Show("電價表名稱不得重複,請重新輸入!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //取序號直接塞進Map
            String seqUserSql = "SELECT NEXT VALUE FOR [ICE_STORAGE].[dbo].Electricity_Price_Sequence";

            command = new SqlCommand(seqUserSql, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        e_Price_Map["e_price_seq"] = reader.GetInt32(0).ToString();
                    }
                }
            }


            String insertUserSql = "INSERT INTO [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                                   "  (e_price_seq,e_price_name,currency_system,exchange_rate_TW,two_summer_peak,two_summer_normal," +
                                   "   two_summer_basic_price,two_non_summer_peak,two_non_summer_normal,two_non_summer_basic_price," +
                                   "   three_summer_peak,three_summer_normal,three_summer_trough,three_summer_basic_price,three_non_summer_peak," +
                                   "   three_non_summer_normal,three_non_summer_trough,three_non_summer_basic_price,e_price_time,peak_e_month) " +
                                   " VALUES ('" + e_Price_Map["e_price_seq"] + "','" + e_Price_Map["e_price_name"] + "','" + e_Price_Map["currency_system"] + "'," +
                                   "         '" + e_Price_Map["exchange_rate_TW"] + "','" + e_Price_Map["two_summer_peak"] + "','" + e_Price_Map["two_summer_normal"] + "'," +
                                   "         '" + e_Price_Map["two_summer_basic_price"] + "','" + e_Price_Map["two_non_summer_peak"] + "','" + e_Price_Map["two_non_summer_normal"] + "'," +
                                   "         '" + e_Price_Map["two_non_summer_basic_price"] + "','" + e_Price_Map["three_summer_peak"] + "','" + e_Price_Map["three_summer_normal"] + "'," +
                                   "         '" + e_Price_Map["three_summer_trough"] + "','" + e_Price_Map["three_summer_basic_price"] + "','" + e_Price_Map["three_non_summer_peak"] + "'," +
                                   "         '" + e_Price_Map["three_non_summer_normal"] + "','" + e_Price_Map["three_non_summer_trough"] + "','" + e_Price_Map["three_non_summer_basic_price"] + "'," +
                                   "         '" + e_Price_Map["e_price_time"] + "','" + e_Price_Map["peak_e_month"] + "' )";

            //更新ComboBox的值及選項
            String selectNameStrSql = "SELECT CONVERT(varchar(30),[e_price_seq]) +' . '+[e_price_name] 'nameStr' , e_price_seq  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                                      "UNION ALL " +
                                      "SELECT  '--- 請選擇 ---' 'nameStr' , 0 " +
                                      "ORDER BY  e_price_seq";

            try
            {
                command = new SqlCommand(insertUserSql, sqlConnection);
                if (command.ExecuteNonQuery().ToString() != "0")
                {
                    MessageBox.Show("新增成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SqlDataAdapter newAdapter = new SqlDataAdapter(selectNameStrSql, sqlConnection);
                    DataSet newDataset = new DataSet();
                    newAdapter.Fill(newDataset, "New");
                    e_Price_Name_comboBox.DataSource = newDataset.Tables["NEW"];
                    e_Price_Name_comboBox.DisplayMember = "nameStr";
                    e_Price_Name_comboBox.ValueMember = "e_price_seq";
                    e_Price_Name_comboBox.SelectedValue = e_Price_Map["e_price_seq"];
                }
                else
                {
                    MessageBox.Show("新增失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            sqlConnection.Close();

        }  //新增紀錄資料

        private void e_Price_Edit_Button_Click(object sender, EventArgs e)  //修改選單選項中的資料
        {
            String nameStr = this.e_Price_Name_comboBox.Text;
            seq = Convert.ToInt32(this.e_Price_Name_comboBox.SelectedValue);

            if (seq == 0)
            {
                MessageBox.Show("請選擇要修改哪筆資料!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("確定要修改此筆資料 : " + nameStr + " ?", "修改",
                     MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
            {
                return;
            }

            if (e_Price_Name_TextBox.Text == "")
            {
                MessageBox.Show("電價表名稱不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            updateMap();

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //驗證電價表名稱是否重複
            String loginSql = "SELECT COUNT(1)" +
                              "  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                              " WHERE REPLACE(e_price_name,' ','') = '" + e_Price_Name_TextBox.Text.Replace(" ", "") + "'" +
                              "   AND e_price_seq != " + seq + "";

            SqlCommand command = new SqlCommand(loginSql, sqlConnection);
            int nameCount = 0;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        nameCount = reader.GetInt32(0);
                    }
                }
            }

            if (nameCount > 0)
            {
                e_Price_Name_TextBox.Text = "";
                MessageBox.Show("電價表名稱不得重複,請重新輸入!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String updateUserSql = "UPDATE [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] SET " +
                                   "  e_price_name = '" + e_Price_Map["e_price_name"] + "',currency_system = '" + e_Price_Map["currency_system"] + "',exchange_rate_TW = '" + e_Price_Map["exchange_rate_TW"] + "'," +
                                   "  two_summer_peak = '" + e_Price_Map["two_summer_peak"] + "',two_summer_normal = '" + e_Price_Map["two_summer_normal"] + "'," +
                                   "  two_summer_basic_price = '" + e_Price_Map["two_summer_basic_price"] + "',two_non_summer_peak = '" + e_Price_Map["two_non_summer_peak"] + "',two_non_summer_normal = '" + e_Price_Map["two_non_summer_normal"] + "'," +
                                   "  two_non_summer_basic_price = '" + e_Price_Map["two_non_summer_basic_price"] + "', three_summer_peak = '" + e_Price_Map["three_summer_peak"] + "'," +
                                   "  three_summer_normal = '" + e_Price_Map["three_summer_normal"] + "',three_summer_trough = '" + e_Price_Map["three_summer_trough"] + "'," +
                                   "  three_summer_basic_price = '" + e_Price_Map["three_summer_basic_price"] + "',three_non_summer_peak = '" + e_Price_Map["three_non_summer_peak"] + "'," +
                                   "  three_non_summer_normal = '" + e_Price_Map["three_non_summer_normal"] + "',three_non_summer_trough = '" + e_Price_Map["three_non_summer_trough"] + "'," +
                                   "  three_non_summer_basic_price = '" + e_Price_Map["three_non_summer_basic_price"] + "',e_price_time = '" + e_Price_Map["e_price_time"] + "',peak_e_month = '" + e_Price_Map["peak_e_month"] + "'" +
                                   "WHERE e_price_seq = " + seq + "";

            //更新ComboBox的值及選項
            String selectNameStrSql = "SELECT CONVERT(varchar(30),[e_price_seq]) +' . '+[e_price_name] 'nameStr' , e_price_seq  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                                      "UNION ALL " +
                                      "SELECT  '--- 請選擇 ---' 'nameStr' , 0 " +
                                      "ORDER BY  e_price_seq";

            try
            {
                command = new SqlCommand(updateUserSql, sqlConnection);
                if (command.ExecuteNonQuery().ToString() != "0")
                {
                    MessageBox.Show("修改成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SqlDataAdapter newAdapter = new SqlDataAdapter(selectNameStrSql, sqlConnection);
                    DataSet newDataset = new DataSet();
                    newAdapter.Fill(newDataset, "UPDATE");
                    e_Price_Name_comboBox.DataSource = newDataset.Tables["UPDATE"];
                    e_Price_Name_comboBox.DisplayMember = "nameStr";
                    e_Price_Name_comboBox.ValueMember = "e_price_seq";
                    e_Price_Name_comboBox.SelectedValue = e_Price_Map["e_price_seq"];
                }
                else
                {
                    MessageBox.Show("修改失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            sqlConnection.Close();

        }

        private void e_Price_Delete_Button_Click(object sender, EventArgs e)
        {
            String nameStr = this.e_Price_Name_comboBox.Text;
            seq = Convert.ToInt32(this.e_Price_Name_comboBox.SelectedValue);

            if (seq == 0){
                MessageBox.Show("請選擇要刪除哪筆資料!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("確定要刪除此筆資料 : " + nameStr + " ?", "刪除",
                     MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
            {
                return;
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String deleteSql = "DELETE FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] WHERE e_price_seq = " + seq + "";
            String selectNameStrSql = "SELECT CONVERT(varchar(30),[e_price_seq]) +' . '+[e_price_name] 'nameStr' , e_price_seq  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                                      "UNION ALL "+
                                      "SELECT  '--- 請選擇 ---' 'nameStr' , 0 "+
                                      "ORDER BY  e_price_seq";

            try
            {
                SqlCommand command = new SqlCommand(deleteSql, sqlConnection);
                if (command.ExecuteNonQuery().ToString() != "0")
                {
                    MessageBox.Show("刪除成功!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SqlDataAdapter newAdapter = new SqlDataAdapter(selectNameStrSql, sqlConnection);
                    DataSet newDataset = new DataSet();
                    newAdapter.Fill(newDataset, "DELETE");
                    e_Price_Name_comboBox.DataSource = newDataset.Tables["DELETE"];
                    e_Price_Name_comboBox.DisplayMember = "nameStr";
                    e_Price_Name_comboBox.ValueMember = "e_price_seq";
                    e_Price_Map["e_price_seq"] = "0";
                    e_Price_Name_comboBox.SelectedValue = e_Price_Map["e_price_seq"];
                }
                else
                {
                    MessageBox.Show("刪除失敗,請確認網路連線!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            sqlConnection.Close();
        }  //刪除紀錄資料

        private void e_Price_Name_comboBox_SelectionChangeCommitted(object sender, EventArgs e) //依據ComboBox的值,從資料庫將值取出至Controls、Map
        {
            if (Convert.ToInt32(e_Price_Name_comboBox.SelectedValue) == 0)
            {
                MessageBox.Show("請選擇要顯示哪筆資料!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e_Price_Name_comboBox.SelectedValue = e_Price_Map["e_price_seq"];
                return;
            }
            else
            {
                e_Price_Map["e_price_seq"] = e_Price_Name_comboBox.SelectedValue.ToString();
            }

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("請確認網路連線是否正常!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            seq = Convert.ToInt32(this.e_Price_Name_comboBox.SelectedValue);

            String chooseSql = "SELECT * " +
                              "  FROM [ICE_STORAGE].[dbo].[ELECTRICITY_PRICE] " +
                              " WHERE e_price_seq = " + seq ;

            SqlCommand command = new SqlCommand(chooseSql, sqlConnection);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        String value = "";
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            if (!reader.IsDBNull(i)) //欄位值不為空
                            {
                                if (i == 0) //欄位e_price_seq資料類型為int
                                {
                                    value = reader.GetInt32(i).ToString();
                                }
                                else
                                {
                                    value = reader.GetString(i);
                                }
                            }
                            else
                            {
                                value = "";
                            }

                            //將值塞進Map
                            e_Price_Map[reader.GetName(i)] = value;

                            //將值塞進control(TextBox) , 用變數名稱與DB欄位名稱塞值
                            foreach (Control control in this.Controls)
                            {
                                if (control.Name.Contains("Price_GroupBox"))
                                {
                                    foreach (Control GroupBoxControl in control.Controls)
                                    {
                                        if (GroupBoxControl.Name.ToLower().Contains(reader.GetName(i).ToLower()))
                                        {
                                            GroupBoxControl.Text = value;
                                        }
                                    }
                                }
                                else if (control.Name.ToLower().Contains(reader.GetName(i).ToLower()))
                                {
                                    control.Text = value;
                                }
                            }
                        }
                    }
                }
            }

            //塞值進電價時段 RadioButton
            String[] priceTimeArray = e_Price_Map["e_price_time"].Split(',');
            RadioButton crest_RadioButton;
            RadioButton flat_RadioButton;
            RadioButton trough_RadioButton;

            for (int i = 1; i < 49; i++)
            {     // 48個電價時段
                if (priceTimeArray[i - 1] == "C")
                {
                    crest_RadioButton = (RadioButton)this.Controls.Find("panel" + i, true)[0].Controls.Find("crest_RadioButton_" + i, true)[0];
                    crest_RadioButton.Checked = true;
                }
                else if (priceTimeArray[i - 1] == "F")
                {
                    flat_RadioButton = (RadioButton)this.Controls.Find("panel" + i, true)[0].Controls.Find("flat_RadioButton_" + i, true)[0];
                    flat_RadioButton.Checked = true;
                }
                else if (priceTimeArray[i - 1] == "T")
                {
                    trough_RadioButton = (RadioButton)this.Controls.Find("panel" + i, true)[0].Controls.Find("trough_RadioButton_" + i, true)[0];
                    trough_RadioButton.Checked = true;
                }

            }

            // 塞值進用電月份 CheckBox
            String[] monthArray = e_Price_Map["peak_e_month"].Split(',');
            CheckBox month_CheckBox;
            for (int i = 1; i < 13; i++)
            {
                month_CheckBox = (CheckBox)this.Controls.Find("M" + i + "_CheckBox", true)[0];
                if (monthArray[i - 1] == "Y")
                {
                    month_CheckBox.Checked = true;
                }
                else
                {
                    month_CheckBox.Checked = false;
                }
            }
            sqlConnection.Close();
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        } //確定鈕

    }
}
