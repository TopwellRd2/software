﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Mode_Running_Time : Form
    {
        private Form1 form1;
        public Dictionary<string, string> mode_Running_Time_Map { get; set; } = new Dictionary<string, string>();

        public Form_Mode_Running_Time(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Mode_Running_Time_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.modeRunningTime_CheckBox.Checked)
            {
                form1.modeRunningTime_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void RadioButton_Click(object sender, EventArgs e)  //改成非必選,把AutoCheck功能關閉,已選的radioButton再按一次可以取消勾選
        {
            RadioButton radioButton = sender as RadioButton;
            Boolean isButtonChecked = false;    //一個panel裡的radioButton是否有被勾選的,若有則要扣除另一個裝置的時數,否則不扣
            if (!radioButton.Checked) {      //將radioButton勾選
                foreach(RadioButton allButton in radioButton.Parent.Controls)
                {
                    if (allButton.Checked)
                    {
                        isButtonChecked = true;
                    }
                    allButton.Checked = false;
                }
                radioButton.Checked = true;
                if (radioButton.Name.Contains("full_Ice"))
                {
                    full_Ice_Hour_TextBox.Text = (Double.Parse(full_Ice_Hour_TextBox.Text) + 0.5).ToString();
                    if (isButtonChecked)
                    {
                        full_Air_Hour_TextBox.Text = (Double.Parse(full_Air_Hour_TextBox.Text) - 0.5).ToString();
                    }
                }
                else if (radioButton.Name.Contains("full_Air"))
                {
                    full_Air_Hour_TextBox.Text = (Double.Parse(full_Air_Hour_TextBox.Text) + 0.5).ToString();
                    if (isButtonChecked)
                    {
                        full_Ice_Hour_TextBox.Text = (Double.Parse(full_Ice_Hour_TextBox.Text) - 0.5).ToString();
                    }
                }
                else if (radioButton.Name.Contains("part_Ice"))
                {
                    part_Ice_Hour_TextBox.Text = (Double.Parse(part_Ice_Hour_TextBox.Text) + 0.5).ToString();
                    if (isButtonChecked)
                    {
                        part_Air_Hour_TextBox.Text = (Double.Parse(part_Air_Hour_TextBox.Text) - 0.5).ToString();
                    }
                }
                else if (radioButton.Name.Contains("part_Air"))
                {
                    part_Air_Hour_TextBox.Text = (Double.Parse(part_Air_Hour_TextBox.Text) + 0.5).ToString();
                    if (isButtonChecked)
                    {
                        part_Ice_Hour_TextBox.Text = (Double.Parse(part_Ice_Hour_TextBox.Text) - 0.5).ToString();
                    }
                }
            }
            else
            {
                radioButton.Checked = false;
                if (radioButton.Name.Contains("full_Ice"))
                {
                    full_Ice_Hour_TextBox.Text = (Double.Parse(full_Ice_Hour_TextBox.Text) - 0.5).ToString();
                }
                else if (radioButton.Name.Contains("full_Air"))
                {
                    full_Air_Hour_TextBox.Text = (Double.Parse(full_Air_Hour_TextBox.Text) - 0.5).ToString();
                }
                else if (radioButton.Name.Contains("part_Ice"))
                {
                    part_Ice_Hour_TextBox.Text = (Double.Parse(part_Ice_Hour_TextBox.Text) - 0.5).ToString();
                }
                else if (radioButton.Name.Contains("part_Air"))
                {
                    part_Air_Hour_TextBox.Text = (Double.Parse(part_Air_Hour_TextBox.Text) - 0.5).ToString();
                }
            }
        }

        private void initialMap()
        {
            mode_Running_Time_Map.Add("full", "FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,0,0,0,0,0,0,0,0,FI,FI,FI,FI");
            mode_Running_Time_Map.Add("part", "PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,0,0,0,0,0,0,0,0,PI,PI,PI,PI");
            mode_Running_Time_Map.Add("e_price", "T,T,T,T,T,T,T,T,T,T,T,T,T,F,F,F,F,F,C,C,C,C,F,F,C,C,C,C,C,C,C,C,F,F,F,F,F,F,F,F,F,F,F,T,T,T,T,T");
            mode_Running_Time_Map.Add("full_ice_hour", "9");
            mode_Running_Time_Map.Add("full_air_hour", "11");
            mode_Running_Time_Map.Add("part_ice_hour", "9");
            mode_Running_Time_Map.Add("part_air_hour", "11");
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            String full = "";
            String part = "";
            String e_price = "";
            int noChoose = 0; //一個panel中的RadioButton沒被勾選的個數
            //全量 panel 1~24、73~96
            for (int i =1; i<25; i++){
                noChoose = 0;
                foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                {
                    if (radioButton.Checked)
                    {
                        if (radioButton.Name.Contains("full_Ice"))
                        {
                            full = full + "FI,";
                        }else if (radioButton.Name.Contains("full_Air"))
                        {
                            full = full + "FA,";
                        }
                    } else {  //未勾選
                        noChoose++;
                    }
                }
                if (noChoose == 2) {
                    full = full + "0,";
                }
            }

            for (int i = 73; i < 97; i++)
            {
                noChoose = 0;
                foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                {
                    if (radioButton.Checked)
                    {
                        if (radioButton.Name.Contains("full_Ice"))
                        {
                            full = full + "FI,";
                        }
                        else if (radioButton.Name.Contains("full_Air"))
                        {
                            full = full + "FA,";
                        }
                    }
                    else
                    {  //都未勾選
                        noChoose++;
                    }
                }
                if (noChoose == 2)
                {
                    full = full + "0,";
                }
            }

            //分量 panel 25~48、97~120
            for (int i = 25; i < 49; i++)
            {
                noChoose = 0;
                foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                {
                    if (radioButton.Checked)
                    {
                        if (radioButton.Name.Contains("part_Ice"))
                        {
                            part = part + "PI,";
                        }
                        else if (radioButton.Name.Contains("part_Air"))
                        {
                            part = part + "PA,";
                        }
                    }
                    else
                    {  //都未勾選
                        noChoose++;
                    }
                }
                if (noChoose == 2)
                {
                    part = part + "0,";
                }
            }

            for (int i = 97; i < 121; i++)
            {
                noChoose = 0;
                foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                {
                    if (radioButton.Checked)
                    {
                        if (radioButton.Name.Contains("part_Ice"))
                        {
                            part = part + "PI,";
                        }
                        else if (radioButton.Name.Contains("part_Air"))
                        {
                            part = part + "PA,";
                        }
                    }
                    else
                    {  //都未勾選
                        noChoose++;
                    }
                }
                if (noChoose == 2)
                {
                    part = part + "0,";
                }
            }

            //電價時段 panel 49~72、121~144
            for (int i = 49; i < 73; i++)
            {
                noChoose = 0;
                foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                {
                    if (radioButton.Checked)
                    {
                        if (radioButton.Name.Contains("crest"))
                        {
                            e_price = e_price + "C,";
                        }
                        else if (radioButton.Name.Contains("flat"))
                        {
                            e_price = e_price + "F,";
                        }
                        else if (radioButton.Name.Contains("trough"))
                        {
                            e_price = e_price + "T,";
                        }
                    }
                    else
                    {  //都未勾選
                       noChoose++;
                    }
                }
                if (noChoose == 3)
                {
                    e_price = e_price + "0,";
                }
            }

            for (int i = 121; i < 145; i++)
            {
                noChoose = 0;
                foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                {
                    if (radioButton.Checked)
                    {
                        if (radioButton.Name.Contains("crest"))
                        {
                            e_price = e_price + "C,";
                        }
                        else if (radioButton.Name.Contains("flat"))
                        {
                            e_price = e_price + "F,";
                        }
                        else if (radioButton.Name.Contains("trough"))
                        {
                            e_price = e_price + "T,";
                        }
                    }
                    else
                    {  //都未勾選
                        noChoose++;
                    }
                }
                if (noChoose == 3)
                {
                    e_price = e_price + "0,";
                }
            }

            mode_Running_Time_Map["full"] = full.TrimEnd(',');
            mode_Running_Time_Map["part"] = part.TrimEnd(',');
            mode_Running_Time_Map["e_price"] = e_price.TrimEnd(',');
            mode_Running_Time_Map["full_ice_hour"] = full_Ice_Hour_TextBox.Text;
            mode_Running_Time_Map["full_air_hour"] = full_Air_Hour_TextBox.Text;
            mode_Running_Time_Map["part_ice_hour"] = part_Ice_Hour_TextBox.Text;
            mode_Running_Time_Map["part_air_hour"] = part_Air_Hour_TextBox.Text;
        }

        private void mode_Running_Time_Ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void full_Rollback_Button_Click(object sender, EventArgs e)
        {
            String full = "FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FI,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,FA,0,0,0,0,0,0,0,0,FI,FI,FI,FI";
            String[] fullArray = full.Split(',');

            for (int i = 1; i < 25; i++){
                if (fullArray[i-1] == "FI")
                {
                    foreach(RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("full_Ice"))
                        {
                            radioButton.Checked = true;
                        } 
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                } 
                else if (fullArray[i-1] == "FA")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("full_Air"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (fullArray[i - 1] == "0")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + i, true)[0].Controls)
                    {
                         radioButton.Checked = false;
                    }
                }
            }

            for (int i = 25; i < 49; i++)
            {
                if (fullArray[i-1] == "FI")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 48), true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("full_Ice"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (fullArray[i-1] == "FA")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 48), true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("full_Air"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (fullArray[i - 1] == "0")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 48), true)[0].Controls)
                    {
                        radioButton.Checked = false;
                    }
                }
            }
            full_Ice_Hour_TextBox.Text = "9";
            full_Air_Hour_TextBox.Text = "11";

        } //全量選項回復預設值(依據顏色)

        private void part_Rollback_Button_Click(object sender, EventArgs e)
        {
            String part = "PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PI,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,PA,0,0,0,0,0,0,0,0,PI,PI,PI,PI";
            String[] partArray = part.Split(',');

            for (int i = 1; i < 25; i++)
            {
                if (partArray[i - 1] == "PI")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 24), true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("part_Ice"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (partArray[i - 1] == "PA")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 24), true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("part_Air"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (partArray[i - 1] == "0")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 24), true)[0].Controls)
                    {
                        radioButton.Checked = false;
                    }
                }
            }

            for (int i = 25; i < 49; i++)
            {
                if (partArray[i - 1] == "PI")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 72), true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("part_Ice"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (partArray[i - 1] == "PA")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 72), true)[0].Controls)
                    {
                        if (radioButton.Name.Contains("part_Air"))
                        {
                            radioButton.Checked = true;
                        }
                        else
                        {
                            radioButton.Checked = false;
                        }
                    }
                }
                else if (partArray[i - 1] == "0")
                {
                    foreach (RadioButton radioButton in this.Controls.Find("panel" + (i + 72), true)[0].Controls)
                    {
                        radioButton.Checked = false;
                    }
                }
            }
            part_Ice_Hour_TextBox.Text = "9";
            part_Air_Hour_TextBox.Text = "11";
        } //分量選項回復預設值(依據顏色)
    }
}
