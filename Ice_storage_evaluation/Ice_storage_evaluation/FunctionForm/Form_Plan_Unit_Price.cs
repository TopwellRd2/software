﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Plan_Unit_Price : Form
    {
        private Form1 form1;
        public Dictionary<string, string> plan_Unit_Price_Map { get; set; } = new Dictionary<string, string>();

        public Form_Plan_Unit_Price(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Plan_Unit_Price_FormClosing(object sender, FormClosingEventArgs e)   //關閉前檢查
        {
            if (air_Set_RadioButton.Checked && string.IsNullOrWhiteSpace(air_Set_TextBox.Text))
            {
                MessageBox.Show("傳統空調計劃單價設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                air_Set_TextBox.Text = "";
                air_Set_TextBox.Focus();
                e.Cancel = true;
            }
            else if (ice_Set_RadioButton.Checked && string.IsNullOrWhiteSpace(ice_Set_TextBox.Text))
            {
                MessageBox.Show("儲冰設備計劃單價設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ice_Set_TextBox.Text = "";
                ice_Set_TextBox.Focus();
                e.Cancel = true;
            }
            else if (auto_Set_RadioButton.Checked && string.IsNullOrWhiteSpace(auto_Set_TextBox.Text))
            {
                MessageBox.Show("自動控制設計單價設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                auto_Set_TextBox.Text = "";
                auto_Set_TextBox.Focus();
                e.Cancel = true;
            }
            else if (high_Set_RadioButton.Checked && (string.IsNullOrWhiteSpace(high_Set_TextBox.Text)))
            {
                MessageBox.Show("高壓裝置費用設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                high_Set_TextBox.Text = "";
                high_Set_TextBox.Focus();
                e.Cancel = true;
            }
            else if (high_Set_RadioButton.Checked && string.IsNullOrWhiteSpace(out_Set_TextBox.Text))
            {
                MessageBox.Show("外線補助費用設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                out_Set_TextBox.Text = "";
                out_Set_TextBox.Focus();
                e.Cancel = true;
            }
            else if (design_Set_RadioButton.Checked && string.IsNullOrWhiteSpace(design_Set_TextBox.Text))
            {
                MessageBox.Show("設計費用設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                design_Set_TextBox.Text = "";
                design_Set_TextBox.Focus();
                e.Cancel = true;
            }
        }

        private void Form_Plan_Unit_Price_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.planUnitPrice_CheckBox.Checked)
            {
                form1.planUnitPrice_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            plan_Unit_Price_Map.Add("air_choose", air_Set_TextBox.Text);
            plan_Unit_Price_Map.Add("ice_choose", ice_Set_TextBox.Text);
            plan_Unit_Price_Map.Add("auto_choose", auto_Set_TextBox.Text);
            plan_Unit_Price_Map.Add("high_choose", (int.Parse(high_Set_TextBox.Text) + int.Parse(out_Set_TextBox.Text)).ToString());
            plan_Unit_Price_Map.Add("design_choose", design_Set_TextBox.Text);
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            plan_Unit_Price_Map["air_choose"] = air_Set_TextBox.Text;
            plan_Unit_Price_Map["ice_choose"] = ice_Set_TextBox.Text;
            plan_Unit_Price_Map["auto_choose"] = auto_Set_TextBox.Text;
            plan_Unit_Price_Map["high_choose"] = (int.Parse(high_Set_TextBox.Text) + int.Parse(out_Set_TextBox.Text)).ToString();
            plan_Unit_Price_Map["high_choose"] = design_Set_TextBox.Text;

            /*
            if (air_30_RadioButton.Checked)
            {
                plan_Unit_Price_Map["air_choose"] = "30000";
            }
            else if (air_35_RadioButton.Checked)
            {
                plan_Unit_Price_Map["air_choose"] = "35000";
            }
            else if (air_40_RadioButton.Checked)
            {
                plan_Unit_Price_Map["air_choose"] = "40000";
            }
            else
            {
                plan_Unit_Price_Map["air_choose"] = air_Set_TextBox.Text;
            }

            if (ice_18_RadioButton.Checked)
            {
                plan_Unit_Price_Map["ice_choose"] = "1800";
            }
            else if (ice_20_RadioButton.Checked)
            {
                plan_Unit_Price_Map["ice_choose"] = "2000";
            }
            else
            {
                plan_Unit_Price_Map["ice_choose"] = ice_Set_TextBox.Text;
            }

            if (auto_Y_RadioButton.Checked)
            {
                plan_Unit_Price_Map["auto_choose"] = "400000";
            }
            else if (auto_N_RadioButton.Checked)
            {
                plan_Unit_Price_Map["auto_choose"] = "0";
            }
            else
            {
                plan_Unit_Price_Map["auto_choose"] = auto_Set_TextBox.Text;
            }

            if (high_Y_RadioButton.Checked)
            {
                plan_Unit_Price_Map["high_choose"] = "2800";
            }
            else if (high_N_RadioButton.Checked)
            {
                plan_Unit_Price_Map["high_choose"] = "0";
            }
            else
            {
                plan_Unit_Price_Map["high_choose"] = (int.Parse(high_Set_TextBox.Text) + int.Parse(out_Set_TextBox.Text)).ToString();
            }

            if (disign_Y_RadioButton.Checked)
            {
                plan_Unit_Price_Map["high_choose"] = "Y";
            }
            else if (disign_N_RadioButton.Checked)
            {
                plan_Unit_Price_Map["high_choose"] = "N";
            }
            else
            {
                plan_Unit_Price_Map["high_choose"] = design_Set_TextBox.Text;
            }
            */
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            TextBox textbox;

            if (radioButton.Name.Contains("Set_RadioButton"))
            {
                foreach(Control control in radioButton.Parent.Controls)
                {
                    if (control is TextBox)
                    {
                        textbox = (TextBox) control;
                        textbox.Enabled = true;
                        textbox.Text = "";
                    }
                }
            }
            else
            {
                foreach (Control control in radioButton.Parent.Controls)
                {
                    if (control is TextBox)
                    {
                        textbox = (TextBox)control;
                        textbox.Enabled = false;
                        if (radioButton.Name == "air_30_RadioButton")
                        {
                            textbox.Text = "30000";
                        }
                        else if (radioButton.Name == "air_35_RadioButton")
                        {
                            textbox.Text = "35000";
                        }
                        else if (radioButton.Name == "air_40_RadioButton")
                        {
                            textbox.Text = "40000";
                        }
                        else if (radioButton.Name == "ice_18_RadioButton")
                        {
                            textbox.Text = "1800";
                        }
                        else if (radioButton.Name == "ice_20_RadioButton")
                        {
                            textbox.Text = "2000";
                        }
                        else if (radioButton.Name == "auto_Y_RadioButton")
                        {
                            textbox.Text = "400000";
                        }
                        else if (radioButton.Name == "high_Y_RadioButton")
                        {
                            if (textbox.Name == "high_Set_TextBox")
                            { 
                                high_Set_TextBox.Text = "1200"; 
                            } else
                            {
                                out_Set_TextBox.Text = "1600";
                            }
                        }
                        else if (radioButton.Name == "disign_Y_RadioButton")
                        {
                            textbox.Text = "0.015";
                        }
                        else
                        {
                            textbox.Text = "0";
                        }
                    }
                }
            }
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        
    }
}
