﻿namespace Ice_storage_evaluation
{
    partial class Form_Project_Design_Load
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projectNameGroupBox = new System.Windows.Forms.GroupBox();
            this.projectName_TextBox = new System.Windows.Forms.TextBox();
            this.fullDayLoadDesignSamplingGroupBox = new System.Windows.Forms.GroupBox();
            this.day_003_TextBox = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.day_240_TextBox = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.day_233_TextBox = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.day_230_TextBox = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.day_223_TextBox = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.day_220_TextBox = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.day_213_TextBox = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.day_210_TextBox = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.day_203_TextBox = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.day_200_TextBox = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.day_193_TextBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.day_190_TextBox = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.day_183_TextBox = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.day_180_TextBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.day_173_TextBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.day_170_TextBox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.day_163_TextBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.day_160_TextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.day_153_TextBox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.day_150_TextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.day_143_TextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.day_140_TextBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.day_133_TextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.day_130_TextBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.day_123_TextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.day_120_TextBox = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.day_113_TextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.day_110_TextBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.day_103_TextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.day_100_TextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.day_093_TextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.day_090_TextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.day_083_TextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.day_080_TextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.day_073_TextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.day_070_TextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.day_063_TextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.day_060_TextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.day_053_TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.day_050_TextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.day_043_TextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.day_040_TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.day_033_TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.day_030_TextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.day_023_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.day_020_TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.day_013_TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.day_010_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.monthlyLoadgroupBox = new System.Windows.Forms.GroupBox();
            this.label82 = new System.Windows.Forms.Label();
            this.month12_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.month12_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.month11_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.month11_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.month10_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.month10_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.month9_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.month9_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.month8_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.month8_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.month7_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.month7_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.month6_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.month6_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.month5_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.month5_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.month4_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.month4_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.month3_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.month3_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.month2_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.month2_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.month1_Workday_TextBox = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.month1_Scale_TextBox = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.dayToDef_Button = new System.Windows.Forms.Button();
            this.scaleToDef_Button = new System.Windows.Forms.Button();
            this.workdayToDef_Button = new System.Windows.Forms.Button();
            this.project_Design_Ok_Button = new System.Windows.Forms.Button();
            this.peakLoadGroupBox = new System.Windows.Forms.GroupBox();
            this.peak_Load_TextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.hr15_Button = new System.Windows.Forms.Button();
            this.label89 = new System.Windows.Forms.Label();
            this.hr14_Button = new System.Windows.Forms.Button();
            this.label88 = new System.Windows.Forms.Label();
            this.hr11_Button = new System.Windows.Forms.Button();
            this.label87 = new System.Windows.Forms.Label();
            this.hr10_Button = new System.Windows.Forms.Button();
            this.label86 = new System.Windows.Forms.Label();
            this.hr9_Button = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.factory_Button = new System.Windows.Forms.Button();
            this.office_Button = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.day31_Button = new System.Windows.Forms.Button();
            this.day20_Button = new System.Windows.Forms.Button();
            this.projectNameGroupBox.SuspendLayout();
            this.fullDayLoadDesignSamplingGroupBox.SuspendLayout();
            this.monthlyLoadgroupBox.SuspendLayout();
            this.peakLoadGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // projectNameGroupBox
            // 
            this.projectNameGroupBox.Controls.Add(this.projectName_TextBox);
            this.projectNameGroupBox.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.projectNameGroupBox.Location = new System.Drawing.Point(10, 10);
            this.projectNameGroupBox.Name = "projectNameGroupBox";
            this.projectNameGroupBox.Size = new System.Drawing.Size(1439, 64);
            this.projectNameGroupBox.TabIndex = 0;
            this.projectNameGroupBox.TabStop = false;
            this.projectNameGroupBox.Text = "專案名稱";
            // 
            // projectName_TextBox
            // 
            this.projectName_TextBox.Location = new System.Drawing.Point(20, 24);
            this.projectName_TextBox.Name = "projectName_TextBox";
            this.projectName_TextBox.Size = new System.Drawing.Size(1407, 31);
            this.projectName_TextBox.TabIndex = 0;
            this.projectName_TextBox.Leave += new System.EventHandler(this.projectName_TextBox_Leave);
            // 
            // fullDayLoadDesignSamplingGroupBox
            // 
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_003_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label47);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_240_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label48);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_233_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label45);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_230_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label46);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_223_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label43);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_220_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label44);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_213_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label41);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_210_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label42);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_203_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label39);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_200_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label40);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_193_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label37);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_190_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label38);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_183_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label35);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_180_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label36);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_173_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label33);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_170_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label34);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_163_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label31);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_160_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label32);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_153_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label29);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_150_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label30);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_143_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label27);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_140_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label28);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_133_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label25);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_130_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label26);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_123_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label23);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_120_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label24);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_113_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label21);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_110_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label22);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_103_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label19);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_100_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label20);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_093_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label17);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_090_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label18);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_083_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label15);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_080_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label16);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_073_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label13);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_070_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label14);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_063_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label11);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_060_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label12);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_053_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label9);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_050_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label10);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_043_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label7);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_040_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label8);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_033_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label5);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_030_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label6);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_023_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label3);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_020_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label4);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_013_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label2);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.day_010_TextBox);
            this.fullDayLoadDesignSamplingGroupBox.Controls.Add(this.label1);
            this.fullDayLoadDesignSamplingGroupBox.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.fullDayLoadDesignSamplingGroupBox.Location = new System.Drawing.Point(10, 79);
            this.fullDayLoadDesignSamplingGroupBox.Name = "fullDayLoadDesignSamplingGroupBox";
            this.fullDayLoadDesignSamplingGroupBox.Size = new System.Drawing.Size(630, 718);
            this.fullDayLoadDesignSamplingGroupBox.TabIndex = 1;
            this.fullDayLoadDesignSamplingGroupBox.TabStop = false;
            this.fullDayLoadDesignSamplingGroupBox.Text = "全日負荷設計取樣";
            // 
            // day_003_TextBox
            // 
            this.day_003_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_003_TextBox.Location = new System.Drawing.Point(394, 673);
            this.day_003_TextBox.Name = "day_003_TextBox";
            this.day_003_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_003_TextBox.TabIndex = 95;
            this.day_003_TextBox.Text = "0";
            this.day_003_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(319, 676);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(54, 22);
            this.label47.TabIndex = 94;
            this.label47.Text = "00:30";
            // 
            // day_240_TextBox
            // 
            this.day_240_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_240_TextBox.Location = new System.Drawing.Point(116, 673);
            this.day_240_TextBox.Name = "day_240_TextBox";
            this.day_240_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_240_TextBox.TabIndex = 93;
            this.day_240_TextBox.Text = "0";
            this.day_240_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label48.Location = new System.Drawing.Point(41, 676);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(54, 22);
            this.label48.TabIndex = 92;
            this.label48.Text = "24:00";
            // 
            // day_233_TextBox
            // 
            this.day_233_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_233_TextBox.Location = new System.Drawing.Point(394, 645);
            this.day_233_TextBox.Name = "day_233_TextBox";
            this.day_233_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_233_TextBox.TabIndex = 91;
            this.day_233_TextBox.Text = "0";
            this.day_233_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(319, 648);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 22);
            this.label45.TabIndex = 90;
            this.label45.Text = "23:30";
            // 
            // day_230_TextBox
            // 
            this.day_230_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_230_TextBox.Location = new System.Drawing.Point(116, 645);
            this.day_230_TextBox.Name = "day_230_TextBox";
            this.day_230_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_230_TextBox.TabIndex = 89;
            this.day_230_TextBox.Text = "0";
            this.day_230_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label46.Location = new System.Drawing.Point(41, 648);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(54, 22);
            this.label46.TabIndex = 88;
            this.label46.Text = "23:00";
            // 
            // day_223_TextBox
            // 
            this.day_223_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_223_TextBox.Location = new System.Drawing.Point(394, 617);
            this.day_223_TextBox.Name = "day_223_TextBox";
            this.day_223_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_223_TextBox.TabIndex = 87;
            this.day_223_TextBox.Text = "0";
            this.day_223_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(319, 620);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 22);
            this.label43.TabIndex = 86;
            this.label43.Text = "22:30";
            // 
            // day_220_TextBox
            // 
            this.day_220_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_220_TextBox.Location = new System.Drawing.Point(116, 617);
            this.day_220_TextBox.Name = "day_220_TextBox";
            this.day_220_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_220_TextBox.TabIndex = 85;
            this.day_220_TextBox.Text = "0";
            this.day_220_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label44.Location = new System.Drawing.Point(41, 620);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(54, 22);
            this.label44.TabIndex = 84;
            this.label44.Text = "22:00";
            // 
            // day_213_TextBox
            // 
            this.day_213_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_213_TextBox.Location = new System.Drawing.Point(394, 589);
            this.day_213_TextBox.Name = "day_213_TextBox";
            this.day_213_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_213_TextBox.TabIndex = 83;
            this.day_213_TextBox.Text = "0";
            this.day_213_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(319, 592);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(54, 22);
            this.label41.TabIndex = 82;
            this.label41.Text = "21:30";
            // 
            // day_210_TextBox
            // 
            this.day_210_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_210_TextBox.Location = new System.Drawing.Point(116, 589);
            this.day_210_TextBox.Name = "day_210_TextBox";
            this.day_210_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_210_TextBox.TabIndex = 81;
            this.day_210_TextBox.Text = "0";
            this.day_210_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label42.Location = new System.Drawing.Point(41, 592);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(54, 22);
            this.label42.TabIndex = 80;
            this.label42.Text = "21:00";
            // 
            // day_203_TextBox
            // 
            this.day_203_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_203_TextBox.Location = new System.Drawing.Point(394, 561);
            this.day_203_TextBox.Name = "day_203_TextBox";
            this.day_203_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_203_TextBox.TabIndex = 79;
            this.day_203_TextBox.Text = "0";
            this.day_203_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(319, 564);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 22);
            this.label39.TabIndex = 78;
            this.label39.Text = "20:30";
            // 
            // day_200_TextBox
            // 
            this.day_200_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_200_TextBox.Location = new System.Drawing.Point(116, 561);
            this.day_200_TextBox.Name = "day_200_TextBox";
            this.day_200_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_200_TextBox.TabIndex = 77;
            this.day_200_TextBox.Text = "0";
            this.day_200_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label40.Location = new System.Drawing.Point(41, 564);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 22);
            this.label40.TabIndex = 76;
            this.label40.Text = "20:00";
            // 
            // day_193_TextBox
            // 
            this.day_193_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_193_TextBox.Location = new System.Drawing.Point(394, 533);
            this.day_193_TextBox.Name = "day_193_TextBox";
            this.day_193_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_193_TextBox.TabIndex = 75;
            this.day_193_TextBox.Text = "0";
            this.day_193_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(319, 536);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 22);
            this.label37.TabIndex = 74;
            this.label37.Text = "19:30";
            // 
            // day_190_TextBox
            // 
            this.day_190_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_190_TextBox.Location = new System.Drawing.Point(116, 533);
            this.day_190_TextBox.Name = "day_190_TextBox";
            this.day_190_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_190_TextBox.TabIndex = 73;
            this.day_190_TextBox.Text = "0";
            this.day_190_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label38.Location = new System.Drawing.Point(41, 536);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(54, 22);
            this.label38.TabIndex = 72;
            this.label38.Text = "19:00";
            // 
            // day_183_TextBox
            // 
            this.day_183_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_183_TextBox.Location = new System.Drawing.Point(394, 504);
            this.day_183_TextBox.Name = "day_183_TextBox";
            this.day_183_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_183_TextBox.TabIndex = 71;
            this.day_183_TextBox.Text = "0";
            this.day_183_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(319, 507);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 22);
            this.label35.TabIndex = 70;
            this.label35.Text = "18:30";
            // 
            // day_180_TextBox
            // 
            this.day_180_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_180_TextBox.Location = new System.Drawing.Point(116, 504);
            this.day_180_TextBox.Name = "day_180_TextBox";
            this.day_180_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_180_TextBox.TabIndex = 69;
            this.day_180_TextBox.Text = "0";
            this.day_180_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label36.Location = new System.Drawing.Point(41, 507);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(54, 22);
            this.label36.TabIndex = 68;
            this.label36.Text = "18:00";
            // 
            // day_173_TextBox
            // 
            this.day_173_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_173_TextBox.Location = new System.Drawing.Point(394, 476);
            this.day_173_TextBox.Name = "day_173_TextBox";
            this.day_173_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_173_TextBox.TabIndex = 67;
            this.day_173_TextBox.Text = "0";
            this.day_173_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(319, 479);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 22);
            this.label33.TabIndex = 66;
            this.label33.Text = "17:30";
            // 
            // day_170_TextBox
            // 
            this.day_170_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_170_TextBox.Location = new System.Drawing.Point(116, 476);
            this.day_170_TextBox.Name = "day_170_TextBox";
            this.day_170_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_170_TextBox.TabIndex = 65;
            this.day_170_TextBox.Text = "0";
            this.day_170_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label34.Location = new System.Drawing.Point(41, 479);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 22);
            this.label34.TabIndex = 64;
            this.label34.Text = "17:00";
            // 
            // day_163_TextBox
            // 
            this.day_163_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_163_TextBox.Location = new System.Drawing.Point(394, 448);
            this.day_163_TextBox.Name = "day_163_TextBox";
            this.day_163_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_163_TextBox.TabIndex = 63;
            this.day_163_TextBox.Text = "607.5";
            this.day_163_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(319, 451);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(54, 22);
            this.label31.TabIndex = 62;
            this.label31.Text = "16:30";
            // 
            // day_160_TextBox
            // 
            this.day_160_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_160_TextBox.Location = new System.Drawing.Point(116, 448);
            this.day_160_TextBox.Name = "day_160_TextBox";
            this.day_160_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_160_TextBox.TabIndex = 61;
            this.day_160_TextBox.Text = "641.25";
            this.day_160_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label32.Location = new System.Drawing.Point(41, 451);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 22);
            this.label32.TabIndex = 60;
            this.label32.Text = "16:00";
            // 
            // day_153_TextBox
            // 
            this.day_153_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_153_TextBox.Location = new System.Drawing.Point(394, 420);
            this.day_153_TextBox.Name = "day_153_TextBox";
            this.day_153_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_153_TextBox.TabIndex = 59;
            this.day_153_TextBox.Text = "675";
            this.day_153_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(319, 423);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 22);
            this.label29.TabIndex = 58;
            this.label29.Text = "15:30";
            // 
            // day_150_TextBox
            // 
            this.day_150_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_150_TextBox.Location = new System.Drawing.Point(116, 420);
            this.day_150_TextBox.Name = "day_150_TextBox";
            this.day_150_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_150_TextBox.TabIndex = 57;
            this.day_150_TextBox.Text = "675";
            this.day_150_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label30.Location = new System.Drawing.Point(41, 423);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(54, 22);
            this.label30.TabIndex = 56;
            this.label30.Text = "15:00";
            // 
            // day_143_TextBox
            // 
            this.day_143_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_143_TextBox.Location = new System.Drawing.Point(394, 392);
            this.day_143_TextBox.Name = "day_143_TextBox";
            this.day_143_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_143_TextBox.TabIndex = 55;
            this.day_143_TextBox.Text = "675";
            this.day_143_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(319, 395);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 22);
            this.label27.TabIndex = 54;
            this.label27.Text = "14:30";
            // 
            // day_140_TextBox
            // 
            this.day_140_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_140_TextBox.Location = new System.Drawing.Point(116, 392);
            this.day_140_TextBox.Name = "day_140_TextBox";
            this.day_140_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_140_TextBox.TabIndex = 53;
            this.day_140_TextBox.Text = "675";
            this.day_140_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label28.Location = new System.Drawing.Point(41, 395);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 22);
            this.label28.TabIndex = 52;
            this.label28.Text = "14:00";
            // 
            // day_133_TextBox
            // 
            this.day_133_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_133_TextBox.Location = new System.Drawing.Point(394, 364);
            this.day_133_TextBox.Name = "day_133_TextBox";
            this.day_133_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_133_TextBox.TabIndex = 51;
            this.day_133_TextBox.Text = "641.25";
            this.day_133_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(319, 367);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 22);
            this.label25.TabIndex = 50;
            this.label25.Text = "13:30";
            // 
            // day_130_TextBox
            // 
            this.day_130_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_130_TextBox.Location = new System.Drawing.Point(116, 364);
            this.day_130_TextBox.Name = "day_130_TextBox";
            this.day_130_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_130_TextBox.TabIndex = 49;
            this.day_130_TextBox.Text = "607.5";
            this.day_130_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label26.Location = new System.Drawing.Point(41, 367);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 22);
            this.label26.TabIndex = 48;
            this.label26.Text = "13:00";
            // 
            // day_123_TextBox
            // 
            this.day_123_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_123_TextBox.Location = new System.Drawing.Point(394, 336);
            this.day_123_TextBox.Name = "day_123_TextBox";
            this.day_123_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_123_TextBox.TabIndex = 47;
            this.day_123_TextBox.Text = "573.75";
            this.day_123_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(319, 339);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 22);
            this.label23.TabIndex = 46;
            this.label23.Text = "12:30";
            // 
            // day_120_TextBox
            // 
            this.day_120_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_120_TextBox.Location = new System.Drawing.Point(116, 336);
            this.day_120_TextBox.Name = "day_120_TextBox";
            this.day_120_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_120_TextBox.TabIndex = 45;
            this.day_120_TextBox.Text = "573.75";
            this.day_120_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label24.Location = new System.Drawing.Point(41, 339);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 22);
            this.label24.TabIndex = 44;
            this.label24.Text = "12:00";
            // 
            // day_113_TextBox
            // 
            this.day_113_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_113_TextBox.Location = new System.Drawing.Point(394, 308);
            this.day_113_TextBox.Name = "day_113_TextBox";
            this.day_113_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_113_TextBox.TabIndex = 43;
            this.day_113_TextBox.Text = "540";
            this.day_113_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(319, 311);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 22);
            this.label21.TabIndex = 42;
            this.label21.Text = "11:30";
            // 
            // day_110_TextBox
            // 
            this.day_110_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_110_TextBox.Location = new System.Drawing.Point(116, 308);
            this.day_110_TextBox.Name = "day_110_TextBox";
            this.day_110_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_110_TextBox.TabIndex = 41;
            this.day_110_TextBox.Text = "573.75";
            this.day_110_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label22.Location = new System.Drawing.Point(41, 311);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 22);
            this.label22.TabIndex = 40;
            this.label22.Text = "11:00";
            // 
            // day_103_TextBox
            // 
            this.day_103_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_103_TextBox.Location = new System.Drawing.Point(394, 280);
            this.day_103_TextBox.Name = "day_103_TextBox";
            this.day_103_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_103_TextBox.TabIndex = 39;
            this.day_103_TextBox.Text = "607.5";
            this.day_103_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(319, 283);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 22);
            this.label19.TabIndex = 38;
            this.label19.Text = "10:30";
            // 
            // day_100_TextBox
            // 
            this.day_100_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_100_TextBox.Location = new System.Drawing.Point(116, 280);
            this.day_100_TextBox.Name = "day_100_TextBox";
            this.day_100_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_100_TextBox.TabIndex = 37;
            this.day_100_TextBox.Text = "641.25";
            this.day_100_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label20.Location = new System.Drawing.Point(41, 283);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 22);
            this.label20.TabIndex = 36;
            this.label20.Text = "10:00";
            // 
            // day_093_TextBox
            // 
            this.day_093_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_093_TextBox.Location = new System.Drawing.Point(394, 252);
            this.day_093_TextBox.Name = "day_093_TextBox";
            this.day_093_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_093_TextBox.TabIndex = 35;
            this.day_093_TextBox.Text = "675";
            this.day_093_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(319, 255);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 22);
            this.label17.TabIndex = 34;
            this.label17.Text = "09:30";
            // 
            // day_090_TextBox
            // 
            this.day_090_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_090_TextBox.Location = new System.Drawing.Point(116, 252);
            this.day_090_TextBox.Name = "day_090_TextBox";
            this.day_090_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_090_TextBox.TabIndex = 33;
            this.day_090_TextBox.Text = "675";
            this.day_090_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label18.Location = new System.Drawing.Point(41, 255);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 22);
            this.label18.TabIndex = 32;
            this.label18.Text = "09:00";
            // 
            // day_083_TextBox
            // 
            this.day_083_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_083_TextBox.Location = new System.Drawing.Point(394, 224);
            this.day_083_TextBox.Name = "day_083_TextBox";
            this.day_083_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_083_TextBox.TabIndex = 31;
            this.day_083_TextBox.Text = "641.25";
            this.day_083_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(319, 227);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 22);
            this.label15.TabIndex = 30;
            this.label15.Text = "08:30";
            // 
            // day_080_TextBox
            // 
            this.day_080_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_080_TextBox.Location = new System.Drawing.Point(116, 224);
            this.day_080_TextBox.Name = "day_080_TextBox";
            this.day_080_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_080_TextBox.TabIndex = 29;
            this.day_080_TextBox.Text = "641.25";
            this.day_080_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label16.Location = new System.Drawing.Point(41, 227);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 22);
            this.label16.TabIndex = 28;
            this.label16.Text = "08:00";
            // 
            // day_073_TextBox
            // 
            this.day_073_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_073_TextBox.Location = new System.Drawing.Point(394, 196);
            this.day_073_TextBox.Name = "day_073_TextBox";
            this.day_073_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_073_TextBox.TabIndex = 27;
            this.day_073_TextBox.Text = "0";
            this.day_073_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(319, 199);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 22);
            this.label13.TabIndex = 26;
            this.label13.Text = "07:30";
            // 
            // day_070_TextBox
            // 
            this.day_070_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_070_TextBox.Location = new System.Drawing.Point(116, 196);
            this.day_070_TextBox.Name = "day_070_TextBox";
            this.day_070_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_070_TextBox.TabIndex = 25;
            this.day_070_TextBox.Text = "0";
            this.day_070_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label14.Location = new System.Drawing.Point(41, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 22);
            this.label14.TabIndex = 24;
            this.label14.Text = "07:00";
            // 
            // day_063_TextBox
            // 
            this.day_063_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_063_TextBox.Location = new System.Drawing.Point(394, 168);
            this.day_063_TextBox.Name = "day_063_TextBox";
            this.day_063_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_063_TextBox.TabIndex = 23;
            this.day_063_TextBox.Text = "0";
            this.day_063_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(319, 171);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 22);
            this.label11.TabIndex = 22;
            this.label11.Text = "06:30";
            // 
            // day_060_TextBox
            // 
            this.day_060_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_060_TextBox.Location = new System.Drawing.Point(116, 168);
            this.day_060_TextBox.Name = "day_060_TextBox";
            this.day_060_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_060_TextBox.TabIndex = 21;
            this.day_060_TextBox.Text = "0";
            this.day_060_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label12.Location = new System.Drawing.Point(41, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 22);
            this.label12.TabIndex = 20;
            this.label12.Text = "06:00";
            // 
            // day_053_TextBox
            // 
            this.day_053_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_053_TextBox.Location = new System.Drawing.Point(394, 140);
            this.day_053_TextBox.Name = "day_053_TextBox";
            this.day_053_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_053_TextBox.TabIndex = 19;
            this.day_053_TextBox.Text = "0";
            this.day_053_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(319, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 22);
            this.label9.TabIndex = 18;
            this.label9.Text = "05:30";
            // 
            // day_050_TextBox
            // 
            this.day_050_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_050_TextBox.Location = new System.Drawing.Point(116, 140);
            this.day_050_TextBox.Name = "day_050_TextBox";
            this.day_050_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_050_TextBox.TabIndex = 17;
            this.day_050_TextBox.Text = "0";
            this.day_050_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label10.Location = new System.Drawing.Point(41, 143);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 22);
            this.label10.TabIndex = 16;
            this.label10.Text = "05:00";
            // 
            // day_043_TextBox
            // 
            this.day_043_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_043_TextBox.Location = new System.Drawing.Point(394, 113);
            this.day_043_TextBox.Name = "day_043_TextBox";
            this.day_043_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_043_TextBox.TabIndex = 15;
            this.day_043_TextBox.Text = "0";
            this.day_043_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(319, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 22);
            this.label7.TabIndex = 14;
            this.label7.Text = "04:30";
            // 
            // day_040_TextBox
            // 
            this.day_040_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_040_TextBox.Location = new System.Drawing.Point(116, 113);
            this.day_040_TextBox.Name = "day_040_TextBox";
            this.day_040_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_040_TextBox.TabIndex = 13;
            this.day_040_TextBox.Text = "0";
            this.day_040_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label8.Location = new System.Drawing.Point(41, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 22);
            this.label8.TabIndex = 12;
            this.label8.Text = "04:00";
            // 
            // day_033_TextBox
            // 
            this.day_033_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_033_TextBox.Location = new System.Drawing.Point(394, 85);
            this.day_033_TextBox.Name = "day_033_TextBox";
            this.day_033_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_033_TextBox.TabIndex = 11;
            this.day_033_TextBox.Text = "0";
            this.day_033_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(319, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 22);
            this.label5.TabIndex = 10;
            this.label5.Text = "03:30";
            // 
            // day_030_TextBox
            // 
            this.day_030_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_030_TextBox.Location = new System.Drawing.Point(116, 85);
            this.day_030_TextBox.Name = "day_030_TextBox";
            this.day_030_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_030_TextBox.TabIndex = 9;
            this.day_030_TextBox.Text = "0";
            this.day_030_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label6.Location = new System.Drawing.Point(41, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 22);
            this.label6.TabIndex = 8;
            this.label6.Text = "03:00";
            // 
            // day_023_TextBox
            // 
            this.day_023_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_023_TextBox.Location = new System.Drawing.Point(394, 57);
            this.day_023_TextBox.Name = "day_023_TextBox";
            this.day_023_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_023_TextBox.TabIndex = 7;
            this.day_023_TextBox.Text = "0";
            this.day_023_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(319, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 22);
            this.label3.TabIndex = 6;
            this.label3.Text = "02:30";
            // 
            // day_020_TextBox
            // 
            this.day_020_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_020_TextBox.Location = new System.Drawing.Point(116, 57);
            this.day_020_TextBox.Name = "day_020_TextBox";
            this.day_020_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_020_TextBox.TabIndex = 5;
            this.day_020_TextBox.Text = "0";
            this.day_020_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label4.Location = new System.Drawing.Point(41, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 22);
            this.label4.TabIndex = 4;
            this.label4.Text = "02:00";
            // 
            // day_013_TextBox
            // 
            this.day_013_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_013_TextBox.Location = new System.Drawing.Point(394, 29);
            this.day_013_TextBox.Name = "day_013_TextBox";
            this.day_013_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_013_TextBox.TabIndex = 3;
            this.day_013_TextBox.Text = "0";
            this.day_013_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(319, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "01:30";
            // 
            // day_010_TextBox
            // 
            this.day_010_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.day_010_TextBox.Location = new System.Drawing.Point(116, 29);
            this.day_010_TextBox.Name = "day_010_TextBox";
            this.day_010_TextBox.Size = new System.Drawing.Size(172, 30);
            this.day_010_TextBox.TabIndex = 1;
            this.day_010_TextBox.Text = "0";
            this.day_010_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label1.Location = new System.Drawing.Point(41, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "01:00";
            // 
            // monthlyLoadgroupBox
            // 
            this.monthlyLoadgroupBox.Controls.Add(this.label82);
            this.monthlyLoadgroupBox.Controls.Add(this.month12_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label83);
            this.monthlyLoadgroupBox.Controls.Add(this.month12_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label84);
            this.monthlyLoadgroupBox.Controls.Add(this.label79);
            this.monthlyLoadgroupBox.Controls.Add(this.month11_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label80);
            this.monthlyLoadgroupBox.Controls.Add(this.month11_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label81);
            this.monthlyLoadgroupBox.Controls.Add(this.label76);
            this.monthlyLoadgroupBox.Controls.Add(this.month10_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label77);
            this.monthlyLoadgroupBox.Controls.Add(this.month10_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label78);
            this.monthlyLoadgroupBox.Controls.Add(this.label73);
            this.monthlyLoadgroupBox.Controls.Add(this.month9_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label74);
            this.monthlyLoadgroupBox.Controls.Add(this.month9_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label75);
            this.monthlyLoadgroupBox.Controls.Add(this.label70);
            this.monthlyLoadgroupBox.Controls.Add(this.month8_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label71);
            this.monthlyLoadgroupBox.Controls.Add(this.month8_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label72);
            this.monthlyLoadgroupBox.Controls.Add(this.label67);
            this.monthlyLoadgroupBox.Controls.Add(this.month7_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label68);
            this.monthlyLoadgroupBox.Controls.Add(this.month7_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label69);
            this.monthlyLoadgroupBox.Controls.Add(this.label64);
            this.monthlyLoadgroupBox.Controls.Add(this.month6_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label65);
            this.monthlyLoadgroupBox.Controls.Add(this.month6_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label66);
            this.monthlyLoadgroupBox.Controls.Add(this.label61);
            this.monthlyLoadgroupBox.Controls.Add(this.month5_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label62);
            this.monthlyLoadgroupBox.Controls.Add(this.month5_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label63);
            this.monthlyLoadgroupBox.Controls.Add(this.label58);
            this.monthlyLoadgroupBox.Controls.Add(this.month4_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label59);
            this.monthlyLoadgroupBox.Controls.Add(this.month4_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label60);
            this.monthlyLoadgroupBox.Controls.Add(this.label55);
            this.monthlyLoadgroupBox.Controls.Add(this.month3_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label56);
            this.monthlyLoadgroupBox.Controls.Add(this.month3_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label57);
            this.monthlyLoadgroupBox.Controls.Add(this.label52);
            this.monthlyLoadgroupBox.Controls.Add(this.month2_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label53);
            this.monthlyLoadgroupBox.Controls.Add(this.month2_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label54);
            this.monthlyLoadgroupBox.Controls.Add(this.label51);
            this.monthlyLoadgroupBox.Controls.Add(this.month1_Workday_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label50);
            this.monthlyLoadgroupBox.Controls.Add(this.month1_Scale_TextBox);
            this.monthlyLoadgroupBox.Controls.Add(this.label49);
            this.monthlyLoadgroupBox.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.monthlyLoadgroupBox.Location = new System.Drawing.Point(834, 80);
            this.monthlyLoadgroupBox.Name = "monthlyLoadgroupBox";
            this.monthlyLoadgroupBox.Size = new System.Drawing.Size(615, 564);
            this.monthlyLoadgroupBox.TabIndex = 2;
            this.monthlyLoadgroupBox.TabStop = false;
            this.monthlyLoadgroupBox.Text = "月負荷比例設計及工作天數";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label82.Location = new System.Drawing.Point(549, 491);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(34, 28);
            this.label82.TabIndex = 59;
            this.label82.Text = "天";
            // 
            // month12_Workday_TextBox
            // 
            this.month12_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month12_Workday_TextBox.Location = new System.Drawing.Point(356, 491);
            this.month12_Workday_TextBox.Name = "month12_Workday_TextBox";
            this.month12_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month12_Workday_TextBox.TabIndex = 58;
            this.month12_Workday_TextBox.Text = "26";
            this.month12_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label83.Location = new System.Drawing.Point(307, 491);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(31, 28);
            this.label83.TabIndex = 57;
            this.label83.Text = "%";
            // 
            // month12_Scale_TextBox
            // 
            this.month12_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month12_Scale_TextBox.Location = new System.Drawing.Point(114, 491);
            this.month12_Scale_TextBox.Name = "month12_Scale_TextBox";
            this.month12_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month12_Scale_TextBox.TabIndex = 56;
            this.month12_Scale_TextBox.Text = "20";
            this.month12_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label84.Location = new System.Drawing.Point(48, 491);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(60, 28);
            this.label84.TabIndex = 55;
            this.label84.Text = "12月";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label79.Location = new System.Drawing.Point(549, 451);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(34, 28);
            this.label79.TabIndex = 54;
            this.label79.Text = "天";
            // 
            // month11_Workday_TextBox
            // 
            this.month11_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month11_Workday_TextBox.Location = new System.Drawing.Point(356, 451);
            this.month11_Workday_TextBox.Name = "month11_Workday_TextBox";
            this.month11_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month11_Workday_TextBox.TabIndex = 53;
            this.month11_Workday_TextBox.Text = "25";
            this.month11_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label80.Location = new System.Drawing.Point(307, 451);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(31, 28);
            this.label80.TabIndex = 52;
            this.label80.Text = "%";
            // 
            // month11_Scale_TextBox
            // 
            this.month11_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month11_Scale_TextBox.Location = new System.Drawing.Point(114, 451);
            this.month11_Scale_TextBox.Name = "month11_Scale_TextBox";
            this.month11_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month11_Scale_TextBox.TabIndex = 51;
            this.month11_Scale_TextBox.Text = "30";
            this.month11_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label81.Location = new System.Drawing.Point(48, 451);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(60, 28);
            this.label81.TabIndex = 50;
            this.label81.Text = "11月";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label76.Location = new System.Drawing.Point(549, 411);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(34, 28);
            this.label76.TabIndex = 49;
            this.label76.Text = "天";
            // 
            // month10_Workday_TextBox
            // 
            this.month10_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month10_Workday_TextBox.Location = new System.Drawing.Point(356, 411);
            this.month10_Workday_TextBox.Name = "month10_Workday_TextBox";
            this.month10_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month10_Workday_TextBox.TabIndex = 48;
            this.month10_Workday_TextBox.Text = "26";
            this.month10_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label77.Location = new System.Drawing.Point(307, 411);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(31, 28);
            this.label77.TabIndex = 47;
            this.label77.Text = "%";
            // 
            // month10_Scale_TextBox
            // 
            this.month10_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month10_Scale_TextBox.Location = new System.Drawing.Point(114, 411);
            this.month10_Scale_TextBox.Name = "month10_Scale_TextBox";
            this.month10_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month10_Scale_TextBox.TabIndex = 46;
            this.month10_Scale_TextBox.Text = "70";
            this.month10_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label78.Location = new System.Drawing.Point(48, 411);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(60, 28);
            this.label78.TabIndex = 45;
            this.label78.Text = "10月";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label73.Location = new System.Drawing.Point(549, 371);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(34, 28);
            this.label73.TabIndex = 44;
            this.label73.Text = "天";
            // 
            // month9_Workday_TextBox
            // 
            this.month9_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month9_Workday_TextBox.Location = new System.Drawing.Point(356, 371);
            this.month9_Workday_TextBox.Name = "month9_Workday_TextBox";
            this.month9_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month9_Workday_TextBox.TabIndex = 43;
            this.month9_Workday_TextBox.Text = "25";
            this.month9_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label74.Location = new System.Drawing.Point(307, 371);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(31, 28);
            this.label74.TabIndex = 42;
            this.label74.Text = "%";
            // 
            // month9_Scale_TextBox
            // 
            this.month9_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month9_Scale_TextBox.Location = new System.Drawing.Point(114, 371);
            this.month9_Scale_TextBox.Name = "month9_Scale_TextBox";
            this.month9_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month9_Scale_TextBox.TabIndex = 41;
            this.month9_Scale_TextBox.Text = "100";
            this.month9_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label75.Location = new System.Drawing.Point(48, 371);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(47, 28);
            this.label75.TabIndex = 40;
            this.label75.Text = "9月";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label70.Location = new System.Drawing.Point(549, 331);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(34, 28);
            this.label70.TabIndex = 39;
            this.label70.Text = "天";
            // 
            // month8_Workday_TextBox
            // 
            this.month8_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month8_Workday_TextBox.Location = new System.Drawing.Point(356, 331);
            this.month8_Workday_TextBox.Name = "month8_Workday_TextBox";
            this.month8_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month8_Workday_TextBox.TabIndex = 38;
            this.month8_Workday_TextBox.Text = "26";
            this.month8_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label71.Location = new System.Drawing.Point(307, 331);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(31, 28);
            this.label71.TabIndex = 37;
            this.label71.Text = "%";
            // 
            // month8_Scale_TextBox
            // 
            this.month8_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month8_Scale_TextBox.Location = new System.Drawing.Point(114, 331);
            this.month8_Scale_TextBox.Name = "month8_Scale_TextBox";
            this.month8_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month8_Scale_TextBox.TabIndex = 36;
            this.month8_Scale_TextBox.Text = "100";
            this.month8_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label72.Location = new System.Drawing.Point(48, 331);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(47, 28);
            this.label72.TabIndex = 35;
            this.label72.Text = "8月";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label67.Location = new System.Drawing.Point(549, 291);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(34, 28);
            this.label67.TabIndex = 34;
            this.label67.Text = "天";
            // 
            // month7_Workday_TextBox
            // 
            this.month7_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month7_Workday_TextBox.Location = new System.Drawing.Point(356, 291);
            this.month7_Workday_TextBox.Name = "month7_Workday_TextBox";
            this.month7_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month7_Workday_TextBox.TabIndex = 33;
            this.month7_Workday_TextBox.Text = "26";
            this.month7_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label68.Location = new System.Drawing.Point(307, 291);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(31, 28);
            this.label68.TabIndex = 32;
            this.label68.Text = "%";
            // 
            // month7_Scale_TextBox
            // 
            this.month7_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month7_Scale_TextBox.Location = new System.Drawing.Point(114, 291);
            this.month7_Scale_TextBox.Name = "month7_Scale_TextBox";
            this.month7_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month7_Scale_TextBox.TabIndex = 31;
            this.month7_Scale_TextBox.Text = "100";
            this.month7_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label69.Location = new System.Drawing.Point(48, 291);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(47, 28);
            this.label69.TabIndex = 30;
            this.label69.Text = "7月";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label64.Location = new System.Drawing.Point(549, 251);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(34, 28);
            this.label64.TabIndex = 29;
            this.label64.Text = "天";
            // 
            // month6_Workday_TextBox
            // 
            this.month6_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month6_Workday_TextBox.Location = new System.Drawing.Point(356, 251);
            this.month6_Workday_TextBox.Name = "month6_Workday_TextBox";
            this.month6_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month6_Workday_TextBox.TabIndex = 28;
            this.month6_Workday_TextBox.Text = "25";
            this.month6_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label65.Location = new System.Drawing.Point(307, 251);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(31, 28);
            this.label65.TabIndex = 27;
            this.label65.Text = "%";
            // 
            // month6_Scale_TextBox
            // 
            this.month6_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month6_Scale_TextBox.Location = new System.Drawing.Point(114, 251);
            this.month6_Scale_TextBox.Name = "month6_Scale_TextBox";
            this.month6_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month6_Scale_TextBox.TabIndex = 26;
            this.month6_Scale_TextBox.Text = "90";
            this.month6_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label66.Location = new System.Drawing.Point(48, 251);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(47, 28);
            this.label66.TabIndex = 25;
            this.label66.Text = "6月";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label61.Location = new System.Drawing.Point(549, 211);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(34, 28);
            this.label61.TabIndex = 24;
            this.label61.Text = "天";
            // 
            // month5_Workday_TextBox
            // 
            this.month5_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month5_Workday_TextBox.Location = new System.Drawing.Point(356, 211);
            this.month5_Workday_TextBox.Name = "month5_Workday_TextBox";
            this.month5_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month5_Workday_TextBox.TabIndex = 23;
            this.month5_Workday_TextBox.Text = "26";
            this.month5_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label62.Location = new System.Drawing.Point(307, 211);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(31, 28);
            this.label62.TabIndex = 22;
            this.label62.Text = "%";
            // 
            // month5_Scale_TextBox
            // 
            this.month5_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month5_Scale_TextBox.Location = new System.Drawing.Point(114, 211);
            this.month5_Scale_TextBox.Name = "month5_Scale_TextBox";
            this.month5_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month5_Scale_TextBox.TabIndex = 21;
            this.month5_Scale_TextBox.Text = "60";
            this.month5_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label63.Location = new System.Drawing.Point(48, 211);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(47, 28);
            this.label63.TabIndex = 20;
            this.label63.Text = "5月";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label58.Location = new System.Drawing.Point(549, 171);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(34, 28);
            this.label58.TabIndex = 19;
            this.label58.Text = "天";
            // 
            // month4_Workday_TextBox
            // 
            this.month4_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month4_Workday_TextBox.Location = new System.Drawing.Point(356, 171);
            this.month4_Workday_TextBox.Name = "month4_Workday_TextBox";
            this.month4_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month4_Workday_TextBox.TabIndex = 18;
            this.month4_Workday_TextBox.Text = "25";
            this.month4_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label59.Location = new System.Drawing.Point(307, 171);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(31, 28);
            this.label59.TabIndex = 17;
            this.label59.Text = "%";
            // 
            // month4_Scale_TextBox
            // 
            this.month4_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month4_Scale_TextBox.Location = new System.Drawing.Point(114, 171);
            this.month4_Scale_TextBox.Name = "month4_Scale_TextBox";
            this.month4_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month4_Scale_TextBox.TabIndex = 16;
            this.month4_Scale_TextBox.Text = "30";
            this.month4_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label60.Location = new System.Drawing.Point(48, 171);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(47, 28);
            this.label60.TabIndex = 15;
            this.label60.Text = "4月";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label55.Location = new System.Drawing.Point(549, 131);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 28);
            this.label55.TabIndex = 14;
            this.label55.Text = "天";
            // 
            // month3_Workday_TextBox
            // 
            this.month3_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month3_Workday_TextBox.Location = new System.Drawing.Point(356, 131);
            this.month3_Workday_TextBox.Name = "month3_Workday_TextBox";
            this.month3_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month3_Workday_TextBox.TabIndex = 13;
            this.month3_Workday_TextBox.Text = "26";
            this.month3_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label56.Location = new System.Drawing.Point(307, 131);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(31, 28);
            this.label56.TabIndex = 12;
            this.label56.Text = "%";
            // 
            // month3_Scale_TextBox
            // 
            this.month3_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month3_Scale_TextBox.Location = new System.Drawing.Point(114, 131);
            this.month3_Scale_TextBox.Name = "month3_Scale_TextBox";
            this.month3_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month3_Scale_TextBox.TabIndex = 11;
            this.month3_Scale_TextBox.Text = "20";
            this.month3_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label57.Location = new System.Drawing.Point(48, 131);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(47, 28);
            this.label57.TabIndex = 10;
            this.label57.Text = "3月";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label52.Location = new System.Drawing.Point(549, 91);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 28);
            this.label52.TabIndex = 9;
            this.label52.Text = "天";
            // 
            // month2_Workday_TextBox
            // 
            this.month2_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month2_Workday_TextBox.Location = new System.Drawing.Point(356, 91);
            this.month2_Workday_TextBox.Name = "month2_Workday_TextBox";
            this.month2_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month2_Workday_TextBox.TabIndex = 8;
            this.month2_Workday_TextBox.Text = "16";
            this.month2_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label53.Location = new System.Drawing.Point(307, 91);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(31, 28);
            this.label53.TabIndex = 7;
            this.label53.Text = "%";
            // 
            // month2_Scale_TextBox
            // 
            this.month2_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month2_Scale_TextBox.Location = new System.Drawing.Point(114, 91);
            this.month2_Scale_TextBox.Name = "month2_Scale_TextBox";
            this.month2_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month2_Scale_TextBox.TabIndex = 6;
            this.month2_Scale_TextBox.Text = "10";
            this.month2_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label54.Location = new System.Drawing.Point(48, 91);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(47, 28);
            this.label54.TabIndex = 5;
            this.label54.Text = "2月";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label51.Location = new System.Drawing.Point(549, 51);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 28);
            this.label51.TabIndex = 4;
            this.label51.Text = "天";
            // 
            // month1_Workday_TextBox
            // 
            this.month1_Workday_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month1_Workday_TextBox.Location = new System.Drawing.Point(356, 51);
            this.month1_Workday_TextBox.Name = "month1_Workday_TextBox";
            this.month1_Workday_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month1_Workday_TextBox.TabIndex = 3;
            this.month1_Workday_TextBox.Text = "26";
            this.month1_Workday_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label50.Location = new System.Drawing.Point(307, 51);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(31, 28);
            this.label50.TabIndex = 2;
            this.label50.Text = "%";
            // 
            // month1_Scale_TextBox
            // 
            this.month1_Scale_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.month1_Scale_TextBox.Location = new System.Drawing.Point(114, 51);
            this.month1_Scale_TextBox.Name = "month1_Scale_TextBox";
            this.month1_Scale_TextBox.Size = new System.Drawing.Size(187, 30);
            this.month1_Scale_TextBox.TabIndex = 1;
            this.month1_Scale_TextBox.Text = "10";
            this.month1_Scale_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 13F);
            this.label49.Location = new System.Drawing.Point(48, 51);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(47, 28);
            this.label49.TabIndex = 0;
            this.label49.Text = "1月";
            // 
            // dayToDef_Button
            // 
            this.dayToDef_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dayToDef_Button.Location = new System.Drawing.Point(1136, 668);
            this.dayToDef_Button.Name = "dayToDef_Button";
            this.dayToDef_Button.Size = new System.Drawing.Size(161, 35);
            this.dayToDef_Button.TabIndex = 4;
            this.dayToDef_Button.Text = "全日負荷回復";
            this.dayToDef_Button.UseVisualStyleBackColor = true;
            this.dayToDef_Button.Click += new System.EventHandler(this.dayToDef_Button_Click);
            // 
            // scaleToDef_Button
            // 
            this.scaleToDef_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.scaleToDef_Button.Location = new System.Drawing.Point(1136, 709);
            this.scaleToDef_Button.Name = "scaleToDef_Button";
            this.scaleToDef_Button.Size = new System.Drawing.Size(161, 35);
            this.scaleToDef_Button.TabIndex = 5;
            this.scaleToDef_Button.Text = "月負荷比例回復";
            this.scaleToDef_Button.UseVisualStyleBackColor = true;
            this.scaleToDef_Button.Click += new System.EventHandler(this.scaleToDef_Button_Click);
            // 
            // workdayToDef_Button
            // 
            this.workdayToDef_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.workdayToDef_Button.Location = new System.Drawing.Point(1136, 750);
            this.workdayToDef_Button.Name = "workdayToDef_Button";
            this.workdayToDef_Button.Size = new System.Drawing.Size(161, 35);
            this.workdayToDef_Button.TabIndex = 6;
            this.workdayToDef_Button.Text = "月工作天數回復";
            this.workdayToDef_Button.UseVisualStyleBackColor = true;
            this.workdayToDef_Button.Click += new System.EventHandler(this.workdayToDef_Button_Click);
            // 
            // project_Design_Ok_Button
            // 
            this.project_Design_Ok_Button.Font = new System.Drawing.Font("微軟正黑體", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.project_Design_Ok_Button.Location = new System.Drawing.Point(1324, 663);
            this.project_Design_Ok_Button.Name = "project_Design_Ok_Button";
            this.project_Design_Ok_Button.Size = new System.Drawing.Size(120, 120);
            this.project_Design_Ok_Button.TabIndex = 7;
            this.project_Design_Ok_Button.Text = "確定";
            this.project_Design_Ok_Button.UseVisualStyleBackColor = true;
            this.project_Design_Ok_Button.Click += new System.EventHandler(this.project_Design_Ok_Button_Click);
            // 
            // peakLoadGroupBox
            // 
            this.peakLoadGroupBox.Controls.Add(this.peak_Load_TextBox);
            this.peakLoadGroupBox.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.peakLoadGroupBox.Location = new System.Drawing.Point(646, 79);
            this.peakLoadGroupBox.Name = "peakLoadGroupBox";
            this.peakLoadGroupBox.Size = new System.Drawing.Size(182, 110);
            this.peakLoadGroupBox.TabIndex = 8;
            this.peakLoadGroupBox.TabStop = false;
            this.peakLoadGroupBox.Text = "尖峰負荷RT/HR";
            // 
            // peak_Load_TextBox
            // 
            this.peak_Load_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.peak_Load_TextBox.Location = new System.Drawing.Point(22, 51);
            this.peak_Load_TextBox.Name = "peak_Load_TextBox";
            this.peak_Load_TextBox.Size = new System.Drawing.Size(139, 34);
            this.peak_Load_TextBox.TabIndex = 0;
            this.peak_Load_TextBox.Text = "1350";
            this.peak_Load_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.peak_Load_TextBox.Leave += new System.EventHandler(this.peak_Load_TextBox_Leave);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.hr15_Button);
            this.groupBox2.Controls.Add(this.label89);
            this.groupBox2.Controls.Add(this.hr14_Button);
            this.groupBox2.Controls.Add(this.label88);
            this.groupBox2.Controls.Add(this.hr11_Button);
            this.groupBox2.Controls.Add(this.label87);
            this.groupBox2.Controls.Add(this.hr10_Button);
            this.groupBox2.Controls.Add(this.label86);
            this.groupBox2.Controls.Add(this.hr9_Button);
            this.groupBox2.Controls.Add(this.label85);
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.Location = new System.Drawing.Point(646, 200);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(182, 444);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "空調運轉時數";
            // 
            // hr15_Button
            // 
            this.hr15_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.hr15_Button.Location = new System.Drawing.Point(22, 381);
            this.hr15_Button.Name = "hr15_Button";
            this.hr15_Button.Size = new System.Drawing.Size(139, 36);
            this.hr15_Button.TabIndex = 11;
            this.hr15_Button.Text = "15 hr";
            this.hr15_Button.UseVisualStyleBackColor = true;
            this.hr15_Button.Click += new System.EventHandler(this.hr15_Button_Click);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label89.Location = new System.Drawing.Point(30, 354);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(122, 24);
            this.label89.TabIndex = 10;
            this.label89.Text = "07:30~22:30";
            // 
            // hr14_Button
            // 
            this.hr14_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.hr14_Button.Location = new System.Drawing.Point(22, 301);
            this.hr14_Button.Name = "hr14_Button";
            this.hr14_Button.Size = new System.Drawing.Size(139, 36);
            this.hr14_Button.TabIndex = 7;
            this.hr14_Button.Text = "14 hr";
            this.hr14_Button.UseVisualStyleBackColor = true;
            this.hr14_Button.Click += new System.EventHandler(this.hr14_Button_Click);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label88.Location = new System.Drawing.Point(30, 274);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(122, 24);
            this.label88.TabIndex = 6;
            this.label88.Text = "07:30~21:30";
            // 
            // hr11_Button
            // 
            this.hr11_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.hr11_Button.Location = new System.Drawing.Point(22, 221);
            this.hr11_Button.Name = "hr11_Button";
            this.hr11_Button.Size = new System.Drawing.Size(139, 36);
            this.hr11_Button.TabIndex = 5;
            this.hr11_Button.Text = "11 hr";
            this.hr11_Button.UseVisualStyleBackColor = true;
            this.hr11_Button.Click += new System.EventHandler(this.hr11_Button_Click);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label87.Location = new System.Drawing.Point(30, 194);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(122, 24);
            this.label87.TabIndex = 4;
            this.label87.Text = "07:30~18:30";
            // 
            // hr10_Button
            // 
            this.hr10_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.hr10_Button.Location = new System.Drawing.Point(22, 141);
            this.hr10_Button.Name = "hr10_Button";
            this.hr10_Button.Size = new System.Drawing.Size(139, 36);
            this.hr10_Button.TabIndex = 3;
            this.hr10_Button.Text = "10 hr";
            this.hr10_Button.UseVisualStyleBackColor = true;
            this.hr10_Button.Click += new System.EventHandler(this.hr10_Button_Click);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label86.Location = new System.Drawing.Point(30, 114);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(122, 24);
            this.label86.TabIndex = 2;
            this.label86.Text = "07:30~17:30";
            // 
            // hr9_Button
            // 
            this.hr9_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.hr9_Button.Location = new System.Drawing.Point(22, 61);
            this.hr9_Button.Name = "hr9_Button";
            this.hr9_Button.Size = new System.Drawing.Size(139, 36);
            this.hr9_Button.TabIndex = 1;
            this.hr9_Button.Text = "9 hr";
            this.hr9_Button.UseVisualStyleBackColor = true;
            this.hr9_Button.Click += new System.EventHandler(this.hr9_Button_Click);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label85.Location = new System.Drawing.Point(30, 34);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(122, 24);
            this.label85.TabIndex = 0;
            this.label85.Text = "08:00~17:00";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.factory_Button);
            this.groupBox3.Controls.Add(this.office_Button);
            this.groupBox3.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox3.Location = new System.Drawing.Point(646, 657);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(231, 140);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "月負荷比例";
            // 
            // factory_Button
            // 
            this.factory_Button.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.factory_Button.Location = new System.Drawing.Point(34, 87);
            this.factory_Button.Name = "factory_Button";
            this.factory_Button.Size = new System.Drawing.Size(161, 35);
            this.factory_Button.TabIndex = 6;
            this.factory_Button.Text = "工廠";
            this.factory_Button.UseVisualStyleBackColor = true;
            this.factory_Button.Click += new System.EventHandler(this.factory_Button_Click);
            // 
            // office_Button
            // 
            this.office_Button.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.office_Button.Location = new System.Drawing.Point(34, 39);
            this.office_Button.Name = "office_Button";
            this.office_Button.Size = new System.Drawing.Size(161, 35);
            this.office_Button.TabIndex = 5;
            this.office_Button.Text = "辦公室";
            this.office_Button.UseVisualStyleBackColor = true;
            this.office_Button.Click += new System.EventHandler(this.office_Button_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.day31_Button);
            this.groupBox4.Controls.Add(this.day20_Button);
            this.groupBox4.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox4.Location = new System.Drawing.Point(887, 657);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(231, 140);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "工作天數";
            // 
            // day31_Button
            // 
            this.day31_Button.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.day31_Button.Location = new System.Drawing.Point(35, 87);
            this.day31_Button.Name = "day31_Button";
            this.day31_Button.Size = new System.Drawing.Size(161, 35);
            this.day31_Button.TabIndex = 8;
            this.day31_Button.Text = "31日";
            this.day31_Button.UseVisualStyleBackColor = true;
            this.day31_Button.Click += new System.EventHandler(this.day31_Button_Click);
            // 
            // day20_Button
            // 
            this.day20_Button.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.day20_Button.Location = new System.Drawing.Point(35, 39);
            this.day20_Button.Name = "day20_Button";
            this.day20_Button.Size = new System.Drawing.Size(161, 35);
            this.day20_Button.TabIndex = 7;
            this.day20_Button.Text = "20日";
            this.day20_Button.UseVisualStyleBackColor = true;
            this.day20_Button.Click += new System.EventHandler(this.day20_Button_Click);
            // 
            // Form_Project_Design_Load
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1461, 809);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.peakLoadGroupBox);
            this.Controls.Add(this.project_Design_Ok_Button);
            this.Controls.Add(this.workdayToDef_Button);
            this.Controls.Add(this.scaleToDef_Button);
            this.Controls.Add(this.dayToDef_Button);
            this.Controls.Add(this.monthlyLoadgroupBox);
            this.Controls.Add(this.fullDayLoadDesignSamplingGroupBox);
            this.Controls.Add(this.projectNameGroupBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Project_Design_Load";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "專案設計負荷";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Project_Design_Load_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Project_Design_Load_Closed);
            this.projectNameGroupBox.ResumeLayout(false);
            this.projectNameGroupBox.PerformLayout();
            this.fullDayLoadDesignSamplingGroupBox.ResumeLayout(false);
            this.fullDayLoadDesignSamplingGroupBox.PerformLayout();
            this.monthlyLoadgroupBox.ResumeLayout(false);
            this.monthlyLoadgroupBox.PerformLayout();
            this.peakLoadGroupBox.ResumeLayout(false);
            this.peakLoadGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox projectNameGroupBox;
        private System.Windows.Forms.TextBox projectName_TextBox;
        private System.Windows.Forms.GroupBox fullDayLoadDesignSamplingGroupBox;
        private System.Windows.Forms.GroupBox monthlyLoadgroupBox;
        private System.Windows.Forms.Button dayToDef_Button;
        private System.Windows.Forms.Button scaleToDef_Button;
        private System.Windows.Forms.Button workdayToDef_Button;
        private System.Windows.Forms.Button project_Design_Ok_Button;
        private System.Windows.Forms.TextBox day_003_TextBox;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox day_240_TextBox;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox day_233_TextBox;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox day_230_TextBox;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox day_223_TextBox;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox day_220_TextBox;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox day_213_TextBox;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox day_210_TextBox;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox day_203_TextBox;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox day_200_TextBox;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox day_193_TextBox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox day_190_TextBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox day_183_TextBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox day_180_TextBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox day_173_TextBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox day_170_TextBox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox day_163_TextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox day_160_TextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox day_153_TextBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox day_150_TextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox day_143_TextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox day_140_TextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox day_133_TextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox day_130_TextBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox day_123_TextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox day_120_TextBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox day_113_TextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox day_110_TextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox day_103_TextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox day_100_TextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox day_093_TextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox day_090_TextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox day_083_TextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox day_080_TextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox day_073_TextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox day_070_TextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox day_063_TextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox day_060_TextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox day_053_TextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox day_050_TextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox day_043_TextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox day_040_TextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox day_033_TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox day_030_TextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox day_023_TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox day_020_TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox day_013_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox day_010_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox month12_Workday_TextBox;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox month12_Scale_TextBox;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox month11_Workday_TextBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox month11_Scale_TextBox;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox month10_Workday_TextBox;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox month10_Scale_TextBox;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox month9_Workday_TextBox;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox month9_Scale_TextBox;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox month8_Workday_TextBox;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox month8_Scale_TextBox;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox month7_Workday_TextBox;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox month7_Scale_TextBox;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox month6_Workday_TextBox;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox month6_Scale_TextBox;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox month5_Workday_TextBox;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox month5_Scale_TextBox;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox month4_Workday_TextBox;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox month4_Scale_TextBox;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox month3_Workday_TextBox;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox month3_Scale_TextBox;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox month2_Workday_TextBox;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox month2_Scale_TextBox;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox month1_Workday_TextBox;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox month1_Scale_TextBox;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox peakLoadGroupBox;
        private System.Windows.Forms.TextBox peak_Load_TextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button hr10_Button;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Button hr9_Button;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Button hr14_Button;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Button hr11_Button;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Button hr15_Button;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button factory_Button;
        private System.Windows.Forms.Button office_Button;
        private System.Windows.Forms.Button day31_Button;
        private System.Windows.Forms.Button day20_Button;
    }
}