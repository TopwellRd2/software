﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Plan_Cost_Analysis : Form
    {
        private Form1 form1;
        public Dictionary<string, string> plan_Cost_Analysis_Map { get; set; } = new Dictionary<string, string>();
        public String traditional_Air_Cost;
        public String ice_Storage_Cost;
        public String high_Pressure_Cost;
        public String auto_Control_Cost;
        public String design_Cost;
        public String peak_Load;
        public String full_Horsepower;
        public String part_Horsepower;
        public String evaporator_Type;
        public String full_Ice_Storage_Time;
        public String part_Ice_Storage_Time;
        public Form_Plan_Cost_Analysis(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            initialMap();
        }

        private void Form_Plan_Cost_Analysis_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach(Control control in this.Controls){
                if (control is TextBox)
                {
                    if (string.IsNullOrWhiteSpace(control.Text))
                    {
                        MessageBox.Show("請勿為空! 若無需費用,請填 0 ", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        control.Focus();
                        e.Cancel = true;
                        return;
                    }
                }
            }
        }

        private void Form_Plan_Cost_Analysis_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.planCostAnalysis_CheckBox.Checked)
            {
                form1.planCostAnalysis_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            for (int i = 1; i < 10; i++)
            {
                plan_Cost_Analysis_Map.Add("traditional_TextBox" + i, ((TextBox)this.Controls.Find("traditional_TextBox" + i, true)[0]).Text);
            }

            for (int i = 1; i < 10; i++)
            {
                plan_Cost_Analysis_Map.Add("full_TextBox" + i, ((TextBox)this.Controls.Find("full_TextBox" + i, true)[0]).Text);
            }

            for (int i = 1; i < 10; i++)
            {
                plan_Cost_Analysis_Map.Add("part_TextBox" + i, ((TextBox)this.Controls.Find("part_TextBox" + i, true)[0]).Text);
            }

            plan_Cost_Analysis_Map.Add("two_full_TextBox", two_full_TextBox.Text);
            plan_Cost_Analysis_Map.Add("two_part_TextBox", two_part_TextBox.Text);
            plan_Cost_Analysis_Map.Add("three_full_TextBox", three_full_TextBox.Text);
            plan_Cost_Analysis_Map.Add("three_part_TextBox", three_part_TextBox.Text);

        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            for (int i = 1; i < 10; i++)
            {
                plan_Cost_Analysis_Map["traditional_TextBox" + i] = ((TextBox)this.Controls.Find("traditional_TextBox" + i, true)[0]).Text;
            }

            for (int i = 1; i < 10; i++)
            {
                plan_Cost_Analysis_Map["full_TextBox" + i] = ((TextBox)this.Controls.Find("full_TextBox" + i, true)[0]).Text;
            }

            for (int i = 1; i < 10; i++)
            {
                plan_Cost_Analysis_Map["part_TextBox" + i] = ((TextBox)this.Controls.Find("part_TextBox" + i, true)[0]).Text;
            }
            plan_Cost_Analysis_Map["two_full_TextBox"] = two_full_TextBox.Text;
            plan_Cost_Analysis_Map["two_part_TextBox"] = two_part_TextBox.Text;
            plan_Cost_Analysis_Map["three_full_TextBox"] = three_full_TextBox.Text;
            plan_Cost_Analysis_Map["three_part_TextBox"] = three_part_TextBox.Text;
        }

        private void ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void auto_Button_Click(object sender, EventArgs e)
        {
            //傳統空調計價單價
            traditional_Air_Cost = form1.form_Plan_Unit_Price.plan_Unit_Price_Map["air_choose"];
            //儲冰設備計價單價
            ice_Storage_Cost = form1.form_Plan_Unit_Price.plan_Unit_Price_Map["ice_choose"];
            //高壓裝置費用
            high_Pressure_Cost = form1.form_Plan_Unit_Price.plan_Unit_Price_Map["high_choose"];
            //自動控制
            auto_Control_Cost = form1.form_Plan_Unit_Price.plan_Unit_Price_Map["auto_choose"];
            //設計費
            design_Cost = form1.form_Plan_Unit_Price.plan_Unit_Price_Map["design_choose"];
            //尖峰負荷
            peak_Load = form1.form_Project_Design_Load.project_Design_Map["peak_Load"];
            //全量馬力
            full_Horsepower = form1.form_Horsepower_Data.horsepower_Data_Map["full_horsepower"];
            //分量馬力
            part_Horsepower = form1.form_Horsepower_Data.horsepower_Data_Map["part_horsepower"];
            //蒸發器型式
            evaporator_Type = form1.form_Evaporator_Type.evaportator_Type_Map["choose"];
            //全量儲冰時間
            full_Ice_Storage_Time = form1.form_Mode_Running_Time.mode_Running_Time_Map["full_ice_hour"];
            //分量儲冰時間
            part_Ice_Storage_Time = form1.form_Mode_Running_Time.mode_Running_Time_Map["part_ice_hour"];

            //傳統空調
            traditional_TextBox1.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.49).ToString();
            traditional_TextBox2.Text = "0";
            traditional_TextBox3.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.23).ToString();
            traditional_TextBox4.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.13).ToString();
            traditional_TextBox5.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.04).ToString();
            traditional_TextBox6.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.02).ToString();
            traditional_TextBox7.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.09).ToString();
            traditional_TextBox8.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load)).ToString();
            traditional_TextBox9.Text = (Double.Parse(traditional_TextBox8.Text) * Double.Parse(design_Cost)).ToString();

            //全量儲冰
            full_TextBox1.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.49 * 1.4).ToString();
            full_TextBox2.Text = (int.Parse(full_Horsepower) * Double.Parse(evaporator_Type) * Double.Parse(full_Ice_Storage_Time) * int.Parse(ice_Storage_Cost)).ToString();
            full_TextBox3.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.23 * 1.09).ToString();
            full_TextBox4.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.13 * 1.1).ToString();
            full_TextBox5.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.04 * 1.3).ToString();
            full_TextBox6.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.02 * 1.375).ToString();
            full_TextBox7.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.09 * 1.3).ToString();
            Double y = 0;
            for(int i = 1; i < 8; i++)
            {
                y = y + Double.Parse(((TextBox)this.Controls.Find("full_TextBox" + i, true)[0]).Text);
            }
            full_TextBox8.Text = y.ToString();
            full_TextBox9.Text = (Double.Parse(full_TextBox8.Text) * Double.Parse(design_Cost)).ToString();

            //分量儲冰
            part_TextBox1.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.49 * 0.86).ToString();
            part_TextBox2.Text = (int.Parse(part_Horsepower) * Double.Parse(evaporator_Type) * Double.Parse(part_Ice_Storage_Time) * int.Parse(ice_Storage_Cost)).ToString();
            part_TextBox3.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.23 * 1.02).ToString();
            part_TextBox4.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.13 * 1.05).ToString();
            part_TextBox5.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.04 * 1.3).ToString();
            part_TextBox6.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.02 * 1.3).ToString();
            part_TextBox7.Text = (int.Parse(traditional_Air_Cost) * int.Parse(peak_Load) * 0.09 * 1.2).ToString();
            Double z = 0;
            for (int i = 1; i < 8; i++)
            {
                z = z + Double.Parse(((TextBox)this.Controls.Find("part_TextBox" + i, true)[0]).Text);
            }
            part_TextBox8.Text = z.ToString();
            part_TextBox9.Text = (Double.Parse(part_TextBox8.Text) * Double.Parse(design_Cost)).ToString();
        }
    }
}
