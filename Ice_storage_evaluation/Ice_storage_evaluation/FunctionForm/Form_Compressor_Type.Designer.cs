﻿namespace Ice_storage_evaluation
{
    partial class Form_Compressor_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.set_GroupBox = new System.Windows.Forms.GroupBox();
            this.set_TextBox = new System.Windows.Forms.TextBox();
            this.ok_Button = new System.Windows.Forms.Button();
            this.choose_GroupBox = new System.Windows.Forms.GroupBox();
            this.centrifugal_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_RadioButton = new System.Windows.Forms.RadioButton();
            this.screw_RadioButton = new System.Windows.Forms.RadioButton();
            this.repeat_RadioButton = new System.Windows.Forms.RadioButton();
            this.set_GroupBox.SuspendLayout();
            this.choose_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // set_GroupBox
            // 
            this.set_GroupBox.Controls.Add(this.set_TextBox);
            this.set_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.set_GroupBox.Location = new System.Drawing.Point(328, 29);
            this.set_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_GroupBox.Name = "set_GroupBox";
            this.set_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_GroupBox.Size = new System.Drawing.Size(252, 133);
            this.set_GroupBox.TabIndex = 4;
            this.set_GroupBox.TabStop = false;
            this.set_GroupBox.Text = "自行設定";
            // 
            // set_TextBox
            // 
            this.set_TextBox.Enabled = false;
            this.set_TextBox.Location = new System.Drawing.Point(36, 52);
            this.set_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_TextBox.Name = "set_TextBox";
            this.set_TextBox.Size = new System.Drawing.Size(182, 41);
            this.set_TextBox.TabIndex = 0;
            this.set_TextBox.Text = "0.9";
            // 
            // ok_Button
            // 
            this.ok_Button.Font = new System.Drawing.Font("微軟正黑體", 15F);
            this.ok_Button.Location = new System.Drawing.Point(328, 192);
            this.ok_Button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(252, 86);
            this.ok_Button.TabIndex = 5;
            this.ok_Button.Text = "確定";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.ok_Button_Click);
            // 
            // choose_GroupBox
            // 
            this.choose_GroupBox.Controls.Add(this.centrifugal_RadioButton);
            this.choose_GroupBox.Controls.Add(this.set_RadioButton);
            this.choose_GroupBox.Controls.Add(this.screw_RadioButton);
            this.choose_GroupBox.Controls.Add(this.repeat_RadioButton);
            this.choose_GroupBox.Location = new System.Drawing.Point(25, 29);
            this.choose_GroupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Name = "choose_GroupBox";
            this.choose_GroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.choose_GroupBox.Size = new System.Drawing.Size(282, 249);
            this.choose_GroupBox.TabIndex = 6;
            this.choose_GroupBox.TabStop = false;
            // 
            // centrifugal_RadioButton
            // 
            this.centrifugal_RadioButton.AutoSize = true;
            this.centrifugal_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.centrifugal_RadioButton.Location = new System.Drawing.Point(73, 138);
            this.centrifugal_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.centrifugal_RadioButton.Name = "centrifugal_RadioButton";
            this.centrifugal_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.centrifugal_RadioButton.TabIndex = 9;
            this.centrifugal_RadioButton.Text = "離心式";
            this.centrifugal_RadioButton.UseVisualStyleBackColor = true;
            this.centrifugal_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // set_RadioButton
            // 
            this.set_RadioButton.AutoSize = true;
            this.set_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.set_RadioButton.Location = new System.Drawing.Point(73, 193);
            this.set_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.set_RadioButton.Name = "set_RadioButton";
            this.set_RadioButton.Size = new System.Drawing.Size(135, 35);
            this.set_RadioButton.TabIndex = 8;
            this.set_RadioButton.Text = "自行設定";
            this.set_RadioButton.UseVisualStyleBackColor = true;
            this.set_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // screw_RadioButton
            // 
            this.screw_RadioButton.AutoSize = true;
            this.screw_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.screw_RadioButton.Location = new System.Drawing.Point(73, 83);
            this.screw_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.screw_RadioButton.Name = "screw_RadioButton";
            this.screw_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.screw_RadioButton.TabIndex = 7;
            this.screw_RadioButton.Text = "螺旋式";
            this.screw_RadioButton.UseVisualStyleBackColor = true;
            this.screw_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // repeat_RadioButton
            // 
            this.repeat_RadioButton.AutoSize = true;
            this.repeat_RadioButton.Checked = true;
            this.repeat_RadioButton.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.repeat_RadioButton.Location = new System.Drawing.Point(73, 28);
            this.repeat_RadioButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.repeat_RadioButton.Name = "repeat_RadioButton";
            this.repeat_RadioButton.Size = new System.Drawing.Size(110, 35);
            this.repeat_RadioButton.TabIndex = 6;
            this.repeat_RadioButton.TabStop = true;
            this.repeat_RadioButton.Text = "往復式";
            this.repeat_RadioButton.UseVisualStyleBackColor = true;
            this.repeat_RadioButton.Click += new System.EventHandler(this.RadioButton_Click);
            // 
            // Form_Compressor_Type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(609, 307);
            this.Controls.Add(this.choose_GroupBox);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.set_GroupBox);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Compressor_Type";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "壓縮機型式";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Compressor_Type_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Compressor_Type_Closed);
            this.set_GroupBox.ResumeLayout(false);
            this.set_GroupBox.PerformLayout();
            this.choose_GroupBox.ResumeLayout(false);
            this.choose_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox set_GroupBox;
        private System.Windows.Forms.TextBox set_TextBox;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.GroupBox choose_GroupBox;
        private System.Windows.Forms.RadioButton set_RadioButton;
        private System.Windows.Forms.RadioButton screw_RadioButton;
        private System.Windows.Forms.RadioButton repeat_RadioButton;
        private System.Windows.Forms.RadioButton centrifugal_RadioButton;
    }
}