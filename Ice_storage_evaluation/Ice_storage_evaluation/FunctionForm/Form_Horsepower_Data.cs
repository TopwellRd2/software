﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Horsepower_Data : Form
    {
        private Form1 form1;
        public Dictionary<string, string> horsepower_Data_Map { get; set; } = new Dictionary<string, string>();

        public Form_Horsepower_Data(Form1 form)
        {
            InitializeComponent();
            form1 = form;
            calculate_Horsepower();
            initialMap();
        }

        private void Form_Horsepower_Data_Load(object sender, EventArgs e)
        {
            calculate_Horsepower();
            full_Horsepower_Set_TextBox.Text = horsepower_Data_Map["full_horsepower"];
            part_Horsepower_Set_TextBox.Text = horsepower_Data_Map["part_horsepower"];
        }

        private void calculate_Horsepower()
        {
            double x = 0; //尖峰負荷數值total
            double full_Ice_Hour = 0;  //全量儲冰時間
            double part_Ice_Hour = 0;  //分量儲冰時間
            double part_Air_Hour = 0;  //分量空調時間
            double Evaporator = 0; //蒸發器型式

            //全量適合馬力 =  尖峰負荷最大值total / 儲冰時間(HR) / 蒸發器型式
            foreach (KeyValuePair<string, string> kvp in form1.form_Project_Design_Load.project_Design_Map)
            {
                if (kvp.Key.Contains("day_"))
                {
                    x += double.Parse(kvp.Value);
                }
            }
            full_Ice_Hour = double.Parse(form1.form_Mode_Running_Time.mode_Running_Time_Map["full_ice_hour"]);
            Evaporator = double.Parse(form1.form_Evaporator_Type.evaportator_Type_Map["choose"]);
            full_horsepower_Label.Text = Convert.ToInt32(x / full_Ice_Hour / Evaporator).ToString();
            full_Horsepower_Set_TextBox.Text = full_horsepower_Label.Text;

            //分量適合馬力 = 尖峰負荷最大值total / (分量空調運轉時間 + 分量儲冰時間 * 蒸發器型式)
            part_Ice_Hour = double.Parse(form1.form_Mode_Running_Time.mode_Running_Time_Map["part_ice_hour"]);
            part_Air_Hour = double.Parse(form1.form_Mode_Running_Time.mode_Running_Time_Map["part_air_hour"]);
            part_horsepower_Label.Text = Convert.ToInt32(x / (part_Air_Hour + part_Ice_Hour * Evaporator)).ToString();
            part_Horsepower_Set_TextBox.Text = part_horsepower_Label.Text;
        }

        private void Form_Horsepower_Data_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(full_Horsepower_Set_TextBox.Text))
            {
                MessageBox.Show("設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                full_Horsepower_Set_TextBox.Text = "";
                full_Horsepower_Set_TextBox.Focus();
                e.Cancel = true;
                return;
            }
            if (String.IsNullOrWhiteSpace(part_Horsepower_Set_TextBox.Text))
            {
                MessageBox.Show("設定不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                part_Horsepower_Set_TextBox.Text = "";
                part_Horsepower_Set_TextBox.Focus();
                e.Cancel = true;
                return;
            }
        }

        private void Form_Horsepower_Data_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.horsepowerData_CheckBox.Checked)
            {
                form1.horsepowerData_CheckBox.Checked = false;
            }
            updateMap();
        }

        private void initialMap()
        {
            horsepower_Data_Map.Add("full_horsepower", full_Horsepower_Set_TextBox.Text);
            horsepower_Data_Map.Add("part_horsepower", part_Horsepower_Set_TextBox.Text);
        }

        private void updateMap()  //將控制項的值存進Map裡
        {
            horsepower_Data_Map["full_horsepower"] = full_Horsepower_Set_TextBox.Text;
            horsepower_Data_Map["part_horsepower"] = part_Horsepower_Set_TextBox.Text;
        }

            private void horsepower_Data_Ok_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
