﻿namespace Ice_storage_evaluation
{
    partial class Form_User_Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.accountNewTextBox = new System.Windows.Forms.TextBox();
            this.passwordNewTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.adminNewCheckBox = new System.Windows.Forms.CheckBox();
            this.userNewButton = new System.Windows.Forms.Button();
            this.iCE_STORAGE_DataSet = new Ice_storage_evaluation.ICE_STORAGE_DataSet();
            this.uSERBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uSERTableAdapter = new Ice_storage_evaluation.ICE_STORAGE_DataSetTableAdapters.USERTableAdapter();
            this.userDeleteButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.userEditButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.administratorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.passwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.iCE_STORAGE_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSERBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "使用者帳號";
            // 
            // accountNewTextBox
            // 
            this.accountNewTextBox.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.accountNewTextBox.Location = new System.Drawing.Point(17, 43);
            this.accountNewTextBox.Name = "accountNewTextBox";
            this.accountNewTextBox.Size = new System.Drawing.Size(179, 32);
            this.accountNewTextBox.TabIndex = 2;
            this.accountNewTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.accountNewTextBox_KeyUp);
            // 
            // passwordNewTextBox
            // 
            this.passwordNewTextBox.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.passwordNewTextBox.Location = new System.Drawing.Point(216, 43);
            this.passwordNewTextBox.Name = "passwordNewTextBox";
            this.passwordNewTextBox.Size = new System.Drawing.Size(179, 32);
            this.passwordNewTextBox.TabIndex = 4;
            this.passwordNewTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.passwordNewTextBox_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(211, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "密碼";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(443, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "管理員權限";
            // 
            // adminNewCheckBox
            // 
            this.adminNewCheckBox.AutoSize = true;
            this.adminNewCheckBox.Location = new System.Drawing.Point(491, 48);
            this.adminNewCheckBox.Name = "adminNewCheckBox";
            this.adminNewCheckBox.Size = new System.Drawing.Size(18, 17);
            this.adminNewCheckBox.TabIndex = 6;
            this.adminNewCheckBox.UseVisualStyleBackColor = true;
            // 
            // userNewButton
            // 
            this.userNewButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userNewButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.userNewButton.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.userNewButton.Location = new System.Drawing.Point(642, 33);
            this.userNewButton.Name = "userNewButton";
            this.userNewButton.Size = new System.Drawing.Size(133, 38);
            this.userNewButton.TabIndex = 7;
            this.userNewButton.Text = "新增";
            this.userNewButton.UseVisualStyleBackColor = false;
            this.userNewButton.Click += new System.EventHandler(this.userNewButton_Click);
            // 
            // iCE_STORAGE_DataSet
            // 
            this.iCE_STORAGE_DataSet.DataSetName = "ICE_STORAGE_DataSet";
            this.iCE_STORAGE_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uSERBindingSource
            // 
            this.uSERBindingSource.DataMember = "USER";
            this.uSERBindingSource.DataSource = this.iCE_STORAGE_DataSet;
            // 
            // uSERTableAdapter
            // 
            this.uSERTableAdapter.ClearBeforeFill = true;
            // 
            // userDeleteButton
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userDeleteButton.DefaultCellStyle = dataGridViewCellStyle1;
            this.userDeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.userDeleteButton.HeaderText = "";
            this.userDeleteButton.MinimumWidth = 6;
            this.userDeleteButton.Name = "userDeleteButton";
            this.userDeleteButton.ReadOnly = true;
            this.userDeleteButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.userDeleteButton.Text = "刪除";
            this.userDeleteButton.UseColumnTextForButtonValue = true;
            // 
            // userEditButton
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.userEditButton.DefaultCellStyle = dataGridViewCellStyle2;
            this.userEditButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.userEditButton.HeaderText = "";
            this.userEditButton.MinimumWidth = 6;
            this.userEditButton.Name = "userEditButton";
            this.userEditButton.ReadOnly = true;
            this.userEditButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.userEditButton.Text = "修改";
            this.userEditButton.UseColumnTextForButtonValue = true;
            // 
            // administratorDataGridViewTextBoxColumn
            // 
            this.administratorDataGridViewTextBoxColumn.DataPropertyName = "administrator";
            this.administratorDataGridViewTextBoxColumn.FalseValue = "N";
            this.administratorDataGridViewTextBoxColumn.HeaderText = "管理員權限";
            this.administratorDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.administratorDataGridViewTextBoxColumn.Name = "administratorDataGridViewTextBoxColumn";
            this.administratorDataGridViewTextBoxColumn.ReadOnly = true;
            this.administratorDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.administratorDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.administratorDataGridViewTextBoxColumn.TrueValue = "Y";
            // 
            // passwordDataGridViewTextBoxColumn
            // 
            this.passwordDataGridViewTextBoxColumn.DataPropertyName = "password";
            this.passwordDataGridViewTextBoxColumn.HeaderText = "密碼";
            this.passwordDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.passwordDataGridViewTextBoxColumn.Name = "passwordDataGridViewTextBoxColumn";
            this.passwordDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // accountDataGridViewTextBoxColumn
            // 
            this.accountDataGridViewTextBoxColumn.DataPropertyName = "account";
            this.accountDataGridViewTextBoxColumn.HeaderText = "使用者帳號";
            this.accountDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.accountDataGridViewTextBoxColumn.Name = "accountDataGridViewTextBoxColumn";
            this.accountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // UserDataGridView
            // 
            this.UserDataGridView.AllowUserToAddRows = false;
            this.UserDataGridView.AllowUserToDeleteRows = false;
            this.UserDataGridView.AutoGenerateColumns = false;
            this.UserDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.UserDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.UserDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.UserDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.UserDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UserDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.accountDataGridViewTextBoxColumn,
            this.passwordDataGridViewTextBoxColumn,
            this.administratorDataGridViewTextBoxColumn,
            this.userEditButton,
            this.userDeleteButton});
            this.UserDataGridView.DataSource = this.uSERBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.UserDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.UserDataGridView.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.UserDataGridView.Location = new System.Drawing.Point(2, 93);
            this.UserDataGridView.Name = "UserDataGridView";
            this.UserDataGridView.ReadOnly = true;
            this.UserDataGridView.RowHeadersVisible = false;
            this.UserDataGridView.RowHeadersWidth = 51;
            this.UserDataGridView.RowTemplate.Height = 27;
            this.UserDataGridView.Size = new System.Drawing.Size(1018, 575);
            this.UserDataGridView.TabIndex = 0;
            this.UserDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UserDataGridView_CellContentClick);
            // 
            // Form_User_Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 670);
            this.Controls.Add(this.userNewButton);
            this.Controls.Add(this.adminNewCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.passwordNewTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.accountNewTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UserDataGridView);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_User_Management";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "使用者管理";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_User_Management_FormClosed);
            this.Load += new System.EventHandler(this.Form_User_Management_Load);
            ((System.ComponentModel.ISupportInitialize)(this.iCE_STORAGE_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSERBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button userNewButton;
        public System.Windows.Forms.TextBox accountNewTextBox;
        public System.Windows.Forms.TextBox passwordNewTextBox;
        public System.Windows.Forms.CheckBox adminNewCheckBox;
        private ICE_STORAGE_DataSet iCE_STORAGE_DataSet;
        private System.Windows.Forms.BindingSource uSERBindingSource;
        private ICE_STORAGE_DataSetTableAdapters.USERTableAdapter uSERTableAdapter;
        private System.Windows.Forms.DataGridViewButtonColumn userDeleteButton;
        private System.Windows.Forms.DataGridViewButtonColumn userEditButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn administratorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn passwordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountDataGridViewTextBoxColumn;
        public System.Windows.Forms.DataGridView UserDataGridView;
    }
}