﻿namespace Ice_storage_evaluation
{
    partial class Form_Basic_Electricity_Price_Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.checkBox44 = new System.Windows.Forms.CheckBox();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.checkBox46 = new System.Windows.Forms.CheckBox();
            this.checkBox47 = new System.Windows.Forms.CheckBox();
            this.checkBox48 = new System.Windows.Forms.CheckBox();
            this.checkBox49 = new System.Windows.Forms.CheckBox();
            this.checkBox50 = new System.Windows.Forms.CheckBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.checkBox53 = new System.Windows.Forms.CheckBox();
            this.checkBox54 = new System.Windows.Forms.CheckBox();
            this.checkBox55 = new System.Windows.Forms.CheckBox();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.checkBox85 = new System.Windows.Forms.CheckBox();
            this.checkBox86 = new System.Windows.Forms.CheckBox();
            this.checkBox87 = new System.Windows.Forms.CheckBox();
            this.checkBox88 = new System.Windows.Forms.CheckBox();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.checkBox92 = new System.Windows.Forms.CheckBox();
            this.checkBox93 = new System.Windows.Forms.CheckBox();
            this.checkBox94 = new System.Windows.Forms.CheckBox();
            this.checkBox95 = new System.Windows.Forms.CheckBox();
            this.checkBox96 = new System.Windows.Forms.CheckBox();
            this.checkBox97 = new System.Windows.Forms.CheckBox();
            this.checkBox98 = new System.Windows.Forms.CheckBox();
            this.checkBox99 = new System.Windows.Forms.CheckBox();
            this.checkBox100 = new System.Windows.Forms.CheckBox();
            this.checkBox101 = new System.Windows.Forms.CheckBox();
            this.checkBox102 = new System.Windows.Forms.CheckBox();
            this.checkBox103 = new System.Windows.Forms.CheckBox();
            this.checkBox104 = new System.Windows.Forms.CheckBox();
            this.checkBox105 = new System.Windows.Forms.CheckBox();
            this.checkBox106 = new System.Windows.Forms.CheckBox();
            this.checkBox107 = new System.Windows.Forms.CheckBox();
            this.checkBox108 = new System.Windows.Forms.CheckBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.checkBox109 = new System.Windows.Forms.CheckBox();
            this.checkBox110 = new System.Windows.Forms.CheckBox();
            this.checkBox111 = new System.Windows.Forms.CheckBox();
            this.checkBox112 = new System.Windows.Forms.CheckBox();
            this.checkBox113 = new System.Windows.Forms.CheckBox();
            this.checkBox114 = new System.Windows.Forms.CheckBox();
            this.checkBox115 = new System.Windows.Forms.CheckBox();
            this.checkBox116 = new System.Windows.Forms.CheckBox();
            this.checkBox117 = new System.Windows.Forms.CheckBox();
            this.checkBox118 = new System.Windows.Forms.CheckBox();
            this.checkBox119 = new System.Windows.Forms.CheckBox();
            this.checkBox120 = new System.Windows.Forms.CheckBox();
            this.checkBox121 = new System.Windows.Forms.CheckBox();
            this.checkBox122 = new System.Windows.Forms.CheckBox();
            this.checkBox123 = new System.Windows.Forms.CheckBox();
            this.checkBox124 = new System.Windows.Forms.CheckBox();
            this.checkBox125 = new System.Windows.Forms.CheckBox();
            this.checkBox126 = new System.Windows.Forms.CheckBox();
            this.checkBox127 = new System.Windows.Forms.CheckBox();
            this.checkBox128 = new System.Windows.Forms.CheckBox();
            this.checkBox129 = new System.Windows.Forms.CheckBox();
            this.checkBox130 = new System.Windows.Forms.CheckBox();
            this.checkBox131 = new System.Windows.Forms.CheckBox();
            this.checkBox132 = new System.Windows.Forms.CheckBox();
            this.checkBox133 = new System.Windows.Forms.CheckBox();
            this.checkBox134 = new System.Windows.Forms.CheckBox();
            this.checkBox135 = new System.Windows.Forms.CheckBox();
            this.checkBox136 = new System.Windows.Forms.CheckBox();
            this.checkBox137 = new System.Windows.Forms.CheckBox();
            this.checkBox138 = new System.Windows.Forms.CheckBox();
            this.checkBox139 = new System.Windows.Forms.CheckBox();
            this.checkBox140 = new System.Windows.Forms.CheckBox();
            this.checkBox141 = new System.Windows.Forms.CheckBox();
            this.checkBox142 = new System.Windows.Forms.CheckBox();
            this.checkBox143 = new System.Windows.Forms.CheckBox();
            this.checkBox144 = new System.Windows.Forms.CheckBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.checkBox146 = new System.Windows.Forms.CheckBox();
            this.label88 = new System.Windows.Forms.Label();
            this.checkBox145 = new System.Windows.Forms.CheckBox();
            this.label87 = new System.Windows.Forms.Label();
            this.checkBox147 = new System.Windows.Forms.CheckBox();
            this.label89 = new System.Windows.Forms.Label();
            this.checkBox148 = new System.Windows.Forms.CheckBox();
            this.label90 = new System.Windows.Forms.Label();
            this.checkBox149 = new System.Windows.Forms.CheckBox();
            this.label91 = new System.Windows.Forms.Label();
            this.checkBox150 = new System.Windows.Forms.CheckBox();
            this.label92 = new System.Windows.Forms.Label();
            this.checkBox151 = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.checkBox152 = new System.Windows.Forms.CheckBox();
            this.label94 = new System.Windows.Forms.Label();
            this.checkBox153 = new System.Windows.Forms.CheckBox();
            this.label95 = new System.Windows.Forms.Label();
            this.checkBox154 = new System.Windows.Forms.CheckBox();
            this.label96 = new System.Windows.Forms.Label();
            this.checkBox155 = new System.Windows.Forms.CheckBox();
            this.label97 = new System.Windows.Forms.Label();
            this.checkBox156 = new System.Windows.Forms.CheckBox();
            this.label98 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(27, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "電價表名稱";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("新細明體", 12F);
            this.textBox1.Location = new System.Drawing.Point(177, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(726, 31);
            this.textBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(924, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 31);
            this.label2.TabIndex = 2;
            this.label2.Text = "幣制";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("新細明體", 12F);
            this.textBox2.Location = new System.Drawing.Point(1003, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(179, 31);
            this.textBox2.TabIndex = 3;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("新細明體", 12F);
            this.textBox3.Location = new System.Drawing.Point(1362, 19);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(221, 31);
            this.textBox3.TabIndex = 5;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(1209, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "對台幣匯率";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox1.Location = new System.Drawing.Point(32, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(672, 143);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "二段式電價";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox7.Location = new System.Drawing.Point(507, 95);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(145, 30);
            this.textBox7.TabIndex = 18;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox6.Location = new System.Drawing.Point(507, 57);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(145, 30);
            this.textBox6.TabIndex = 15;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox8.Location = new System.Drawing.Point(327, 95);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(145, 30);
            this.textBox8.TabIndex = 17;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox5.Location = new System.Drawing.Point(327, 57);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(145, 30);
            this.textBox5.TabIndex = 14;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox9.Location = new System.Drawing.Point(145, 95);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(145, 30);
            this.textBox9.TabIndex = 16;
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox4.Location = new System.Drawing.Point(145, 57);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(145, 30);
            this.textBox4.TabIndex = 13;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(534, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 25);
            this.label8.TabIndex = 12;
            this.label8.Text = "基本電費";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(354, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 25);
            this.label7.TabIndex = 11;
            this.label7.Text = "一般時段";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(174, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "尖峰時段";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(35, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "非夏月";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(55, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "夏月";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox16);
            this.groupBox2.Controls.Add(this.textBox17);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.textBox10);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.textBox14);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.Location = new System.Drawing.Point(731, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(852, 143);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "三段式電價";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox16.Location = new System.Drawing.Point(700, 95);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(145, 30);
            this.textBox16.TabIndex = 32;
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox17.Location = new System.Drawing.Point(700, 57);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(145, 30);
            this.textBox17.TabIndex = 31;
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(727, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 25);
            this.label14.TabIndex = 30;
            this.label14.Text = "基本電費";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox10.Location = new System.Drawing.Point(520, 95);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(145, 30);
            this.textBox10.TabIndex = 29;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox11.Location = new System.Drawing.Point(520, 57);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(145, 30);
            this.textBox11.TabIndex = 26;
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox12.Location = new System.Drawing.Point(340, 95);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(145, 30);
            this.textBox12.TabIndex = 28;
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox13.Location = new System.Drawing.Point(340, 57);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(145, 30);
            this.textBox13.TabIndex = 25;
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox14.Location = new System.Drawing.Point(158, 95);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(145, 30);
            this.textBox14.TabIndex = 27;
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox15.Location = new System.Drawing.Point(158, 57);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(145, 30);
            this.textBox15.TabIndex = 24;
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(547, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 25);
            this.label9.TabIndex = 23;
            this.label9.Text = "波谷時段";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(367, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 25);
            this.label10.TabIndex = 22;
            this.label10.Text = "一般時段";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(187, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 25);
            this.label11.TabIndex = 21;
            this.label11.Text = "尖峰時段";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(48, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 25);
            this.label12.TabIndex = 20;
            this.label12.Text = "非夏月";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(68, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 25);
            this.label13.TabIndex = 19;
            this.label13.Text = "夏月";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(67, 245);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 28);
            this.label15.TabIndex = 11;
            this.label15.Text = "時段";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(200, 217);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 28);
            this.label16.TabIndex = 12;
            this.label16.Text = "電價時段";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(182, 245);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 51);
            this.label17.TabIndex = 13;
            this.label17.Text = "波峰";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.UseWaitCursor = true;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(230, 245);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 51);
            this.label18.TabIndex = 14;
            this.label18.Text = "平";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label18.UseWaitCursor = true;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(280, 245);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 51);
            this.label19.TabIndex = 15;
            this.label19.Text = "波谷";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(599, 245);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 51);
            this.label20.TabIndex = 20;
            this.label20.Text = "波谷";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label20.UseWaitCursor = true;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(549, 245);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 51);
            this.label21.TabIndex = 19;
            this.label21.Text = "平";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.UseWaitCursor = true;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(501, 245);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 51);
            this.label22.TabIndex = 18;
            this.label22.Text = "波峰";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.UseWaitCursor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(518, 217);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 28);
            this.label23.TabIndex = 17;
            this.label23.Text = "電價時段";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(386, 245);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 28);
            this.label24.TabIndex = 16;
            this.label24.Text = "時段";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(918, 245);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 51);
            this.label25.TabIndex = 25;
            this.label25.Text = "波谷";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.UseWaitCursor = true;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(868, 245);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 51);
            this.label26.TabIndex = 24;
            this.label26.Text = "平";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label26.UseWaitCursor = true;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(820, 245);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 51);
            this.label27.TabIndex = 23;
            this.label27.Text = "波峰";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label27.UseWaitCursor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(836, 217);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 28);
            this.label28.TabIndex = 22;
            this.label28.Text = "電價時段";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(705, 245);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 28);
            this.label29.TabIndex = 21;
            this.label29.Text = "時段";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(1237, 245);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 51);
            this.label30.TabIndex = 30;
            this.label30.Text = "波谷";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label30.UseWaitCursor = true;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(1187, 245);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(33, 51);
            this.label31.TabIndex = 29;
            this.label31.Text = "平";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label31.UseWaitCursor = true;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(1139, 245);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(33, 51);
            this.label32.TabIndex = 28;
            this.label32.Text = "波峰";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label32.UseWaitCursor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(1154, 217);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 28);
            this.label33.TabIndex = 27;
            this.label33.Text = "電價時段";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(1024, 245);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(56, 28);
            this.label34.TabIndex = 26;
            this.label34.Text = "時段";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 13F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(1396, 255);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(154, 28);
            this.label35.TabIndex = 31;
            this.label35.Text = "高峰/夏月用電";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Location = new System.Drawing.Point(32, 296);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(1550, 2);
            this.label36.TabIndex = 32;
            // 
            // label37
            // 
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Location = new System.Drawing.Point(1335, 217);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(2, 480);
            this.label37.TabIndex = 33;
            // 
            // label38
            // 
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.Location = new System.Drawing.Point(33, 695);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(1550, 2);
            this.label38.TabIndex = 34;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(391, 700);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(695, 85);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button4.Location = new System.Drawing.Point(517, 24);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(157, 40);
            this.button4.TabIndex = 3;
            this.button4.Text = "最後筆";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button3.Location = new System.Drawing.Point(354, 24);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(157, 40);
            this.button3.TabIndex = 2;
            this.button3.Text = "第一筆";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button2.Location = new System.Drawing.Point(191, 24);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(157, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "下一筆";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button1.Location = new System.Drawing.Point(28, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "上一筆";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button5.Location = new System.Drawing.Point(331, 794);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(157, 40);
            this.button5.TabIndex = 36;
            this.button5.Text = "修改";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button6.Location = new System.Drawing.Point(494, 794);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(157, 40);
            this.button6.TabIndex = 37;
            this.button6.Text = "儲存";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button7.Location = new System.Drawing.Point(657, 794);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(157, 40);
            this.button7.TabIndex = 38;
            this.button7.Text = "新增";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button8.Location = new System.Drawing.Point(820, 794);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(157, 40);
            this.button8.TabIndex = 39;
            this.button8.Text = "取消";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button9.Location = new System.Drawing.Point(983, 794);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(157, 40);
            this.button9.TabIndex = 40;
            this.button9.Text = "刪除";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button10.Location = new System.Drawing.Point(1228, 724);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(177, 120);
            this.button10.TabIndex = 41;
            this.button10.Text = "選擇";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button11.Location = new System.Drawing.Point(1428, 724);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(177, 120);
            this.button11.TabIndex = 42;
            this.button11.Text = "退出";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(37, 317);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(133, 25);
            this.label39.TabIndex = 43;
            this.label39.Text = "01:00~01:30";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(37, 347);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(133, 25);
            this.label40.TabIndex = 44;
            this.label40.Text = "01:30~02:00";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(37, 377);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(133, 25);
            this.label41.TabIndex = 45;
            this.label41.Text = "02:00~02:30";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(37, 407);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(133, 25);
            this.label42.TabIndex = 46;
            this.label42.Text = "02:30~03:00";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(37, 437);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(133, 25);
            this.label43.TabIndex = 47;
            this.label43.Text = "03:00~03:30";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(37, 467);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(133, 25);
            this.label44.TabIndex = 48;
            this.label44.Text = "03:30~04:00";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(37, 497);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(133, 25);
            this.label45.TabIndex = 49;
            this.label45.Text = "04:00~04:30";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(37, 527);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(133, 25);
            this.label46.TabIndex = 50;
            this.label46.Text = "04:30~05:00";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(37, 557);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(133, 25);
            this.label47.TabIndex = 51;
            this.label47.Text = "05:00~05:30";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label48.Location = new System.Drawing.Point(37, 587);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(133, 25);
            this.label48.TabIndex = 52;
            this.label48.Text = "05:30~06:00";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(37, 617);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(133, 25);
            this.label49.TabIndex = 53;
            this.label49.Text = "06:00~06:30";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label50.Location = new System.Drawing.Point(37, 647);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(133, 25);
            this.label50.TabIndex = 54;
            this.label50.Text = "06:30~07:00";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox1.Location = new System.Drawing.Point(190, 325);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 55;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox2.Location = new System.Drawing.Point(238, 325);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 56;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.Color.Green;
            this.checkBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox3.Location = new System.Drawing.Point(288, 325);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 57;
            this.checkBox3.UseVisualStyleBackColor = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.BackColor = System.Drawing.Color.Green;
            this.checkBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox4.Location = new System.Drawing.Point(288, 355);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 60;
            this.checkBox4.UseVisualStyleBackColor = false;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox5.Location = new System.Drawing.Point(238, 355);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 59;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox6.Location = new System.Drawing.Point(190, 355);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 58;
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.BackColor = System.Drawing.Color.Green;
            this.checkBox7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox7.Location = new System.Drawing.Point(288, 385);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 63;
            this.checkBox7.UseVisualStyleBackColor = false;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox8.Location = new System.Drawing.Point(238, 385);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 62;
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox9.Location = new System.Drawing.Point(190, 385);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 61;
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.BackColor = System.Drawing.Color.Green;
            this.checkBox10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox10.Location = new System.Drawing.Point(288, 415);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 66;
            this.checkBox10.UseVisualStyleBackColor = false;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox11.Location = new System.Drawing.Point(238, 415);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 65;
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox12.Location = new System.Drawing.Point(190, 415);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 64;
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.BackColor = System.Drawing.Color.Green;
            this.checkBox13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox13.Location = new System.Drawing.Point(288, 445);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 69;
            this.checkBox13.UseVisualStyleBackColor = false;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox14.Location = new System.Drawing.Point(238, 445);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 68;
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox15.Location = new System.Drawing.Point(190, 445);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 67;
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.BackColor = System.Drawing.Color.Green;
            this.checkBox16.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox16.Location = new System.Drawing.Point(288, 475);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(15, 14);
            this.checkBox16.TabIndex = 72;
            this.checkBox16.UseVisualStyleBackColor = false;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox17.Location = new System.Drawing.Point(238, 475);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(15, 14);
            this.checkBox17.TabIndex = 71;
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox18.Location = new System.Drawing.Point(190, 475);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(15, 14);
            this.checkBox18.TabIndex = 70;
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.BackColor = System.Drawing.Color.Green;
            this.checkBox19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox19.Location = new System.Drawing.Point(288, 505);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(15, 14);
            this.checkBox19.TabIndex = 75;
            this.checkBox19.UseVisualStyleBackColor = false;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox20.Location = new System.Drawing.Point(238, 505);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(15, 14);
            this.checkBox20.TabIndex = 74;
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox21.Location = new System.Drawing.Point(190, 505);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(15, 14);
            this.checkBox21.TabIndex = 73;
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.BackColor = System.Drawing.Color.Green;
            this.checkBox22.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox22.Location = new System.Drawing.Point(288, 535);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(15, 14);
            this.checkBox22.TabIndex = 78;
            this.checkBox22.UseVisualStyleBackColor = false;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox23.Location = new System.Drawing.Point(238, 535);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(15, 14);
            this.checkBox23.TabIndex = 77;
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox24.Location = new System.Drawing.Point(190, 535);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(15, 14);
            this.checkBox24.TabIndex = 76;
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.BackColor = System.Drawing.Color.Green;
            this.checkBox25.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox25.Location = new System.Drawing.Point(288, 565);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(15, 14);
            this.checkBox25.TabIndex = 81;
            this.checkBox25.UseVisualStyleBackColor = false;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox26.Location = new System.Drawing.Point(238, 565);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(15, 14);
            this.checkBox26.TabIndex = 80;
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox27.Location = new System.Drawing.Point(190, 565);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(15, 14);
            this.checkBox27.TabIndex = 79;
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.BackColor = System.Drawing.Color.Green;
            this.checkBox28.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox28.Location = new System.Drawing.Point(288, 595);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(15, 14);
            this.checkBox28.TabIndex = 84;
            this.checkBox28.UseVisualStyleBackColor = false;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox29.Location = new System.Drawing.Point(238, 595);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(15, 14);
            this.checkBox29.TabIndex = 83;
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox30.Location = new System.Drawing.Point(190, 595);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(15, 14);
            this.checkBox30.TabIndex = 82;
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.BackColor = System.Drawing.Color.Green;
            this.checkBox31.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox31.Location = new System.Drawing.Point(288, 625);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(15, 14);
            this.checkBox31.TabIndex = 87;
            this.checkBox31.UseVisualStyleBackColor = false;
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox32.Location = new System.Drawing.Point(238, 625);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(15, 14);
            this.checkBox32.TabIndex = 86;
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox33.Location = new System.Drawing.Point(190, 625);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(15, 14);
            this.checkBox33.TabIndex = 85;
            this.checkBox33.UseVisualStyleBackColor = true;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.BackColor = System.Drawing.Color.Green;
            this.checkBox34.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox34.Location = new System.Drawing.Point(288, 655);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(15, 14);
            this.checkBox34.TabIndex = 90;
            this.checkBox34.UseVisualStyleBackColor = false;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox35.Location = new System.Drawing.Point(238, 655);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(15, 14);
            this.checkBox35.TabIndex = 89;
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox36.Location = new System.Drawing.Point(190, 655);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(15, 14);
            this.checkBox36.TabIndex = 88;
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox37.Location = new System.Drawing.Point(607, 655);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(15, 14);
            this.checkBox37.TabIndex = 138;
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox38.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox38.Location = new System.Drawing.Point(557, 655);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(15, 14);
            this.checkBox38.TabIndex = 137;
            this.checkBox38.UseVisualStyleBackColor = false;
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox39.Location = new System.Drawing.Point(509, 655);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(15, 14);
            this.checkBox39.TabIndex = 136;
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox40.Location = new System.Drawing.Point(607, 625);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(15, 14);
            this.checkBox40.TabIndex = 135;
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox41.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox41.Location = new System.Drawing.Point(557, 625);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(15, 14);
            this.checkBox41.TabIndex = 134;
            this.checkBox41.UseVisualStyleBackColor = false;
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox42.Location = new System.Drawing.Point(509, 625);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(15, 14);
            this.checkBox42.TabIndex = 133;
            this.checkBox42.UseVisualStyleBackColor = true;
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox43.Location = new System.Drawing.Point(607, 595);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(15, 14);
            this.checkBox43.TabIndex = 132;
            this.checkBox43.UseVisualStyleBackColor = true;
            // 
            // checkBox44
            // 
            this.checkBox44.AutoSize = true;
            this.checkBox44.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox44.Location = new System.Drawing.Point(557, 595);
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new System.Drawing.Size(15, 14);
            this.checkBox44.TabIndex = 131;
            this.checkBox44.UseVisualStyleBackColor = true;
            // 
            // checkBox45
            // 
            this.checkBox45.AutoSize = true;
            this.checkBox45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox45.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox45.Location = new System.Drawing.Point(509, 595);
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new System.Drawing.Size(15, 14);
            this.checkBox45.TabIndex = 130;
            this.checkBox45.UseVisualStyleBackColor = false;
            // 
            // checkBox46
            // 
            this.checkBox46.AutoSize = true;
            this.checkBox46.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox46.Location = new System.Drawing.Point(607, 565);
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new System.Drawing.Size(15, 14);
            this.checkBox46.TabIndex = 129;
            this.checkBox46.UseVisualStyleBackColor = true;
            // 
            // checkBox47
            // 
            this.checkBox47.AutoSize = true;
            this.checkBox47.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox47.Location = new System.Drawing.Point(557, 565);
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new System.Drawing.Size(15, 14);
            this.checkBox47.TabIndex = 128;
            this.checkBox47.UseVisualStyleBackColor = true;
            // 
            // checkBox48
            // 
            this.checkBox48.AutoSize = true;
            this.checkBox48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox48.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox48.Location = new System.Drawing.Point(509, 565);
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new System.Drawing.Size(15, 14);
            this.checkBox48.TabIndex = 127;
            this.checkBox48.UseVisualStyleBackColor = false;
            // 
            // checkBox49
            // 
            this.checkBox49.AutoSize = true;
            this.checkBox49.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox49.Location = new System.Drawing.Point(607, 535);
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new System.Drawing.Size(15, 14);
            this.checkBox49.TabIndex = 126;
            this.checkBox49.UseVisualStyleBackColor = true;
            // 
            // checkBox50
            // 
            this.checkBox50.AutoSize = true;
            this.checkBox50.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox50.Location = new System.Drawing.Point(557, 535);
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new System.Drawing.Size(15, 14);
            this.checkBox50.TabIndex = 125;
            this.checkBox50.UseVisualStyleBackColor = true;
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox51.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox51.Location = new System.Drawing.Point(509, 535);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(15, 14);
            this.checkBox51.TabIndex = 124;
            this.checkBox51.UseVisualStyleBackColor = false;
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox52.Location = new System.Drawing.Point(607, 505);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(15, 14);
            this.checkBox52.TabIndex = 123;
            this.checkBox52.UseVisualStyleBackColor = true;
            // 
            // checkBox53
            // 
            this.checkBox53.AutoSize = true;
            this.checkBox53.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox53.Location = new System.Drawing.Point(557, 505);
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new System.Drawing.Size(15, 14);
            this.checkBox53.TabIndex = 122;
            this.checkBox53.UseVisualStyleBackColor = true;
            // 
            // checkBox54
            // 
            this.checkBox54.AutoSize = true;
            this.checkBox54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox54.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox54.Location = new System.Drawing.Point(509, 505);
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new System.Drawing.Size(15, 14);
            this.checkBox54.TabIndex = 121;
            this.checkBox54.UseVisualStyleBackColor = false;
            // 
            // checkBox55
            // 
            this.checkBox55.AutoSize = true;
            this.checkBox55.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox55.Location = new System.Drawing.Point(607, 475);
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new System.Drawing.Size(15, 14);
            this.checkBox55.TabIndex = 120;
            this.checkBox55.UseVisualStyleBackColor = true;
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox56.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox56.Location = new System.Drawing.Point(557, 475);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(15, 14);
            this.checkBox56.TabIndex = 119;
            this.checkBox56.UseVisualStyleBackColor = false;
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox57.Location = new System.Drawing.Point(509, 475);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(15, 14);
            this.checkBox57.TabIndex = 118;
            this.checkBox57.UseVisualStyleBackColor = true;
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox58.Location = new System.Drawing.Point(607, 445);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(15, 14);
            this.checkBox58.TabIndex = 117;
            this.checkBox58.UseVisualStyleBackColor = true;
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox59.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox59.Location = new System.Drawing.Point(557, 445);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(15, 14);
            this.checkBox59.TabIndex = 116;
            this.checkBox59.UseVisualStyleBackColor = false;
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox60.Location = new System.Drawing.Point(509, 445);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(15, 14);
            this.checkBox60.TabIndex = 115;
            this.checkBox60.UseVisualStyleBackColor = true;
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox61.Location = new System.Drawing.Point(607, 415);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(15, 14);
            this.checkBox61.TabIndex = 114;
            this.checkBox61.UseVisualStyleBackColor = true;
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox62.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox62.Location = new System.Drawing.Point(557, 415);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(15, 14);
            this.checkBox62.TabIndex = 113;
            this.checkBox62.UseVisualStyleBackColor = false;
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox63.Location = new System.Drawing.Point(509, 415);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(15, 14);
            this.checkBox63.TabIndex = 112;
            this.checkBox63.UseVisualStyleBackColor = true;
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox64.Location = new System.Drawing.Point(607, 385);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(15, 14);
            this.checkBox64.TabIndex = 111;
            this.checkBox64.UseVisualStyleBackColor = true;
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox65.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox65.Location = new System.Drawing.Point(557, 385);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(15, 14);
            this.checkBox65.TabIndex = 110;
            this.checkBox65.UseVisualStyleBackColor = false;
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox66.Location = new System.Drawing.Point(509, 385);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(15, 14);
            this.checkBox66.TabIndex = 109;
            this.checkBox66.UseVisualStyleBackColor = true;
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox67.Location = new System.Drawing.Point(607, 355);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(15, 14);
            this.checkBox67.TabIndex = 108;
            this.checkBox67.UseVisualStyleBackColor = true;
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox68.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox68.Location = new System.Drawing.Point(557, 355);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(15, 14);
            this.checkBox68.TabIndex = 107;
            this.checkBox68.UseVisualStyleBackColor = false;
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox69.Location = new System.Drawing.Point(509, 355);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(15, 14);
            this.checkBox69.TabIndex = 106;
            this.checkBox69.UseVisualStyleBackColor = true;
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.BackColor = System.Drawing.Color.Green;
            this.checkBox70.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox70.Location = new System.Drawing.Point(607, 325);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(15, 14);
            this.checkBox70.TabIndex = 105;
            this.checkBox70.UseVisualStyleBackColor = false;
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox71.Location = new System.Drawing.Point(557, 325);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(15, 14);
            this.checkBox71.TabIndex = 104;
            this.checkBox71.UseVisualStyleBackColor = true;
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox72.Location = new System.Drawing.Point(509, 325);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(15, 14);
            this.checkBox72.TabIndex = 103;
            this.checkBox72.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label51.Location = new System.Drawing.Point(356, 647);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(133, 25);
            this.label51.TabIndex = 102;
            this.label51.Text = "12:30~13:00";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label52.Location = new System.Drawing.Point(356, 617);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(133, 25);
            this.label52.TabIndex = 101;
            this.label52.Text = "12:00~12:30";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label53.Location = new System.Drawing.Point(356, 587);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(133, 25);
            this.label53.TabIndex = 100;
            this.label53.Text = "11:30~12:00";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label54.Location = new System.Drawing.Point(356, 557);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(133, 25);
            this.label54.TabIndex = 99;
            this.label54.Text = "11:00~11:30";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label55.Location = new System.Drawing.Point(356, 527);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(133, 25);
            this.label55.TabIndex = 98;
            this.label55.Text = "10:30~11:00";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(356, 497);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(133, 25);
            this.label56.TabIndex = 97;
            this.label56.Text = "10:00~10:30";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label57.Location = new System.Drawing.Point(356, 467);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(133, 25);
            this.label57.TabIndex = 96;
            this.label57.Text = "09:30~10:00";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label58.Location = new System.Drawing.Point(356, 437);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(133, 25);
            this.label58.TabIndex = 95;
            this.label58.Text = "09:00~09:30";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label59.Location = new System.Drawing.Point(356, 407);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(133, 25);
            this.label59.TabIndex = 94;
            this.label59.Text = "08:30~09:00";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label60.Location = new System.Drawing.Point(356, 377);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(133, 25);
            this.label60.TabIndex = 93;
            this.label60.Text = "08:00~08:30";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label61.Location = new System.Drawing.Point(356, 347);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(133, 25);
            this.label61.TabIndex = 92;
            this.label61.Text = "07:30~08:00";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label62.Location = new System.Drawing.Point(356, 317);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(133, 25);
            this.label62.TabIndex = 91;
            this.label62.Text = "07:00~07:30";
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox73.Location = new System.Drawing.Point(926, 655);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(15, 14);
            this.checkBox73.TabIndex = 186;
            this.checkBox73.UseVisualStyleBackColor = true;
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox74.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox74.Location = new System.Drawing.Point(876, 655);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(15, 14);
            this.checkBox74.TabIndex = 185;
            this.checkBox74.UseVisualStyleBackColor = false;
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox75.Location = new System.Drawing.Point(828, 655);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(15, 14);
            this.checkBox75.TabIndex = 184;
            this.checkBox75.UseVisualStyleBackColor = true;
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox76.Location = new System.Drawing.Point(926, 625);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(15, 14);
            this.checkBox76.TabIndex = 183;
            this.checkBox76.UseVisualStyleBackColor = true;
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox77.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox77.Location = new System.Drawing.Point(876, 625);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(15, 14);
            this.checkBox77.TabIndex = 182;
            this.checkBox77.UseVisualStyleBackColor = false;
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox78.Location = new System.Drawing.Point(828, 625);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(15, 14);
            this.checkBox78.TabIndex = 181;
            this.checkBox78.UseVisualStyleBackColor = true;
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox79.Location = new System.Drawing.Point(926, 595);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(15, 14);
            this.checkBox79.TabIndex = 180;
            this.checkBox79.UseVisualStyleBackColor = true;
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox80.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox80.Location = new System.Drawing.Point(876, 595);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(15, 14);
            this.checkBox80.TabIndex = 179;
            this.checkBox80.UseVisualStyleBackColor = false;
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox81.Location = new System.Drawing.Point(828, 595);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(15, 14);
            this.checkBox81.TabIndex = 178;
            this.checkBox81.UseVisualStyleBackColor = true;
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox82.Location = new System.Drawing.Point(926, 565);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(15, 14);
            this.checkBox82.TabIndex = 177;
            this.checkBox82.UseVisualStyleBackColor = true;
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox83.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox83.Location = new System.Drawing.Point(876, 565);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(15, 14);
            this.checkBox83.TabIndex = 176;
            this.checkBox83.UseVisualStyleBackColor = false;
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox84.Location = new System.Drawing.Point(828, 565);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(15, 14);
            this.checkBox84.TabIndex = 175;
            this.checkBox84.UseVisualStyleBackColor = true;
            // 
            // checkBox85
            // 
            this.checkBox85.AutoSize = true;
            this.checkBox85.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox85.Location = new System.Drawing.Point(926, 535);
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new System.Drawing.Size(15, 14);
            this.checkBox85.TabIndex = 174;
            this.checkBox85.UseVisualStyleBackColor = true;
            // 
            // checkBox86
            // 
            this.checkBox86.AutoSize = true;
            this.checkBox86.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox86.Location = new System.Drawing.Point(876, 535);
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new System.Drawing.Size(15, 14);
            this.checkBox86.TabIndex = 173;
            this.checkBox86.UseVisualStyleBackColor = true;
            // 
            // checkBox87
            // 
            this.checkBox87.AutoSize = true;
            this.checkBox87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox87.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox87.Location = new System.Drawing.Point(828, 535);
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new System.Drawing.Size(15, 14);
            this.checkBox87.TabIndex = 172;
            this.checkBox87.UseVisualStyleBackColor = false;
            // 
            // checkBox88
            // 
            this.checkBox88.AutoSize = true;
            this.checkBox88.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox88.Location = new System.Drawing.Point(926, 505);
            this.checkBox88.Name = "checkBox88";
            this.checkBox88.Size = new System.Drawing.Size(15, 14);
            this.checkBox88.TabIndex = 171;
            this.checkBox88.UseVisualStyleBackColor = true;
            // 
            // checkBox89
            // 
            this.checkBox89.AutoSize = true;
            this.checkBox89.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox89.Location = new System.Drawing.Point(876, 505);
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new System.Drawing.Size(15, 14);
            this.checkBox89.TabIndex = 170;
            this.checkBox89.UseVisualStyleBackColor = true;
            // 
            // checkBox90
            // 
            this.checkBox90.AutoSize = true;
            this.checkBox90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox90.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox90.Location = new System.Drawing.Point(828, 505);
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new System.Drawing.Size(15, 14);
            this.checkBox90.TabIndex = 169;
            this.checkBox90.UseVisualStyleBackColor = false;
            // 
            // checkBox91
            // 
            this.checkBox91.AutoSize = true;
            this.checkBox91.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox91.Location = new System.Drawing.Point(926, 475);
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new System.Drawing.Size(15, 14);
            this.checkBox91.TabIndex = 168;
            this.checkBox91.UseVisualStyleBackColor = true;
            // 
            // checkBox92
            // 
            this.checkBox92.AutoSize = true;
            this.checkBox92.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox92.Location = new System.Drawing.Point(876, 475);
            this.checkBox92.Name = "checkBox92";
            this.checkBox92.Size = new System.Drawing.Size(15, 14);
            this.checkBox92.TabIndex = 167;
            this.checkBox92.UseVisualStyleBackColor = true;
            // 
            // checkBox93
            // 
            this.checkBox93.AutoSize = true;
            this.checkBox93.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox93.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox93.Location = new System.Drawing.Point(828, 475);
            this.checkBox93.Name = "checkBox93";
            this.checkBox93.Size = new System.Drawing.Size(15, 14);
            this.checkBox93.TabIndex = 166;
            this.checkBox93.UseVisualStyleBackColor = false;
            // 
            // checkBox94
            // 
            this.checkBox94.AutoSize = true;
            this.checkBox94.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox94.Location = new System.Drawing.Point(926, 445);
            this.checkBox94.Name = "checkBox94";
            this.checkBox94.Size = new System.Drawing.Size(15, 14);
            this.checkBox94.TabIndex = 165;
            this.checkBox94.UseVisualStyleBackColor = true;
            // 
            // checkBox95
            // 
            this.checkBox95.AutoSize = true;
            this.checkBox95.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox95.Location = new System.Drawing.Point(876, 445);
            this.checkBox95.Name = "checkBox95";
            this.checkBox95.Size = new System.Drawing.Size(15, 14);
            this.checkBox95.TabIndex = 164;
            this.checkBox95.UseVisualStyleBackColor = true;
            // 
            // checkBox96
            // 
            this.checkBox96.AutoSize = true;
            this.checkBox96.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox96.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox96.Location = new System.Drawing.Point(828, 445);
            this.checkBox96.Name = "checkBox96";
            this.checkBox96.Size = new System.Drawing.Size(15, 14);
            this.checkBox96.TabIndex = 163;
            this.checkBox96.UseVisualStyleBackColor = false;
            // 
            // checkBox97
            // 
            this.checkBox97.AutoSize = true;
            this.checkBox97.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox97.Location = new System.Drawing.Point(926, 415);
            this.checkBox97.Name = "checkBox97";
            this.checkBox97.Size = new System.Drawing.Size(15, 14);
            this.checkBox97.TabIndex = 162;
            this.checkBox97.UseVisualStyleBackColor = true;
            // 
            // checkBox98
            // 
            this.checkBox98.AutoSize = true;
            this.checkBox98.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox98.Location = new System.Drawing.Point(876, 415);
            this.checkBox98.Name = "checkBox98";
            this.checkBox98.Size = new System.Drawing.Size(15, 14);
            this.checkBox98.TabIndex = 161;
            this.checkBox98.UseVisualStyleBackColor = true;
            // 
            // checkBox99
            // 
            this.checkBox99.AutoSize = true;
            this.checkBox99.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox99.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox99.Location = new System.Drawing.Point(828, 415);
            this.checkBox99.Name = "checkBox99";
            this.checkBox99.Size = new System.Drawing.Size(15, 14);
            this.checkBox99.TabIndex = 160;
            this.checkBox99.UseVisualStyleBackColor = false;
            // 
            // checkBox100
            // 
            this.checkBox100.AutoSize = true;
            this.checkBox100.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox100.Location = new System.Drawing.Point(926, 385);
            this.checkBox100.Name = "checkBox100";
            this.checkBox100.Size = new System.Drawing.Size(15, 14);
            this.checkBox100.TabIndex = 159;
            this.checkBox100.UseVisualStyleBackColor = true;
            // 
            // checkBox101
            // 
            this.checkBox101.AutoSize = true;
            this.checkBox101.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox101.Location = new System.Drawing.Point(876, 385);
            this.checkBox101.Name = "checkBox101";
            this.checkBox101.Size = new System.Drawing.Size(15, 14);
            this.checkBox101.TabIndex = 158;
            this.checkBox101.UseVisualStyleBackColor = true;
            // 
            // checkBox102
            // 
            this.checkBox102.AutoSize = true;
            this.checkBox102.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox102.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox102.Location = new System.Drawing.Point(828, 385);
            this.checkBox102.Name = "checkBox102";
            this.checkBox102.Size = new System.Drawing.Size(15, 14);
            this.checkBox102.TabIndex = 157;
            this.checkBox102.UseVisualStyleBackColor = false;
            // 
            // checkBox103
            // 
            this.checkBox103.AutoSize = true;
            this.checkBox103.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox103.Location = new System.Drawing.Point(926, 355);
            this.checkBox103.Name = "checkBox103";
            this.checkBox103.Size = new System.Drawing.Size(15, 14);
            this.checkBox103.TabIndex = 156;
            this.checkBox103.UseVisualStyleBackColor = true;
            // 
            // checkBox104
            // 
            this.checkBox104.AutoSize = true;
            this.checkBox104.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox104.Location = new System.Drawing.Point(876, 355);
            this.checkBox104.Name = "checkBox104";
            this.checkBox104.Size = new System.Drawing.Size(15, 14);
            this.checkBox104.TabIndex = 155;
            this.checkBox104.UseVisualStyleBackColor = true;
            // 
            // checkBox105
            // 
            this.checkBox105.AutoSize = true;
            this.checkBox105.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox105.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox105.Location = new System.Drawing.Point(828, 355);
            this.checkBox105.Name = "checkBox105";
            this.checkBox105.Size = new System.Drawing.Size(15, 14);
            this.checkBox105.TabIndex = 154;
            this.checkBox105.UseVisualStyleBackColor = false;
            // 
            // checkBox106
            // 
            this.checkBox106.AutoSize = true;
            this.checkBox106.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox106.Location = new System.Drawing.Point(926, 325);
            this.checkBox106.Name = "checkBox106";
            this.checkBox106.Size = new System.Drawing.Size(15, 14);
            this.checkBox106.TabIndex = 153;
            this.checkBox106.UseVisualStyleBackColor = true;
            // 
            // checkBox107
            // 
            this.checkBox107.AutoSize = true;
            this.checkBox107.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox107.Location = new System.Drawing.Point(876, 325);
            this.checkBox107.Name = "checkBox107";
            this.checkBox107.Size = new System.Drawing.Size(15, 14);
            this.checkBox107.TabIndex = 152;
            this.checkBox107.UseVisualStyleBackColor = true;
            // 
            // checkBox108
            // 
            this.checkBox108.AutoSize = true;
            this.checkBox108.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.checkBox108.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox108.Location = new System.Drawing.Point(828, 325);
            this.checkBox108.Name = "checkBox108";
            this.checkBox108.Size = new System.Drawing.Size(15, 14);
            this.checkBox108.TabIndex = 151;
            this.checkBox108.UseVisualStyleBackColor = false;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label63.Location = new System.Drawing.Point(675, 647);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(133, 25);
            this.label63.TabIndex = 150;
            this.label63.Text = "18:30~19:00";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label64.Location = new System.Drawing.Point(675, 617);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(133, 25);
            this.label64.TabIndex = 149;
            this.label64.Text = "18:00~18:30";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label65.Location = new System.Drawing.Point(675, 587);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(133, 25);
            this.label65.TabIndex = 148;
            this.label65.Text = "17:30~18:00";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label66.Location = new System.Drawing.Point(675, 557);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(133, 25);
            this.label66.TabIndex = 147;
            this.label66.Text = "17:00~17:30";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label67.Location = new System.Drawing.Point(675, 527);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(133, 25);
            this.label67.TabIndex = 146;
            this.label67.Text = "16:30~17:00";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label68.Location = new System.Drawing.Point(675, 497);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(133, 25);
            this.label68.TabIndex = 145;
            this.label68.Text = "16:00~16:30";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label69.Location = new System.Drawing.Point(675, 467);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(133, 25);
            this.label69.TabIndex = 144;
            this.label69.Text = "15:30~16:00";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label70.Location = new System.Drawing.Point(675, 437);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(133, 25);
            this.label70.TabIndex = 143;
            this.label70.Text = "15:00~15:30";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label71.Location = new System.Drawing.Point(675, 407);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(133, 25);
            this.label71.TabIndex = 142;
            this.label71.Text = "14:30~15:00";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label72.Location = new System.Drawing.Point(675, 377);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(133, 25);
            this.label72.TabIndex = 141;
            this.label72.Text = "14:00~14:30";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label73.Location = new System.Drawing.Point(675, 347);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(133, 25);
            this.label73.TabIndex = 140;
            this.label73.Text = "13:30~14:00";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label74.Location = new System.Drawing.Point(675, 317);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(133, 25);
            this.label74.TabIndex = 139;
            this.label74.Text = "13:00~13:30";
            // 
            // checkBox109
            // 
            this.checkBox109.AutoSize = true;
            this.checkBox109.BackColor = System.Drawing.Color.Green;
            this.checkBox109.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox109.Location = new System.Drawing.Point(1246, 655);
            this.checkBox109.Name = "checkBox109";
            this.checkBox109.Size = new System.Drawing.Size(15, 14);
            this.checkBox109.TabIndex = 234;
            this.checkBox109.UseVisualStyleBackColor = false;
            // 
            // checkBox110
            // 
            this.checkBox110.AutoSize = true;
            this.checkBox110.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox110.Location = new System.Drawing.Point(1196, 655);
            this.checkBox110.Name = "checkBox110";
            this.checkBox110.Size = new System.Drawing.Size(15, 14);
            this.checkBox110.TabIndex = 233;
            this.checkBox110.UseVisualStyleBackColor = true;
            // 
            // checkBox111
            // 
            this.checkBox111.AutoSize = true;
            this.checkBox111.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox111.Location = new System.Drawing.Point(1148, 655);
            this.checkBox111.Name = "checkBox111";
            this.checkBox111.Size = new System.Drawing.Size(15, 14);
            this.checkBox111.TabIndex = 232;
            this.checkBox111.UseVisualStyleBackColor = true;
            // 
            // checkBox112
            // 
            this.checkBox112.AutoSize = true;
            this.checkBox112.BackColor = System.Drawing.Color.Green;
            this.checkBox112.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox112.Location = new System.Drawing.Point(1246, 625);
            this.checkBox112.Name = "checkBox112";
            this.checkBox112.Size = new System.Drawing.Size(15, 14);
            this.checkBox112.TabIndex = 231;
            this.checkBox112.UseVisualStyleBackColor = false;
            // 
            // checkBox113
            // 
            this.checkBox113.AutoSize = true;
            this.checkBox113.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox113.Location = new System.Drawing.Point(1196, 625);
            this.checkBox113.Name = "checkBox113";
            this.checkBox113.Size = new System.Drawing.Size(15, 14);
            this.checkBox113.TabIndex = 230;
            this.checkBox113.UseVisualStyleBackColor = true;
            // 
            // checkBox114
            // 
            this.checkBox114.AutoSize = true;
            this.checkBox114.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox114.Location = new System.Drawing.Point(1148, 625);
            this.checkBox114.Name = "checkBox114";
            this.checkBox114.Size = new System.Drawing.Size(15, 14);
            this.checkBox114.TabIndex = 229;
            this.checkBox114.UseVisualStyleBackColor = true;
            // 
            // checkBox115
            // 
            this.checkBox115.AutoSize = true;
            this.checkBox115.BackColor = System.Drawing.Color.Green;
            this.checkBox115.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox115.Location = new System.Drawing.Point(1246, 595);
            this.checkBox115.Name = "checkBox115";
            this.checkBox115.Size = new System.Drawing.Size(15, 14);
            this.checkBox115.TabIndex = 228;
            this.checkBox115.UseVisualStyleBackColor = false;
            // 
            // checkBox116
            // 
            this.checkBox116.AutoSize = true;
            this.checkBox116.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox116.Location = new System.Drawing.Point(1196, 595);
            this.checkBox116.Name = "checkBox116";
            this.checkBox116.Size = new System.Drawing.Size(15, 14);
            this.checkBox116.TabIndex = 227;
            this.checkBox116.UseVisualStyleBackColor = true;
            // 
            // checkBox117
            // 
            this.checkBox117.AutoSize = true;
            this.checkBox117.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox117.Location = new System.Drawing.Point(1148, 595);
            this.checkBox117.Name = "checkBox117";
            this.checkBox117.Size = new System.Drawing.Size(15, 14);
            this.checkBox117.TabIndex = 226;
            this.checkBox117.UseVisualStyleBackColor = true;
            // 
            // checkBox118
            // 
            this.checkBox118.AutoSize = true;
            this.checkBox118.BackColor = System.Drawing.Color.Green;
            this.checkBox118.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox118.Location = new System.Drawing.Point(1246, 565);
            this.checkBox118.Name = "checkBox118";
            this.checkBox118.Size = new System.Drawing.Size(15, 14);
            this.checkBox118.TabIndex = 225;
            this.checkBox118.UseVisualStyleBackColor = false;
            // 
            // checkBox119
            // 
            this.checkBox119.AutoSize = true;
            this.checkBox119.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox119.Location = new System.Drawing.Point(1196, 565);
            this.checkBox119.Name = "checkBox119";
            this.checkBox119.Size = new System.Drawing.Size(15, 14);
            this.checkBox119.TabIndex = 224;
            this.checkBox119.UseVisualStyleBackColor = true;
            // 
            // checkBox120
            // 
            this.checkBox120.AutoSize = true;
            this.checkBox120.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox120.Location = new System.Drawing.Point(1148, 565);
            this.checkBox120.Name = "checkBox120";
            this.checkBox120.Size = new System.Drawing.Size(15, 14);
            this.checkBox120.TabIndex = 223;
            this.checkBox120.UseVisualStyleBackColor = true;
            // 
            // checkBox121
            // 
            this.checkBox121.AutoSize = true;
            this.checkBox121.BackColor = System.Drawing.Color.Green;
            this.checkBox121.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox121.Location = new System.Drawing.Point(1246, 535);
            this.checkBox121.Name = "checkBox121";
            this.checkBox121.Size = new System.Drawing.Size(15, 14);
            this.checkBox121.TabIndex = 222;
            this.checkBox121.UseVisualStyleBackColor = false;
            // 
            // checkBox122
            // 
            this.checkBox122.AutoSize = true;
            this.checkBox122.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox122.Location = new System.Drawing.Point(1196, 535);
            this.checkBox122.Name = "checkBox122";
            this.checkBox122.Size = new System.Drawing.Size(15, 14);
            this.checkBox122.TabIndex = 221;
            this.checkBox122.UseVisualStyleBackColor = true;
            // 
            // checkBox123
            // 
            this.checkBox123.AutoSize = true;
            this.checkBox123.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox123.Location = new System.Drawing.Point(1148, 535);
            this.checkBox123.Name = "checkBox123";
            this.checkBox123.Size = new System.Drawing.Size(15, 14);
            this.checkBox123.TabIndex = 220;
            this.checkBox123.UseVisualStyleBackColor = true;
            // 
            // checkBox124
            // 
            this.checkBox124.AutoSize = true;
            this.checkBox124.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox124.Location = new System.Drawing.Point(1246, 505);
            this.checkBox124.Name = "checkBox124";
            this.checkBox124.Size = new System.Drawing.Size(15, 14);
            this.checkBox124.TabIndex = 219;
            this.checkBox124.UseVisualStyleBackColor = true;
            // 
            // checkBox125
            // 
            this.checkBox125.AutoSize = true;
            this.checkBox125.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox125.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox125.Location = new System.Drawing.Point(1196, 505);
            this.checkBox125.Name = "checkBox125";
            this.checkBox125.Size = new System.Drawing.Size(15, 14);
            this.checkBox125.TabIndex = 218;
            this.checkBox125.UseVisualStyleBackColor = false;
            // 
            // checkBox126
            // 
            this.checkBox126.AutoSize = true;
            this.checkBox126.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox126.Location = new System.Drawing.Point(1148, 505);
            this.checkBox126.Name = "checkBox126";
            this.checkBox126.Size = new System.Drawing.Size(15, 14);
            this.checkBox126.TabIndex = 217;
            this.checkBox126.UseVisualStyleBackColor = true;
            // 
            // checkBox127
            // 
            this.checkBox127.AutoSize = true;
            this.checkBox127.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox127.Location = new System.Drawing.Point(1246, 475);
            this.checkBox127.Name = "checkBox127";
            this.checkBox127.Size = new System.Drawing.Size(15, 14);
            this.checkBox127.TabIndex = 216;
            this.checkBox127.UseVisualStyleBackColor = true;
            // 
            // checkBox128
            // 
            this.checkBox128.AutoSize = true;
            this.checkBox128.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox128.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox128.Location = new System.Drawing.Point(1196, 475);
            this.checkBox128.Name = "checkBox128";
            this.checkBox128.Size = new System.Drawing.Size(15, 14);
            this.checkBox128.TabIndex = 215;
            this.checkBox128.UseVisualStyleBackColor = false;
            // 
            // checkBox129
            // 
            this.checkBox129.AutoSize = true;
            this.checkBox129.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox129.Location = new System.Drawing.Point(1148, 475);
            this.checkBox129.Name = "checkBox129";
            this.checkBox129.Size = new System.Drawing.Size(15, 14);
            this.checkBox129.TabIndex = 214;
            this.checkBox129.UseVisualStyleBackColor = true;
            // 
            // checkBox130
            // 
            this.checkBox130.AutoSize = true;
            this.checkBox130.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox130.Location = new System.Drawing.Point(1246, 445);
            this.checkBox130.Name = "checkBox130";
            this.checkBox130.Size = new System.Drawing.Size(15, 14);
            this.checkBox130.TabIndex = 213;
            this.checkBox130.UseVisualStyleBackColor = true;
            // 
            // checkBox131
            // 
            this.checkBox131.AutoSize = true;
            this.checkBox131.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox131.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox131.Location = new System.Drawing.Point(1196, 445);
            this.checkBox131.Name = "checkBox131";
            this.checkBox131.Size = new System.Drawing.Size(15, 14);
            this.checkBox131.TabIndex = 212;
            this.checkBox131.UseVisualStyleBackColor = false;
            // 
            // checkBox132
            // 
            this.checkBox132.AutoSize = true;
            this.checkBox132.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox132.Location = new System.Drawing.Point(1148, 445);
            this.checkBox132.Name = "checkBox132";
            this.checkBox132.Size = new System.Drawing.Size(15, 14);
            this.checkBox132.TabIndex = 211;
            this.checkBox132.UseVisualStyleBackColor = true;
            // 
            // checkBox133
            // 
            this.checkBox133.AutoSize = true;
            this.checkBox133.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox133.Location = new System.Drawing.Point(1246, 415);
            this.checkBox133.Name = "checkBox133";
            this.checkBox133.Size = new System.Drawing.Size(15, 14);
            this.checkBox133.TabIndex = 210;
            this.checkBox133.UseVisualStyleBackColor = true;
            // 
            // checkBox134
            // 
            this.checkBox134.AutoSize = true;
            this.checkBox134.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox134.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox134.Location = new System.Drawing.Point(1196, 415);
            this.checkBox134.Name = "checkBox134";
            this.checkBox134.Size = new System.Drawing.Size(15, 14);
            this.checkBox134.TabIndex = 209;
            this.checkBox134.UseVisualStyleBackColor = false;
            // 
            // checkBox135
            // 
            this.checkBox135.AutoSize = true;
            this.checkBox135.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox135.Location = new System.Drawing.Point(1148, 415);
            this.checkBox135.Name = "checkBox135";
            this.checkBox135.Size = new System.Drawing.Size(15, 14);
            this.checkBox135.TabIndex = 208;
            this.checkBox135.UseVisualStyleBackColor = true;
            // 
            // checkBox136
            // 
            this.checkBox136.AutoSize = true;
            this.checkBox136.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox136.Location = new System.Drawing.Point(1246, 385);
            this.checkBox136.Name = "checkBox136";
            this.checkBox136.Size = new System.Drawing.Size(15, 14);
            this.checkBox136.TabIndex = 207;
            this.checkBox136.UseVisualStyleBackColor = true;
            // 
            // checkBox137
            // 
            this.checkBox137.AutoSize = true;
            this.checkBox137.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox137.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox137.Location = new System.Drawing.Point(1196, 385);
            this.checkBox137.Name = "checkBox137";
            this.checkBox137.Size = new System.Drawing.Size(15, 14);
            this.checkBox137.TabIndex = 206;
            this.checkBox137.UseVisualStyleBackColor = false;
            // 
            // checkBox138
            // 
            this.checkBox138.AutoSize = true;
            this.checkBox138.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox138.Location = new System.Drawing.Point(1148, 385);
            this.checkBox138.Name = "checkBox138";
            this.checkBox138.Size = new System.Drawing.Size(15, 14);
            this.checkBox138.TabIndex = 205;
            this.checkBox138.UseVisualStyleBackColor = true;
            // 
            // checkBox139
            // 
            this.checkBox139.AutoSize = true;
            this.checkBox139.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox139.Location = new System.Drawing.Point(1246, 355);
            this.checkBox139.Name = "checkBox139";
            this.checkBox139.Size = new System.Drawing.Size(15, 14);
            this.checkBox139.TabIndex = 204;
            this.checkBox139.UseVisualStyleBackColor = true;
            // 
            // checkBox140
            // 
            this.checkBox140.AutoSize = true;
            this.checkBox140.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox140.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox140.Location = new System.Drawing.Point(1196, 355);
            this.checkBox140.Name = "checkBox140";
            this.checkBox140.Size = new System.Drawing.Size(15, 14);
            this.checkBox140.TabIndex = 203;
            this.checkBox140.UseVisualStyleBackColor = false;
            // 
            // checkBox141
            // 
            this.checkBox141.AutoSize = true;
            this.checkBox141.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox141.Location = new System.Drawing.Point(1148, 355);
            this.checkBox141.Name = "checkBox141";
            this.checkBox141.Size = new System.Drawing.Size(15, 14);
            this.checkBox141.TabIndex = 202;
            this.checkBox141.UseVisualStyleBackColor = true;
            // 
            // checkBox142
            // 
            this.checkBox142.AutoSize = true;
            this.checkBox142.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox142.Location = new System.Drawing.Point(1246, 325);
            this.checkBox142.Name = "checkBox142";
            this.checkBox142.Size = new System.Drawing.Size(15, 14);
            this.checkBox142.TabIndex = 201;
            this.checkBox142.UseVisualStyleBackColor = true;
            // 
            // checkBox143
            // 
            this.checkBox143.AutoSize = true;
            this.checkBox143.BackColor = System.Drawing.Color.DarkKhaki;
            this.checkBox143.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox143.Location = new System.Drawing.Point(1196, 325);
            this.checkBox143.Name = "checkBox143";
            this.checkBox143.Size = new System.Drawing.Size(15, 14);
            this.checkBox143.TabIndex = 200;
            this.checkBox143.UseVisualStyleBackColor = false;
            // 
            // checkBox144
            // 
            this.checkBox144.AutoSize = true;
            this.checkBox144.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox144.Location = new System.Drawing.Point(1148, 325);
            this.checkBox144.Name = "checkBox144";
            this.checkBox144.Size = new System.Drawing.Size(15, 14);
            this.checkBox144.TabIndex = 199;
            this.checkBox144.UseVisualStyleBackColor = true;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label75.Location = new System.Drawing.Point(995, 647);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(133, 25);
            this.label75.TabIndex = 198;
            this.label75.Text = "24:30~01:00";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label76.Location = new System.Drawing.Point(995, 617);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(133, 25);
            this.label76.TabIndex = 197;
            this.label76.Text = "24:00~24:30";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label77.Location = new System.Drawing.Point(995, 587);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(133, 25);
            this.label77.TabIndex = 196;
            this.label77.Text = "23:30~24:00";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label78.Location = new System.Drawing.Point(995, 557);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(133, 25);
            this.label78.TabIndex = 195;
            this.label78.Text = "23:00~23:30";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label79.Location = new System.Drawing.Point(995, 527);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(133, 25);
            this.label79.TabIndex = 194;
            this.label79.Text = "22:30~23:00";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label80.Location = new System.Drawing.Point(995, 497);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(133, 25);
            this.label80.TabIndex = 193;
            this.label80.Text = "22:00~22:30";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label81.Location = new System.Drawing.Point(995, 467);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(133, 25);
            this.label81.TabIndex = 192;
            this.label81.Text = "21:30~22:00";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label82.Location = new System.Drawing.Point(995, 437);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(133, 25);
            this.label82.TabIndex = 191;
            this.label82.Text = "21:00~21:30";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label83.Location = new System.Drawing.Point(995, 407);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(133, 25);
            this.label83.TabIndex = 190;
            this.label83.Text = "20:30~21:00";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label84.Location = new System.Drawing.Point(995, 377);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(133, 25);
            this.label84.TabIndex = 189;
            this.label84.Text = "20:00~20:30";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label85.Location = new System.Drawing.Point(995, 347);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(133, 25);
            this.label85.TabIndex = 188;
            this.label85.Text = "19:30~20:00";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label86.Location = new System.Drawing.Point(995, 317);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(133, 25);
            this.label86.TabIndex = 187;
            this.label86.Text = "19:00~19:30";
            // 
            // checkBox146
            // 
            this.checkBox146.AutoSize = true;
            this.checkBox146.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox146.Location = new System.Drawing.Point(1493, 322);
            this.checkBox146.Name = "checkBox146";
            this.checkBox146.Size = new System.Drawing.Size(15, 14);
            this.checkBox146.TabIndex = 240;
            this.checkBox146.UseVisualStyleBackColor = true;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label88.Location = new System.Drawing.Point(1426, 317);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(44, 25);
            this.label88.TabIndex = 239;
            this.label88.Text = "1月";
            // 
            // checkBox145
            // 
            this.checkBox145.AutoSize = true;
            this.checkBox145.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox145.Location = new System.Drawing.Point(1493, 352);
            this.checkBox145.Name = "checkBox145";
            this.checkBox145.Size = new System.Drawing.Size(15, 14);
            this.checkBox145.TabIndex = 242;
            this.checkBox145.UseVisualStyleBackColor = true;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label87.Location = new System.Drawing.Point(1426, 347);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(44, 25);
            this.label87.TabIndex = 241;
            this.label87.Text = "2月";
            // 
            // checkBox147
            // 
            this.checkBox147.AutoSize = true;
            this.checkBox147.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox147.Location = new System.Drawing.Point(1493, 382);
            this.checkBox147.Name = "checkBox147";
            this.checkBox147.Size = new System.Drawing.Size(15, 14);
            this.checkBox147.TabIndex = 244;
            this.checkBox147.UseVisualStyleBackColor = true;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label89.Location = new System.Drawing.Point(1426, 377);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(44, 25);
            this.label89.TabIndex = 243;
            this.label89.Text = "3月";
            // 
            // checkBox148
            // 
            this.checkBox148.AutoSize = true;
            this.checkBox148.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox148.Location = new System.Drawing.Point(1493, 412);
            this.checkBox148.Name = "checkBox148";
            this.checkBox148.Size = new System.Drawing.Size(15, 14);
            this.checkBox148.TabIndex = 246;
            this.checkBox148.UseVisualStyleBackColor = true;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label90.Location = new System.Drawing.Point(1426, 407);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(44, 25);
            this.label90.TabIndex = 245;
            this.label90.Text = "4月";
            // 
            // checkBox149
            // 
            this.checkBox149.AutoSize = true;
            this.checkBox149.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox149.Location = new System.Drawing.Point(1493, 442);
            this.checkBox149.Name = "checkBox149";
            this.checkBox149.Size = new System.Drawing.Size(15, 14);
            this.checkBox149.TabIndex = 248;
            this.checkBox149.UseVisualStyleBackColor = true;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label91.Location = new System.Drawing.Point(1426, 437);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(44, 25);
            this.label91.TabIndex = 247;
            this.label91.Text = "5月";
            // 
            // checkBox150
            // 
            this.checkBox150.AutoSize = true;
            this.checkBox150.BackColor = System.Drawing.Color.DeepPink;
            this.checkBox150.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox150.Location = new System.Drawing.Point(1493, 472);
            this.checkBox150.Name = "checkBox150";
            this.checkBox150.Size = new System.Drawing.Size(15, 14);
            this.checkBox150.TabIndex = 250;
            this.checkBox150.UseVisualStyleBackColor = false;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label92.Location = new System.Drawing.Point(1426, 467);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(44, 25);
            this.label92.TabIndex = 249;
            this.label92.Text = "6月";
            // 
            // checkBox151
            // 
            this.checkBox151.AutoSize = true;
            this.checkBox151.BackColor = System.Drawing.Color.DeepPink;
            this.checkBox151.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox151.Location = new System.Drawing.Point(1493, 502);
            this.checkBox151.Name = "checkBox151";
            this.checkBox151.Size = new System.Drawing.Size(15, 14);
            this.checkBox151.TabIndex = 252;
            this.checkBox151.UseVisualStyleBackColor = false;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label93.Location = new System.Drawing.Point(1426, 497);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(44, 25);
            this.label93.TabIndex = 251;
            this.label93.Text = "7月";
            // 
            // checkBox152
            // 
            this.checkBox152.AutoSize = true;
            this.checkBox152.BackColor = System.Drawing.Color.DeepPink;
            this.checkBox152.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox152.Location = new System.Drawing.Point(1493, 532);
            this.checkBox152.Name = "checkBox152";
            this.checkBox152.Size = new System.Drawing.Size(15, 14);
            this.checkBox152.TabIndex = 254;
            this.checkBox152.UseVisualStyleBackColor = false;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label94.Location = new System.Drawing.Point(1426, 527);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(44, 25);
            this.label94.TabIndex = 253;
            this.label94.Text = "8月";
            // 
            // checkBox153
            // 
            this.checkBox153.AutoSize = true;
            this.checkBox153.BackColor = System.Drawing.Color.DeepPink;
            this.checkBox153.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox153.Location = new System.Drawing.Point(1493, 562);
            this.checkBox153.Name = "checkBox153";
            this.checkBox153.Size = new System.Drawing.Size(15, 14);
            this.checkBox153.TabIndex = 256;
            this.checkBox153.UseVisualStyleBackColor = false;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label95.Location = new System.Drawing.Point(1426, 557);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(44, 25);
            this.label95.TabIndex = 255;
            this.label95.Text = "9月";
            // 
            // checkBox154
            // 
            this.checkBox154.AutoSize = true;
            this.checkBox154.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox154.Location = new System.Drawing.Point(1493, 592);
            this.checkBox154.Name = "checkBox154";
            this.checkBox154.Size = new System.Drawing.Size(15, 14);
            this.checkBox154.TabIndex = 258;
            this.checkBox154.UseVisualStyleBackColor = true;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label96.Location = new System.Drawing.Point(1414, 587);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(56, 25);
            this.label96.TabIndex = 257;
            this.label96.Text = "10月";
            // 
            // checkBox155
            // 
            this.checkBox155.AutoSize = true;
            this.checkBox155.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox155.Location = new System.Drawing.Point(1493, 622);
            this.checkBox155.Name = "checkBox155";
            this.checkBox155.Size = new System.Drawing.Size(15, 14);
            this.checkBox155.TabIndex = 260;
            this.checkBox155.UseVisualStyleBackColor = true;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label97.Location = new System.Drawing.Point(1414, 617);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(56, 25);
            this.label97.TabIndex = 259;
            this.label97.Text = "11月";
            // 
            // checkBox156
            // 
            this.checkBox156.AutoSize = true;
            this.checkBox156.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox156.Location = new System.Drawing.Point(1493, 652);
            this.checkBox156.Name = "checkBox156";
            this.checkBox156.Size = new System.Drawing.Size(15, 14);
            this.checkBox156.TabIndex = 262;
            this.checkBox156.UseVisualStyleBackColor = true;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label98.Location = new System.Drawing.Point(1414, 647);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(56, 25);
            this.label98.TabIndex = 261;
            this.label98.Text = "12月";
            // 
            // Form_Basic_Electricity_Price_Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1617, 856);
            this.Controls.Add(this.checkBox156);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.checkBox155);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.checkBox154);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.checkBox153);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.checkBox152);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.checkBox151);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.checkBox150);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.checkBox149);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.checkBox148);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.checkBox147);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.checkBox145);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.checkBox146);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.checkBox109);
            this.Controls.Add(this.checkBox110);
            this.Controls.Add(this.checkBox111);
            this.Controls.Add(this.checkBox112);
            this.Controls.Add(this.checkBox113);
            this.Controls.Add(this.checkBox114);
            this.Controls.Add(this.checkBox115);
            this.Controls.Add(this.checkBox116);
            this.Controls.Add(this.checkBox117);
            this.Controls.Add(this.checkBox118);
            this.Controls.Add(this.checkBox119);
            this.Controls.Add(this.checkBox120);
            this.Controls.Add(this.checkBox121);
            this.Controls.Add(this.checkBox122);
            this.Controls.Add(this.checkBox123);
            this.Controls.Add(this.checkBox124);
            this.Controls.Add(this.checkBox125);
            this.Controls.Add(this.checkBox126);
            this.Controls.Add(this.checkBox127);
            this.Controls.Add(this.checkBox128);
            this.Controls.Add(this.checkBox129);
            this.Controls.Add(this.checkBox130);
            this.Controls.Add(this.checkBox131);
            this.Controls.Add(this.checkBox132);
            this.Controls.Add(this.checkBox133);
            this.Controls.Add(this.checkBox134);
            this.Controls.Add(this.checkBox135);
            this.Controls.Add(this.checkBox136);
            this.Controls.Add(this.checkBox137);
            this.Controls.Add(this.checkBox138);
            this.Controls.Add(this.checkBox139);
            this.Controls.Add(this.checkBox140);
            this.Controls.Add(this.checkBox141);
            this.Controls.Add(this.checkBox142);
            this.Controls.Add(this.checkBox143);
            this.Controls.Add(this.checkBox144);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.checkBox73);
            this.Controls.Add(this.checkBox74);
            this.Controls.Add(this.checkBox75);
            this.Controls.Add(this.checkBox76);
            this.Controls.Add(this.checkBox77);
            this.Controls.Add(this.checkBox78);
            this.Controls.Add(this.checkBox79);
            this.Controls.Add(this.checkBox80);
            this.Controls.Add(this.checkBox81);
            this.Controls.Add(this.checkBox82);
            this.Controls.Add(this.checkBox83);
            this.Controls.Add(this.checkBox84);
            this.Controls.Add(this.checkBox85);
            this.Controls.Add(this.checkBox86);
            this.Controls.Add(this.checkBox87);
            this.Controls.Add(this.checkBox88);
            this.Controls.Add(this.checkBox89);
            this.Controls.Add(this.checkBox90);
            this.Controls.Add(this.checkBox91);
            this.Controls.Add(this.checkBox92);
            this.Controls.Add(this.checkBox93);
            this.Controls.Add(this.checkBox94);
            this.Controls.Add(this.checkBox95);
            this.Controls.Add(this.checkBox96);
            this.Controls.Add(this.checkBox97);
            this.Controls.Add(this.checkBox98);
            this.Controls.Add(this.checkBox99);
            this.Controls.Add(this.checkBox100);
            this.Controls.Add(this.checkBox101);
            this.Controls.Add(this.checkBox102);
            this.Controls.Add(this.checkBox103);
            this.Controls.Add(this.checkBox104);
            this.Controls.Add(this.checkBox105);
            this.Controls.Add(this.checkBox106);
            this.Controls.Add(this.checkBox107);
            this.Controls.Add(this.checkBox108);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.checkBox37);
            this.Controls.Add(this.checkBox38);
            this.Controls.Add(this.checkBox39);
            this.Controls.Add(this.checkBox40);
            this.Controls.Add(this.checkBox41);
            this.Controls.Add(this.checkBox42);
            this.Controls.Add(this.checkBox43);
            this.Controls.Add(this.checkBox44);
            this.Controls.Add(this.checkBox45);
            this.Controls.Add(this.checkBox46);
            this.Controls.Add(this.checkBox47);
            this.Controls.Add(this.checkBox48);
            this.Controls.Add(this.checkBox49);
            this.Controls.Add(this.checkBox50);
            this.Controls.Add(this.checkBox51);
            this.Controls.Add(this.checkBox52);
            this.Controls.Add(this.checkBox53);
            this.Controls.Add(this.checkBox54);
            this.Controls.Add(this.checkBox55);
            this.Controls.Add(this.checkBox56);
            this.Controls.Add(this.checkBox57);
            this.Controls.Add(this.checkBox58);
            this.Controls.Add(this.checkBox59);
            this.Controls.Add(this.checkBox60);
            this.Controls.Add(this.checkBox61);
            this.Controls.Add(this.checkBox62);
            this.Controls.Add(this.checkBox63);
            this.Controls.Add(this.checkBox64);
            this.Controls.Add(this.checkBox65);
            this.Controls.Add(this.checkBox66);
            this.Controls.Add(this.checkBox67);
            this.Controls.Add(this.checkBox68);
            this.Controls.Add(this.checkBox69);
            this.Controls.Add(this.checkBox70);
            this.Controls.Add(this.checkBox71);
            this.Controls.Add(this.checkBox72);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.checkBox34);
            this.Controls.Add(this.checkBox35);
            this.Controls.Add(this.checkBox36);
            this.Controls.Add(this.checkBox31);
            this.Controls.Add(this.checkBox32);
            this.Controls.Add(this.checkBox33);
            this.Controls.Add(this.checkBox28);
            this.Controls.Add(this.checkBox29);
            this.Controls.Add(this.checkBox30);
            this.Controls.Add(this.checkBox25);
            this.Controls.Add(this.checkBox26);
            this.Controls.Add(this.checkBox27);
            this.Controls.Add(this.checkBox22);
            this.Controls.Add(this.checkBox23);
            this.Controls.Add(this.checkBox24);
            this.Controls.Add(this.checkBox19);
            this.Controls.Add(this.checkBox20);
            this.Controls.Add(this.checkBox21);
            this.Controls.Add(this.checkBox16);
            this.Controls.Add(this.checkBox17);
            this.Controls.Add(this.checkBox18);
            this.Controls.Add(this.checkBox13);
            this.Controls.Add(this.checkBox14);
            this.Controls.Add(this.checkBox15);
            this.Controls.Add(this.checkBox10);
            this.Controls.Add(this.checkBox11);
            this.Controls.Add(this.checkBox12);
            this.Controls.Add(this.checkBox7);
            this.Controls.Add(this.checkBox8);
            this.Controls.Add(this.checkBox9);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Basic_Electricity_Price_Management";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "         ";
            this.TopMost = false;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Basic_Electricity_Price_Management_Closed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.CheckBox checkBox44;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox checkBox46;
        private System.Windows.Forms.CheckBox checkBox47;
        private System.Windows.Forms.CheckBox checkBox48;
        private System.Windows.Forms.CheckBox checkBox49;
        private System.Windows.Forms.CheckBox checkBox50;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.CheckBox checkBox53;
        private System.Windows.Forms.CheckBox checkBox54;
        private System.Windows.Forms.CheckBox checkBox55;
        private System.Windows.Forms.CheckBox checkBox56;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.CheckBox checkBox85;
        private System.Windows.Forms.CheckBox checkBox86;
        private System.Windows.Forms.CheckBox checkBox87;
        private System.Windows.Forms.CheckBox checkBox88;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox92;
        private System.Windows.Forms.CheckBox checkBox93;
        private System.Windows.Forms.CheckBox checkBox94;
        private System.Windows.Forms.CheckBox checkBox95;
        private System.Windows.Forms.CheckBox checkBox96;
        private System.Windows.Forms.CheckBox checkBox97;
        private System.Windows.Forms.CheckBox checkBox98;
        private System.Windows.Forms.CheckBox checkBox99;
        private System.Windows.Forms.CheckBox checkBox100;
        private System.Windows.Forms.CheckBox checkBox101;
        private System.Windows.Forms.CheckBox checkBox102;
        private System.Windows.Forms.CheckBox checkBox103;
        private System.Windows.Forms.CheckBox checkBox104;
        private System.Windows.Forms.CheckBox checkBox105;
        private System.Windows.Forms.CheckBox checkBox106;
        private System.Windows.Forms.CheckBox checkBox107;
        private System.Windows.Forms.CheckBox checkBox108;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.CheckBox checkBox109;
        private System.Windows.Forms.CheckBox checkBox110;
        private System.Windows.Forms.CheckBox checkBox111;
        private System.Windows.Forms.CheckBox checkBox112;
        private System.Windows.Forms.CheckBox checkBox113;
        private System.Windows.Forms.CheckBox checkBox114;
        private System.Windows.Forms.CheckBox checkBox115;
        private System.Windows.Forms.CheckBox checkBox116;
        private System.Windows.Forms.CheckBox checkBox117;
        private System.Windows.Forms.CheckBox checkBox118;
        private System.Windows.Forms.CheckBox checkBox119;
        private System.Windows.Forms.CheckBox checkBox120;
        private System.Windows.Forms.CheckBox checkBox121;
        private System.Windows.Forms.CheckBox checkBox122;
        private System.Windows.Forms.CheckBox checkBox123;
        private System.Windows.Forms.CheckBox checkBox124;
        private System.Windows.Forms.CheckBox checkBox125;
        private System.Windows.Forms.CheckBox checkBox126;
        private System.Windows.Forms.CheckBox checkBox127;
        private System.Windows.Forms.CheckBox checkBox128;
        private System.Windows.Forms.CheckBox checkBox129;
        private System.Windows.Forms.CheckBox checkBox130;
        private System.Windows.Forms.CheckBox checkBox131;
        private System.Windows.Forms.CheckBox checkBox132;
        private System.Windows.Forms.CheckBox checkBox133;
        private System.Windows.Forms.CheckBox checkBox134;
        private System.Windows.Forms.CheckBox checkBox135;
        private System.Windows.Forms.CheckBox checkBox136;
        private System.Windows.Forms.CheckBox checkBox137;
        private System.Windows.Forms.CheckBox checkBox138;
        private System.Windows.Forms.CheckBox checkBox139;
        private System.Windows.Forms.CheckBox checkBox140;
        private System.Windows.Forms.CheckBox checkBox141;
        private System.Windows.Forms.CheckBox checkBox142;
        private System.Windows.Forms.CheckBox checkBox143;
        private System.Windows.Forms.CheckBox checkBox144;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.CheckBox checkBox146;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox checkBox145;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.CheckBox checkBox147;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.CheckBox checkBox148;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.CheckBox checkBox149;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.CheckBox checkBox150;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.CheckBox checkBox151;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.CheckBox checkBox152;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.CheckBox checkBox153;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.CheckBox checkBox154;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.CheckBox checkBox155;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.CheckBox checkBox156;
        private System.Windows.Forms.Label label98;
    }
}