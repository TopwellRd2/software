﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ice_storage_evaluation
{
    public partial class Form_Basic_Electricity_Price_Management : Form
    {
        private Form1 form1;
        public Form_Basic_Electricity_Price_Management(Form1 form)
        {
            InitializeComponent();
            form1 = form;
        }

        private void Form_Basic_Electricity_Price_Management_Closed(object sender, FormClosedEventArgs e)
        {
            if (form1.basicElectricityPriceManagement_CheckBox.Checked)
            {
                form1.basicElectricityPriceManagement_CheckBox.Checked = false;
            }
        }
    }
}
