﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

using UnitData = System.Collections.Generic.Dictionary<int, int[]>; //id:data(起停、設定轉速、警報...) 

namespace TOPWELL_INNOLUX_EDC1
{
    public class GatewayConnection
    {
        public static readonly ushort READ_GATEWAY_DEVICE_COUNT = 8; //一次讀取gateway的台數

        public string Ip { get; private set; }
        public bool ConnectState { get; set; }

        private String gatewayStr;
        private Form1 form;
        private int unitCount;
        private int readLength = 0;
        private int timeOutCount = 0;
        public TcpClient myTcpClient;
        public NetworkStream myNetworkStream;
        private Dictionary<int, UnitData> unitDataMap = new Dictionary<int, UnitData>();
        private Thread thread;
        private int tcpCommunicationTime = 0;

        public GatewayConnection(String gateway, Form1 form1 ,int count, int Length)
        {
            ConnectState = false;
            gatewayStr = gateway;
            form = form1;
            unitCount = count;
            readLength = Length;
        }

        public void connect()
        {
            Ip = ((TextBox)form.Controls.Find(gatewayStr + "_IP_TextBox", true)[0]).Text;

            for (int i = 0; i < unitCount; i++)   
            {
                UnitData unitData = new UnitData();
                int ffuCnt = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_FFU_Quanity_TextBox", true)[0].Text);
                int dpsCnt = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_DPS_Quanity_TextBox", true)[0].Text);
                int dpsStartID = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_DPS_ID_TextBox", true)[0].Text);
                int particleCnt = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_Particle_Quanity_TextBox", true)[0].Text);
                int particleStartID = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_Particle_ID_TextBox", true)[0].Text);
                for (int j = 0; j < ffuCnt; j++)
                {
                    if (!unitData.ContainsKey(j + 1))
                    {
                        unitData.Add(j + 1, new int[10]);
                    }
                }

                for (int j = 0; j < dpsCnt; j++)
                {
                    if (!unitData.ContainsKey(j + dpsStartID))
                    {
                        unitData.Add(j + dpsStartID, new int[10]);
                    }
                }

                for (int j = 0; j < particleCnt; j++)
                {
                    if (!unitData.ContainsKey(j + particleStartID))
                    {
                        unitData.Add(j + particleStartID, new int[10]);
                    }
                }

                if (!unitDataMap.ContainsKey(i + 1))
                {
                    unitDataMap.Add(i + 1, unitData);
                }
                else
                {
                    unitDataMap[i + 1] = unitData;
                }
            }  //塞陣列
            Ping ping = new Ping();
            PingReply reply = null;
            try
            {
                reply = ping.Send(Ip);
            }
            catch (Exception e)
            {
                ConnectState = false;
                closeThread();
                return;
            }

            if (reply.Status == IPStatus.Success)
            {
            }
            else
            {
                ConnectState = false;
                closeThread();
                return;
            }

            try
            {
                if (myTcpClient != null)
                    myTcpClient.Close();
                if (myNetworkStream != null)
                    myNetworkStream.Close();

                myTcpClient = new TcpClient(Ip, 502);

                if (myTcpClient.Connected)
                {
                    myNetworkStream = myTcpClient.GetStream();
                    myNetworkStream.WriteTimeout = 5000;
                    myNetworkStream.ReadTimeout = 5000;  //設定Timeout 有可能讀不到 程式會卡住
                    ConnectState = true;
                }
                else
                {
                    ConnectState = false;
                }
            }
            catch (Exception ex)
            {
                ConnectState = false;
                closeThread();
                return;
            }
            closeThread();
            return;
        }

        public void closeThread()
        {
            if (thread != null)
            {
                thread.Abort();
                thread = null;
            }
        }

        public int[] getDeviceDataAry(int register,int id)
        {
            return unitDataMap[register][id];
        }

        public void doAction()
        { 
            if (ConnectState == false)
            {
                if (thread == null ||
                    thread.ThreadState == ThreadState.Stopped  ||
                    thread.ThreadState == ThreadState.Unstarted ) 
                {
                    thread = new Thread(connect);
                    thread.IsBackground = true;
                    thread.Start();
                }
            }
            else
            {
                if (++tcpCommunicationTime > 4) //每 x 次重新連線一次
                {
                    tcpCommunicationTime = 0;

                    try
                    {
                        if (myTcpClient != null)
                            myTcpClient.Close();
                        if (myNetworkStream != null)
                            myNetworkStream.Close();

                        myTcpClient = new TcpClient(Ip, 502);

                        if (myTcpClient.Connected)
                        {
                            myNetworkStream = myTcpClient.GetStream();
                            myNetworkStream.WriteTimeout = 5000;
                            myNetworkStream.ReadTimeout = 5000;  //設定Timeout 有可能讀不到 程式會卡住
                            ConnectState = true;
                        }
                        else
                        {
                            ConnectState = false;
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        ConnectState = false;
                        return;
                    }
                }
                for (int i = 0; i < unitCount; i++)
                {
                    int ffuCnt = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_FFU_Quanity_TextBox", true)[0].Text);
                    int dpsCnt = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_DPS_Quanity_TextBox", true)[0].Text);
                    int particleCnt = int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_Particle_Quanity_TextBox", true)[0].Text);

                    int idCnt = 0;

                    while (idCnt < ffuCnt && ConnectState)
                    {
                        requestaDeviceData(i + 1, idCnt + 1);
                        idCnt += READ_GATEWAY_DEVICE_COUNT;
                        Thread.Sleep(50);
                    }

                    idCnt = 0;

                    while (idCnt < dpsCnt && ConnectState)
                    {
                        requestaDeviceData(i + 1, idCnt + int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_DPS_ID_TextBox", true)[0].Text));
                        idCnt += READ_GATEWAY_DEVICE_COUNT;
                        Thread.Sleep(50);
                    }

                    idCnt = 0;

                    while (idCnt < particleCnt && ConnectState)
                    {
                        requestaDeviceData(i + 1, idCnt + int.Parse(form.Controls.Find(gatewayStr + "_0" + (i + 1) + "_Particle_ID_TextBox", true)[0].Text));
                        idCnt += READ_GATEWAY_DEVICE_COUNT;
                        Thread.Sleep(50);
                    }
                }
            }
        }

        private bool requestaDeviceData(int register, int startId)
        {
            byte[] inputByte = new byte[12];
            byte[] outputByte = new byte[9 + (READ_GATEWAY_DEVICE_COUNT * readLength * 2)];
            int startAddress = startId  * readLength;

            inputByte[0] = 0;    //Tcp Header
            inputByte[1] = 1;    //Tcp Header
            inputByte[2] = 0;    //Tcp Header
            inputByte[3] = 0;    //Tcp Header
            inputByte[4] = 0;    //Tcp Header
            inputByte[5] = 6;    //Tcp Header

            inputByte[6] = Convert.ToByte(register);    //Tcp Header   

            inputByte[7] = 3;    //Function Code

            byte[] _adr = BitConverter.GetBytes(startAddress);
            inputByte[8] = _adr[1];   //startAddress
            inputByte[9] = _adr[0];   //startAddress 

            byte[] _length = BitConverter.GetBytes(READ_GATEWAY_DEVICE_COUNT * readLength);
            inputByte[10] = _length[1];   //Data Length
            inputByte[11] = _length[0];   //Data Length

            try {
                myNetworkStream.Write(inputByte, 0, inputByte.Length);
                myNetworkStream.Read(outputByte, 0, outputByte.Length);
            }
            catch (TimeoutException e)
            {
                ++timeOutCount;
                MessageBox.Show("ip:"+Ip +"timeout 第"+ timeOutCount.ToString() + "次   " + e.ToString());
                ConnectState = false;                
                return false;
            }
            catch (Exception ex){
                //MessageBox.Show(ex.ToString());
                ConnectState = false;
                return false;
            }

            if (outputByte[0] != 0 || outputByte[1] != 1) {
                return false;
            }
            else {
                UnitData unitData = unitDataMap[register];
                for (int i = 0; i < READ_GATEWAY_DEVICE_COUNT; i++) {
                    if (unitData.ContainsKey(startId + i)) {
                        for (int j = 0; j < readLength; j++) {
                            int data = outputByte[9 + (i * 2 * readLength) + (2 * j)];
                            data = (data << 8) + outputByte[10 + (i * 2 * readLength) + (2 * j)];
                            unitData[startId + i][j] = data;
                        }
                    }
                }
            }
            return true;
        }
    }
}
