﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TOPWELL_INNOLUX_EDC1
{
    public partial class Form1 : Form
    {
        private IniHandler iniHandler = new IniHandler(Application.StartupPath + "/settings.ini");

        private Dictionary<string, GatewayConnection> gatewayMap = new Dictionary<string, GatewayConnection>();

        private Dictionary<string, System.Timers.Timer> timerMap = new Dictionary<string, System.Timers.Timer>();

        private System.Timers.Timer g01_GatewayUpdateTimer;

        private int[] unitCount = {1};     //依序為每個gateway的Unit個數 (增加畫面需變更)

        public Form1()
        {
            InitializeComponent();

            //gateway   參數: 1.畫面gateway控制項名稱 2.主畫面 3.unit數量 4.單台讀取資料長度(幾個word) (增加畫面需變更)
            gatewayMap.Add("g01", new GatewayConnection("g01", this, unitCount[0], 10));
            //Timer  (增加畫面需變更)
            g01_GatewayUpdateTimer = new System.Timers.Timer();
            g01_GatewayUpdateTimer.Interval = 5000;
            g01_GatewayUpdateTimer.Elapsed += new System.Timers.ElapsedEventHandler(G01_GenerateXML);
            timerMap.Add("g01", g01_GatewayUpdateTimer);
            initial_Time_ComboBox("g01");
        }

        private void Form1_Load(object sender, EventArgs e)  //(增加畫面需變更)
        {
            //把ini檔資料讀出畫面
            for (int i = 1; i <= unitCount.Length; i++)
            {
                for (int j = 1; j <= unitCount[i - 1]; j++)
                {
                    LoadDataFromIni("g0" + i + "_0" + j);
                }
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) //(增加畫面需變更)
        {
            //把畫面資料寫入ini檔
            for (int i = 1; i <= unitCount.Length; i++)
            {
                for (int j = 1; j <= unitCount[i-1]; j++)
                {
                    SaveDataToIni("g0" + i + "_0" + j);
                }
            }
        }

        private void initial_Time_ComboBox(String gateway) 
        {
            ComboBox ComboBox = ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]);
            ArrayList data = new ArrayList();
            data.Add(new DictionaryEntry("一分鐘", 60000));
            data.Add(new DictionaryEntry("五分鐘", 300000));
            data.Add(new DictionaryEntry("十分鐘", 600000));
            data.Add(new DictionaryEntry("十五分鐘", 900000));
            data.Add(new DictionaryEntry("三十分鐘", 1800000));
            data.Add(new DictionaryEntry("一小時", 3600000));
            ComboBox.DisplayMember = "Key";
            ComboBox.ValueMember = "Value";
            ComboBox.DataSource = data;
        }

        private void DPS_ID_TextBox_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;

            if (int.Parse(textbox.Text) < 64 && int.Parse(textbox.Text) != 0)
            {
                MessageBox.Show("DPS的ID必須大於63!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textbox.Focus();
                return;
            }
        }

        private void Particle_ID_TextBox_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;

            if (int.Parse(textbox.Text) < 64 && int.Parse(textbox.Text) != 0)
            {
                MessageBox.Show("Particle的ID必須大於63!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textbox.Focus();
                return;
            }
        }

        private void SaveDataToIni(String unit)
        {
            String gateway = unit.Substring(0, 3);
            iniHandler.WriteIniFile(unit, gateway + "_IP_TextBox", ((TextBox)this.Controls.Find(gateway + "_IP_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, gateway + "_Folder_Label", ((Label)this.Controls.Find(gateway + "_Folder_Label", true)[0]).Text);
            iniHandler.WriteIniFile(unit, gateway + "_Time_comboBox", ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]).SelectedValue);
            for (int i = 1; i < 10; i++)
            {
                iniHandler.WriteIniFile(unit, unit + "_TextBox" + i, ((TextBox)this.Controls.Find(unit + "_TextBox" + i, true)[0]).Text);
            }
            iniHandler.WriteIniFile(unit, unit + "_DPS_ID_TextBox", ((TextBox)this.Controls.Find(unit + "_DPS_ID_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_Particle_ID_TextBox", ((TextBox)this.Controls.Find(unit + "_Particle_ID_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_FFU_Quanity_TextBox", ((TextBox)this.Controls.Find(unit + "_FFU_Quanity_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_DPS_Quanity_TextBox", ((TextBox)this.Controls.Find(unit + "_DPS_Quanity_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_Particle_Quanity_TextBox", ((TextBox)this.Controls.Find(unit + "_Particle_Quanity_TextBox", true)[0]).Text);
        }

        private void LoadDataFromIni(String unit)
        {
            if (iniHandler.isExistIni() == true)
            {
                String gateway = unit.Substring(0, 3);
                ((TextBox)this.Controls.Find(gateway + "_IP_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, gateway + "_IP_TextBox", "");
                ((Label)this.Controls.Find(gateway + "_Folder_Label", true)[0]).Text = iniHandler.ReadIniFile(unit, gateway + "_Folder_Label", "");
                String time_ComboBox_Value = iniHandler.ReadIniFile(unit, gateway + "_Time_comboBox", "");
                if (!string.IsNullOrWhiteSpace(time_ComboBox_Value))
                {
                    ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]).SelectedValue = int.Parse(time_ComboBox_Value);
                }
                for (int i = 1; i < 10; i++)
                {
                    ((TextBox)this.Controls.Find(unit + "_TextBox" + i, true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_TextBox" + i, "");
                }
                ((TextBox)this.Controls.Find(unit + "_DPS_ID_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_DPS_ID_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_Particle_ID_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_Particle_ID_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_FFU_Quanity_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_FFU_Quanity_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_DPS_Quanity_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_DPS_Quanity_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_Particle_Quanity_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_Particle_Quanity_TextBox", "");
            }
        }

        private void Connect_Button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            String gateway = button.Name.Substring(0, 3);
            Label connectLabel = (Label)this.Controls.Find(gateway + "_connect_Label", true)[0];
            Label folderLabel = (Label)this.Controls.Find(gateway + "_Folder_Label", true)[0];
            if ("請選擇匯出路徑" == folderLabel.Text)
            {
                MessageBox.Show("請先選擇匯出路徑!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                this.Controls.Find(gateway + "_Folder_Label", true)[0].Text = folderBrowserDialog.SelectedPath + "\\<eqp_id>\\<sub_eqp_id>\\";
            }
            if (!gatewayMap[gateway].ConnectState)          //未連線時
            {
                TextBox ipTextBox = (TextBox)this.Controls.Find(gateway + "_IP_TextBox", true)[0];

                if (string.IsNullOrWhiteSpace(ipTextBox.Text))
                {
                    MessageBox.Show("IP不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ipTextBox.Focus();
                    return;
                }
                
                gatewayMap[gateway].connect();

                if (gatewayMap[gateway].ConnectState)
                {
                    //依據匯出頻率更改timer
                    ComboBox time_ComboBox = (ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0];
                    timerMap[gateway].Interval = Convert.ToInt32(time_ComboBox.SelectedValue);

                    timerMap[gateway].Start();
                    button.Text = "中斷連線";
                    connectLabel.Text = "已連線";

                    //連線中控制項不給更改
                    foreach (Control control in ((Panel)this.Controls.Find(gateway + "_Panel", true)[0]).Controls)
                    {
                        if (control is TextBox)
                        {
                            ((TextBox)control).Enabled = false;
                        }
                        else if (control is TabControl)
                        {
                            String unit = ((TabControl)control).Name.Substring(0, 6);
                            foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_EDC_GroupBox", true)[0]).Controls)
                            {
                                if (subControl is TextBox)
                                {
                                    ((TextBox)subControl).Enabled = false;
                                }
                            }
                            foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_FFU_GroupBox", true)[0]).Controls)
                            {
                                if (subControl is TextBox)
                                {
                                    ((TextBox)subControl).Enabled = false;
                                }
                            }
                        }
                    }
                    ((Button)this.Controls.Find(gateway + "_selectFolder_Button", true)[0]).Enabled = false;
                    time_ComboBox.Enabled = false;
                }
                else
                {
                    MessageBox.Show("連線異常,請確認IP是否正確!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else       //連線時
            {

                if (gatewayMap[gateway].myTcpClient != null)
                    gatewayMap[gateway].myTcpClient.Close();
                if (gatewayMap[gateway].myNetworkStream != null)
                    gatewayMap[gateway].myNetworkStream.Close();
                gatewayMap[gateway].closeThread();

                gatewayMap[gateway].ConnectState = false;
                timerMap[gateway].Stop();
                button.Text = "連線";
                connectLabel.Text = "未連線";

                //離線時開放控制項更改
                foreach (Control control in ((Panel)this.Controls.Find(gateway + "_Panel", true)[0]).Controls)
                {
                    if (control is TextBox)
                    {
                        ((TextBox)control).Enabled = true;
                    }
                    else if (control is TabControl)
                    {
                        String unit = ((TabControl)control).Name.Substring(0, 6);
                        foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_EDC_GroupBox", true)[0]).Controls)
                        {
                            if (subControl is TextBox)
                            {
                                ((TextBox)subControl).Enabled = true;
                            }
                        }
                        foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_FFU_GroupBox", true)[0]).Controls)
                        {
                            if (subControl is TextBox)
                            {
                                ((TextBox)subControl).Enabled = true;
                            }
                        }
                    }
                }
                ((Button)this.Controls.Find(gateway + "_selectFolder_Button", true)[0]).Enabled = true;
                ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]).Enabled = true;
            }
        }

        private void SelectFolder_Button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            String gateway = button.Name.Substring(0, 3);
            folderBrowserDialog.SelectedPath = ((Label)this.Controls.Find(gateway + "_Folder_Label", true)[0]).Text.Replace("<eqp_id>\\<sub_eqp_id>\\", "");
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }
            this.Controls.Find(gateway + "_Folder_Label", true)[0].Text = folderBrowserDialog.SelectedPath + "\\<eqp_id>\\<sub_eqp_id>\\";
        }

        private void G01_GenerateXML(object sender, EventArgs e)  //(增加畫面需變更)
        {
            //造成執行時間一直往後推延,先不使用
            //timerMap["g01"].Stop();
            gatewayMap["g01"].doAction();
            bool exportXMLCheck = false;
            for (int i = 1; i <= unitCount[0]; i++)      //依據目前有開幾個unit
            {

                exportXMLCheck = XmlHandler.exportXml("g01_0" + i, this, gatewayMap["g01"], g01_Folder_Label.Text);
                if (!exportXMLCheck)
                {
                    return;
                }
            }
            //timerMap["g01"].Start();
        }
    }
}
