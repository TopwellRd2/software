﻿
namespace TOPWELL_INNOLUX_EDC1
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.g01_TabPage = new System.Windows.Forms.TabPage();
            this.g01_Panel = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.g01_Time_comboBox = new System.Windows.Forms.ComboBox();
            this.g01_Folder_Label = new System.Windows.Forms.Label();
            this.g01_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_connect_Button = new System.Windows.Forms.Button();
            this.g01_connect_Label = new System.Windows.Forms.Label();
            this.g01_IP_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.g01_01_Tab = new System.Windows.Forms.TabControl();
            this.g01_01_TabPage = new System.Windows.Forms.TabPage();
            this.g01_01_Panel = new System.Windows.Forms.Panel();
            this.g01_01_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.g01_01_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_01_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.g01_01_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_01_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.g01_01_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.g01_01_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.g01_01_TextBox9 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.g01_01_TextBox8 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.g01_01_TextBox7 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.g01_01_TextBox6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.g01_01_TextBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.g01_01_TextBox4 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.g01_01_TextBox3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.g01_01_TextBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.g01_01_TextBox1 = new System.Windows.Forms.TextBox();
            this.g01_Tab = new System.Windows.Forms.TabControl();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.g01_TabPage.SuspendLayout();
            this.g01_Panel.SuspendLayout();
            this.g01_01_Tab.SuspendLayout();
            this.g01_01_TabPage.SuspendLayout();
            this.g01_01_Panel.SuspendLayout();
            this.g01_01_FFU_GroupBox.SuspendLayout();
            this.panel3.SuspendLayout();
            this.g01_01_EDC_GroupBox.SuspendLayout();
            this.g01_Tab.SuspendLayout();
            this.SuspendLayout();
            // 
            // g01_TabPage
            // 
            this.g01_TabPage.Controls.Add(this.g01_Panel);
            this.g01_TabPage.Location = new System.Drawing.Point(4, 37);
            this.g01_TabPage.Name = "g01_TabPage";
            this.g01_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_TabPage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.g01_TabPage.Size = new System.Drawing.Size(1350, 719);
            this.g01_TabPage.TabIndex = 0;
            this.g01_TabPage.Text = "G01";
            this.g01_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_Panel
            // 
            this.g01_Panel.Controls.Add(this.label18);
            this.g01_Panel.Controls.Add(this.g01_Time_comboBox);
            this.g01_Panel.Controls.Add(this.g01_Folder_Label);
            this.g01_Panel.Controls.Add(this.g01_selectFolder_Button);
            this.g01_Panel.Controls.Add(this.g01_connect_Button);
            this.g01_Panel.Controls.Add(this.g01_connect_Label);
            this.g01_Panel.Controls.Add(this.g01_IP_TextBox);
            this.g01_Panel.Controls.Add(this.label1);
            this.g01_Panel.Controls.Add(this.g01_01_Tab);
            this.g01_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_Panel.Name = "g01_Panel";
            this.g01_Panel.Size = new System.Drawing.Size(1338, 707);
            this.g01_Panel.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(27, 75);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(116, 31);
            this.label18.TabIndex = 8;
            this.label18.Text = "匯出頻率:";
            // 
            // g01_Time_comboBox
            // 
            this.g01_Time_comboBox.FormattingEnabled = true;
            this.g01_Time_comboBox.Location = new System.Drawing.Point(149, 75);
            this.g01_Time_comboBox.Name = "g01_Time_comboBox";
            this.g01_Time_comboBox.Size = new System.Drawing.Size(171, 33);
            this.g01_Time_comboBox.TabIndex = 7;
            // 
            // g01_Folder_Label
            // 
            this.g01_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_Folder_Label.Location = new System.Drawing.Point(587, 7);
            this.g01_Folder_Label.Name = "g01_Folder_Label";
            this.g01_Folder_Label.Size = new System.Drawing.Size(529, 73);
            this.g01_Folder_Label.TabIndex = 6;
            this.g01_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_selectFolder_Button
            // 
            this.g01_selectFolder_Button.Location = new System.Drawing.Point(423, 16);
            this.g01_selectFolder_Button.Name = "g01_selectFolder_Button";
            this.g01_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_selectFolder_Button.TabIndex = 5;
            this.g01_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_connect_Button
            // 
            this.g01_connect_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.g01_connect_Button.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_connect_Button.Location = new System.Drawing.Point(1134, 29);
            this.g01_connect_Button.Name = "g01_connect_Button";
            this.g01_connect_Button.Size = new System.Drawing.Size(188, 66);
            this.g01_connect_Button.TabIndex = 4;
            this.g01_connect_Button.Text = "連線";
            this.g01_connect_Button.UseVisualStyleBackColor = true;
            this.g01_connect_Button.Click += new System.EventHandler(this.Connect_Button_Click);
            // 
            // g01_connect_Label
            // 
            this.g01_connect_Label.AutoSize = true;
            this.g01_connect_Label.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_connect_Label.ForeColor = System.Drawing.Color.Red;
            this.g01_connect_Label.Location = new System.Drawing.Point(326, 26);
            this.g01_connect_Label.Name = "g01_connect_Label";
            this.g01_connect_Label.Size = new System.Drawing.Size(86, 31);
            this.g01_connect_Label.TabIndex = 3;
            this.g01_connect_Label.Text = "未連線";
            this.g01_connect_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_IP_TextBox
            // 
            this.g01_IP_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_IP_TextBox.Location = new System.Drawing.Point(82, 23);
            this.g01_IP_TextBox.Name = "g01_IP_TextBox";
            this.g01_IP_TextBox.Size = new System.Drawing.Size(238, 34);
            this.g01_IP_TextBox.TabIndex = 2;
            this.g01_IP_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(27, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP:";
            // 
            // g01_01_Tab
            // 
            this.g01_01_Tab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.g01_01_Tab.Controls.Add(this.g01_01_TabPage);
            this.g01_01_Tab.Location = new System.Drawing.Point(3, 118);
            this.g01_01_Tab.Name = "g01_01_Tab";
            this.g01_01_Tab.SelectedIndex = 0;
            this.g01_01_Tab.Size = new System.Drawing.Size(1332, 586);
            this.g01_01_Tab.TabIndex = 0;
            // 
            // g01_01_TabPage
            // 
            this.g01_01_TabPage.Controls.Add(this.g01_01_Panel);
            this.g01_01_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_01_TabPage.Name = "g01_01_TabPage";
            this.g01_01_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_01_TabPage.Size = new System.Drawing.Size(1324, 548);
            this.g01_01_TabPage.TabIndex = 0;
            this.g01_01_TabPage.Text = "Unit01";
            this.g01_01_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_01_Panel
            // 
            this.g01_01_Panel.Controls.Add(this.g01_01_FFU_GroupBox);
            this.g01_01_Panel.Controls.Add(this.g01_01_EDC_GroupBox);
            this.g01_01_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_01_Panel.Name = "g01_01_Panel";
            this.g01_01_Panel.Size = new System.Drawing.Size(1309, 529);
            this.g01_01_Panel.TabIndex = 0;
            // 
            // g01_01_FFU_GroupBox
            // 
            this.g01_01_FFU_GroupBox.Controls.Add(this.label17);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_Particle_ID_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_Particle_Quanity_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label16);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_DPS_ID_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_DPS_Quanity_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label15);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_FFU_Quanity_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.panel3);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label14);
            this.g01_01_FFU_GroupBox.Location = new System.Drawing.Point(808, 19);
            this.g01_01_FFU_GroupBox.Name = "g01_01_FFU_GroupBox";
            this.g01_01_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_01_FFU_GroupBox.TabIndex = 21;
            this.g01_01_FFU_GroupBox.TabStop = false;
            this.g01_01_FFU_GroupBox.Text = "FFU";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(212, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 31);
            this.label17.TabIndex = 25;
            this.label17.Text = "1";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_Particle_ID_TextBox
            // 
            this.g01_01_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_01_Particle_ID_TextBox.Name = "g01_01_Particle_ID_TextBox";
            this.g01_01_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_Particle_ID_TextBox.TabIndex = 24;
            this.g01_01_Particle_ID_TextBox.Text = "65";
            this.g01_01_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_01_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_01_Particle_Quanity_TextBox
            // 
            this.g01_01_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_01_Particle_Quanity_TextBox.Name = "g01_01_Particle_Quanity_TextBox";
            this.g01_01_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_01_Particle_Quanity_TextBox.Text = "1";
            this.g01_01_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(28, 273);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 31);
            this.label16.TabIndex = 22;
            this.label16.Text = "Particle";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_DPS_ID_TextBox
            // 
            this.g01_01_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_01_DPS_ID_TextBox.Name = "g01_01_DPS_ID_TextBox";
            this.g01_01_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_DPS_ID_TextBox.TabIndex = 21;
            this.g01_01_DPS_ID_TextBox.Text = "64";
            this.g01_01_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_01_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_01_DPS_Quanity_TextBox
            // 
            this.g01_01_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_01_DPS_Quanity_TextBox.Name = "g01_01_DPS_Quanity_TextBox";
            this.g01_01_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_01_DPS_Quanity_TextBox.Text = "1";
            this.g01_01_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(47, 202);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 31);
            this.label15.TabIndex = 19;
            this.label15.Text = "DPS";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_FFU_Quanity_TextBox
            // 
            this.g01_01_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_01_FFU_Quanity_TextBox.Name = "g01_01_FFU_Quanity_TextBox";
            this.g01_01_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_01_FFU_Quanity_TextBox.Text = "1";
            this.g01_01_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Location = new System.Drawing.Point(25, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(445, 69);
            this.panel3.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(21, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 31);
            this.label7.TabIndex = 0;
            this.label7.Text = "格式";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(148, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 31);
            this.label12.TabIndex = 1;
            this.label12.Text = "起始站號";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(316, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 31);
            this.label13.TabIndex = 2;
            this.label13.Text = "輸出數量";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(40, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 31);
            this.label14.TabIndex = 3;
            this.label14.Text = "SPEED";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_EDC_GroupBox
            // 
            this.g01_01_EDC_GroupBox.Controls.Add(this.label4);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox9);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label2);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox8);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label3);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox7);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label5);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox6);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label6);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox5);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label11);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox4);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label10);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox3);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label9);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox2);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label8);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox1);
            this.g01_01_EDC_GroupBox.Location = new System.Drawing.Point(16, 19);
            this.g01_01_EDC_GroupBox.Name = "g01_01_EDC_GroupBox";
            this.g01_01_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_01_EDC_GroupBox.TabIndex = 20;
            this.g01_01_EDC_GroupBox.TabStop = false;
            this.g01_01_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(51, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 31);
            this.label4.TabIndex = 2;
            this.label4.Text = "glass_id :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox9
            // 
            this.g01_01_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_01_TextBox9.Name = "g01_01_TextBox9";
            this.g01_01_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox9.TabIndex = 19;
            this.g01_01_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(17, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = "product_id :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox8
            // 
            this.g01_01_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_01_TextBox8.Name = "g01_01_TextBox8";
            this.g01_01_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox8.TabIndex = 18;
            this.g01_01_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(63, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "eqp_id :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox7
            // 
            this.g01_01_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_01_TextBox7.Name = "g01_01_TextBox7";
            this.g01_01_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox7.TabIndex = 17;
            this.g01_01_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(10, 341);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 31);
            this.label5.TabIndex = 5;
            this.label5.Text = "sub_eqp_id :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox6
            // 
            this.g01_01_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_01_TextBox6.Name = "g01_01_TextBox6";
            this.g01_01_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox6.TabIndex = 16;
            this.g01_01_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(71, 431);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 31);
            this.label6.TabIndex = 6;
            this.label6.Text = "owner :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox5
            // 
            this.g01_01_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_01_TextBox5.Name = "g01_01_TextBox5";
            this.g01_01_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox5.TabIndex = 15;
            this.g01_01_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(409, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 31);
            this.label11.TabIndex = 7;
            this.label11.Text = "recipe_id :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox4
            // 
            this.g01_01_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_01_TextBox4.Name = "g01_01_TextBox4";
            this.g01_01_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox4.TabIndex = 14;
            this.g01_01_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(402, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 31);
            this.label10.TabIndex = 8;
            this.label10.Text = "operation :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox3
            // 
            this.g01_01_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_01_TextBox3.Name = "g01_01_TextBox3";
            this.g01_01_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox3.TabIndex = 13;
            this.g01_01_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(413, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 31);
            this.label9.TabIndex = 9;
            this.label9.Text = "chamber :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox2
            // 
            this.g01_01_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_01_TextBox2.Name = "g01_01_TextBox2";
            this.g01_01_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox2.TabIndex = 12;
            this.g01_01_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(415, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 31);
            this.label8.TabIndex = 10;
            this.label8.Text = "operator :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox1
            // 
            this.g01_01_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_01_TextBox1.Name = "g01_01_TextBox1";
            this.g01_01_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox1.TabIndex = 11;
            this.g01_01_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g01_Tab
            // 
            this.g01_Tab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.g01_Tab.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.g01_Tab.Controls.Add(this.g01_TabPage);
            this.g01_Tab.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_Tab.Location = new System.Drawing.Point(12, 12);
            this.g01_Tab.Name = "g01_Tab";
            this.g01_Tab.SelectedIndex = 0;
            this.g01_Tab.Size = new System.Drawing.Size(1358, 760);
            this.g01_Tab.TabIndex = 0;
            this.g01_Tab.Tag = "";
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.SelectedPath = "C:\\Users\\TOPWELL\\Desktop";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1382, 784);
            this.Controls.Add(this.g01_Tab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "INNOLUX_EDC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.g01_TabPage.ResumeLayout(false);
            this.g01_Panel.ResumeLayout(false);
            this.g01_Panel.PerformLayout();
            this.g01_01_Tab.ResumeLayout(false);
            this.g01_01_TabPage.ResumeLayout(false);
            this.g01_01_Panel.ResumeLayout(false);
            this.g01_01_FFU_GroupBox.ResumeLayout(false);
            this.g01_01_FFU_GroupBox.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.g01_01_EDC_GroupBox.ResumeLayout(false);
            this.g01_01_EDC_GroupBox.PerformLayout();
            this.g01_Tab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage g01_TabPage;
        private System.Windows.Forms.TabControl g01_01_Tab;
        private System.Windows.Forms.TabPage g01_01_TabPage;
        private System.Windows.Forms.TabControl g01_Tab;
        private System.Windows.Forms.Panel g01_Panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label g01_connect_Label;
        private System.Windows.Forms.Button g01_connect_Button;
        private System.Windows.Forms.Panel g01_01_Panel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox g01_01_TextBox1;
        private System.Windows.Forms.TextBox g01_01_TextBox9;
        private System.Windows.Forms.TextBox g01_01_TextBox8;
        private System.Windows.Forms.TextBox g01_01_TextBox7;
        private System.Windows.Forms.TextBox g01_01_TextBox6;
        private System.Windows.Forms.TextBox g01_01_TextBox5;
        private System.Windows.Forms.TextBox g01_01_TextBox4;
        private System.Windows.Forms.TextBox g01_01_TextBox3;
        private System.Windows.Forms.TextBox g01_01_TextBox2;
        private System.Windows.Forms.GroupBox g01_01_EDC_GroupBox;
        private System.Windows.Forms.GroupBox g01_01_FFU_GroupBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox g01_01_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_01_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox g01_01_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_01_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox g01_01_FFU_Quanity_TextBox;
        private System.Windows.Forms.TextBox g01_IP_TextBox;
        private System.Windows.Forms.Label g01_Folder_Label;
        private System.Windows.Forms.Button g01_selectFolder_Button;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox g01_Time_comboBox;
    }
}

