﻿
namespace TOPWELL_INNOLUX_EDC2
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.g01_TabPage = new System.Windows.Forms.TabPage();
            this.g01_Panel = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.g01_Time_comboBox = new System.Windows.Forms.ComboBox();
            this.g01_connect_Button = new System.Windows.Forms.Button();
            this.g01_connect_Label = new System.Windows.Forms.Label();
            this.g01_IP_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.unit_Tab = new System.Windows.Forms.TabControl();
            this.g01_01_TabPage = new System.Windows.Forms.TabPage();
            this.g01_01_Panel = new System.Windows.Forms.Panel();
            this.g01_01_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_01_Folder_Label = new System.Windows.Forms.Label();
            this.g01_01_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_01_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_01_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.g01_01_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_01_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.g01_01_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_01_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.g01_01_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.g01_01_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.g01_01_TextBox9 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.g01_01_TextBox8 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.g01_01_TextBox7 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.g01_01_TextBox6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.g01_01_TextBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.g01_01_TextBox4 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.g01_01_TextBox3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.g01_01_TextBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.g01_01_TextBox1 = new System.Windows.Forms.TextBox();
            this.g01_02_TabPage = new System.Windows.Forms.TabPage();
            this.g01_02_Panel = new System.Windows.Forms.Panel();
            this.g01_02_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_02_Folder_Label = new System.Windows.Forms.Label();
            this.g01_02_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_02_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_02_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.g01_02_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_02_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.g01_02_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_02_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.g01_02_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.g01_02_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.g01_02_TextBox9 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.g01_02_TextBox8 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.g01_02_TextBox7 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.g01_02_TextBox6 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.g01_02_TextBox5 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.g01_02_TextBox4 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.g01_02_TextBox3 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.g01_02_TextBox2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.g01_02_TextBox1 = new System.Windows.Forms.TextBox();
            this.g01_03_TabPage = new System.Windows.Forms.TabPage();
            this.g01_03_Panel = new System.Windows.Forms.Panel();
            this.g01_03_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_03_Folder_Label = new System.Windows.Forms.Label();
            this.g01_03_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_03_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_03_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.g01_03_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_03_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.g01_03_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_03_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.g01_03_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.g01_03_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.g01_03_TextBox9 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.g01_03_TextBox8 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.g01_03_TextBox7 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.g01_03_TextBox6 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.g01_03_TextBox5 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.g01_03_TextBox4 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.g01_03_TextBox3 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.g01_03_TextBox2 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.g01_03_TextBox1 = new System.Windows.Forms.TextBox();
            this.g01_04_TabPage = new System.Windows.Forms.TabPage();
            this.g01_04_Panel = new System.Windows.Forms.Panel();
            this.g01_04_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_04_Folder_Label = new System.Windows.Forms.Label();
            this.g01_04_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_04_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_04_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.g01_04_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_04_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.g01_04_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_04_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.g01_04_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.g01_04_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.g01_04_TextBox9 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.g01_04_TextBox8 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.g01_04_TextBox7 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.g01_04_TextBox6 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.g01_04_TextBox5 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.g01_04_TextBox4 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.g01_04_TextBox3 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.g01_04_TextBox2 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.g01_04_TextBox1 = new System.Windows.Forms.TextBox();
            this.g01_05_TabPage = new System.Windows.Forms.TabPage();
            this.g01_05_Panel = new System.Windows.Forms.Panel();
            this.g01_05_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_05_Folder_Label = new System.Windows.Forms.Label();
            this.g01_05_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_05_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_05_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label67 = new System.Windows.Forms.Label();
            this.g01_05_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_05_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.g01_05_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_05_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.g01_05_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.g01_05_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label74 = new System.Windows.Forms.Label();
            this.g01_05_TextBox9 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.g01_05_TextBox8 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.g01_05_TextBox7 = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.g01_05_TextBox6 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.g01_05_TextBox5 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.g01_05_TextBox4 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.g01_05_TextBox3 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.g01_05_TextBox2 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.g01_05_TextBox1 = new System.Windows.Forms.TextBox();
            this.g01_06_TabPage = new System.Windows.Forms.TabPage();
            this.g01_06_Panel = new System.Windows.Forms.Panel();
            this.g01_06_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_06_Folder_Label = new System.Windows.Forms.Label();
            this.g01_06_selectFolder_Button = new System.Windows.Forms.Button();
            this.g01_06_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g01_06_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label83 = new System.Windows.Forms.Label();
            this.g01_06_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_06_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.g01_06_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g01_06_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.g01_06_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.g01_06_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label90 = new System.Windows.Forms.Label();
            this.g01_06_TextBox9 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.g01_06_TextBox8 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.g01_06_TextBox7 = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.g01_06_TextBox6 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.g01_06_TextBox5 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.g01_06_TextBox4 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.g01_06_TextBox3 = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.g01_06_TextBox2 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.g01_06_TextBox1 = new System.Windows.Forms.TextBox();
            this.gateway_Tab = new System.Windows.Forms.TabControl();
            this.g02_TabPage = new System.Windows.Forms.TabPage();
            this.g02_Panel = new System.Windows.Forms.Panel();
            this.label99 = new System.Windows.Forms.Label();
            this.g02_Time_comboBox = new System.Windows.Forms.ComboBox();
            this.g02_connect_Button = new System.Windows.Forms.Button();
            this.g02_connect_Label = new System.Windows.Forms.Label();
            this.g02_IP_TextBox = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.unit_Tab2 = new System.Windows.Forms.TabControl();
            this.g02_01_TabPage = new System.Windows.Forms.TabPage();
            this.g02_01_Panel = new System.Windows.Forms.Panel();
            this.g02_01_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_01_Folder_Label = new System.Windows.Forms.Label();
            this.g02_01_selectFolder_Button = new System.Windows.Forms.Button();
            this.g02_01_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_01_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label103 = new System.Windows.Forms.Label();
            this.g02_01_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_01_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.g02_01_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_01_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.g02_01_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.g02_01_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label110 = new System.Windows.Forms.Label();
            this.g02_01_TextBox9 = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.g02_01_TextBox8 = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.g02_01_TextBox7 = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.g02_01_TextBox6 = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.g02_01_TextBox5 = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.g02_01_TextBox4 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.g02_01_TextBox3 = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.g02_01_TextBox2 = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.g02_01_TextBox1 = new System.Windows.Forms.TextBox();
            this.g02_02_TabPage = new System.Windows.Forms.TabPage();
            this.g02_02_Panel = new System.Windows.Forms.Panel();
            this.g02_02_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_02_Folder_Label = new System.Windows.Forms.Label();
            this.g02_02_selectFolder_Button = new System.Windows.Forms.Button();
            this.g02_02_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_02_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label119 = new System.Windows.Forms.Label();
            this.g02_02_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_02_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.g02_02_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_02_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.g02_02_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.g02_02_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label126 = new System.Windows.Forms.Label();
            this.g02_02_TextBox9 = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.g02_02_TextBox8 = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.g02_02_TextBox7 = new System.Windows.Forms.TextBox();
            this.label129 = new System.Windows.Forms.Label();
            this.g02_02_TextBox6 = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.g02_02_TextBox5 = new System.Windows.Forms.TextBox();
            this.label131 = new System.Windows.Forms.Label();
            this.g02_02_TextBox4 = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.g02_02_TextBox3 = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.g02_02_TextBox2 = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.g02_02_TextBox1 = new System.Windows.Forms.TextBox();
            this.g02_03_TabPage = new System.Windows.Forms.TabPage();
            this.g02_03_Panel = new System.Windows.Forms.Panel();
            this.g02_03_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_03_Folder_Label = new System.Windows.Forms.Label();
            this.g02_03_selectFolder_Button = new System.Windows.Forms.Button();
            this.g02_03_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_03_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label135 = new System.Windows.Forms.Label();
            this.g02_03_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_03_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.g02_03_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_03_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this.g02_03_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.g02_03_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label142 = new System.Windows.Forms.Label();
            this.g02_03_TextBox9 = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.g02_03_TextBox8 = new System.Windows.Forms.TextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.g02_03_TextBox7 = new System.Windows.Forms.TextBox();
            this.label145 = new System.Windows.Forms.Label();
            this.g02_03_TextBox6 = new System.Windows.Forms.TextBox();
            this.label146 = new System.Windows.Forms.Label();
            this.g02_03_TextBox5 = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.g02_03_TextBox4 = new System.Windows.Forms.TextBox();
            this.label148 = new System.Windows.Forms.Label();
            this.g02_03_TextBox3 = new System.Windows.Forms.TextBox();
            this.label149 = new System.Windows.Forms.Label();
            this.g02_03_TextBox2 = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.g02_03_TextBox1 = new System.Windows.Forms.TextBox();
            this.g02_04_TabPage = new System.Windows.Forms.TabPage();
            this.g02_04_Panel = new System.Windows.Forms.Panel();
            this.g02_04_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_04_Folder_Label = new System.Windows.Forms.Label();
            this.g02_04_selectFolder_Button = new System.Windows.Forms.Button();
            this.g02_04_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_04_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label151 = new System.Windows.Forms.Label();
            this.g02_04_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_04_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label152 = new System.Windows.Forms.Label();
            this.g02_04_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_04_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.g02_04_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.g02_04_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label158 = new System.Windows.Forms.Label();
            this.g02_04_TextBox9 = new System.Windows.Forms.TextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.g02_04_TextBox8 = new System.Windows.Forms.TextBox();
            this.label160 = new System.Windows.Forms.Label();
            this.g02_04_TextBox7 = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.g02_04_TextBox6 = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.g02_04_TextBox5 = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.g02_04_TextBox4 = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.g02_04_TextBox3 = new System.Windows.Forms.TextBox();
            this.label165 = new System.Windows.Forms.Label();
            this.g02_04_TextBox2 = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.g02_04_TextBox1 = new System.Windows.Forms.TextBox();
            this.g02_05_TabPage = new System.Windows.Forms.TabPage();
            this.g02_05_Panel = new System.Windows.Forms.Panel();
            this.g02_05_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_05_Folder_Label = new System.Windows.Forms.Label();
            this.g02_05_selectFolder_Button = new System.Windows.Forms.Button();
            this.g02_05_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_05_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label167 = new System.Windows.Forms.Label();
            this.g02_05_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_05_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.g02_05_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_05_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label169 = new System.Windows.Forms.Label();
            this.g02_05_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.g02_05_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label174 = new System.Windows.Forms.Label();
            this.g02_05_TextBox9 = new System.Windows.Forms.TextBox();
            this.label175 = new System.Windows.Forms.Label();
            this.g02_05_TextBox8 = new System.Windows.Forms.TextBox();
            this.label176 = new System.Windows.Forms.Label();
            this.g02_05_TextBox7 = new System.Windows.Forms.TextBox();
            this.label177 = new System.Windows.Forms.Label();
            this.g02_05_TextBox6 = new System.Windows.Forms.TextBox();
            this.label178 = new System.Windows.Forms.Label();
            this.g02_05_TextBox5 = new System.Windows.Forms.TextBox();
            this.label179 = new System.Windows.Forms.Label();
            this.g02_05_TextBox4 = new System.Windows.Forms.TextBox();
            this.label180 = new System.Windows.Forms.Label();
            this.g02_05_TextBox3 = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.g02_05_TextBox2 = new System.Windows.Forms.TextBox();
            this.label182 = new System.Windows.Forms.Label();
            this.g02_05_TextBox1 = new System.Windows.Forms.TextBox();
            this.g02_06_TabPage = new System.Windows.Forms.TabPage();
            this.g02_06_Panel = new System.Windows.Forms.Panel();
            this.g02_06_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_06_Folder_Label = new System.Windows.Forms.Label();
            this.g02_06_selectFolder_Button = new System.Windows.Forms.Button();
            this.g02_06_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g02_06_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label183 = new System.Windows.Forms.Label();
            this.g02_06_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_06_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label184 = new System.Windows.Forms.Label();
            this.g02_06_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g02_06_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.g02_06_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.g02_06_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label190 = new System.Windows.Forms.Label();
            this.g02_06_TextBox9 = new System.Windows.Forms.TextBox();
            this.label191 = new System.Windows.Forms.Label();
            this.g02_06_TextBox8 = new System.Windows.Forms.TextBox();
            this.label192 = new System.Windows.Forms.Label();
            this.g02_06_TextBox7 = new System.Windows.Forms.TextBox();
            this.label193 = new System.Windows.Forms.Label();
            this.g02_06_TextBox6 = new System.Windows.Forms.TextBox();
            this.label194 = new System.Windows.Forms.Label();
            this.g02_06_TextBox5 = new System.Windows.Forms.TextBox();
            this.label195 = new System.Windows.Forms.Label();
            this.g02_06_TextBox4 = new System.Windows.Forms.TextBox();
            this.label196 = new System.Windows.Forms.Label();
            this.g02_06_TextBox3 = new System.Windows.Forms.TextBox();
            this.label197 = new System.Windows.Forms.Label();
            this.g02_06_TextBox2 = new System.Windows.Forms.TextBox();
            this.label198 = new System.Windows.Forms.Label();
            this.g02_06_TextBox1 = new System.Windows.Forms.TextBox();
            this.g03_TabPage = new System.Windows.Forms.TabPage();
            this.g03_Panel = new System.Windows.Forms.Panel();
            this.label100 = new System.Windows.Forms.Label();
            this.g03_Time_comboBox = new System.Windows.Forms.ComboBox();
            this.g03_connect_Button = new System.Windows.Forms.Button();
            this.g03_connect_Label = new System.Windows.Forms.Label();
            this.g03_IP_TextBox = new System.Windows.Forms.TextBox();
            this.label199 = new System.Windows.Forms.Label();
            this.unit_Tab3 = new System.Windows.Forms.TabControl();
            this.g03_01_TabPage = new System.Windows.Forms.TabPage();
            this.g03_01_Panel = new System.Windows.Forms.Panel();
            this.g03_01_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_01_Folder_Label = new System.Windows.Forms.Label();
            this.g03_01_selectFolder_Button = new System.Windows.Forms.Button();
            this.g03_01_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_01_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label201 = new System.Windows.Forms.Label();
            this.g03_01_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_01_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label202 = new System.Windows.Forms.Label();
            this.g03_01_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_01_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label203 = new System.Windows.Forms.Label();
            this.g03_01_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.g03_01_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label208 = new System.Windows.Forms.Label();
            this.g03_01_TextBox9 = new System.Windows.Forms.TextBox();
            this.label209 = new System.Windows.Forms.Label();
            this.g03_01_TextBox8 = new System.Windows.Forms.TextBox();
            this.label210 = new System.Windows.Forms.Label();
            this.g03_01_TextBox7 = new System.Windows.Forms.TextBox();
            this.label211 = new System.Windows.Forms.Label();
            this.g03_01_TextBox6 = new System.Windows.Forms.TextBox();
            this.label212 = new System.Windows.Forms.Label();
            this.g03_01_TextBox5 = new System.Windows.Forms.TextBox();
            this.label213 = new System.Windows.Forms.Label();
            this.g03_01_TextBox4 = new System.Windows.Forms.TextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.g03_01_TextBox3 = new System.Windows.Forms.TextBox();
            this.label215 = new System.Windows.Forms.Label();
            this.g03_01_TextBox2 = new System.Windows.Forms.TextBox();
            this.label216 = new System.Windows.Forms.Label();
            this.g03_01_TextBox1 = new System.Windows.Forms.TextBox();
            this.g03_02_TabPage = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.g03_02_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_02_Folder_Label = new System.Windows.Forms.Label();
            this.g03_02_selectFolder_Button = new System.Windows.Forms.Button();
            this.g03_02_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_02_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label218 = new System.Windows.Forms.Label();
            this.g03_02_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_02_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label219 = new System.Windows.Forms.Label();
            this.g03_02_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_02_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label220 = new System.Windows.Forms.Label();
            this.g03_02_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.g03_02_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label225 = new System.Windows.Forms.Label();
            this.g03_02_TextBox9 = new System.Windows.Forms.TextBox();
            this.label226 = new System.Windows.Forms.Label();
            this.g03_02_TextBox8 = new System.Windows.Forms.TextBox();
            this.label227 = new System.Windows.Forms.Label();
            this.g03_02_TextBox7 = new System.Windows.Forms.TextBox();
            this.label228 = new System.Windows.Forms.Label();
            this.g03_02_TextBox6 = new System.Windows.Forms.TextBox();
            this.label229 = new System.Windows.Forms.Label();
            this.g03_02_TextBox5 = new System.Windows.Forms.TextBox();
            this.label230 = new System.Windows.Forms.Label();
            this.g03_02_TextBox4 = new System.Windows.Forms.TextBox();
            this.label231 = new System.Windows.Forms.Label();
            this.g03_02_TextBox3 = new System.Windows.Forms.TextBox();
            this.label232 = new System.Windows.Forms.Label();
            this.g03_02_TextBox2 = new System.Windows.Forms.TextBox();
            this.label233 = new System.Windows.Forms.Label();
            this.g03_02_TextBox1 = new System.Windows.Forms.TextBox();
            this.g03_03_TabPage = new System.Windows.Forms.TabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.g03_03_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_03_Folder_Label = new System.Windows.Forms.Label();
            this.g03_03_selectFolder_Button = new System.Windows.Forms.Button();
            this.g03_03_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_03_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label235 = new System.Windows.Forms.Label();
            this.g03_03_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_03_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label236 = new System.Windows.Forms.Label();
            this.g03_03_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_03_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label237 = new System.Windows.Forms.Label();
            this.g03_03_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label238 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this.g03_03_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label242 = new System.Windows.Forms.Label();
            this.g03_03_TextBox9 = new System.Windows.Forms.TextBox();
            this.label243 = new System.Windows.Forms.Label();
            this.g03_03_TextBox8 = new System.Windows.Forms.TextBox();
            this.label244 = new System.Windows.Forms.Label();
            this.g03_03_TextBox7 = new System.Windows.Forms.TextBox();
            this.label245 = new System.Windows.Forms.Label();
            this.g03_03_TextBox6 = new System.Windows.Forms.TextBox();
            this.label246 = new System.Windows.Forms.Label();
            this.g03_03_TextBox5 = new System.Windows.Forms.TextBox();
            this.label247 = new System.Windows.Forms.Label();
            this.g03_03_TextBox4 = new System.Windows.Forms.TextBox();
            this.label248 = new System.Windows.Forms.Label();
            this.g03_03_TextBox3 = new System.Windows.Forms.TextBox();
            this.label249 = new System.Windows.Forms.Label();
            this.g03_03_TextBox2 = new System.Windows.Forms.TextBox();
            this.label250 = new System.Windows.Forms.Label();
            this.g03_03_TextBox1 = new System.Windows.Forms.TextBox();
            this.g03_04_TabPage = new System.Windows.Forms.TabPage();
            this.panel20 = new System.Windows.Forms.Panel();
            this.g03_04_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_04_Folder_Label = new System.Windows.Forms.Label();
            this.g03_04_selectFolder_Button = new System.Windows.Forms.Button();
            this.g03_04_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_04_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label252 = new System.Windows.Forms.Label();
            this.g03_04_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_04_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label253 = new System.Windows.Forms.Label();
            this.g03_04_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_04_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label254 = new System.Windows.Forms.Label();
            this.g03_04_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label255 = new System.Windows.Forms.Label();
            this.label256 = new System.Windows.Forms.Label();
            this.label257 = new System.Windows.Forms.Label();
            this.label258 = new System.Windows.Forms.Label();
            this.g03_04_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label259 = new System.Windows.Forms.Label();
            this.g03_04_TextBox9 = new System.Windows.Forms.TextBox();
            this.label260 = new System.Windows.Forms.Label();
            this.g03_04_TextBox8 = new System.Windows.Forms.TextBox();
            this.label261 = new System.Windows.Forms.Label();
            this.g03_04_TextBox7 = new System.Windows.Forms.TextBox();
            this.label262 = new System.Windows.Forms.Label();
            this.g03_04_TextBox6 = new System.Windows.Forms.TextBox();
            this.label263 = new System.Windows.Forms.Label();
            this.g03_04_TextBox5 = new System.Windows.Forms.TextBox();
            this.label264 = new System.Windows.Forms.Label();
            this.g03_04_TextBox4 = new System.Windows.Forms.TextBox();
            this.label265 = new System.Windows.Forms.Label();
            this.g03_04_TextBox3 = new System.Windows.Forms.TextBox();
            this.label266 = new System.Windows.Forms.Label();
            this.g03_04_TextBox2 = new System.Windows.Forms.TextBox();
            this.label267 = new System.Windows.Forms.Label();
            this.g03_04_TextBox1 = new System.Windows.Forms.TextBox();
            this.g03_05_TabPage = new System.Windows.Forms.TabPage();
            this.panel22 = new System.Windows.Forms.Panel();
            this.g03_05_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_05_Folder_Label = new System.Windows.Forms.Label();
            this.g03_05_selectFolder_Button = new System.Windows.Forms.Button();
            this.g03_05_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_05_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label269 = new System.Windows.Forms.Label();
            this.g03_05_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_05_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label270 = new System.Windows.Forms.Label();
            this.g03_05_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_05_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label271 = new System.Windows.Forms.Label();
            this.g03_05_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label272 = new System.Windows.Forms.Label();
            this.label273 = new System.Windows.Forms.Label();
            this.label274 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.g03_05_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label276 = new System.Windows.Forms.Label();
            this.g03_05_TextBox9 = new System.Windows.Forms.TextBox();
            this.label277 = new System.Windows.Forms.Label();
            this.g03_05_TextBox8 = new System.Windows.Forms.TextBox();
            this.label278 = new System.Windows.Forms.Label();
            this.g03_05_TextBox7 = new System.Windows.Forms.TextBox();
            this.label279 = new System.Windows.Forms.Label();
            this.g03_05_TextBox6 = new System.Windows.Forms.TextBox();
            this.label280 = new System.Windows.Forms.Label();
            this.g03_05_TextBox5 = new System.Windows.Forms.TextBox();
            this.label281 = new System.Windows.Forms.Label();
            this.g03_05_TextBox4 = new System.Windows.Forms.TextBox();
            this.label282 = new System.Windows.Forms.Label();
            this.g03_05_TextBox3 = new System.Windows.Forms.TextBox();
            this.label283 = new System.Windows.Forms.Label();
            this.g03_05_TextBox2 = new System.Windows.Forms.TextBox();
            this.label284 = new System.Windows.Forms.Label();
            this.g03_05_TextBox1 = new System.Windows.Forms.TextBox();
            this.g03_06_TabPage = new System.Windows.Forms.TabPage();
            this.panel24 = new System.Windows.Forms.Panel();
            this.g03_06_Export_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_06_Folder_Label = new System.Windows.Forms.Label();
            this.g03_06_selectFolder_Button = new System.Windows.Forms.Button();
            this.g03_06_FFU_GroupBox = new System.Windows.Forms.GroupBox();
            this.g03_06_IsSensor_CheckBox = new System.Windows.Forms.CheckBox();
            this.label286 = new System.Windows.Forms.Label();
            this.g03_06_Particle_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_06_Particle_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label287 = new System.Windows.Forms.Label();
            this.g03_06_DPS_ID_TextBox = new System.Windows.Forms.TextBox();
            this.g03_06_DPS_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.label288 = new System.Windows.Forms.Label();
            this.g03_06_FFU_Quanity_TextBox = new System.Windows.Forms.TextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label289 = new System.Windows.Forms.Label();
            this.label290 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.g03_06_EDC_GroupBox = new System.Windows.Forms.GroupBox();
            this.label293 = new System.Windows.Forms.Label();
            this.g03_06_TextBox9 = new System.Windows.Forms.TextBox();
            this.label294 = new System.Windows.Forms.Label();
            this.g03_06_TextBox8 = new System.Windows.Forms.TextBox();
            this.label295 = new System.Windows.Forms.Label();
            this.g03_06_TextBox7 = new System.Windows.Forms.TextBox();
            this.label296 = new System.Windows.Forms.Label();
            this.g03_06_TextBox6 = new System.Windows.Forms.TextBox();
            this.label297 = new System.Windows.Forms.Label();
            this.g03_06_TextBox5 = new System.Windows.Forms.TextBox();
            this.label298 = new System.Windows.Forms.Label();
            this.g03_06_TextBox4 = new System.Windows.Forms.TextBox();
            this.label299 = new System.Windows.Forms.Label();
            this.g03_06_TextBox3 = new System.Windows.Forms.TextBox();
            this.label300 = new System.Windows.Forms.Label();
            this.g03_06_TextBox2 = new System.Windows.Forms.TextBox();
            this.label301 = new System.Windows.Forms.Label();
            this.g03_06_TextBox1 = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.g01_TabPage.SuspendLayout();
            this.g01_Panel.SuspendLayout();
            this.unit_Tab.SuspendLayout();
            this.g01_01_TabPage.SuspendLayout();
            this.g01_01_Panel.SuspendLayout();
            this.g01_01_Export_GroupBox.SuspendLayout();
            this.g01_01_FFU_GroupBox.SuspendLayout();
            this.panel3.SuspendLayout();
            this.g01_01_EDC_GroupBox.SuspendLayout();
            this.g01_02_TabPage.SuspendLayout();
            this.g01_02_Panel.SuspendLayout();
            this.g01_02_Export_GroupBox.SuspendLayout();
            this.g01_02_FFU_GroupBox.SuspendLayout();
            this.panel2.SuspendLayout();
            this.g01_02_EDC_GroupBox.SuspendLayout();
            this.g01_03_TabPage.SuspendLayout();
            this.g01_03_Panel.SuspendLayout();
            this.g01_03_Export_GroupBox.SuspendLayout();
            this.g01_03_FFU_GroupBox.SuspendLayout();
            this.panel4.SuspendLayout();
            this.g01_03_EDC_GroupBox.SuspendLayout();
            this.g01_04_TabPage.SuspendLayout();
            this.g01_04_Panel.SuspendLayout();
            this.g01_04_Export_GroupBox.SuspendLayout();
            this.g01_04_FFU_GroupBox.SuspendLayout();
            this.panel6.SuspendLayout();
            this.g01_04_EDC_GroupBox.SuspendLayout();
            this.g01_05_TabPage.SuspendLayout();
            this.g01_05_Panel.SuspendLayout();
            this.g01_05_Export_GroupBox.SuspendLayout();
            this.g01_05_FFU_GroupBox.SuspendLayout();
            this.panel8.SuspendLayout();
            this.g01_05_EDC_GroupBox.SuspendLayout();
            this.g01_06_TabPage.SuspendLayout();
            this.g01_06_Panel.SuspendLayout();
            this.g01_06_Export_GroupBox.SuspendLayout();
            this.g01_06_FFU_GroupBox.SuspendLayout();
            this.panel10.SuspendLayout();
            this.g01_06_EDC_GroupBox.SuspendLayout();
            this.gateway_Tab.SuspendLayout();
            this.g02_TabPage.SuspendLayout();
            this.g02_Panel.SuspendLayout();
            this.unit_Tab2.SuspendLayout();
            this.g02_01_TabPage.SuspendLayout();
            this.g02_01_Panel.SuspendLayout();
            this.g02_01_Export_GroupBox.SuspendLayout();
            this.g02_01_FFU_GroupBox.SuspendLayout();
            this.panel7.SuspendLayout();
            this.g02_01_EDC_GroupBox.SuspendLayout();
            this.g02_02_TabPage.SuspendLayout();
            this.g02_02_Panel.SuspendLayout();
            this.g02_02_Export_GroupBox.SuspendLayout();
            this.g02_02_FFU_GroupBox.SuspendLayout();
            this.panel11.SuspendLayout();
            this.g02_02_EDC_GroupBox.SuspendLayout();
            this.g02_03_TabPage.SuspendLayout();
            this.g02_03_Panel.SuspendLayout();
            this.g02_03_Export_GroupBox.SuspendLayout();
            this.g02_03_FFU_GroupBox.SuspendLayout();
            this.panel13.SuspendLayout();
            this.g02_03_EDC_GroupBox.SuspendLayout();
            this.g02_04_TabPage.SuspendLayout();
            this.g02_04_Panel.SuspendLayout();
            this.g02_04_Export_GroupBox.SuspendLayout();
            this.g02_04_FFU_GroupBox.SuspendLayout();
            this.panel15.SuspendLayout();
            this.g02_04_EDC_GroupBox.SuspendLayout();
            this.g02_05_TabPage.SuspendLayout();
            this.g02_05_Panel.SuspendLayout();
            this.g02_05_Export_GroupBox.SuspendLayout();
            this.g02_05_FFU_GroupBox.SuspendLayout();
            this.panel17.SuspendLayout();
            this.g02_05_EDC_GroupBox.SuspendLayout();
            this.g02_06_TabPage.SuspendLayout();
            this.g02_06_Panel.SuspendLayout();
            this.g02_06_Export_GroupBox.SuspendLayout();
            this.g02_06_FFU_GroupBox.SuspendLayout();
            this.panel19.SuspendLayout();
            this.g02_06_EDC_GroupBox.SuspendLayout();
            this.g03_TabPage.SuspendLayout();
            this.g03_Panel.SuspendLayout();
            this.unit_Tab3.SuspendLayout();
            this.g03_01_TabPage.SuspendLayout();
            this.g03_01_Panel.SuspendLayout();
            this.g03_01_Export_GroupBox.SuspendLayout();
            this.g03_01_FFU_GroupBox.SuspendLayout();
            this.panel9.SuspendLayout();
            this.g03_01_EDC_GroupBox.SuspendLayout();
            this.g03_02_TabPage.SuspendLayout();
            this.panel12.SuspendLayout();
            this.g03_02_Export_GroupBox.SuspendLayout();
            this.g03_02_FFU_GroupBox.SuspendLayout();
            this.panel14.SuspendLayout();
            this.g03_02_EDC_GroupBox.SuspendLayout();
            this.g03_03_TabPage.SuspendLayout();
            this.panel16.SuspendLayout();
            this.g03_03_Export_GroupBox.SuspendLayout();
            this.g03_03_FFU_GroupBox.SuspendLayout();
            this.panel18.SuspendLayout();
            this.g03_03_EDC_GroupBox.SuspendLayout();
            this.g03_04_TabPage.SuspendLayout();
            this.panel20.SuspendLayout();
            this.g03_04_Export_GroupBox.SuspendLayout();
            this.g03_04_FFU_GroupBox.SuspendLayout();
            this.panel21.SuspendLayout();
            this.g03_04_EDC_GroupBox.SuspendLayout();
            this.g03_05_TabPage.SuspendLayout();
            this.panel22.SuspendLayout();
            this.g03_05_Export_GroupBox.SuspendLayout();
            this.g03_05_FFU_GroupBox.SuspendLayout();
            this.panel23.SuspendLayout();
            this.g03_05_EDC_GroupBox.SuspendLayout();
            this.g03_06_TabPage.SuspendLayout();
            this.panel24.SuspendLayout();
            this.g03_06_Export_GroupBox.SuspendLayout();
            this.g03_06_FFU_GroupBox.SuspendLayout();
            this.panel25.SuspendLayout();
            this.g03_06_EDC_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // g01_TabPage
            // 
            this.g01_TabPage.Controls.Add(this.g01_Panel);
            this.g01_TabPage.Location = new System.Drawing.Point(4, 37);
            this.g01_TabPage.Name = "g01_TabPage";
            this.g01_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_TabPage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.g01_TabPage.Size = new System.Drawing.Size(1358, 842);
            this.g01_TabPage.TabIndex = 0;
            this.g01_TabPage.Text = "Gateway01";
            this.g01_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_Panel
            // 
            this.g01_Panel.Controls.Add(this.label18);
            this.g01_Panel.Controls.Add(this.g01_Time_comboBox);
            this.g01_Panel.Controls.Add(this.g01_connect_Button);
            this.g01_Panel.Controls.Add(this.g01_connect_Label);
            this.g01_Panel.Controls.Add(this.g01_IP_TextBox);
            this.g01_Panel.Controls.Add(this.label1);
            this.g01_Panel.Controls.Add(this.unit_Tab);
            this.g01_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_Panel.Name = "g01_Panel";
            this.g01_Panel.Size = new System.Drawing.Size(1338, 830);
            this.g01_Panel.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(27, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(116, 31);
            this.label18.TabIndex = 8;
            this.label18.Text = "匯出頻率:";
            // 
            // g01_Time_comboBox
            // 
            this.g01_Time_comboBox.FormattingEnabled = true;
            this.g01_Time_comboBox.Location = new System.Drawing.Point(149, 71);
            this.g01_Time_comboBox.Name = "g01_Time_comboBox";
            this.g01_Time_comboBox.Size = new System.Drawing.Size(171, 33);
            this.g01_Time_comboBox.TabIndex = 7;
            // 
            // g01_connect_Button
            // 
            this.g01_connect_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.g01_connect_Button.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_connect_Button.Location = new System.Drawing.Point(1134, 29);
            this.g01_connect_Button.Name = "g01_connect_Button";
            this.g01_connect_Button.Size = new System.Drawing.Size(188, 66);
            this.g01_connect_Button.TabIndex = 4;
            this.g01_connect_Button.Text = "連線";
            this.g01_connect_Button.UseVisualStyleBackColor = true;
            this.g01_connect_Button.Click += new System.EventHandler(this.Connect_Button_Click);
            // 
            // g01_connect_Label
            // 
            this.g01_connect_Label.AutoSize = true;
            this.g01_connect_Label.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_connect_Label.ForeColor = System.Drawing.Color.Red;
            this.g01_connect_Label.Location = new System.Drawing.Point(326, 22);
            this.g01_connect_Label.Name = "g01_connect_Label";
            this.g01_connect_Label.Size = new System.Drawing.Size(86, 31);
            this.g01_connect_Label.TabIndex = 3;
            this.g01_connect_Label.Text = "未連線";
            this.g01_connect_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_IP_TextBox
            // 
            this.g01_IP_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_IP_TextBox.Location = new System.Drawing.Point(82, 19);
            this.g01_IP_TextBox.Name = "g01_IP_TextBox";
            this.g01_IP_TextBox.Size = new System.Drawing.Size(238, 34);
            this.g01_IP_TextBox.TabIndex = 2;
            this.g01_IP_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(27, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP:";
            // 
            // unit_Tab
            // 
            this.unit_Tab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unit_Tab.Controls.Add(this.g01_01_TabPage);
            this.unit_Tab.Controls.Add(this.g01_02_TabPage);
            this.unit_Tab.Controls.Add(this.g01_03_TabPage);
            this.unit_Tab.Controls.Add(this.g01_04_TabPage);
            this.unit_Tab.Controls.Add(this.g01_05_TabPage);
            this.unit_Tab.Controls.Add(this.g01_06_TabPage);
            this.unit_Tab.Location = new System.Drawing.Point(3, 118);
            this.unit_Tab.Name = "unit_Tab";
            this.unit_Tab.SelectedIndex = 0;
            this.unit_Tab.Size = new System.Drawing.Size(1332, 709);
            this.unit_Tab.TabIndex = 0;
            // 
            // g01_01_TabPage
            // 
            this.g01_01_TabPage.Controls.Add(this.g01_01_Panel);
            this.g01_01_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_01_TabPage.Name = "g01_01_TabPage";
            this.g01_01_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_01_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g01_01_TabPage.TabIndex = 0;
            this.g01_01_TabPage.Text = "Unit01";
            this.g01_01_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_01_Panel
            // 
            this.g01_01_Panel.Controls.Add(this.g01_01_Export_GroupBox);
            this.g01_01_Panel.Controls.Add(this.g01_01_FFU_GroupBox);
            this.g01_01_Panel.Controls.Add(this.g01_01_EDC_GroupBox);
            this.g01_01_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_01_Panel.Name = "g01_01_Panel";
            this.g01_01_Panel.Size = new System.Drawing.Size(1309, 649);
            this.g01_01_Panel.TabIndex = 0;
            // 
            // g01_01_Export_GroupBox
            // 
            this.g01_01_Export_GroupBox.Controls.Add(this.g01_01_Folder_Label);
            this.g01_01_Export_GroupBox.Controls.Add(this.g01_01_selectFolder_Button);
            this.g01_01_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g01_01_Export_GroupBox.Name = "g01_01_Export_GroupBox";
            this.g01_01_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g01_01_Export_GroupBox.TabIndex = 22;
            this.g01_01_Export_GroupBox.TabStop = false;
            this.g01_01_Export_GroupBox.Text = "匯出路徑";
            // 
            // g01_01_Folder_Label
            // 
            this.g01_01_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_01_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g01_01_Folder_Label.Name = "g01_01_Folder_Label";
            this.g01_01_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g01_01_Folder_Label.TabIndex = 8;
            this.g01_01_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_01_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_01_selectFolder_Button
            // 
            this.g01_01_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g01_01_selectFolder_Button.Name = "g01_01_selectFolder_Button";
            this.g01_01_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_01_selectFolder_Button.TabIndex = 7;
            this.g01_01_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_01_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_01_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_01_FFU_GroupBox
            // 
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_IsSensor_CheckBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label17);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_Particle_ID_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_Particle_Quanity_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label16);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_DPS_ID_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_DPS_Quanity_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label15);
            this.g01_01_FFU_GroupBox.Controls.Add(this.g01_01_FFU_Quanity_TextBox);
            this.g01_01_FFU_GroupBox.Controls.Add(this.panel3);
            this.g01_01_FFU_GroupBox.Controls.Add(this.label14);
            this.g01_01_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g01_01_FFU_GroupBox.Name = "g01_01_FFU_GroupBox";
            this.g01_01_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_01_FFU_GroupBox.TabIndex = 21;
            this.g01_01_FFU_GroupBox.TabStop = false;
            this.g01_01_FFU_GroupBox.Text = "FFU";
            // 
            // g01_01_IsSensor_CheckBox
            // 
            this.g01_01_IsSensor_CheckBox.AutoSize = true;
            this.g01_01_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_01_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g01_01_IsSensor_CheckBox.Name = "g01_01_IsSensor_CheckBox";
            this.g01_01_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g01_01_IsSensor_CheckBox.TabIndex = 26;
            this.g01_01_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g01_01_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(212, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 31);
            this.label17.TabIndex = 25;
            this.label17.Text = "1";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_Particle_ID_TextBox
            // 
            this.g01_01_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_01_Particle_ID_TextBox.Name = "g01_01_Particle_ID_TextBox";
            this.g01_01_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_Particle_ID_TextBox.TabIndex = 24;
            this.g01_01_Particle_ID_TextBox.Text = "65";
            this.g01_01_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_01_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_01_Particle_Quanity_TextBox
            // 
            this.g01_01_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_01_Particle_Quanity_TextBox.Name = "g01_01_Particle_Quanity_TextBox";
            this.g01_01_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_01_Particle_Quanity_TextBox.Text = "1";
            this.g01_01_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(28, 273);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 31);
            this.label16.TabIndex = 22;
            this.label16.Text = "Particle";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_DPS_ID_TextBox
            // 
            this.g01_01_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_01_DPS_ID_TextBox.Name = "g01_01_DPS_ID_TextBox";
            this.g01_01_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_DPS_ID_TextBox.TabIndex = 21;
            this.g01_01_DPS_ID_TextBox.Text = "64";
            this.g01_01_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_01_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_01_DPS_Quanity_TextBox
            // 
            this.g01_01_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_01_DPS_Quanity_TextBox.Name = "g01_01_DPS_Quanity_TextBox";
            this.g01_01_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_01_DPS_Quanity_TextBox.Text = "1";
            this.g01_01_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(47, 202);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 31);
            this.label15.TabIndex = 19;
            this.label15.Text = "DPS";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_FFU_Quanity_TextBox
            // 
            this.g01_01_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_01_FFU_Quanity_TextBox.Name = "g01_01_FFU_Quanity_TextBox";
            this.g01_01_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_01_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_01_FFU_Quanity_TextBox.Text = "1";
            this.g01_01_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Location = new System.Drawing.Point(25, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(445, 69);
            this.panel3.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(21, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 31);
            this.label7.TabIndex = 0;
            this.label7.Text = "格式";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(148, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 31);
            this.label12.TabIndex = 1;
            this.label12.Text = "起始站號";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(316, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 31);
            this.label13.TabIndex = 2;
            this.label13.Text = "輸出數量";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(40, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 31);
            this.label14.TabIndex = 3;
            this.label14.Text = "SPEED";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_EDC_GroupBox
            // 
            this.g01_01_EDC_GroupBox.Controls.Add(this.label4);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox9);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label2);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox8);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label3);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox7);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label5);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox6);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label6);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox5);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label11);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox4);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label10);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox3);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label9);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox2);
            this.g01_01_EDC_GroupBox.Controls.Add(this.label8);
            this.g01_01_EDC_GroupBox.Controls.Add(this.g01_01_TextBox1);
            this.g01_01_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g01_01_EDC_GroupBox.Name = "g01_01_EDC_GroupBox";
            this.g01_01_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_01_EDC_GroupBox.TabIndex = 20;
            this.g01_01_EDC_GroupBox.TabStop = false;
            this.g01_01_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(51, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 31);
            this.label4.TabIndex = 2;
            this.label4.Text = "glass_id :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox9
            // 
            this.g01_01_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_01_TextBox9.Name = "g01_01_TextBox9";
            this.g01_01_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox9.TabIndex = 19;
            this.g01_01_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(17, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = "product_id :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox8
            // 
            this.g01_01_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_01_TextBox8.Name = "g01_01_TextBox8";
            this.g01_01_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox8.TabIndex = 18;
            this.g01_01_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(63, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "eqp_id :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox7
            // 
            this.g01_01_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_01_TextBox7.Name = "g01_01_TextBox7";
            this.g01_01_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox7.TabIndex = 17;
            this.g01_01_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(10, 341);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 31);
            this.label5.TabIndex = 5;
            this.label5.Text = "sub_eqp_id :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox6
            // 
            this.g01_01_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_01_TextBox6.Name = "g01_01_TextBox6";
            this.g01_01_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox6.TabIndex = 16;
            this.g01_01_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(71, 431);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 31);
            this.label6.TabIndex = 6;
            this.label6.Text = "owner :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox5
            // 
            this.g01_01_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_01_TextBox5.Name = "g01_01_TextBox5";
            this.g01_01_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox5.TabIndex = 15;
            this.g01_01_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(409, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 31);
            this.label11.TabIndex = 7;
            this.label11.Text = "recipe_id :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox4
            // 
            this.g01_01_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_01_TextBox4.Name = "g01_01_TextBox4";
            this.g01_01_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox4.TabIndex = 14;
            this.g01_01_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(402, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 31);
            this.label10.TabIndex = 8;
            this.label10.Text = "operation :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox3
            // 
            this.g01_01_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_01_TextBox3.Name = "g01_01_TextBox3";
            this.g01_01_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox3.TabIndex = 13;
            this.g01_01_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(413, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 31);
            this.label9.TabIndex = 9;
            this.label9.Text = "chamber :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox2
            // 
            this.g01_01_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_01_TextBox2.Name = "g01_01_TextBox2";
            this.g01_01_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox2.TabIndex = 12;
            this.g01_01_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(415, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 31);
            this.label8.TabIndex = 10;
            this.label8.Text = "operator :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_01_TextBox1
            // 
            this.g01_01_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_01_TextBox1.Name = "g01_01_TextBox1";
            this.g01_01_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_01_TextBox1.TabIndex = 11;
            this.g01_01_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g01_02_TabPage
            // 
            this.g01_02_TabPage.Controls.Add(this.g01_02_Panel);
            this.g01_02_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_02_TabPage.Name = "g01_02_TabPage";
            this.g01_02_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_02_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g01_02_TabPage.TabIndex = 1;
            this.g01_02_TabPage.Text = "Unit02";
            this.g01_02_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_02_Panel
            // 
            this.g01_02_Panel.Controls.Add(this.g01_02_Export_GroupBox);
            this.g01_02_Panel.Controls.Add(this.g01_02_FFU_GroupBox);
            this.g01_02_Panel.Controls.Add(this.g01_02_EDC_GroupBox);
            this.g01_02_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_02_Panel.Name = "g01_02_Panel";
            this.g01_02_Panel.Size = new System.Drawing.Size(1309, 649);
            this.g01_02_Panel.TabIndex = 1;
            // 
            // g01_02_Export_GroupBox
            // 
            this.g01_02_Export_GroupBox.Controls.Add(this.g01_02_Folder_Label);
            this.g01_02_Export_GroupBox.Controls.Add(this.g01_02_selectFolder_Button);
            this.g01_02_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g01_02_Export_GroupBox.Name = "g01_02_Export_GroupBox";
            this.g01_02_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g01_02_Export_GroupBox.TabIndex = 23;
            this.g01_02_Export_GroupBox.TabStop = false;
            this.g01_02_Export_GroupBox.Text = "匯出路徑";
            // 
            // g01_02_Folder_Label
            // 
            this.g01_02_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_02_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g01_02_Folder_Label.Name = "g01_02_Folder_Label";
            this.g01_02_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g01_02_Folder_Label.TabIndex = 8;
            this.g01_02_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_02_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_02_selectFolder_Button
            // 
            this.g01_02_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g01_02_selectFolder_Button.Name = "g01_02_selectFolder_Button";
            this.g01_02_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_02_selectFolder_Button.TabIndex = 7;
            this.g01_02_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_02_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_02_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_02_FFU_GroupBox
            // 
            this.g01_02_FFU_GroupBox.Controls.Add(this.g01_02_IsSensor_CheckBox);
            this.g01_02_FFU_GroupBox.Controls.Add(this.label19);
            this.g01_02_FFU_GroupBox.Controls.Add(this.g01_02_Particle_ID_TextBox);
            this.g01_02_FFU_GroupBox.Controls.Add(this.g01_02_Particle_Quanity_TextBox);
            this.g01_02_FFU_GroupBox.Controls.Add(this.label20);
            this.g01_02_FFU_GroupBox.Controls.Add(this.g01_02_DPS_ID_TextBox);
            this.g01_02_FFU_GroupBox.Controls.Add(this.g01_02_DPS_Quanity_TextBox);
            this.g01_02_FFU_GroupBox.Controls.Add(this.label21);
            this.g01_02_FFU_GroupBox.Controls.Add(this.g01_02_FFU_Quanity_TextBox);
            this.g01_02_FFU_GroupBox.Controls.Add(this.panel2);
            this.g01_02_FFU_GroupBox.Controls.Add(this.label25);
            this.g01_02_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g01_02_FFU_GroupBox.Name = "g01_02_FFU_GroupBox";
            this.g01_02_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_02_FFU_GroupBox.TabIndex = 21;
            this.g01_02_FFU_GroupBox.TabStop = false;
            this.g01_02_FFU_GroupBox.Text = "FFU";
            // 
            // g01_02_IsSensor_CheckBox
            // 
            this.g01_02_IsSensor_CheckBox.AutoSize = true;
            this.g01_02_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_02_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g01_02_IsSensor_CheckBox.Name = "g01_02_IsSensor_CheckBox";
            this.g01_02_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g01_02_IsSensor_CheckBox.TabIndex = 27;
            this.g01_02_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g01_02_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(212, 129);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 31);
            this.label19.TabIndex = 25;
            this.label19.Text = "1";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_Particle_ID_TextBox
            // 
            this.g01_02_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_02_Particle_ID_TextBox.Name = "g01_02_Particle_ID_TextBox";
            this.g01_02_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_02_Particle_ID_TextBox.TabIndex = 24;
            this.g01_02_Particle_ID_TextBox.Text = "65";
            this.g01_02_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_02_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_02_Particle_Quanity_TextBox
            // 
            this.g01_02_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_02_Particle_Quanity_TextBox.Name = "g01_02_Particle_Quanity_TextBox";
            this.g01_02_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_02_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_02_Particle_Quanity_TextBox.Text = "1";
            this.g01_02_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(28, 273);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 31);
            this.label20.TabIndex = 22;
            this.label20.Text = "Particle";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_DPS_ID_TextBox
            // 
            this.g01_02_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_02_DPS_ID_TextBox.Name = "g01_02_DPS_ID_TextBox";
            this.g01_02_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_02_DPS_ID_TextBox.TabIndex = 21;
            this.g01_02_DPS_ID_TextBox.Text = "64";
            this.g01_02_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_02_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_02_DPS_Quanity_TextBox
            // 
            this.g01_02_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_02_DPS_Quanity_TextBox.Name = "g01_02_DPS_Quanity_TextBox";
            this.g01_02_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_02_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_02_DPS_Quanity_TextBox.Text = "1";
            this.g01_02_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(47, 202);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 31);
            this.label21.TabIndex = 19;
            this.label21.Text = "DPS";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_FFU_Quanity_TextBox
            // 
            this.g01_02_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_02_FFU_Quanity_TextBox.Name = "g01_02_FFU_Quanity_TextBox";
            this.g01_02_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_02_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_02_FFU_Quanity_TextBox.Text = "1";
            this.g01_02_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Location = new System.Drawing.Point(25, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(445, 69);
            this.panel2.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(21, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 31);
            this.label22.TabIndex = 0;
            this.label22.Text = "格式";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(148, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 31);
            this.label23.TabIndex = 1;
            this.label23.Text = "起始站號";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(316, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(110, 31);
            this.label24.TabIndex = 2;
            this.label24.Text = "輸出數量";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(40, 133);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 31);
            this.label25.TabIndex = 3;
            this.label25.Text = "SPEED";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_EDC_GroupBox
            // 
            this.g01_02_EDC_GroupBox.Controls.Add(this.label26);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox9);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label27);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox8);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label28);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox7);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label29);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox6);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label30);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox5);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label31);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox4);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label32);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox3);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label33);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox2);
            this.g01_02_EDC_GroupBox.Controls.Add(this.label34);
            this.g01_02_EDC_GroupBox.Controls.Add(this.g01_02_TextBox1);
            this.g01_02_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g01_02_EDC_GroupBox.Name = "g01_02_EDC_GroupBox";
            this.g01_02_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_02_EDC_GroupBox.TabIndex = 20;
            this.g01_02_EDC_GroupBox.TabStop = false;
            this.g01_02_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(51, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(118, 31);
            this.label26.TabIndex = 2;
            this.label26.Text = "glass_id :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox9
            // 
            this.g01_02_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_02_TextBox9.Name = "g01_02_TextBox9";
            this.g01_02_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox9.TabIndex = 19;
            this.g01_02_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(17, 161);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(152, 31);
            this.label27.TabIndex = 3;
            this.label27.Text = "product_id :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox8
            // 
            this.g01_02_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_02_TextBox8.Name = "g01_02_TextBox8";
            this.g01_02_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox8.TabIndex = 18;
            this.g01_02_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(63, 251);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(106, 31);
            this.label28.TabIndex = 4;
            this.label28.Text = "eqp_id :";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox7
            // 
            this.g01_02_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_02_TextBox7.Name = "g01_02_TextBox7";
            this.g01_02_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox7.TabIndex = 17;
            this.g01_02_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(10, 341);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(159, 31);
            this.label29.TabIndex = 5;
            this.label29.Text = "sub_eqp_id :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox6
            // 
            this.g01_02_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_02_TextBox6.Name = "g01_02_TextBox6";
            this.g01_02_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox6.TabIndex = 16;
            this.g01_02_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(71, 431);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(98, 31);
            this.label30.TabIndex = 6;
            this.label30.Text = "owner :";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox5
            // 
            this.g01_02_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_02_TextBox5.Name = "g01_02_TextBox5";
            this.g01_02_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox5.TabIndex = 15;
            this.g01_02_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(409, 71);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(132, 31);
            this.label31.TabIndex = 7;
            this.label31.Text = "recipe_id :";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox4
            // 
            this.g01_02_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_02_TextBox4.Name = "g01_02_TextBox4";
            this.g01_02_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox4.TabIndex = 14;
            this.g01_02_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(402, 161);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(139, 31);
            this.label32.TabIndex = 8;
            this.label32.Text = "operation :";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox3
            // 
            this.g01_02_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_02_TextBox3.Name = "g01_02_TextBox3";
            this.g01_02_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox3.TabIndex = 13;
            this.g01_02_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(413, 251);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(128, 31);
            this.label33.TabIndex = 9;
            this.label33.Text = "chamber :";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox2
            // 
            this.g01_02_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_02_TextBox2.Name = "g01_02_TextBox2";
            this.g01_02_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox2.TabIndex = 12;
            this.g01_02_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(415, 341);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(126, 31);
            this.label34.TabIndex = 10;
            this.label34.Text = "operator :";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_02_TextBox1
            // 
            this.g01_02_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_02_TextBox1.Name = "g01_02_TextBox1";
            this.g01_02_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_02_TextBox1.TabIndex = 11;
            this.g01_02_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g01_03_TabPage
            // 
            this.g01_03_TabPage.Controls.Add(this.g01_03_Panel);
            this.g01_03_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_03_TabPage.Name = "g01_03_TabPage";
            this.g01_03_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_03_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g01_03_TabPage.TabIndex = 2;
            this.g01_03_TabPage.Text = "Unit03";
            this.g01_03_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_03_Panel
            // 
            this.g01_03_Panel.Controls.Add(this.g01_03_Export_GroupBox);
            this.g01_03_Panel.Controls.Add(this.g01_03_FFU_GroupBox);
            this.g01_03_Panel.Controls.Add(this.g01_03_EDC_GroupBox);
            this.g01_03_Panel.Location = new System.Drawing.Point(6, 5);
            this.g01_03_Panel.Name = "g01_03_Panel";
            this.g01_03_Panel.Size = new System.Drawing.Size(1309, 649);
            this.g01_03_Panel.TabIndex = 2;
            // 
            // g01_03_Export_GroupBox
            // 
            this.g01_03_Export_GroupBox.Controls.Add(this.g01_03_Folder_Label);
            this.g01_03_Export_GroupBox.Controls.Add(this.g01_03_selectFolder_Button);
            this.g01_03_Export_GroupBox.Location = new System.Drawing.Point(20, 4);
            this.g01_03_Export_GroupBox.Name = "g01_03_Export_GroupBox";
            this.g01_03_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g01_03_Export_GroupBox.TabIndex = 24;
            this.g01_03_Export_GroupBox.TabStop = false;
            this.g01_03_Export_GroupBox.Text = "匯出路徑";
            // 
            // g01_03_Folder_Label
            // 
            this.g01_03_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_03_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g01_03_Folder_Label.Name = "g01_03_Folder_Label";
            this.g01_03_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g01_03_Folder_Label.TabIndex = 8;
            this.g01_03_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_03_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_03_selectFolder_Button
            // 
            this.g01_03_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g01_03_selectFolder_Button.Name = "g01_03_selectFolder_Button";
            this.g01_03_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_03_selectFolder_Button.TabIndex = 7;
            this.g01_03_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_03_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_03_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_03_FFU_GroupBox
            // 
            this.g01_03_FFU_GroupBox.Controls.Add(this.g01_03_IsSensor_CheckBox);
            this.g01_03_FFU_GroupBox.Controls.Add(this.label35);
            this.g01_03_FFU_GroupBox.Controls.Add(this.g01_03_Particle_ID_TextBox);
            this.g01_03_FFU_GroupBox.Controls.Add(this.g01_03_Particle_Quanity_TextBox);
            this.g01_03_FFU_GroupBox.Controls.Add(this.label36);
            this.g01_03_FFU_GroupBox.Controls.Add(this.g01_03_DPS_ID_TextBox);
            this.g01_03_FFU_GroupBox.Controls.Add(this.g01_03_DPS_Quanity_TextBox);
            this.g01_03_FFU_GroupBox.Controls.Add(this.label37);
            this.g01_03_FFU_GroupBox.Controls.Add(this.g01_03_FFU_Quanity_TextBox);
            this.g01_03_FFU_GroupBox.Controls.Add(this.panel4);
            this.g01_03_FFU_GroupBox.Controls.Add(this.label41);
            this.g01_03_FFU_GroupBox.Location = new System.Drawing.Point(802, 134);
            this.g01_03_FFU_GroupBox.Name = "g01_03_FFU_GroupBox";
            this.g01_03_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_03_FFU_GroupBox.TabIndex = 21;
            this.g01_03_FFU_GroupBox.TabStop = false;
            this.g01_03_FFU_GroupBox.Text = "FFU";
            // 
            // g01_03_IsSensor_CheckBox
            // 
            this.g01_03_IsSensor_CheckBox.AutoSize = true;
            this.g01_03_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_03_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g01_03_IsSensor_CheckBox.Name = "g01_03_IsSensor_CheckBox";
            this.g01_03_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g01_03_IsSensor_CheckBox.TabIndex = 28;
            this.g01_03_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g01_03_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(212, 129);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 31);
            this.label35.TabIndex = 25;
            this.label35.Text = "1";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_Particle_ID_TextBox
            // 
            this.g01_03_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_03_Particle_ID_TextBox.Name = "g01_03_Particle_ID_TextBox";
            this.g01_03_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_03_Particle_ID_TextBox.TabIndex = 24;
            this.g01_03_Particle_ID_TextBox.Text = "65";
            this.g01_03_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_03_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_03_Particle_Quanity_TextBox
            // 
            this.g01_03_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_03_Particle_Quanity_TextBox.Name = "g01_03_Particle_Quanity_TextBox";
            this.g01_03_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_03_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_03_Particle_Quanity_TextBox.Text = "1";
            this.g01_03_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label36.Location = new System.Drawing.Point(28, 273);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(100, 31);
            this.label36.TabIndex = 22;
            this.label36.Text = "Particle";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_DPS_ID_TextBox
            // 
            this.g01_03_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_03_DPS_ID_TextBox.Name = "g01_03_DPS_ID_TextBox";
            this.g01_03_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_03_DPS_ID_TextBox.TabIndex = 21;
            this.g01_03_DPS_ID_TextBox.Text = "64";
            this.g01_03_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_03_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_03_DPS_Quanity_TextBox
            // 
            this.g01_03_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_03_DPS_Quanity_TextBox.Name = "g01_03_DPS_Quanity_TextBox";
            this.g01_03_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_03_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_03_DPS_Quanity_TextBox.Text = "1";
            this.g01_03_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(47, 202);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(62, 31);
            this.label37.TabIndex = 19;
            this.label37.Text = "DPS";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_FFU_Quanity_TextBox
            // 
            this.g01_03_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_03_FFU_Quanity_TextBox.Name = "g01_03_FFU_Quanity_TextBox";
            this.g01_03_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_03_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_03_FFU_Quanity_TextBox.Text = "1";
            this.g01_03_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Location = new System.Drawing.Point(25, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(445, 69);
            this.panel4.TabIndex = 4;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label38.Location = new System.Drawing.Point(21, 16);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(62, 31);
            this.label38.TabIndex = 0;
            this.label38.Text = "格式";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(148, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(110, 31);
            this.label39.TabIndex = 1;
            this.label39.Text = "起始站號";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(316, 16);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(110, 31);
            this.label40.TabIndex = 2;
            this.label40.Text = "輸出數量";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(40, 133);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(88, 31);
            this.label41.TabIndex = 3;
            this.label41.Text = "SPEED";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_EDC_GroupBox
            // 
            this.g01_03_EDC_GroupBox.Controls.Add(this.label42);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox9);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label43);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox8);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label44);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox7);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label45);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox6);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label46);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox5);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label47);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox4);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label48);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox3);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label49);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox2);
            this.g01_03_EDC_GroupBox.Controls.Add(this.label50);
            this.g01_03_EDC_GroupBox.Controls.Add(this.g01_03_TextBox1);
            this.g01_03_EDC_GroupBox.Location = new System.Drawing.Point(20, 134);
            this.g01_03_EDC_GroupBox.Name = "g01_03_EDC_GroupBox";
            this.g01_03_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_03_EDC_GroupBox.TabIndex = 20;
            this.g01_03_EDC_GroupBox.TabStop = false;
            this.g01_03_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(51, 71);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(118, 31);
            this.label42.TabIndex = 2;
            this.label42.Text = "glass_id :";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox9
            // 
            this.g01_03_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_03_TextBox9.Name = "g01_03_TextBox9";
            this.g01_03_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox9.TabIndex = 19;
            this.g01_03_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(17, 161);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(152, 31);
            this.label43.TabIndex = 3;
            this.label43.Text = "product_id :";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox8
            // 
            this.g01_03_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_03_TextBox8.Name = "g01_03_TextBox8";
            this.g01_03_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox8.TabIndex = 18;
            this.g01_03_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(63, 251);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(106, 31);
            this.label44.TabIndex = 4;
            this.label44.Text = "eqp_id :";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox7
            // 
            this.g01_03_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_03_TextBox7.Name = "g01_03_TextBox7";
            this.g01_03_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox7.TabIndex = 17;
            this.g01_03_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(10, 341);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(159, 31);
            this.label45.TabIndex = 5;
            this.label45.Text = "sub_eqp_id :";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox6
            // 
            this.g01_03_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_03_TextBox6.Name = "g01_03_TextBox6";
            this.g01_03_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox6.TabIndex = 16;
            this.g01_03_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(71, 431);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(98, 31);
            this.label46.TabIndex = 6;
            this.label46.Text = "owner :";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox5
            // 
            this.g01_03_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_03_TextBox5.Name = "g01_03_TextBox5";
            this.g01_03_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox5.TabIndex = 15;
            this.g01_03_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(409, 71);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(132, 31);
            this.label47.TabIndex = 7;
            this.label47.Text = "recipe_id :";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox4
            // 
            this.g01_03_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_03_TextBox4.Name = "g01_03_TextBox4";
            this.g01_03_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox4.TabIndex = 14;
            this.g01_03_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label48.Location = new System.Drawing.Point(402, 161);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(139, 31);
            this.label48.TabIndex = 8;
            this.label48.Text = "operation :";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox3
            // 
            this.g01_03_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_03_TextBox3.Name = "g01_03_TextBox3";
            this.g01_03_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox3.TabIndex = 13;
            this.g01_03_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(413, 251);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(128, 31);
            this.label49.TabIndex = 9;
            this.label49.Text = "chamber :";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox2
            // 
            this.g01_03_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_03_TextBox2.Name = "g01_03_TextBox2";
            this.g01_03_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox2.TabIndex = 12;
            this.g01_03_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label50.Location = new System.Drawing.Point(415, 341);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(126, 31);
            this.label50.TabIndex = 10;
            this.label50.Text = "operator :";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_03_TextBox1
            // 
            this.g01_03_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_03_TextBox1.Name = "g01_03_TextBox1";
            this.g01_03_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_03_TextBox1.TabIndex = 11;
            this.g01_03_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g01_04_TabPage
            // 
            this.g01_04_TabPage.Controls.Add(this.g01_04_Panel);
            this.g01_04_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_04_TabPage.Name = "g01_04_TabPage";
            this.g01_04_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_04_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g01_04_TabPage.TabIndex = 3;
            this.g01_04_TabPage.Text = "Unit04";
            this.g01_04_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_04_Panel
            // 
            this.g01_04_Panel.Controls.Add(this.g01_04_Export_GroupBox);
            this.g01_04_Panel.Controls.Add(this.g01_04_FFU_GroupBox);
            this.g01_04_Panel.Controls.Add(this.g01_04_EDC_GroupBox);
            this.g01_04_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_04_Panel.Name = "g01_04_Panel";
            this.g01_04_Panel.Size = new System.Drawing.Size(1309, 649);
            this.g01_04_Panel.TabIndex = 2;
            // 
            // g01_04_Export_GroupBox
            // 
            this.g01_04_Export_GroupBox.Controls.Add(this.g01_04_Folder_Label);
            this.g01_04_Export_GroupBox.Controls.Add(this.g01_04_selectFolder_Button);
            this.g01_04_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g01_04_Export_GroupBox.Name = "g01_04_Export_GroupBox";
            this.g01_04_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g01_04_Export_GroupBox.TabIndex = 25;
            this.g01_04_Export_GroupBox.TabStop = false;
            this.g01_04_Export_GroupBox.Text = "匯出路徑";
            // 
            // g01_04_Folder_Label
            // 
            this.g01_04_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_04_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g01_04_Folder_Label.Name = "g01_04_Folder_Label";
            this.g01_04_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g01_04_Folder_Label.TabIndex = 8;
            this.g01_04_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_04_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_04_selectFolder_Button
            // 
            this.g01_04_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g01_04_selectFolder_Button.Name = "g01_04_selectFolder_Button";
            this.g01_04_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_04_selectFolder_Button.TabIndex = 7;
            this.g01_04_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_04_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_04_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_04_FFU_GroupBox
            // 
            this.g01_04_FFU_GroupBox.Controls.Add(this.g01_04_IsSensor_CheckBox);
            this.g01_04_FFU_GroupBox.Controls.Add(this.label51);
            this.g01_04_FFU_GroupBox.Controls.Add(this.g01_04_Particle_ID_TextBox);
            this.g01_04_FFU_GroupBox.Controls.Add(this.g01_04_Particle_Quanity_TextBox);
            this.g01_04_FFU_GroupBox.Controls.Add(this.label52);
            this.g01_04_FFU_GroupBox.Controls.Add(this.g01_04_DPS_ID_TextBox);
            this.g01_04_FFU_GroupBox.Controls.Add(this.g01_04_DPS_Quanity_TextBox);
            this.g01_04_FFU_GroupBox.Controls.Add(this.label53);
            this.g01_04_FFU_GroupBox.Controls.Add(this.g01_04_FFU_Quanity_TextBox);
            this.g01_04_FFU_GroupBox.Controls.Add(this.panel6);
            this.g01_04_FFU_GroupBox.Controls.Add(this.label57);
            this.g01_04_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g01_04_FFU_GroupBox.Name = "g01_04_FFU_GroupBox";
            this.g01_04_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_04_FFU_GroupBox.TabIndex = 21;
            this.g01_04_FFU_GroupBox.TabStop = false;
            this.g01_04_FFU_GroupBox.Text = "FFU";
            // 
            // g01_04_IsSensor_CheckBox
            // 
            this.g01_04_IsSensor_CheckBox.AutoSize = true;
            this.g01_04_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_04_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g01_04_IsSensor_CheckBox.Name = "g01_04_IsSensor_CheckBox";
            this.g01_04_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g01_04_IsSensor_CheckBox.TabIndex = 29;
            this.g01_04_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g01_04_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label51.Location = new System.Drawing.Point(212, 129);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(28, 31);
            this.label51.TabIndex = 25;
            this.label51.Text = "1";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_Particle_ID_TextBox
            // 
            this.g01_04_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_04_Particle_ID_TextBox.Name = "g01_04_Particle_ID_TextBox";
            this.g01_04_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_04_Particle_ID_TextBox.TabIndex = 24;
            this.g01_04_Particle_ID_TextBox.Text = "65";
            this.g01_04_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_04_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_04_Particle_Quanity_TextBox
            // 
            this.g01_04_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_04_Particle_Quanity_TextBox.Name = "g01_04_Particle_Quanity_TextBox";
            this.g01_04_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_04_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_04_Particle_Quanity_TextBox.Text = "1";
            this.g01_04_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label52.Location = new System.Drawing.Point(28, 273);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(100, 31);
            this.label52.TabIndex = 22;
            this.label52.Text = "Particle";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_DPS_ID_TextBox
            // 
            this.g01_04_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_04_DPS_ID_TextBox.Name = "g01_04_DPS_ID_TextBox";
            this.g01_04_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_04_DPS_ID_TextBox.TabIndex = 21;
            this.g01_04_DPS_ID_TextBox.Text = "64";
            this.g01_04_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_04_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_04_DPS_Quanity_TextBox
            // 
            this.g01_04_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_04_DPS_Quanity_TextBox.Name = "g01_04_DPS_Quanity_TextBox";
            this.g01_04_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_04_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_04_DPS_Quanity_TextBox.Text = "1";
            this.g01_04_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label53.Location = new System.Drawing.Point(47, 202);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(62, 31);
            this.label53.TabIndex = 19;
            this.label53.Text = "DPS";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_FFU_Quanity_TextBox
            // 
            this.g01_04_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_04_FFU_Quanity_TextBox.Name = "g01_04_FFU_Quanity_TextBox";
            this.g01_04_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_04_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_04_FFU_Quanity_TextBox.Text = "1";
            this.g01_04_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label54);
            this.panel6.Controls.Add(this.label55);
            this.panel6.Controls.Add(this.label56);
            this.panel6.Location = new System.Drawing.Point(25, 33);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(445, 69);
            this.panel6.TabIndex = 4;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label54.Location = new System.Drawing.Point(21, 16);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(62, 31);
            this.label54.TabIndex = 0;
            this.label54.Text = "格式";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label55.Location = new System.Drawing.Point(148, 16);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(110, 31);
            this.label55.TabIndex = 1;
            this.label55.Text = "起始站號";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(316, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(110, 31);
            this.label56.TabIndex = 2;
            this.label56.Text = "輸出數量";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label57.Location = new System.Drawing.Point(40, 133);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(88, 31);
            this.label57.TabIndex = 3;
            this.label57.Text = "SPEED";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_EDC_GroupBox
            // 
            this.g01_04_EDC_GroupBox.Controls.Add(this.label58);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox9);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label59);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox8);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label60);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox7);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label61);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox6);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label62);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox5);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label63);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox4);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label64);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox3);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label65);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox2);
            this.g01_04_EDC_GroupBox.Controls.Add(this.label66);
            this.g01_04_EDC_GroupBox.Controls.Add(this.g01_04_TextBox1);
            this.g01_04_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g01_04_EDC_GroupBox.Name = "g01_04_EDC_GroupBox";
            this.g01_04_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_04_EDC_GroupBox.TabIndex = 20;
            this.g01_04_EDC_GroupBox.TabStop = false;
            this.g01_04_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label58.Location = new System.Drawing.Point(51, 71);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(118, 31);
            this.label58.TabIndex = 2;
            this.label58.Text = "glass_id :";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox9
            // 
            this.g01_04_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_04_TextBox9.Name = "g01_04_TextBox9";
            this.g01_04_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox9.TabIndex = 19;
            this.g01_04_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label59.Location = new System.Drawing.Point(17, 161);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(152, 31);
            this.label59.TabIndex = 3;
            this.label59.Text = "product_id :";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox8
            // 
            this.g01_04_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_04_TextBox8.Name = "g01_04_TextBox8";
            this.g01_04_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox8.TabIndex = 18;
            this.g01_04_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label60.Location = new System.Drawing.Point(63, 251);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(106, 31);
            this.label60.TabIndex = 4;
            this.label60.Text = "eqp_id :";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox7
            // 
            this.g01_04_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_04_TextBox7.Name = "g01_04_TextBox7";
            this.g01_04_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox7.TabIndex = 17;
            this.g01_04_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label61.Location = new System.Drawing.Point(10, 341);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(159, 31);
            this.label61.TabIndex = 5;
            this.label61.Text = "sub_eqp_id :";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox6
            // 
            this.g01_04_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_04_TextBox6.Name = "g01_04_TextBox6";
            this.g01_04_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox6.TabIndex = 16;
            this.g01_04_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label62.Location = new System.Drawing.Point(71, 431);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(98, 31);
            this.label62.TabIndex = 6;
            this.label62.Text = "owner :";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox5
            // 
            this.g01_04_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_04_TextBox5.Name = "g01_04_TextBox5";
            this.g01_04_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox5.TabIndex = 15;
            this.g01_04_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label63.Location = new System.Drawing.Point(409, 71);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(132, 31);
            this.label63.TabIndex = 7;
            this.label63.Text = "recipe_id :";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox4
            // 
            this.g01_04_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_04_TextBox4.Name = "g01_04_TextBox4";
            this.g01_04_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox4.TabIndex = 14;
            this.g01_04_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label64.Location = new System.Drawing.Point(402, 161);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(139, 31);
            this.label64.TabIndex = 8;
            this.label64.Text = "operation :";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox3
            // 
            this.g01_04_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_04_TextBox3.Name = "g01_04_TextBox3";
            this.g01_04_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox3.TabIndex = 13;
            this.g01_04_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label65.Location = new System.Drawing.Point(413, 251);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(128, 31);
            this.label65.TabIndex = 9;
            this.label65.Text = "chamber :";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox2
            // 
            this.g01_04_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_04_TextBox2.Name = "g01_04_TextBox2";
            this.g01_04_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox2.TabIndex = 12;
            this.g01_04_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label66.Location = new System.Drawing.Point(415, 341);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(126, 31);
            this.label66.TabIndex = 10;
            this.label66.Text = "operator :";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_04_TextBox1
            // 
            this.g01_04_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_04_TextBox1.Name = "g01_04_TextBox1";
            this.g01_04_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_04_TextBox1.TabIndex = 11;
            this.g01_04_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g01_05_TabPage
            // 
            this.g01_05_TabPage.Controls.Add(this.g01_05_Panel);
            this.g01_05_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_05_TabPage.Name = "g01_05_TabPage";
            this.g01_05_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_05_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g01_05_TabPage.TabIndex = 4;
            this.g01_05_TabPage.Text = "Unit05";
            this.g01_05_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_05_Panel
            // 
            this.g01_05_Panel.Controls.Add(this.g01_05_Export_GroupBox);
            this.g01_05_Panel.Controls.Add(this.g01_05_FFU_GroupBox);
            this.g01_05_Panel.Controls.Add(this.g01_05_EDC_GroupBox);
            this.g01_05_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_05_Panel.Name = "g01_05_Panel";
            this.g01_05_Panel.Size = new System.Drawing.Size(1309, 649);
            this.g01_05_Panel.TabIndex = 2;
            // 
            // g01_05_Export_GroupBox
            // 
            this.g01_05_Export_GroupBox.Controls.Add(this.g01_05_Folder_Label);
            this.g01_05_Export_GroupBox.Controls.Add(this.g01_05_selectFolder_Button);
            this.g01_05_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g01_05_Export_GroupBox.Name = "g01_05_Export_GroupBox";
            this.g01_05_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g01_05_Export_GroupBox.TabIndex = 26;
            this.g01_05_Export_GroupBox.TabStop = false;
            this.g01_05_Export_GroupBox.Text = "匯出路徑";
            // 
            // g01_05_Folder_Label
            // 
            this.g01_05_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_05_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g01_05_Folder_Label.Name = "g01_05_Folder_Label";
            this.g01_05_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g01_05_Folder_Label.TabIndex = 8;
            this.g01_05_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_05_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_05_selectFolder_Button
            // 
            this.g01_05_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g01_05_selectFolder_Button.Name = "g01_05_selectFolder_Button";
            this.g01_05_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_05_selectFolder_Button.TabIndex = 7;
            this.g01_05_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_05_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_05_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_05_FFU_GroupBox
            // 
            this.g01_05_FFU_GroupBox.Controls.Add(this.g01_05_IsSensor_CheckBox);
            this.g01_05_FFU_GroupBox.Controls.Add(this.label67);
            this.g01_05_FFU_GroupBox.Controls.Add(this.g01_05_Particle_ID_TextBox);
            this.g01_05_FFU_GroupBox.Controls.Add(this.g01_05_Particle_Quanity_TextBox);
            this.g01_05_FFU_GroupBox.Controls.Add(this.label68);
            this.g01_05_FFU_GroupBox.Controls.Add(this.g01_05_DPS_ID_TextBox);
            this.g01_05_FFU_GroupBox.Controls.Add(this.g01_05_DPS_Quanity_TextBox);
            this.g01_05_FFU_GroupBox.Controls.Add(this.label69);
            this.g01_05_FFU_GroupBox.Controls.Add(this.g01_05_FFU_Quanity_TextBox);
            this.g01_05_FFU_GroupBox.Controls.Add(this.panel8);
            this.g01_05_FFU_GroupBox.Controls.Add(this.label73);
            this.g01_05_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g01_05_FFU_GroupBox.Name = "g01_05_FFU_GroupBox";
            this.g01_05_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_05_FFU_GroupBox.TabIndex = 21;
            this.g01_05_FFU_GroupBox.TabStop = false;
            this.g01_05_FFU_GroupBox.Text = "FFU";
            // 
            // g01_05_IsSensor_CheckBox
            // 
            this.g01_05_IsSensor_CheckBox.AutoSize = true;
            this.g01_05_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_05_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g01_05_IsSensor_CheckBox.Name = "g01_05_IsSensor_CheckBox";
            this.g01_05_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g01_05_IsSensor_CheckBox.TabIndex = 30;
            this.g01_05_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g01_05_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label67.Location = new System.Drawing.Point(212, 129);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(28, 31);
            this.label67.TabIndex = 25;
            this.label67.Text = "1";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_Particle_ID_TextBox
            // 
            this.g01_05_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_05_Particle_ID_TextBox.Name = "g01_05_Particle_ID_TextBox";
            this.g01_05_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_05_Particle_ID_TextBox.TabIndex = 24;
            this.g01_05_Particle_ID_TextBox.Text = "65";
            this.g01_05_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_05_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_05_Particle_Quanity_TextBox
            // 
            this.g01_05_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_05_Particle_Quanity_TextBox.Name = "g01_05_Particle_Quanity_TextBox";
            this.g01_05_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_05_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_05_Particle_Quanity_TextBox.Text = "1";
            this.g01_05_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label68.Location = new System.Drawing.Point(28, 273);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(100, 31);
            this.label68.TabIndex = 22;
            this.label68.Text = "Particle";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_DPS_ID_TextBox
            // 
            this.g01_05_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_05_DPS_ID_TextBox.Name = "g01_05_DPS_ID_TextBox";
            this.g01_05_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_05_DPS_ID_TextBox.TabIndex = 21;
            this.g01_05_DPS_ID_TextBox.Text = "64";
            this.g01_05_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_05_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_05_DPS_Quanity_TextBox
            // 
            this.g01_05_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_05_DPS_Quanity_TextBox.Name = "g01_05_DPS_Quanity_TextBox";
            this.g01_05_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_05_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_05_DPS_Quanity_TextBox.Text = "1";
            this.g01_05_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label69.Location = new System.Drawing.Point(47, 202);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(62, 31);
            this.label69.TabIndex = 19;
            this.label69.Text = "DPS";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_FFU_Quanity_TextBox
            // 
            this.g01_05_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_05_FFU_Quanity_TextBox.Name = "g01_05_FFU_Quanity_TextBox";
            this.g01_05_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_05_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_05_FFU_Quanity_TextBox.Text = "1";
            this.g01_05_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label70);
            this.panel8.Controls.Add(this.label71);
            this.panel8.Controls.Add(this.label72);
            this.panel8.Location = new System.Drawing.Point(25, 33);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(445, 69);
            this.panel8.TabIndex = 4;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label70.Location = new System.Drawing.Point(21, 16);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(62, 31);
            this.label70.TabIndex = 0;
            this.label70.Text = "格式";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label71.Location = new System.Drawing.Point(148, 16);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(110, 31);
            this.label71.TabIndex = 1;
            this.label71.Text = "起始站號";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label72.Location = new System.Drawing.Point(316, 16);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(110, 31);
            this.label72.TabIndex = 2;
            this.label72.Text = "輸出數量";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label73.Location = new System.Drawing.Point(40, 133);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(88, 31);
            this.label73.TabIndex = 3;
            this.label73.Text = "SPEED";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_EDC_GroupBox
            // 
            this.g01_05_EDC_GroupBox.Controls.Add(this.label74);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox9);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label75);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox8);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label76);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox7);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label77);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox6);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label78);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox5);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label79);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox4);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label80);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox3);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label81);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox2);
            this.g01_05_EDC_GroupBox.Controls.Add(this.label82);
            this.g01_05_EDC_GroupBox.Controls.Add(this.g01_05_TextBox1);
            this.g01_05_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g01_05_EDC_GroupBox.Name = "g01_05_EDC_GroupBox";
            this.g01_05_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_05_EDC_GroupBox.TabIndex = 20;
            this.g01_05_EDC_GroupBox.TabStop = false;
            this.g01_05_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label74.Location = new System.Drawing.Point(51, 71);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(118, 31);
            this.label74.TabIndex = 2;
            this.label74.Text = "glass_id :";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox9
            // 
            this.g01_05_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_05_TextBox9.Name = "g01_05_TextBox9";
            this.g01_05_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox9.TabIndex = 19;
            this.g01_05_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label75.Location = new System.Drawing.Point(17, 161);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(152, 31);
            this.label75.TabIndex = 3;
            this.label75.Text = "product_id :";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox8
            // 
            this.g01_05_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_05_TextBox8.Name = "g01_05_TextBox8";
            this.g01_05_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox8.TabIndex = 18;
            this.g01_05_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label76.Location = new System.Drawing.Point(63, 251);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(106, 31);
            this.label76.TabIndex = 4;
            this.label76.Text = "eqp_id :";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox7
            // 
            this.g01_05_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_05_TextBox7.Name = "g01_05_TextBox7";
            this.g01_05_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox7.TabIndex = 17;
            this.g01_05_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label77.Location = new System.Drawing.Point(10, 341);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(159, 31);
            this.label77.TabIndex = 5;
            this.label77.Text = "sub_eqp_id :";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox6
            // 
            this.g01_05_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_05_TextBox6.Name = "g01_05_TextBox6";
            this.g01_05_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox6.TabIndex = 16;
            this.g01_05_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label78.Location = new System.Drawing.Point(71, 431);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(98, 31);
            this.label78.TabIndex = 6;
            this.label78.Text = "owner :";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox5
            // 
            this.g01_05_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_05_TextBox5.Name = "g01_05_TextBox5";
            this.g01_05_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox5.TabIndex = 15;
            this.g01_05_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label79.Location = new System.Drawing.Point(409, 71);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(132, 31);
            this.label79.TabIndex = 7;
            this.label79.Text = "recipe_id :";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox4
            // 
            this.g01_05_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_05_TextBox4.Name = "g01_05_TextBox4";
            this.g01_05_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox4.TabIndex = 14;
            this.g01_05_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label80.Location = new System.Drawing.Point(402, 161);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(139, 31);
            this.label80.TabIndex = 8;
            this.label80.Text = "operation :";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox3
            // 
            this.g01_05_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_05_TextBox3.Name = "g01_05_TextBox3";
            this.g01_05_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox3.TabIndex = 13;
            this.g01_05_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label81.Location = new System.Drawing.Point(413, 251);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(128, 31);
            this.label81.TabIndex = 9;
            this.label81.Text = "chamber :";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox2
            // 
            this.g01_05_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_05_TextBox2.Name = "g01_05_TextBox2";
            this.g01_05_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox2.TabIndex = 12;
            this.g01_05_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label82.Location = new System.Drawing.Point(415, 341);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(126, 31);
            this.label82.TabIndex = 10;
            this.label82.Text = "operator :";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_05_TextBox1
            // 
            this.g01_05_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_05_TextBox1.Name = "g01_05_TextBox1";
            this.g01_05_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_05_TextBox1.TabIndex = 11;
            this.g01_05_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g01_06_TabPage
            // 
            this.g01_06_TabPage.Controls.Add(this.g01_06_Panel);
            this.g01_06_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g01_06_TabPage.Name = "g01_06_TabPage";
            this.g01_06_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g01_06_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g01_06_TabPage.TabIndex = 5;
            this.g01_06_TabPage.Text = "Unit06";
            this.g01_06_TabPage.UseVisualStyleBackColor = true;
            // 
            // g01_06_Panel
            // 
            this.g01_06_Panel.Controls.Add(this.g01_06_Export_GroupBox);
            this.g01_06_Panel.Controls.Add(this.g01_06_FFU_GroupBox);
            this.g01_06_Panel.Controls.Add(this.g01_06_EDC_GroupBox);
            this.g01_06_Panel.Location = new System.Drawing.Point(6, 6);
            this.g01_06_Panel.Name = "g01_06_Panel";
            this.g01_06_Panel.Size = new System.Drawing.Size(1309, 649);
            this.g01_06_Panel.TabIndex = 2;
            // 
            // g01_06_Export_GroupBox
            // 
            this.g01_06_Export_GroupBox.Controls.Add(this.g01_06_Folder_Label);
            this.g01_06_Export_GroupBox.Controls.Add(this.g01_06_selectFolder_Button);
            this.g01_06_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g01_06_Export_GroupBox.Name = "g01_06_Export_GroupBox";
            this.g01_06_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g01_06_Export_GroupBox.TabIndex = 27;
            this.g01_06_Export_GroupBox.TabStop = false;
            this.g01_06_Export_GroupBox.Text = "匯出路徑";
            // 
            // g01_06_Folder_Label
            // 
            this.g01_06_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_06_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g01_06_Folder_Label.Name = "g01_06_Folder_Label";
            this.g01_06_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g01_06_Folder_Label.TabIndex = 8;
            this.g01_06_Folder_Label.Text = "請選擇匯出路徑";
            this.g01_06_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g01_06_selectFolder_Button
            // 
            this.g01_06_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g01_06_selectFolder_Button.Name = "g01_06_selectFolder_Button";
            this.g01_06_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g01_06_selectFolder_Button.TabIndex = 7;
            this.g01_06_selectFolder_Button.Text = "選擇匯出路徑";
            this.g01_06_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g01_06_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g01_06_FFU_GroupBox
            // 
            this.g01_06_FFU_GroupBox.Controls.Add(this.g01_06_IsSensor_CheckBox);
            this.g01_06_FFU_GroupBox.Controls.Add(this.label83);
            this.g01_06_FFU_GroupBox.Controls.Add(this.g01_06_Particle_ID_TextBox);
            this.g01_06_FFU_GroupBox.Controls.Add(this.g01_06_Particle_Quanity_TextBox);
            this.g01_06_FFU_GroupBox.Controls.Add(this.label84);
            this.g01_06_FFU_GroupBox.Controls.Add(this.g01_06_DPS_ID_TextBox);
            this.g01_06_FFU_GroupBox.Controls.Add(this.g01_06_DPS_Quanity_TextBox);
            this.g01_06_FFU_GroupBox.Controls.Add(this.label85);
            this.g01_06_FFU_GroupBox.Controls.Add(this.g01_06_FFU_Quanity_TextBox);
            this.g01_06_FFU_GroupBox.Controls.Add(this.panel10);
            this.g01_06_FFU_GroupBox.Controls.Add(this.label89);
            this.g01_06_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g01_06_FFU_GroupBox.Name = "g01_06_FFU_GroupBox";
            this.g01_06_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g01_06_FFU_GroupBox.TabIndex = 21;
            this.g01_06_FFU_GroupBox.TabStop = false;
            this.g01_06_FFU_GroupBox.Text = "FFU";
            // 
            // g01_06_IsSensor_CheckBox
            // 
            this.g01_06_IsSensor_CheckBox.AutoSize = true;
            this.g01_06_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g01_06_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g01_06_IsSensor_CheckBox.Name = "g01_06_IsSensor_CheckBox";
            this.g01_06_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g01_06_IsSensor_CheckBox.TabIndex = 31;
            this.g01_06_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g01_06_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label83.Location = new System.Drawing.Point(212, 129);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 31);
            this.label83.TabIndex = 25;
            this.label83.Text = "1";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_Particle_ID_TextBox
            // 
            this.g01_06_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g01_06_Particle_ID_TextBox.Name = "g01_06_Particle_ID_TextBox";
            this.g01_06_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_06_Particle_ID_TextBox.TabIndex = 24;
            this.g01_06_Particle_ID_TextBox.Text = "65";
            this.g01_06_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_06_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g01_06_Particle_Quanity_TextBox
            // 
            this.g01_06_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g01_06_Particle_Quanity_TextBox.Name = "g01_06_Particle_Quanity_TextBox";
            this.g01_06_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_06_Particle_Quanity_TextBox.TabIndex = 23;
            this.g01_06_Particle_Quanity_TextBox.Text = "1";
            this.g01_06_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label84.Location = new System.Drawing.Point(28, 273);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(100, 31);
            this.label84.TabIndex = 22;
            this.label84.Text = "Particle";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_DPS_ID_TextBox
            // 
            this.g01_06_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g01_06_DPS_ID_TextBox.Name = "g01_06_DPS_ID_TextBox";
            this.g01_06_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_06_DPS_ID_TextBox.TabIndex = 21;
            this.g01_06_DPS_ID_TextBox.Text = "64";
            this.g01_06_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g01_06_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g01_06_DPS_Quanity_TextBox
            // 
            this.g01_06_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g01_06_DPS_Quanity_TextBox.Name = "g01_06_DPS_Quanity_TextBox";
            this.g01_06_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_06_DPS_Quanity_TextBox.TabIndex = 20;
            this.g01_06_DPS_Quanity_TextBox.Text = "1";
            this.g01_06_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label85.Location = new System.Drawing.Point(47, 202);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(62, 31);
            this.label85.TabIndex = 19;
            this.label85.Text = "DPS";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_FFU_Quanity_TextBox
            // 
            this.g01_06_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g01_06_FFU_Quanity_TextBox.Name = "g01_06_FFU_Quanity_TextBox";
            this.g01_06_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g01_06_FFU_Quanity_TextBox.TabIndex = 17;
            this.g01_06_FFU_Quanity_TextBox.Text = "1";
            this.g01_06_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label86);
            this.panel10.Controls.Add(this.label87);
            this.panel10.Controls.Add(this.label88);
            this.panel10.Location = new System.Drawing.Point(25, 33);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(445, 69);
            this.panel10.TabIndex = 4;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label86.Location = new System.Drawing.Point(21, 16);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(62, 31);
            this.label86.TabIndex = 0;
            this.label86.Text = "格式";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label87.Location = new System.Drawing.Point(148, 16);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(110, 31);
            this.label87.TabIndex = 1;
            this.label87.Text = "起始站號";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label88.Location = new System.Drawing.Point(316, 16);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(110, 31);
            this.label88.TabIndex = 2;
            this.label88.Text = "輸出數量";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label89.Location = new System.Drawing.Point(40, 133);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(88, 31);
            this.label89.TabIndex = 3;
            this.label89.Text = "SPEED";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_EDC_GroupBox
            // 
            this.g01_06_EDC_GroupBox.Controls.Add(this.label90);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox9);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label91);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox8);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label92);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox7);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label93);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox6);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label94);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox5);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label95);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox4);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label96);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox3);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label97);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox2);
            this.g01_06_EDC_GroupBox.Controls.Add(this.label98);
            this.g01_06_EDC_GroupBox.Controls.Add(this.g01_06_TextBox1);
            this.g01_06_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g01_06_EDC_GroupBox.Name = "g01_06_EDC_GroupBox";
            this.g01_06_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g01_06_EDC_GroupBox.TabIndex = 20;
            this.g01_06_EDC_GroupBox.TabStop = false;
            this.g01_06_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label90.Location = new System.Drawing.Point(51, 71);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(118, 31);
            this.label90.TabIndex = 2;
            this.label90.Text = "glass_id :";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox9
            // 
            this.g01_06_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g01_06_TextBox9.Name = "g01_06_TextBox9";
            this.g01_06_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox9.TabIndex = 19;
            this.g01_06_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label91.Location = new System.Drawing.Point(17, 161);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(152, 31);
            this.label91.TabIndex = 3;
            this.label91.Text = "product_id :";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox8
            // 
            this.g01_06_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g01_06_TextBox8.Name = "g01_06_TextBox8";
            this.g01_06_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox8.TabIndex = 18;
            this.g01_06_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label92.Location = new System.Drawing.Point(63, 251);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(106, 31);
            this.label92.TabIndex = 4;
            this.label92.Text = "eqp_id :";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox7
            // 
            this.g01_06_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g01_06_TextBox7.Name = "g01_06_TextBox7";
            this.g01_06_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox7.TabIndex = 17;
            this.g01_06_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label93.Location = new System.Drawing.Point(10, 341);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(159, 31);
            this.label93.TabIndex = 5;
            this.label93.Text = "sub_eqp_id :";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox6
            // 
            this.g01_06_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g01_06_TextBox6.Name = "g01_06_TextBox6";
            this.g01_06_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox6.TabIndex = 16;
            this.g01_06_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label94.Location = new System.Drawing.Point(71, 431);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(98, 31);
            this.label94.TabIndex = 6;
            this.label94.Text = "owner :";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox5
            // 
            this.g01_06_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g01_06_TextBox5.Name = "g01_06_TextBox5";
            this.g01_06_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox5.TabIndex = 15;
            this.g01_06_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label95.Location = new System.Drawing.Point(409, 71);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(132, 31);
            this.label95.TabIndex = 7;
            this.label95.Text = "recipe_id :";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox4
            // 
            this.g01_06_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g01_06_TextBox4.Name = "g01_06_TextBox4";
            this.g01_06_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox4.TabIndex = 14;
            this.g01_06_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label96.Location = new System.Drawing.Point(402, 161);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(139, 31);
            this.label96.TabIndex = 8;
            this.label96.Text = "operation :";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox3
            // 
            this.g01_06_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g01_06_TextBox3.Name = "g01_06_TextBox3";
            this.g01_06_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox3.TabIndex = 13;
            this.g01_06_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label97.Location = new System.Drawing.Point(413, 251);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(128, 31);
            this.label97.TabIndex = 9;
            this.label97.Text = "chamber :";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox2
            // 
            this.g01_06_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g01_06_TextBox2.Name = "g01_06_TextBox2";
            this.g01_06_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox2.TabIndex = 12;
            this.g01_06_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label98.Location = new System.Drawing.Point(415, 341);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(126, 31);
            this.label98.TabIndex = 10;
            this.label98.Text = "operator :";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g01_06_TextBox1
            // 
            this.g01_06_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g01_06_TextBox1.Name = "g01_06_TextBox1";
            this.g01_06_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g01_06_TextBox1.TabIndex = 11;
            this.g01_06_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gateway_Tab
            // 
            this.gateway_Tab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gateway_Tab.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.gateway_Tab.Controls.Add(this.g01_TabPage);
            this.gateway_Tab.Controls.Add(this.g02_TabPage);
            this.gateway_Tab.Controls.Add(this.g03_TabPage);
            this.gateway_Tab.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gateway_Tab.Location = new System.Drawing.Point(12, 12);
            this.gateway_Tab.Name = "gateway_Tab";
            this.gateway_Tab.SelectedIndex = 0;
            this.gateway_Tab.Size = new System.Drawing.Size(1366, 883);
            this.gateway_Tab.TabIndex = 0;
            this.gateway_Tab.Tag = "";
            // 
            // g02_TabPage
            // 
            this.g02_TabPage.Controls.Add(this.g02_Panel);
            this.g02_TabPage.Location = new System.Drawing.Point(4, 37);
            this.g02_TabPage.Name = "g02_TabPage";
            this.g02_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_TabPage.Size = new System.Drawing.Size(1358, 842);
            this.g02_TabPage.TabIndex = 1;
            this.g02_TabPage.Text = "Gateway02";
            this.g02_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_Panel
            // 
            this.g02_Panel.Controls.Add(this.label99);
            this.g02_Panel.Controls.Add(this.g02_Time_comboBox);
            this.g02_Panel.Controls.Add(this.g02_connect_Button);
            this.g02_Panel.Controls.Add(this.g02_connect_Label);
            this.g02_Panel.Controls.Add(this.g02_IP_TextBox);
            this.g02_Panel.Controls.Add(this.label102);
            this.g02_Panel.Controls.Add(this.unit_Tab2);
            this.g02_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_Panel.Name = "g02_Panel";
            this.g02_Panel.Size = new System.Drawing.Size(1338, 830);
            this.g02_Panel.TabIndex = 3;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label99.Location = new System.Drawing.Point(27, 71);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(116, 31);
            this.label99.TabIndex = 8;
            this.label99.Text = "匯出頻率:";
            // 
            // g02_Time_comboBox
            // 
            this.g02_Time_comboBox.FormattingEnabled = true;
            this.g02_Time_comboBox.Location = new System.Drawing.Point(149, 71);
            this.g02_Time_comboBox.Name = "g02_Time_comboBox";
            this.g02_Time_comboBox.Size = new System.Drawing.Size(171, 33);
            this.g02_Time_comboBox.TabIndex = 7;
            // 
            // g02_connect_Button
            // 
            this.g02_connect_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.g02_connect_Button.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_connect_Button.Location = new System.Drawing.Point(1134, 29);
            this.g02_connect_Button.Name = "g02_connect_Button";
            this.g02_connect_Button.Size = new System.Drawing.Size(188, 66);
            this.g02_connect_Button.TabIndex = 4;
            this.g02_connect_Button.Text = "連線";
            this.g02_connect_Button.UseVisualStyleBackColor = true;
            this.g02_connect_Button.Click += new System.EventHandler(this.Connect_Button_Click);
            // 
            // g02_connect_Label
            // 
            this.g02_connect_Label.AutoSize = true;
            this.g02_connect_Label.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_connect_Label.ForeColor = System.Drawing.Color.Red;
            this.g02_connect_Label.Location = new System.Drawing.Point(326, 22);
            this.g02_connect_Label.Name = "g02_connect_Label";
            this.g02_connect_Label.Size = new System.Drawing.Size(86, 31);
            this.g02_connect_Label.TabIndex = 3;
            this.g02_connect_Label.Text = "未連線";
            this.g02_connect_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_IP_TextBox
            // 
            this.g02_IP_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_IP_TextBox.Location = new System.Drawing.Point(82, 19);
            this.g02_IP_TextBox.Name = "g02_IP_TextBox";
            this.g02_IP_TextBox.Size = new System.Drawing.Size(238, 34);
            this.g02_IP_TextBox.TabIndex = 2;
            this.g02_IP_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label102.Location = new System.Drawing.Point(27, 19);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(49, 36);
            this.label102.TabIndex = 1;
            this.label102.Text = "IP:";
            // 
            // unit_Tab2
            // 
            this.unit_Tab2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unit_Tab2.Controls.Add(this.g02_01_TabPage);
            this.unit_Tab2.Controls.Add(this.g02_02_TabPage);
            this.unit_Tab2.Controls.Add(this.g02_03_TabPage);
            this.unit_Tab2.Controls.Add(this.g02_04_TabPage);
            this.unit_Tab2.Controls.Add(this.g02_05_TabPage);
            this.unit_Tab2.Controls.Add(this.g02_06_TabPage);
            this.unit_Tab2.Location = new System.Drawing.Point(3, 118);
            this.unit_Tab2.Name = "unit_Tab2";
            this.unit_Tab2.SelectedIndex = 0;
            this.unit_Tab2.Size = new System.Drawing.Size(1332, 709);
            this.unit_Tab2.TabIndex = 0;
            // 
            // g02_01_TabPage
            // 
            this.g02_01_TabPage.Controls.Add(this.g02_01_Panel);
            this.g02_01_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g02_01_TabPage.Name = "g02_01_TabPage";
            this.g02_01_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_01_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g02_01_TabPage.TabIndex = 0;
            this.g02_01_TabPage.Text = "Unit01";
            this.g02_01_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_01_Panel
            // 
            this.g02_01_Panel.Controls.Add(this.g02_01_Export_GroupBox);
            this.g02_01_Panel.Controls.Add(this.g02_01_FFU_GroupBox);
            this.g02_01_Panel.Controls.Add(this.g02_01_EDC_GroupBox);
            this.g02_01_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_01_Panel.Name = "g02_01_Panel";
            this.g02_01_Panel.Size = new System.Drawing.Size(1309, 659);
            this.g02_01_Panel.TabIndex = 0;
            // 
            // g02_01_Export_GroupBox
            // 
            this.g02_01_Export_GroupBox.Controls.Add(this.g02_01_Folder_Label);
            this.g02_01_Export_GroupBox.Controls.Add(this.g02_01_selectFolder_Button);
            this.g02_01_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g02_01_Export_GroupBox.Name = "g02_01_Export_GroupBox";
            this.g02_01_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g02_01_Export_GroupBox.TabIndex = 28;
            this.g02_01_Export_GroupBox.TabStop = false;
            this.g02_01_Export_GroupBox.Text = "匯出路徑";
            // 
            // g02_01_Folder_Label
            // 
            this.g02_01_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_01_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g02_01_Folder_Label.Name = "g02_01_Folder_Label";
            this.g02_01_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g02_01_Folder_Label.TabIndex = 8;
            this.g02_01_Folder_Label.Text = "請選擇匯出路徑";
            this.g02_01_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g02_01_selectFolder_Button
            // 
            this.g02_01_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g02_01_selectFolder_Button.Name = "g02_01_selectFolder_Button";
            this.g02_01_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g02_01_selectFolder_Button.TabIndex = 7;
            this.g02_01_selectFolder_Button.Text = "選擇匯出路徑";
            this.g02_01_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g02_01_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g02_01_FFU_GroupBox
            // 
            this.g02_01_FFU_GroupBox.Controls.Add(this.g02_01_IsSensor_CheckBox);
            this.g02_01_FFU_GroupBox.Controls.Add(this.label103);
            this.g02_01_FFU_GroupBox.Controls.Add(this.g02_01_Particle_ID_TextBox);
            this.g02_01_FFU_GroupBox.Controls.Add(this.g02_01_Particle_Quanity_TextBox);
            this.g02_01_FFU_GroupBox.Controls.Add(this.label104);
            this.g02_01_FFU_GroupBox.Controls.Add(this.g02_01_DPS_ID_TextBox);
            this.g02_01_FFU_GroupBox.Controls.Add(this.g02_01_DPS_Quanity_TextBox);
            this.g02_01_FFU_GroupBox.Controls.Add(this.label105);
            this.g02_01_FFU_GroupBox.Controls.Add(this.g02_01_FFU_Quanity_TextBox);
            this.g02_01_FFU_GroupBox.Controls.Add(this.panel7);
            this.g02_01_FFU_GroupBox.Controls.Add(this.label109);
            this.g02_01_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g02_01_FFU_GroupBox.Name = "g02_01_FFU_GroupBox";
            this.g02_01_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g02_01_FFU_GroupBox.TabIndex = 21;
            this.g02_01_FFU_GroupBox.TabStop = false;
            this.g02_01_FFU_GroupBox.Text = "FFU";
            // 
            // g02_01_IsSensor_CheckBox
            // 
            this.g02_01_IsSensor_CheckBox.AutoSize = true;
            this.g02_01_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_01_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g02_01_IsSensor_CheckBox.Name = "g02_01_IsSensor_CheckBox";
            this.g02_01_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g02_01_IsSensor_CheckBox.TabIndex = 32;
            this.g02_01_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g02_01_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label103.Location = new System.Drawing.Point(212, 129);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(28, 31);
            this.label103.TabIndex = 25;
            this.label103.Text = "1";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_Particle_ID_TextBox
            // 
            this.g02_01_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g02_01_Particle_ID_TextBox.Name = "g02_01_Particle_ID_TextBox";
            this.g02_01_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_01_Particle_ID_TextBox.TabIndex = 24;
            this.g02_01_Particle_ID_TextBox.Text = "65";
            this.g02_01_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_01_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g02_01_Particle_Quanity_TextBox
            // 
            this.g02_01_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g02_01_Particle_Quanity_TextBox.Name = "g02_01_Particle_Quanity_TextBox";
            this.g02_01_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_01_Particle_Quanity_TextBox.TabIndex = 23;
            this.g02_01_Particle_Quanity_TextBox.Text = "1";
            this.g02_01_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label104.Location = new System.Drawing.Point(28, 273);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(100, 31);
            this.label104.TabIndex = 22;
            this.label104.Text = "Particle";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_DPS_ID_TextBox
            // 
            this.g02_01_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g02_01_DPS_ID_TextBox.Name = "g02_01_DPS_ID_TextBox";
            this.g02_01_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_01_DPS_ID_TextBox.TabIndex = 21;
            this.g02_01_DPS_ID_TextBox.Text = "64";
            this.g02_01_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_01_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g02_01_DPS_Quanity_TextBox
            // 
            this.g02_01_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g02_01_DPS_Quanity_TextBox.Name = "g02_01_DPS_Quanity_TextBox";
            this.g02_01_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_01_DPS_Quanity_TextBox.TabIndex = 20;
            this.g02_01_DPS_Quanity_TextBox.Text = "1";
            this.g02_01_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label105.Location = new System.Drawing.Point(47, 202);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(62, 31);
            this.label105.TabIndex = 19;
            this.label105.Text = "DPS";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_FFU_Quanity_TextBox
            // 
            this.g02_01_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g02_01_FFU_Quanity_TextBox.Name = "g02_01_FFU_Quanity_TextBox";
            this.g02_01_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_01_FFU_Quanity_TextBox.TabIndex = 17;
            this.g02_01_FFU_Quanity_TextBox.Text = "1";
            this.g02_01_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label106);
            this.panel7.Controls.Add(this.label107);
            this.panel7.Controls.Add(this.label108);
            this.panel7.Location = new System.Drawing.Point(25, 33);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(445, 69);
            this.panel7.TabIndex = 4;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label106.Location = new System.Drawing.Point(21, 16);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(62, 31);
            this.label106.TabIndex = 0;
            this.label106.Text = "格式";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label107.Location = new System.Drawing.Point(148, 16);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(110, 31);
            this.label107.TabIndex = 1;
            this.label107.Text = "起始站號";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label108.Location = new System.Drawing.Point(316, 16);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(110, 31);
            this.label108.TabIndex = 2;
            this.label108.Text = "輸出數量";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label109.Location = new System.Drawing.Point(40, 133);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(88, 31);
            this.label109.TabIndex = 3;
            this.label109.Text = "SPEED";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_EDC_GroupBox
            // 
            this.g02_01_EDC_GroupBox.Controls.Add(this.label110);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox9);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label111);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox8);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label112);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox7);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label113);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox6);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label114);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox5);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label115);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox4);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label116);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox3);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label117);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox2);
            this.g02_01_EDC_GroupBox.Controls.Add(this.label118);
            this.g02_01_EDC_GroupBox.Controls.Add(this.g02_01_TextBox1);
            this.g02_01_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g02_01_EDC_GroupBox.Name = "g02_01_EDC_GroupBox";
            this.g02_01_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g02_01_EDC_GroupBox.TabIndex = 20;
            this.g02_01_EDC_GroupBox.TabStop = false;
            this.g02_01_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label110.Location = new System.Drawing.Point(51, 71);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(118, 31);
            this.label110.TabIndex = 2;
            this.label110.Text = "glass_id :";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox9
            // 
            this.g02_01_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g02_01_TextBox9.Name = "g02_01_TextBox9";
            this.g02_01_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox9.TabIndex = 19;
            this.g02_01_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label111.Location = new System.Drawing.Point(17, 161);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(152, 31);
            this.label111.TabIndex = 3;
            this.label111.Text = "product_id :";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox8
            // 
            this.g02_01_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g02_01_TextBox8.Name = "g02_01_TextBox8";
            this.g02_01_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox8.TabIndex = 18;
            this.g02_01_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label112.Location = new System.Drawing.Point(63, 251);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(106, 31);
            this.label112.TabIndex = 4;
            this.label112.Text = "eqp_id :";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox7
            // 
            this.g02_01_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g02_01_TextBox7.Name = "g02_01_TextBox7";
            this.g02_01_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox7.TabIndex = 17;
            this.g02_01_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label113.Location = new System.Drawing.Point(10, 341);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(159, 31);
            this.label113.TabIndex = 5;
            this.label113.Text = "sub_eqp_id :";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox6
            // 
            this.g02_01_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g02_01_TextBox6.Name = "g02_01_TextBox6";
            this.g02_01_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox6.TabIndex = 16;
            this.g02_01_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label114.Location = new System.Drawing.Point(71, 431);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(98, 31);
            this.label114.TabIndex = 6;
            this.label114.Text = "owner :";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox5
            // 
            this.g02_01_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g02_01_TextBox5.Name = "g02_01_TextBox5";
            this.g02_01_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox5.TabIndex = 15;
            this.g02_01_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label115.Location = new System.Drawing.Point(409, 71);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(132, 31);
            this.label115.TabIndex = 7;
            this.label115.Text = "recipe_id :";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox4
            // 
            this.g02_01_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g02_01_TextBox4.Name = "g02_01_TextBox4";
            this.g02_01_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox4.TabIndex = 14;
            this.g02_01_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label116.Location = new System.Drawing.Point(402, 161);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(139, 31);
            this.label116.TabIndex = 8;
            this.label116.Text = "operation :";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox3
            // 
            this.g02_01_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g02_01_TextBox3.Name = "g02_01_TextBox3";
            this.g02_01_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox3.TabIndex = 13;
            this.g02_01_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label117.Location = new System.Drawing.Point(413, 251);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(128, 31);
            this.label117.TabIndex = 9;
            this.label117.Text = "chamber :";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox2
            // 
            this.g02_01_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g02_01_TextBox2.Name = "g02_01_TextBox2";
            this.g02_01_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox2.TabIndex = 12;
            this.g02_01_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label118.Location = new System.Drawing.Point(415, 341);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(126, 31);
            this.label118.TabIndex = 10;
            this.label118.Text = "operator :";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_01_TextBox1
            // 
            this.g02_01_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g02_01_TextBox1.Name = "g02_01_TextBox1";
            this.g02_01_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g02_01_TextBox1.TabIndex = 11;
            this.g02_01_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g02_02_TabPage
            // 
            this.g02_02_TabPage.Controls.Add(this.g02_02_Panel);
            this.g02_02_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g02_02_TabPage.Name = "g02_02_TabPage";
            this.g02_02_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_02_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g02_02_TabPage.TabIndex = 1;
            this.g02_02_TabPage.Text = "Unit02";
            this.g02_02_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_02_Panel
            // 
            this.g02_02_Panel.Controls.Add(this.g02_02_Export_GroupBox);
            this.g02_02_Panel.Controls.Add(this.g02_02_FFU_GroupBox);
            this.g02_02_Panel.Controls.Add(this.g02_02_EDC_GroupBox);
            this.g02_02_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_02_Panel.Name = "g02_02_Panel";
            this.g02_02_Panel.Size = new System.Drawing.Size(1309, 659);
            this.g02_02_Panel.TabIndex = 1;
            // 
            // g02_02_Export_GroupBox
            // 
            this.g02_02_Export_GroupBox.Controls.Add(this.g02_02_Folder_Label);
            this.g02_02_Export_GroupBox.Controls.Add(this.g02_02_selectFolder_Button);
            this.g02_02_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g02_02_Export_GroupBox.Name = "g02_02_Export_GroupBox";
            this.g02_02_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g02_02_Export_GroupBox.TabIndex = 27;
            this.g02_02_Export_GroupBox.TabStop = false;
            this.g02_02_Export_GroupBox.Text = "匯出路徑";
            // 
            // g02_02_Folder_Label
            // 
            this.g02_02_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_02_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g02_02_Folder_Label.Name = "g02_02_Folder_Label";
            this.g02_02_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g02_02_Folder_Label.TabIndex = 8;
            this.g02_02_Folder_Label.Text = "請選擇匯出路徑";
            this.g02_02_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g02_02_selectFolder_Button
            // 
            this.g02_02_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g02_02_selectFolder_Button.Name = "g02_02_selectFolder_Button";
            this.g02_02_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g02_02_selectFolder_Button.TabIndex = 7;
            this.g02_02_selectFolder_Button.Text = "選擇匯出路徑";
            this.g02_02_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g02_02_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g02_02_FFU_GroupBox
            // 
            this.g02_02_FFU_GroupBox.Controls.Add(this.g02_02_IsSensor_CheckBox);
            this.g02_02_FFU_GroupBox.Controls.Add(this.label119);
            this.g02_02_FFU_GroupBox.Controls.Add(this.g02_02_Particle_ID_TextBox);
            this.g02_02_FFU_GroupBox.Controls.Add(this.g02_02_Particle_Quanity_TextBox);
            this.g02_02_FFU_GroupBox.Controls.Add(this.label120);
            this.g02_02_FFU_GroupBox.Controls.Add(this.g02_02_DPS_ID_TextBox);
            this.g02_02_FFU_GroupBox.Controls.Add(this.g02_02_DPS_Quanity_TextBox);
            this.g02_02_FFU_GroupBox.Controls.Add(this.label121);
            this.g02_02_FFU_GroupBox.Controls.Add(this.g02_02_FFU_Quanity_TextBox);
            this.g02_02_FFU_GroupBox.Controls.Add(this.panel11);
            this.g02_02_FFU_GroupBox.Controls.Add(this.label125);
            this.g02_02_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g02_02_FFU_GroupBox.Name = "g02_02_FFU_GroupBox";
            this.g02_02_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g02_02_FFU_GroupBox.TabIndex = 21;
            this.g02_02_FFU_GroupBox.TabStop = false;
            this.g02_02_FFU_GroupBox.Text = "FFU";
            // 
            // g02_02_IsSensor_CheckBox
            // 
            this.g02_02_IsSensor_CheckBox.AutoSize = true;
            this.g02_02_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_02_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g02_02_IsSensor_CheckBox.Name = "g02_02_IsSensor_CheckBox";
            this.g02_02_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g02_02_IsSensor_CheckBox.TabIndex = 33;
            this.g02_02_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g02_02_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label119.Location = new System.Drawing.Point(212, 129);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(28, 31);
            this.label119.TabIndex = 25;
            this.label119.Text = "1";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_Particle_ID_TextBox
            // 
            this.g02_02_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g02_02_Particle_ID_TextBox.Name = "g02_02_Particle_ID_TextBox";
            this.g02_02_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_02_Particle_ID_TextBox.TabIndex = 24;
            this.g02_02_Particle_ID_TextBox.Text = "65";
            this.g02_02_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_02_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g02_02_Particle_Quanity_TextBox
            // 
            this.g02_02_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g02_02_Particle_Quanity_TextBox.Name = "g02_02_Particle_Quanity_TextBox";
            this.g02_02_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_02_Particle_Quanity_TextBox.TabIndex = 23;
            this.g02_02_Particle_Quanity_TextBox.Text = "1";
            this.g02_02_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label120.Location = new System.Drawing.Point(28, 273);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(100, 31);
            this.label120.TabIndex = 22;
            this.label120.Text = "Particle";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_DPS_ID_TextBox
            // 
            this.g02_02_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g02_02_DPS_ID_TextBox.Name = "g02_02_DPS_ID_TextBox";
            this.g02_02_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_02_DPS_ID_TextBox.TabIndex = 21;
            this.g02_02_DPS_ID_TextBox.Text = "64";
            this.g02_02_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_02_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g02_02_DPS_Quanity_TextBox
            // 
            this.g02_02_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g02_02_DPS_Quanity_TextBox.Name = "g02_02_DPS_Quanity_TextBox";
            this.g02_02_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_02_DPS_Quanity_TextBox.TabIndex = 20;
            this.g02_02_DPS_Quanity_TextBox.Text = "1";
            this.g02_02_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label121.Location = new System.Drawing.Point(47, 202);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(62, 31);
            this.label121.TabIndex = 19;
            this.label121.Text = "DPS";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_FFU_Quanity_TextBox
            // 
            this.g02_02_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g02_02_FFU_Quanity_TextBox.Name = "g02_02_FFU_Quanity_TextBox";
            this.g02_02_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_02_FFU_Quanity_TextBox.TabIndex = 17;
            this.g02_02_FFU_Quanity_TextBox.Text = "1";
            this.g02_02_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label122);
            this.panel11.Controls.Add(this.label123);
            this.panel11.Controls.Add(this.label124);
            this.panel11.Location = new System.Drawing.Point(25, 33);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(445, 69);
            this.panel11.TabIndex = 4;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label122.Location = new System.Drawing.Point(21, 16);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(62, 31);
            this.label122.TabIndex = 0;
            this.label122.Text = "格式";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label123.Location = new System.Drawing.Point(148, 16);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(110, 31);
            this.label123.TabIndex = 1;
            this.label123.Text = "起始站號";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label124.Location = new System.Drawing.Point(316, 16);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(110, 31);
            this.label124.TabIndex = 2;
            this.label124.Text = "輸出數量";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label125.Location = new System.Drawing.Point(40, 133);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(88, 31);
            this.label125.TabIndex = 3;
            this.label125.Text = "SPEED";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_EDC_GroupBox
            // 
            this.g02_02_EDC_GroupBox.Controls.Add(this.label126);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox9);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label127);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox8);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label128);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox7);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label129);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox6);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label130);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox5);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label131);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox4);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label132);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox3);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label133);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox2);
            this.g02_02_EDC_GroupBox.Controls.Add(this.label134);
            this.g02_02_EDC_GroupBox.Controls.Add(this.g02_02_TextBox1);
            this.g02_02_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g02_02_EDC_GroupBox.Name = "g02_02_EDC_GroupBox";
            this.g02_02_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g02_02_EDC_GroupBox.TabIndex = 20;
            this.g02_02_EDC_GroupBox.TabStop = false;
            this.g02_02_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label126.Location = new System.Drawing.Point(51, 71);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(118, 31);
            this.label126.TabIndex = 2;
            this.label126.Text = "glass_id :";
            this.label126.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox9
            // 
            this.g02_02_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g02_02_TextBox9.Name = "g02_02_TextBox9";
            this.g02_02_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox9.TabIndex = 19;
            this.g02_02_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label127.Location = new System.Drawing.Point(17, 161);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(152, 31);
            this.label127.TabIndex = 3;
            this.label127.Text = "product_id :";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox8
            // 
            this.g02_02_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g02_02_TextBox8.Name = "g02_02_TextBox8";
            this.g02_02_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox8.TabIndex = 18;
            this.g02_02_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label128.Location = new System.Drawing.Point(63, 251);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(106, 31);
            this.label128.TabIndex = 4;
            this.label128.Text = "eqp_id :";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox7
            // 
            this.g02_02_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g02_02_TextBox7.Name = "g02_02_TextBox7";
            this.g02_02_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox7.TabIndex = 17;
            this.g02_02_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label129.Location = new System.Drawing.Point(10, 341);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(159, 31);
            this.label129.TabIndex = 5;
            this.label129.Text = "sub_eqp_id :";
            this.label129.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox6
            // 
            this.g02_02_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g02_02_TextBox6.Name = "g02_02_TextBox6";
            this.g02_02_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox6.TabIndex = 16;
            this.g02_02_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label130.Location = new System.Drawing.Point(71, 431);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(98, 31);
            this.label130.TabIndex = 6;
            this.label130.Text = "owner :";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox5
            // 
            this.g02_02_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g02_02_TextBox5.Name = "g02_02_TextBox5";
            this.g02_02_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox5.TabIndex = 15;
            this.g02_02_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label131.Location = new System.Drawing.Point(409, 71);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(132, 31);
            this.label131.TabIndex = 7;
            this.label131.Text = "recipe_id :";
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox4
            // 
            this.g02_02_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g02_02_TextBox4.Name = "g02_02_TextBox4";
            this.g02_02_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox4.TabIndex = 14;
            this.g02_02_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label132.Location = new System.Drawing.Point(402, 161);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(139, 31);
            this.label132.TabIndex = 8;
            this.label132.Text = "operation :";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox3
            // 
            this.g02_02_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g02_02_TextBox3.Name = "g02_02_TextBox3";
            this.g02_02_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox3.TabIndex = 13;
            this.g02_02_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label133.Location = new System.Drawing.Point(413, 251);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(128, 31);
            this.label133.TabIndex = 9;
            this.label133.Text = "chamber :";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox2
            // 
            this.g02_02_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g02_02_TextBox2.Name = "g02_02_TextBox2";
            this.g02_02_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox2.TabIndex = 12;
            this.g02_02_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label134.Location = new System.Drawing.Point(415, 341);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(126, 31);
            this.label134.TabIndex = 10;
            this.label134.Text = "operator :";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_02_TextBox1
            // 
            this.g02_02_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g02_02_TextBox1.Name = "g02_02_TextBox1";
            this.g02_02_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g02_02_TextBox1.TabIndex = 11;
            this.g02_02_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g02_03_TabPage
            // 
            this.g02_03_TabPage.Controls.Add(this.g02_03_Panel);
            this.g02_03_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g02_03_TabPage.Name = "g02_03_TabPage";
            this.g02_03_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_03_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g02_03_TabPage.TabIndex = 2;
            this.g02_03_TabPage.Text = "Unit03";
            this.g02_03_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_03_Panel
            // 
            this.g02_03_Panel.Controls.Add(this.g02_03_Export_GroupBox);
            this.g02_03_Panel.Controls.Add(this.g02_03_FFU_GroupBox);
            this.g02_03_Panel.Controls.Add(this.g02_03_EDC_GroupBox);
            this.g02_03_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_03_Panel.Name = "g02_03_Panel";
            this.g02_03_Panel.Size = new System.Drawing.Size(1309, 659);
            this.g02_03_Panel.TabIndex = 2;
            // 
            // g02_03_Export_GroupBox
            // 
            this.g02_03_Export_GroupBox.Controls.Add(this.g02_03_Folder_Label);
            this.g02_03_Export_GroupBox.Controls.Add(this.g02_03_selectFolder_Button);
            this.g02_03_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g02_03_Export_GroupBox.Name = "g02_03_Export_GroupBox";
            this.g02_03_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g02_03_Export_GroupBox.TabIndex = 26;
            this.g02_03_Export_GroupBox.TabStop = false;
            this.g02_03_Export_GroupBox.Text = "匯出路徑";
            // 
            // g02_03_Folder_Label
            // 
            this.g02_03_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_03_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g02_03_Folder_Label.Name = "g02_03_Folder_Label";
            this.g02_03_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g02_03_Folder_Label.TabIndex = 8;
            this.g02_03_Folder_Label.Text = "請選擇匯出路徑";
            this.g02_03_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g02_03_selectFolder_Button
            // 
            this.g02_03_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g02_03_selectFolder_Button.Name = "g02_03_selectFolder_Button";
            this.g02_03_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g02_03_selectFolder_Button.TabIndex = 7;
            this.g02_03_selectFolder_Button.Text = "選擇匯出路徑";
            this.g02_03_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g02_03_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g02_03_FFU_GroupBox
            // 
            this.g02_03_FFU_GroupBox.Controls.Add(this.g02_03_IsSensor_CheckBox);
            this.g02_03_FFU_GroupBox.Controls.Add(this.label135);
            this.g02_03_FFU_GroupBox.Controls.Add(this.g02_03_Particle_ID_TextBox);
            this.g02_03_FFU_GroupBox.Controls.Add(this.g02_03_Particle_Quanity_TextBox);
            this.g02_03_FFU_GroupBox.Controls.Add(this.label136);
            this.g02_03_FFU_GroupBox.Controls.Add(this.g02_03_DPS_ID_TextBox);
            this.g02_03_FFU_GroupBox.Controls.Add(this.g02_03_DPS_Quanity_TextBox);
            this.g02_03_FFU_GroupBox.Controls.Add(this.label137);
            this.g02_03_FFU_GroupBox.Controls.Add(this.g02_03_FFU_Quanity_TextBox);
            this.g02_03_FFU_GroupBox.Controls.Add(this.panel13);
            this.g02_03_FFU_GroupBox.Controls.Add(this.label141);
            this.g02_03_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g02_03_FFU_GroupBox.Name = "g02_03_FFU_GroupBox";
            this.g02_03_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g02_03_FFU_GroupBox.TabIndex = 21;
            this.g02_03_FFU_GroupBox.TabStop = false;
            this.g02_03_FFU_GroupBox.Text = "FFU";
            // 
            // g02_03_IsSensor_CheckBox
            // 
            this.g02_03_IsSensor_CheckBox.AutoSize = true;
            this.g02_03_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_03_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g02_03_IsSensor_CheckBox.Name = "g02_03_IsSensor_CheckBox";
            this.g02_03_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g02_03_IsSensor_CheckBox.TabIndex = 34;
            this.g02_03_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g02_03_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label135.Location = new System.Drawing.Point(212, 129);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(28, 31);
            this.label135.TabIndex = 25;
            this.label135.Text = "1";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_Particle_ID_TextBox
            // 
            this.g02_03_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g02_03_Particle_ID_TextBox.Name = "g02_03_Particle_ID_TextBox";
            this.g02_03_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_03_Particle_ID_TextBox.TabIndex = 24;
            this.g02_03_Particle_ID_TextBox.Text = "65";
            this.g02_03_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_03_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g02_03_Particle_Quanity_TextBox
            // 
            this.g02_03_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g02_03_Particle_Quanity_TextBox.Name = "g02_03_Particle_Quanity_TextBox";
            this.g02_03_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_03_Particle_Quanity_TextBox.TabIndex = 23;
            this.g02_03_Particle_Quanity_TextBox.Text = "1";
            this.g02_03_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label136.Location = new System.Drawing.Point(28, 273);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(100, 31);
            this.label136.TabIndex = 22;
            this.label136.Text = "Particle";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_DPS_ID_TextBox
            // 
            this.g02_03_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g02_03_DPS_ID_TextBox.Name = "g02_03_DPS_ID_TextBox";
            this.g02_03_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_03_DPS_ID_TextBox.TabIndex = 21;
            this.g02_03_DPS_ID_TextBox.Text = "64";
            this.g02_03_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_03_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g02_03_DPS_Quanity_TextBox
            // 
            this.g02_03_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g02_03_DPS_Quanity_TextBox.Name = "g02_03_DPS_Quanity_TextBox";
            this.g02_03_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_03_DPS_Quanity_TextBox.TabIndex = 20;
            this.g02_03_DPS_Quanity_TextBox.Text = "1";
            this.g02_03_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label137.Location = new System.Drawing.Point(47, 202);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(62, 31);
            this.label137.TabIndex = 19;
            this.label137.Text = "DPS";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_FFU_Quanity_TextBox
            // 
            this.g02_03_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g02_03_FFU_Quanity_TextBox.Name = "g02_03_FFU_Quanity_TextBox";
            this.g02_03_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_03_FFU_Quanity_TextBox.TabIndex = 17;
            this.g02_03_FFU_Quanity_TextBox.Text = "1";
            this.g02_03_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label138);
            this.panel13.Controls.Add(this.label139);
            this.panel13.Controls.Add(this.label140);
            this.panel13.Location = new System.Drawing.Point(25, 33);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(445, 69);
            this.panel13.TabIndex = 4;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label138.Location = new System.Drawing.Point(21, 16);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(62, 31);
            this.label138.TabIndex = 0;
            this.label138.Text = "格式";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label139.Location = new System.Drawing.Point(148, 16);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(110, 31);
            this.label139.TabIndex = 1;
            this.label139.Text = "起始站號";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label140.Location = new System.Drawing.Point(316, 16);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(110, 31);
            this.label140.TabIndex = 2;
            this.label140.Text = "輸出數量";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label141.Location = new System.Drawing.Point(40, 133);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(88, 31);
            this.label141.TabIndex = 3;
            this.label141.Text = "SPEED";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_EDC_GroupBox
            // 
            this.g02_03_EDC_GroupBox.Controls.Add(this.label142);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox9);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label143);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox8);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label144);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox7);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label145);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox6);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label146);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox5);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label147);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox4);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label148);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox3);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label149);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox2);
            this.g02_03_EDC_GroupBox.Controls.Add(this.label150);
            this.g02_03_EDC_GroupBox.Controls.Add(this.g02_03_TextBox1);
            this.g02_03_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g02_03_EDC_GroupBox.Name = "g02_03_EDC_GroupBox";
            this.g02_03_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g02_03_EDC_GroupBox.TabIndex = 20;
            this.g02_03_EDC_GroupBox.TabStop = false;
            this.g02_03_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label142.Location = new System.Drawing.Point(51, 71);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(118, 31);
            this.label142.TabIndex = 2;
            this.label142.Text = "glass_id :";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox9
            // 
            this.g02_03_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g02_03_TextBox9.Name = "g02_03_TextBox9";
            this.g02_03_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox9.TabIndex = 19;
            this.g02_03_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label143.Location = new System.Drawing.Point(17, 161);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(152, 31);
            this.label143.TabIndex = 3;
            this.label143.Text = "product_id :";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox8
            // 
            this.g02_03_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g02_03_TextBox8.Name = "g02_03_TextBox8";
            this.g02_03_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox8.TabIndex = 18;
            this.g02_03_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label144.Location = new System.Drawing.Point(63, 251);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(106, 31);
            this.label144.TabIndex = 4;
            this.label144.Text = "eqp_id :";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox7
            // 
            this.g02_03_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g02_03_TextBox7.Name = "g02_03_TextBox7";
            this.g02_03_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox7.TabIndex = 17;
            this.g02_03_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label145.Location = new System.Drawing.Point(10, 341);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(159, 31);
            this.label145.TabIndex = 5;
            this.label145.Text = "sub_eqp_id :";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox6
            // 
            this.g02_03_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g02_03_TextBox6.Name = "g02_03_TextBox6";
            this.g02_03_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox6.TabIndex = 16;
            this.g02_03_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label146.Location = new System.Drawing.Point(71, 431);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(98, 31);
            this.label146.TabIndex = 6;
            this.label146.Text = "owner :";
            this.label146.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox5
            // 
            this.g02_03_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g02_03_TextBox5.Name = "g02_03_TextBox5";
            this.g02_03_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox5.TabIndex = 15;
            this.g02_03_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label147.Location = new System.Drawing.Point(409, 71);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(132, 31);
            this.label147.TabIndex = 7;
            this.label147.Text = "recipe_id :";
            this.label147.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox4
            // 
            this.g02_03_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g02_03_TextBox4.Name = "g02_03_TextBox4";
            this.g02_03_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox4.TabIndex = 14;
            this.g02_03_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label148.Location = new System.Drawing.Point(402, 161);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(139, 31);
            this.label148.TabIndex = 8;
            this.label148.Text = "operation :";
            this.label148.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox3
            // 
            this.g02_03_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g02_03_TextBox3.Name = "g02_03_TextBox3";
            this.g02_03_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox3.TabIndex = 13;
            this.g02_03_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label149.Location = new System.Drawing.Point(413, 251);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(128, 31);
            this.label149.TabIndex = 9;
            this.label149.Text = "chamber :";
            this.label149.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox2
            // 
            this.g02_03_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g02_03_TextBox2.Name = "g02_03_TextBox2";
            this.g02_03_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox2.TabIndex = 12;
            this.g02_03_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label150.Location = new System.Drawing.Point(415, 341);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(126, 31);
            this.label150.TabIndex = 10;
            this.label150.Text = "operator :";
            this.label150.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_03_TextBox1
            // 
            this.g02_03_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g02_03_TextBox1.Name = "g02_03_TextBox1";
            this.g02_03_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g02_03_TextBox1.TabIndex = 11;
            this.g02_03_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g02_04_TabPage
            // 
            this.g02_04_TabPage.Controls.Add(this.g02_04_Panel);
            this.g02_04_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g02_04_TabPage.Name = "g02_04_TabPage";
            this.g02_04_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_04_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g02_04_TabPage.TabIndex = 3;
            this.g02_04_TabPage.Text = "Unit04";
            this.g02_04_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_04_Panel
            // 
            this.g02_04_Panel.Controls.Add(this.g02_04_Export_GroupBox);
            this.g02_04_Panel.Controls.Add(this.g02_04_FFU_GroupBox);
            this.g02_04_Panel.Controls.Add(this.g02_04_EDC_GroupBox);
            this.g02_04_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_04_Panel.Name = "g02_04_Panel";
            this.g02_04_Panel.Size = new System.Drawing.Size(1309, 659);
            this.g02_04_Panel.TabIndex = 2;
            // 
            // g02_04_Export_GroupBox
            // 
            this.g02_04_Export_GroupBox.Controls.Add(this.g02_04_Folder_Label);
            this.g02_04_Export_GroupBox.Controls.Add(this.g02_04_selectFolder_Button);
            this.g02_04_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g02_04_Export_GroupBox.Name = "g02_04_Export_GroupBox";
            this.g02_04_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g02_04_Export_GroupBox.TabIndex = 25;
            this.g02_04_Export_GroupBox.TabStop = false;
            this.g02_04_Export_GroupBox.Text = "匯出路徑";
            // 
            // g02_04_Folder_Label
            // 
            this.g02_04_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_04_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g02_04_Folder_Label.Name = "g02_04_Folder_Label";
            this.g02_04_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g02_04_Folder_Label.TabIndex = 8;
            this.g02_04_Folder_Label.Text = "請選擇匯出路徑";
            this.g02_04_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g02_04_selectFolder_Button
            // 
            this.g02_04_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g02_04_selectFolder_Button.Name = "g02_04_selectFolder_Button";
            this.g02_04_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g02_04_selectFolder_Button.TabIndex = 7;
            this.g02_04_selectFolder_Button.Text = "選擇匯出路徑";
            this.g02_04_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g02_04_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g02_04_FFU_GroupBox
            // 
            this.g02_04_FFU_GroupBox.Controls.Add(this.g02_04_IsSensor_CheckBox);
            this.g02_04_FFU_GroupBox.Controls.Add(this.label151);
            this.g02_04_FFU_GroupBox.Controls.Add(this.g02_04_Particle_ID_TextBox);
            this.g02_04_FFU_GroupBox.Controls.Add(this.g02_04_Particle_Quanity_TextBox);
            this.g02_04_FFU_GroupBox.Controls.Add(this.label152);
            this.g02_04_FFU_GroupBox.Controls.Add(this.g02_04_DPS_ID_TextBox);
            this.g02_04_FFU_GroupBox.Controls.Add(this.g02_04_DPS_Quanity_TextBox);
            this.g02_04_FFU_GroupBox.Controls.Add(this.label153);
            this.g02_04_FFU_GroupBox.Controls.Add(this.g02_04_FFU_Quanity_TextBox);
            this.g02_04_FFU_GroupBox.Controls.Add(this.panel15);
            this.g02_04_FFU_GroupBox.Controls.Add(this.label157);
            this.g02_04_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g02_04_FFU_GroupBox.Name = "g02_04_FFU_GroupBox";
            this.g02_04_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g02_04_FFU_GroupBox.TabIndex = 21;
            this.g02_04_FFU_GroupBox.TabStop = false;
            this.g02_04_FFU_GroupBox.Text = "FFU";
            // 
            // g02_04_IsSensor_CheckBox
            // 
            this.g02_04_IsSensor_CheckBox.AutoSize = true;
            this.g02_04_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_04_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g02_04_IsSensor_CheckBox.Name = "g02_04_IsSensor_CheckBox";
            this.g02_04_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g02_04_IsSensor_CheckBox.TabIndex = 35;
            this.g02_04_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g02_04_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label151.Location = new System.Drawing.Point(212, 129);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(28, 31);
            this.label151.TabIndex = 25;
            this.label151.Text = "1";
            this.label151.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_Particle_ID_TextBox
            // 
            this.g02_04_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g02_04_Particle_ID_TextBox.Name = "g02_04_Particle_ID_TextBox";
            this.g02_04_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_04_Particle_ID_TextBox.TabIndex = 24;
            this.g02_04_Particle_ID_TextBox.Text = "65";
            this.g02_04_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_04_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g02_04_Particle_Quanity_TextBox
            // 
            this.g02_04_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g02_04_Particle_Quanity_TextBox.Name = "g02_04_Particle_Quanity_TextBox";
            this.g02_04_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_04_Particle_Quanity_TextBox.TabIndex = 23;
            this.g02_04_Particle_Quanity_TextBox.Text = "1";
            this.g02_04_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label152.Location = new System.Drawing.Point(28, 273);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(100, 31);
            this.label152.TabIndex = 22;
            this.label152.Text = "Particle";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_DPS_ID_TextBox
            // 
            this.g02_04_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g02_04_DPS_ID_TextBox.Name = "g02_04_DPS_ID_TextBox";
            this.g02_04_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_04_DPS_ID_TextBox.TabIndex = 21;
            this.g02_04_DPS_ID_TextBox.Text = "64";
            this.g02_04_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_04_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g02_04_DPS_Quanity_TextBox
            // 
            this.g02_04_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g02_04_DPS_Quanity_TextBox.Name = "g02_04_DPS_Quanity_TextBox";
            this.g02_04_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_04_DPS_Quanity_TextBox.TabIndex = 20;
            this.g02_04_DPS_Quanity_TextBox.Text = "1";
            this.g02_04_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label153.Location = new System.Drawing.Point(47, 202);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(62, 31);
            this.label153.TabIndex = 19;
            this.label153.Text = "DPS";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_FFU_Quanity_TextBox
            // 
            this.g02_04_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g02_04_FFU_Quanity_TextBox.Name = "g02_04_FFU_Quanity_TextBox";
            this.g02_04_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_04_FFU_Quanity_TextBox.TabIndex = 17;
            this.g02_04_FFU_Quanity_TextBox.Text = "1";
            this.g02_04_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.label154);
            this.panel15.Controls.Add(this.label155);
            this.panel15.Controls.Add(this.label156);
            this.panel15.Location = new System.Drawing.Point(25, 33);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(445, 69);
            this.panel15.TabIndex = 4;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label154.Location = new System.Drawing.Point(21, 16);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(62, 31);
            this.label154.TabIndex = 0;
            this.label154.Text = "格式";
            this.label154.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label155.Location = new System.Drawing.Point(148, 16);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(110, 31);
            this.label155.TabIndex = 1;
            this.label155.Text = "起始站號";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label156.Location = new System.Drawing.Point(316, 16);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(110, 31);
            this.label156.TabIndex = 2;
            this.label156.Text = "輸出數量";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label157.Location = new System.Drawing.Point(40, 133);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(88, 31);
            this.label157.TabIndex = 3;
            this.label157.Text = "SPEED";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_EDC_GroupBox
            // 
            this.g02_04_EDC_GroupBox.Controls.Add(this.label158);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox9);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label159);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox8);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label160);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox7);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label161);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox6);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label162);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox5);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label163);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox4);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label164);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox3);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label165);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox2);
            this.g02_04_EDC_GroupBox.Controls.Add(this.label166);
            this.g02_04_EDC_GroupBox.Controls.Add(this.g02_04_TextBox1);
            this.g02_04_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g02_04_EDC_GroupBox.Name = "g02_04_EDC_GroupBox";
            this.g02_04_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g02_04_EDC_GroupBox.TabIndex = 20;
            this.g02_04_EDC_GroupBox.TabStop = false;
            this.g02_04_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label158.Location = new System.Drawing.Point(51, 71);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(118, 31);
            this.label158.TabIndex = 2;
            this.label158.Text = "glass_id :";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox9
            // 
            this.g02_04_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g02_04_TextBox9.Name = "g02_04_TextBox9";
            this.g02_04_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox9.TabIndex = 19;
            this.g02_04_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label159.Location = new System.Drawing.Point(17, 161);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(152, 31);
            this.label159.TabIndex = 3;
            this.label159.Text = "product_id :";
            this.label159.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox8
            // 
            this.g02_04_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g02_04_TextBox8.Name = "g02_04_TextBox8";
            this.g02_04_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox8.TabIndex = 18;
            this.g02_04_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label160.Location = new System.Drawing.Point(63, 251);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(106, 31);
            this.label160.TabIndex = 4;
            this.label160.Text = "eqp_id :";
            this.label160.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox7
            // 
            this.g02_04_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g02_04_TextBox7.Name = "g02_04_TextBox7";
            this.g02_04_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox7.TabIndex = 17;
            this.g02_04_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label161.Location = new System.Drawing.Point(10, 341);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(159, 31);
            this.label161.TabIndex = 5;
            this.label161.Text = "sub_eqp_id :";
            this.label161.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox6
            // 
            this.g02_04_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g02_04_TextBox6.Name = "g02_04_TextBox6";
            this.g02_04_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox6.TabIndex = 16;
            this.g02_04_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label162.Location = new System.Drawing.Point(71, 431);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(98, 31);
            this.label162.TabIndex = 6;
            this.label162.Text = "owner :";
            this.label162.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox5
            // 
            this.g02_04_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g02_04_TextBox5.Name = "g02_04_TextBox5";
            this.g02_04_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox5.TabIndex = 15;
            this.g02_04_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label163.Location = new System.Drawing.Point(409, 71);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(132, 31);
            this.label163.TabIndex = 7;
            this.label163.Text = "recipe_id :";
            this.label163.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox4
            // 
            this.g02_04_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g02_04_TextBox4.Name = "g02_04_TextBox4";
            this.g02_04_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox4.TabIndex = 14;
            this.g02_04_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label164.Location = new System.Drawing.Point(402, 161);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(139, 31);
            this.label164.TabIndex = 8;
            this.label164.Text = "operation :";
            this.label164.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox3
            // 
            this.g02_04_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g02_04_TextBox3.Name = "g02_04_TextBox3";
            this.g02_04_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox3.TabIndex = 13;
            this.g02_04_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label165.Location = new System.Drawing.Point(413, 251);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(128, 31);
            this.label165.TabIndex = 9;
            this.label165.Text = "chamber :";
            this.label165.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox2
            // 
            this.g02_04_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g02_04_TextBox2.Name = "g02_04_TextBox2";
            this.g02_04_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox2.TabIndex = 12;
            this.g02_04_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label166.Location = new System.Drawing.Point(415, 341);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(126, 31);
            this.label166.TabIndex = 10;
            this.label166.Text = "operator :";
            this.label166.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_04_TextBox1
            // 
            this.g02_04_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g02_04_TextBox1.Name = "g02_04_TextBox1";
            this.g02_04_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g02_04_TextBox1.TabIndex = 11;
            this.g02_04_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g02_05_TabPage
            // 
            this.g02_05_TabPage.Controls.Add(this.g02_05_Panel);
            this.g02_05_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g02_05_TabPage.Name = "g02_05_TabPage";
            this.g02_05_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_05_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g02_05_TabPage.TabIndex = 4;
            this.g02_05_TabPage.Text = "Unit05";
            this.g02_05_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_05_Panel
            // 
            this.g02_05_Panel.Controls.Add(this.g02_05_Export_GroupBox);
            this.g02_05_Panel.Controls.Add(this.g02_05_FFU_GroupBox);
            this.g02_05_Panel.Controls.Add(this.g02_05_EDC_GroupBox);
            this.g02_05_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_05_Panel.Name = "g02_05_Panel";
            this.g02_05_Panel.Size = new System.Drawing.Size(1309, 659);
            this.g02_05_Panel.TabIndex = 2;
            // 
            // g02_05_Export_GroupBox
            // 
            this.g02_05_Export_GroupBox.Controls.Add(this.g02_05_Folder_Label);
            this.g02_05_Export_GroupBox.Controls.Add(this.g02_05_selectFolder_Button);
            this.g02_05_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g02_05_Export_GroupBox.Name = "g02_05_Export_GroupBox";
            this.g02_05_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g02_05_Export_GroupBox.TabIndex = 24;
            this.g02_05_Export_GroupBox.TabStop = false;
            this.g02_05_Export_GroupBox.Text = "匯出路徑";
            // 
            // g02_05_Folder_Label
            // 
            this.g02_05_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_05_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g02_05_Folder_Label.Name = "g02_05_Folder_Label";
            this.g02_05_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g02_05_Folder_Label.TabIndex = 8;
            this.g02_05_Folder_Label.Text = "請選擇匯出路徑";
            this.g02_05_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g02_05_selectFolder_Button
            // 
            this.g02_05_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g02_05_selectFolder_Button.Name = "g02_05_selectFolder_Button";
            this.g02_05_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g02_05_selectFolder_Button.TabIndex = 7;
            this.g02_05_selectFolder_Button.Text = "選擇匯出路徑";
            this.g02_05_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g02_05_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g02_05_FFU_GroupBox
            // 
            this.g02_05_FFU_GroupBox.Controls.Add(this.g02_05_IsSensor_CheckBox);
            this.g02_05_FFU_GroupBox.Controls.Add(this.label167);
            this.g02_05_FFU_GroupBox.Controls.Add(this.g02_05_Particle_ID_TextBox);
            this.g02_05_FFU_GroupBox.Controls.Add(this.g02_05_Particle_Quanity_TextBox);
            this.g02_05_FFU_GroupBox.Controls.Add(this.label168);
            this.g02_05_FFU_GroupBox.Controls.Add(this.g02_05_DPS_ID_TextBox);
            this.g02_05_FFU_GroupBox.Controls.Add(this.g02_05_DPS_Quanity_TextBox);
            this.g02_05_FFU_GroupBox.Controls.Add(this.label169);
            this.g02_05_FFU_GroupBox.Controls.Add(this.g02_05_FFU_Quanity_TextBox);
            this.g02_05_FFU_GroupBox.Controls.Add(this.panel17);
            this.g02_05_FFU_GroupBox.Controls.Add(this.label173);
            this.g02_05_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g02_05_FFU_GroupBox.Name = "g02_05_FFU_GroupBox";
            this.g02_05_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g02_05_FFU_GroupBox.TabIndex = 21;
            this.g02_05_FFU_GroupBox.TabStop = false;
            this.g02_05_FFU_GroupBox.Text = "FFU";
            // 
            // g02_05_IsSensor_CheckBox
            // 
            this.g02_05_IsSensor_CheckBox.AutoSize = true;
            this.g02_05_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_05_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g02_05_IsSensor_CheckBox.Name = "g02_05_IsSensor_CheckBox";
            this.g02_05_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g02_05_IsSensor_CheckBox.TabIndex = 36;
            this.g02_05_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g02_05_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label167.Location = new System.Drawing.Point(212, 129);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(28, 31);
            this.label167.TabIndex = 25;
            this.label167.Text = "1";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_Particle_ID_TextBox
            // 
            this.g02_05_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g02_05_Particle_ID_TextBox.Name = "g02_05_Particle_ID_TextBox";
            this.g02_05_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_05_Particle_ID_TextBox.TabIndex = 24;
            this.g02_05_Particle_ID_TextBox.Text = "65";
            this.g02_05_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_05_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g02_05_Particle_Quanity_TextBox
            // 
            this.g02_05_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g02_05_Particle_Quanity_TextBox.Name = "g02_05_Particle_Quanity_TextBox";
            this.g02_05_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_05_Particle_Quanity_TextBox.TabIndex = 23;
            this.g02_05_Particle_Quanity_TextBox.Text = "1";
            this.g02_05_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label168.Location = new System.Drawing.Point(28, 273);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(100, 31);
            this.label168.TabIndex = 22;
            this.label168.Text = "Particle";
            this.label168.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_DPS_ID_TextBox
            // 
            this.g02_05_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g02_05_DPS_ID_TextBox.Name = "g02_05_DPS_ID_TextBox";
            this.g02_05_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_05_DPS_ID_TextBox.TabIndex = 21;
            this.g02_05_DPS_ID_TextBox.Text = "64";
            this.g02_05_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_05_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g02_05_DPS_Quanity_TextBox
            // 
            this.g02_05_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g02_05_DPS_Quanity_TextBox.Name = "g02_05_DPS_Quanity_TextBox";
            this.g02_05_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_05_DPS_Quanity_TextBox.TabIndex = 20;
            this.g02_05_DPS_Quanity_TextBox.Text = "1";
            this.g02_05_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label169.Location = new System.Drawing.Point(47, 202);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(62, 31);
            this.label169.TabIndex = 19;
            this.label169.Text = "DPS";
            this.label169.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_FFU_Quanity_TextBox
            // 
            this.g02_05_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g02_05_FFU_Quanity_TextBox.Name = "g02_05_FFU_Quanity_TextBox";
            this.g02_05_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_05_FFU_Quanity_TextBox.TabIndex = 17;
            this.g02_05_FFU_Quanity_TextBox.Text = "1";
            this.g02_05_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.label170);
            this.panel17.Controls.Add(this.label171);
            this.panel17.Controls.Add(this.label172);
            this.panel17.Location = new System.Drawing.Point(25, 33);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(445, 69);
            this.panel17.TabIndex = 4;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label170.Location = new System.Drawing.Point(21, 16);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(62, 31);
            this.label170.TabIndex = 0;
            this.label170.Text = "格式";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label171.Location = new System.Drawing.Point(148, 16);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(110, 31);
            this.label171.TabIndex = 1;
            this.label171.Text = "起始站號";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label172.Location = new System.Drawing.Point(316, 16);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(110, 31);
            this.label172.TabIndex = 2;
            this.label172.Text = "輸出數量";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label173.Location = new System.Drawing.Point(40, 133);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(88, 31);
            this.label173.TabIndex = 3;
            this.label173.Text = "SPEED";
            this.label173.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_EDC_GroupBox
            // 
            this.g02_05_EDC_GroupBox.Controls.Add(this.label174);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox9);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label175);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox8);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label176);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox7);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label177);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox6);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label178);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox5);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label179);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox4);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label180);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox3);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label181);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox2);
            this.g02_05_EDC_GroupBox.Controls.Add(this.label182);
            this.g02_05_EDC_GroupBox.Controls.Add(this.g02_05_TextBox1);
            this.g02_05_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g02_05_EDC_GroupBox.Name = "g02_05_EDC_GroupBox";
            this.g02_05_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g02_05_EDC_GroupBox.TabIndex = 20;
            this.g02_05_EDC_GroupBox.TabStop = false;
            this.g02_05_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label174.Location = new System.Drawing.Point(51, 71);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(118, 31);
            this.label174.TabIndex = 2;
            this.label174.Text = "glass_id :";
            this.label174.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox9
            // 
            this.g02_05_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g02_05_TextBox9.Name = "g02_05_TextBox9";
            this.g02_05_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox9.TabIndex = 19;
            this.g02_05_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label175.Location = new System.Drawing.Point(17, 161);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(152, 31);
            this.label175.TabIndex = 3;
            this.label175.Text = "product_id :";
            this.label175.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox8
            // 
            this.g02_05_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g02_05_TextBox8.Name = "g02_05_TextBox8";
            this.g02_05_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox8.TabIndex = 18;
            this.g02_05_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label176.Location = new System.Drawing.Point(63, 251);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(106, 31);
            this.label176.TabIndex = 4;
            this.label176.Text = "eqp_id :";
            this.label176.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox7
            // 
            this.g02_05_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g02_05_TextBox7.Name = "g02_05_TextBox7";
            this.g02_05_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox7.TabIndex = 17;
            this.g02_05_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label177.Location = new System.Drawing.Point(10, 341);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(159, 31);
            this.label177.TabIndex = 5;
            this.label177.Text = "sub_eqp_id :";
            this.label177.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox6
            // 
            this.g02_05_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g02_05_TextBox6.Name = "g02_05_TextBox6";
            this.g02_05_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox6.TabIndex = 16;
            this.g02_05_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label178.Location = new System.Drawing.Point(71, 431);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(98, 31);
            this.label178.TabIndex = 6;
            this.label178.Text = "owner :";
            this.label178.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox5
            // 
            this.g02_05_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g02_05_TextBox5.Name = "g02_05_TextBox5";
            this.g02_05_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox5.TabIndex = 15;
            this.g02_05_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label179.Location = new System.Drawing.Point(409, 71);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(132, 31);
            this.label179.TabIndex = 7;
            this.label179.Text = "recipe_id :";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox4
            // 
            this.g02_05_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g02_05_TextBox4.Name = "g02_05_TextBox4";
            this.g02_05_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox4.TabIndex = 14;
            this.g02_05_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label180.Location = new System.Drawing.Point(402, 161);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(139, 31);
            this.label180.TabIndex = 8;
            this.label180.Text = "operation :";
            this.label180.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox3
            // 
            this.g02_05_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g02_05_TextBox3.Name = "g02_05_TextBox3";
            this.g02_05_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox3.TabIndex = 13;
            this.g02_05_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label181.Location = new System.Drawing.Point(413, 251);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(128, 31);
            this.label181.TabIndex = 9;
            this.label181.Text = "chamber :";
            this.label181.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox2
            // 
            this.g02_05_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g02_05_TextBox2.Name = "g02_05_TextBox2";
            this.g02_05_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox2.TabIndex = 12;
            this.g02_05_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label182.Location = new System.Drawing.Point(415, 341);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(126, 31);
            this.label182.TabIndex = 10;
            this.label182.Text = "operator :";
            this.label182.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_05_TextBox1
            // 
            this.g02_05_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g02_05_TextBox1.Name = "g02_05_TextBox1";
            this.g02_05_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g02_05_TextBox1.TabIndex = 11;
            this.g02_05_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g02_06_TabPage
            // 
            this.g02_06_TabPage.Controls.Add(this.g02_06_Panel);
            this.g02_06_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g02_06_TabPage.Name = "g02_06_TabPage";
            this.g02_06_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g02_06_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g02_06_TabPage.TabIndex = 5;
            this.g02_06_TabPage.Text = "Unit06";
            this.g02_06_TabPage.UseVisualStyleBackColor = true;
            // 
            // g02_06_Panel
            // 
            this.g02_06_Panel.Controls.Add(this.g02_06_Export_GroupBox);
            this.g02_06_Panel.Controls.Add(this.g02_06_FFU_GroupBox);
            this.g02_06_Panel.Controls.Add(this.g02_06_EDC_GroupBox);
            this.g02_06_Panel.Location = new System.Drawing.Point(6, 6);
            this.g02_06_Panel.Name = "g02_06_Panel";
            this.g02_06_Panel.Size = new System.Drawing.Size(1307, 659);
            this.g02_06_Panel.TabIndex = 2;
            // 
            // g02_06_Export_GroupBox
            // 
            this.g02_06_Export_GroupBox.Controls.Add(this.g02_06_Folder_Label);
            this.g02_06_Export_GroupBox.Controls.Add(this.g02_06_selectFolder_Button);
            this.g02_06_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g02_06_Export_GroupBox.Name = "g02_06_Export_GroupBox";
            this.g02_06_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g02_06_Export_GroupBox.TabIndex = 23;
            this.g02_06_Export_GroupBox.TabStop = false;
            this.g02_06_Export_GroupBox.Text = "匯出路徑";
            // 
            // g02_06_Folder_Label
            // 
            this.g02_06_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_06_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g02_06_Folder_Label.Name = "g02_06_Folder_Label";
            this.g02_06_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g02_06_Folder_Label.TabIndex = 8;
            this.g02_06_Folder_Label.Text = "請選擇匯出路徑";
            this.g02_06_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g02_06_selectFolder_Button
            // 
            this.g02_06_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g02_06_selectFolder_Button.Name = "g02_06_selectFolder_Button";
            this.g02_06_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g02_06_selectFolder_Button.TabIndex = 7;
            this.g02_06_selectFolder_Button.Text = "選擇匯出路徑";
            this.g02_06_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g02_06_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g02_06_FFU_GroupBox
            // 
            this.g02_06_FFU_GroupBox.Controls.Add(this.g02_06_IsSensor_CheckBox);
            this.g02_06_FFU_GroupBox.Controls.Add(this.label183);
            this.g02_06_FFU_GroupBox.Controls.Add(this.g02_06_Particle_ID_TextBox);
            this.g02_06_FFU_GroupBox.Controls.Add(this.g02_06_Particle_Quanity_TextBox);
            this.g02_06_FFU_GroupBox.Controls.Add(this.label184);
            this.g02_06_FFU_GroupBox.Controls.Add(this.g02_06_DPS_ID_TextBox);
            this.g02_06_FFU_GroupBox.Controls.Add(this.g02_06_DPS_Quanity_TextBox);
            this.g02_06_FFU_GroupBox.Controls.Add(this.label185);
            this.g02_06_FFU_GroupBox.Controls.Add(this.g02_06_FFU_Quanity_TextBox);
            this.g02_06_FFU_GroupBox.Controls.Add(this.panel19);
            this.g02_06_FFU_GroupBox.Controls.Add(this.label189);
            this.g02_06_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g02_06_FFU_GroupBox.Name = "g02_06_FFU_GroupBox";
            this.g02_06_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g02_06_FFU_GroupBox.TabIndex = 21;
            this.g02_06_FFU_GroupBox.TabStop = false;
            this.g02_06_FFU_GroupBox.Text = "FFU";
            // 
            // g02_06_IsSensor_CheckBox
            // 
            this.g02_06_IsSensor_CheckBox.AutoSize = true;
            this.g02_06_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g02_06_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g02_06_IsSensor_CheckBox.Name = "g02_06_IsSensor_CheckBox";
            this.g02_06_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g02_06_IsSensor_CheckBox.TabIndex = 37;
            this.g02_06_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g02_06_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label183.Location = new System.Drawing.Point(212, 129);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(28, 31);
            this.label183.TabIndex = 25;
            this.label183.Text = "1";
            this.label183.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_Particle_ID_TextBox
            // 
            this.g02_06_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g02_06_Particle_ID_TextBox.Name = "g02_06_Particle_ID_TextBox";
            this.g02_06_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_06_Particle_ID_TextBox.TabIndex = 24;
            this.g02_06_Particle_ID_TextBox.Text = "65";
            this.g02_06_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_06_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g02_06_Particle_Quanity_TextBox
            // 
            this.g02_06_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g02_06_Particle_Quanity_TextBox.Name = "g02_06_Particle_Quanity_TextBox";
            this.g02_06_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_06_Particle_Quanity_TextBox.TabIndex = 23;
            this.g02_06_Particle_Quanity_TextBox.Text = "1";
            this.g02_06_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label184.Location = new System.Drawing.Point(28, 273);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(100, 31);
            this.label184.TabIndex = 22;
            this.label184.Text = "Particle";
            this.label184.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_DPS_ID_TextBox
            // 
            this.g02_06_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g02_06_DPS_ID_TextBox.Name = "g02_06_DPS_ID_TextBox";
            this.g02_06_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_06_DPS_ID_TextBox.TabIndex = 21;
            this.g02_06_DPS_ID_TextBox.Text = "64";
            this.g02_06_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g02_06_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g02_06_DPS_Quanity_TextBox
            // 
            this.g02_06_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g02_06_DPS_Quanity_TextBox.Name = "g02_06_DPS_Quanity_TextBox";
            this.g02_06_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_06_DPS_Quanity_TextBox.TabIndex = 20;
            this.g02_06_DPS_Quanity_TextBox.Text = "1";
            this.g02_06_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label185.Location = new System.Drawing.Point(47, 202);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(62, 31);
            this.label185.TabIndex = 19;
            this.label185.Text = "DPS";
            this.label185.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_FFU_Quanity_TextBox
            // 
            this.g02_06_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g02_06_FFU_Quanity_TextBox.Name = "g02_06_FFU_Quanity_TextBox";
            this.g02_06_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g02_06_FFU_Quanity_TextBox.TabIndex = 17;
            this.g02_06_FFU_Quanity_TextBox.Text = "1";
            this.g02_06_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.label186);
            this.panel19.Controls.Add(this.label187);
            this.panel19.Controls.Add(this.label188);
            this.panel19.Location = new System.Drawing.Point(25, 33);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(445, 69);
            this.panel19.TabIndex = 4;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label186.Location = new System.Drawing.Point(21, 16);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(62, 31);
            this.label186.TabIndex = 0;
            this.label186.Text = "格式";
            this.label186.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label187.Location = new System.Drawing.Point(148, 16);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(110, 31);
            this.label187.TabIndex = 1;
            this.label187.Text = "起始站號";
            this.label187.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label188.Location = new System.Drawing.Point(316, 16);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(110, 31);
            this.label188.TabIndex = 2;
            this.label188.Text = "輸出數量";
            this.label188.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label189.Location = new System.Drawing.Point(40, 133);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(88, 31);
            this.label189.TabIndex = 3;
            this.label189.Text = "SPEED";
            this.label189.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_EDC_GroupBox
            // 
            this.g02_06_EDC_GroupBox.Controls.Add(this.label190);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox9);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label191);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox8);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label192);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox7);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label193);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox6);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label194);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox5);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label195);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox4);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label196);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox3);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label197);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox2);
            this.g02_06_EDC_GroupBox.Controls.Add(this.label198);
            this.g02_06_EDC_GroupBox.Controls.Add(this.g02_06_TextBox1);
            this.g02_06_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g02_06_EDC_GroupBox.Name = "g02_06_EDC_GroupBox";
            this.g02_06_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g02_06_EDC_GroupBox.TabIndex = 20;
            this.g02_06_EDC_GroupBox.TabStop = false;
            this.g02_06_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label190.Location = new System.Drawing.Point(51, 71);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(118, 31);
            this.label190.TabIndex = 2;
            this.label190.Text = "glass_id :";
            this.label190.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox9
            // 
            this.g02_06_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g02_06_TextBox9.Name = "g02_06_TextBox9";
            this.g02_06_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox9.TabIndex = 19;
            this.g02_06_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label191.Location = new System.Drawing.Point(17, 161);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(152, 31);
            this.label191.TabIndex = 3;
            this.label191.Text = "product_id :";
            this.label191.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox8
            // 
            this.g02_06_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g02_06_TextBox8.Name = "g02_06_TextBox8";
            this.g02_06_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox8.TabIndex = 18;
            this.g02_06_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label192.Location = new System.Drawing.Point(63, 251);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(106, 31);
            this.label192.TabIndex = 4;
            this.label192.Text = "eqp_id :";
            this.label192.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox7
            // 
            this.g02_06_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g02_06_TextBox7.Name = "g02_06_TextBox7";
            this.g02_06_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox7.TabIndex = 17;
            this.g02_06_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label193.Location = new System.Drawing.Point(10, 341);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(159, 31);
            this.label193.TabIndex = 5;
            this.label193.Text = "sub_eqp_id :";
            this.label193.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox6
            // 
            this.g02_06_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g02_06_TextBox6.Name = "g02_06_TextBox6";
            this.g02_06_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox6.TabIndex = 16;
            this.g02_06_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label194.Location = new System.Drawing.Point(71, 431);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(98, 31);
            this.label194.TabIndex = 6;
            this.label194.Text = "owner :";
            this.label194.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox5
            // 
            this.g02_06_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g02_06_TextBox5.Name = "g02_06_TextBox5";
            this.g02_06_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox5.TabIndex = 15;
            this.g02_06_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label195.Location = new System.Drawing.Point(409, 71);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(132, 31);
            this.label195.TabIndex = 7;
            this.label195.Text = "recipe_id :";
            this.label195.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox4
            // 
            this.g02_06_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g02_06_TextBox4.Name = "g02_06_TextBox4";
            this.g02_06_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox4.TabIndex = 14;
            this.g02_06_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label196.Location = new System.Drawing.Point(402, 161);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(139, 31);
            this.label196.TabIndex = 8;
            this.label196.Text = "operation :";
            this.label196.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox3
            // 
            this.g02_06_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g02_06_TextBox3.Name = "g02_06_TextBox3";
            this.g02_06_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox3.TabIndex = 13;
            this.g02_06_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label197.Location = new System.Drawing.Point(413, 251);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(128, 31);
            this.label197.TabIndex = 9;
            this.label197.Text = "chamber :";
            this.label197.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox2
            // 
            this.g02_06_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g02_06_TextBox2.Name = "g02_06_TextBox2";
            this.g02_06_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox2.TabIndex = 12;
            this.g02_06_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label198.Location = new System.Drawing.Point(415, 341);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(126, 31);
            this.label198.TabIndex = 10;
            this.label198.Text = "operator :";
            this.label198.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g02_06_TextBox1
            // 
            this.g02_06_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g02_06_TextBox1.Name = "g02_06_TextBox1";
            this.g02_06_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g02_06_TextBox1.TabIndex = 11;
            this.g02_06_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g03_TabPage
            // 
            this.g03_TabPage.Controls.Add(this.g03_Panel);
            this.g03_TabPage.Location = new System.Drawing.Point(4, 37);
            this.g03_TabPage.Name = "g03_TabPage";
            this.g03_TabPage.Size = new System.Drawing.Size(1358, 842);
            this.g03_TabPage.TabIndex = 2;
            this.g03_TabPage.Text = "Gateway03";
            this.g03_TabPage.UseVisualStyleBackColor = true;
            // 
            // g03_Panel
            // 
            this.g03_Panel.Controls.Add(this.label100);
            this.g03_Panel.Controls.Add(this.g03_Time_comboBox);
            this.g03_Panel.Controls.Add(this.g03_connect_Button);
            this.g03_Panel.Controls.Add(this.g03_connect_Label);
            this.g03_Panel.Controls.Add(this.g03_IP_TextBox);
            this.g03_Panel.Controls.Add(this.label199);
            this.g03_Panel.Controls.Add(this.unit_Tab3);
            this.g03_Panel.Location = new System.Drawing.Point(6, 6);
            this.g03_Panel.Name = "g03_Panel";
            this.g03_Panel.Size = new System.Drawing.Size(1338, 830);
            this.g03_Panel.TabIndex = 4;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label100.Location = new System.Drawing.Point(27, 71);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(116, 31);
            this.label100.TabIndex = 8;
            this.label100.Text = "匯出頻率:";
            // 
            // g03_Time_comboBox
            // 
            this.g03_Time_comboBox.FormattingEnabled = true;
            this.g03_Time_comboBox.Location = new System.Drawing.Point(149, 71);
            this.g03_Time_comboBox.Name = "g03_Time_comboBox";
            this.g03_Time_comboBox.Size = new System.Drawing.Size(171, 33);
            this.g03_Time_comboBox.TabIndex = 7;
            // 
            // g03_connect_Button
            // 
            this.g03_connect_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.g03_connect_Button.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_connect_Button.Location = new System.Drawing.Point(1134, 29);
            this.g03_connect_Button.Name = "g03_connect_Button";
            this.g03_connect_Button.Size = new System.Drawing.Size(188, 66);
            this.g03_connect_Button.TabIndex = 4;
            this.g03_connect_Button.Text = "連線";
            this.g03_connect_Button.UseVisualStyleBackColor = true;
            this.g03_connect_Button.Click += new System.EventHandler(this.Connect_Button_Click);
            // 
            // g03_connect_Label
            // 
            this.g03_connect_Label.AutoSize = true;
            this.g03_connect_Label.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_connect_Label.ForeColor = System.Drawing.Color.Red;
            this.g03_connect_Label.Location = new System.Drawing.Point(326, 22);
            this.g03_connect_Label.Name = "g03_connect_Label";
            this.g03_connect_Label.Size = new System.Drawing.Size(86, 31);
            this.g03_connect_Label.TabIndex = 3;
            this.g03_connect_Label.Text = "未連線";
            this.g03_connect_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_IP_TextBox
            // 
            this.g03_IP_TextBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_IP_TextBox.Location = new System.Drawing.Point(82, 19);
            this.g03_IP_TextBox.Name = "g03_IP_TextBox";
            this.g03_IP_TextBox.Size = new System.Drawing.Size(238, 34);
            this.g03_IP_TextBox.TabIndex = 2;
            this.g03_IP_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label199.Location = new System.Drawing.Point(27, 19);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(49, 36);
            this.label199.TabIndex = 1;
            this.label199.Text = "IP:";
            // 
            // unit_Tab3
            // 
            this.unit_Tab3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unit_Tab3.Controls.Add(this.g03_01_TabPage);
            this.unit_Tab3.Controls.Add(this.g03_02_TabPage);
            this.unit_Tab3.Controls.Add(this.g03_03_TabPage);
            this.unit_Tab3.Controls.Add(this.g03_04_TabPage);
            this.unit_Tab3.Controls.Add(this.g03_05_TabPage);
            this.unit_Tab3.Controls.Add(this.g03_06_TabPage);
            this.unit_Tab3.Location = new System.Drawing.Point(3, 118);
            this.unit_Tab3.Name = "unit_Tab3";
            this.unit_Tab3.SelectedIndex = 0;
            this.unit_Tab3.Size = new System.Drawing.Size(1332, 709);
            this.unit_Tab3.TabIndex = 0;
            // 
            // g03_01_TabPage
            // 
            this.g03_01_TabPage.Controls.Add(this.g03_01_Panel);
            this.g03_01_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g03_01_TabPage.Name = "g03_01_TabPage";
            this.g03_01_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g03_01_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g03_01_TabPage.TabIndex = 0;
            this.g03_01_TabPage.Text = "Unit01";
            this.g03_01_TabPage.UseVisualStyleBackColor = true;
            // 
            // g03_01_Panel
            // 
            this.g03_01_Panel.Controls.Add(this.g03_01_Export_GroupBox);
            this.g03_01_Panel.Controls.Add(this.g03_01_FFU_GroupBox);
            this.g03_01_Panel.Controls.Add(this.g03_01_EDC_GroupBox);
            this.g03_01_Panel.Location = new System.Drawing.Point(6, 6);
            this.g03_01_Panel.Name = "g03_01_Panel";
            this.g03_01_Panel.Size = new System.Drawing.Size(1309, 659);
            this.g03_01_Panel.TabIndex = 0;
            // 
            // g03_01_Export_GroupBox
            // 
            this.g03_01_Export_GroupBox.Controls.Add(this.g03_01_Folder_Label);
            this.g03_01_Export_GroupBox.Controls.Add(this.g03_01_selectFolder_Button);
            this.g03_01_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g03_01_Export_GroupBox.Name = "g03_01_Export_GroupBox";
            this.g03_01_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g03_01_Export_GroupBox.TabIndex = 28;
            this.g03_01_Export_GroupBox.TabStop = false;
            this.g03_01_Export_GroupBox.Text = "匯出路徑";
            // 
            // g03_01_Folder_Label
            // 
            this.g03_01_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_01_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g03_01_Folder_Label.Name = "g03_01_Folder_Label";
            this.g03_01_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g03_01_Folder_Label.TabIndex = 8;
            this.g03_01_Folder_Label.Text = "請選擇匯出路徑";
            this.g03_01_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g03_01_selectFolder_Button
            // 
            this.g03_01_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g03_01_selectFolder_Button.Name = "g03_01_selectFolder_Button";
            this.g03_01_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g03_01_selectFolder_Button.TabIndex = 7;
            this.g03_01_selectFolder_Button.Text = "選擇匯出路徑";
            this.g03_01_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g03_01_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g03_01_FFU_GroupBox
            // 
            this.g03_01_FFU_GroupBox.Controls.Add(this.g03_01_IsSensor_CheckBox);
            this.g03_01_FFU_GroupBox.Controls.Add(this.label201);
            this.g03_01_FFU_GroupBox.Controls.Add(this.g03_01_Particle_ID_TextBox);
            this.g03_01_FFU_GroupBox.Controls.Add(this.g03_01_Particle_Quanity_TextBox);
            this.g03_01_FFU_GroupBox.Controls.Add(this.label202);
            this.g03_01_FFU_GroupBox.Controls.Add(this.g03_01_DPS_ID_TextBox);
            this.g03_01_FFU_GroupBox.Controls.Add(this.g03_01_DPS_Quanity_TextBox);
            this.g03_01_FFU_GroupBox.Controls.Add(this.label203);
            this.g03_01_FFU_GroupBox.Controls.Add(this.g03_01_FFU_Quanity_TextBox);
            this.g03_01_FFU_GroupBox.Controls.Add(this.panel9);
            this.g03_01_FFU_GroupBox.Controls.Add(this.label207);
            this.g03_01_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g03_01_FFU_GroupBox.Name = "g03_01_FFU_GroupBox";
            this.g03_01_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g03_01_FFU_GroupBox.TabIndex = 21;
            this.g03_01_FFU_GroupBox.TabStop = false;
            this.g03_01_FFU_GroupBox.Text = "FFU";
            // 
            // g03_01_IsSensor_CheckBox
            // 
            this.g03_01_IsSensor_CheckBox.AutoSize = true;
            this.g03_01_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_01_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g03_01_IsSensor_CheckBox.Name = "g03_01_IsSensor_CheckBox";
            this.g03_01_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g03_01_IsSensor_CheckBox.TabIndex = 32;
            this.g03_01_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g03_01_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label201.Location = new System.Drawing.Point(212, 129);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(28, 31);
            this.label201.TabIndex = 25;
            this.label201.Text = "1";
            this.label201.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_Particle_ID_TextBox
            // 
            this.g03_01_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g03_01_Particle_ID_TextBox.Name = "g03_01_Particle_ID_TextBox";
            this.g03_01_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_01_Particle_ID_TextBox.TabIndex = 24;
            this.g03_01_Particle_ID_TextBox.Text = "65";
            this.g03_01_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_01_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g03_01_Particle_Quanity_TextBox
            // 
            this.g03_01_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g03_01_Particle_Quanity_TextBox.Name = "g03_01_Particle_Quanity_TextBox";
            this.g03_01_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_01_Particle_Quanity_TextBox.TabIndex = 23;
            this.g03_01_Particle_Quanity_TextBox.Text = "1";
            this.g03_01_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label202.Location = new System.Drawing.Point(28, 273);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(100, 31);
            this.label202.TabIndex = 22;
            this.label202.Text = "Particle";
            this.label202.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_DPS_ID_TextBox
            // 
            this.g03_01_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g03_01_DPS_ID_TextBox.Name = "g03_01_DPS_ID_TextBox";
            this.g03_01_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_01_DPS_ID_TextBox.TabIndex = 21;
            this.g03_01_DPS_ID_TextBox.Text = "64";
            this.g03_01_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_01_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g03_01_DPS_Quanity_TextBox
            // 
            this.g03_01_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g03_01_DPS_Quanity_TextBox.Name = "g03_01_DPS_Quanity_TextBox";
            this.g03_01_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_01_DPS_Quanity_TextBox.TabIndex = 20;
            this.g03_01_DPS_Quanity_TextBox.Text = "1";
            this.g03_01_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label203.Location = new System.Drawing.Point(47, 202);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(62, 31);
            this.label203.TabIndex = 19;
            this.label203.Text = "DPS";
            this.label203.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_FFU_Quanity_TextBox
            // 
            this.g03_01_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g03_01_FFU_Quanity_TextBox.Name = "g03_01_FFU_Quanity_TextBox";
            this.g03_01_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_01_FFU_Quanity_TextBox.TabIndex = 17;
            this.g03_01_FFU_Quanity_TextBox.Text = "1";
            this.g03_01_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label204);
            this.panel9.Controls.Add(this.label205);
            this.panel9.Controls.Add(this.label206);
            this.panel9.Location = new System.Drawing.Point(25, 33);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(445, 69);
            this.panel9.TabIndex = 4;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label204.Location = new System.Drawing.Point(21, 16);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(62, 31);
            this.label204.TabIndex = 0;
            this.label204.Text = "格式";
            this.label204.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label205.Location = new System.Drawing.Point(148, 16);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(110, 31);
            this.label205.TabIndex = 1;
            this.label205.Text = "起始站號";
            this.label205.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label206.Location = new System.Drawing.Point(316, 16);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(110, 31);
            this.label206.TabIndex = 2;
            this.label206.Text = "輸出數量";
            this.label206.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label207.Location = new System.Drawing.Point(40, 133);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(88, 31);
            this.label207.TabIndex = 3;
            this.label207.Text = "SPEED";
            this.label207.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_EDC_GroupBox
            // 
            this.g03_01_EDC_GroupBox.Controls.Add(this.label208);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox9);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label209);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox8);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label210);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox7);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label211);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox6);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label212);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox5);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label213);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox4);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label214);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox3);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label215);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox2);
            this.g03_01_EDC_GroupBox.Controls.Add(this.label216);
            this.g03_01_EDC_GroupBox.Controls.Add(this.g03_01_TextBox1);
            this.g03_01_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g03_01_EDC_GroupBox.Name = "g03_01_EDC_GroupBox";
            this.g03_01_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g03_01_EDC_GroupBox.TabIndex = 20;
            this.g03_01_EDC_GroupBox.TabStop = false;
            this.g03_01_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label208.Location = new System.Drawing.Point(51, 71);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(118, 31);
            this.label208.TabIndex = 2;
            this.label208.Text = "glass_id :";
            this.label208.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox9
            // 
            this.g03_01_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g03_01_TextBox9.Name = "g03_01_TextBox9";
            this.g03_01_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox9.TabIndex = 19;
            this.g03_01_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label209.Location = new System.Drawing.Point(17, 161);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(152, 31);
            this.label209.TabIndex = 3;
            this.label209.Text = "product_id :";
            this.label209.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox8
            // 
            this.g03_01_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g03_01_TextBox8.Name = "g03_01_TextBox8";
            this.g03_01_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox8.TabIndex = 18;
            this.g03_01_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label210.Location = new System.Drawing.Point(63, 251);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(106, 31);
            this.label210.TabIndex = 4;
            this.label210.Text = "eqp_id :";
            this.label210.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox7
            // 
            this.g03_01_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g03_01_TextBox7.Name = "g03_01_TextBox7";
            this.g03_01_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox7.TabIndex = 17;
            this.g03_01_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label211.Location = new System.Drawing.Point(10, 341);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(159, 31);
            this.label211.TabIndex = 5;
            this.label211.Text = "sub_eqp_id :";
            this.label211.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox6
            // 
            this.g03_01_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g03_01_TextBox6.Name = "g03_01_TextBox6";
            this.g03_01_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox6.TabIndex = 16;
            this.g03_01_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label212.Location = new System.Drawing.Point(71, 431);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(98, 31);
            this.label212.TabIndex = 6;
            this.label212.Text = "owner :";
            this.label212.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox5
            // 
            this.g03_01_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g03_01_TextBox5.Name = "g03_01_TextBox5";
            this.g03_01_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox5.TabIndex = 15;
            this.g03_01_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label213.Location = new System.Drawing.Point(409, 71);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(132, 31);
            this.label213.TabIndex = 7;
            this.label213.Text = "recipe_id :";
            this.label213.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox4
            // 
            this.g03_01_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g03_01_TextBox4.Name = "g03_01_TextBox4";
            this.g03_01_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox4.TabIndex = 14;
            this.g03_01_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label214.Location = new System.Drawing.Point(402, 161);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(139, 31);
            this.label214.TabIndex = 8;
            this.label214.Text = "operation :";
            this.label214.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox3
            // 
            this.g03_01_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g03_01_TextBox3.Name = "g03_01_TextBox3";
            this.g03_01_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox3.TabIndex = 13;
            this.g03_01_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label215.Location = new System.Drawing.Point(413, 251);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(128, 31);
            this.label215.TabIndex = 9;
            this.label215.Text = "chamber :";
            this.label215.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox2
            // 
            this.g03_01_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g03_01_TextBox2.Name = "g03_01_TextBox2";
            this.g03_01_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox2.TabIndex = 12;
            this.g03_01_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label216.Location = new System.Drawing.Point(415, 341);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(126, 31);
            this.label216.TabIndex = 10;
            this.label216.Text = "operator :";
            this.label216.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_01_TextBox1
            // 
            this.g03_01_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g03_01_TextBox1.Name = "g03_01_TextBox1";
            this.g03_01_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g03_01_TextBox1.TabIndex = 11;
            this.g03_01_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g03_02_TabPage
            // 
            this.g03_02_TabPage.Controls.Add(this.panel12);
            this.g03_02_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g03_02_TabPage.Name = "g03_02_TabPage";
            this.g03_02_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g03_02_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g03_02_TabPage.TabIndex = 1;
            this.g03_02_TabPage.Text = "Unit02";
            this.g03_02_TabPage.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.g03_02_Export_GroupBox);
            this.panel12.Controls.Add(this.g03_02_FFU_GroupBox);
            this.panel12.Controls.Add(this.g03_02_EDC_GroupBox);
            this.panel12.Location = new System.Drawing.Point(6, 6);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1309, 659);
            this.panel12.TabIndex = 1;
            // 
            // g03_02_Export_GroupBox
            // 
            this.g03_02_Export_GroupBox.Controls.Add(this.g03_02_Folder_Label);
            this.g03_02_Export_GroupBox.Controls.Add(this.g03_02_selectFolder_Button);
            this.g03_02_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g03_02_Export_GroupBox.Name = "g03_02_Export_GroupBox";
            this.g03_02_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g03_02_Export_GroupBox.TabIndex = 27;
            this.g03_02_Export_GroupBox.TabStop = false;
            this.g03_02_Export_GroupBox.Text = "匯出路徑";
            // 
            // g03_02_Folder_Label
            // 
            this.g03_02_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_02_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g03_02_Folder_Label.Name = "g03_02_Folder_Label";
            this.g03_02_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g03_02_Folder_Label.TabIndex = 8;
            this.g03_02_Folder_Label.Text = "請選擇匯出路徑";
            this.g03_02_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g03_02_selectFolder_Button
            // 
            this.g03_02_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g03_02_selectFolder_Button.Name = "g03_02_selectFolder_Button";
            this.g03_02_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g03_02_selectFolder_Button.TabIndex = 7;
            this.g03_02_selectFolder_Button.Text = "選擇匯出路徑";
            this.g03_02_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g03_02_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g03_02_FFU_GroupBox
            // 
            this.g03_02_FFU_GroupBox.Controls.Add(this.g03_02_IsSensor_CheckBox);
            this.g03_02_FFU_GroupBox.Controls.Add(this.label218);
            this.g03_02_FFU_GroupBox.Controls.Add(this.g03_02_Particle_ID_TextBox);
            this.g03_02_FFU_GroupBox.Controls.Add(this.g03_02_Particle_Quanity_TextBox);
            this.g03_02_FFU_GroupBox.Controls.Add(this.label219);
            this.g03_02_FFU_GroupBox.Controls.Add(this.g03_02_DPS_ID_TextBox);
            this.g03_02_FFU_GroupBox.Controls.Add(this.g03_02_DPS_Quanity_TextBox);
            this.g03_02_FFU_GroupBox.Controls.Add(this.label220);
            this.g03_02_FFU_GroupBox.Controls.Add(this.g03_02_FFU_Quanity_TextBox);
            this.g03_02_FFU_GroupBox.Controls.Add(this.panel14);
            this.g03_02_FFU_GroupBox.Controls.Add(this.label224);
            this.g03_02_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g03_02_FFU_GroupBox.Name = "g03_02_FFU_GroupBox";
            this.g03_02_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g03_02_FFU_GroupBox.TabIndex = 21;
            this.g03_02_FFU_GroupBox.TabStop = false;
            this.g03_02_FFU_GroupBox.Text = "FFU";
            // 
            // g03_02_IsSensor_CheckBox
            // 
            this.g03_02_IsSensor_CheckBox.AutoSize = true;
            this.g03_02_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_02_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g03_02_IsSensor_CheckBox.Name = "g03_02_IsSensor_CheckBox";
            this.g03_02_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g03_02_IsSensor_CheckBox.TabIndex = 33;
            this.g03_02_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g03_02_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label218.Location = new System.Drawing.Point(212, 129);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(28, 31);
            this.label218.TabIndex = 25;
            this.label218.Text = "1";
            this.label218.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_Particle_ID_TextBox
            // 
            this.g03_02_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g03_02_Particle_ID_TextBox.Name = "g03_02_Particle_ID_TextBox";
            this.g03_02_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_02_Particle_ID_TextBox.TabIndex = 24;
            this.g03_02_Particle_ID_TextBox.Text = "65";
            this.g03_02_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_02_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g03_02_Particle_Quanity_TextBox
            // 
            this.g03_02_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g03_02_Particle_Quanity_TextBox.Name = "g03_02_Particle_Quanity_TextBox";
            this.g03_02_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_02_Particle_Quanity_TextBox.TabIndex = 23;
            this.g03_02_Particle_Quanity_TextBox.Text = "1";
            this.g03_02_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label219.Location = new System.Drawing.Point(28, 273);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(100, 31);
            this.label219.TabIndex = 22;
            this.label219.Text = "Particle";
            this.label219.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_DPS_ID_TextBox
            // 
            this.g03_02_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g03_02_DPS_ID_TextBox.Name = "g03_02_DPS_ID_TextBox";
            this.g03_02_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_02_DPS_ID_TextBox.TabIndex = 21;
            this.g03_02_DPS_ID_TextBox.Text = "64";
            this.g03_02_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_02_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g03_02_DPS_Quanity_TextBox
            // 
            this.g03_02_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g03_02_DPS_Quanity_TextBox.Name = "g03_02_DPS_Quanity_TextBox";
            this.g03_02_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_02_DPS_Quanity_TextBox.TabIndex = 20;
            this.g03_02_DPS_Quanity_TextBox.Text = "1";
            this.g03_02_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label220.Location = new System.Drawing.Point(47, 202);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(62, 31);
            this.label220.TabIndex = 19;
            this.label220.Text = "DPS";
            this.label220.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_FFU_Quanity_TextBox
            // 
            this.g03_02_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g03_02_FFU_Quanity_TextBox.Name = "g03_02_FFU_Quanity_TextBox";
            this.g03_02_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_02_FFU_Quanity_TextBox.TabIndex = 17;
            this.g03_02_FFU_Quanity_TextBox.Text = "1";
            this.g03_02_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.label221);
            this.panel14.Controls.Add(this.label222);
            this.panel14.Controls.Add(this.label223);
            this.panel14.Location = new System.Drawing.Point(25, 33);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(445, 69);
            this.panel14.TabIndex = 4;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label221.Location = new System.Drawing.Point(21, 16);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(62, 31);
            this.label221.TabIndex = 0;
            this.label221.Text = "格式";
            this.label221.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label222.Location = new System.Drawing.Point(148, 16);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(110, 31);
            this.label222.TabIndex = 1;
            this.label222.Text = "起始站號";
            this.label222.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label223.Location = new System.Drawing.Point(316, 16);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(110, 31);
            this.label223.TabIndex = 2;
            this.label223.Text = "輸出數量";
            this.label223.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label224.Location = new System.Drawing.Point(40, 133);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(88, 31);
            this.label224.TabIndex = 3;
            this.label224.Text = "SPEED";
            this.label224.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_EDC_GroupBox
            // 
            this.g03_02_EDC_GroupBox.Controls.Add(this.label225);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox9);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label226);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox8);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label227);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox7);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label228);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox6);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label229);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox5);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label230);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox4);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label231);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox3);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label232);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox2);
            this.g03_02_EDC_GroupBox.Controls.Add(this.label233);
            this.g03_02_EDC_GroupBox.Controls.Add(this.g03_02_TextBox1);
            this.g03_02_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g03_02_EDC_GroupBox.Name = "g03_02_EDC_GroupBox";
            this.g03_02_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g03_02_EDC_GroupBox.TabIndex = 20;
            this.g03_02_EDC_GroupBox.TabStop = false;
            this.g03_02_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label225.Location = new System.Drawing.Point(51, 71);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(118, 31);
            this.label225.TabIndex = 2;
            this.label225.Text = "glass_id :";
            this.label225.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox9
            // 
            this.g03_02_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g03_02_TextBox9.Name = "g03_02_TextBox9";
            this.g03_02_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox9.TabIndex = 19;
            this.g03_02_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label226.Location = new System.Drawing.Point(17, 161);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(152, 31);
            this.label226.TabIndex = 3;
            this.label226.Text = "product_id :";
            this.label226.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox8
            // 
            this.g03_02_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g03_02_TextBox8.Name = "g03_02_TextBox8";
            this.g03_02_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox8.TabIndex = 18;
            this.g03_02_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label227.Location = new System.Drawing.Point(63, 251);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(106, 31);
            this.label227.TabIndex = 4;
            this.label227.Text = "eqp_id :";
            this.label227.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox7
            // 
            this.g03_02_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g03_02_TextBox7.Name = "g03_02_TextBox7";
            this.g03_02_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox7.TabIndex = 17;
            this.g03_02_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label228.Location = new System.Drawing.Point(10, 341);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(159, 31);
            this.label228.TabIndex = 5;
            this.label228.Text = "sub_eqp_id :";
            this.label228.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox6
            // 
            this.g03_02_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g03_02_TextBox6.Name = "g03_02_TextBox6";
            this.g03_02_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox6.TabIndex = 16;
            this.g03_02_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label229.Location = new System.Drawing.Point(71, 431);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(98, 31);
            this.label229.TabIndex = 6;
            this.label229.Text = "owner :";
            this.label229.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox5
            // 
            this.g03_02_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g03_02_TextBox5.Name = "g03_02_TextBox5";
            this.g03_02_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox5.TabIndex = 15;
            this.g03_02_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label230.Location = new System.Drawing.Point(409, 71);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(132, 31);
            this.label230.TabIndex = 7;
            this.label230.Text = "recipe_id :";
            this.label230.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox4
            // 
            this.g03_02_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g03_02_TextBox4.Name = "g03_02_TextBox4";
            this.g03_02_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox4.TabIndex = 14;
            this.g03_02_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label231.Location = new System.Drawing.Point(402, 161);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(139, 31);
            this.label231.TabIndex = 8;
            this.label231.Text = "operation :";
            this.label231.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox3
            // 
            this.g03_02_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g03_02_TextBox3.Name = "g03_02_TextBox3";
            this.g03_02_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox3.TabIndex = 13;
            this.g03_02_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label232.Location = new System.Drawing.Point(413, 251);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(128, 31);
            this.label232.TabIndex = 9;
            this.label232.Text = "chamber :";
            this.label232.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox2
            // 
            this.g03_02_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g03_02_TextBox2.Name = "g03_02_TextBox2";
            this.g03_02_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox2.TabIndex = 12;
            this.g03_02_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label233.Location = new System.Drawing.Point(415, 341);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(126, 31);
            this.label233.TabIndex = 10;
            this.label233.Text = "operator :";
            this.label233.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_02_TextBox1
            // 
            this.g03_02_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g03_02_TextBox1.Name = "g03_02_TextBox1";
            this.g03_02_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g03_02_TextBox1.TabIndex = 11;
            this.g03_02_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g03_03_TabPage
            // 
            this.g03_03_TabPage.Controls.Add(this.panel16);
            this.g03_03_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g03_03_TabPage.Name = "g03_03_TabPage";
            this.g03_03_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g03_03_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g03_03_TabPage.TabIndex = 2;
            this.g03_03_TabPage.Text = "Unit03";
            this.g03_03_TabPage.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.g03_03_Export_GroupBox);
            this.panel16.Controls.Add(this.g03_03_FFU_GroupBox);
            this.panel16.Controls.Add(this.g03_03_EDC_GroupBox);
            this.panel16.Location = new System.Drawing.Point(6, 6);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1309, 659);
            this.panel16.TabIndex = 2;
            // 
            // g03_03_Export_GroupBox
            // 
            this.g03_03_Export_GroupBox.Controls.Add(this.g03_03_Folder_Label);
            this.g03_03_Export_GroupBox.Controls.Add(this.g03_03_selectFolder_Button);
            this.g03_03_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g03_03_Export_GroupBox.Name = "g03_03_Export_GroupBox";
            this.g03_03_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g03_03_Export_GroupBox.TabIndex = 26;
            this.g03_03_Export_GroupBox.TabStop = false;
            this.g03_03_Export_GroupBox.Text = "匯出路徑";
            // 
            // g03_03_Folder_Label
            // 
            this.g03_03_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_03_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g03_03_Folder_Label.Name = "g03_03_Folder_Label";
            this.g03_03_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g03_03_Folder_Label.TabIndex = 8;
            this.g03_03_Folder_Label.Text = "請選擇匯出路徑";
            this.g03_03_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g03_03_selectFolder_Button
            // 
            this.g03_03_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g03_03_selectFolder_Button.Name = "g03_03_selectFolder_Button";
            this.g03_03_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g03_03_selectFolder_Button.TabIndex = 7;
            this.g03_03_selectFolder_Button.Text = "選擇匯出路徑";
            this.g03_03_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g03_03_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g03_03_FFU_GroupBox
            // 
            this.g03_03_FFU_GroupBox.Controls.Add(this.g03_03_IsSensor_CheckBox);
            this.g03_03_FFU_GroupBox.Controls.Add(this.label235);
            this.g03_03_FFU_GroupBox.Controls.Add(this.g03_03_Particle_ID_TextBox);
            this.g03_03_FFU_GroupBox.Controls.Add(this.g03_03_Particle_Quanity_TextBox);
            this.g03_03_FFU_GroupBox.Controls.Add(this.label236);
            this.g03_03_FFU_GroupBox.Controls.Add(this.g03_03_DPS_ID_TextBox);
            this.g03_03_FFU_GroupBox.Controls.Add(this.g03_03_DPS_Quanity_TextBox);
            this.g03_03_FFU_GroupBox.Controls.Add(this.label237);
            this.g03_03_FFU_GroupBox.Controls.Add(this.g03_03_FFU_Quanity_TextBox);
            this.g03_03_FFU_GroupBox.Controls.Add(this.panel18);
            this.g03_03_FFU_GroupBox.Controls.Add(this.label241);
            this.g03_03_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g03_03_FFU_GroupBox.Name = "g03_03_FFU_GroupBox";
            this.g03_03_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g03_03_FFU_GroupBox.TabIndex = 21;
            this.g03_03_FFU_GroupBox.TabStop = false;
            this.g03_03_FFU_GroupBox.Text = "FFU";
            // 
            // g03_03_IsSensor_CheckBox
            // 
            this.g03_03_IsSensor_CheckBox.AutoSize = true;
            this.g03_03_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_03_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g03_03_IsSensor_CheckBox.Name = "g03_03_IsSensor_CheckBox";
            this.g03_03_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g03_03_IsSensor_CheckBox.TabIndex = 34;
            this.g03_03_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g03_03_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label235.Location = new System.Drawing.Point(212, 129);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(28, 31);
            this.label235.TabIndex = 25;
            this.label235.Text = "1";
            this.label235.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_Particle_ID_TextBox
            // 
            this.g03_03_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g03_03_Particle_ID_TextBox.Name = "g03_03_Particle_ID_TextBox";
            this.g03_03_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_03_Particle_ID_TextBox.TabIndex = 24;
            this.g03_03_Particle_ID_TextBox.Text = "65";
            this.g03_03_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_03_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g03_03_Particle_Quanity_TextBox
            // 
            this.g03_03_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g03_03_Particle_Quanity_TextBox.Name = "g03_03_Particle_Quanity_TextBox";
            this.g03_03_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_03_Particle_Quanity_TextBox.TabIndex = 23;
            this.g03_03_Particle_Quanity_TextBox.Text = "1";
            this.g03_03_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label236.Location = new System.Drawing.Point(28, 273);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(100, 31);
            this.label236.TabIndex = 22;
            this.label236.Text = "Particle";
            this.label236.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_DPS_ID_TextBox
            // 
            this.g03_03_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g03_03_DPS_ID_TextBox.Name = "g03_03_DPS_ID_TextBox";
            this.g03_03_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_03_DPS_ID_TextBox.TabIndex = 21;
            this.g03_03_DPS_ID_TextBox.Text = "64";
            this.g03_03_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_03_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g03_03_DPS_Quanity_TextBox
            // 
            this.g03_03_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g03_03_DPS_Quanity_TextBox.Name = "g03_03_DPS_Quanity_TextBox";
            this.g03_03_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_03_DPS_Quanity_TextBox.TabIndex = 20;
            this.g03_03_DPS_Quanity_TextBox.Text = "1";
            this.g03_03_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label237.Location = new System.Drawing.Point(47, 202);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(62, 31);
            this.label237.TabIndex = 19;
            this.label237.Text = "DPS";
            this.label237.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_FFU_Quanity_TextBox
            // 
            this.g03_03_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g03_03_FFU_Quanity_TextBox.Name = "g03_03_FFU_Quanity_TextBox";
            this.g03_03_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_03_FFU_Quanity_TextBox.TabIndex = 17;
            this.g03_03_FFU_Quanity_TextBox.Text = "1";
            this.g03_03_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label238);
            this.panel18.Controls.Add(this.label239);
            this.panel18.Controls.Add(this.label240);
            this.panel18.Location = new System.Drawing.Point(25, 33);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(445, 69);
            this.panel18.TabIndex = 4;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label238.Location = new System.Drawing.Point(21, 16);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(62, 31);
            this.label238.TabIndex = 0;
            this.label238.Text = "格式";
            this.label238.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label239.Location = new System.Drawing.Point(148, 16);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(110, 31);
            this.label239.TabIndex = 1;
            this.label239.Text = "起始站號";
            this.label239.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label240.Location = new System.Drawing.Point(316, 16);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(110, 31);
            this.label240.TabIndex = 2;
            this.label240.Text = "輸出數量";
            this.label240.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label241.Location = new System.Drawing.Point(40, 133);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(88, 31);
            this.label241.TabIndex = 3;
            this.label241.Text = "SPEED";
            this.label241.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_EDC_GroupBox
            // 
            this.g03_03_EDC_GroupBox.Controls.Add(this.label242);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox9);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label243);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox8);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label244);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox7);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label245);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox6);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label246);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox5);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label247);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox4);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label248);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox3);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label249);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox2);
            this.g03_03_EDC_GroupBox.Controls.Add(this.label250);
            this.g03_03_EDC_GroupBox.Controls.Add(this.g03_03_TextBox1);
            this.g03_03_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g03_03_EDC_GroupBox.Name = "g03_03_EDC_GroupBox";
            this.g03_03_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g03_03_EDC_GroupBox.TabIndex = 20;
            this.g03_03_EDC_GroupBox.TabStop = false;
            this.g03_03_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label242.Location = new System.Drawing.Point(51, 71);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(118, 31);
            this.label242.TabIndex = 2;
            this.label242.Text = "glass_id :";
            this.label242.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox9
            // 
            this.g03_03_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g03_03_TextBox9.Name = "g03_03_TextBox9";
            this.g03_03_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox9.TabIndex = 19;
            this.g03_03_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label243.Location = new System.Drawing.Point(17, 161);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(152, 31);
            this.label243.TabIndex = 3;
            this.label243.Text = "product_id :";
            this.label243.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox8
            // 
            this.g03_03_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g03_03_TextBox8.Name = "g03_03_TextBox8";
            this.g03_03_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox8.TabIndex = 18;
            this.g03_03_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label244.Location = new System.Drawing.Point(63, 251);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(106, 31);
            this.label244.TabIndex = 4;
            this.label244.Text = "eqp_id :";
            this.label244.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox7
            // 
            this.g03_03_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g03_03_TextBox7.Name = "g03_03_TextBox7";
            this.g03_03_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox7.TabIndex = 17;
            this.g03_03_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label245.Location = new System.Drawing.Point(10, 341);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(159, 31);
            this.label245.TabIndex = 5;
            this.label245.Text = "sub_eqp_id :";
            this.label245.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox6
            // 
            this.g03_03_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g03_03_TextBox6.Name = "g03_03_TextBox6";
            this.g03_03_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox6.TabIndex = 16;
            this.g03_03_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label246.Location = new System.Drawing.Point(71, 431);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(98, 31);
            this.label246.TabIndex = 6;
            this.label246.Text = "owner :";
            this.label246.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox5
            // 
            this.g03_03_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g03_03_TextBox5.Name = "g03_03_TextBox5";
            this.g03_03_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox5.TabIndex = 15;
            this.g03_03_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label247.Location = new System.Drawing.Point(409, 71);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(132, 31);
            this.label247.TabIndex = 7;
            this.label247.Text = "recipe_id :";
            this.label247.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox4
            // 
            this.g03_03_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g03_03_TextBox4.Name = "g03_03_TextBox4";
            this.g03_03_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox4.TabIndex = 14;
            this.g03_03_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label248.Location = new System.Drawing.Point(402, 161);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(139, 31);
            this.label248.TabIndex = 8;
            this.label248.Text = "operation :";
            this.label248.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox3
            // 
            this.g03_03_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g03_03_TextBox3.Name = "g03_03_TextBox3";
            this.g03_03_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox3.TabIndex = 13;
            this.g03_03_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label249.Location = new System.Drawing.Point(413, 251);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(128, 31);
            this.label249.TabIndex = 9;
            this.label249.Text = "chamber :";
            this.label249.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox2
            // 
            this.g03_03_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g03_03_TextBox2.Name = "g03_03_TextBox2";
            this.g03_03_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox2.TabIndex = 12;
            this.g03_03_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label250.Location = new System.Drawing.Point(415, 341);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(126, 31);
            this.label250.TabIndex = 10;
            this.label250.Text = "operator :";
            this.label250.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_03_TextBox1
            // 
            this.g03_03_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g03_03_TextBox1.Name = "g03_03_TextBox1";
            this.g03_03_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g03_03_TextBox1.TabIndex = 11;
            this.g03_03_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g03_04_TabPage
            // 
            this.g03_04_TabPage.Controls.Add(this.panel20);
            this.g03_04_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g03_04_TabPage.Name = "g03_04_TabPage";
            this.g03_04_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g03_04_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g03_04_TabPage.TabIndex = 3;
            this.g03_04_TabPage.Text = "Unit04";
            this.g03_04_TabPage.UseVisualStyleBackColor = true;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.g03_04_Export_GroupBox);
            this.panel20.Controls.Add(this.g03_04_FFU_GroupBox);
            this.panel20.Controls.Add(this.g03_04_EDC_GroupBox);
            this.panel20.Location = new System.Drawing.Point(6, 6);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(1309, 659);
            this.panel20.TabIndex = 2;
            // 
            // g03_04_Export_GroupBox
            // 
            this.g03_04_Export_GroupBox.Controls.Add(this.g03_04_Folder_Label);
            this.g03_04_Export_GroupBox.Controls.Add(this.g03_04_selectFolder_Button);
            this.g03_04_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g03_04_Export_GroupBox.Name = "g03_04_Export_GroupBox";
            this.g03_04_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g03_04_Export_GroupBox.TabIndex = 25;
            this.g03_04_Export_GroupBox.TabStop = false;
            this.g03_04_Export_GroupBox.Text = "匯出路徑";
            // 
            // g03_04_Folder_Label
            // 
            this.g03_04_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_04_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g03_04_Folder_Label.Name = "g03_04_Folder_Label";
            this.g03_04_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g03_04_Folder_Label.TabIndex = 8;
            this.g03_04_Folder_Label.Text = "請選擇匯出路徑";
            this.g03_04_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g03_04_selectFolder_Button
            // 
            this.g03_04_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g03_04_selectFolder_Button.Name = "g03_04_selectFolder_Button";
            this.g03_04_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g03_04_selectFolder_Button.TabIndex = 7;
            this.g03_04_selectFolder_Button.Text = "選擇匯出路徑";
            this.g03_04_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g03_04_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g03_04_FFU_GroupBox
            // 
            this.g03_04_FFU_GroupBox.Controls.Add(this.g03_04_IsSensor_CheckBox);
            this.g03_04_FFU_GroupBox.Controls.Add(this.label252);
            this.g03_04_FFU_GroupBox.Controls.Add(this.g03_04_Particle_ID_TextBox);
            this.g03_04_FFU_GroupBox.Controls.Add(this.g03_04_Particle_Quanity_TextBox);
            this.g03_04_FFU_GroupBox.Controls.Add(this.label253);
            this.g03_04_FFU_GroupBox.Controls.Add(this.g03_04_DPS_ID_TextBox);
            this.g03_04_FFU_GroupBox.Controls.Add(this.g03_04_DPS_Quanity_TextBox);
            this.g03_04_FFU_GroupBox.Controls.Add(this.label254);
            this.g03_04_FFU_GroupBox.Controls.Add(this.g03_04_FFU_Quanity_TextBox);
            this.g03_04_FFU_GroupBox.Controls.Add(this.panel21);
            this.g03_04_FFU_GroupBox.Controls.Add(this.label258);
            this.g03_04_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g03_04_FFU_GroupBox.Name = "g03_04_FFU_GroupBox";
            this.g03_04_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g03_04_FFU_GroupBox.TabIndex = 21;
            this.g03_04_FFU_GroupBox.TabStop = false;
            this.g03_04_FFU_GroupBox.Text = "FFU";
            // 
            // g03_04_IsSensor_CheckBox
            // 
            this.g03_04_IsSensor_CheckBox.AutoSize = true;
            this.g03_04_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_04_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g03_04_IsSensor_CheckBox.Name = "g03_04_IsSensor_CheckBox";
            this.g03_04_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g03_04_IsSensor_CheckBox.TabIndex = 35;
            this.g03_04_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g03_04_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label252.Location = new System.Drawing.Point(212, 129);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(28, 31);
            this.label252.TabIndex = 25;
            this.label252.Text = "1";
            this.label252.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_Particle_ID_TextBox
            // 
            this.g03_04_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g03_04_Particle_ID_TextBox.Name = "g03_04_Particle_ID_TextBox";
            this.g03_04_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_04_Particle_ID_TextBox.TabIndex = 24;
            this.g03_04_Particle_ID_TextBox.Text = "65";
            this.g03_04_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_04_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g03_04_Particle_Quanity_TextBox
            // 
            this.g03_04_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g03_04_Particle_Quanity_TextBox.Name = "g03_04_Particle_Quanity_TextBox";
            this.g03_04_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_04_Particle_Quanity_TextBox.TabIndex = 23;
            this.g03_04_Particle_Quanity_TextBox.Text = "1";
            this.g03_04_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label253.Location = new System.Drawing.Point(28, 273);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(100, 31);
            this.label253.TabIndex = 22;
            this.label253.Text = "Particle";
            this.label253.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_DPS_ID_TextBox
            // 
            this.g03_04_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g03_04_DPS_ID_TextBox.Name = "g03_04_DPS_ID_TextBox";
            this.g03_04_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_04_DPS_ID_TextBox.TabIndex = 21;
            this.g03_04_DPS_ID_TextBox.Text = "64";
            this.g03_04_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_04_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g03_04_DPS_Quanity_TextBox
            // 
            this.g03_04_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g03_04_DPS_Quanity_TextBox.Name = "g03_04_DPS_Quanity_TextBox";
            this.g03_04_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_04_DPS_Quanity_TextBox.TabIndex = 20;
            this.g03_04_DPS_Quanity_TextBox.Text = "1";
            this.g03_04_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label254.Location = new System.Drawing.Point(47, 202);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(62, 31);
            this.label254.TabIndex = 19;
            this.label254.Text = "DPS";
            this.label254.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_FFU_Quanity_TextBox
            // 
            this.g03_04_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g03_04_FFU_Quanity_TextBox.Name = "g03_04_FFU_Quanity_TextBox";
            this.g03_04_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_04_FFU_Quanity_TextBox.TabIndex = 17;
            this.g03_04_FFU_Quanity_TextBox.Text = "1";
            this.g03_04_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.label255);
            this.panel21.Controls.Add(this.label256);
            this.panel21.Controls.Add(this.label257);
            this.panel21.Location = new System.Drawing.Point(25, 33);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(445, 69);
            this.panel21.TabIndex = 4;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label255.Location = new System.Drawing.Point(21, 16);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(62, 31);
            this.label255.TabIndex = 0;
            this.label255.Text = "格式";
            this.label255.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label256.Location = new System.Drawing.Point(148, 16);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(110, 31);
            this.label256.TabIndex = 1;
            this.label256.Text = "起始站號";
            this.label256.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label257.Location = new System.Drawing.Point(316, 16);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(110, 31);
            this.label257.TabIndex = 2;
            this.label257.Text = "輸出數量";
            this.label257.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label258.Location = new System.Drawing.Point(40, 133);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(88, 31);
            this.label258.TabIndex = 3;
            this.label258.Text = "SPEED";
            this.label258.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_EDC_GroupBox
            // 
            this.g03_04_EDC_GroupBox.Controls.Add(this.label259);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox9);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label260);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox8);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label261);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox7);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label262);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox6);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label263);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox5);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label264);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox4);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label265);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox3);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label266);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox2);
            this.g03_04_EDC_GroupBox.Controls.Add(this.label267);
            this.g03_04_EDC_GroupBox.Controls.Add(this.g03_04_TextBox1);
            this.g03_04_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g03_04_EDC_GroupBox.Name = "g03_04_EDC_GroupBox";
            this.g03_04_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g03_04_EDC_GroupBox.TabIndex = 20;
            this.g03_04_EDC_GroupBox.TabStop = false;
            this.g03_04_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label259.Location = new System.Drawing.Point(51, 71);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(118, 31);
            this.label259.TabIndex = 2;
            this.label259.Text = "glass_id :";
            this.label259.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox9
            // 
            this.g03_04_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g03_04_TextBox9.Name = "g03_04_TextBox9";
            this.g03_04_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox9.TabIndex = 19;
            this.g03_04_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label260.Location = new System.Drawing.Point(17, 161);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(152, 31);
            this.label260.TabIndex = 3;
            this.label260.Text = "product_id :";
            this.label260.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox8
            // 
            this.g03_04_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g03_04_TextBox8.Name = "g03_04_TextBox8";
            this.g03_04_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox8.TabIndex = 18;
            this.g03_04_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label261.Location = new System.Drawing.Point(63, 251);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(106, 31);
            this.label261.TabIndex = 4;
            this.label261.Text = "eqp_id :";
            this.label261.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox7
            // 
            this.g03_04_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g03_04_TextBox7.Name = "g03_04_TextBox7";
            this.g03_04_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox7.TabIndex = 17;
            this.g03_04_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label262.Location = new System.Drawing.Point(10, 341);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(159, 31);
            this.label262.TabIndex = 5;
            this.label262.Text = "sub_eqp_id :";
            this.label262.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox6
            // 
            this.g03_04_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g03_04_TextBox6.Name = "g03_04_TextBox6";
            this.g03_04_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox6.TabIndex = 16;
            this.g03_04_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label263.Location = new System.Drawing.Point(71, 431);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(98, 31);
            this.label263.TabIndex = 6;
            this.label263.Text = "owner :";
            this.label263.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox5
            // 
            this.g03_04_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g03_04_TextBox5.Name = "g03_04_TextBox5";
            this.g03_04_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox5.TabIndex = 15;
            this.g03_04_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label264.Location = new System.Drawing.Point(409, 71);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(132, 31);
            this.label264.TabIndex = 7;
            this.label264.Text = "recipe_id :";
            this.label264.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox4
            // 
            this.g03_04_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g03_04_TextBox4.Name = "g03_04_TextBox4";
            this.g03_04_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox4.TabIndex = 14;
            this.g03_04_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label265.Location = new System.Drawing.Point(402, 161);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(139, 31);
            this.label265.TabIndex = 8;
            this.label265.Text = "operation :";
            this.label265.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox3
            // 
            this.g03_04_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g03_04_TextBox3.Name = "g03_04_TextBox3";
            this.g03_04_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox3.TabIndex = 13;
            this.g03_04_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label266.Location = new System.Drawing.Point(413, 251);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(128, 31);
            this.label266.TabIndex = 9;
            this.label266.Text = "chamber :";
            this.label266.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox2
            // 
            this.g03_04_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g03_04_TextBox2.Name = "g03_04_TextBox2";
            this.g03_04_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox2.TabIndex = 12;
            this.g03_04_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label267.Location = new System.Drawing.Point(415, 341);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(126, 31);
            this.label267.TabIndex = 10;
            this.label267.Text = "operator :";
            this.label267.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_04_TextBox1
            // 
            this.g03_04_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g03_04_TextBox1.Name = "g03_04_TextBox1";
            this.g03_04_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g03_04_TextBox1.TabIndex = 11;
            this.g03_04_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g03_05_TabPage
            // 
            this.g03_05_TabPage.Controls.Add(this.panel22);
            this.g03_05_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g03_05_TabPage.Name = "g03_05_TabPage";
            this.g03_05_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g03_05_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g03_05_TabPage.TabIndex = 4;
            this.g03_05_TabPage.Text = "Unit05";
            this.g03_05_TabPage.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.g03_05_Export_GroupBox);
            this.panel22.Controls.Add(this.g03_05_FFU_GroupBox);
            this.panel22.Controls.Add(this.g03_05_EDC_GroupBox);
            this.panel22.Location = new System.Drawing.Point(6, 6);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1309, 659);
            this.panel22.TabIndex = 2;
            // 
            // g03_05_Export_GroupBox
            // 
            this.g03_05_Export_GroupBox.Controls.Add(this.g03_05_Folder_Label);
            this.g03_05_Export_GroupBox.Controls.Add(this.g03_05_selectFolder_Button);
            this.g03_05_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g03_05_Export_GroupBox.Name = "g03_05_Export_GroupBox";
            this.g03_05_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g03_05_Export_GroupBox.TabIndex = 24;
            this.g03_05_Export_GroupBox.TabStop = false;
            this.g03_05_Export_GroupBox.Text = "匯出路徑";
            // 
            // g03_05_Folder_Label
            // 
            this.g03_05_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_05_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g03_05_Folder_Label.Name = "g03_05_Folder_Label";
            this.g03_05_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g03_05_Folder_Label.TabIndex = 8;
            this.g03_05_Folder_Label.Text = "請選擇匯出路徑";
            this.g03_05_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g03_05_selectFolder_Button
            // 
            this.g03_05_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g03_05_selectFolder_Button.Name = "g03_05_selectFolder_Button";
            this.g03_05_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g03_05_selectFolder_Button.TabIndex = 7;
            this.g03_05_selectFolder_Button.Text = "選擇匯出路徑";
            this.g03_05_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g03_05_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g03_05_FFU_GroupBox
            // 
            this.g03_05_FFU_GroupBox.Controls.Add(this.g03_05_IsSensor_CheckBox);
            this.g03_05_FFU_GroupBox.Controls.Add(this.label269);
            this.g03_05_FFU_GroupBox.Controls.Add(this.g03_05_Particle_ID_TextBox);
            this.g03_05_FFU_GroupBox.Controls.Add(this.g03_05_Particle_Quanity_TextBox);
            this.g03_05_FFU_GroupBox.Controls.Add(this.label270);
            this.g03_05_FFU_GroupBox.Controls.Add(this.g03_05_DPS_ID_TextBox);
            this.g03_05_FFU_GroupBox.Controls.Add(this.g03_05_DPS_Quanity_TextBox);
            this.g03_05_FFU_GroupBox.Controls.Add(this.label271);
            this.g03_05_FFU_GroupBox.Controls.Add(this.g03_05_FFU_Quanity_TextBox);
            this.g03_05_FFU_GroupBox.Controls.Add(this.panel23);
            this.g03_05_FFU_GroupBox.Controls.Add(this.label275);
            this.g03_05_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g03_05_FFU_GroupBox.Name = "g03_05_FFU_GroupBox";
            this.g03_05_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g03_05_FFU_GroupBox.TabIndex = 21;
            this.g03_05_FFU_GroupBox.TabStop = false;
            this.g03_05_FFU_GroupBox.Text = "FFU";
            // 
            // g03_05_IsSensor_CheckBox
            // 
            this.g03_05_IsSensor_CheckBox.AutoSize = true;
            this.g03_05_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_05_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g03_05_IsSensor_CheckBox.Name = "g03_05_IsSensor_CheckBox";
            this.g03_05_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g03_05_IsSensor_CheckBox.TabIndex = 36;
            this.g03_05_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g03_05_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label269.Location = new System.Drawing.Point(212, 129);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(28, 31);
            this.label269.TabIndex = 25;
            this.label269.Text = "1";
            this.label269.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_Particle_ID_TextBox
            // 
            this.g03_05_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g03_05_Particle_ID_TextBox.Name = "g03_05_Particle_ID_TextBox";
            this.g03_05_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_05_Particle_ID_TextBox.TabIndex = 24;
            this.g03_05_Particle_ID_TextBox.Text = "65";
            this.g03_05_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_05_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g03_05_Particle_Quanity_TextBox
            // 
            this.g03_05_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g03_05_Particle_Quanity_TextBox.Name = "g03_05_Particle_Quanity_TextBox";
            this.g03_05_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_05_Particle_Quanity_TextBox.TabIndex = 23;
            this.g03_05_Particle_Quanity_TextBox.Text = "1";
            this.g03_05_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label270.Location = new System.Drawing.Point(28, 273);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(100, 31);
            this.label270.TabIndex = 22;
            this.label270.Text = "Particle";
            this.label270.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_DPS_ID_TextBox
            // 
            this.g03_05_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g03_05_DPS_ID_TextBox.Name = "g03_05_DPS_ID_TextBox";
            this.g03_05_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_05_DPS_ID_TextBox.TabIndex = 21;
            this.g03_05_DPS_ID_TextBox.Text = "64";
            this.g03_05_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_05_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g03_05_DPS_Quanity_TextBox
            // 
            this.g03_05_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g03_05_DPS_Quanity_TextBox.Name = "g03_05_DPS_Quanity_TextBox";
            this.g03_05_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_05_DPS_Quanity_TextBox.TabIndex = 20;
            this.g03_05_DPS_Quanity_TextBox.Text = "1";
            this.g03_05_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label271.Location = new System.Drawing.Point(47, 202);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(62, 31);
            this.label271.TabIndex = 19;
            this.label271.Text = "DPS";
            this.label271.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_FFU_Quanity_TextBox
            // 
            this.g03_05_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g03_05_FFU_Quanity_TextBox.Name = "g03_05_FFU_Quanity_TextBox";
            this.g03_05_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_05_FFU_Quanity_TextBox.TabIndex = 17;
            this.g03_05_FFU_Quanity_TextBox.Text = "1";
            this.g03_05_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.label272);
            this.panel23.Controls.Add(this.label273);
            this.panel23.Controls.Add(this.label274);
            this.panel23.Location = new System.Drawing.Point(25, 33);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(445, 69);
            this.panel23.TabIndex = 4;
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label272.Location = new System.Drawing.Point(21, 16);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(62, 31);
            this.label272.TabIndex = 0;
            this.label272.Text = "格式";
            this.label272.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label273.Location = new System.Drawing.Point(148, 16);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(110, 31);
            this.label273.TabIndex = 1;
            this.label273.Text = "起始站號";
            this.label273.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label274.Location = new System.Drawing.Point(316, 16);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(110, 31);
            this.label274.TabIndex = 2;
            this.label274.Text = "輸出數量";
            this.label274.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label275.Location = new System.Drawing.Point(40, 133);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(88, 31);
            this.label275.TabIndex = 3;
            this.label275.Text = "SPEED";
            this.label275.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_EDC_GroupBox
            // 
            this.g03_05_EDC_GroupBox.Controls.Add(this.label276);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox9);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label277);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox8);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label278);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox7);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label279);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox6);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label280);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox5);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label281);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox4);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label282);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox3);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label283);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox2);
            this.g03_05_EDC_GroupBox.Controls.Add(this.label284);
            this.g03_05_EDC_GroupBox.Controls.Add(this.g03_05_TextBox1);
            this.g03_05_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g03_05_EDC_GroupBox.Name = "g03_05_EDC_GroupBox";
            this.g03_05_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g03_05_EDC_GroupBox.TabIndex = 20;
            this.g03_05_EDC_GroupBox.TabStop = false;
            this.g03_05_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label276.Location = new System.Drawing.Point(51, 71);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(118, 31);
            this.label276.TabIndex = 2;
            this.label276.Text = "glass_id :";
            this.label276.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox9
            // 
            this.g03_05_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g03_05_TextBox9.Name = "g03_05_TextBox9";
            this.g03_05_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox9.TabIndex = 19;
            this.g03_05_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label277.Location = new System.Drawing.Point(17, 161);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(152, 31);
            this.label277.TabIndex = 3;
            this.label277.Text = "product_id :";
            this.label277.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox8
            // 
            this.g03_05_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g03_05_TextBox8.Name = "g03_05_TextBox8";
            this.g03_05_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox8.TabIndex = 18;
            this.g03_05_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label278.Location = new System.Drawing.Point(63, 251);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(106, 31);
            this.label278.TabIndex = 4;
            this.label278.Text = "eqp_id :";
            this.label278.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox7
            // 
            this.g03_05_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g03_05_TextBox7.Name = "g03_05_TextBox7";
            this.g03_05_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox7.TabIndex = 17;
            this.g03_05_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label279.Location = new System.Drawing.Point(10, 341);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(159, 31);
            this.label279.TabIndex = 5;
            this.label279.Text = "sub_eqp_id :";
            this.label279.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox6
            // 
            this.g03_05_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g03_05_TextBox6.Name = "g03_05_TextBox6";
            this.g03_05_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox6.TabIndex = 16;
            this.g03_05_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label280.Location = new System.Drawing.Point(71, 431);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(98, 31);
            this.label280.TabIndex = 6;
            this.label280.Text = "owner :";
            this.label280.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox5
            // 
            this.g03_05_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g03_05_TextBox5.Name = "g03_05_TextBox5";
            this.g03_05_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox5.TabIndex = 15;
            this.g03_05_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label281.Location = new System.Drawing.Point(409, 71);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(132, 31);
            this.label281.TabIndex = 7;
            this.label281.Text = "recipe_id :";
            this.label281.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox4
            // 
            this.g03_05_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g03_05_TextBox4.Name = "g03_05_TextBox4";
            this.g03_05_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox4.TabIndex = 14;
            this.g03_05_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label282.Location = new System.Drawing.Point(402, 161);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(139, 31);
            this.label282.TabIndex = 8;
            this.label282.Text = "operation :";
            this.label282.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox3
            // 
            this.g03_05_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g03_05_TextBox3.Name = "g03_05_TextBox3";
            this.g03_05_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox3.TabIndex = 13;
            this.g03_05_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label283.Location = new System.Drawing.Point(413, 251);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(128, 31);
            this.label283.TabIndex = 9;
            this.label283.Text = "chamber :";
            this.label283.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox2
            // 
            this.g03_05_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g03_05_TextBox2.Name = "g03_05_TextBox2";
            this.g03_05_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox2.TabIndex = 12;
            this.g03_05_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label284.Location = new System.Drawing.Point(415, 341);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(126, 31);
            this.label284.TabIndex = 10;
            this.label284.Text = "operator :";
            this.label284.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_05_TextBox1
            // 
            this.g03_05_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g03_05_TextBox1.Name = "g03_05_TextBox1";
            this.g03_05_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g03_05_TextBox1.TabIndex = 11;
            this.g03_05_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // g03_06_TabPage
            // 
            this.g03_06_TabPage.Controls.Add(this.panel24);
            this.g03_06_TabPage.Location = new System.Drawing.Point(4, 34);
            this.g03_06_TabPage.Name = "g03_06_TabPage";
            this.g03_06_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.g03_06_TabPage.Size = new System.Drawing.Size(1324, 671);
            this.g03_06_TabPage.TabIndex = 5;
            this.g03_06_TabPage.Text = "Unit06";
            this.g03_06_TabPage.UseVisualStyleBackColor = true;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.g03_06_Export_GroupBox);
            this.panel24.Controls.Add(this.g03_06_FFU_GroupBox);
            this.panel24.Controls.Add(this.g03_06_EDC_GroupBox);
            this.panel24.Location = new System.Drawing.Point(6, 6);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1307, 659);
            this.panel24.TabIndex = 2;
            // 
            // g03_06_Export_GroupBox
            // 
            this.g03_06_Export_GroupBox.Controls.Add(this.g03_06_Folder_Label);
            this.g03_06_Export_GroupBox.Controls.Add(this.g03_06_selectFolder_Button);
            this.g03_06_Export_GroupBox.Location = new System.Drawing.Point(20, 3);
            this.g03_06_Export_GroupBox.Name = "g03_06_Export_GroupBox";
            this.g03_06_Export_GroupBox.Size = new System.Drawing.Size(1272, 123);
            this.g03_06_Export_GroupBox.TabIndex = 23;
            this.g03_06_Export_GroupBox.TabStop = false;
            this.g03_06_Export_GroupBox.Text = "匯出路徑";
            // 
            // g03_06_Folder_Label
            // 
            this.g03_06_Folder_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_06_Folder_Label.Location = new System.Drawing.Point(339, 36);
            this.g03_06_Folder_Label.Name = "g03_06_Folder_Label";
            this.g03_06_Folder_Label.Size = new System.Drawing.Size(913, 54);
            this.g03_06_Folder_Label.TabIndex = 8;
            this.g03_06_Folder_Label.Text = "請選擇匯出路徑";
            this.g03_06_Folder_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // g03_06_selectFolder_Button
            // 
            this.g03_06_selectFolder_Button.Location = new System.Drawing.Point(175, 36);
            this.g03_06_selectFolder_Button.Name = "g03_06_selectFolder_Button";
            this.g03_06_selectFolder_Button.Size = new System.Drawing.Size(158, 54);
            this.g03_06_selectFolder_Button.TabIndex = 7;
            this.g03_06_selectFolder_Button.Text = "選擇匯出路徑";
            this.g03_06_selectFolder_Button.UseVisualStyleBackColor = true;
            this.g03_06_selectFolder_Button.Click += new System.EventHandler(this.SelectFolder_Button_Click);
            // 
            // g03_06_FFU_GroupBox
            // 
            this.g03_06_FFU_GroupBox.Controls.Add(this.g03_06_IsSensor_CheckBox);
            this.g03_06_FFU_GroupBox.Controls.Add(this.label286);
            this.g03_06_FFU_GroupBox.Controls.Add(this.g03_06_Particle_ID_TextBox);
            this.g03_06_FFU_GroupBox.Controls.Add(this.g03_06_Particle_Quanity_TextBox);
            this.g03_06_FFU_GroupBox.Controls.Add(this.label287);
            this.g03_06_FFU_GroupBox.Controls.Add(this.g03_06_DPS_ID_TextBox);
            this.g03_06_FFU_GroupBox.Controls.Add(this.g03_06_DPS_Quanity_TextBox);
            this.g03_06_FFU_GroupBox.Controls.Add(this.label288);
            this.g03_06_FFU_GroupBox.Controls.Add(this.g03_06_FFU_Quanity_TextBox);
            this.g03_06_FFU_GroupBox.Controls.Add(this.panel25);
            this.g03_06_FFU_GroupBox.Controls.Add(this.label292);
            this.g03_06_FFU_GroupBox.Location = new System.Drawing.Point(802, 132);
            this.g03_06_FFU_GroupBox.Name = "g03_06_FFU_GroupBox";
            this.g03_06_FFU_GroupBox.Size = new System.Drawing.Size(490, 503);
            this.g03_06_FFU_GroupBox.TabIndex = 21;
            this.g03_06_FFU_GroupBox.TabStop = false;
            this.g03_06_FFU_GroupBox.Text = "FFU";
            // 
            // g03_06_IsSensor_CheckBox
            // 
            this.g03_06_IsSensor_CheckBox.AutoSize = true;
            this.g03_06_IsSensor_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.g03_06_IsSensor_CheckBox.Location = new System.Drawing.Point(34, 342);
            this.g03_06_IsSensor_CheckBox.Name = "g03_06_IsSensor_CheckBox";
            this.g03_06_IsSensor_CheckBox.Size = new System.Drawing.Size(228, 35);
            this.g03_06_IsSensor_CheckBox.TabIndex = 37;
            this.g03_06_IsSensor_CheckBox.Text = "包含壓差之控制器";
            this.g03_06_IsSensor_CheckBox.UseVisualStyleBackColor = true;
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label286.Location = new System.Drawing.Point(212, 129);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(28, 31);
            this.label286.TabIndex = 25;
            this.label286.Text = "1";
            this.label286.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_Particle_ID_TextBox
            // 
            this.g03_06_Particle_ID_TextBox.Location = new System.Drawing.Point(165, 270);
            this.g03_06_Particle_ID_TextBox.Name = "g03_06_Particle_ID_TextBox";
            this.g03_06_Particle_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_06_Particle_ID_TextBox.TabIndex = 24;
            this.g03_06_Particle_ID_TextBox.Text = "65";
            this.g03_06_Particle_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_06_Particle_ID_TextBox.Leave += new System.EventHandler(this.Particle_ID_TextBox_Leave);
            // 
            // g03_06_Particle_Quanity_TextBox
            // 
            this.g03_06_Particle_Quanity_TextBox.Location = new System.Drawing.Point(335, 270);
            this.g03_06_Particle_Quanity_TextBox.Name = "g03_06_Particle_Quanity_TextBox";
            this.g03_06_Particle_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_06_Particle_Quanity_TextBox.TabIndex = 23;
            this.g03_06_Particle_Quanity_TextBox.Text = "1";
            this.g03_06_Particle_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label287.Location = new System.Drawing.Point(28, 273);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(100, 31);
            this.label287.TabIndex = 22;
            this.label287.Text = "Particle";
            this.label287.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_DPS_ID_TextBox
            // 
            this.g03_06_DPS_ID_TextBox.Location = new System.Drawing.Point(165, 199);
            this.g03_06_DPS_ID_TextBox.Name = "g03_06_DPS_ID_TextBox";
            this.g03_06_DPS_ID_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_06_DPS_ID_TextBox.TabIndex = 21;
            this.g03_06_DPS_ID_TextBox.Text = "64";
            this.g03_06_DPS_ID_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.g03_06_DPS_ID_TextBox.Leave += new System.EventHandler(this.DPS_ID_TextBox_Leave);
            // 
            // g03_06_DPS_Quanity_TextBox
            // 
            this.g03_06_DPS_Quanity_TextBox.Location = new System.Drawing.Point(335, 199);
            this.g03_06_DPS_Quanity_TextBox.Name = "g03_06_DPS_Quanity_TextBox";
            this.g03_06_DPS_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_06_DPS_Quanity_TextBox.TabIndex = 20;
            this.g03_06_DPS_Quanity_TextBox.Text = "1";
            this.g03_06_DPS_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label288.Location = new System.Drawing.Point(47, 202);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(62, 31);
            this.label288.TabIndex = 19;
            this.label288.Text = "DPS";
            this.label288.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_FFU_Quanity_TextBox
            // 
            this.g03_06_FFU_Quanity_TextBox.Location = new System.Drawing.Point(335, 130);
            this.g03_06_FFU_Quanity_TextBox.Name = "g03_06_FFU_Quanity_TextBox";
            this.g03_06_FFU_Quanity_TextBox.Size = new System.Drawing.Size(128, 34);
            this.g03_06_FFU_Quanity_TextBox.TabIndex = 17;
            this.g03_06_FFU_Quanity_TextBox.Text = "1";
            this.g03_06_FFU_Quanity_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.label289);
            this.panel25.Controls.Add(this.label290);
            this.panel25.Controls.Add(this.label291);
            this.panel25.Location = new System.Drawing.Point(25, 33);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(445, 69);
            this.panel25.TabIndex = 4;
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label289.Location = new System.Drawing.Point(21, 16);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(62, 31);
            this.label289.TabIndex = 0;
            this.label289.Text = "格式";
            this.label289.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label290.Location = new System.Drawing.Point(148, 16);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(110, 31);
            this.label290.TabIndex = 1;
            this.label290.Text = "起始站號";
            this.label290.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label291.Location = new System.Drawing.Point(316, 16);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(110, 31);
            this.label291.TabIndex = 2;
            this.label291.Text = "輸出數量";
            this.label291.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label292.Location = new System.Drawing.Point(40, 133);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(88, 31);
            this.label292.TabIndex = 3;
            this.label292.Text = "SPEED";
            this.label292.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_EDC_GroupBox
            // 
            this.g03_06_EDC_GroupBox.Controls.Add(this.label293);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox9);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label294);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox8);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label295);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox7);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label296);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox6);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label297);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox5);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label298);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox4);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label299);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox3);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label300);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox2);
            this.g03_06_EDC_GroupBox.Controls.Add(this.label301);
            this.g03_06_EDC_GroupBox.Controls.Add(this.g03_06_TextBox1);
            this.g03_06_EDC_GroupBox.Location = new System.Drawing.Point(20, 132);
            this.g03_06_EDC_GroupBox.Name = "g03_06_EDC_GroupBox";
            this.g03_06_EDC_GroupBox.Size = new System.Drawing.Size(776, 503);
            this.g03_06_EDC_GroupBox.TabIndex = 20;
            this.g03_06_EDC_GroupBox.TabStop = false;
            this.g03_06_EDC_GroupBox.Text = "EDC標頭";
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label293.Location = new System.Drawing.Point(51, 71);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(118, 31);
            this.label293.TabIndex = 2;
            this.label293.Text = "glass_id :";
            this.label293.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox9
            // 
            this.g03_06_TextBox9.Location = new System.Drawing.Point(547, 342);
            this.g03_06_TextBox9.Name = "g03_06_TextBox9";
            this.g03_06_TextBox9.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox9.TabIndex = 19;
            this.g03_06_TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label294.Location = new System.Drawing.Point(17, 161);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(152, 31);
            this.label294.TabIndex = 3;
            this.label294.Text = "product_id :";
            this.label294.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox8
            // 
            this.g03_06_TextBox8.Location = new System.Drawing.Point(547, 252);
            this.g03_06_TextBox8.Name = "g03_06_TextBox8";
            this.g03_06_TextBox8.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox8.TabIndex = 18;
            this.g03_06_TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label295.Location = new System.Drawing.Point(63, 251);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(106, 31);
            this.label295.TabIndex = 4;
            this.label295.Text = "eqp_id :";
            this.label295.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox7
            // 
            this.g03_06_TextBox7.Location = new System.Drawing.Point(547, 162);
            this.g03_06_TextBox7.Name = "g03_06_TextBox7";
            this.g03_06_TextBox7.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox7.TabIndex = 17;
            this.g03_06_TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label296.Location = new System.Drawing.Point(10, 341);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(159, 31);
            this.label296.TabIndex = 5;
            this.label296.Text = "sub_eqp_id :";
            this.label296.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox6
            // 
            this.g03_06_TextBox6.Location = new System.Drawing.Point(547, 72);
            this.g03_06_TextBox6.Name = "g03_06_TextBox6";
            this.g03_06_TextBox6.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox6.TabIndex = 16;
            this.g03_06_TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label297.Location = new System.Drawing.Point(71, 431);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(98, 31);
            this.label297.TabIndex = 6;
            this.label297.Text = "owner :";
            this.label297.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox5
            // 
            this.g03_06_TextBox5.Location = new System.Drawing.Point(175, 432);
            this.g03_06_TextBox5.Name = "g03_06_TextBox5";
            this.g03_06_TextBox5.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox5.TabIndex = 15;
            this.g03_06_TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label298.Location = new System.Drawing.Point(409, 71);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(132, 31);
            this.label298.TabIndex = 7;
            this.label298.Text = "recipe_id :";
            this.label298.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox4
            // 
            this.g03_06_TextBox4.Location = new System.Drawing.Point(175, 342);
            this.g03_06_TextBox4.Name = "g03_06_TextBox4";
            this.g03_06_TextBox4.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox4.TabIndex = 14;
            this.g03_06_TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label299.Location = new System.Drawing.Point(402, 161);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(139, 31);
            this.label299.TabIndex = 8;
            this.label299.Text = "operation :";
            this.label299.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox3
            // 
            this.g03_06_TextBox3.Location = new System.Drawing.Point(175, 252);
            this.g03_06_TextBox3.Name = "g03_06_TextBox3";
            this.g03_06_TextBox3.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox3.TabIndex = 13;
            this.g03_06_TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label300.Location = new System.Drawing.Point(413, 251);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(128, 31);
            this.label300.TabIndex = 9;
            this.label300.Text = "chamber :";
            this.label300.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox2
            // 
            this.g03_06_TextBox2.Location = new System.Drawing.Point(175, 162);
            this.g03_06_TextBox2.Name = "g03_06_TextBox2";
            this.g03_06_TextBox2.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox2.TabIndex = 12;
            this.g03_06_TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label301.Location = new System.Drawing.Point(415, 341);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(126, 31);
            this.label301.TabIndex = 10;
            this.label301.Text = "operator :";
            this.label301.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g03_06_TextBox1
            // 
            this.g03_06_TextBox1.Location = new System.Drawing.Point(175, 72);
            this.g03_06_TextBox1.Name = "g03_06_TextBox1";
            this.g03_06_TextBox1.Size = new System.Drawing.Size(208, 34);
            this.g03_06_TextBox1.TabIndex = 11;
            this.g03_06_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.SelectedPath = "C:\\Users\\TOPWELL\\Desktop";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1390, 907);
            this.Controls.Add(this.gateway_Tab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "INNOLUX_EDC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.g01_TabPage.ResumeLayout(false);
            this.g01_Panel.ResumeLayout(false);
            this.g01_Panel.PerformLayout();
            this.unit_Tab.ResumeLayout(false);
            this.g01_01_TabPage.ResumeLayout(false);
            this.g01_01_Panel.ResumeLayout(false);
            this.g01_01_Export_GroupBox.ResumeLayout(false);
            this.g01_01_FFU_GroupBox.ResumeLayout(false);
            this.g01_01_FFU_GroupBox.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.g01_01_EDC_GroupBox.ResumeLayout(false);
            this.g01_01_EDC_GroupBox.PerformLayout();
            this.g01_02_TabPage.ResumeLayout(false);
            this.g01_02_Panel.ResumeLayout(false);
            this.g01_02_Export_GroupBox.ResumeLayout(false);
            this.g01_02_FFU_GroupBox.ResumeLayout(false);
            this.g01_02_FFU_GroupBox.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.g01_02_EDC_GroupBox.ResumeLayout(false);
            this.g01_02_EDC_GroupBox.PerformLayout();
            this.g01_03_TabPage.ResumeLayout(false);
            this.g01_03_Panel.ResumeLayout(false);
            this.g01_03_Export_GroupBox.ResumeLayout(false);
            this.g01_03_FFU_GroupBox.ResumeLayout(false);
            this.g01_03_FFU_GroupBox.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.g01_03_EDC_GroupBox.ResumeLayout(false);
            this.g01_03_EDC_GroupBox.PerformLayout();
            this.g01_04_TabPage.ResumeLayout(false);
            this.g01_04_Panel.ResumeLayout(false);
            this.g01_04_Export_GroupBox.ResumeLayout(false);
            this.g01_04_FFU_GroupBox.ResumeLayout(false);
            this.g01_04_FFU_GroupBox.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.g01_04_EDC_GroupBox.ResumeLayout(false);
            this.g01_04_EDC_GroupBox.PerformLayout();
            this.g01_05_TabPage.ResumeLayout(false);
            this.g01_05_Panel.ResumeLayout(false);
            this.g01_05_Export_GroupBox.ResumeLayout(false);
            this.g01_05_FFU_GroupBox.ResumeLayout(false);
            this.g01_05_FFU_GroupBox.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.g01_05_EDC_GroupBox.ResumeLayout(false);
            this.g01_05_EDC_GroupBox.PerformLayout();
            this.g01_06_TabPage.ResumeLayout(false);
            this.g01_06_Panel.ResumeLayout(false);
            this.g01_06_Export_GroupBox.ResumeLayout(false);
            this.g01_06_FFU_GroupBox.ResumeLayout(false);
            this.g01_06_FFU_GroupBox.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.g01_06_EDC_GroupBox.ResumeLayout(false);
            this.g01_06_EDC_GroupBox.PerformLayout();
            this.gateway_Tab.ResumeLayout(false);
            this.g02_TabPage.ResumeLayout(false);
            this.g02_Panel.ResumeLayout(false);
            this.g02_Panel.PerformLayout();
            this.unit_Tab2.ResumeLayout(false);
            this.g02_01_TabPage.ResumeLayout(false);
            this.g02_01_Panel.ResumeLayout(false);
            this.g02_01_Export_GroupBox.ResumeLayout(false);
            this.g02_01_FFU_GroupBox.ResumeLayout(false);
            this.g02_01_FFU_GroupBox.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.g02_01_EDC_GroupBox.ResumeLayout(false);
            this.g02_01_EDC_GroupBox.PerformLayout();
            this.g02_02_TabPage.ResumeLayout(false);
            this.g02_02_Panel.ResumeLayout(false);
            this.g02_02_Export_GroupBox.ResumeLayout(false);
            this.g02_02_FFU_GroupBox.ResumeLayout(false);
            this.g02_02_FFU_GroupBox.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.g02_02_EDC_GroupBox.ResumeLayout(false);
            this.g02_02_EDC_GroupBox.PerformLayout();
            this.g02_03_TabPage.ResumeLayout(false);
            this.g02_03_Panel.ResumeLayout(false);
            this.g02_03_Export_GroupBox.ResumeLayout(false);
            this.g02_03_FFU_GroupBox.ResumeLayout(false);
            this.g02_03_FFU_GroupBox.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.g02_03_EDC_GroupBox.ResumeLayout(false);
            this.g02_03_EDC_GroupBox.PerformLayout();
            this.g02_04_TabPage.ResumeLayout(false);
            this.g02_04_Panel.ResumeLayout(false);
            this.g02_04_Export_GroupBox.ResumeLayout(false);
            this.g02_04_FFU_GroupBox.ResumeLayout(false);
            this.g02_04_FFU_GroupBox.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.g02_04_EDC_GroupBox.ResumeLayout(false);
            this.g02_04_EDC_GroupBox.PerformLayout();
            this.g02_05_TabPage.ResumeLayout(false);
            this.g02_05_Panel.ResumeLayout(false);
            this.g02_05_Export_GroupBox.ResumeLayout(false);
            this.g02_05_FFU_GroupBox.ResumeLayout(false);
            this.g02_05_FFU_GroupBox.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.g02_05_EDC_GroupBox.ResumeLayout(false);
            this.g02_05_EDC_GroupBox.PerformLayout();
            this.g02_06_TabPage.ResumeLayout(false);
            this.g02_06_Panel.ResumeLayout(false);
            this.g02_06_Export_GroupBox.ResumeLayout(false);
            this.g02_06_FFU_GroupBox.ResumeLayout(false);
            this.g02_06_FFU_GroupBox.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.g02_06_EDC_GroupBox.ResumeLayout(false);
            this.g02_06_EDC_GroupBox.PerformLayout();
            this.g03_TabPage.ResumeLayout(false);
            this.g03_Panel.ResumeLayout(false);
            this.g03_Panel.PerformLayout();
            this.unit_Tab3.ResumeLayout(false);
            this.g03_01_TabPage.ResumeLayout(false);
            this.g03_01_Panel.ResumeLayout(false);
            this.g03_01_Export_GroupBox.ResumeLayout(false);
            this.g03_01_FFU_GroupBox.ResumeLayout(false);
            this.g03_01_FFU_GroupBox.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.g03_01_EDC_GroupBox.ResumeLayout(false);
            this.g03_01_EDC_GroupBox.PerformLayout();
            this.g03_02_TabPage.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.g03_02_Export_GroupBox.ResumeLayout(false);
            this.g03_02_FFU_GroupBox.ResumeLayout(false);
            this.g03_02_FFU_GroupBox.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.g03_02_EDC_GroupBox.ResumeLayout(false);
            this.g03_02_EDC_GroupBox.PerformLayout();
            this.g03_03_TabPage.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.g03_03_Export_GroupBox.ResumeLayout(false);
            this.g03_03_FFU_GroupBox.ResumeLayout(false);
            this.g03_03_FFU_GroupBox.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.g03_03_EDC_GroupBox.ResumeLayout(false);
            this.g03_03_EDC_GroupBox.PerformLayout();
            this.g03_04_TabPage.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.g03_04_Export_GroupBox.ResumeLayout(false);
            this.g03_04_FFU_GroupBox.ResumeLayout(false);
            this.g03_04_FFU_GroupBox.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.g03_04_EDC_GroupBox.ResumeLayout(false);
            this.g03_04_EDC_GroupBox.PerformLayout();
            this.g03_05_TabPage.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.g03_05_Export_GroupBox.ResumeLayout(false);
            this.g03_05_FFU_GroupBox.ResumeLayout(false);
            this.g03_05_FFU_GroupBox.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.g03_05_EDC_GroupBox.ResumeLayout(false);
            this.g03_05_EDC_GroupBox.PerformLayout();
            this.g03_06_TabPage.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.g03_06_Export_GroupBox.ResumeLayout(false);
            this.g03_06_FFU_GroupBox.ResumeLayout(false);
            this.g03_06_FFU_GroupBox.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.g03_06_EDC_GroupBox.ResumeLayout(false);
            this.g03_06_EDC_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage g01_TabPage;
        private System.Windows.Forms.TabControl unit_Tab;
        private System.Windows.Forms.TabPage g01_01_TabPage;
        private System.Windows.Forms.TabControl gateway_Tab;
        private System.Windows.Forms.Panel g01_Panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label g01_connect_Label;
        private System.Windows.Forms.Button g01_connect_Button;
        private System.Windows.Forms.Panel g01_01_Panel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox g01_01_TextBox1;
        private System.Windows.Forms.TextBox g01_01_TextBox9;
        private System.Windows.Forms.TextBox g01_01_TextBox8;
        private System.Windows.Forms.TextBox g01_01_TextBox7;
        private System.Windows.Forms.TextBox g01_01_TextBox6;
        private System.Windows.Forms.TextBox g01_01_TextBox5;
        private System.Windows.Forms.TextBox g01_01_TextBox4;
        private System.Windows.Forms.TextBox g01_01_TextBox3;
        private System.Windows.Forms.TextBox g01_01_TextBox2;
        private System.Windows.Forms.GroupBox g01_01_EDC_GroupBox;
        private System.Windows.Forms.GroupBox g01_01_FFU_GroupBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox g01_01_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_01_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox g01_01_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_01_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox g01_01_FFU_Quanity_TextBox;
        private System.Windows.Forms.TextBox g01_IP_TextBox;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox g01_Time_comboBox;
        private System.Windows.Forms.TabPage g01_02_TabPage;
        private System.Windows.Forms.Panel g01_02_Panel;
        private System.Windows.Forms.GroupBox g01_02_FFU_GroupBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox g01_02_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_02_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox g01_02_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_02_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox g01_02_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox g01_02_EDC_GroupBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox g01_02_TextBox9;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox g01_02_TextBox8;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox g01_02_TextBox7;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox g01_02_TextBox6;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox g01_02_TextBox5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox g01_02_TextBox4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox g01_02_TextBox3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox g01_02_TextBox2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox g01_02_TextBox1;
        private System.Windows.Forms.TabPage g01_03_TabPage;
        private System.Windows.Forms.Panel g01_03_Panel;
        private System.Windows.Forms.GroupBox g01_03_FFU_GroupBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox g01_03_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_03_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox g01_03_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_03_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox g01_03_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox g01_03_EDC_GroupBox;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox g01_03_TextBox9;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox g01_03_TextBox8;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox g01_03_TextBox7;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox g01_03_TextBox6;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox g01_03_TextBox5;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox g01_03_TextBox4;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox g01_03_TextBox3;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox g01_03_TextBox2;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox g01_03_TextBox1;
        private System.Windows.Forms.TabPage g01_04_TabPage;
        private System.Windows.Forms.Panel g01_04_Panel;
        private System.Windows.Forms.GroupBox g01_04_FFU_GroupBox;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox g01_04_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_04_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox g01_04_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_04_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox g01_04_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox g01_04_EDC_GroupBox;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox g01_04_TextBox9;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox g01_04_TextBox8;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox g01_04_TextBox7;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox g01_04_TextBox6;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox g01_04_TextBox5;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox g01_04_TextBox4;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox g01_04_TextBox3;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox g01_04_TextBox2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox g01_04_TextBox1;
        private System.Windows.Forms.TabPage g01_05_TabPage;
        private System.Windows.Forms.Panel g01_05_Panel;
        private System.Windows.Forms.GroupBox g01_05_FFU_GroupBox;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox g01_05_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_05_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox g01_05_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_05_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox g01_05_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.GroupBox g01_05_EDC_GroupBox;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox g01_05_TextBox9;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox g01_05_TextBox8;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox g01_05_TextBox7;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox g01_05_TextBox6;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox g01_05_TextBox5;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox g01_05_TextBox4;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox g01_05_TextBox3;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox g01_05_TextBox2;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox g01_05_TextBox1;
        private System.Windows.Forms.TabPage g01_06_TabPage;
        private System.Windows.Forms.Panel g01_06_Panel;
        private System.Windows.Forms.GroupBox g01_06_FFU_GroupBox;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox g01_06_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g01_06_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox g01_06_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g01_06_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox g01_06_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.GroupBox g01_06_EDC_GroupBox;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox g01_06_TextBox9;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox g01_06_TextBox8;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox g01_06_TextBox7;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox g01_06_TextBox6;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox g01_06_TextBox5;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox g01_06_TextBox4;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox g01_06_TextBox3;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox g01_06_TextBox2;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox g01_06_TextBox1;
        private System.Windows.Forms.TabPage g02_TabPage;
        private System.Windows.Forms.Panel g02_Panel;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.ComboBox g02_Time_comboBox;
        private System.Windows.Forms.Button g02_connect_Button;
        private System.Windows.Forms.Label g02_connect_Label;
        private System.Windows.Forms.TextBox g02_IP_TextBox;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TabControl unit_Tab2;
        private System.Windows.Forms.TabPage g02_01_TabPage;
        private System.Windows.Forms.Panel g02_01_Panel;
        private System.Windows.Forms.GroupBox g02_01_FFU_GroupBox;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox g02_01_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g02_01_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox g02_01_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g02_01_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox g02_01_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.GroupBox g02_01_EDC_GroupBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox g02_01_TextBox9;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox g02_01_TextBox8;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox g02_01_TextBox7;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox g02_01_TextBox6;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox g02_01_TextBox5;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox g02_01_TextBox4;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox g02_01_TextBox3;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox g02_01_TextBox2;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox g02_01_TextBox1;
        private System.Windows.Forms.TabPage g02_02_TabPage;
        private System.Windows.Forms.Panel g02_02_Panel;
        private System.Windows.Forms.GroupBox g02_02_FFU_GroupBox;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox g02_02_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g02_02_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox g02_02_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g02_02_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox g02_02_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.GroupBox g02_02_EDC_GroupBox;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.TextBox g02_02_TextBox9;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox g02_02_TextBox8;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.TextBox g02_02_TextBox7;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.TextBox g02_02_TextBox6;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox g02_02_TextBox5;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.TextBox g02_02_TextBox4;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox g02_02_TextBox3;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.TextBox g02_02_TextBox2;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox g02_02_TextBox1;
        private System.Windows.Forms.TabPage g02_03_TabPage;
        private System.Windows.Forms.Panel g02_03_Panel;
        private System.Windows.Forms.GroupBox g02_03_FFU_GroupBox;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TextBox g02_03_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g02_03_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox g02_03_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g02_03_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox g02_03_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.GroupBox g02_03_EDC_GroupBox;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.TextBox g02_03_TextBox9;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TextBox g02_03_TextBox8;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.TextBox g02_03_TextBox7;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TextBox g02_03_TextBox6;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.TextBox g02_03_TextBox5;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.TextBox g02_03_TextBox4;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.TextBox g02_03_TextBox3;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.TextBox g02_03_TextBox2;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.TextBox g02_03_TextBox1;
        private System.Windows.Forms.TabPage g02_04_TabPage;
        private System.Windows.Forms.Panel g02_04_Panel;
        private System.Windows.Forms.GroupBox g02_04_FFU_GroupBox;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.TextBox g02_04_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g02_04_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.TextBox g02_04_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g02_04_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.TextBox g02_04_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.GroupBox g02_04_EDC_GroupBox;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox g02_04_TextBox9;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.TextBox g02_04_TextBox8;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.TextBox g02_04_TextBox7;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox g02_04_TextBox6;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.TextBox g02_04_TextBox5;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.TextBox g02_04_TextBox4;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.TextBox g02_04_TextBox3;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TextBox g02_04_TextBox2;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.TextBox g02_04_TextBox1;
        private System.Windows.Forms.TabPage g02_05_TabPage;
        private System.Windows.Forms.Panel g02_05_Panel;
        private System.Windows.Forms.GroupBox g02_05_FFU_GroupBox;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.TextBox g02_05_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g02_05_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.TextBox g02_05_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g02_05_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.TextBox g02_05_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.GroupBox g02_05_EDC_GroupBox;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.TextBox g02_05_TextBox9;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.TextBox g02_05_TextBox8;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.TextBox g02_05_TextBox7;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.TextBox g02_05_TextBox6;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.TextBox g02_05_TextBox5;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.TextBox g02_05_TextBox4;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.TextBox g02_05_TextBox3;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.TextBox g02_05_TextBox2;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.TextBox g02_05_TextBox1;
        private System.Windows.Forms.TabPage g02_06_TabPage;
        private System.Windows.Forms.Panel g02_06_Panel;
        private System.Windows.Forms.GroupBox g02_06_FFU_GroupBox;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.TextBox g02_06_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g02_06_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.TextBox g02_06_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g02_06_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.TextBox g02_06_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.GroupBox g02_06_EDC_GroupBox;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.TextBox g02_06_TextBox9;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.TextBox g02_06_TextBox8;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.TextBox g02_06_TextBox7;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.TextBox g02_06_TextBox6;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.TextBox g02_06_TextBox5;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.TextBox g02_06_TextBox4;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.TextBox g02_06_TextBox3;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.TextBox g02_06_TextBox2;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.TextBox g02_06_TextBox1;
        private System.Windows.Forms.GroupBox g01_01_Export_GroupBox;
        private System.Windows.Forms.Label g01_01_Folder_Label;
        private System.Windows.Forms.Button g01_01_selectFolder_Button;
        private System.Windows.Forms.GroupBox g01_02_Export_GroupBox;
        private System.Windows.Forms.Label g01_02_Folder_Label;
        private System.Windows.Forms.Button g01_02_selectFolder_Button;
        private System.Windows.Forms.GroupBox g01_03_Export_GroupBox;
        private System.Windows.Forms.Label g01_03_Folder_Label;
        private System.Windows.Forms.Button g01_03_selectFolder_Button;
        private System.Windows.Forms.GroupBox g01_04_Export_GroupBox;
        private System.Windows.Forms.Label g01_04_Folder_Label;
        private System.Windows.Forms.Button g01_04_selectFolder_Button;
        private System.Windows.Forms.GroupBox g01_05_Export_GroupBox;
        private System.Windows.Forms.Label g01_05_Folder_Label;
        private System.Windows.Forms.Button g01_05_selectFolder_Button;
        private System.Windows.Forms.GroupBox g01_06_Export_GroupBox;
        private System.Windows.Forms.Label g01_06_Folder_Label;
        private System.Windows.Forms.Button g01_06_selectFolder_Button;
        private System.Windows.Forms.GroupBox g02_03_Export_GroupBox;
        private System.Windows.Forms.Label g02_03_Folder_Label;
        private System.Windows.Forms.Button g02_03_selectFolder_Button;
        private System.Windows.Forms.GroupBox g02_04_Export_GroupBox;
        private System.Windows.Forms.Label g02_04_Folder_Label;
        private System.Windows.Forms.Button g02_04_selectFolder_Button;
        private System.Windows.Forms.GroupBox g02_05_Export_GroupBox;
        private System.Windows.Forms.Label g02_05_Folder_Label;
        private System.Windows.Forms.Button g02_05_selectFolder_Button;
        private System.Windows.Forms.GroupBox g02_06_Export_GroupBox;
        private System.Windows.Forms.Label g02_06_Folder_Label;
        private System.Windows.Forms.Button g02_06_selectFolder_Button;
        private System.Windows.Forms.GroupBox g02_01_Export_GroupBox;
        private System.Windows.Forms.Label g02_01_Folder_Label;
        private System.Windows.Forms.Button g02_01_selectFolder_Button;
        private System.Windows.Forms.GroupBox g02_02_Export_GroupBox;
        private System.Windows.Forms.Label g02_02_Folder_Label;
        private System.Windows.Forms.Button g02_02_selectFolder_Button;
        private System.Windows.Forms.CheckBox g01_01_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g01_02_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g01_03_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g01_04_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g01_05_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g01_06_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g02_01_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g02_02_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g02_03_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g02_04_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g02_05_IsSensor_CheckBox;
        private System.Windows.Forms.CheckBox g02_06_IsSensor_CheckBox;
        private System.Windows.Forms.TabPage g03_TabPage;
        private System.Windows.Forms.Panel g03_Panel;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ComboBox g03_Time_comboBox;
        private System.Windows.Forms.Button g03_connect_Button;
        private System.Windows.Forms.Label g03_connect_Label;
        private System.Windows.Forms.TextBox g03_IP_TextBox;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.TabControl unit_Tab3;
        private System.Windows.Forms.TabPage g03_01_TabPage;
        private System.Windows.Forms.Panel g03_01_Panel;
        private System.Windows.Forms.GroupBox g03_01_Export_GroupBox;
        private System.Windows.Forms.Label g03_01_Folder_Label;
        private System.Windows.Forms.Button g03_01_selectFolder_Button;
        private System.Windows.Forms.GroupBox g03_01_FFU_GroupBox;
        private System.Windows.Forms.CheckBox g03_01_IsSensor_CheckBox;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.TextBox g03_01_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g03_01_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.TextBox g03_01_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g03_01_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.TextBox g03_01_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.GroupBox g03_01_EDC_GroupBox;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.TextBox g03_01_TextBox9;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.TextBox g03_01_TextBox8;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.TextBox g03_01_TextBox7;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.TextBox g03_01_TextBox6;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.TextBox g03_01_TextBox5;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.TextBox g03_01_TextBox4;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.TextBox g03_01_TextBox3;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.TextBox g03_01_TextBox2;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.TextBox g03_01_TextBox1;
        private System.Windows.Forms.TabPage g03_02_TabPage;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.GroupBox g03_02_Export_GroupBox;
        private System.Windows.Forms.Label g03_02_Folder_Label;
        private System.Windows.Forms.Button g03_02_selectFolder_Button;
        private System.Windows.Forms.GroupBox g03_02_FFU_GroupBox;
        private System.Windows.Forms.CheckBox g03_02_IsSensor_CheckBox;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.TextBox g03_02_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g03_02_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.TextBox g03_02_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g03_02_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.TextBox g03_02_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.GroupBox g03_02_EDC_GroupBox;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.TextBox g03_02_TextBox9;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.TextBox g03_02_TextBox8;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.TextBox g03_02_TextBox7;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.TextBox g03_02_TextBox6;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.TextBox g03_02_TextBox5;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.TextBox g03_02_TextBox4;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.TextBox g03_02_TextBox3;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.TextBox g03_02_TextBox2;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.TextBox g03_02_TextBox1;
        private System.Windows.Forms.TabPage g03_03_TabPage;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.GroupBox g03_03_Export_GroupBox;
        private System.Windows.Forms.Label g03_03_Folder_Label;
        private System.Windows.Forms.Button g03_03_selectFolder_Button;
        private System.Windows.Forms.GroupBox g03_03_FFU_GroupBox;
        private System.Windows.Forms.CheckBox g03_03_IsSensor_CheckBox;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.TextBox g03_03_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g03_03_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.TextBox g03_03_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g03_03_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.TextBox g03_03_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.GroupBox g03_03_EDC_GroupBox;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.TextBox g03_03_TextBox9;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.TextBox g03_03_TextBox8;
        private System.Windows.Forms.Label label244;
        private System.Windows.Forms.TextBox g03_03_TextBox7;
        private System.Windows.Forms.Label label245;
        private System.Windows.Forms.TextBox g03_03_TextBox6;
        private System.Windows.Forms.Label label246;
        private System.Windows.Forms.TextBox g03_03_TextBox5;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.TextBox g03_03_TextBox4;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.TextBox g03_03_TextBox3;
        private System.Windows.Forms.Label label249;
        private System.Windows.Forms.TextBox g03_03_TextBox2;
        private System.Windows.Forms.Label label250;
        private System.Windows.Forms.TextBox g03_03_TextBox1;
        private System.Windows.Forms.TabPage g03_04_TabPage;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.GroupBox g03_04_Export_GroupBox;
        private System.Windows.Forms.Label g03_04_Folder_Label;
        private System.Windows.Forms.Button g03_04_selectFolder_Button;
        private System.Windows.Forms.GroupBox g03_04_FFU_GroupBox;
        private System.Windows.Forms.CheckBox g03_04_IsSensor_CheckBox;
        private System.Windows.Forms.Label label252;
        private System.Windows.Forms.TextBox g03_04_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g03_04_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label253;
        private System.Windows.Forms.TextBox g03_04_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g03_04_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label254;
        private System.Windows.Forms.TextBox g03_04_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label255;
        private System.Windows.Forms.Label label256;
        private System.Windows.Forms.Label label257;
        private System.Windows.Forms.Label label258;
        private System.Windows.Forms.GroupBox g03_04_EDC_GroupBox;
        private System.Windows.Forms.Label label259;
        private System.Windows.Forms.TextBox g03_04_TextBox9;
        private System.Windows.Forms.Label label260;
        private System.Windows.Forms.TextBox g03_04_TextBox8;
        private System.Windows.Forms.Label label261;
        private System.Windows.Forms.TextBox g03_04_TextBox7;
        private System.Windows.Forms.Label label262;
        private System.Windows.Forms.TextBox g03_04_TextBox6;
        private System.Windows.Forms.Label label263;
        private System.Windows.Forms.TextBox g03_04_TextBox5;
        private System.Windows.Forms.Label label264;
        private System.Windows.Forms.TextBox g03_04_TextBox4;
        private System.Windows.Forms.Label label265;
        private System.Windows.Forms.TextBox g03_04_TextBox3;
        private System.Windows.Forms.Label label266;
        private System.Windows.Forms.TextBox g03_04_TextBox2;
        private System.Windows.Forms.Label label267;
        private System.Windows.Forms.TextBox g03_04_TextBox1;
        private System.Windows.Forms.TabPage g03_05_TabPage;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.GroupBox g03_05_Export_GroupBox;
        private System.Windows.Forms.Label g03_05_Folder_Label;
        private System.Windows.Forms.Button g03_05_selectFolder_Button;
        private System.Windows.Forms.GroupBox g03_05_FFU_GroupBox;
        private System.Windows.Forms.CheckBox g03_05_IsSensor_CheckBox;
        private System.Windows.Forms.Label label269;
        private System.Windows.Forms.TextBox g03_05_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g03_05_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.TextBox g03_05_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g03_05_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label271;
        private System.Windows.Forms.TextBox g03_05_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label272;
        private System.Windows.Forms.Label label273;
        private System.Windows.Forms.Label label274;
        private System.Windows.Forms.Label label275;
        private System.Windows.Forms.GroupBox g03_05_EDC_GroupBox;
        private System.Windows.Forms.Label label276;
        private System.Windows.Forms.TextBox g03_05_TextBox9;
        private System.Windows.Forms.Label label277;
        private System.Windows.Forms.TextBox g03_05_TextBox8;
        private System.Windows.Forms.Label label278;
        private System.Windows.Forms.TextBox g03_05_TextBox7;
        private System.Windows.Forms.Label label279;
        private System.Windows.Forms.TextBox g03_05_TextBox6;
        private System.Windows.Forms.Label label280;
        private System.Windows.Forms.TextBox g03_05_TextBox5;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.TextBox g03_05_TextBox4;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.TextBox g03_05_TextBox3;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.TextBox g03_05_TextBox2;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.TextBox g03_05_TextBox1;
        private System.Windows.Forms.TabPage g03_06_TabPage;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.GroupBox g03_06_Export_GroupBox;
        private System.Windows.Forms.Label g03_06_Folder_Label;
        private System.Windows.Forms.Button g03_06_selectFolder_Button;
        private System.Windows.Forms.GroupBox g03_06_FFU_GroupBox;
        private System.Windows.Forms.CheckBox g03_06_IsSensor_CheckBox;
        private System.Windows.Forms.Label label286;
        private System.Windows.Forms.TextBox g03_06_Particle_ID_TextBox;
        private System.Windows.Forms.TextBox g03_06_Particle_Quanity_TextBox;
        private System.Windows.Forms.Label label287;
        private System.Windows.Forms.TextBox g03_06_DPS_ID_TextBox;
        private System.Windows.Forms.TextBox g03_06_DPS_Quanity_TextBox;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.TextBox g03_06_FFU_Quanity_TextBox;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.GroupBox g03_06_EDC_GroupBox;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.TextBox g03_06_TextBox9;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.TextBox g03_06_TextBox8;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.TextBox g03_06_TextBox7;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.TextBox g03_06_TextBox6;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.TextBox g03_06_TextBox5;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.TextBox g03_06_TextBox4;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.TextBox g03_06_TextBox3;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.TextBox g03_06_TextBox2;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.TextBox g03_06_TextBox1;
    }
}

