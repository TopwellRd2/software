﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TOPWELL_INNOLUX_EDC2
{
    public partial class Form1 : Form
    {
        private IniHandler iniHandler = new IniHandler(Application.StartupPath + "/settings.ini");

        private Dictionary<string, GatewayConnection> gatewayMap = new Dictionary<string, GatewayConnection>();

        private Dictionary<string, System.Timers.Timer> timerMap = new Dictionary<string, System.Timers.Timer>();

        private System.Timers.Timer g01_GatewayUpdateTimer; //(增加畫面需變更)
        private System.Timers.Timer g02_GatewayUpdateTimer;
        private System.Timers.Timer g03_GatewayUpdateTimer;

        private int[] unitCount = {6,6,6};     //依序為每個gateway的Unit個數 (增加畫面需變更)

        public Form1()
        {
            InitializeComponent();

            //gateway   參數: 1.畫面gateway控制項名稱 2.主畫面 3.unit數量 4.單台讀取資料長度(幾個word) (增加畫面需變更)
            gatewayMap.Add("g01", new GatewayConnection("g01", this, unitCount[0], 10));
            gatewayMap.Add("g02", new GatewayConnection("g02", this, unitCount[1], 10));
            gatewayMap.Add("g03", new GatewayConnection("g03", this, unitCount[2], 10));

            //Timer  (增加畫面需變更)
            //gateway01
            g01_GatewayUpdateTimer = new System.Timers.Timer();
            g01_GatewayUpdateTimer.Interval = 5000;
            g01_GatewayUpdateTimer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => GenerateXML(s, e, "g01")); ;
            timerMap.Add("g01", g01_GatewayUpdateTimer);
            initial_Time_ComboBox("g01");
            //gateway02
            g02_GatewayUpdateTimer = new System.Timers.Timer();
            g02_GatewayUpdateTimer.Interval = 5000;
            g02_GatewayUpdateTimer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => GenerateXML(s, e, "g02")); ;
            timerMap.Add("g02", g02_GatewayUpdateTimer);
            initial_Time_ComboBox("g02");
            //gateway03
            g03_GatewayUpdateTimer = new System.Timers.Timer();
            g03_GatewayUpdateTimer.Interval = 5000;
            g03_GatewayUpdateTimer.Elapsed += new System.Timers.ElapsedEventHandler((s, e) => GenerateXML(s, e, "g03")); ;
            timerMap.Add("g03", g03_GatewayUpdateTimer);
            initial_Time_ComboBox("g03");
        }

        private void Form1_Load(object sender, EventArgs e)  
        {
            //把ini檔資料讀出畫面
            for (int i = 1; i <= unitCount.Length; i++)
            {
                for (int j = 1; j <= unitCount[i - 1]; j++)
                {
                    LoadDataFromIni("g0" + i + "_0" + j);
                }
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) 
        {
            //把畫面資料寫入ini檔
            for (int i = 1; i <= unitCount.Length; i++)
            {
                for (int j = 1; j <= unitCount[i-1]; j++)
                {
                    SaveDataToIni("g0" + i + "_0" + j);
                }
            }
        }

        private void initial_Time_ComboBox(String gateway) 
        {
            ComboBox ComboBox = ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]);
            ArrayList data = new ArrayList();
            data.Add(new DictionaryEntry("1分鐘", 60000));
            data.Add(new DictionaryEntry("5分鐘", 300000));
            data.Add(new DictionaryEntry("10分鐘", 600000));
            data.Add(new DictionaryEntry("15分鐘", 900000));
            data.Add(new DictionaryEntry("30分鐘", 1800000));
            for(int i = 1; i < 25; i++)
            {
                data.Add(new DictionaryEntry(i+"小時", 3600000 * i));
            }
            //data.Add(new DictionaryEntry("1小時", 3600000));
            //data.Add(new DictionaryEntry("2小時", 3600000 * 2));
            //data.Add(new DictionaryEntry("3小時", 3600000 * 3));
            //data.Add(new DictionaryEntry("4小時", 3600000 * 4));
            //data.Add(new DictionaryEntry("5小時", 3600000 * 5));
            //data.Add(new DictionaryEntry("6小時", 3600000 * 6));
            //data.Add(new DictionaryEntry("7小時", 3600000 * 7));
            //data.Add(new DictionaryEntry("8小時", 3600000 * 8));
            //data.Add(new DictionaryEntry("9小時", 3600000 * 9));
            //data.Add(new DictionaryEntry("10小時", 3600000 * 10));
            //data.Add(new DictionaryEntry("11小時", 3600000 * 11));
            //data.Add(new DictionaryEntry("12小時", 3600000 * 12));
            //data.Add(new DictionaryEntry("13小時", 3600000 * 13));
            //data.Add(new DictionaryEntry("14小時", 3600000 * 14));
            //data.Add(new DictionaryEntry("15小時", 3600000 * 15));
            //data.Add(new DictionaryEntry("16小時", 3600000 * 16));
            //data.Add(new DictionaryEntry("17小時", 3600000 * 17));
            //data.Add(new DictionaryEntry("18小時", 3600000 * 18));
            //data.Add(new DictionaryEntry("19小時", 3600000 * 19));
            //data.Add(new DictionaryEntry("20小時", 3600000 * 20));
            //data.Add(new DictionaryEntry("21小時", 3600000 * 21));
            //data.Add(new DictionaryEntry("22小時", 3600000 * 22));
            //data.Add(new DictionaryEntry("23小時", 3600000 * 23));
            //data.Add(new DictionaryEntry("24小時", 3600000 * 24));
            ComboBox.DisplayMember = "Key";
            ComboBox.ValueMember = "Value";
            ComboBox.DataSource = data;
        }

        private void DPS_ID_TextBox_Leave(object sender, EventArgs e)    //(增加畫面需變更) 增加至每一個按鈕事件中
        {
            TextBox textbox = sender as TextBox;
            int n = 0;
            if (!string.IsNullOrWhiteSpace(textbox.Text))
            {
                if (int.TryParse(textbox.Text, out n))
                {
                    if (int.Parse(textbox.Text) < 64 && int.Parse(textbox.Text) != 0)
                    {
                        MessageBox.Show("DPS的ID必須大於63!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textbox.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("DPS的ID必須為數字!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textbox.Focus();
                    return;
                }
            }
            else
            {
                MessageBox.Show("DPS的ID不得為空!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textbox.Focus();
                return;
            }
        }

        private void Particle_ID_TextBox_Leave(object sender, EventArgs e)  //(增加畫面需變更) 增加至每一個按鈕事件中
        {
            TextBox textbox = sender as TextBox;
            int n = 0;
            if (!string.IsNullOrWhiteSpace(textbox.Text))
            {
                if (int.TryParse(textbox.Text, out n))
                {
                    if (int.Parse(textbox.Text) < 64 && int.Parse(textbox.Text) != 0)
                    {
                        MessageBox.Show("Particle的ID必須大於63!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textbox.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Particle的ID必須為數字!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textbox.Focus();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Particle的ID不得為空!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textbox.Focus();
                return;
            }
        }

        private void SaveDataToIni(String unit)
        {
            String gateway = unit.Substring(0, 3);
            iniHandler.WriteIniFile(unit, gateway + "_IP_TextBox", ((TextBox)this.Controls.Find(gateway + "_IP_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_Folder_Label", ((Label)this.Controls.Find(unit + "_Folder_Label", true)[0]).Text);
            iniHandler.WriteIniFile(unit, gateway + "_Time_comboBox", ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]).SelectedValue);
            for (int i = 1; i < 10; i++)
            {
                iniHandler.WriteIniFile(unit, unit + "_TextBox" + i, ((TextBox)this.Controls.Find(unit + "_TextBox" + i, true)[0]).Text);
            }
            iniHandler.WriteIniFile(unit, unit + "_DPS_ID_TextBox", ((TextBox)this.Controls.Find(unit + "_DPS_ID_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_Particle_ID_TextBox", ((TextBox)this.Controls.Find(unit + "_Particle_ID_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_FFU_Quanity_TextBox", ((TextBox)this.Controls.Find(unit + "_FFU_Quanity_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_DPS_Quanity_TextBox", ((TextBox)this.Controls.Find(unit + "_DPS_Quanity_TextBox", true)[0]).Text);
            iniHandler.WriteIniFile(unit, unit + "_Particle_Quanity_TextBox", ((TextBox)this.Controls.Find(unit + "_Particle_Quanity_TextBox", true)[0]).Text);
            if (((CheckBox)this.Controls.Find(unit + "_IsSensor_CheckBox", true)[0]).Checked)
            {
                iniHandler.WriteIniFile(unit, unit + "_IsSensor_CheckBox", "Y");
            }
            else
            {
                iniHandler.WriteIniFile(unit, unit + "_IsSensor_CheckBox", "N");
            }
        }

        private void LoadDataFromIni(String unit)
        {
            if (iniHandler.isExistIni() == true)
            {
                String gateway = unit.Substring(0, 3);
                ((TextBox)this.Controls.Find(gateway + "_IP_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, gateway + "_IP_TextBox", "");
                ((Label)this.Controls.Find(unit + "_Folder_Label", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_Folder_Label", "");
                String time_ComboBox_Value = iniHandler.ReadIniFile(unit, gateway + "_Time_comboBox", "");
                if (!string.IsNullOrWhiteSpace(time_ComboBox_Value))
                {
                    ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]).SelectedValue = int.Parse(time_ComboBox_Value);
                }
                for (int i = 1; i < 10; i++)
                {
                    ((TextBox)this.Controls.Find(unit + "_TextBox" + i, true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_TextBox" + i, "");
                }
                ((TextBox)this.Controls.Find(unit + "_DPS_ID_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_DPS_ID_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_Particle_ID_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_Particle_ID_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_FFU_Quanity_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_FFU_Quanity_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_DPS_Quanity_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_DPS_Quanity_TextBox", "");
                ((TextBox)this.Controls.Find(unit + "_Particle_Quanity_TextBox", true)[0]).Text = iniHandler.ReadIniFile(unit, unit + "_Particle_Quanity_TextBox", "");
                if ("Y" == iniHandler.ReadIniFile(unit, unit + "_IsSensor_CheckBox", ""))
                {
                    ((CheckBox)this.Controls.Find(unit + "_IsSensor_CheckBox", true)[0]).Checked = true;
                }
                else
                {
                    ((CheckBox)this.Controls.Find(unit + "_IsSensor_CheckBox", true)[0]).Checked = false;
                }
            }
        }

        private void Connect_Button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            String gateway = button.Name.Substring(0, 3);
            int gatewayNum = int.Parse(gateway.Substring(1, 2));
            Label connectLabel = (Label)this.Controls.Find(gateway + "_connect_Label", true)[0];
            for(int i = 1; i <= unitCount[gatewayNum - 1]; i++)
            {
                Label folderLabel = (Label)this.Controls.Find(gateway + "_0" + i + "_Folder_Label", true)[0];
                if ("請選擇匯出路徑" == folderLabel.Text || string.IsNullOrEmpty(folderLabel.Text))
                {
                    MessageBox.Show("gateway" + gatewayNum + "- unit0" + i + " : 請先選擇匯出路徑!!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                    {
                        return;
                    }
                    this.Controls.Find(gateway + "_0" + i + "_Folder_Label", true)[0].Text = folderBrowserDialog.SelectedPath;
                }
            }
            if (!gatewayMap[gateway].ConnectState)          //未連線時
            {
                TextBox ipTextBox = (TextBox)this.Controls.Find(gateway + "_IP_TextBox", true)[0];

                if (string.IsNullOrWhiteSpace(ipTextBox.Text))
                {
                    MessageBox.Show("IP不得為空!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ipTextBox.Focus();
                    return;
                }
                
                gatewayMap[gateway].connect();

                if (gatewayMap[gateway].ConnectState)
                {
                    //依據匯出頻率更改timer
                    ComboBox time_ComboBox = (ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0];
                    timerMap[gateway].Interval = Convert.ToInt32(time_ComboBox.SelectedValue);

                    timerMap[gateway].Start();
                    button.Text = "中斷連線";
                    connectLabel.Text = "已連線";

                    //連線中控制項不給更改
                    foreach (Control control in ((Panel)this.Controls.Find(gateway + "_Panel", true)[0]).Controls)
                    {
                        if (control is TextBox)
                        {
                            ((TextBox)control).Enabled = false;
                        }
                        else if (control is TabControl)
                        {
                            foreach (Control tabPageControl in control.Controls)
                            {
                                if (tabPageControl is TabPage)
                                {
                                    String unit = ((TabPage)tabPageControl).Name.Substring(0, 6);
                                    foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_EDC_GroupBox", true)[0]).Controls)
                                    {
                                        if (subControl is TextBox)
                                        {
                                            ((TextBox)subControl).Enabled = false;
                                        }
                                    }
                                    foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_FFU_GroupBox", true)[0]).Controls)
                                    {
                                        if (subControl is TextBox)
                                        {
                                            ((TextBox)subControl).Enabled = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (int i = 1; i <= unitCount[gatewayNum - 1]; i++)
                    {
                        ((Button)this.Controls.Find(gateway + "_0" + i + "_selectFolder_Button", true)[0]).Enabled = false;
                    }
                    time_ComboBox.Enabled = false;
                }
                else
                {
                    MessageBox.Show("連線異常,請確認IP是否正確!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else       //連線時
            {
                if (gatewayMap[gateway].myTcpClient != null)
                    gatewayMap[gateway].myTcpClient.Close();
                if (gatewayMap[gateway].myNetworkStream != null)
                    gatewayMap[gateway].myNetworkStream.Close();
                gatewayMap[gateway].closeThread();

                gatewayMap[gateway].ConnectState = false;
                timerMap[gateway].Stop();
                button.Text = "連線";
                connectLabel.Text = "未連線";

                //離線時開放控制項更改
                foreach (Control control in ((Panel)this.Controls.Find(gateway + "_Panel", true)[0]).Controls)
                {
                    if (control is TextBox)
                    {
                        ((TextBox)control).Enabled = true;
                    }
                    else if (control is TabControl)
                    {
                        foreach(Control tabPageControl in control.Controls)
                        {
                            if (tabPageControl is TabPage)
                            {
                                String unit = ((TabPage)tabPageControl).Name.Substring(0, 6);
                                foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_EDC_GroupBox", true)[0]).Controls)
                                {
                                    if (subControl is TextBox)
                                    {
                                        ((TextBox)subControl).Enabled = true;
                                    }
                                }
                                foreach (Control subControl in ((GroupBox)this.Controls.Find(unit + "_FFU_GroupBox", true)[0]).Controls)
                                {
                                    if (subControl is TextBox)
                                    {
                                        ((TextBox)subControl).Enabled = true;
                                    }
                                }
                            }
                        }
                    }
                }
                for (int i = 1; i <= unitCount[gatewayNum - 1]; i++)
                {
                    ((Button)this.Controls.Find(gateway + "_0" + i + "_selectFolder_Button", true)[0]).Enabled = true;
                }
                ((ComboBox)this.Controls.Find(gateway + "_Time_comboBox", true)[0]).Enabled = true;
            }
        }

        private void SelectFolder_Button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            String unit = button.Name.Substring(0, 6);
            folderBrowserDialog.SelectedPath = ((Label)this.Controls.Find(unit + "_Folder_Label", true)[0]).Text;
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }
            this.Controls.Find(unit + "_Folder_Label", true)[0].Text = folderBrowserDialog.SelectedPath;
        }

        //private void G01_GenerateXML(object sender, EventArgs e)  //(增加畫面需變更)
        //{
        //    timerMap["g01"].Stop();
        //    gatewayMap["g01"].doAction();
        //    bool exportXMLCheck = false;
        //    for (int i = 1; i <= unitCount[0]; i++)      //依據目前有開幾個unit
        //    {

        //        exportXMLCheck = XmlHandler.exportXml("g01_0" + i, this, gatewayMap["g01"], g01_Folder_Label.Text);
        //        if (!exportXMLCheck)
        //        {
        //            return;
        //        }
        //    }
        //    timerMap["g01"].Start();
        //}

        //private void G02_GenerateXML(object sender, EventArgs e) 
        //{
        //    timerMap["g02"].Stop();
        //    gatewayMap["g02"].doAction();
        //    bool exportXMLCheck = false;
        //    for (int i = 1; i <= unitCount[1]; i++)      //依據目前有開幾個unit
        //    {

        //        exportXMLCheck = XmlHandler.exportXml("g02_0" + i, this, gatewayMap["g02"], g02_Folder_Label.Text);
        //        if (!exportXMLCheck)
        //        {
        //            return;
        //        }
        //    }
        //    timerMap["g02"].Start();
        //}

        private void GenerateXML(object sender, EventArgs e, String gateway)
        {
            //造成執行時間一直往後推延,先不使用
            //timerMap[gateway].Stop();
            gatewayMap[gateway].doAction();
            bool exportXMLCheck = false;
            for (int i = 1; i <= unitCount[int.Parse(gateway.Replace("g","")) - 1]; i++)      //依據目前有開幾個unit
            {
                String unit = gateway + "_0" + i;
                exportXMLCheck = XmlHandler.exportXml(unit, this, gatewayMap[gateway], ((Label)this.Controls.Find(unit + "_Folder_Label", true)[0]).Text);
                if (!exportXMLCheck)
                {
                    ((Button)this.Controls.Find(gateway + "_connect_Button", true)[0]).PerformClick();
                    return;
                }
            }
            //timerMap[gateway].Start();
        }
    }
}
