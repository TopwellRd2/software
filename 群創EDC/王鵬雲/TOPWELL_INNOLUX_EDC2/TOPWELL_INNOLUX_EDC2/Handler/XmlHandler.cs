﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;
using System.IO;

namespace TOPWELL_INNOLUX_EDC2
{
    static class XmlHandler
    {

        public static bool exportXml(string unit, Form1 form, GatewayConnection gatewayConnection, string path)
        {
            XmlDocument xmlDoc;
            XmlElement edcElement;

            try {
                xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclare = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                xmlDoc.AppendChild(xmlDeclare);

                //建立根節點物件並加入 XmlDocument 中 (第0層)
                edcElement = xmlDoc.CreateElement("EDC");
                xmlDoc.AppendChild(edcElement);

                //開始寫檔
                writeAElementInEDC(xmlDoc, "glass_id", form.Controls.Find(unit + "_TextBox1", true)[0].Text);
                writeAElementInEDC(xmlDoc, "group_id");
                writeAElementInEDC(xmlDoc, "lot_id");
                writeAElementInEDC(xmlDoc, "product_id", form.Controls.Find(unit + "_TextBox2", true)[0].Text);
                writeAElementInEDC(xmlDoc, "pfcd", form.Controls.Find(unit + "_TextBox10", true)[0].Text);
                writeAElementInEDC(xmlDoc, "eqp_id", form.Controls.Find(unit + "_TextBox3", true)[0].Text);
                writeAElementInEDC(xmlDoc, "sub_eqp_id", form.Controls.Find(unit + "_TextBox4", true)[0].Text);
                writeAElementInEDC(xmlDoc, "ec_code");
                writeAElementInEDC(xmlDoc, "route_no");
                writeAElementInEDC(xmlDoc, "route_version");
                writeAElementInEDC(xmlDoc, "owner", form.Controls.Find(unit + "_TextBox5", true)[0].Text);
                writeAElementInEDC(xmlDoc, "recipe_id", form.Controls.Find(unit + "_TextBox6", true)[0].Text);
                writeAElementInEDC(xmlDoc, "operation", form.Controls.Find(unit + "_TextBox7", true)[0].Text);
                writeAElementInEDC(xmlDoc, "rtc_flag");
                writeAElementInEDC(xmlDoc, "pnp");
                writeAElementInEDC(xmlDoc, "chamber", form.Controls.Find(unit + "_TextBox8", true)[0].Text);
                writeAElementInEDC(xmlDoc, "cassette_id");
                writeAElementInEDC(xmlDoc, "line_batch_id");
                writeAElementInEDC(xmlDoc, "split_id");
                writeAElementInEDC(xmlDoc, "mes_link_key");
                writeAElementInEDC(xmlDoc, "rework_count");
                writeAElementInEDC(xmlDoc, "operator", form.Controls.Find(unit + "_TextBox9", true)[0].Text);
                writeAElementInEDC(xmlDoc, "cldate", DateTime.Now.ToString("yyyy-MM-dd"));
                writeAElementInEDC(xmlDoc, "cltime", DateTime.Now.ToString("HH:mm:ss"));
                writeAElementInEDC(xmlDoc, "datas");

                int ffuCnt = int.Parse(form.Controls.Find(unit + "_FFU_Quanity_TextBox", true)[0].Text);
                int dpsCnt = int.Parse(form.Controls.Find(unit + "_DPS_Quanity_TextBox", true)[0].Text);
                int particleCnt = int.Parse(form.Controls.Find(unit + "_Particle_Quanity_TextBox", true)[0].Text);
                int dpsStartID = int.Parse(form.Controls.Find(unit + "_DPS_ID_TextBox", true)[0].Text);
                int particleStartID = int.Parse(form.Controls.Find(unit + "_Particle_ID_TextBox", true)[0].Text);

                String gateway = unit.Substring(0, 3);
                String unitString = unit.Substring(unit.Length - 1);

                String FFUData;
                //FFU_SPEED
                for (int i = 1; i < (1 + ffuCnt) ; i++) 
                {
                    FFUData = gatewayConnection.getDeviceDataAry(int.Parse(unitString), i)[4].ToString();

                    if(i < 10)
                    {
                        writeAElementInDatas(xmlDoc, "FFU_SPEED_" + gateway + "-" + unitString + "-0" + i, "X", FFUData);
                    }
                    else
                    {
                        writeAElementInDatas(xmlDoc, "FFU_SPEED_" + gateway + "-" + unitString + "-" + i, "X", FFUData);
                    }
                }

                //FFU_DPS
                String DPSData;
                int y = 0;
                for (int i = dpsStartID; i < (dpsStartID + dpsCnt); i++)
                {
                    DPSData = ((gatewayConnection.getDeviceDataAry(int.Parse(unitString), i)[8]) / 10.0F).ToString();
                    
                    if(i > 63)
                    {
                        y = i - dpsStartID + 1;   
                    }
                    else
                    {
                        MessageBox.Show("DPS起始站號請大於63", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false; 
                    }

                    if (y < 10)
                    {
                        writeAElementInDatas(xmlDoc, "FFU_DPS_" + gateway + "-" + unitString + "-0" + y, "X", DPSData);
                    }
                    else
                    {
                        writeAElementInDatas(xmlDoc, "FFU_DPS_" + gateway + "-" + unitString + "-" + y, "X", DPSData);
                    }
                }

                //FFU_Particle
                String ParticleData;
                y = 0;
                for (int i = particleStartID; i < (particleStartID + particleCnt); i++)
                {
                    ParticleData = ((gatewayConnection.getDeviceDataAry(int.Parse(unitString), i)[9])).ToString();

                    if (i > 63)
                    {
                        y = i - particleStartID + 1;    //待調整
                    }
                    else
                    {
                        MessageBox.Show("Particle起始站號請大於63", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    if (i < 10)
                    {
                        writeAElementInDatas(xmlDoc, "FFU_Particle_" + gateway + "-" + unitString + "-0" + y, "X", ParticleData);
                    }
                    else
                    {
                        writeAElementInDatas(xmlDoc, "FFU_Particle_" + gateway + "-" + unitString + "-" + y, "X", ParticleData);
                    }
                }

                String eqp_id = form.Controls.Find(unit + "_TextBox3", true)[0].Text;
                String glass_id = form.Controls.Find(unit + "_TextBox1", true)[0].Text;
                //String sub_eqp_id = form.Controls.Find(unit + "_TextBox4", true)[0].Text;
                //if (!string.IsNullOrWhiteSpace(eqp_id))
                //{
                //    path = path.Replace("<eqp_id>", eqp_id);
                //}
                //else
                //{
                //    path = path.Replace("\\<eqp_id>", "");
                //}
                //if (!string.IsNullOrWhiteSpace(sub_eqp_id))
                //{
                //    path = path.Replace("<sub_eqp_id>", sub_eqp_id);
                //}
                //else
                //{
                //    path = path.Replace("\\<sub_eqp_id>", "");
                //}
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                xmlDoc.Save(path + "/"  + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + eqp_id + "_" + glass_id + ".xml");
            }
            catch (Exception ex) {
                MessageBox.Show("XML匯出異常! 請聯絡設計人員!" + ex, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private static void writeAElementInEDC(XmlDocument xmlDoc, string name, string innerTxt)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlElement element = xmlDoc.CreateElement(name); //建立一個節點 

            element.InnerText = innerTxt;
            root.AppendChild(element);
        }

        private static void writeAElementInEDC(XmlDocument xmlDoc, string name)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlElement element = xmlDoc.CreateElement(name); //建立一個節點 

            root.AppendChild(element);
        }

        private static void writeAElementInDatas(XmlDocument xmlDoc, string innerTxt1, string innerTxt2, string innerTxt3)
        {
            XmlNode root = xmlDoc.SelectSingleNode("EDC"); //查詢<EDC>
            XmlNode dataRoot = root.SelectSingleNode("datas"); //查詢<datas> 
            XmlElement iaryElement = xmlDoc.CreateElement("iary"); //建立一個節點 
            XmlElement element1 = xmlDoc.CreateElement("item_name"); //建立一個節點 
            element1.InnerText = innerTxt1;
            XmlElement element2 = xmlDoc.CreateElement("item_type"); //建立一個節點 
            element2.InnerText = innerTxt2;
            XmlElement element3 = xmlDoc.CreateElement("item_value"); //建立一個節點 
            element3.InnerText = innerTxt3;

            iaryElement.AppendChild(element1);
            iaryElement.AppendChild(element2);
            iaryElement.AppendChild(element3);
            dataRoot.AppendChild(iaryElement);
            root.AppendChild(dataRoot);
        }
    }
}
