﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataComparison_TSMC
{
    public partial class ComparisonForm : Form
    {
        public Form1 MainForm;
        public ComparisonForm(Form1 form)
        {
            InitializeComponent();
            MainForm = form;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MainForm.comparison_Excel();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            percentageLabel.Text = e.ProgressPercentage.ToString() + "%";
            if (e.ProgressPercentage > 65)
            {
                percentageLabel.BackColor = Color.FromArgb(27, 177, 27);
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
            MainForm.resultForm.Show();
        }
    }
}
