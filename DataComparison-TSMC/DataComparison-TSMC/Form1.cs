﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Threading;

namespace DataComparison_TSMC
{
    public partial class Form1 : Form
    {
        public ResultForm resultForm ;
        public ComparisonForm comparisonForm ;
        public Form1()
        {
            InitializeComponent();
        }

        private void selectPriceBook_Button_Click(object sender, EventArgs e)
        {
            file_PriceBook.ShowDialog();
            if (!string.IsNullOrWhiteSpace(file_PriceBook.FileName))
            {
                selectPriceBookLabel.Text = file_PriceBook.FileName;
            }
        }

        private void selectQuotation_Button_Click(object sender, EventArgs e)
        {
            file_Quotation.ShowDialog();
            if (!string.IsNullOrWhiteSpace(file_Quotation.FileName))
            {
                selectQuotationLabel.Text = file_Quotation.FileName;
            }
        }

        private void comparison_Button_Click(object sender, EventArgs e)
        {
            resultForm = new ResultForm();
            comparisonForm = new ComparisonForm(this);

            if (string.IsNullOrWhiteSpace(file_PriceBook.FileName))
            {
                MessageBox.Show("請先選擇 PriceBook !!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (file_PriceBook.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                else if (!string.IsNullOrWhiteSpace(file_PriceBook.FileName))
                {
                    selectPriceBookLabel.Text = file_PriceBook.FileName;
                }
            }

            if (string.IsNullOrWhiteSpace(file_Quotation.FileName))
            {
                MessageBox.Show("請先選擇 Quotation !!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (file_Quotation.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                else if (!string.IsNullOrWhiteSpace(file_Quotation.FileName))
                {
                    selectQuotationLabel.Text = file_Quotation.FileName;
                }
            }

            comparisonForm.Show();
            comparisonForm.backgroundWorker.RunWorkerAsync();
        }

        public void comparison_Excel()
        {
            Excel.Application excelApp = new Excel.Application();  //應用程式
            Excel.Workbook priceBookExcel = excelApp.Workbooks.Open(file_PriceBook.FileName);   //開啟PriceBook路徑檔案
            Excel.Worksheet priceBookWorksheet = (Excel.Worksheet)priceBookExcel.Worksheets[1];
            Excel.Workbook quotationExcel = excelApp.Workbooks.Open(file_Quotation.FileName);   //開啟Quotation路徑檔案
            Excel.Worksheet quotationWorksheet = (Excel.Worksheet)quotationExcel.Worksheets[1];

            int quotation_i = 2;
            int priceBook_i = 2;

            Double quotation_Total = 0;
            Double quotation_Right = 0;
            Double quotation_Error = 0;
            Double priceBook_Total = 0;

            string quitation_text;
            string priceBook_text;

            for (priceBook_i = 2; string.IsNullOrEmpty((String)priceBookWorksheet.Cells[priceBook_i, 7].Value) == false; priceBook_i++)
            {
                priceBook_Total++;
            }

            for (quotation_i = 2; string.IsNullOrEmpty((String)quotationWorksheet.Cells[quotation_i, 7].Value) == false; quotation_i++)
            {
                quitation_text = (String)quotationWorksheet.Cells[quotation_i, 7].Value;
                for (priceBook_i = 2; string.IsNullOrEmpty((String)priceBookWorksheet.Cells[priceBook_i, 7].Value) == false; priceBook_i++)
                {
                    priceBook_text = (String)priceBookWorksheet.Cells[priceBook_i, 7].Value;
                    if (quitation_text == priceBook_text)
                    {
                        if ((String)quotationWorksheet.Cells[quotation_i, 19].Value == (String)priceBookWorksheet.Cells[priceBook_i, 19].Value)
                        {
                            quotation_Right++;
                            resultForm.resultDataGridView.Rows.Add(quotation_i, (String)quotationWorksheet.Cells[quotation_i, 3].Value, (String)quotationWorksheet.Cells[quotation_i, 7].Value, (String)quotationWorksheet.Cells[quotation_i, 10].Value, (String)quotationWorksheet.Cells[quotation_i, 11].Value, (String)quotationWorksheet.Cells[quotation_i, 12].Value, (String)quotationWorksheet.Cells[quotation_i, 25].Value, (String)quotationWorksheet.Cells[quotation_i, 26].Value, (String)quotationWorksheet.Cells[quotation_i, 32].Value, (String)quotationWorksheet.Cells[quotation_i, 18].Value, (String)quotationWorksheet.Cells[quotation_i, 19].Value);
                            break;
                        }
                        else
                        {
                            quotation_Error++;
                            resultForm.resultDataGridView.Rows.Add(quotation_i, (String)quotationWorksheet.Cells[quotation_i, 3].Value, (String)quotationWorksheet.Cells[quotation_i, 7].Value, (String)quotationWorksheet.Cells[quotation_i, 10].Value, (String)quotationWorksheet.Cells[quotation_i, 11].Value, (String)quotationWorksheet.Cells[quotation_i, 12].Value, (String)quotationWorksheet.Cells[quotation_i, 25].Value, (String)quotationWorksheet.Cells[quotation_i, 26].Value, (String)quotationWorksheet.Cells[quotation_i, 32].Value, (String)quotationWorksheet.Cells[quotation_i, 18].Value, (String)quotationWorksheet.Cells[quotation_i, 19].Value);
                            resultForm.resultDataGridView.Rows[Convert.ToInt32(quotation_Total)].DefaultCellStyle.ForeColor = Color.Red;
                            break;
                        }
                    }
                }
                quotation_Total++;

                comparisonForm.backgroundWorker.ReportProgress(Convert.ToInt32(quotation_Total / priceBook_Total * 100));
                Thread.Sleep(200);
            }

            quotationExcel.Close();
            if (quotationExcel!= priceBookExcel) {
                priceBookExcel.Close();
            }
            excelApp.Quit();

            resultForm.QAmountlabel.Text = quotation_Total.ToString();
            resultForm.PAmountlabel.Text = priceBook_Total.ToString();
            resultForm.errorAmountlabel.Text = quotation_Error.ToString();
            if (!(0 == quotation_Right))
            {
                resultForm.successAmountlabel.Text = (quotation_Right / quotation_Total * 100).ToString();
            }
            else
            {
                resultForm.successAmountlabel.Text = "0";
            }
            resultForm.warnAmountlabel.Text = quotation_Total.ToString();
            comparisonForm.backgroundWorker.ReportProgress(100);
            Thread.Sleep(600);
        }
    }
}
