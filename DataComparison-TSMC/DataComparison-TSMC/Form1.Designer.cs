﻿
namespace DataComparison_TSMC
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.selectPriceBookLabel = new System.Windows.Forms.Label();
            this.selectPriceBook_Button = new System.Windows.Forms.Button();
            this.selectQuotationLabel = new System.Windows.Forms.Label();
            this.selectQuotation_Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comparison_Button = new System.Windows.Forms.Button();
            this.file_PriceBook = new System.Windows.Forms.OpenFileDialog();
            this.file_Quotation = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(547, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "此比對程式只適用台積電固定格式之報價單";
            // 
            // selectPriceBookLabel
            // 
            this.selectPriceBookLabel.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.selectPriceBookLabel.Location = new System.Drawing.Point(259, 108);
            this.selectPriceBookLabel.Name = "selectPriceBookLabel";
            this.selectPriceBookLabel.Size = new System.Drawing.Size(473, 103);
            this.selectPriceBookLabel.TabIndex = 14;
            this.selectPriceBookLabel.Text = "檔案路徑";
            this.selectPriceBookLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectPriceBook_Button
            // 
            this.selectPriceBook_Button.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.selectPriceBook_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectPriceBook_Button.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.selectPriceBook_Button.Location = new System.Drawing.Point(57, 131);
            this.selectPriceBook_Button.Name = "selectPriceBook_Button";
            this.selectPriceBook_Button.Size = new System.Drawing.Size(186, 54);
            this.selectPriceBook_Button.TabIndex = 13;
            this.selectPriceBook_Button.Text = "選擇檔案";
            this.selectPriceBook_Button.UseVisualStyleBackColor = false;
            this.selectPriceBook_Button.Click += new System.EventHandler(this.selectPriceBook_Button_Click);
            // 
            // selectQuotationLabel
            // 
            this.selectQuotationLabel.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.selectQuotationLabel.Location = new System.Drawing.Point(259, 253);
            this.selectQuotationLabel.Name = "selectQuotationLabel";
            this.selectQuotationLabel.Size = new System.Drawing.Size(473, 103);
            this.selectQuotationLabel.TabIndex = 16;
            this.selectQuotationLabel.Text = "檔案路徑";
            this.selectQuotationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectQuotation_Button
            // 
            this.selectQuotation_Button.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.selectQuotation_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectQuotation_Button.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.selectQuotation_Button.Location = new System.Drawing.Point(57, 276);
            this.selectQuotation_Button.Name = "selectQuotation_Button";
            this.selectQuotation_Button.Size = new System.Drawing.Size(186, 54);
            this.selectQuotation_Button.TabIndex = 15;
            this.selectQuotation_Button.Text = "選擇檔案";
            this.selectQuotation_Button.UseVisualStyleBackColor = false;
            this.selectQuotation_Button.Click += new System.EventHandler(this.selectQuotation_Button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(25, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 28);
            this.label3.TabIndex = 17;
            this.label3.Text = "PriceBook：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(25, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 28);
            this.label4.TabIndex = 18;
            this.label4.Text = "Quotation：";
            // 
            // comparison_Button
            // 
            this.comparison_Button.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.comparison_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comparison_Button.Font = new System.Drawing.Font("微軟正黑體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comparison_Button.Location = new System.Drawing.Point(248, 384);
            this.comparison_Button.Name = "comparison_Button";
            this.comparison_Button.Size = new System.Drawing.Size(244, 70);
            this.comparison_Button.TabIndex = 19;
            this.comparison_Button.Text = "開始比對";
            this.comparison_Button.UseVisualStyleBackColor = false;
            this.comparison_Button.Click += new System.EventHandler(this.comparison_Button_Click);
            // 
            // file_PriceBook
            // 
            this.file_PriceBook.Filter = "Excel files (*.xls or *.xlsx)|*.xls;*.xlsx";
            this.file_PriceBook.Title = "選擇PriceBook";
            // 
            // file_Quotation
            // 
            this.file_Quotation.Filter = "Excel files (*.xls or *.xlsx)|*.xls;*.xlsx";
            this.file_Quotation.Title = "選擇Quotation";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 473);
            this.Controls.Add(this.comparison_Button);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectQuotationLabel);
            this.Controls.Add(this.selectQuotation_Button);
            this.Controls.Add(this.selectPriceBookLabel);
            this.Controls.Add(this.selectPriceBook_Button);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DataComparison-TSMC";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label selectPriceBookLabel;
        private System.Windows.Forms.Button selectPriceBook_Button;
        private System.Windows.Forms.Label selectQuotationLabel;
        private System.Windows.Forms.Button selectQuotation_Button;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button comparison_Button;
        private System.Windows.Forms.OpenFileDialog file_PriceBook;
        private System.Windows.Forms.OpenFileDialog file_Quotation;
    }
}

