﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace topwell_monitor
{
    public enum ReadLengthEnum { Six, Ten }//Gateway 讀取一組devicey資料的長度
    public enum AlarmTypeEnum { FFU, SMF }
    //public enum RegionEnum { OHGT01, OHGT03, REGION_STKL25_L10, REGION_STKL21_L30 }
    class Info
    {
        public const int TOTAL_GATEWAY = 4;
        public const int TOTAL_REGION = 4;

        public static readonly string REGION_OHGT01 = "OHGT01";
        public static readonly string REGION_OHGT03 = "OHGT03";
        public static readonly string REGION_STKL25_L10 = "STKL25_L10";
        public static readonly string REGION_STKL21_L30 = "STKL21_L30";

        public static readonly Color NORMAL_COLOR = Color.Green;
        public static readonly Color ALARM_COLOR = Color.Red;
        public static readonly Color STOP_COLOR = Color.Yellow;
        public static readonly Color BYPASS_COLOR = Color.Purple;
        public static readonly Color DISCONNECT_COLOR = Color.Gray;

    }
}
