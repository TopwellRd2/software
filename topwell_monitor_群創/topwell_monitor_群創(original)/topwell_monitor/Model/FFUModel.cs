﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    public enum FFUStatus { Normal, Alarm, Stop, Disconnect }
    class FFUModel : BaseModel
    {
        private const double REPORT_TIME_INTERVAL_HOURS = 1;

        public bool Operation { get; private set; }

        public int SettingSpeed { get; private set; }

        public int ActualSpeed { get; private set; }

        public int AlarmCode { get; private set; }

        public FFUStatus Status { get; private set; }

        private DateTime m_reportAlarmTime;

        private bool m_isReportTime = false;

        public FFUModel(string regionName, string ip, int register, int id, ReadLengthEnum readLength)
            : base(regionName, ip, register, id, readLength)
        {
            Status = FFUStatus.Disconnect;
        }

        public override bool setData(int[] data)
        {
            if (data.Length != ReadLength) {
                Status = FFUStatus.Disconnect;
                return false;
            }

            if (data[0] == 1)
            {
                if (AlarmCode == 0)
                {
                    if (data[3] > 0)
                    {
                        m_reportAlarmTime = DateTime.Now;
                        m_isReportTime = true;
                    }
                }
                else
                {
                    if (AlarmCode != data[3])
                    {
                        m_reportAlarmTime = DateTime.Now;
                        m_isReportTime = true;
                    }
                }
            }
                
            Operation = data[0] == 1 ? true : false;
            SettingSpeed = data[1];
            AlarmCode = data[3];
            ActualSpeed = data[4];

            if (Operation == true) {
                if (AlarmCode != 0)
                    Status = FFUStatus.Alarm;
                else
                    Status = FFUStatus.Normal;
            }
            else
                Status = FFUStatus.Stop;

            return true;
        }

        public bool isTime2Report()
        {
            if (m_isReportTime == true)
            {
                m_isReportTime = false;
                return true;
            }
            else
            {
                TimeSpan ts = DateTime.Now - m_reportAlarmTime;
                if (ts.TotalHours > REPORT_TIME_INTERVAL_HOURS)
                {
                    m_isReportTime = true;
                    m_reportAlarmTime = DateTime.Now;
                }
                return false;
            }
        }
    }
}
