﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    abstract class BaseModel
    {
        public string RegionName { get; private set; }
        public string Ip { get; private set; }
        public int Register { get; private set; }
        public int Id { get; private set; }

        public ushort ReadLength { get; private set; }//讀取GATEWAY的資料數量 目前讀有6筆或10筆

        public BaseModel(string regionName, string ip, int register, int id, ReadLengthEnum readLength)
        {
            RegionName = regionName;
            Ip = ip;
            Register = register;
            Id = id;

            switch (readLength)
            {
                case ReadLengthEnum.Six:
                    ReadLength = 6;
                    break;
                case ReadLengthEnum.Ten:
                    ReadLength = 10;
                    break;
            }
        }

        abstract public bool setData(int[] data);
    }
}
