﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.Drawing;

namespace topwell_monitor
{
    class SizeHandler
    {
        private int m_spiltCnt;

        private Panel[] m_panel;
        private PictureBox[] m_mainPic;
        private Button[] m_buttonAry;
        private PictureBox[] m_maskPicAry;

        private Size[] m_mainPicSize;
        private Size[] m_buttonSizeAry;
        private Size[] m_maskPicSizeAry;

        private Point[] m_mainPicPos;
        private Point[] m_buttonPosAry;
        private Point[] m_maskPicPosAry;

        private float[] m_buttonFontSizeAry;

        public SizeHandler(Panel[] panel, PictureBox[] mainPic, Button[] buttonAry, PictureBox[] maskPicAry, int spiltCnt)
        {
            m_spiltCnt = spiltCnt;

            m_panel = new Panel[panel.Length];

            for (int i = 0; i < panel.Length; i++)
            {
                m_panel[i] = panel[i];
            }

            m_mainPic = new PictureBox[mainPic.Length];
            m_mainPicSize = new Size[mainPic.Length];
            m_mainPicPos = new Point[mainPic.Length];

            for (int i = 0; i < mainPic.Length; i++)
            {
                m_mainPic[i] = mainPic[i];
                m_mainPic[i].SizeMode = PictureBoxSizeMode.Zoom;
                m_mainPicSize[i] = m_mainPic[i].Size;
                m_mainPicPos[i] = m_mainPic[i].Location;
            }
           
            m_buttonAry = new Button[buttonAry.Length];
            m_buttonSizeAry = new Size[buttonAry.Length];
            m_buttonPosAry = new Point[buttonAry.Length];
            m_buttonFontSizeAry = new float[buttonAry.Length];

            m_maskPicAry = new PictureBox[maskPicAry.Length];
            m_maskPicSizeAry = new Size[maskPicAry.Length];
            m_maskPicPosAry = new Point[maskPicAry.Length];

            for (int i = 0; i < buttonAry.Length; i++)
            {
                m_buttonAry[i] = buttonAry[i];
                m_buttonSizeAry[i] = buttonAry[i].Size;
                m_buttonPosAry[i] = buttonAry[i].Location;
                m_buttonFontSizeAry[i] = buttonAry[i].Font.Size;
            }

            for (int i = 0; i < maskPicAry.Length; i++)
            {
                m_maskPicAry[i] = maskPicAry[i];
                m_maskPicSizeAry[i] = maskPicAry[i].Size;
                m_maskPicPosAry[i] = maskPicAry[i].Location;
            }
        }

        public void changeSize(int value)
        {
            foreach (var panel in m_panel)
                panel.AutoScroll = false;

            for (int i = 0; i < m_mainPic.Length; i++)
            {
                m_mainPic[i].Size = new Size(m_mainPicSize[i].Width * value / m_spiltCnt, m_mainPicSize[i].Height * value / m_spiltCnt);
                m_mainPic[i].Location = new Point(m_mainPicPos[i].X * value / m_spiltCnt, m_mainPicPos[i].Y * value / m_spiltCnt);
            }


            for (int i = 0; i < m_buttonAry.Length; i++)
            {
                m_buttonAry[i].Size = new Size(m_buttonSizeAry[i].Width * value / m_spiltCnt, m_buttonSizeAry[i].Height * value / m_spiltCnt);
                m_buttonAry[i].Location = new Point(m_buttonPosAry[i].X * value / m_spiltCnt, m_buttonPosAry[i].Y * value / m_spiltCnt);   
                m_buttonAry[i].Font = new Font("", m_buttonFontSizeAry[i] * value / m_spiltCnt);
            }

            for (int i = 0; i < m_maskPicAry.Length; i++)
            {
                m_maskPicAry[i].Size = new Size(m_maskPicSizeAry[i].Width * value / m_spiltCnt, m_maskPicSizeAry[i].Height * value / m_spiltCnt);
                m_maskPicAry[i].Location = new Point(m_maskPicPosAry[i].X * value / m_spiltCnt, m_maskPicPosAry[i].Y * value / m_spiltCnt);
            }

            foreach (var panel in m_panel)
                panel.AutoScroll = true;
        }

    }
}
