﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    class ViewAlarm
    {
        public DataGridView m_dataGridView;
        Dictionary<string, RegionModel> m_regionModelMap;

        private delegate void dataGridDelegate();

        public ViewAlarm(DataGridView dataGridView, Dictionary<string, RegionModel> regionModelMap)
        {
            m_dataGridView = dataGridView;
            m_regionModelMap = regionModelMap;
        }

        public void update()
        {
            dataGridUpdate();
        }

        private void dataGridUpdate()
        {
            if (m_dataGridView.InvokeRequired)
            {
                dataGridDelegate callBack = new dataGridDelegate(dataGridUpdate);
                m_dataGridView.Invoke(callBack);
            }
            else
                updateDataGrid();
        }

        private void updateDataGrid()
        {
            m_dataGridView.Rows.Clear();

            foreach (var regionModel in m_regionModelMap)
            {
                foreach (var device in regionModel.Value.getDeviceList())
                {
                    if (device.Status == DeviceStatusEnum.FFUAlarm)
                        m_dataGridView.Rows.Add(new object[] { regionModel.Key + "_"+ device.Id, device.AlarmCode });
                }
            }
        }
    }
}
