﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    class ViewGateway
    {
        public DataGridView m_dataGridView;
        Dictionary<string, GatewayConnection> m_gatewayConnectionMap;
        Dictionary<string, string> m_ipMap;

        private delegate void dataGridDelegate();

        public ViewGateway(DataGridView dataGridView, Dictionary<string, GatewayConnection> gatewayConnectionMap, Dictionary<string, string>ipMap)
        {
            m_dataGridView = dataGridView;
            m_ipMap = ipMap;
            m_gatewayConnectionMap = gatewayConnectionMap;
        }

        public void update()
        {
            dataGridUpdate();
        }

        private void dataGridUpdate()
        {
            if (m_dataGridView.InvokeRequired)
            {
                dataGridDelegate callBack = new dataGridDelegate(dataGridUpdate);
                m_dataGridView.Invoke(callBack);
            }
            else
                updateDataGrid();
        }

        private void updateDataGrid()
        {
            m_dataGridView.Rows.Clear();
           
            foreach (var gatewayConnection in m_gatewayConnectionMap) {
                if (gatewayConnection.Value.ConnectState == true) {
                    var myKey = m_ipMap.FirstOrDefault(x => x.Value == gatewayConnection.Key).Key;
                    m_dataGridView.Rows.Add(new object[] { gatewayConnection.Key, "連線中", gatewayConnection.Value.getLatestUpdateTime().ToString("yyyy/MM/dd HH:mm:ss"), myKey });
                    m_dataGridView.Rows[m_dataGridView.Rows.Count - 1].Cells[1].Style.ForeColor = Info.NORMAL_COLOR;
                }
                else {
                    var myKey = m_ipMap.FirstOrDefault(x => x.Value == gatewayConnection.Key).Key;
                    m_dataGridView.Rows.Add(new object[] { gatewayConnection.Key, "未連線", gatewayConnection.Value.getLatestUpdateTime().ToString("yyyy/MM/dd HH:mm:ss"), myKey });
                    m_dataGridView.Rows[m_dataGridView.Rows.Count - 1].Cells[1].Style.ForeColor = Info.ALARM_COLOR;
                }
            }
        }
    }
}
