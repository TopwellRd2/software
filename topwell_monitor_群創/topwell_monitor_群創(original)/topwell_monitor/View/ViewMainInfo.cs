﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    class ViewMainInfo
    {
        public DataGridView m_dataGridView;
        Dictionary<string, RegionModel> m_regionModelMap;

        private delegate void dataGridDelegate();

        public ViewMainInfo(DataGridView dataGridView, Dictionary<string, RegionModel> regionModelMap)
        {
            m_dataGridView = dataGridView;
            m_regionModelMap = regionModelMap;
        }

        public void update()
        {
            dataGridUpdate();
        }

        private void dataGridUpdate()
        {
            if (m_dataGridView.InvokeRequired)
            {
                dataGridDelegate callBack = new dataGridDelegate(dataGridUpdate);
                m_dataGridView.Invoke(callBack);
            }
            else
                updateDataGrid();
        }

        private void updateDataGrid()
        {
            m_dataGridView.Rows.Clear();

            foreach (var regionModel in m_regionModelMap)
                m_dataGridView.Rows.Add(new object[] { regionModel.Key, 
                                        regionModel.Value.DisconnectNum, 
                                        regionModel.Value.NormalNum, 
                                        regionModel.Value.StopNum,
                                        regionModel.Value.AlarmNum,
                                        regionModel.Value.BypassNum,
                                        regionModel.Value.getProperRate() + "%",
                                        regionModel.Value.TotalAirVolume,
                                        regionModel.Value.Area,
                                        regionModel.Value.TotalAirVolume/regionModel.Value.Area}); 
        }
    }
}
