﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    public struct FFUParemeter 
    {
        public double a;
        public double b;
        public double c;
        public double d;
    }

    public abstract class ParameterBase
    {
        abstract public FFUParemeter getParameter(int speed);

        public FFUParemeter createParameter(double a, double b, double c, double d)
        {
            FFUParemeter p = new FFUParemeter();

            p.a = a;
            p.b = b;
            p.c = c;
            p.d = d;

            return p;
        }
    }
}
