﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

using RegisterData = System.Collections.Generic.Dictionary<int, int[]>; //id:data(起停、設定轉速、警報...) 

namespace topwell_monitor
{
    public class GatewayConnection
    {
        public static readonly ushort READ_GATEWAY_DEVICE_COUNT = 8; //一次讀取gateway的數量

        public string Ip { get; private set; }
        public bool ConnectState { get; private set; }
        public int ErrorTransmission { get; private set; }

        private int timeOutCount = 0;

        private GatewayInfo m_gatewayInfo;
        private TcpClient m_myTcpClient;
        private NetworkStream m_myNetworkStream;

        private Dictionary<int, RegisterData> m_registerDataMap = new Dictionary<int,RegisterData>();

        private DateTime m_latestUpdateTime; 

        private Thread m_thread;

        private int m_tcpCommunicationTime = 0;

        public GatewayConnection(String ip, GatewayInfo gatewayInfo)
        {
            Ip = ip;
            m_gatewayInfo = gatewayInfo;
            ErrorTransmission = 0;

            m_latestUpdateTime = DateTime.Now;

            //m_thread = new Thread(connect);
            ConnectState = false;

            for (int i = 0; i < GatewayInfo.REGISTER_TOTAL; i++)
            {
                var ffuCnt = gatewayInfo.RegisterFFUCountAry[i];
                var smfCnt = gatewayInfo.RegisterSMFCountAry[i];

                if (ffuCnt > 0 || smfCnt > 0)
                {
                    RegisterData registerData = new RegisterData();

                    for (int j = 0; j < ffuCnt; j++)
                        registerData.Add(j + 1,new int[10]);

                    for (int j = 0; j < smfCnt; j++)
                        registerData.Add(j + 64, new int[10]);

                    m_registerDataMap.Add(i + 1, registerData);
                }
            }
        }

        public DateTime getLatestUpdateTime()
        {
            return m_latestUpdateTime;
        }

        public int[] getDeviceDataAry(int register,int id)
        {
            return m_registerDataMap[register][id];
        }

        public void doAction()
        { 
            if (ConnectState == false)
            {
                if (m_thread == null ||
                    m_thread.ThreadState == ThreadState.Stopped  ||
                    m_thread.ThreadState == ThreadState.Unstarted ) 
                {
                    m_thread = new Thread(connect);
                    m_thread.IsBackground = true;
                    m_thread.Start();
                }
            }
            else
            {
                //每一小關閉TcpClient 
                if (++m_tcpCommunicationTime > 700)
                {
                    m_tcpCommunicationTime = 0;

                    try
                    {
                        if (m_myTcpClient != null)
                            m_myTcpClient.Close();
                        if (m_myNetworkStream != null)
                            m_myNetworkStream.Close();

                        m_myTcpClient = new TcpClient(Ip, 502);

                        if (m_myTcpClient.Connected)
                        {
                            m_myNetworkStream = m_myTcpClient.GetStream();
                            m_myNetworkStream.WriteTimeout = 5000;
                            m_myNetworkStream.ReadTimeout = 5000;  //設定Timeout 有可能讀不到 程式會卡住
                            ConnectState = true;
                        }
                        else
                        {
                            ConnectState = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        ConnectState = false;
                    }
                }
                else
                {
                    for (int i = 0; i < GatewayInfo.REGISTER_TOTAL; i++)
                    {
                        var ffuCnt = m_gatewayInfo.RegisterFFUCountAry[i];
                        var smfCnt = m_gatewayInfo.RegisterSMFCountAry[i];

                        int idCnt = 1;

                        while (idCnt <= ffuCnt && ConnectState)
                        {
                            requestaDeviceData(i + 1, idCnt);
                            idCnt += READ_GATEWAY_DEVICE_COUNT;
                            Thread.Sleep(50);
                        }

                        idCnt = 1;

                        while (idCnt <= smfCnt && ConnectState)
                        {
                            requestaDeviceData(i + 1, idCnt + 63);
                            idCnt += READ_GATEWAY_DEVICE_COUNT;
                            Thread.Sleep(50);
                        }
                    }
                }

                m_latestUpdateTime = DateTime.Now;

            }
        }

        public void connect()
        {
            Ping ping = new Ping();
            PingReply reply = null;

            try
            {
                reply = ping.Send(Ip);
            }
            catch (Exception e)
            {
                ConnectState = false;
                closeThread();
                return;
            }

            if (reply.Status == IPStatus.Success){
            }
            else{
                ConnectState = false;
                closeThread();
                return;
            }

            try
            {
                if (m_myTcpClient != null)
                    m_myTcpClient.Close();
                if (m_myNetworkStream != null)
                    m_myNetworkStream.Close();

                m_myTcpClient = new TcpClient(Ip, 502);

                if (m_myTcpClient.Connected){
                    m_myNetworkStream = m_myTcpClient.GetStream();
                    m_myNetworkStream.WriteTimeout = 5000;
                    m_myNetworkStream.ReadTimeout = 5000;  //設定Timeout 有可能讀不到 程式會卡住
                    ConnectState = true;
                }
                else{
                    ConnectState = false;
                }
            }
            catch (Exception ex) 
            {
                ConnectState = false;
                closeThread();
                return;
            }
            closeThread();
            return;
        }

        private void closeThread()
        {
            if (m_thread != null)
            {
                m_thread.Abort();
                m_thread = null;
            }    
        }

        private bool requestaDeviceData(int register, int startId)
        {
            byte[] inputByte = new byte[12];
            byte[] outputByte = new byte[9 + (READ_GATEWAY_DEVICE_COUNT * m_gatewayInfo.ReadLength * 2)];
            int startAddress = startId  * m_gatewayInfo.ReadLength;

            inputByte[0] = 0;    //Tcp Header
            inputByte[1] = 1;    //Tcp Header
            inputByte[2] = 0;    //Tcp Header
            inputByte[3] = 0;    //Tcp Header
            inputByte[4] = 0;    //Tcp Header
            inputByte[5] = 6;    //Tcp Header

            inputByte[6] = Convert.ToByte(register);    //Tcp Header   

            inputByte[7] = 3;    //Function Code

            byte[] _adr = BitConverter.GetBytes(startAddress);
            inputByte[8] = _adr[1];   //startAddress
            inputByte[9] = _adr[0];   //startAddress 

            byte[] _length = BitConverter.GetBytes(READ_GATEWAY_DEVICE_COUNT * m_gatewayInfo.ReadLength);
            inputByte[10] = _length[1];   //Data Length
            inputByte[11] = _length[0];   //Data Length

            try {
                m_myNetworkStream.Write(inputByte, 0, inputByte.Length);
                m_myNetworkStream.Read(outputByte, 0, outputByte.Length);
            }
            catch (TimeoutException e)
            {
                ++timeOutCount;
                MessageBox.Show("ip:"+Ip +"timeout 第"+ timeOutCount.ToString() + "次   " + e.ToString());
                ConnectState = false;                
                return false;
            }
            catch (Exception ex){
                //MessageBox.Show(ex.ToString());
                ConnectState = false;
                return false;
            }

            if (outputByte[0] != 0 || outputByte[1] != 1) {
                ErrorTransmission++;
                return false;
            }
            else {
                ErrorTransmission = 0;
                RegisterData registerData = m_registerDataMap[register];
                for (int i = 0; i < READ_GATEWAY_DEVICE_COUNT; i++) {
                    if (registerData.ContainsKey(startId + i)) {
                        for (int j = 0; j < m_gatewayInfo.ReadLength; j++) {
                            int data = outputByte[9 + (i * 2 * m_gatewayInfo.ReadLength) + (2 * j)];
                            data = (data << 8) + outputByte[10 + (i * 2 * m_gatewayInfo.ReadLength) + (2 * j)];
                            registerData[startId + i][j] = data;
                        }
                    }
                }
            }
            return true;
        }

        public bool writeOperation(int register, int startId, bool value)
        {
            int operation = value ? 1 : 0;
            bool res = writeData(register, startId, 0, operation);
            return res;
        }

        public bool writeSettingSpeed(int register, int startId, int value)
        {
            bool res = writeData(register, startId, 1, value);
            return res;
        }

        private bool writeData(int register, int startId, int pos, int value)
        {
            byte[] inputByte = new byte[12];
            byte[] outputByte = new byte[12];
            int startAddress = startId * m_gatewayInfo.ReadLength + pos;

            inputByte[0] = 0;    //Tcp Header
            inputByte[1] = 1;    //Tcp Header
            inputByte[2] = 0;    //Tcp Header
            inputByte[3] = 0;    //Tcp Header
            inputByte[4] = 0;    //Tcp Header
            inputByte[5] = 6;    //Tcp Header

            inputByte[6] = Convert.ToByte(register);    //Tcp Header   

            inputByte[7] = 6;    //Function Code  6:寫入

            byte[] _adr = BitConverter.GetBytes(startAddress);
            inputByte[8] = _adr[1];    //Tcp Header
            inputByte[9] = _adr[0];    //Tcp Header

            byte[] _length = BitConverter.GetBytes(value);
            inputByte[10] = _length[1];    //Tcp Header
            inputByte[11] = _length[0];    //Tcp Header

            try {
                m_myNetworkStream.Write(inputByte, 0, inputByte.Length);
                m_myNetworkStream.Read(outputByte, 0, outputByte.Length);
            }
            catch (Exception ex) {
                ConnectState = false;
                return false;
            }

            //確認寫入是否成功  (Slave會 Echo相同訊息  表示寫入成功)
            for (int i = 0; i < 12; i++) {
                if (inputByte[i] != outputByte[i])
                    return false;
            }
            return true;
        }

    }
}
