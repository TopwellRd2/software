﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topwell_monitor
{
    public enum DeviceTypeEnum { Single, Double, ACSingle }
    public enum DeviceStatusEnum { Normal, Stop, FFUAlarm, SMFAlarm, Disconnect, Bypass }

    class Device
    {
        private FFUModel m_ffuModel;

        private SMFModel m_smfModel;

        private ParameterBase m_parameter;

        public string Ip { get; private set; }

        public int Id { get; private set; }

        public int Register { get; private set; }

        public bool IsBypass { get; set; }

        public double AirVolume { get; private set; }

        public int SettingSpeed
        {
            get
            {
                return m_ffuModel.SettingSpeed;
            }
            private set { }
        }

        public int ActualSpeed
        {
            get
            {
                return m_ffuModel.ActualSpeed;
            }
            private set{}
        }

        public bool Operation
        {
            get
            {
                return m_ffuModel.Operation;
            }
            private set{}
        }

        public int AlarmCode
        {
            get
            {
                return m_ffuModel.AlarmCode;
            }
            private set { }
        }

        public double Temperature
        {
            get
            {
                return m_smfModel.Temperature;
            }
            private set { }
        }

        public double Pressure
        {
            get
            {
                return m_smfModel.Pressure;
            }
            private set { }
        }

        public double Humidity
        {
            get
            {
                return m_smfModel.Humidity;
            }
            private set { }
        }

        public DeviceTypeEnum DeviceType { get; private set; }
        public DeviceStatusEnum Status { get; private set; }

        public Device(DeviceTypeEnum deciceType, ParameterBase parameter, FFUModel ffuModel, SMFModel smfModel = null)
        {
            m_ffuModel = ffuModel;
            m_smfModel = smfModel;
            DeviceType = deciceType;
            IsBypass = false;
            Status = DeviceStatusEnum.Disconnect;

            m_parameter = parameter;

            Ip = m_ffuModel.Ip;
            Id = m_ffuModel.Id;
            Register = m_ffuModel.Register;
        }

        public FFUModel getFFUModel()
        {
            return m_ffuModel;
        }

        public SMFModel getSMFModel()
        {
            return m_smfModel;
        }

        public bool updateModel(Dictionary<string, GatewayConnection> gatewayMap)
        {
            bool ret = true;
            int[] datas;

            if (m_ffuModel != null)
            {
                if (gatewayMap[m_ffuModel.Ip].ConnectState)
                {
                    datas = gatewayMap[m_ffuModel.Ip].getDeviceDataAry(m_ffuModel.Register, m_ffuModel.Id);
                    ret = m_ffuModel.setData(datas);
                }
                else 
                    ret = m_ffuModel.setData(new int[] { }); //給空陣列 表示斷線               
            }

            if (m_smfModel != null)
            {
                if (gatewayMap[m_ffuModel.Ip].ConnectState) {
                    if(m_ffuModel.RegionName == Info.REGION_SORC01 && m_ffuModel.Id <= 11) {
                        datas = gatewayMap[m_ffuModel.Ip].getDeviceDataAry(m_ffuModel.Register, m_ffuModel.Id);
                        ret = ret & m_smfModel.setYsdData(datas);
                    } else if (m_ffuModel.RegionName == Info.REGION_STKT10){
                        datas = gatewayMap[m_ffuModel.Ip].getDeviceDataAry(m_ffuModel.Register, m_ffuModel.Id);
                        ret = ret & m_smfModel.setYsdData(datas);
                    } else {
                        datas = gatewayMap[m_smfModel.Ip].getDeviceDataAry(m_smfModel.Register, m_smfModel.Id);
                        ret = ret & m_smfModel.setData(datas);
                    }
                }
                else
                    ret = ret & m_smfModel.setData(new int[] { });//給空陣列 表示斷線

                FFUParemeter parameter = m_parameter.getParameter(m_ffuModel.ActualSpeed);
                double pressure = m_smfModel.Pressure;

                //風量 = A*x*x*x + B*x*x + C*x + D
                AirVolume = parameter.a * pressure * pressure * pressure +
                            parameter.b * pressure * pressure +
                            parameter.c * pressure +
                            parameter.d;

                if (m_ffuModel.Operation == false)
                    AirVolume = 0;
            }

            if (DeviceType == DeviceTypeEnum.Double)
            {
                if (m_smfModel.Status == SMFStatus.Alarm)
                    Status = DeviceStatusEnum.SMFAlarm;
                else if (m_smfModel.Status == SMFStatus.Disconnect)
                    Status = DeviceStatusEnum.Disconnect;

                if (m_ffuModel.Status == FFUStatus.Stop)
                    Status = DeviceStatusEnum.Stop;
                else if (m_ffuModel.Status == FFUStatus.Alarm)
                    Status = DeviceStatusEnum.FFUAlarm;
                else if (m_ffuModel.Status == FFUStatus.Disconnect)
                    Status = DeviceStatusEnum.Disconnect;

                if (m_ffuModel.Status == FFUStatus.Normal && m_smfModel.Status == SMFStatus.Normal)
                    Status = DeviceStatusEnum.Normal;
            }
            else
            {
                if (m_ffuModel.Status == FFUStatus.Normal)
                    Status = DeviceStatusEnum.Normal;
                else if (m_ffuModel.Status == FFUStatus.Stop)
                    Status = DeviceStatusEnum.Stop;
                else if (m_ffuModel.Status == FFUStatus.Alarm)
                    Status = DeviceStatusEnum.FFUAlarm;
                else if (m_ffuModel.Status == FFUStatus.Disconnect)
                    Status = DeviceStatusEnum.Disconnect;
            }

            if (IsBypass)
                Status = DeviceStatusEnum.Bypass;

            return ret;
        }        
    }
}
