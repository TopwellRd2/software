﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public enum SMFStatus { Normal, Alarm, Disconnect }
    public enum SMFAlarmType { None, Pressure, Temperature, Humidity }
    class SMFModel : BaseModel
    {
        public Double Pressure { get; private set; }
        public Double Temperature { get; private set; }
        public Double Humidity { get; private set; }
        public SMFStatus Status { get; private set; }
        public SMFAlarmType AlarmType { get; private set; }  //還未實做 以後可能要知道哪種警報

        public int PressureAlarmSettingLow { get; private set; }
        public int PressureAlarmSettingHight { get; private set; }

        public int TemperatureAlarmSettingLow { get; private set; }
        public int TemperatureAlarmSettingHight { get; private set; }

        public int HumidityAlarmSettingLow { get; private set; }
        public int HumidityAlarmSettingHight { get; private set; }
        

        public SMFModel(string regionName, string ip, int register, int id, ReadLengthEnum readLength)
            : base(regionName, ip, register, id, readLength)
        {
            Status = SMFStatus.Disconnect;
            AlarmType = SMFAlarmType.None;

            readAlarmIni();
        }

        public override bool setData(int[] data)
        {
            if (data.Length != ReadLength) {
                Status = SMFStatus.Disconnect;
                return false;
            }
               
            Pressure = data[2] / 10.0F;
            Temperature = data[6] / 10.0F; ;
            Humidity = data[7] / 10.0F; ;

            Status = SMFStatus.Normal;

            if (Pressure < PressureAlarmSettingLow       || Pressure > PressureAlarmSettingHight ||
                Temperature < TemperatureAlarmSettingLow || Temperature > TemperatureAlarmSettingHight ||
                Humidity < HumidityAlarmSettingLow       || Humidity > HumidityAlarmSettingHight)
            {
                Status = SMFStatus.Alarm;
            }

            return true;
        }

        public override bool setYsdData(int[] data)
        {
            if (data.Length != ReadLength)
            {
                Status = SMFStatus.Disconnect;
                return false;
            }

            Pressure = data[8];
            Temperature = 0 ;
            Humidity = 0 ;

            Status = SMFStatus.Normal;

            if (Pressure < PressureAlarmSettingLow || Pressure > PressureAlarmSettingHight ||
                Temperature < TemperatureAlarmSettingLow || Temperature > TemperatureAlarmSettingHight ||
                Humidity < HumidityAlarmSettingLow || Humidity > HumidityAlarmSettingHight)
            {
                Status = SMFStatus.Alarm;
            }

            return true;
        }

        private void readAlarmIni()
        {
            IniHandler iniHandler = new IniHandler(Application.StartupPath + "/SmfAlarmSetting.ini");

            if (iniHandler.isExistIni() == false) {
                MessageBox.Show("缺少 SmfAlarmSetting.ini 檔案");
            }
            else {
                string str = iniHandler.ReadIniFile(RegionName, Id.ToString(), "default");
                if (str == "default")
                    MessageBox.Show(RegionName + Id + "AlarmSetting ERROR!!!");
                else
                {
                    string[] dataAry = str.Split('|');

                    PressureAlarmSettingLow = Convert.ToInt32(dataAry[0].Split(',')[0]);
                    PressureAlarmSettingHight = Convert.ToInt32(dataAry[0].Split(',')[1]);

                    TemperatureAlarmSettingLow = Convert.ToInt32(dataAry[1].Split(',')[0]);
                    TemperatureAlarmSettingHight = Convert.ToInt32(dataAry[1].Split(',')[1]);

                    HumidityAlarmSettingLow = Convert.ToInt32(dataAry[2].Split(',')[0]);
                    HumidityAlarmSettingHight = Convert.ToInt32(dataAry[2].Split(',')[1]);
                }
            }
        }

        public bool changeAlarmSetting(string str)
        {
            try {
                IniHandler iniHandler = new IniHandler(Application.StartupPath + "/SmfAlarmSetting.ini");

                if (iniHandler.isExistIni() == false)
                    MessageBox.Show("缺少 SmfAlarmSetting.ini 檔案");
                else
                    iniHandler.WriteIniFile(RegionName, Id.ToString(), str);

                readAlarmIni();//讀回來 改變MODEL現在的值
            }
            catch (Exception ex) {
                return false;
            }
            return true;
        }
    }
}
