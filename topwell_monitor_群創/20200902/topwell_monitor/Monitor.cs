﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Timers;

namespace topwell_monitor
{
    public class Monitor
    {
        private System.Timers.Timer m_gatewayUpdateTimer;
        private System.Timers.Timer m_viewUpdateTimer;
        private System.Timers.Timer m_mappTimer;
        private System.Timers.Timer m_historyTimer;

        public Form1 MainForm;
        //Key:ip  Value:GatewayConnection
        private Dictionary<string, GatewayConnection> m_gatewayMap = new Dictionary<string,GatewayConnection>();
        //Key:RegionName  Value:Ip
        private Dictionary<string, string> m_ipMap = new Dictionary<string, string>();
        //Key:RegionName  Value:RegionModel
        private Dictionary<string, RegionModel> m_regionModelMap = new Dictionary<string, RegionModel>();
        //Key:RegionName  Value:View
        private Dictionary<string, RegionView> m_viewMap = new Dictionary<string, RegionView>();
        //Key:RegionName  Value:Xml Export path
        private Dictionary<string, string> m_xmlPathMap = new Dictionary<string, string>();
        //Key:RegionName  Value:area
        private Dictionary<string, string> m_areaMap = new Dictionary<string, string>();
        
        private ViewMainInfo m_viewDataGrid;

        private ViewAlarm m_viewAlarm;
        private ViewGateway m_viewGateway;
        private ViewLog m_viewLog;

        private MappClient m_mappClient;//目前未開啟

        private delegate void UiUpdatedelegate(Control c, string s);

        public Monitor(Form1 form)
        {
            MainForm = form;
            
            m_mappClient = new MappClient();
            form.MappIpLabel.Text = m_mappClient.Ip;

            string[] RegionArray = {Info.REGION_OHGT01,
                                    Info.REGION_OHGT03,
                                    Info.REGION_STKL21_L30,
                                    Info.REGION_STKL25_L10,
                                    Info.REGION_STKT10,
                                    Info.REGION_SORC01 };

            m_ipMap = initialMap(m_ipMap, RegionArray);
            m_xmlPathMap = initialMap(m_xmlPathMap, RegionArray);
            m_areaMap = initialMap(m_areaMap, RegionArray);

            readIni2IpMap();
            readIni2PathMap();
            readIni2AreaMap();

            //ADD GATEWAY 
            //======================================================
            addGateway(Info.REGION_OHGT01, new ushort[] { 9, 0, 0, 0, 0, 0 }, new ushort[] { 9, 0, 0, 0, 0, 0 }, ReadLengthEnum.Ten);
            addGateway(Info.REGION_OHGT03, new ushort[] { 13, 0, 0, 0, 0, 0 }, new ushort[] { 13, 0, 0, 0, 0, 0 }, ReadLengthEnum.Ten);
            addGateway(Info.REGION_STKL21_L30, new ushort[] { 12, 27, 0, 0, 0, 0 }, new ushort[] { 13, 0, 0, 0, 0, 0 }, ReadLengthEnum.Six);
            addGateway(Info.REGION_STKL25_L10, new ushort[] { 11, 0, 0, 0, 0, 0 }, new ushort[] { 0, 0, 0, 0, 0, 0 }, ReadLengthEnum.Six);
            addGateway(Info.REGION_STKT10, new ushort[] { 8, 0, 0, 0, 0, 0 }, new ushort[] { 8, 0, 0, 0, 0, 0 }, ReadLengthEnum.Ten);
            addGateway(Info.REGION_SORC01, new ushort[] { 36, 0, 0, 0, 0, 0 }, new ushort[] { 36, 0, 0, 0, 0, 0 }, ReadLengthEnum.Ten);
            //======================================================

            //======================================================
            List<Device> deviceList = new List<Device>();
            ReadLengthEnum readLength;
            string regionIp;
            //======================================================

            // OHGT01
            regionIp = m_ipMap[Info.REGION_OHGT01];
            readLength = ReadLengthEnum.Ten;
            for (int i = 1; i <= 9; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200480(), new FFUModel(Info.REGION_OHGT01, regionIp, 1, i, readLength), new SMFModel(Info.REGION_OHGT01, regionIp, 1, i + 63, readLength)));
            }
            m_regionModelMap.Add(Info.REGION_OHGT01, new RegionModel(deviceList, m_areaMap[Info.REGION_OHGT01]));

            // OHGT03
            deviceList = new List<Device>();
            regionIp = m_ipMap[Info.REGION_OHGT03];
            readLength = ReadLengthEnum.Ten;
            for (int i = 1; i <= 13; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_OHGT03, regionIp, 1, i, readLength), new SMFModel(Info.REGION_OHGT03, regionIp, 1, i + 63, readLength)));
            }
            m_regionModelMap.Add(Info.REGION_OHGT03, new RegionModel(deviceList, m_areaMap[Info.REGION_OHGT03]));

            // STKL21_L30
            deviceList = new List<Device>();
            regionIp = m_ipMap[Info.REGION_STKL21_L30];
            readLength = ReadLengthEnum.Six;
            for (int i = 1; i <= 12; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.ACSingle, new P_None(), new FFUModel(Info.REGION_STKL21_L30, regionIp, 1, i, readLength)));
            }
            for (int i = 1; i <= 27; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.ACSingle, new P_None(), new FFUModel(Info.REGION_STKL21_L30, regionIp, 2, i, readLength)));
            }
            m_regionModelMap.Add(Info.REGION_STKL21_L30, new RegionModel(deviceList, m_areaMap[Info.REGION_STKL21_L30]));

            // STKL25_L10
            deviceList = new List<Device>();
            regionIp = m_ipMap[Info.REGION_STKL25_L10];
            readLength = ReadLengthEnum.Six;
            for (int i = 1; i <= 11; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.ACSingle, new P_None(), new FFUModel(Info.REGION_STKL25_L10, regionIp, 1, i, readLength)));
            }
            m_regionModelMap.Add(Info.REGION_STKL25_L10, new RegionModel(deviceList, m_areaMap[Info.REGION_STKL25_L10]));

            // STKT10
            deviceList = new List<Device>();
            regionIp = m_ipMap[Info.REGION_STKT10];
            readLength = ReadLengthEnum.Ten;
            int y = 0;
            for (int i = 1; i <= 8; i++)
            {
                if (i % 2 != 0){
                    y++;
                }
                deviceList.Add(new Device(DeviceTypeEnum.Double, new P_None(), new FFUModel(Info.REGION_STKT10, regionIp, 1, i, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, y + 63, readLength)));
            }
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 1, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 64, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 2, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 64, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 3, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 65, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 4, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 65, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 5, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 66, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 6, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 66, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 7, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 67, readLength)));
            //deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200367(), new FFUModel(Info.REGION_STKT10, regionIp, 1, 8, readLength), new SMFModel(Info.REGION_STKT10, regionIp, 1, 67, readLength)));
            m_regionModelMap.Add(Info.REGION_STKT10, new RegionModel(deviceList, m_areaMap[Info.REGION_STKT10]));

            // SORC01
            deviceList = new List<Device>();
            regionIp = m_ipMap[Info.REGION_SORC01];
            readLength = ReadLengthEnum.Ten;
            for (int i = 1; i <= 11; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200577(), new FFUModel(Info.REGION_SORC01, regionIp, 1, i, readLength), new SMFModel(Info.REGION_SORC01, regionIp, 1, i + 63, readLength)));
            }
            for (int i = 12; i <= 36; i++)
            {
                deviceList.Add(new Device(DeviceTypeEnum.Double, new P_4200312(), new FFUModel(Info.REGION_SORC01, regionIp, 1, i, readLength), new SMFModel(Info.REGION_SORC01, regionIp, 1, i + 63, readLength)));
            }
            m_regionModelMap.Add(Info.REGION_SORC01, new RegionModel(deviceList, m_areaMap[Info.REGION_SORC01]));

            m_viewMap.Add(Info.REGION_OHGT01, new RegionView(this,MainForm.OHGT01Btn, new Form_OHGT01(), m_regionModelMap[Info.REGION_OHGT01], MainForm.getLoginHandler()));
            m_viewMap.Add(Info.REGION_OHGT03, new RegionView(this, MainForm.OHGT03Btn, new Form_OHGT03(), m_regionModelMap[Info.REGION_OHGT03], MainForm.getLoginHandler()));
            m_viewMap.Add(Info.REGION_STKL21_L30, new RegionView(this, MainForm.STKL21_L30Btn, new Form_STKL21_L30(), m_regionModelMap[Info.REGION_STKL21_L30], MainForm.getLoginHandler()));
            m_viewMap.Add(Info.REGION_STKL25_L10, new RegionView(this, MainForm.STKL25_L10Btn, new Form_STKL25_L10(), m_regionModelMap[Info.REGION_STKL25_L10], MainForm.getLoginHandler()));
            m_viewMap.Add(Info.REGION_STKT10, new RegionView(this, MainForm.STKT10Btn, new Form_STKT10(), m_regionModelMap[Info.REGION_STKT10], MainForm.getLoginHandler()));
            m_viewMap.Add(Info.REGION_SORC01, new RegionView(this, MainForm.SORC01Btn, new Form_SORC01(), m_regionModelMap[Info.REGION_SORC01], MainForm.getLoginHandler()));

            m_viewDataGrid = new ViewMainInfo(MainForm.InfoDataGrid, m_regionModelMap);

            m_viewAlarm = new ViewAlarm(MainForm.form_Alarm.DataGridView1, m_regionModelMap);
            m_viewGateway = new ViewGateway(MainForm.form_GatewayInfo.DataGridView1, m_gatewayMap, m_ipMap);
            m_viewLog = new ViewLog(MainForm.form_Log.LogTxt);

            //TIMER
            //===========================================================================
            m_gatewayUpdateTimer = new System.Timers.Timer();
            m_gatewayUpdateTimer.Interval = 3000;
            m_gatewayUpdateTimer.Elapsed += new ElapsedEventHandler(updatetAction);

            m_viewUpdateTimer = new System.Timers.Timer();
            m_viewUpdateTimer.Interval = 1500;
            m_viewUpdateTimer.Elapsed += new ElapsedEventHandler(updatetViewAction);
             
            m_mappTimer = new System.Timers.Timer();
            m_mappTimer.Interval = 5000;
            m_mappTimer.Elapsed += new ElapsedEventHandler(mappAction);

            m_historyTimer = new System.Timers.Timer();
            //m_historyTimer.Interval = 5000;
            m_historyTimer.Interval = 900000;
            m_historyTimer.Elapsed += new ElapsedEventHandler(historyAction);
            
            //===========================================================================
        }
        
        /*@brief 新增gateway
         *@param name 名稱
         *@param ffuCnt gateway 每個迴路有幾個ffu  {9,6,0,0,0,0}表示 第一個迴路有9台ffu 第二個迴路有6台ffu
         *@param smfCnt gateway 每個迴路有幾個smf  {9,6,0,0,0,0}表示 第一個迴路有9台smf 第二個迴路有6台smf
         *@param readLength gateway讀一個裝置的資料長度(目前有6個word和10個word)
        */
        private void addGateway(string name, ushort[] ffuCnt, ushort[] smfCnt, ReadLengthEnum readLength)
        {
            string regionIp;
            GatewayInfo gatewayInfo;

            if (m_ipMap.ContainsKey(name)) {
                regionIp = m_ipMap[name];
                gatewayInfo = new GatewayInfo(ffuCnt, smfCnt, readLength);
                m_gatewayMap.Add(regionIp, new GatewayConnection(regionIp, gatewayInfo));
            }
            else {
                MessageBox.Show("addGateway error !!");
            } 
        }

        private void readIni2IpMap()
        {
            IniHandler iniHandler = new IniHandler(Application.StartupPath + "/config.ini");

            if (iniHandler.isExistIni() == false) {
                MessageBox.Show("缺少 config.ini 檔案");
            }
            else {
                string[] keyAry = m_ipMap.Keys.ToArray();
                foreach (string name in keyAry) {
                    string ip = iniHandler.ReadIniFile("IP", name, "default");
                    if (ip == "default")
                        MessageBox.Show(name + "IP ERROR!!!");
                    else
                        m_ipMap[name] = ip;
                }
            }
        }

        private void readIni2PathMap()
        {
            IniHandler iniHandler = new IniHandler(Application.StartupPath + "/config.ini");

            if (iniHandler.isExistIni() == false) {
                MessageBox.Show("缺少 config.ini 檔案");
            }
            else {
                string[] keyAry = m_xmlPathMap.Keys.ToArray();
                foreach (string name in keyAry)
                {
                    string path = iniHandler.ReadIniFile("XML_Path", name, "default");
                    if (path == "default")
                        MessageBox.Show(name + "XML_Path ERROR!!!");
                    else
                        m_xmlPathMap[name] = path;
                }
            }
        }

        private void readIni2AreaMap()
        {
            IniHandler iniHandler = new IniHandler(Application.StartupPath + "/config.ini");

            if (iniHandler.isExistIni() == false)
            {
                MessageBox.Show("缺少 config.ini 檔案");
            }
            else
            {
                string[] keyAry = m_areaMap.Keys.ToArray();
                foreach (string name in keyAry)
                {
                    string area = iniHandler.ReadIniFile("Area", name, "default");
                    if (area == "default")
                        m_areaMap[name] = "0";
                    else
                        m_areaMap[name] = area;
                }
            }
        }

        public void start()
        {
            m_gatewayUpdateTimer.Start();
            m_viewUpdateTimer.Start();
            //m_mappTimer.Start();
            m_historyTimer.Start();
        }

        private void updatetViewAction(object sender, EventArgs e)
        {
            foreach (KeyValuePair<string, RegionView> kvp in m_viewMap)
                kvp.Value.update();

            //if (XmlHandler.isTime2Export())
            //{
            //    foreach (KeyValuePair<string, string> kvp in m_xmlPathMap)
            //        XmlHandler.exportXml(kvp.Key, m_regionModelMap[kvp.Key].getProperRate(), kvp.Value);
            //}

            m_viewDataGrid.update();
            //if (m_form.form_Alarm.Visible)
            m_viewAlarm.update();
            //if (m_form.form_GatewayInfo.Visible)
            m_viewGateway.update();
        }

        private void updatetAction(object sender, EventArgs e)
        {
            m_gatewayUpdateTimer.Stop();

            uiUpdate(MainForm.DateLabel, DateTime.Now.ToString("yyyy/MM/dd"));
            uiUpdate(MainForm.TimeLabel, DateTime.Now.ToString("HH:mm"));
      
            foreach (KeyValuePair<string, GatewayConnection> kvp in m_gatewayMap)
                kvp.Value.doAction();

            foreach (KeyValuePair<string, RegionModel> kvp in m_regionModelMap)
                kvp.Value.updateDevicesData(m_gatewayMap);

            m_gatewayUpdateTimer.Start();
        }

        private void mappAction(object sender, EventArgs e)
        {
            m_mappTimer.Stop();

            if (m_mappClient.Connected) 
                m_mappClient.writeData("1");
            else 
                m_mappClient.connectServer();

            m_mappTimer.Start();
        }

        private void historyAction(object sender, EventArgs e)
        {
            historyAction();
        }

        private void historyAction()
        {
            foreach (KeyValuePair<string, RegionModel> kvp in m_regionModelMap)
            {
                string msg = "";
                bool isAnyAlrm = false;

                foreach (var device in kvp.Value.getDeviceList())
                {
                    if (device.Status == DeviceStatusEnum.FFUAlarm)
                    {
                        isAnyAlrm = true;
                        string nowDateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        msg += "[" + nowDateTime + "]  " + kvp.Key + "_" + device.Id + "  發生" + device.AlarmCode.ToString() + "警報" + "\r\n";
                        //if (m_mappClient.Connected)
                        //{
                        //    if (m_mappClient.writeData("$" + msg))
                        //        m_viewLog.writeLog(nowDateTime + "已傳送訊息至MAPP SERVER", Info.NORMAL_COLOR);
                        //    else
                        //        m_viewLog.writeLog(nowDateTime + "傳送失敗", Info.ALARM_COLOR);
                        //}
                        //else
                        //{
                        //    //m_viewLog.writeLog(nowDateTime + "MAPP 未連線 ", Info.ALARM_COLOR);
                        //}
                    }
                }

                if (isAnyAlrm)
                    m_viewLog.writeLog(msg, Info.ALARM_COLOR);
            }
        }

        public void firstGatewayAction()
        {
            foreach (KeyValuePair<string, GatewayConnection> kvp in m_gatewayMap)
                kvp.Value.connect();
            foreach (KeyValuePair<string, GatewayConnection> kvp in m_gatewayMap)
                kvp.Value.doAction();
            foreach (KeyValuePair<string, RegionModel> kvp in m_regionModelMap)
                kvp.Value.updateDevicesData(m_gatewayMap);
            foreach (KeyValuePair<string, RegionView> kvp in m_viewMap)
                kvp.Value.update();

            uiUpdate(MainForm.DateLabel, DateTime.Now.ToString("yyyy/MM/dd"));
            uiUpdate(MainForm.TimeLabel, DateTime.Now.ToString("HH:mm"));
             
            m_viewDataGrid.update();
            m_viewAlarm.update();
            m_viewGateway.update();
        }

        private void uiUpdate(Control ctrl, string value)
        {
            if (ctrl.InvokeRequired)
            {
                UiUpdatedelegate callBack = new UiUpdatedelegate(uiUpdate);
                ctrl.Invoke(callBack, ctrl, value);
            }
            else
            {
                ctrl.Text = value;
            }
        }

        private Dictionary<string, string> initialMap(Dictionary<string, string> map, string[] regionArray)
        {
            foreach (String region in regionArray)
            {
                map.Add(region , "");
            }

            return map;
        }



        public Dictionary<string, string> getIpMap()
        {
            return m_ipMap;
        }

        public Dictionary<string, GatewayConnection> getGatewayMap()
        {
            return m_gatewayMap;
        }
    }
}
