﻿namespace topwell_monitor
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.STKT10Btn = new System.Windows.Forms.Button();
            this.maskPic01 = new System.Windows.Forms.PictureBox();
            this.OHGT03Btn = new System.Windows.Forms.Button();
            this.OHGT01Btn = new System.Windows.Forms.Button();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.STKL21_L30Btn = new System.Windows.Forms.Button();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.STKL25_L10Btn = new System.Windows.Forms.Button();
            this.PictureBox3 = new System.Windows.Forms.PictureBox();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.SORC01Btn = new System.Windows.Forms.Button();
            this.PictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.InfoDataGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrackBar1 = new System.Windows.Forms.TrackBar();
            this.LoginBtn = new System.Windows.Forms.Button();
            this.AlarmBtn = new System.Windows.Forms.Button();
            this.GatewayInfoBtn = new System.Windows.Forms.Button();
            this.AlarmCodeInfoBtn = new System.Windows.Forms.Button();
            this.LoginDetectTimer = new System.Windows.Forms.Timer(this.components);
            this.LoginLabel = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.MappStateLabel = new System.Windows.Forms.Label();
            this.MappIpLabel = new System.Windows.Forms.Label();
            this.DateLabel = new System.Windows.Forms.Label();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.LogBtn = new System.Windows.Forms.Button();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.DisconnectLabel = new System.Windows.Forms.Label();
            this.AlarmLabel = new System.Windows.Forms.Label();
            this.StopLabel = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.RunLabel = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maskPic01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.TabPage2.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            this.TabPage3.SuspendLayout();
            this.Panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).BeginInit();
            this.TabPage4.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InfoDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.TabPage3);
            this.TabControl1.Controls.Add(this.TabPage4);
            this.TabControl1.Controls.Add(this.tabPage5);
            this.TabControl1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TabControl1.Location = new System.Drawing.Point(8, 8);
            this.TabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(1769, 721);
            this.TabControl1.TabIndex = 19;
            // 
            // TabPage1
            // 
            this.TabPage1.AutoScroll = true;
            this.TabPage1.Controls.Add(this.Panel1);
            this.TabPage1.Location = new System.Drawing.Point(4, 30);
            this.TabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.TabPage1.Size = new System.Drawing.Size(1761, 687);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "TFT";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // Panel1
            // 
            this.Panel1.AutoScroll = true;
            this.Panel1.Controls.Add(this.STKT10Btn);
            this.Panel1.Controls.Add(this.maskPic01);
            this.Panel1.Controls.Add(this.OHGT03Btn);
            this.Panel1.Controls.Add(this.OHGT01Btn);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(4, 4);
            this.Panel1.Margin = new System.Windows.Forms.Padding(4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1753, 679);
            this.Panel1.TabIndex = 8;
            // 
            // STKT10Btn
            // 
            this.STKT10Btn.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.STKT10Btn.Location = new System.Drawing.Point(755, 1495);
            this.STKT10Btn.Margin = new System.Windows.Forms.Padding(4);
            this.STKT10Btn.Name = "STKT10Btn";
            this.STKT10Btn.Size = new System.Drawing.Size(593, 136);
            this.STKT10Btn.TabIndex = 21;
            this.STKT10Btn.Text = "STKT10";
            this.STKT10Btn.UseVisualStyleBackColor = true;
            // 
            // maskPic01
            // 
            this.maskPic01.Location = new System.Drawing.Point(1375, 2210);
            this.maskPic01.Margin = new System.Windows.Forms.Padding(4);
            this.maskPic01.Name = "maskPic01";
            this.maskPic01.Size = new System.Drawing.Size(72, 131);
            this.maskPic01.TabIndex = 20;
            this.maskPic01.TabStop = false;
            // 
            // OHGT03Btn
            // 
            this.OHGT03Btn.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.OHGT03Btn.Location = new System.Drawing.Point(1257, 2138);
            this.OHGT03Btn.Margin = new System.Windows.Forms.Padding(4);
            this.OHGT03Btn.Name = "OHGT03Btn";
            this.OHGT03Btn.Size = new System.Drawing.Size(173, 120);
            this.OHGT03Btn.TabIndex = 7;
            this.OHGT03Btn.Text = "OHGT03";
            this.OHGT03Btn.UseVisualStyleBackColor = true;
            // 
            // OHGT01Btn
            // 
            this.OHGT01Btn.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.OHGT01Btn.Location = new System.Drawing.Point(1287, 1233);
            this.OHGT01Btn.Margin = new System.Windows.Forms.Padding(4);
            this.OHGT01Btn.Name = "OHGT01Btn";
            this.OHGT01Btn.Size = new System.Drawing.Size(145, 137);
            this.OHGT01Btn.TabIndex = 6;
            this.OHGT01Btn.Text = "OHGT01";
            this.OHGT01Btn.UseVisualStyleBackColor = true;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::topwell_monitor.Properties.Resources.TFT;
            this.PictureBox1.Location = new System.Drawing.Point(0, 0);
            this.PictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(3193, 3065);
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.Panel2);
            this.TabPage2.Location = new System.Drawing.Point(4, 30);
            this.TabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.TabPage2.Size = new System.Drawing.Size(1761, 687);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "LCD L30   ";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // Panel2
            // 
            this.Panel2.AutoScroll = true;
            this.Panel2.Controls.Add(this.STKL21_L30Btn);
            this.Panel2.Controls.Add(this.PictureBox2);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel2.Location = new System.Drawing.Point(4, 4);
            this.Panel2.Margin = new System.Windows.Forms.Padding(4);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(1753, 679);
            this.Panel2.TabIndex = 10;
            // 
            // STKL21_L30Btn
            // 
            this.STKL21_L30Btn.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.STKL21_L30Btn.Location = new System.Drawing.Point(975, 874);
            this.STKL21_L30Btn.Margin = new System.Windows.Forms.Padding(4);
            this.STKL21_L30Btn.Name = "STKL21_L30Btn";
            this.STKL21_L30Btn.Size = new System.Drawing.Size(148, 435);
            this.STKL21_L30Btn.TabIndex = 9;
            this.STKL21_L30Btn.Text = "STKL21";
            this.STKL21_L30Btn.UseVisualStyleBackColor = true;
            // 
            // PictureBox2
            // 
            this.PictureBox2.Image = global::topwell_monitor.Properties.Resources.LCD;
            this.PictureBox2.Location = new System.Drawing.Point(0, 0);
            this.PictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(2185, 1475);
            this.PictureBox2.TabIndex = 10;
            this.PictureBox2.TabStop = false;
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.Panel3);
            this.TabPage3.Location = new System.Drawing.Point(4, 30);
            this.TabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.TabPage3.Size = new System.Drawing.Size(1761, 687);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Text = "LCD L10  ";
            this.TabPage3.UseVisualStyleBackColor = true;
            // 
            // Panel3
            // 
            this.Panel3.AutoScroll = true;
            this.Panel3.Controls.Add(this.STKL25_L10Btn);
            this.Panel3.Controls.Add(this.PictureBox3);
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel3.Location = new System.Drawing.Point(4, 4);
            this.Panel3.Margin = new System.Windows.Forms.Padding(4);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(1753, 679);
            this.Panel3.TabIndex = 9;
            // 
            // STKL25_L10Btn
            // 
            this.STKL25_L10Btn.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.STKL25_L10Btn.Location = new System.Drawing.Point(1620, 941);
            this.STKL25_L10Btn.Margin = new System.Windows.Forms.Padding(4);
            this.STKL25_L10Btn.Name = "STKL25_L10Btn";
            this.STKL25_L10Btn.Size = new System.Drawing.Size(118, 523);
            this.STKL25_L10Btn.TabIndex = 8;
            this.STKL25_L10Btn.Text = "STKL25";
            this.STKL25_L10Btn.UseVisualStyleBackColor = true;
            // 
            // PictureBox3
            // 
            this.PictureBox3.Image = global::topwell_monitor.Properties.Resources.LCD_L10;
            this.PictureBox3.Location = new System.Drawing.Point(0, 0);
            this.PictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox3.Name = "PictureBox3";
            this.PictureBox3.Size = new System.Drawing.Size(3025, 1641);
            this.PictureBox3.TabIndex = 0;
            this.PictureBox3.TabStop = false;
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.Panel4);
            this.TabPage4.Location = new System.Drawing.Point(4, 30);
            this.TabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Padding = new System.Windows.Forms.Padding(4);
            this.TabPage4.Size = new System.Drawing.Size(1761, 687);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Text = "CF";
            this.TabPage4.UseVisualStyleBackColor = true;
            // 
            // Panel4
            // 
            this.Panel4.AutoScroll = true;
            this.Panel4.Controls.Add(this.SORC01Btn);
            this.Panel4.Controls.Add(this.PictureBox4);
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel4.Location = new System.Drawing.Point(4, 4);
            this.Panel4.Margin = new System.Windows.Forms.Padding(4);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1753, 679);
            this.Panel4.TabIndex = 0;
            // 
            // SORC01Btn
            // 
            this.SORC01Btn.Font = new System.Drawing.Font("新細明體", 14.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SORC01Btn.Location = new System.Drawing.Point(45, 1030);
            this.SORC01Btn.Margin = new System.Windows.Forms.Padding(4);
            this.SORC01Btn.Name = "SORC01Btn";
            this.SORC01Btn.Size = new System.Drawing.Size(130, 313);
            this.SORC01Btn.TabIndex = 7;
            this.SORC01Btn.Text = "SORC01";
            this.SORC01Btn.UseVisualStyleBackColor = true;
            // 
            // PictureBox4
            // 
            this.PictureBox4.Image = global::topwell_monitor.Properties.Resources.CF;
            this.PictureBox4.Location = new System.Drawing.Point(0, 0);
            this.PictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox4.Name = "PictureBox4";
            this.PictureBox4.Size = new System.Drawing.Size(2497, 2532);
            this.PictureBox4.TabIndex = 0;
            this.PictureBox4.TabStop = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.InfoDataGrid);
            this.tabPage5.Location = new System.Drawing.Point(4, 30);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1761, 687);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "詳細資訊";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // InfoDataGrid
            // 
            this.InfoDataGrid.AllowUserToAddRows = false;
            this.InfoDataGrid.AllowUserToDeleteRows = false;
            this.InfoDataGrid.AllowUserToResizeColumns = false;
            this.InfoDataGrid.AllowUserToResizeRows = false;
            this.InfoDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InfoDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.InfoDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InfoDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column10,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.InfoDataGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.InfoDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoDataGrid.Location = new System.Drawing.Point(3, 3);
            this.InfoDataGrid.Name = "InfoDataGrid";
            this.InfoDataGrid.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InfoDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.InfoDataGrid.RowHeadersVisible = false;
            this.InfoDataGrid.RowHeadersWidth = 51;
            this.InfoDataGrid.RowTemplate.Height = 27;
            this.InfoDataGrid.Size = new System.Drawing.Size(1755, 681);
            this.InfoDataGrid.TabIndex = 1;
            this.InfoDataGrid.TabStop = false;
            this.InfoDataGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.InfoDataGrid_CellDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "廠區";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 130;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "斷線";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 125;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 40F;
            this.Column2.HeaderText = "運轉";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 60F;
            this.Column3.HeaderText = "停止";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 125;
            // 
            // Column4
            // 
            this.Column4.FillWeight = 60F;
            this.Column4.HeaderText = "警報";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.FillWeight = 60F;
            this.Column5.HeaderText = "By Pass";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 125;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "妥善率";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 125;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "總風量";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 150;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "面積";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 125;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "(總風量/面積)";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 165;
            // 
            // TrackBar1
            // 
            this.TrackBar1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TrackBar1.Location = new System.Drawing.Point(936, 762);
            this.TrackBar1.Margin = new System.Windows.Forms.Padding(4);
            this.TrackBar1.Maximum = 40;
            this.TrackBar1.Minimum = 1;
            this.TrackBar1.Name = "TrackBar1";
            this.TrackBar1.Size = new System.Drawing.Size(333, 56);
            this.TrackBar1.TabIndex = 20;
            this.TrackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.TrackBar1.Value = 20;
            this.TrackBar1.Scroll += new System.EventHandler(this.TrackBar1_Scroll);
            // 
            // LoginBtn
            // 
            this.LoginBtn.FlatAppearance.BorderSize = 0;
            this.LoginBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LoginBtn.Location = new System.Drawing.Point(5, 731);
            this.LoginBtn.Margin = new System.Windows.Forms.Padding(4);
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.Size = new System.Drawing.Size(133, 125);
            this.LoginBtn.TabIndex = 21;
            this.LoginBtn.TabStop = false;
            this.LoginBtn.Text = "登入";
            this.LoginBtn.UseVisualStyleBackColor = true;
            this.LoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // AlarmBtn
            // 
            this.AlarmBtn.FlatAppearance.BorderSize = 0;
            this.AlarmBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AlarmBtn.Location = new System.Drawing.Point(136, 731);
            this.AlarmBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AlarmBtn.Name = "AlarmBtn";
            this.AlarmBtn.Size = new System.Drawing.Size(133, 125);
            this.AlarmBtn.TabIndex = 22;
            this.AlarmBtn.TabStop = false;
            this.AlarmBtn.Text = "即時警報";
            this.AlarmBtn.UseVisualStyleBackColor = true;
            this.AlarmBtn.Click += new System.EventHandler(this.AlarmBtn_Click);
            // 
            // GatewayInfoBtn
            // 
            this.GatewayInfoBtn.FlatAppearance.BorderSize = 0;
            this.GatewayInfoBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.GatewayInfoBtn.Location = new System.Drawing.Point(267, 731);
            this.GatewayInfoBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GatewayInfoBtn.Name = "GatewayInfoBtn";
            this.GatewayInfoBtn.Size = new System.Drawing.Size(133, 125);
            this.GatewayInfoBtn.TabIndex = 23;
            this.GatewayInfoBtn.TabStop = false;
            this.GatewayInfoBtn.Text = "GATEWAY 資訊";
            this.GatewayInfoBtn.UseVisualStyleBackColor = true;
            this.GatewayInfoBtn.Click += new System.EventHandler(this.GatewayInfoBtn_Click);
            // 
            // AlarmCodeInfoBtn
            // 
            this.AlarmCodeInfoBtn.FlatAppearance.BorderSize = 0;
            this.AlarmCodeInfoBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AlarmCodeInfoBtn.Location = new System.Drawing.Point(398, 731);
            this.AlarmCodeInfoBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AlarmCodeInfoBtn.Name = "AlarmCodeInfoBtn";
            this.AlarmCodeInfoBtn.Size = new System.Drawing.Size(133, 125);
            this.AlarmCodeInfoBtn.TabIndex = 24;
            this.AlarmCodeInfoBtn.TabStop = false;
            this.AlarmCodeInfoBtn.Text = "警報代碼資訊";
            this.AlarmCodeInfoBtn.UseVisualStyleBackColor = true;
            this.AlarmCodeInfoBtn.Click += new System.EventHandler(this.AlarmCodeInfoBtn_Click);
            // 
            // LoginDetectTimer
            // 
            this.LoginDetectTimer.Enabled = true;
            this.LoginDetectTimer.Interval = 5000;
            this.LoginDetectTimer.Tick += new System.EventHandler(this.LoginDetectTimer_Tick);
            // 
            // LoginLabel
            // 
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.LoginLabel.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LoginLabel.ForeColor = System.Drawing.Color.Red;
            this.LoginLabel.Location = new System.Drawing.Point(1662, 2);
            this.LoginLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(106, 30);
            this.LoginLabel.TabIndex = 25;
            this.LoginLabel.Text = "未登入";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.MappStateLabel);
            this.GroupBox1.Controls.Add(this.MappIpLabel);
            this.GroupBox1.Location = new System.Drawing.Point(1541, 736);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(225, 108);
            this.GroupBox1.TabIndex = 26;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Visible = false;
            // 
            // MappStateLabel
            // 
            this.MappStateLabel.AutoSize = true;
            this.MappStateLabel.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MappStateLabel.ForeColor = System.Drawing.Color.Red;
            this.MappStateLabel.Location = new System.Drawing.Point(19, 26);
            this.MappStateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MappStateLabel.Name = "MappStateLabel";
            this.MappStateLabel.Size = new System.Drawing.Size(187, 30);
            this.MappStateLabel.TabIndex = 20;
            this.MappStateLabel.Text = "MAPP未連線";
            // 
            // MappIpLabel
            // 
            this.MappIpLabel.AutoSize = true;
            this.MappIpLabel.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MappIpLabel.Location = new System.Drawing.Point(43, 68);
            this.MappIpLabel.Name = "MappIpLabel";
            this.MappIpLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MappIpLabel.Size = new System.Drawing.Size(120, 28);
            this.MappIpLabel.TabIndex = 21;
            this.MappIpLabel.Text = "127.0.0.1";
            this.MappIpLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.DateLabel.Location = new System.Drawing.Point(1354, 777);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(140, 28);
            this.DateLabel.TabIndex = 27;
            this.DateLabel.Text = "2020/01/02";
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TimeLabel.Location = new System.Drawing.Point(1387, 810);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(76, 28);
            this.TimeLabel.TabIndex = 28;
            this.TimeLabel.Text = "14:36";
            // 
            // LogBtn
            // 
            this.LogBtn.FlatAppearance.BorderSize = 0;
            this.LogBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LogBtn.Location = new System.Drawing.Point(529, 731);
            this.LogBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LogBtn.Name = "LogBtn";
            this.LogBtn.Size = new System.Drawing.Size(133, 125);
            this.LogBtn.TabIndex = 29;
            this.LogBtn.TabStop = false;
            this.LogBtn.Text = "歷史紀錄";
            this.LogBtn.UseVisualStyleBackColor = true;
            this.LogBtn.Click += new System.EventHandler(this.LogBtn_Click);
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.Label5);
            this.GroupBox2.Controls.Add(this.DisconnectLabel);
            this.GroupBox2.Controls.Add(this.AlarmLabel);
            this.GroupBox2.Controls.Add(this.StopLabel);
            this.GroupBox2.Controls.Add(this.Label2);
            this.GroupBox2.Controls.Add(this.Label3);
            this.GroupBox2.Controls.Add(this.RunLabel);
            this.GroupBox2.Controls.Add(this.Label4);
            this.GroupBox2.Location = new System.Drawing.Point(697, 729);
            this.GroupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GroupBox2.Size = new System.Drawing.Size(218, 128);
            this.GroupBox2.TabIndex = 30;
            this.GroupBox2.TabStop = false;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(70, 105);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(138, 15);
            this.Label5.TabIndex = 9;
            this.Label5.Text = "GATEWAY無法連線";
            // 
            // DisconnectLabel
            // 
            this.DisconnectLabel.AutoSize = true;
            this.DisconnectLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.DisconnectLabel.Location = new System.Drawing.Point(5, 102);
            this.DisconnectLabel.Name = "DisconnectLabel";
            this.DisconnectLabel.Size = new System.Drawing.Size(59, 15);
            this.DisconnectLabel.TabIndex = 8;
            this.DisconnectLabel.Text = "             ";
            // 
            // AlarmLabel
            // 
            this.AlarmLabel.AutoSize = true;
            this.AlarmLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.AlarmLabel.Location = new System.Drawing.Point(5, 52);
            this.AlarmLabel.Name = "AlarmLabel";
            this.AlarmLabel.Size = new System.Drawing.Size(59, 15);
            this.AlarmLabel.TabIndex = 6;
            this.AlarmLabel.Text = "             ";
            // 
            // StopLabel
            // 
            this.StopLabel.AutoSize = true;
            this.StopLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.StopLabel.Location = new System.Drawing.Point(5, 78);
            this.StopLabel.Name = "StopLabel";
            this.StopLabel.Size = new System.Drawing.Size(59, 15);
            this.StopLabel.TabIndex = 7;
            this.StopLabel.Text = "             ";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(72, 25);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(37, 15);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "正常";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(72, 52);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(82, 15);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "任一台警報";
            // 
            // RunLabel
            // 
            this.RunLabel.AutoSize = true;
            this.RunLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RunLabel.Location = new System.Drawing.Point(5, 25);
            this.RunLabel.Name = "RunLabel";
            this.RunLabel.Size = new System.Drawing.Size(59, 15);
            this.RunLabel.TabIndex = 5;
            this.RunLabel.Text = "             ";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(72, 78);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(108, 15);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "任一台FFU停止";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(1368, 744);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 24);
            this.label6.TabIndex = 32;
            this.label6.Text = "更新時間:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1604, 944);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.LogBtn);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.LoginLabel);
            this.Controls.Add(this.GatewayInfoBtn);
            this.Controls.Add(this.AlarmCodeInfoBtn);
            this.Controls.Add(this.AlarmBtn);
            this.Controls.Add(this.LoginBtn);
            this.Controls.Add(this.TrackBar1);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "奇立監控系統";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maskPic01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.TabPage2.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            this.TabPage3.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).EndInit();
            this.TabPage4.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InfoDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.PictureBox maskPic01;
        internal System.Windows.Forms.Button OHGT03Btn;
        internal System.Windows.Forms.Button OHGT01Btn;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Button STKL21_L30Btn;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Button STKL25_L10Btn;
        internal System.Windows.Forms.PictureBox PictureBox3;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.PictureBox PictureBox4;
        internal System.Windows.Forms.TrackBar TrackBar1;
        internal System.Windows.Forms.Button AlarmBtn;
        internal System.Windows.Forms.Button GatewayInfoBtn;
        internal System.Windows.Forms.Button AlarmCodeInfoBtn;
        private System.Windows.Forms.TabPage tabPage5;
        internal System.Windows.Forms.DataGridView InfoDataGrid;
        private System.Windows.Forms.Timer LoginDetectTimer;
        internal System.Windows.Forms.Label LoginLabel;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label MappStateLabel;
        internal System.Windows.Forms.Label MappIpLabel;
        public System.Windows.Forms.Label DateLabel;
        public System.Windows.Forms.Label TimeLabel;
        internal System.Windows.Forms.Button LogBtn;
        internal System.Windows.Forms.Button LoginBtn;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label DisconnectLabel;
        internal System.Windows.Forms.Label AlarmLabel;
        internal System.Windows.Forms.Label StopLabel;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label RunLabel;
        internal System.Windows.Forms.Label Label4;
        public System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        internal System.Windows.Forms.Button STKT10Btn;
        internal System.Windows.Forms.Button SORC01Btn;
    }
}

