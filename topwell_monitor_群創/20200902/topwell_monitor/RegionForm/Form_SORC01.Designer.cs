﻿namespace topwell_monitor
{
    partial class Form_SORC01
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_SORC01));
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Btn_1_36 = new System.Windows.Forms.Button();
            this.Btn_1_35 = new System.Windows.Forms.Button();
            this.Btn_1_34 = new System.Windows.Forms.Button();
            this.Btn_1_33 = new System.Windows.Forms.Button();
            this.Btn_1_32 = new System.Windows.Forms.Button();
            this.Btn_1_31 = new System.Windows.Forms.Button();
            this.Btn_1_30 = new System.Windows.Forms.Button();
            this.Btn_1_29 = new System.Windows.Forms.Button();
            this.Btn_1_28 = new System.Windows.Forms.Button();
            this.Btn_1_27 = new System.Windows.Forms.Button();
            this.Btn_1_26 = new System.Windows.Forms.Button();
            this.Btn_1_25 = new System.Windows.Forms.Button();
            this.Btn_1_24 = new System.Windows.Forms.Button();
            this.Btn_1_23 = new System.Windows.Forms.Button();
            this.Btn_1_22 = new System.Windows.Forms.Button();
            this.Btn_1_21 = new System.Windows.Forms.Button();
            this.Btn_1_20 = new System.Windows.Forms.Button();
            this.Btn_1_19 = new System.Windows.Forms.Button();
            this.Btn_1_18 = new System.Windows.Forms.Button();
            this.Btn_1_17 = new System.Windows.Forms.Button();
            this.Btn_1_16 = new System.Windows.Forms.Button();
            this.Btn_1_15 = new System.Windows.Forms.Button();
            this.Btn_1_14 = new System.Windows.Forms.Button();
            this.Btn_1_13 = new System.Windows.Forms.Button();
            this.Btn_1_12 = new System.Windows.Forms.Button();
            this.Btn_1_11 = new System.Windows.Forms.Button();
            this.Btn_1_10 = new System.Windows.Forms.Button();
            this.Btn_1_9 = new System.Windows.Forms.Button();
            this.Btn_1_2 = new System.Windows.Forms.Button();
            this.Btn_1_1 = new System.Windows.Forms.Button();
            this.Btn_1_8 = new System.Windows.Forms.Button();
            this.Btn_1_3 = new System.Windows.Forms.Button();
            this.Btn_1_7 = new System.Windows.Forms.Button();
            this.Btn_1_4 = new System.Windows.Forms.Button();
            this.Btn_1_5 = new System.Windows.Forms.Button();
            this.Btn_1_6 = new System.Windows.Forms.Button();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TrackBar1 = new System.Windows.Forms.TrackBar();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.AutoScroll = true;
            this.Panel1.Controls.Add(this.Btn_1_36);
            this.Panel1.Controls.Add(this.Btn_1_35);
            this.Panel1.Controls.Add(this.Btn_1_34);
            this.Panel1.Controls.Add(this.Btn_1_33);
            this.Panel1.Controls.Add(this.Btn_1_32);
            this.Panel1.Controls.Add(this.Btn_1_31);
            this.Panel1.Controls.Add(this.Btn_1_30);
            this.Panel1.Controls.Add(this.Btn_1_29);
            this.Panel1.Controls.Add(this.Btn_1_28);
            this.Panel1.Controls.Add(this.Btn_1_27);
            this.Panel1.Controls.Add(this.Btn_1_26);
            this.Panel1.Controls.Add(this.Btn_1_25);
            this.Panel1.Controls.Add(this.Btn_1_24);
            this.Panel1.Controls.Add(this.Btn_1_23);
            this.Panel1.Controls.Add(this.Btn_1_22);
            this.Panel1.Controls.Add(this.Btn_1_21);
            this.Panel1.Controls.Add(this.Btn_1_20);
            this.Panel1.Controls.Add(this.Btn_1_19);
            this.Panel1.Controls.Add(this.Btn_1_18);
            this.Panel1.Controls.Add(this.Btn_1_17);
            this.Panel1.Controls.Add(this.Btn_1_16);
            this.Panel1.Controls.Add(this.Btn_1_15);
            this.Panel1.Controls.Add(this.Btn_1_14);
            this.Panel1.Controls.Add(this.Btn_1_13);
            this.Panel1.Controls.Add(this.Btn_1_12);
            this.Panel1.Controls.Add(this.Btn_1_11);
            this.Panel1.Controls.Add(this.Btn_1_10);
            this.Panel1.Controls.Add(this.Btn_1_9);
            this.Panel1.Controls.Add(this.Btn_1_2);
            this.Panel1.Controls.Add(this.Btn_1_1);
            this.Panel1.Controls.Add(this.Btn_1_8);
            this.Panel1.Controls.Add(this.Btn_1_3);
            this.Panel1.Controls.Add(this.Btn_1_7);
            this.Panel1.Controls.Add(this.Btn_1_4);
            this.Panel1.Controls.Add(this.Btn_1_5);
            this.Panel1.Controls.Add(this.Btn_1_6);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Location = new System.Drawing.Point(13, 13);
            this.Panel1.Margin = new System.Windows.Forms.Padding(4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1590, 580);
            this.Panel1.TabIndex = 2;
            // 
            // Btn_1_36
            // 
            this.Btn_1_36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_36.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_36.Location = new System.Drawing.Point(19, 285);
            this.Btn_1_36.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_36.Name = "Btn_1_36";
            this.Btn_1_36.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_36.TabIndex = 36;
            this.Btn_1_36.Text = "36";
            this.Btn_1_36.UseVisualStyleBackColor = true;
            // 
            // Btn_1_35
            // 
            this.Btn_1_35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_35.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_35.Location = new System.Drawing.Point(65, 285);
            this.Btn_1_35.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_35.Name = "Btn_1_35";
            this.Btn_1_35.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_35.TabIndex = 35;
            this.Btn_1_35.Text = "35";
            this.Btn_1_35.UseVisualStyleBackColor = true;
            // 
            // Btn_1_34
            // 
            this.Btn_1_34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_34.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_34.Location = new System.Drawing.Point(110, 285);
            this.Btn_1_34.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_34.Name = "Btn_1_34";
            this.Btn_1_34.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_34.TabIndex = 34;
            this.Btn_1_34.Text = "34";
            this.Btn_1_34.UseVisualStyleBackColor = true;
            // 
            // Btn_1_33
            // 
            this.Btn_1_33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_33.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_33.Location = new System.Drawing.Point(156, 285);
            this.Btn_1_33.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_33.Name = "Btn_1_33";
            this.Btn_1_33.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_33.TabIndex = 33;
            this.Btn_1_33.Text = "33";
            this.Btn_1_33.UseVisualStyleBackColor = true;
            // 
            // Btn_1_32
            // 
            this.Btn_1_32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_32.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_32.Location = new System.Drawing.Point(202, 285);
            this.Btn_1_32.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_32.Name = "Btn_1_32";
            this.Btn_1_32.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_32.TabIndex = 32;
            this.Btn_1_32.Text = "32";
            this.Btn_1_32.UseVisualStyleBackColor = true;
            // 
            // Btn_1_31
            // 
            this.Btn_1_31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_31.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_31.Location = new System.Drawing.Point(247, 285);
            this.Btn_1_31.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_31.Name = "Btn_1_31";
            this.Btn_1_31.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_31.TabIndex = 31;
            this.Btn_1_31.Text = "31";
            this.Btn_1_31.UseVisualStyleBackColor = true;
            // 
            // Btn_1_30
            // 
            this.Btn_1_30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_30.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_30.Location = new System.Drawing.Point(293, 285);
            this.Btn_1_30.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_30.Name = "Btn_1_30";
            this.Btn_1_30.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_30.TabIndex = 30;
            this.Btn_1_30.Text = "30";
            this.Btn_1_30.UseVisualStyleBackColor = true;
            // 
            // Btn_1_29
            // 
            this.Btn_1_29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_29.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_29.Location = new System.Drawing.Point(339, 285);
            this.Btn_1_29.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_29.Name = "Btn_1_29";
            this.Btn_1_29.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_29.TabIndex = 29;
            this.Btn_1_29.Text = "29";
            this.Btn_1_29.UseVisualStyleBackColor = true;
            // 
            // Btn_1_28
            // 
            this.Btn_1_28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_28.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_28.Location = new System.Drawing.Point(384, 285);
            this.Btn_1_28.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_28.Name = "Btn_1_28";
            this.Btn_1_28.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_28.TabIndex = 28;
            this.Btn_1_28.Text = "28";
            this.Btn_1_28.UseVisualStyleBackColor = true;
            // 
            // Btn_1_27
            // 
            this.Btn_1_27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_27.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_27.Location = new System.Drawing.Point(429, 285);
            this.Btn_1_27.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_27.Name = "Btn_1_27";
            this.Btn_1_27.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_27.TabIndex = 27;
            this.Btn_1_27.Text = "27";
            this.Btn_1_27.UseVisualStyleBackColor = true;
            // 
            // Btn_1_26
            // 
            this.Btn_1_26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_26.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_26.Location = new System.Drawing.Point(474, 285);
            this.Btn_1_26.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_26.Name = "Btn_1_26";
            this.Btn_1_26.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_26.TabIndex = 26;
            this.Btn_1_26.Text = "26";
            this.Btn_1_26.UseVisualStyleBackColor = true;
            // 
            // Btn_1_25
            // 
            this.Btn_1_25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_25.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_25.Location = new System.Drawing.Point(519, 285);
            this.Btn_1_25.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_25.Name = "Btn_1_25";
            this.Btn_1_25.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_25.TabIndex = 25;
            this.Btn_1_25.Text = "25";
            this.Btn_1_25.UseVisualStyleBackColor = true;
            // 
            // Btn_1_24
            // 
            this.Btn_1_24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_24.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_24.Location = new System.Drawing.Point(563, 285);
            this.Btn_1_24.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_24.Name = "Btn_1_24";
            this.Btn_1_24.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_24.TabIndex = 24;
            this.Btn_1_24.Text = "24";
            this.Btn_1_24.UseVisualStyleBackColor = true;
            // 
            // Btn_1_23
            // 
            this.Btn_1_23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_23.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_23.Location = new System.Drawing.Point(609, 285);
            this.Btn_1_23.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_23.Name = "Btn_1_23";
            this.Btn_1_23.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_23.TabIndex = 23;
            this.Btn_1_23.Text = "23";
            this.Btn_1_23.UseVisualStyleBackColor = true;
            // 
            // Btn_1_22
            // 
            this.Btn_1_22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_22.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_22.Location = new System.Drawing.Point(655, 285);
            this.Btn_1_22.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_22.Name = "Btn_1_22";
            this.Btn_1_22.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_22.TabIndex = 22;
            this.Btn_1_22.Text = "22";
            this.Btn_1_22.UseVisualStyleBackColor = true;
            // 
            // Btn_1_21
            // 
            this.Btn_1_21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_21.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_21.Location = new System.Drawing.Point(701, 285);
            this.Btn_1_21.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_21.Name = "Btn_1_21";
            this.Btn_1_21.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_21.TabIndex = 21;
            this.Btn_1_21.Text = "21";
            this.Btn_1_21.UseVisualStyleBackColor = true;
            // 
            // Btn_1_20
            // 
            this.Btn_1_20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_20.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_20.Location = new System.Drawing.Point(746, 285);
            this.Btn_1_20.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_20.Name = "Btn_1_20";
            this.Btn_1_20.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_20.TabIndex = 20;
            this.Btn_1_20.Text = "20";
            this.Btn_1_20.UseVisualStyleBackColor = true;
            // 
            // Btn_1_19
            // 
            this.Btn_1_19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_19.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_19.Location = new System.Drawing.Point(791, 285);
            this.Btn_1_19.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_19.Name = "Btn_1_19";
            this.Btn_1_19.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_19.TabIndex = 19;
            this.Btn_1_19.Text = "19";
            this.Btn_1_19.UseVisualStyleBackColor = true;
            // 
            // Btn_1_18
            // 
            this.Btn_1_18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_18.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_18.Location = new System.Drawing.Point(836, 285);
            this.Btn_1_18.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_18.Name = "Btn_1_18";
            this.Btn_1_18.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_18.TabIndex = 18;
            this.Btn_1_18.Text = "18";
            this.Btn_1_18.UseVisualStyleBackColor = true;
            // 
            // Btn_1_17
            // 
            this.Btn_1_17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_17.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_17.Location = new System.Drawing.Point(880, 285);
            this.Btn_1_17.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_17.Name = "Btn_1_17";
            this.Btn_1_17.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_17.TabIndex = 17;
            this.Btn_1_17.Text = "17";
            this.Btn_1_17.UseVisualStyleBackColor = true;
            // 
            // Btn_1_16
            // 
            this.Btn_1_16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_16.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_16.Location = new System.Drawing.Point(926, 285);
            this.Btn_1_16.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_16.Name = "Btn_1_16";
            this.Btn_1_16.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_16.TabIndex = 16;
            this.Btn_1_16.Text = "16";
            this.Btn_1_16.UseVisualStyleBackColor = true;
            // 
            // Btn_1_15
            // 
            this.Btn_1_15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_15.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_15.Location = new System.Drawing.Point(972, 285);
            this.Btn_1_15.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_15.Name = "Btn_1_15";
            this.Btn_1_15.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_15.TabIndex = 15;
            this.Btn_1_15.Text = "15";
            this.Btn_1_15.UseVisualStyleBackColor = true;
            // 
            // Btn_1_14
            // 
            this.Btn_1_14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_14.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_14.Location = new System.Drawing.Point(1021, 285);
            this.Btn_1_14.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_14.Name = "Btn_1_14";
            this.Btn_1_14.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_14.TabIndex = 14;
            this.Btn_1_14.Text = "14";
            this.Btn_1_14.UseVisualStyleBackColor = true;
            // 
            // Btn_1_13
            // 
            this.Btn_1_13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_13.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_13.Location = new System.Drawing.Point(1065, 285);
            this.Btn_1_13.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_13.Name = "Btn_1_13";
            this.Btn_1_13.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_13.TabIndex = 13;
            this.Btn_1_13.Text = "13";
            this.Btn_1_13.UseVisualStyleBackColor = true;
            // 
            // Btn_1_12
            // 
            this.Btn_1_12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_12.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_12.Location = new System.Drawing.Point(1110, 285);
            this.Btn_1_12.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_12.Name = "Btn_1_12";
            this.Btn_1_12.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_12.TabIndex = 12;
            this.Btn_1_12.Text = "12";
            this.Btn_1_12.UseVisualStyleBackColor = true;
            // 
            // Btn_1_11
            // 
            this.Btn_1_11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_11.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_11.Location = new System.Drawing.Point(1155, 285);
            this.Btn_1_11.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_11.Name = "Btn_1_11";
            this.Btn_1_11.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_11.TabIndex = 11;
            this.Btn_1_11.Text = "11";
            this.Btn_1_11.UseVisualStyleBackColor = true;
            // 
            // Btn_1_10
            // 
            this.Btn_1_10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_10.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_10.Location = new System.Drawing.Point(1200, 285);
            this.Btn_1_10.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_10.Name = "Btn_1_10";
            this.Btn_1_10.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_10.TabIndex = 10;
            this.Btn_1_10.Text = "10";
            this.Btn_1_10.UseVisualStyleBackColor = true;
            // 
            // Btn_1_9
            // 
            this.Btn_1_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_9.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_9.Location = new System.Drawing.Point(1243, 285);
            this.Btn_1_9.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_9.Name = "Btn_1_9";
            this.Btn_1_9.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_9.TabIndex = 9;
            this.Btn_1_9.Text = "09";
            this.Btn_1_9.UseVisualStyleBackColor = true;
            // 
            // Btn_1_2
            // 
            this.Btn_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_2.Location = new System.Drawing.Point(1560, 285);
            this.Btn_1_2.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_2.Name = "Btn_1_2";
            this.Btn_1_2.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_2.TabIndex = 2;
            this.Btn_1_2.Text = "02";
            this.Btn_1_2.UseVisualStyleBackColor = true;
            // 
            // Btn_1_1
            // 
            this.Btn_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_1.Location = new System.Drawing.Point(1604, 285);
            this.Btn_1_1.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_1.Name = "Btn_1_1";
            this.Btn_1_1.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_1.TabIndex = 1;
            this.Btn_1_1.Text = "01";
            this.Btn_1_1.UseVisualStyleBackColor = true;
            // 
            // Btn_1_8
            // 
            this.Btn_1_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_8.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_8.Location = new System.Drawing.Point(1287, 285);
            this.Btn_1_8.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_8.Name = "Btn_1_8";
            this.Btn_1_8.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_8.TabIndex = 8;
            this.Btn_1_8.Text = "08";
            this.Btn_1_8.UseVisualStyleBackColor = true;
            // 
            // Btn_1_3
            // 
            this.Btn_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_3.Location = new System.Drawing.Point(1513, 285);
            this.Btn_1_3.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_3.Name = "Btn_1_3";
            this.Btn_1_3.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_3.TabIndex = 3;
            this.Btn_1_3.Text = "03";
            this.Btn_1_3.UseVisualStyleBackColor = true;
            // 
            // Btn_1_7
            // 
            this.Btn_1_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_7.Location = new System.Drawing.Point(1330, 285);
            this.Btn_1_7.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_7.Name = "Btn_1_7";
            this.Btn_1_7.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_7.TabIndex = 7;
            this.Btn_1_7.Text = "07";
            this.Btn_1_7.UseVisualStyleBackColor = true;
            // 
            // Btn_1_4
            // 
            this.Btn_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_4.Location = new System.Drawing.Point(1466, 285);
            this.Btn_1_4.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_4.Name = "Btn_1_4";
            this.Btn_1_4.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_4.TabIndex = 4;
            this.Btn_1_4.Text = "04";
            this.Btn_1_4.UseVisualStyleBackColor = true;
            // 
            // Btn_1_5
            // 
            this.Btn_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_5.Location = new System.Drawing.Point(1420, 285);
            this.Btn_1_5.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_5.Name = "Btn_1_5";
            this.Btn_1_5.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_5.TabIndex = 5;
            this.Btn_1_5.Text = "05";
            this.Btn_1_5.UseVisualStyleBackColor = true;
            // 
            // Btn_1_6
            // 
            this.Btn_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_1_6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_1_6.Location = new System.Drawing.Point(1374, 285);
            this.Btn_1_6.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_1_6.Name = "Btn_1_6";
            this.Btn_1_6.Size = new System.Drawing.Size(42, 100);
            this.Btn_1_6.TabIndex = 6;
            this.Btn_1_6.Text = "06";
            this.Btn_1_6.UseVisualStyleBackColor = true;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::topwell_monitor.Properties.Resources.SORC01;
            this.PictureBox1.Location = new System.Drawing.Point(0, 128);
            this.PictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(1679, 333);
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            // 
            // TrackBar1
            // 
            this.TrackBar1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TrackBar1.Location = new System.Drawing.Point(30, 600);
            this.TrackBar1.Margin = new System.Windows.Forms.Padding(4);
            this.TrackBar1.Maximum = 40;
            this.TrackBar1.Minimum = 1;
            this.TrackBar1.Name = "TrackBar1";
            this.TrackBar1.Size = new System.Drawing.Size(333, 56);
            this.TrackBar1.TabIndex = 21;
            this.TrackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.TrackBar1.Value = 30;
            // 
            // Form_SORC01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1604, 670);
            this.Controls.Add(this.TrackBar1);
            this.Controls.Add(this.Panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_SORC01";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SORC01";
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button Btn_1_2;
        internal System.Windows.Forms.Button Btn_1_1;
        internal System.Windows.Forms.Button Btn_1_8;
        internal System.Windows.Forms.Button Btn_1_3;
        internal System.Windows.Forms.Button Btn_1_7;
        internal System.Windows.Forms.Button Btn_1_4;
        internal System.Windows.Forms.Button Btn_1_5;
        internal System.Windows.Forms.Button Btn_1_6;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TrackBar TrackBar1;
        internal System.Windows.Forms.Button Btn_1_36;
        internal System.Windows.Forms.Button Btn_1_35;
        internal System.Windows.Forms.Button Btn_1_34;
        internal System.Windows.Forms.Button Btn_1_33;
        internal System.Windows.Forms.Button Btn_1_32;
        internal System.Windows.Forms.Button Btn_1_31;
        internal System.Windows.Forms.Button Btn_1_30;
        internal System.Windows.Forms.Button Btn_1_29;
        internal System.Windows.Forms.Button Btn_1_28;
        internal System.Windows.Forms.Button Btn_1_27;
        internal System.Windows.Forms.Button Btn_1_26;
        internal System.Windows.Forms.Button Btn_1_25;
        internal System.Windows.Forms.Button Btn_1_24;
        internal System.Windows.Forms.Button Btn_1_23;
        internal System.Windows.Forms.Button Btn_1_22;
        internal System.Windows.Forms.Button Btn_1_21;
        internal System.Windows.Forms.Button Btn_1_20;
        internal System.Windows.Forms.Button Btn_1_19;
        internal System.Windows.Forms.Button Btn_1_18;
        internal System.Windows.Forms.Button Btn_1_17;
        internal System.Windows.Forms.Button Btn_1_16;
        internal System.Windows.Forms.Button Btn_1_15;
        internal System.Windows.Forms.Button Btn_1_14;
        internal System.Windows.Forms.Button Btn_1_13;
        internal System.Windows.Forms.Button Btn_1_12;
        internal System.Windows.Forms.Button Btn_1_11;
        internal System.Windows.Forms.Button Btn_1_10;
        internal System.Windows.Forms.Button Btn_1_9;
    }
}