﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;

namespace topwell_monitor
{
    class RegionView
    {
        private Monitor m_monitor;
        private Button m_mainBtn;
        private Form m_form;
        private RegionModel m_regionModel;

        private Form_DeviceDetail m_form_deviceDetail = new Form_DeviceDetail();
        private Form_DeviceDetailAC m_form_deviceDetailAC = new Form_DeviceDetailAC();
        private Form_SettingSpeed m_form_SettingSpeed = new Form_SettingSpeed();
        private Form_SettingSpeedAC m_form_SettingSpeedAC = new Form_SettingSpeedAC();

        private LoginHandler m_loginHandler;

        private Device m_nowDevice;
        private delegate void UiUpdatedelegate(Control c, string s);
        private delegate void UiVisableUpdatedelegate(Control c, bool b);
        private delegate void UiColorUpdatedelegate(Control c, Color color);

        private SizeHandler m_sizeHandler;
        private TrackBar m_trackBar;

        private List<Panel> panels = new List<Panel>();
        private List<PictureBox> pictureBoxs = new List<PictureBox>();
        private List<Button> buttons = new List<Button>();

        private Dictionary<int, Dictionary<int, Button> > m_deviceBtnMap = new Dictionary<int, Dictionary<int, Button>>();

        public RegionView(Monitor monitor, Button mainBtn, Form form, RegionModel regionModel, LoginHandler loginHandler)
        {
            m_monitor = monitor;
            m_mainBtn = mainBtn;
            initButtonDisplay(m_mainBtn);
            m_form = form;
            m_loginHandler = loginHandler;
            m_regionModel = regionModel;

            findControl(form);

            m_trackBar.Scroll += new EventHandler(this.TrackBar1_Scroll);

            m_sizeHandler = new SizeHandler(panels.ToArray(), pictureBoxs.ToArray(), buttons.ToArray(), new PictureBox[] { }, 40);
            m_sizeHandler.changeSize(30);

            m_form.FormClosing += new FormClosingEventHandler(this.Form_Closing);
            m_form_deviceDetail.FormClosing += new FormClosingEventHandler(this.Form_Closing);
            m_form_deviceDetailAC.FormClosing += new FormClosingEventHandler(this.Form_Closing);
            m_form_SettingSpeed.FormClosing += new FormClosingEventHandler(this.Form_Closing);
            m_form_SettingSpeedAC.FormClosing += new FormClosingEventHandler(this.Form_Closing);

            m_mainBtn.Click += new EventHandler(this.mainBtn_click);
            m_form_deviceDetail.settingSpeedBtn.Click += new EventHandler(this.settingSpeedBtn_click);
            m_form_deviceDetailAC.settingSpeedBtn.Click += new EventHandler(this.settingSpeedACBtn_click);
            m_form_SettingSpeed.EnterBtn.Click += new EventHandler(this.settingSpeedEnterBtn_click);
            m_form_SettingSpeedAC.EnterBtn.Click += new EventHandler(this.settingSpeedACEnterBtn_click);

            m_form_deviceDetail.OffBtn.Click += new EventHandler(this.offBtn_click);
            m_form_deviceDetail.OnBtn.Click += new EventHandler(this.onBtn_click);
            m_form_deviceDetailAC.OffBtn.Click += new EventHandler(this.offBtn_click);
            m_form_deviceDetailAC.OnBtn.Click += new EventHandler(this.onBtn_click);

            m_form_deviceDetail.BypassOffBtn.Click += new EventHandler(this.BypassOffBtn_click);
            m_form_deviceDetail.BypassOnBtn.Click += new EventHandler(this.BypassOnBtn_click);
            m_form_deviceDetailAC.BypassOffBtn.Click += new EventHandler(this.BypassOffBtn_click);
            m_form_deviceDetailAC.BypassOnBtn.Click += new EventHandler(this.BypassOnBtn_click);

            m_form_deviceDetail.PressureLow.Click += new EventHandler(this.Pressure_ValueChanged);
            m_form_deviceDetail.PressureHight.Click += new EventHandler(this.Pressure_ValueChanged);

            m_form_deviceDetail.TemperatureLow.Click += new EventHandler(this.Temperature_ValueChanged);
            m_form_deviceDetail.TemperatureHight.Click += new EventHandler(this.Temperature_ValueChanged);

            m_form_deviceDetail.HumidityLow.Click += new EventHandler(this.Humidity_ValueChanged);
            m_form_deviceDetail.HumidityHight.Click += new EventHandler(this.Humidity_ValueChanged);

            foreach (Device device in m_regionModel.getDeviceList()) {
                int register = device.Register;
                int id = device.Id;
                
                if (form.Controls.Find("Btn_" + register + "_" + id, true).Count() > 0)
                {
                    if (m_deviceBtnMap.ContainsKey(register) == false)
                    {
                        m_deviceBtnMap.Add(register, new Dictionary<int, Button>());
                    }
                    if (m_deviceBtnMap[register].ContainsKey(id)){
                        MessageBox.Show("Button" + register + id + "重複");
                    }
                    else 
                    {
                        Button _btn = (Button)form.Controls.Find("btn_" + device.Register + "_" + device.Id, true)[0];
                        if (device.DeviceType == DeviceTypeEnum.Double)
                            _btn.Click += new EventHandler(this.btnDevice_click);
                        else if (device.DeviceType == DeviceTypeEnum.ACSingle)
                            _btn.Click += new EventHandler(this.btnDeviceAC_click);
                        initButtonDisplay(_btn);
                        m_deviceBtnMap[register].Add(id, _btn);
                    }
                    
                }
                else 
                {
                    MessageBox.Show(form.Text + "Button 缺少 btn_" + register + "_" + id);
                } 
            }
        }

        private void findControl(Control root)
        {
            foreach (Control control in root.Controls)
            {           
                if (control is Panel)
                    panels.Add((Panel)control);
                else if (control is PictureBox)
                    pictureBoxs.Add((PictureBox)control);
                else if (control is Button)
                    buttons.Add((Button)control);
                else if (control is TrackBar)
                    m_trackBar = (TrackBar)control;

                if (control.Controls.Count != 0)
                    findControl(control);
            }
        }

        private void TrackBar1_Scroll(object sender, EventArgs e)
        {
            m_sizeHandler.changeSize(((TrackBar)sender).Value);
        }

        private void Pressure_ValueChanged(object sender, EventArgs e)
        {
            if (m_form_deviceDetail.PressureLow.Value <= m_form_deviceDetail.PressureHight.Value)
                m_nowDevice.getSMFModel().changeAlarmSetting(getSMFAlarmSetting());
            else {
                m_form_deviceDetail.PressureLow.Value = m_nowDevice.getSMFModel().PressureAlarmSettingLow;
                m_form_deviceDetail.PressureHight.Value = m_nowDevice.getSMFModel().PressureAlarmSettingHight;
            }
        }

        private void Temperature_ValueChanged(object sender, EventArgs e)
        {
            if (m_form_deviceDetail.TemperatureLow.Value <= m_form_deviceDetail.TemperatureHight.Value)
                m_nowDevice.getSMFModel().changeAlarmSetting(getSMFAlarmSetting());
            else
            {
                m_form_deviceDetail.TemperatureLow.Value = m_nowDevice.getSMFModel().TemperatureAlarmSettingLow;
                m_form_deviceDetail.TemperatureHight.Value = m_nowDevice.getSMFModel().TemperatureAlarmSettingHight;
            }
        }

        private void Humidity_ValueChanged(object sender, EventArgs e)
        {
            if (m_form_deviceDetail.HumidityLow.Value <= m_form_deviceDetail.HumidityHight.Value)
                m_nowDevice.getSMFModel().changeAlarmSetting(getSMFAlarmSetting());
            else
            {
                m_form_deviceDetail.HumidityLow.Value = m_nowDevice.getSMFModel().HumidityAlarmSettingLow;
                m_form_deviceDetail.HumidityHight.Value = m_nowDevice.getSMFModel().HumidityAlarmSettingHight;
            }
        }

        private string getSMFAlarmSetting()
        {
            string ret = m_form_deviceDetail.PressureLow.Value.ToString() + "," + m_form_deviceDetail.PressureHight.Value.ToString() + "|" +
                    m_form_deviceDetail.TemperatureLow.Value.ToString() + "," + m_form_deviceDetail.TemperatureHight.Value.ToString() + "|" +
                    m_form_deviceDetail.HumidityLow.Value.ToString() + "," + m_form_deviceDetail.HumidityHight.Value.ToString();
            return ret;
        }

        private void mainBtn_click(object sender, EventArgs e)
        {
            //先update UI 避免按下去沒有資料
            formUpdate();
            m_form.ShowDialog();
        }

        private void settingSpeedBtn_click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin) 
                m_form_SettingSpeed.ShowDialog();
            else 
                MessageBox.Show("沒有權限,請登入");
        }

        private void settingSpeedACBtn_click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin)
                m_form_SettingSpeedAC.ShowDialog();
            else
                MessageBox.Show("沒有權限,請登入");
        }

        private void settingSpeedEnterBtn_click(object sender, EventArgs e)
        {
            if (m_form_deviceDetail.Visible) { 
                int val = 0;
                bool result = int.TryParse(m_form_SettingSpeed.TextBox1.Text, out val);
                if (result) {
                    if (val < 500 || val > 1600)
                        MessageBox.Show("不能小於500 或 不能大於1600");
                    else {
                        if (m_nowDevice != null) {
                            if (m_monitor.getGatewayMap()[m_nowDevice.Ip].writeSettingSpeed(m_nowDevice.Register, m_nowDevice.Id, val))
                                MessageBox.Show("設定成功");
                            else
                                MessageBox.Show("設定失敗");
                        }
                        else {
                            MessageBox.Show("設定失敗 nowDevice = null");
                        }
                        m_form_SettingSpeed.Close();
                    }
                }
                else
                    MessageBox.Show("非數字");
            } 
        }

        private void settingSpeedACEnterBtn_click(object sender, EventArgs e)
        {
            if (m_form_deviceDetailAC.Visible) {
                int val = 0;
                bool result = int.TryParse(m_form_SettingSpeedAC.TextBox1.Text, out val);
                if (result) {
                    if (val < 0 || val > 4)
                        MessageBox.Show("不能小於0 或 不能大於4");
                    else {
                        if (m_nowDevice != null) {
                            if (m_monitor.getGatewayMap()[m_nowDevice.Ip].writeSettingSpeed(m_nowDevice.Register, m_nowDevice.Id, val))
                                MessageBox.Show("設定成功");
                            else
                                MessageBox.Show("設定失敗");
                        }
                        else {
                            MessageBox.Show("設定失敗 nowDevice = null");
                        }
                        m_form_SettingSpeedAC.Close();
                    }
                }
                else
                    MessageBox.Show("非數字");
            }
        }

        private void offBtn_click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin == false)
                MessageBox.Show("沒有權限,請登入");
            else {
                if (m_nowDevice != null) {
                    if (m_nowDevice.Operation == true) {
                        if (MessageBox.Show("確定要關閉FFU?", "注意", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                            if (m_monitor.getGatewayMap()[m_nowDevice.Ip].writeOperation(m_nowDevice.Register, m_nowDevice.Id, false))
                                MessageBox.Show("設定成功");
                            else
                                MessageBox.Show("設定失敗");
                        }
                    }
                }
                else
                    MessageBox.Show("OffBtn error");
            }
        }

        private void onBtn_click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin == false)
                MessageBox.Show("沒有權限,請登入");
            else {
                if (m_nowDevice != null) {
                    if (m_nowDevice.Operation == false) {
                        if (MessageBox.Show("確定要開啟FFU?", "注意", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                             if (m_monitor.getGatewayMap()[m_nowDevice.Ip].writeOperation(m_nowDevice.Register, m_nowDevice.Id, true))
                                 MessageBox.Show("設定成功");
                             else
                                 MessageBox.Show("設定失敗");
                         }
                    }
                }
                else
                    MessageBox.Show("OnBtn error");
            }
        }

        private void BypassOnBtn_click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin == false)
                MessageBox.Show("沒有權限,請登入");
            else {
                if (m_nowDevice != null) {
                    if (m_nowDevice.IsBypass == false) {
                        if (MessageBox.Show("確定要開啟Bypass功能?", "注意", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            m_nowDevice.IsBypass = true;
                    }
                    
                }
                else
                    MessageBox.Show("BypassOnBtn error");
            }
        }

        private void BypassOffBtn_click(object sender, EventArgs e)
        {
            if (m_loginHandler.isLogin == false)
                MessageBox.Show("沒有權限,請登入");
            else {
                if (m_nowDevice != null) {
                    if (m_nowDevice.IsBypass == true) {
                        if (MessageBox.Show("確定要關閉Bypass功能?", "注意", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            m_nowDevice.IsBypass = false; 
                    }
                }
                else
                    MessageBox.Show("BypassOffBtn error");
            }
        }
       
        private void btnDevice_click(object sender, EventArgs e)
        {
            string[] info = ((Button)sender).Name.Split('_');
            int register = Convert.ToInt32(info[1]);
            int id = Convert.ToInt32(info[2]);

            foreach (Device device in m_regionModel.getDeviceList()) {
                if (device.Id == id) {
                    if (device.Register == register) {
                        m_nowDevice = device;
                        if (device.DeviceType == DeviceTypeEnum.Single)
                            m_form_deviceDetail.SMFGroupBox.Visible = false;
                        else if (device.DeviceType == DeviceTypeEnum.Double)
                            m_form_deviceDetail.SMFGroupBox.Visible = true;
                    }
                }
            }

            if (m_loginHandler.isLogin) {
                m_form_deviceDetail.AdminFunBox.Visible = true;

                if (m_nowDevice.DeviceType == DeviceTypeEnum.Double)
                {
                    m_form_deviceDetail.SMFAlarmGroupBox.Visible = true;

                    m_form_deviceDetail.PressureLow.Value = m_nowDevice.getSMFModel().PressureAlarmSettingLow;
                    m_form_deviceDetail.PressureHight.Value = m_nowDevice.getSMFModel().PressureAlarmSettingHight;

                    m_form_deviceDetail.TemperatureLow.Value = m_nowDevice.getSMFModel().TemperatureAlarmSettingLow;
                    m_form_deviceDetail.TemperatureHight.Value = m_nowDevice.getSMFModel().TemperatureAlarmSettingHight;

                    m_form_deviceDetail.HumidityLow.Value = m_nowDevice.getSMFModel().HumidityAlarmSettingLow;
                    m_form_deviceDetail.HumidityHight.Value = m_nowDevice.getSMFModel().HumidityAlarmSettingHight;
                }
            }
            else {
                m_form_deviceDetail.AdminFunBox.Visible = false;
                m_form_deviceDetail.SMFAlarmGroupBox.Visible = false;
            }

            m_form_deviceDetail.FFUGroupBox.Text = "FFU ID: " + info[2];
            //先update UI 避免按下去沒有資料
            detailUpdate();   

            m_form_deviceDetail.ShowDialog();
        }

        private void btnDeviceAC_click(object sender, EventArgs e)
        {
            string[] info = ((Button)sender).Name.Split('_');
            int register = Convert.ToInt32(info[1]);
            int id = Convert.ToInt32(info[2]);

            foreach (Device device in m_regionModel.getDeviceList()) {
                if (device.Id == id) {
                    if (device.Register == register) {
                        m_nowDevice = device;
                        if (device.DeviceType == DeviceTypeEnum.ACSingle)
                            m_form_deviceDetailAC.SMFGroupBox.Visible = false;
                    }
                }
            }

            if (m_loginHandler.isLogin) {
                m_form_deviceDetailAC.AdminFunBox.Visible = true;
            }
            else {
                m_form_deviceDetailAC.AdminFunBox.Visible = false;
            }

            m_form_deviceDetailAC.FFUGroupBox.Text = "FFU ID: " + info[2];
            //先update UI 避免按下去沒有資料
            detailACUpdate();
            m_form_deviceDetailAC.ShowDialog();
        }

        private void initButtonDisplay(Button btn)
        {
            btn.BackColor = Info.DISCONNECT_COLOR;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
            btn.MouseMove += new MouseEventHandler(Button_MouseMove);
            btn.MouseLeave += new EventHandler(Button_MouseLeave);
        }

        private void Button_MouseMove(object sender, EventArgs e)
        {
            ((Button)sender).FlatAppearance.BorderSize = 2;
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            ((Button)sender).FlatStyle = FlatStyle.Flat;
            ((Button)sender).FlatAppearance.BorderSize = 0;
        }

        public void update()
        {
            mainBtnUpdate();

            if (m_form.Visible) 
                formUpdate();
            if (m_form_deviceDetail.Visible)
                detailUpdate();
            if (m_form_deviceDetailAC.Visible)
                detailACUpdate();
        }

        private void mainBtnUpdate()
        {
            //smf 沒做
            switch (m_regionModel.Status)
            {
                case RegionStatus.Normal:
                    m_mainBtn.BackColor = Info.NORMAL_COLOR;
                    break;
                case RegionStatus.Alarm:
                    m_mainBtn.BackColor = Info.ALARM_COLOR;
                    break;
                case RegionStatus.Disconnect:
                    m_mainBtn.BackColor = Info.DISCONNECT_COLOR;
                    break;
                case RegionStatus.Stop:
                    m_mainBtn.BackColor = Info.STOP_COLOR;
                    break;
            }
        }

        private void formUpdate()
        {
            foreach (Device device in m_regionModel.getDeviceList()) {
                switch (device.Status)
                {
                    case DeviceStatusEnum.Disconnect:
                        m_deviceBtnMap[device.Register][device.Id].BackColor = Info.DISCONNECT_COLOR;
                        break;
                    case DeviceStatusEnum.FFUAlarm:
                        m_deviceBtnMap[device.Register][device.Id].BackColor = Info.ALARM_COLOR;
                        break;
                    case DeviceStatusEnum.Normal:
                        m_deviceBtnMap[device.Register][device.Id].BackColor = Info.NORMAL_COLOR;
                        break;
                    case DeviceStatusEnum.SMFAlarm:
                        m_deviceBtnMap[device.Register][device.Id].BackColor = Info.ALARM_COLOR;
                        break;
                    case DeviceStatusEnum.Stop:
                        m_deviceBtnMap[device.Register][device.Id].BackColor = Info.STOP_COLOR;
                        break;
                }
            }
        }

        private void detailUpdate()
        {
            if (m_loginHandler.isLogin) {
                uiVisableUpdate(m_form_deviceDetail.AdminFunBox, true);
                uiVisableUpdate(m_form_deviceDetail.SMFAlarmGroupBox, true);
            }
            else {
                uiVisableUpdate(m_form_deviceDetail.AdminFunBox, false);
                uiVisableUpdate(m_form_deviceDetail.SMFAlarmGroupBox, false);
            }

            if (m_nowDevice != null) {

                if (m_nowDevice.IsBypass) {
                    uiColorUpdate(m_form_deviceDetail.BypassOnBtn, Info.NORMAL_COLOR);
                    uiColorUpdate(m_form_deviceDetail.BypassOffBtn, Color.White);
                }
                else {
                    uiColorUpdate(m_form_deviceDetail.BypassOffBtn, Info.ALARM_COLOR);
                    uiColorUpdate(m_form_deviceDetail.BypassOnBtn, Color.White);
                }

                if (m_nowDevice.Operation) {
                    uiColorUpdate(m_form_deviceDetail.OnBtn, Info.NORMAL_COLOR);
                    uiColorUpdate(m_form_deviceDetail.OffBtn, Color.White);
                }
                else {
                    uiColorUpdate(m_form_deviceDetail.OnBtn, Color.White);
                    uiColorUpdate(m_form_deviceDetail.OffBtn, Info.ALARM_COLOR);
                }


                uiUpdate(m_form_deviceDetail.SettingSpeedLabel, m_nowDevice.SettingSpeed.ToString());
                uiUpdate(m_form_deviceDetail.ActualSpeedLabel, m_nowDevice.ActualSpeed.ToString());
                uiUpdate(m_form_deviceDetail.AlarmCodeLabel, m_nowDevice.AlarmCode.ToString());

                if (m_nowDevice.DeviceType == DeviceTypeEnum.Double) {
                    uiUpdate(m_form_deviceDetail.PressureLabel, m_nowDevice.Pressure.ToString());
                    uiUpdate(m_form_deviceDetail.TemperatureLabel, m_nowDevice.Temperature.ToString());
                    uiUpdate(m_form_deviceDetail.HumidityLabel, m_nowDevice.Humidity.ToString());
                    uiUpdate(m_form_deviceDetail.AirVolumeLabel, m_nowDevice.AirVolume.ToString());
                } 
            }
        }

        private void detailACUpdate()
        {
            if (m_loginHandler.isLogin) {
                uiVisableUpdate(m_form_deviceDetailAC.AdminFunBox, true);
            }
            else {
                uiVisableUpdate(m_form_deviceDetailAC.AdminFunBox, false);
            }

            if (m_nowDevice != null) {
                if (m_nowDevice.IsBypass) {
                    uiColorUpdate(m_form_deviceDetail.BypassOnBtn, Info.NORMAL_COLOR);
                    uiColorUpdate(m_form_deviceDetail.BypassOffBtn, Color.White);
                }
                else {
                    uiColorUpdate(m_form_deviceDetail.BypassOffBtn, Info.ALARM_COLOR);
                    uiColorUpdate(m_form_deviceDetail.BypassOnBtn, Color.White);
                }

                if (m_nowDevice.Operation) {
                    uiColorUpdate(m_form_deviceDetailAC.OnBtn, Info.NORMAL_COLOR);
                    uiColorUpdate(m_form_deviceDetailAC.OffBtn, Color.White);
                }
                else {
                    uiColorUpdate(m_form_deviceDetailAC.OnBtn, Color.White);
                    uiColorUpdate(m_form_deviceDetailAC.OffBtn, Info.ALARM_COLOR);
                }

                uiUpdate(m_form_deviceDetailAC.SettingSpeedLabel, m_nowDevice.SettingSpeed.ToString());
                uiUpdate(m_form_deviceDetailAC.ActualSpeedLabel, m_nowDevice.ActualSpeed.ToString());
                uiUpdate(m_form_deviceDetailAC.AlarmCodeLabel, m_nowDevice.AlarmCode.ToString());
            }
        }

        private void uiUpdate(Control ctrl, string value)
        {
            if (ctrl.InvokeRequired) {
                UiUpdatedelegate callBack = new UiUpdatedelegate(uiUpdate);
                ctrl.Invoke(callBack, ctrl, value);
            }
            else {
                ctrl.Text = value;
            }
        }

        private void uiVisableUpdate(Control ctrl, bool value)
        {
            if (ctrl.InvokeRequired) {
                UiVisableUpdatedelegate callBack = new UiVisableUpdatedelegate(uiVisableUpdate);
                ctrl.Invoke(callBack, ctrl, value);
            }
            else {
                ctrl.Visible = value;
            }
        }

        private void uiColorUpdate(Control ctrl, Color color)
        {
            if (ctrl.InvokeRequired) {
                UiColorUpdatedelegate callBack = new UiColorUpdatedelegate(uiColorUpdate);
                ctrl.Invoke(callBack, ctrl, color);
            }
            else {
                ctrl.BackColor = color;
            }   
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (sender.GetType() == typeof(Form_DeviceDetail) || sender.GetType() == typeof(Form_DeviceDetailAC))
                m_nowDevice = null;

            e.Cancel = true; //關閉視窗時取消
            ((Form)sender).Hide();
        }
    }
}
