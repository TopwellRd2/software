﻿namespace topwell_monitor
{
    partial class Form_DeviceDetailAC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_DeviceDetailAC));
            this.Label8 = new System.Windows.Forms.Label();
            this.IndexLabel = new System.Windows.Forms.Label();
            this.OnBtn = new System.Windows.Forms.Button();
            this.OffBtn = new System.Windows.Forms.Button();
            this.SettingSpeedLabel = new System.Windows.Forms.Label();
            this.settingSpeedBtn = new System.Windows.Forms.Button();
            this.AlarmCodeLabel = new System.Windows.Forms.Label();
            this.ActualSpeedLabel = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.BypassOnBtn = new System.Windows.Forms.Button();
            this.AdminFunBox = new System.Windows.Forms.GroupBox();
            this.BypassOffBtn = new System.Windows.Forms.Button();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.HumidityLabel = new System.Windows.Forms.Label();
            this.TemperatureLabel = new System.Windows.Forms.Label();
            this.SMFGroupBox = new System.Windows.Forms.GroupBox();
            this.PressureLabel = new System.Windows.Forms.Label();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.FFUGroupBox = new System.Windows.Forms.GroupBox();
            this.AdminFunBox.SuspendLayout();
            this.SMFGroupBox.SuspendLayout();
            this.FFUGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(8, 38);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(95, 20);
            this.Label8.TabIndex = 0;
            this.Label8.Text = "BY PASS :";
            // 
            // IndexLabel
            // 
            this.IndexLabel.AutoSize = true;
            this.IndexLabel.Location = new System.Drawing.Point(344, 182);
            this.IndexLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IndexLabel.Name = "IndexLabel";
            this.IndexLabel.Size = new System.Drawing.Size(50, 20);
            this.IndexLabel.TabIndex = 19;
            this.IndexLabel.Text = "Index";
            this.IndexLabel.Visible = false;
            // 
            // OnBtn
            // 
            this.OnBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OnBtn.Location = new System.Drawing.Point(200, 20);
            this.OnBtn.Margin = new System.Windows.Forms.Padding(4);
            this.OnBtn.Name = "OnBtn";
            this.OnBtn.Size = new System.Drawing.Size(67, 42);
            this.OnBtn.TabIndex = 12;
            this.OnBtn.Text = "ON";
            this.OnBtn.UseVisualStyleBackColor = true;
            // 
            // OffBtn
            // 
            this.OffBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OffBtn.Location = new System.Drawing.Point(135, 20);
            this.OffBtn.Margin = new System.Windows.Forms.Padding(4);
            this.OffBtn.Name = "OffBtn";
            this.OffBtn.Size = new System.Drawing.Size(67, 42);
            this.OffBtn.TabIndex = 11;
            this.OffBtn.Text = "OFF";
            this.OffBtn.UseVisualStyleBackColor = true;
            // 
            // SettingSpeedLabel
            // 
            this.SettingSpeedLabel.AutoSize = true;
            this.SettingSpeedLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SettingSpeedLabel.Location = new System.Drawing.Point(135, 82);
            this.SettingSpeedLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SettingSpeedLabel.Name = "SettingSpeedLabel";
            this.SettingSpeedLabel.Size = new System.Drawing.Size(104, 20);
            this.SettingSpeedLabel.TabIndex = 10;
            this.SettingSpeedLabel.Text = "SettingSpeed";
            // 
            // settingSpeedBtn
            // 
            this.settingSpeedBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.settingSpeedBtn.Location = new System.Drawing.Point(264, 75);
            this.settingSpeedBtn.Margin = new System.Windows.Forms.Padding(4);
            this.settingSpeedBtn.Name = "settingSpeedBtn";
            this.settingSpeedBtn.Size = new System.Drawing.Size(84, 36);
            this.settingSpeedBtn.TabIndex = 8;
            this.settingSpeedBtn.Text = "設定";
            this.settingSpeedBtn.UseVisualStyleBackColor = true;
            // 
            // AlarmCodeLabel
            // 
            this.AlarmCodeLabel.AutoSize = true;
            this.AlarmCodeLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AlarmCodeLabel.Location = new System.Drawing.Point(135, 182);
            this.AlarmCodeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AlarmCodeLabel.Name = "AlarmCodeLabel";
            this.AlarmCodeLabel.Size = new System.Drawing.Size(96, 20);
            this.AlarmCodeLabel.TabIndex = 7;
            this.AlarmCodeLabel.Text = "AlarmCode";
            // 
            // ActualSpeedLabel
            // 
            this.ActualSpeedLabel.AutoSize = true;
            this.ActualSpeedLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ActualSpeedLabel.Location = new System.Drawing.Point(135, 132);
            this.ActualSpeedLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ActualSpeedLabel.Name = "ActualSpeedLabel";
            this.ActualSpeedLabel.Size = new System.Drawing.Size(102, 20);
            this.ActualSpeedLabel.TabIndex = 6;
            this.ActualSpeedLabel.Text = "ActualSpeed";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label4.Location = new System.Drawing.Point(24, 182);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(99, 20);
            this.Label4.TabIndex = 5;
            this.Label4.Text = "警報代碼 :";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label3.Location = new System.Drawing.Point(24, 132);
            this.Label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(99, 20);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "實際段數 :";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label2.Location = new System.Drawing.Point(24, 32);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(99, 20);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "狀        態 :";
            // 
            // BypassOnBtn
            // 
            this.BypassOnBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BypassOnBtn.Location = new System.Drawing.Point(180, 28);
            this.BypassOnBtn.Margin = new System.Windows.Forms.Padding(4);
            this.BypassOnBtn.Name = "BypassOnBtn";
            this.BypassOnBtn.Size = new System.Drawing.Size(67, 42);
            this.BypassOnBtn.TabIndex = 13;
            this.BypassOnBtn.Text = "ON";
            this.BypassOnBtn.UseVisualStyleBackColor = true;
            // 
            // AdminFunBox
            // 
            this.AdminFunBox.Controls.Add(this.BypassOnBtn);
            this.AdminFunBox.Controls.Add(this.BypassOffBtn);
            this.AdminFunBox.Controls.Add(this.Label8);
            this.AdminFunBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AdminFunBox.Location = new System.Drawing.Point(416, 14);
            this.AdminFunBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AdminFunBox.Name = "AdminFunBox";
            this.AdminFunBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AdminFunBox.Size = new System.Drawing.Size(257, 395);
            this.AdminFunBox.TabIndex = 18;
            this.AdminFunBox.TabStop = false;
            this.AdminFunBox.Text = "進階功能";
            // 
            // BypassOffBtn
            // 
            this.BypassOffBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BypassOffBtn.Location = new System.Drawing.Point(113, 28);
            this.BypassOffBtn.Margin = new System.Windows.Forms.Padding(4);
            this.BypassOffBtn.Name = "BypassOffBtn";
            this.BypassOffBtn.Size = new System.Drawing.Size(67, 42);
            this.BypassOffBtn.TabIndex = 12;
            this.BypassOffBtn.Text = "OFF";
            this.BypassOffBtn.UseVisualStyleBackColor = true;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label7.Location = new System.Drawing.Point(20, 130);
            this.Label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(99, 20);
            this.Label7.TabIndex = 27;
            this.Label7.Text = "濕        度 :";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label6.Location = new System.Drawing.Point(20, 80);
            this.Label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(104, 20);
            this.Label6.TabIndex = 26;
            this.Label6.Text = "溫        度 : ";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label5.Location = new System.Drawing.Point(20, 30);
            this.Label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(99, 20);
            this.Label5.TabIndex = 25;
            this.Label5.Text = "壓        差 :";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label1.Location = new System.Drawing.Point(24, 82);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(99, 20);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "設定段數 :";
            // 
            // HumidityLabel
            // 
            this.HumidityLabel.AutoSize = true;
            this.HumidityLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HumidityLabel.Location = new System.Drawing.Point(131, 130);
            this.HumidityLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HumidityLabel.Name = "HumidityLabel";
            this.HumidityLabel.Size = new System.Drawing.Size(80, 20);
            this.HumidityLabel.TabIndex = 30;
            this.HumidityLabel.Text = "Humidity";
            // 
            // TemperatureLabel
            // 
            this.TemperatureLabel.AutoSize = true;
            this.TemperatureLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TemperatureLabel.Location = new System.Drawing.Point(131, 80);
            this.TemperatureLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TemperatureLabel.Name = "TemperatureLabel";
            this.TemperatureLabel.Size = new System.Drawing.Size(102, 20);
            this.TemperatureLabel.TabIndex = 29;
            this.TemperatureLabel.Text = "Temperature";
            // 
            // SMFGroupBox
            // 
            this.SMFGroupBox.Controls.Add(this.HumidityLabel);
            this.SMFGroupBox.Controls.Add(this.TemperatureLabel);
            this.SMFGroupBox.Controls.Add(this.PressureLabel);
            this.SMFGroupBox.Controls.Add(this.Label7);
            this.SMFGroupBox.Controls.Add(this.Label6);
            this.SMFGroupBox.Controls.Add(this.Label5);
            this.SMFGroupBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SMFGroupBox.Location = new System.Drawing.Point(13, 244);
            this.SMFGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SMFGroupBox.Name = "SMFGroupBox";
            this.SMFGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SMFGroupBox.Size = new System.Drawing.Size(389, 166);
            this.SMFGroupBox.TabIndex = 17;
            this.SMFGroupBox.TabStop = false;
            this.SMFGroupBox.Text = "SMF";
            // 
            // PressureLabel
            // 
            this.PressureLabel.AutoSize = true;
            this.PressureLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.PressureLabel.Location = new System.Drawing.Point(131, 30);
            this.PressureLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PressureLabel.Name = "PressureLabel";
            this.PressureLabel.Size = new System.Drawing.Size(70, 20);
            this.PressureLabel.TabIndex = 28;
            this.PressureLabel.Text = "Pressure";
            // 
            // FFUGroupBox
            // 
            this.FFUGroupBox.Controls.Add(this.IndexLabel);
            this.FFUGroupBox.Controls.Add(this.OnBtn);
            this.FFUGroupBox.Controls.Add(this.OffBtn);
            this.FFUGroupBox.Controls.Add(this.SettingSpeedLabel);
            this.FFUGroupBox.Controls.Add(this.settingSpeedBtn);
            this.FFUGroupBox.Controls.Add(this.AlarmCodeLabel);
            this.FFUGroupBox.Controls.Add(this.ActualSpeedLabel);
            this.FFUGroupBox.Controls.Add(this.Label4);
            this.FFUGroupBox.Controls.Add(this.Label3);
            this.FFUGroupBox.Controls.Add(this.Label2);
            this.FFUGroupBox.Controls.Add(this.Label1);
            this.FFUGroupBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FFUGroupBox.Location = new System.Drawing.Point(9, 9);
            this.FFUGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.FFUGroupBox.Name = "FFUGroupBox";
            this.FFUGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.FFUGroupBox.Size = new System.Drawing.Size(395, 235);
            this.FFUGroupBox.TabIndex = 16;
            this.FFUGroupBox.TabStop = false;
            this.FFUGroupBox.Text = "FFU";
            // 
            // Form_DeviceDetailAC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 424);
            this.Controls.Add(this.AdminFunBox);
            this.Controls.Add(this.SMFGroupBox);
            this.Controls.Add(this.FFUGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_DeviceDetailAC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detail";
            this.AdminFunBox.ResumeLayout(false);
            this.AdminFunBox.PerformLayout();
            this.SMFGroupBox.ResumeLayout(false);
            this.SMFGroupBox.PerformLayout();
            this.FFUGroupBox.ResumeLayout(false);
            this.FFUGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label IndexLabel;
        internal System.Windows.Forms.Button OnBtn;
        internal System.Windows.Forms.Button OffBtn;
        internal System.Windows.Forms.Label SettingSpeedLabel;
        internal System.Windows.Forms.Button settingSpeedBtn;
        internal System.Windows.Forms.Label AlarmCodeLabel;
        internal System.Windows.Forms.Label ActualSpeedLabel;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button BypassOnBtn;
        internal System.Windows.Forms.GroupBox AdminFunBox;
        internal System.Windows.Forms.Button BypassOffBtn;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label HumidityLabel;
        internal System.Windows.Forms.Label TemperatureLabel;
        internal System.Windows.Forms.GroupBox SMFGroupBox;
        internal System.Windows.Forms.Label PressureLabel;
        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.GroupBox FFUGroupBox;
    }
}