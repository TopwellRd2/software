﻿namespace topwell_monitor
{
    partial class Form_DeviceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_DeviceDetail));
            this.AdminFunBox = new System.Windows.Forms.GroupBox();
            this.BypassOnBtn = new System.Windows.Forms.Button();
            this.BypassOffBtn = new System.Windows.Forms.Button();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.HumidityHight = new System.Windows.Forms.NumericUpDown();
            this.Label14 = new System.Windows.Forms.Label();
            this.HumidityLow = new System.Windows.Forms.NumericUpDown();
            this.Label11 = new System.Windows.Forms.Label();
            this.TemperatureHight = new System.Windows.Forms.NumericUpDown();
            this.HumidityLabel = new System.Windows.Forms.Label();
            this.TemperatureLabel = new System.Windows.Forms.Label();
            this.PressureLabel = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.TemperatureLow = new System.Windows.Forms.NumericUpDown();
            this.Label9 = new System.Windows.Forms.Label();
            this.PressureLow = new System.Windows.Forms.NumericUpDown();
            this.Label10 = new System.Windows.Forms.Label();
            this.SMFAlarmGroupBox = new System.Windows.Forms.GroupBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.PressureHight = new System.Windows.Forms.NumericUpDown();
            this.Label6 = new System.Windows.Forms.Label();
            this.SMFGroupBox = new System.Windows.Forms.GroupBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.IndexLabel = new System.Windows.Forms.Label();
            this.OnBtn = new System.Windows.Forms.Button();
            this.OffBtn = new System.Windows.Forms.Button();
            this.FFUGroupBox = new System.Windows.Forms.GroupBox();
            this.SettingSpeedLabel = new System.Windows.Forms.Label();
            this.settingSpeedBtn = new System.Windows.Forms.Button();
            this.AlarmCodeLabel = new System.Windows.Forms.Label();
            this.ActualSpeedLabel = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.AirVolumeLabel = new System.Windows.Forms.Label();
            this.AdminFunBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HumidityHight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HumidityLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureHight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PressureLow)).BeginInit();
            this.SMFAlarmGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PressureHight)).BeginInit();
            this.SMFGroupBox.SuspendLayout();
            this.FFUGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // AdminFunBox
            // 
            this.AdminFunBox.Controls.Add(this.BypassOnBtn);
            this.AdminFunBox.Controls.Add(this.BypassOffBtn);
            this.AdminFunBox.Controls.Add(this.Label8);
            this.AdminFunBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AdminFunBox.Location = new System.Drawing.Point(416, 17);
            this.AdminFunBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AdminFunBox.Name = "AdminFunBox";
            this.AdminFunBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AdminFunBox.Size = new System.Drawing.Size(280, 98);
            this.AdminFunBox.TabIndex = 21;
            this.AdminFunBox.TabStop = false;
            this.AdminFunBox.Text = "進階功能";
            // 
            // BypassOnBtn
            // 
            this.BypassOnBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BypassOnBtn.Location = new System.Drawing.Point(180, 28);
            this.BypassOnBtn.Margin = new System.Windows.Forms.Padding(4);
            this.BypassOnBtn.Name = "BypassOnBtn";
            this.BypassOnBtn.Size = new System.Drawing.Size(67, 42);
            this.BypassOnBtn.TabIndex = 13;
            this.BypassOnBtn.Text = "ON";
            this.BypassOnBtn.UseVisualStyleBackColor = true;
            // 
            // BypassOffBtn
            // 
            this.BypassOffBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BypassOffBtn.Location = new System.Drawing.Point(113, 28);
            this.BypassOffBtn.Margin = new System.Windows.Forms.Padding(4);
            this.BypassOffBtn.Name = "BypassOffBtn";
            this.BypassOffBtn.Size = new System.Drawing.Size(67, 42);
            this.BypassOffBtn.TabIndex = 12;
            this.BypassOffBtn.Text = "OFF";
            this.BypassOffBtn.UseVisualStyleBackColor = true;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(8, 38);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(95, 20);
            this.Label8.TabIndex = 0;
            this.Label8.Text = "BY PASS :";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(8, 130);
            this.Label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(139, 20);
            this.Label13.TabIndex = 25;
            this.Label13.Text = "濕度警報設定 :";
            // 
            // HumidityHight
            // 
            this.HumidityHight.Location = new System.Drawing.Point(265, 125);
            this.HumidityHight.Margin = new System.Windows.Forms.Padding(4);
            this.HumidityHight.Name = "HumidityHight";
            this.HumidityHight.Size = new System.Drawing.Size(79, 31);
            this.HumidityHight.TabIndex = 24;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(240, 132);
            this.Label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(19, 20);
            this.Label14.TabIndex = 23;
            this.Label14.Text = "~";
            // 
            // HumidityLow
            // 
            this.HumidityLow.Location = new System.Drawing.Point(160, 126);
            this.HumidityLow.Margin = new System.Windows.Forms.Padding(4);
            this.HumidityLow.Name = "HumidityLow";
            this.HumidityLow.Size = new System.Drawing.Size(79, 31);
            this.HumidityLow.TabIndex = 22;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(8, 84);
            this.Label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(139, 20);
            this.Label11.TabIndex = 21;
            this.Label11.Text = "溫度警報設定 :";
            // 
            // TemperatureHight
            // 
            this.TemperatureHight.Location = new System.Drawing.Point(264, 79);
            this.TemperatureHight.Margin = new System.Windows.Forms.Padding(4);
            this.TemperatureHight.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.TemperatureHight.Name = "TemperatureHight";
            this.TemperatureHight.Size = new System.Drawing.Size(79, 31);
            this.TemperatureHight.TabIndex = 20;
            // 
            // HumidityLabel
            // 
            this.HumidityLabel.AutoSize = true;
            this.HumidityLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HumidityLabel.Location = new System.Drawing.Point(130, 119);
            this.HumidityLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HumidityLabel.Name = "HumidityLabel";
            this.HumidityLabel.Size = new System.Drawing.Size(80, 20);
            this.HumidityLabel.TabIndex = 30;
            this.HumidityLabel.Text = "Humidity";
            // 
            // TemperatureLabel
            // 
            this.TemperatureLabel.AutoSize = true;
            this.TemperatureLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.TemperatureLabel.Location = new System.Drawing.Point(128, 76);
            this.TemperatureLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TemperatureLabel.Name = "TemperatureLabel";
            this.TemperatureLabel.Size = new System.Drawing.Size(102, 20);
            this.TemperatureLabel.TabIndex = 29;
            this.TemperatureLabel.Text = "Temperature";
            // 
            // PressureLabel
            // 
            this.PressureLabel.AutoSize = true;
            this.PressureLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.PressureLabel.Location = new System.Drawing.Point(131, 30);
            this.PressureLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PressureLabel.Name = "PressureLabel";
            this.PressureLabel.Size = new System.Drawing.Size(70, 20);
            this.PressureLabel.TabIndex = 28;
            this.PressureLabel.Text = "Pressure";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label7.Location = new System.Drawing.Point(20, 119);
            this.Label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(99, 20);
            this.Label7.TabIndex = 27;
            this.Label7.Text = "濕        度 :";
            // 
            // TemperatureLow
            // 
            this.TemperatureLow.Location = new System.Drawing.Point(160, 80);
            this.TemperatureLow.Margin = new System.Windows.Forms.Padding(4);
            this.TemperatureLow.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.TemperatureLow.Name = "TemperatureLow";
            this.TemperatureLow.Size = new System.Drawing.Size(79, 31);
            this.TemperatureLow.TabIndex = 18;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(241, 39);
            this.Label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(19, 20);
            this.Label9.TabIndex = 15;
            this.Label9.Text = "~";
            // 
            // PressureLow
            // 
            this.PressureLow.Location = new System.Drawing.Point(160, 32);
            this.PressureLow.Margin = new System.Windows.Forms.Padding(4);
            this.PressureLow.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.PressureLow.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.PressureLow.Name = "PressureLow";
            this.PressureLow.Size = new System.Drawing.Size(79, 31);
            this.PressureLow.TabIndex = 14;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(8, 36);
            this.Label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(139, 20);
            this.Label10.TabIndex = 17;
            this.Label10.Text = "壓差警報設定 :";
            // 
            // SMFAlarmGroupBox
            // 
            this.SMFAlarmGroupBox.Controls.Add(this.Label13);
            this.SMFAlarmGroupBox.Controls.Add(this.HumidityHight);
            this.SMFAlarmGroupBox.Controls.Add(this.Label14);
            this.SMFAlarmGroupBox.Controls.Add(this.HumidityLow);
            this.SMFAlarmGroupBox.Controls.Add(this.Label11);
            this.SMFAlarmGroupBox.Controls.Add(this.TemperatureHight);
            this.SMFAlarmGroupBox.Controls.Add(this.Label12);
            this.SMFAlarmGroupBox.Controls.Add(this.TemperatureLow);
            this.SMFAlarmGroupBox.Controls.Add(this.Label10);
            this.SMFAlarmGroupBox.Controls.Add(this.PressureHight);
            this.SMFAlarmGroupBox.Controls.Add(this.Label9);
            this.SMFAlarmGroupBox.Controls.Add(this.PressureLow);
            this.SMFAlarmGroupBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SMFAlarmGroupBox.Location = new System.Drawing.Point(420, 247);
            this.SMFAlarmGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.SMFAlarmGroupBox.Name = "SMFAlarmGroupBox";
            this.SMFAlarmGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.SMFAlarmGroupBox.Size = new System.Drawing.Size(357, 198);
            this.SMFAlarmGroupBox.TabIndex = 22;
            this.SMFAlarmGroupBox.TabStop = false;
            this.SMFAlarmGroupBox.Text = "SMF警報設定";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(241, 86);
            this.Label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(19, 20);
            this.Label12.TabIndex = 19;
            this.Label12.Text = "~";
            // 
            // PressureHight
            // 
            this.PressureHight.Location = new System.Drawing.Point(264, 31);
            this.PressureHight.Margin = new System.Windows.Forms.Padding(4);
            this.PressureHight.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.PressureHight.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.PressureHight.Name = "PressureHight";
            this.PressureHight.Size = new System.Drawing.Size(79, 31);
            this.PressureHight.TabIndex = 16;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label6.Location = new System.Drawing.Point(20, 75);
            this.Label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(104, 20);
            this.Label6.TabIndex = 26;
            this.Label6.Text = "溫        度 : ";
            // 
            // SMFGroupBox
            // 
            this.SMFGroupBox.Controls.Add(this.AirVolumeLabel);
            this.SMFGroupBox.Controls.Add(this.label15);
            this.SMFGroupBox.Controls.Add(this.HumidityLabel);
            this.SMFGroupBox.Controls.Add(this.TemperatureLabel);
            this.SMFGroupBox.Controls.Add(this.PressureLabel);
            this.SMFGroupBox.Controls.Add(this.Label7);
            this.SMFGroupBox.Controls.Add(this.Label6);
            this.SMFGroupBox.Controls.Add(this.Label5);
            this.SMFGroupBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SMFGroupBox.Location = new System.Drawing.Point(13, 247);
            this.SMFGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SMFGroupBox.Name = "SMFGroupBox";
            this.SMFGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SMFGroupBox.Size = new System.Drawing.Size(389, 198);
            this.SMFGroupBox.TabIndex = 20;
            this.SMFGroupBox.TabStop = false;
            this.SMFGroupBox.Text = "SMF";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label5.Location = new System.Drawing.Point(20, 30);
            this.Label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(99, 20);
            this.Label5.TabIndex = 25;
            this.Label5.Text = "壓        差 :";
            // 
            // Timer1
            // 
            this.Timer1.Interval = 500;
            // 
            // IndexLabel
            // 
            this.IndexLabel.AutoSize = true;
            this.IndexLabel.Location = new System.Drawing.Point(332, 182);
            this.IndexLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IndexLabel.Name = "IndexLabel";
            this.IndexLabel.Size = new System.Drawing.Size(50, 20);
            this.IndexLabel.TabIndex = 19;
            this.IndexLabel.Text = "Index";
            this.IndexLabel.Visible = false;
            // 
            // OnBtn
            // 
            this.OnBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OnBtn.Location = new System.Drawing.Point(200, 20);
            this.OnBtn.Margin = new System.Windows.Forms.Padding(4);
            this.OnBtn.Name = "OnBtn";
            this.OnBtn.Size = new System.Drawing.Size(67, 42);
            this.OnBtn.TabIndex = 12;
            this.OnBtn.Text = "ON";
            this.OnBtn.UseVisualStyleBackColor = true;
            // 
            // OffBtn
            // 
            this.OffBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OffBtn.Location = new System.Drawing.Point(135, 20);
            this.OffBtn.Margin = new System.Windows.Forms.Padding(4);
            this.OffBtn.Name = "OffBtn";
            this.OffBtn.Size = new System.Drawing.Size(67, 42);
            this.OffBtn.TabIndex = 11;
            this.OffBtn.Text = "OFF";
            this.OffBtn.UseVisualStyleBackColor = true;
            // 
            // FFUGroupBox
            // 
            this.FFUGroupBox.Controls.Add(this.IndexLabel);
            this.FFUGroupBox.Controls.Add(this.OnBtn);
            this.FFUGroupBox.Controls.Add(this.OffBtn);
            this.FFUGroupBox.Controls.Add(this.SettingSpeedLabel);
            this.FFUGroupBox.Controls.Add(this.settingSpeedBtn);
            this.FFUGroupBox.Controls.Add(this.AlarmCodeLabel);
            this.FFUGroupBox.Controls.Add(this.ActualSpeedLabel);
            this.FFUGroupBox.Controls.Add(this.Label4);
            this.FFUGroupBox.Controls.Add(this.Label3);
            this.FFUGroupBox.Controls.Add(this.Label2);
            this.FFUGroupBox.Controls.Add(this.Label1);
            this.FFUGroupBox.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FFUGroupBox.Location = new System.Drawing.Point(9, 12);
            this.FFUGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.FFUGroupBox.Name = "FFUGroupBox";
            this.FFUGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.FFUGroupBox.Size = new System.Drawing.Size(395, 235);
            this.FFUGroupBox.TabIndex = 19;
            this.FFUGroupBox.TabStop = false;
            this.FFUGroupBox.Text = "FFU";
            // 
            // SettingSpeedLabel
            // 
            this.SettingSpeedLabel.AutoSize = true;
            this.SettingSpeedLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SettingSpeedLabel.Location = new System.Drawing.Point(135, 82);
            this.SettingSpeedLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SettingSpeedLabel.Name = "SettingSpeedLabel";
            this.SettingSpeedLabel.Size = new System.Drawing.Size(104, 20);
            this.SettingSpeedLabel.TabIndex = 10;
            this.SettingSpeedLabel.Text = "SettingSpeed";
            // 
            // settingSpeedBtn
            // 
            this.settingSpeedBtn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.settingSpeedBtn.Location = new System.Drawing.Point(264, 75);
            this.settingSpeedBtn.Margin = new System.Windows.Forms.Padding(4);
            this.settingSpeedBtn.Name = "settingSpeedBtn";
            this.settingSpeedBtn.Size = new System.Drawing.Size(84, 36);
            this.settingSpeedBtn.TabIndex = 8;
            this.settingSpeedBtn.Text = "設定";
            this.settingSpeedBtn.UseVisualStyleBackColor = true;
            // 
            // AlarmCodeLabel
            // 
            this.AlarmCodeLabel.AutoSize = true;
            this.AlarmCodeLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AlarmCodeLabel.Location = new System.Drawing.Point(135, 182);
            this.AlarmCodeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AlarmCodeLabel.Name = "AlarmCodeLabel";
            this.AlarmCodeLabel.Size = new System.Drawing.Size(96, 20);
            this.AlarmCodeLabel.TabIndex = 7;
            this.AlarmCodeLabel.Text = "AlarmCode";
            // 
            // ActualSpeedLabel
            // 
            this.ActualSpeedLabel.AutoSize = true;
            this.ActualSpeedLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ActualSpeedLabel.Location = new System.Drawing.Point(135, 132);
            this.ActualSpeedLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ActualSpeedLabel.Name = "ActualSpeedLabel";
            this.ActualSpeedLabel.Size = new System.Drawing.Size(102, 20);
            this.ActualSpeedLabel.TabIndex = 6;
            this.ActualSpeedLabel.Text = "ActualSpeed";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label4.Location = new System.Drawing.Point(24, 182);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(99, 20);
            this.Label4.TabIndex = 5;
            this.Label4.Text = "警報代碼 :";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label3.Location = new System.Drawing.Point(24, 132);
            this.Label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(99, 20);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "實際轉速 :";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label2.Location = new System.Drawing.Point(24, 32);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(99, 20);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "狀        態 :";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Label1.Location = new System.Drawing.Point(24, 82);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(99, 20);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "設定轉速 :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(19, 160);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 20);
            this.label15.TabIndex = 31;
            this.label15.Text = "風        量 :";
            // 
            // AirVolumeLabel
            // 
            this.AirVolumeLabel.AutoSize = true;
            this.AirVolumeLabel.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.AirVolumeLabel.Location = new System.Drawing.Point(129, 161);
            this.AirVolumeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AirVolumeLabel.Name = "AirVolumeLabel";
            this.AirVolumeLabel.Size = new System.Drawing.Size(99, 20);
            this.AirVolumeLabel.TabIndex = 32;
            this.AirVolumeLabel.Text = "Air Volume";
            // 
            // Form_DeviceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 453);
            this.Controls.Add(this.AdminFunBox);
            this.Controls.Add(this.SMFAlarmGroupBox);
            this.Controls.Add(this.SMFGroupBox);
            this.Controls.Add(this.FFUGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_DeviceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detail";
            this.AdminFunBox.ResumeLayout(false);
            this.AdminFunBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HumidityHight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HumidityLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureHight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PressureLow)).EndInit();
            this.SMFAlarmGroupBox.ResumeLayout(false);
            this.SMFAlarmGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PressureHight)).EndInit();
            this.SMFGroupBox.ResumeLayout(false);
            this.SMFGroupBox.PerformLayout();
            this.FFUGroupBox.ResumeLayout(false);
            this.FFUGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox AdminFunBox;
        internal System.Windows.Forms.Button BypassOnBtn;
        internal System.Windows.Forms.Button BypassOffBtn;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.NumericUpDown HumidityHight;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.NumericUpDown HumidityLow;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.NumericUpDown TemperatureHight;
        internal System.Windows.Forms.Label HumidityLabel;
        internal System.Windows.Forms.Label TemperatureLabel;
        internal System.Windows.Forms.Label PressureLabel;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.NumericUpDown TemperatureLow;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.NumericUpDown PressureLow;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.GroupBox SMFAlarmGroupBox;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.NumericUpDown PressureHight;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.GroupBox SMFGroupBox;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.Label IndexLabel;
        internal System.Windows.Forms.Button OnBtn;
        internal System.Windows.Forms.Button OffBtn;
        internal System.Windows.Forms.GroupBox FFUGroupBox;
        internal System.Windows.Forms.Label SettingSpeedLabel;
        internal System.Windows.Forms.Button settingSpeedBtn;
        internal System.Windows.Forms.Label AlarmCodeLabel;
        internal System.Windows.Forms.Label ActualSpeedLabel;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label AirVolumeLabel;
        internal System.Windows.Forms.Label label15;
    }
}