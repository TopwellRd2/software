﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace topwell_monitor
{
    public partial class Form_Login : Form
    {
        private LoginHandler m_loginHandler;
        private Button Form1LoginBtn;
        private Label Form1LoginLabel;

        public Form_Login(LoginHandler loginHandler, Button btn, Label label)
        {
            InitializeComponent();

            m_loginHandler = loginHandler;
            Form1LoginBtn = btn;
            Form1LoginLabel = label;
        }

        private void EnterBtn_Click(object sender, EventArgs e)
        {
            if (AccountTxt.Text == m_loginHandler.ACCOUNT)
            {
                if (PasswordTxt.Text == m_loginHandler.PASSWORD)
                {
                    m_loginHandler.recordLoginDateTime();
                    m_loginHandler.isLogin = true;

                    Form1LoginBtn.Text = "登出";
                    Form1LoginLabel.Text = "已登入";
                    Form1LoginLabel.ForeColor = Color.Green;

                    init();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("密碼錯誤");
                    init();
                }
            }
            else
            {
                MessageBox.Show("帳號錯誤");
                init();
            }
        }

        private void init()
        {
            AccountTxt.Text = "";
            PasswordTxt.Text = "";
        }
    }
}
