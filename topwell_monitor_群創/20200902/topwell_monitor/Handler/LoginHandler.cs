﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

namespace topwell_monitor
{
    public class LoginHandler
    {
        public readonly string ACCOUNT = "Admin";
        public readonly string PASSWORD = "123456";
        private const int LOGIN_LIMIT_TIME_MINUTE = 30;

        private DateTime m_loginTime;

        public bool isLogin { get; set; }

        public LoginHandler()
        {
            isLogin = false;
        }

        public void recordLoginDateTime()
        {
            m_loginTime = DateTime.Now;
        }

        public void detect()
        {
            if (isLogin) {
                TimeSpan ts = DateTime.Now - m_loginTime;
                if (ts.TotalMinutes > LOGIN_LIMIT_TIME_MINUTE) {
                    isLogin = false;
                }
            }  
        }
   
    }
}
