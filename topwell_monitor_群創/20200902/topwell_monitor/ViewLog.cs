﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace topwell_monitor
{
    class ViewLog
    {
        private RichTextBox m_richTextBox;
        private int m_logCount;

        private delegate void uiUpdateDelegate(string s, Color c);

        public ViewLog(RichTextBox richTextBox)
        {
            m_richTextBox = richTextBox;
            m_logCount = 0;
        }

        public void writeLog(string msg, Color color)
        {
            if (m_richTextBox.InvokeRequired)
            {
                uiUpdateDelegate callBack = new uiUpdateDelegate(writeLog);
                m_richTextBox.Invoke(callBack, new object[] { msg, color });
            }
            else
            {
                if (m_logCount > 1000) {
                    m_richTextBox.Clear();
                    m_logCount = 0;
                }

                m_logCount++;

                m_richTextBox.SelectionStart = m_richTextBox.TextLength;
                m_richTextBox.SelectionLength = 0;
                //m_richTextBox.SelectionFont = new Font("", 10);
                m_richTextBox.SelectionColor = color;
                m_richTextBox.AppendText(msg);
                m_richTextBox.SelectionColor = m_richTextBox.ForeColor;
                m_richTextBox.ScrollToCaret();

            }
        }
    }
}